/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Michael Kern [+]
**  Copyright 2017 Marc Rautenhaus [*, previously +]
**  Copyright 2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "jetcoredetectionactor.h"

// standard library imports
#include <utility>

// related third party imports

// local application imports


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MJetcoreDetectionActor::MJetcoreDetectionActor()
        : MIsosurfaceIntersectionActor(),
          hessianFilter(nullptr),
          arrowsVertexBuffer(nullptr),
          arrowHeads(nullptr)
{
    setActorType(staticActorType());
    setName(getActorType());

    variableSettings->groupProp.setName("Detection variables");

    variableSettings->varsProps[0].setName("∂Vs / ∂n");
    variableSettings->varsProps[0].setTooltip("cf. Kern et al. (2018) Eq. 3");
    variableSettings->varsProps[1].setName("∂Vs / ∂z");
    variableSettings->varsProps[1].setTooltip("cf. Kern et al. (2018) Eq. 4");

    variableSettings->groupProp.removeSubProperty(
            variableSettings->varsIsovalueProps[0]);
    variableSettings->groupProp.removeSubProperty(
            variableSettings->varsIsovalueProps[1]);

    variableSettingsCores =
            std::make_shared<VariableSettingsJetcores>(
                    this, &variableSettings->groupProp);

    lineFilterSettingsCores =
            std::make_shared<FilterSettingsJetcores>(
                    this, &lineFilterSettings->groupProp);

    appearanceSettingsCores =
            std::make_shared<AppearanceSettingsJetcores>(
                    this, &appearanceSettings->groupProp);
}


MJetcoreDetectionActor::~MJetcoreDetectionActor()
{
    delete arrowHeads;

    if (arrowsVertexBuffer)
    {
        MGLResourcesManager::getInstance()->releaseGPUItem(arrowsVertexBuffer);
    }
}


/******************************************************************************
***                        SETTINGS CONSTRUCTORS                            ***
*******************************************************************************/

MJetcoreDetectionActor::VariableSettingsJetcores::VariableSettingsJetcores(
        MJetcoreDetectionActor *hostActor, MProperty *groupProp)
{
    vars[0] = MNWPActorVarProperty("∂²Vs / ∂n²");
    vars[0].setConfigKey("d2vs_dn2");
    vars[0].setTooltip("cf. Kern et al. (2018) Eq. 6");
    vars[0].registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(vars[0]);

    vars[1] = MNWPActorVarProperty("∂²Vs / ∂z²");
    vars[1].setConfigKey("d2Vs_dz2");
    vars[1].setTooltip("cf. Kern et al. (2018) Eq. 6");
    vars[1].registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(vars[1]);

    vars[2] = MNWPActorVarProperty("∂²Vs / ∂n∂z");
    vars[2].setConfigKey("d2Vs_dndz");
    vars[2].setTooltip("cf. Kern et al. (2018) Eq. 6");
    vars[2].registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(vars[2]);

    uVarProp = MNWPActorVarProperty("Eastward wind");
    uVarProp.setConfigKey("eastward_wind");
    uVarProp.registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(uVarProp);

    vVarProp = MNWPActorVarProperty("Northward wind");
    vVarProp.setConfigKey("northward_wind");
    vVarProp.registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(vVarProp);
}


MJetcoreDetectionActor::FilterSettingsJetcores::FilterSettingsJetcores(
        MJetcoreDetectionActor *hostActor, MProperty *groupProp)
{
    lambdaThresholdProp = MSciFloatProperty("Hessian computation threshold", 0.0f);
    lambdaThresholdProp.setConfigKey("hessian_computation_threshold");
    lambdaThresholdProp.setMinimum(-std::numeric_limits<double>::min());
    lambdaThresholdProp.setSignificantDigits(6);
    lambdaThresholdProp.setStep(0.1f);
    lambdaThresholdProp.setSuffix(" * 10e-9");
    lambdaThresholdProp.setTooltip("Threshold β for Hessian computation; cf. Kern et al. (2018) Sect. 3.5.1");
    lambdaThresholdProp.registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(lambdaThresholdProp);

    angleThresholdProp = MFloatProperty("Angle threshold", 180.f);
    angleThresholdProp.setConfigKey("angle_threshold");
    angleThresholdProp.setMinMax(0.0f, 180.0f);
    angleThresholdProp.setDecimals(2);
    angleThresholdProp.setStep(1);
    angleThresholdProp.setSuffix(" °");
    angleThresholdProp.setTooltip("The horizontal angle between two line segments must be smaller than this value; cf. Kern et al. (2018) Sect. 3.5.3");
    angleThresholdProp.registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(angleThresholdProp);

    pressureDiffThresholdProp = MFloatProperty("Pressure difference threshold", 10000.0f);
    pressureDiffThresholdProp.setConfigKey("pressure_difference_threshold");
    pressureDiffThresholdProp.setMinMax(0.0f, 10000.0f);
    pressureDiffThresholdProp.setDecimals(2);
    pressureDiffThresholdProp.setStep(1.0f);
    pressureDiffThresholdProp.setSuffix(" hPa");
    pressureDiffThresholdProp.setTooltip("The pressure difference between two line segments must be smaller than this value; cf. Kern et al. (2018) Sect. 3.5.3");
    pressureDiffThresholdProp.registerValueCallback(hostActor, &MJetcoreDetectionActor::requestLineUpdate);
    groupProp->addSubProperty(pressureDiffThresholdProp);
}


MJetcoreDetectionActor::AppearanceSettingsJetcores::AppearanceSettingsJetcores(
        MJetcoreDetectionActor *hostActor, MProperty *groupProp)
{
    arrowsEnabledProp = MBoolProperty("Arrow heads", false);
    arrowsEnabledProp.setConfigKey("arrow_heads_enabled");
    arrowsEnabledProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp->addSubProperty(arrowsEnabledProp);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MJetcoreDetectionActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MIsosurfaceIntersectionActor::loadConfigurationPrior_V_1_14(settings);

    enableActorUpdates(false);
    settings->beginGroup(getSettingsID());

    variableSettingsCores->vars[0].setIndex(settings->value(
            "d2ud2vdn2Var", -1).toInt());
    variableSettingsCores->vars[1].setIndex(settings->value(
            "d2ud2vdz2Var", -1).toInt());
    variableSettingsCores->vars[2].setIndex(settings->value(
            "d2ud2vdndzVar", -1).toInt());

    variableSettingsCores->uVarProp.setIndex(settings->value("uVar", -1).toInt());
    variableSettingsCores->vVarProp.setIndex(settings->value("vVar", -1).toInt());

    lineFilterSettingsCores->lambdaThresholdProp = settings->value(
            "lambdaThreshold", 0.f).toFloat();
    lineFilterSettingsCores->angleThresholdProp = settings->value(
            "angleThreshold", 50.f).toFloat();
    lineFilterSettingsCores->pressureDiffThresholdProp = settings->value(
            "pressureDiffThreshold", 10.f).toFloat();
    appearanceSettingsCores->arrowsEnabledProp = settings->value(
            "arrowsEnabled", false).toBool();

    settings->endGroup();
    enableActorUpdates(true);
}


bool MJetcoreDetectionActor::synchronizationEvent(
        Met3D::MSynchronizationType syncType, QVector<QVariant> data)
{
    if (!enableAutoComputationProp || variables.size() < 3) return false;

    enableActorUpdates(false);

    // Forward sync event to all variables, so that they get the correct
    // data request, when we request the lines here.
    for (auto var : variables)
    {
        var->synchronizationEvent(syncType, data);
    }
    enableActorUpdates(true);

    startSynchronizing();
    requestIsoSurfaceIntersectionLines();

    return true;
}


void MJetcoreDetectionActor::onAddActorVariable(Met3D::MNWPActorVariable *var)
{
    MIsosurfaceIntersectionActor::onAddActorVariable(var);
    variableSettingsCores->vars[0].addVariable(var);
    variableSettingsCores->vars[1].addVariable(var);
    variableSettingsCores->vars[2].addVariable(var);
    variableSettingsCores->uVarProp.addVariable(var);
    variableSettingsCores->vVarProp.addVariable(var);
}


void
MJetcoreDetectionActor::onDeleteActorVariable(Met3D::MNWPActorVariable *var)
{
    MIsosurfaceIntersectionActor::onDeleteActorVariable(var);
    variableSettingsCores->vars[0].removeVariable(var);
    variableSettingsCores->vars[1].removeVariable(var);
    variableSettingsCores->vars[2].removeVariable(var);
    variableSettingsCores->uVarProp.removeVariable(var);
    variableSettingsCores->vVarProp.removeVariable(var);
}


void
MJetcoreDetectionActor::onChangeActorVariable(Met3D::MNWPActorVariable *var)
{
    MIsosurfaceIntersectionActor::onChangeActorVariable(var);
    variableSettingsCores->vars[0].changeVariable(var);
    variableSettingsCores->vars[1].changeVariable(var);
    variableSettingsCores->vars[2].changeVariable(var);
    variableSettingsCores->uVarProp.changeVariable(var);
    variableSettingsCores->vVarProp.changeVariable(var);
}


/******************************************************************************
***                            PROTECTED METHODS                            ***
*******************************************************************************/

void MJetcoreDetectionActor::initializeActorResources()
{
    MIsosurfaceIntersectionActor::initializeActorResources();

    hessianFilter = std::make_shared<MHessianTrajectoryFilter>();
    addFilter(hessianFilter);

    angleFilter = std::make_shared<MAngleTrajectoryFilter>();
    addFilter(angleFilter);

    pressureDiffFilter =
            std::make_shared<MEndPressureDifferenceTrajectoryFilter>();
    addFilter(pressureDiffFilter);

    arrowHeadsSource = std::make_shared<MTrajectoryArrowHeadsSource>();
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");
    arrowHeadsSource->setMemoryManager(memoryManager);

    connect(arrowHeadsSource.get(), &MTrajectoryArrowHeadsSource::dataRequestCompleted,
            this, &MJetcoreDetectionActor::asynchronousArrowsAvailable);
}


void MJetcoreDetectionActor::requestIsoSurfaceIntersectionLines()
{
    // Only send request if actor is connected to a bounding box and at least
    // one scene view.
    if (getViews().empty() || getBBoxConnection()->getBoundingBox() == nullptr)
    {
        endSynchronizing();
        return;
    }

    if (isCalculating
            || variableSettings->varsProps[0].value() == variableSettings->varsProps[1].value())
    {
        endSynchronizing();
        return;
    }

    // Obtain the two variables that should be intersected.
    auto var1st = variableSettings->varsProps[0].value();
    auto var2nd = variableSettings->varsProps[1].value();

    // Check if times are available for variables, if not, don't compute.
    if (! var1st->getPropertyTime(var1st->initTimeProp).isValid()
        || ! var1st->getPropertyTime(var1st->validTimeProp).isValid())
    {
        endSynchronizing();
        return;
    }

    isCalculating = true;

    // If the user has not selected an ensemble member and at least one variable,
    // then obtain all selected ensemble members.
    if (ensembleSelectionSettings->selectedEnsembleMembers.empty() &&
            !variables.empty() &&
            variables.at(0)->dataSource != nullptr)
    {
        ensembleSelectionSettings->selectedEnsembleMembers = variables.at(0)->
                dataSource->availableEnsembleMembers(
                variables.at(0)->levelType,
                variables.at(0)->variableName);
        QString s = MDataRequestHelper::uintSetToString(
                ensembleSelectionSettings->selectedEnsembleMembers);
        ensembleSelectionSettings->ensembleMultiMemberProp = s;
    }

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");

    // Create a new instance of an iso-surface intersection source if not created.
    if (isosurfaceSource == nullptr)
    {
        isosurfaceSource = new MIsosurfaceIntersectionSource();
        isosurfaceSource->setMemoryManager(memoryManager);
        setDataSource(isosurfaceSource);
        sysMC->registerDataSource("isosurfaceIntersectionlines",
                                  isosurfaceSource);
    }

    enableActorUpdates(false);
    variableSettings->groupProp.setEnabled(false);
    ensembleSelectionSettings->enabledProp.setEnabled(false);
    enableActorUpdates(true);

    isosurfaceSource->setInputSourceFirstVar(var1st->dataSource);
    isosurfaceSource->setInputSourceSecondVar(var2nd->dataSource);

    // Disable the sync control during computation.
    if (!isSynchronizing)
    {
        if (var2nd->synchronizationControl != nullptr)
        {
            var2nd->synchronizationControl->setEnabled(false);
        }
        else
        {
            if (var1st->synchronizationControl != nullptr)
            {
                var1st->synchronizationControl->setEnabled(false);
            }
        }
    }

    // Set the line request.
    MDataRequestHelper rh;

    MDataRequestHelper var1Request(var1st->buildDataRequest());
    MDataRequestHelper var2Request(var2nd->buildDataRequest());

    QString memberList = "";
    if (ensembleSelectionSettings->enabledProp)
    {
        memberList = MDataRequestHelper
        ::uintSetToString(ensembleSelectionSettings->selectedEnsembleMembers);
    }
    else
    {
        memberList = QString::number(var1st->getEnsembleMember());
    }

    rh.insert("MEMBERS", memberList);

    var1Request.addKeyPrefix("ISOX_VAR1");
    var2Request.addKeyPrefix("ISOX_VAR2");

    rh.unite(var1Request);
    rh.unite(var2Request);

    rh.insert("ISOX_VALUES", QString::number(0)
            + "/" + QString::number(0));

    rh.insert("ISOX_BOUNDING_BOX",
              QString::number(boundingBoxSettings->llcrnLon) + "/"
                      + QString::number(boundingBoxSettings->llcrnLat) + "/"
                      + QString::number(boundingBoxSettings->pBot_hPa) + "/"
                      + QString::number(boundingBoxSettings->urcrnLon) + "/"
                      + QString::number(boundingBoxSettings->urcrnLat) + "/"
                      + QString::number(boundingBoxSettings->pTop_hPa));

    lineRequest = rh.request();

    // Request the crossing lines.
    isosurfaceSource->requestData(lineRequest);
}


void MJetcoreDetectionActor::buildFilterChain(MDataRequestHelper &rh)
{
    MTrajectorySelectionSource *inputSource = isosurfaceSource;

    MNWPActorVariable *varSource = nullptr;

    if (lineFilterSettings->filterVarProp.value() != nullptr)
    {
        varSource =
                dynamic_cast<MNWPActorVariable *>(lineFilterSettings->filterVarProp.value());
    }

    MNWPActorVariable *varThickness = nullptr;

    if (tubeThicknessSettings->mappedVariableProp != nullptr)
    {
        varThickness = dynamic_cast<MNWPActorVariable *>(tubeThicknessSettings
                ->mappedVariableProp.value());
    }

    MNWPActorVariable *varMapped = nullptr;

    if (appearanceSettings->colorVariableProp != nullptr)
    {
        varMapped = dynamic_cast<MNWPActorVariable *>(appearanceSettings->colorVariableProp.value());
    }

    if (arrowHeadsSource && arrowHeads)
    {
        arrowHeadsSource->releaseData(arrowHeads);
        arrowHeads = nullptr;
    }

    // If the user has selected a variable to filter by, set the filter variable
    // and the corresponding filter value.
    if (varSource)
    {
        MDataRequestHelper varFilterHelper = varSource->buildDataRequest();
        varFilterHelper.addKeyPrefix("VARFILTER_VAR_");
        rh.unite(varFilterHelper);

        rh.insert("VARFILTER_MEMBERS", rh.value("MEMBERS"));
        rh.insert("VARFILTER_VALUE",
                  QString::number(lineFilterSettings->valueFilterProp));

        varTrajectoryFilter->setIsosurfaceSource(isosurfaceSource);
        varTrajectoryFilter->setFilterVariableInputSource(
                varSource->dataSource);
        varTrajectoryFilter->setLineRequest(lineRequest);


        Request request = {varTrajectoryFilter, inputSource, rh.request()};
        filterRequests.push_back(request);
        inputSource = varTrajectoryFilter.get();
    }

    auto d2dn2 = dynamic_cast<MNWPActorVariable *>(variableSettingsCores->vars[0].value());
    auto d2dz2 = dynamic_cast<MNWPActorVariable *>(variableSettingsCores->vars[1].value());
    auto d2dndz = dynamic_cast<MNWPActorVariable *>(variableSettingsCores->vars[2].value());

    // Set the Hessian eigenvalue filter.
    MDataRequestHelper hessianVar1 = d2dn2->buildDataRequest();
    MDataRequestHelper hessianVar2 = d2dz2->buildDataRequest();
    MDataRequestHelper hessianVar3 = d2dndz->buildDataRequest();

    hessianVar1.addKeyPrefix("HESSIANFILTER_VAR1_");
    hessianVar2.addKeyPrefix("HESSIANFILTER_VAR2_");
    hessianVar3.addKeyPrefix("HESSIANFILTER_VAR3_");

    rh.unite(hessianVar1);
    rh.unite(hessianVar2);
    rh.unite(hessianVar3);

    rh.insert("HESSIANFILTER_MEMBERS", rh.value("MEMBERS"));
    rh.insert("HESSIANFILTER_VALUE",
              QString::number(
                      lineFilterSettingsCores->lambdaThresholdProp * 10E-9));

    hessianFilter->setIsosurfaceSource(isosurfaceSource);
    hessianFilter->setDerivedInputSource(d2dn2->dataSource, 0);
    hessianFilter->setDerivedInputSource(d2dz2->dataSource, 1);
    hessianFilter->setDerivedInputSource(d2dndz->dataSource, 2);
    hessianFilter->setLineRequest(lineRequest);

    Request hessianFilterRequest = {hessianFilter, inputSource, rh.request()};
    filterRequests.push_back(hessianFilterRequest);

    inputSource = hessianFilter.get();

    // Set the line segment angle filter.
    rh.insert("ANGLEFILTER_MEMBERS", rh.value("MEMBERS"));
    rh.insert("ANGLEFILTER_VALUE",
              QString::number(lineFilterSettingsCores->angleThresholdProp));

    angleFilter->setIsosurfaceSource(isosurfaceSource);
    angleFilter->setLineRequest(lineRequest);

    Request angleFilterRequest = {angleFilter, inputSource, rh.request()};
    filterRequests.push_back(angleFilterRequest);

    inputSource = angleFilter.get();

    // Set the end pressure difference filter.
    rh.insert("ENDPRESSUREDIFFFILTER_MEMBERS", rh.value("MEMBERS"));
    rh.insert("ENDPRESSUREDIFFFILTER_ANGLE",
              QString::number(lineFilterSettingsCores->angleThresholdProp));
    rh.insert("ENDPRESSUREDIFFFILTER_VALUE",
              QString::number(lineFilterSettingsCores->pressureDiffThresholdProp));

    pressureDiffFilter->setIsosurfaceSource(isosurfaceSource);
    pressureDiffFilter->setLineRequest(lineRequest);

    Request pressureDiffFilterRequest = {pressureDiffFilter, inputSource,
                                         rh.request()};
    filterRequests.push_back(pressureDiffFilterRequest);

    inputSource = pressureDiffFilter.get();

    // Set the geometric length filter.
    geomLengthTrajectoryFilter->setLineRequest(lineRequest);
    geomLengthTrajectoryFilter->setIsosurfaceSource(isosurfaceSource);

    rh.insert("GEOLENFILTER_VALUE",
              QString::number(lineFilterSettings->lineLengthFilterProp));
    rh.insert("GEOLENFILTER_OP", "GREATER_OR_EQUAL");

    Request geomLengthTrajFilterRequest = {geomLengthTrajectoryFilter,
                                           inputSource, rh.request()};
    filterRequests.push_back(geomLengthTrajFilterRequest);

    inputSource = geomLengthTrajectoryFilter.get();

    auto uVar = dynamic_cast<MNWPActorVariable *>(variableSettingsCores->uVarProp.value());
    auto vVar = dynamic_cast<MNWPActorVariable *>(variableSettingsCores->vVarProp.value());

    // Set the arrow head filter.
    arrowHeadsSource->setIsosurfaceSource(isosurfaceSource);
    arrowHeadsSource->setLineRequest(lineRequest);
    arrowHeadsSource->setInputSelectionSource(inputSource);
    arrowHeadsSource->setInputSourceUVar(uVar->dataSource);
    arrowHeadsSource->setInputSourceVVar(vVar->dataSource);

    MDataRequestHelper uVarHelper = uVar->buildDataRequest();
    MDataRequestHelper vVarHelper = vVar->buildDataRequest();
    uVarHelper.addKeyPrefix("ARROWHEADS_UVAR_");
    vVarHelper.addKeyPrefix("ARROWHEADS_VVAR_");

    rh.unite(uVarHelper);
    rh.unite(vVarHelper);

    if (varMapped)
    {
        arrowHeadsSource->setInputSourceVar(varMapped->dataSource);

        MDataRequestHelper mappedVarHelper = varMapped->buildDataRequest();
        mappedVarHelper.addKeyPrefix("ARROWHEADS_MAPPEDVAR_");
        rh.unite(mappedVarHelper);
    }
    else
    {
        arrowHeadsSource->setInputSourceVar(nullptr);
    }

    rh.insert("ARROWHEADS_MEMBERS", rh.value("MEMBERS"));

    arrowRequest = rh.request();

    rh.remove("ARROWHEADS_MEMBERS");

    // Set the value trajectory filter. The filter gathers the value information
    // at each intersection line vertex, especially for coloring and
    // thickness mapping.
    valueTrajectorySource->setIsosurfaceSource(isosurfaceSource);
    valueTrajectorySource->setLineRequest(lineRequest);
    valueTrajectorySource->setInputSelectionSource(inputSource);
    if (varMapped)
    {
        valueTrajectorySource->setInputSourceValueVar(varMapped->dataSource);

        MDataRequestHelper mappedHelper = varMapped->buildDataRequest();
        mappedHelper.addKeyPrefix("TRAJECTORYVALUES_VARMAPPED_");
        rh.unite(mappedHelper);
    }
    else
    {
        valueTrajectorySource->setInputSourceValueVar(nullptr);
    }
    if (varThickness)
    {
        valueTrajectorySource
                ->setInputSourceThicknessVar(varThickness->dataSource);

        MDataRequestHelper thicknessHelper = varThickness->buildDataRequest();
        thicknessHelper.addKeyPrefix("TRAJECTORYVALUES_VARTHICKNESS_");
        rh.unite(thicknessHelper);
    }
    else
    {
        valueTrajectorySource->setInputSourceThicknessVar(nullptr);
    }

    rh.insert("TRAJECTORYVALUES_MEMBERS", rh.value("MEMBERS"));

    valueRequest = rh.request();
}


void MJetcoreDetectionActor::onFilterChainEnd()
{
    arrowHeadsSource->requestData(arrowRequest);
}


/******************************************************************************
***                               PUBLIC SLOTS                              ***
*******************************************************************************/

void MJetcoreDetectionActor::asynchronousArrowsAvailable(MDataRequest request)
{
    arrowHeads = arrowHeadsSource->getData(std::move(request));

    arrowsVertexBuffer = arrowHeads->getVertexBuffer();

    buildGPUResources();
}


void MJetcoreDetectionActor::requestLineUpdate()
{
    if (enableAutoComputationProp && !suppressActorUpdates())
    {
        requestIsoSurfaceIntersectionLines();
    }

    emitActorChangedSignal();
}


void MJetcoreDetectionActor::dataFieldChangedEvent()
{
    if (enableAutoComputationProp && variables.size() >= 3)
    {
        if (synchronizationControl != nullptr
            && synchronizationControl->isSynchronizationInProgress()) return;

        requestIsoSurfaceIntersectionLines();
    }
}


void MJetcoreDetectionActor::renderToCurrentContext(
        MSceneViewGLWidget *sceneView)
{
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }

    MIsosurfaceIntersectionActor::renderToCurrentContext(sceneView);

    if (arrowHeads == nullptr)
    {
        // No arrow heads to render.
        return;
    }

    // Draw the arrow heads at the end of each jet core line.
    if (arrowsVertexBuffer && appearanceSettingsCores->arrowsEnabledProp)
    {
        // Draw arrow heads.
        lineTubeShader->bindProgram("ArrowHeads");
        CHECK_GL_ERROR;

        lineTubeShader->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
        CHECK_GL_ERROR;

        lineTubeShader
                ->setUniformValue("tubeRadius", appearanceSettings->tubeRadiusProp);
        CHECK_GL_ERROR;

        lineTubeShader->setUniformValue("geometryColor",
                                        appearanceSettings->tubeColorProp);
        CHECK_GL_ERROR;

        lineTubeShader
                ->setUniformValue("colorMode", appearanceSettings->colorModeProp);
        CHECK_GL_ERROR;

        if (appearanceSettings->colorVariableProp != nullptr &&
                appearanceSettings->transferFunctionProp != nullptr)
        {
            appearanceSettings->transferFunctionProp->getTexture()
                              ->bindToTextureUnit(
                                      static_cast<GLuint>(appearanceSettings
                                              ->textureUnitTransferFunction));
            lineTubeShader->setUniformValue("transferFunction",
                                            appearanceSettings
                                                    ->textureUnitTransferFunction);
            CHECK_GL_ERROR;
            lineTubeShader->setUniformValue("tfMinimum",
                                            appearanceSettings->transferFunctionProp
                                                              ->getMinimumValue());
            CHECK_GL_ERROR;
            lineTubeShader->setUniformValue("tfMaximum",
                                            appearanceSettings->transferFunctionProp
                                                              ->getMaximumValue());
            CHECK_GL_ERROR;
        }

        lineTubeShader->setUniformValue("thicknessRange",
                                        tubeThicknessSettings->thicknessRange);
        CHECK_GL_ERROR;
        lineTubeShader->setUniformValue("thicknessValueRange",
                                        tubeThicknessSettings->valueRange);
        CHECK_GL_ERROR;

        lineTubeShader->setUniformValue("pToWorldZParams", sceneView
                ->pressureToWorldZParameters());
        CHECK_GL_ERROR;

        sceneView->bindShadowMap(lineTubeShader, shadowMapTexUnit);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ARRAY_BUFFER,
                     arrowsVertexBuffer->getVertexBufferObject());
        CHECK_GL_ERROR;

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(float),
                              nullptr);

        glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 7 * sizeof(float),
                              (const GLvoid *) (3 * sizeof(float)));

        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 7 * sizeof(float),
                              (const GLvoid *) (6 * sizeof(float)));

        glEnableVertexAttribArray(0);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(1);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(2);
        CHECK_GL_ERROR;

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        CHECK_GL_ERROR;
        glDrawArrays(GL_POINTS, 0, arrowHeads->getVertices().size());
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
    }
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/


} // namespace Met3D
