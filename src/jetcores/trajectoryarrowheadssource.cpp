/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectoryarrowheadssource.h"

// standard library imports

// related third party imports

// local application imports


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryArrowHeadsSource::MTrajectoryArrowHeadsSource()
        : MScheduledDataSource(),
          isoSurfaceIntersectionSource(nullptr),
          inputSelectionSource(nullptr),
          inputSources({nullptr, nullptr, nullptr})
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MTrajectoryArrowHeadsSource::setIsosurfaceSource(
        MIsosurfaceIntersectionSource *s)
{
    if (isoSurfaceIntersectionSource != nullptr)
    {
        deregisterInputSource(isoSurfaceIntersectionSource);
    }

    isoSurfaceIntersectionSource = s;
    registerInputSource(isoSurfaceIntersectionSource);
    enablePassThrough(isoSurfaceIntersectionSource);
}


void MTrajectoryArrowHeadsSource::setInputSelectionSource(
        MTrajectorySelectionSource *s)
{
    if (inputSelectionSource != nullptr)
    {
        deregisterInputSource(inputSelectionSource);
    }

    inputSelectionSource = s;
    registerInputSource(inputSelectionSource);
}


void MTrajectoryArrowHeadsSource::setInputSourceUVar(
        MWeatherPredictionDataSource *inputSource)
{
    if (inputSources[0] != nullptr)
    {
        deregisterInputSource(inputSources[0], "ARROWHEADS_UVAR_");
    }

    inputSources[0] = inputSource;
    registerInputSource(inputSources[0], "ARROWHEADS_UVAR_");
    enablePassThrough(inputSources[0]);
}


void MTrajectoryArrowHeadsSource::setInputSourceVVar(
        MWeatherPredictionDataSource *inputSource)
{
    if (inputSources[1] != nullptr)
    {
        deregisterInputSource(inputSources[1], "ARROWHEADS_VVAR_");
    }

    inputSources[1] = inputSource;
    registerInputSource(inputSources[1], "ARROWHEADS_VVAR_");
    enablePassThrough(inputSources[1]);
}


void MTrajectoryArrowHeadsSource::setInputSourceVar(
        MWeatherPredictionDataSource *inputSource)
{
    if (inputSources[2] != nullptr)
    {
        deregisterInputSource(inputSources[2], "ARROWHEADS_MAPPEDVAR_");
    }

    inputSources[2] = inputSource;

    if (!inputSources[2])
    {
        return;
    }

    registerInputSource(inputSources[2], "ARROWHEADS_MAPPEDVAR_");
    enablePassThrough(inputSources[2]);
}


MTrajectoryArrowHeads *
MTrajectoryArrowHeadsSource::produceData(MDataRequest request)
{
    assert(isoSurfaceIntersectionSource != nullptr);
    assert(inputSelectionSource != nullptr);
    assert(inputSources[0] != nullptr);
    assert(inputSources[1] != nullptr);
    assert(lineRequest != "");

    MDataRequestHelper rh(request);

    const QStringList members = rh.value("ARROWHEADS_MEMBERS").split("/");

    MIsosurfaceIntersectionLines *lineSource =
            isoSurfaceIntersectionSource->getData(lineRequest);

    rh.removeAll(locallyRequiredKeys());
    MTrajectoryEnsembleSelection *lineSelection = dynamic_cast<MTrajectoryEnsembleSelection *>(inputSelectionSource
            ->getData(rh.request()));

    MStructuredGrid *gridU;
    MStructuredGrid *gridV;
    MStructuredGrid *gridSource = nullptr;

    const int numTrajectories = lineSelection->getNumTrajectories();

    auto result = new MTrajectoryArrowHeads(numTrajectories);

    QVector<GLint> ensStartIndices = lineSelection->getEnsembleStartIndices();
    QVector<GLsizei> ensIndexCounts = lineSelection->getEnsembleIndexCount();

    // Loop through each member and filter the lines corresponding to that
    // member.
    for (int ee = 0; ee < members.size(); ++ee)
    {
        // Obtain the start and end line index for the current member.
        const int ensStartIndex = ensStartIndices[ee];
        const int ensIndexCount = ensIndexCounts[ee];
        const int ensEndIndex = ensStartIndex + ensIndexCount;

        QString varRequest = varRequests[0];
        varRequests.pop_front();
        gridU = inputSources[0]->getData(varRequest);

        varRequest = varRequests[0];
        varRequests.pop_front();
        gridV = inputSources[1]->getData(varRequest);

        if (inputSources[2])
        {
            varRequest = varRequests[0];
            varRequests.pop_front();
            gridSource = inputSources[2]->getData(varRequest);
        }

        for (int i = ensStartIndex; i < ensEndIndex; ++i)
        {
            int startIndex = lineSelection->getStartIndices()[i];
            const int indexCount = lineSelection->getIndexCount()[i];
            const int endIndex = startIndex + indexCount;

            // Get the points of the first trajectory segment.
            const QVector3D &p0 = lineSource->getVertices()[startIndex];
            const QVector3D &p1 = lineSource->getVertices()[startIndex + 1];

            // Get the points of the last trajectory segment.
            const QVector3D &pn0 = lineSource->getVertices()[endIndex - 2];
            const QVector3D &pn1 = lineSource->getVertices()[endIndex - 1];

            // Compute the tangents at each segment. Each tangent points to the
            // end point of the trajectory line.
            QVector2D tangent0(p1.x() - p0.x(), p1.y() - p0.y());
            tangent0.normalize();
            QVector2D tangentn(pn1.x() - pn0.x(), pn1.y() - pn0.y());
            tangentn.normalize();

            // Obtain the wind direction at the start point.
            QVector2D wind(gridU->interpolateValue(p0),
                           gridV->interpolateValue(p0));
            wind.normalize();

            // Compute the dot product between the first tangent and the wind
            // direction.
            const double dotProduct = QVector2D::dotProduct(tangent0, wind);

            // If both vectors point into the same direction, put the
            // arrow head to the end segment of the first segment and let the
            // head point to the inverse tangent direction.
            if (dotProduct <= 0)
            {
                float value = 0;
                if (gridSource)
                { value = gridSource->interpolateValue(p0); }

                result->setVertex(i, {p0, -tangent0, value});
            }
                // Take the position of the last segment and use the segment at
                // the end for the arrow head.
            else
            {
                float value = 0;
                if (gridSource)
                {
                    value = gridSource->interpolateValue(pn1);
                }

                // Add the arrow to the array.
                result->setVertex(i, {pn1, tangentn, value});
            }
        }

        inputSources[0]->releaseData(gridU);
        inputSources[1]->releaseData(gridV);

        if (inputSources[2])
        {
            inputSources[2]->releaseData(gridSource);
        }
    }

    isoSurfaceIntersectionSource->releaseData(lineSource);
    inputSelectionSource->releaseData(lineSelection);

    return result;
}


MTask *MTrajectoryArrowHeadsSource::createTaskGraph(MDataRequest request)
{
    assert(isoSurfaceIntersectionSource != nullptr);
    assert(inputSelectionSource != nullptr);
    assert(inputSources[0] != nullptr);
    assert(inputSources[1] != nullptr);
    assert(lineRequest != "");

    auto task = new MTask(request, this);
    MDataRequestHelper rh(request);

    const QStringList members = rh.value("ARROWHEADS_MEMBERS").split("/");

    MDataRequestHelper uvVars[2] = { rh.subRequest("ARROWHEADS_UVAR_"),
                                     rh.subRequest("ARROWHEADS_VVAR_") };

    for (const QString &member : members)
    {
        MDataRequestHelper rhVar;

        for (int i = 0; i < 2; ++i)
        {
            rhVar = uvVars[i];
            rhVar.insert("MEMBER", member);
            varRequests.push_back(rhVar.request());
            task->addParent(inputSources[i]->getTaskGraph(rhVar.request()));
        }

        if (inputSources[2])
        {
            rhVar = rh.subRequest("ARROWHEADS_MAPPEDVAR_");
            rhVar.insert("MEMBER", member);
            varRequests.push_back(rhVar.request());
            task->addParent(inputSources[2]->getTaskGraph(rhVar.request()));
        }
    }

    rh.removeAll(locallyRequiredKeys());

    // Get previous line selection.
    task->addParent(inputSelectionSource->getTaskGraph(rh.request()));

    task->addParent(isoSurfaceIntersectionSource
                            ->getTaskGraph(lineRequest));

    return task;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MTrajectoryArrowHeadsSource::locallyRequiredKeys()
{
    return (QStringList()
            << "ARROWHEADS_MEMBERS"
    );
}

} // namespace Met3D
