/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "hessiantrajectoryfilter.h"

// standard library imports
#include <fstream>

// related third party imports
#include <omp.h>

// local application imports

// Enables the output of eigenvalues per line vertex into a text file.
//#define DEBUG_HESSIAN


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MHessianTrajectoryFilter::MHessianTrajectoryFilter()
        : MTrajectoryFilter(getBaseRequestKeys()),
          isoSurfaceIntersectionSource(nullptr),
          multiVarInputSources{nullptr, nullptr, nullptr}
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MHessianTrajectoryFilter::
setIsosurfaceSource(MIsosurfaceIntersectionSource *s)
{
    if (isoSurfaceIntersectionSource != nullptr)
    {
        deregisterInputSource(isoSurfaceIntersectionSource);
    }

    isoSurfaceIntersectionSource = s;
    registerInputSource(isoSurfaceIntersectionSource);
    enablePassThrough(isoSurfaceIntersectionSource);
}


void MHessianTrajectoryFilter::
setDerivedInputSource(MWeatherPredictionDataSource *multiVarFilter, int i)
{
    if (i > 2 || i < 0) return;

    QString prefix = "HESSIANFILTER_VAR%1_";
    prefix = prefix.arg(i + 1);

    if (multiVarInputSources[i] != nullptr)
    {
        deregisterInputSource(multiVarInputSources[i], prefix);
    }

    multiVarInputSources[i] = multiVarFilter;
    registerInputSource(multiVarFilter, prefix);
    enablePassThrough(multiVarFilter);
}


MTrajectoryEnsembleSelection *MHessianTrajectoryFilter::produceData(
        MDataRequest request)
{
    assert(isoSurfaceIntersectionSource != nullptr);
    assert(multiVarInputSources[0] != nullptr);
    assert(multiVarInputSources[1] != nullptr);
    assert(multiVarInputSources[2] != nullptr);

#ifdef DEBUG_HESSIAN
    std::ofstream debugFile;
#endif

    MDataRequestHelper rh(request);

    double lambdaThreshold = rh.value("HESSIANFILTER_VALUE").toDouble();
    const QStringList members = rh.value("HESSIANFILTER_MEMBERS").split("/");

    MIsosurfaceIntersectionLines *lineSource =
            isoSurfaceIntersectionSource->getData(lineRequest);

    rh.removeAll(locallyRequiredKeys());
    MTrajectoryEnsembleSelection *lineSelection =
            dynamic_cast<MTrajectoryEnsembleSelection *>(
                    inputSelectionSource->getData(rh.request()));

    // Counts the number of new trajectories.
    int newNumTrajectories = 0;

    QVector<GLint> newStartIndices;
    QVector<GLsizei> newIndexCounts;

    QVector<GLint> newEnsStartIndices;
    QVector<GLsizei> newEnsIndexCounts;

    QVector<GLint> ensStartIndices = lineSelection->getEnsembleStartIndices();
    QVector<GLsizei> ensIndexCounts = lineSelection->getEnsembleIndexCount();

    // Loop through each member and filter the lines corresponding to that
    // member.
    for (int ee = 0; ee < members.size(); ++ee)
    {
        // Obtain the start and end line index for the current member.
        const int ensStartIndex = ensStartIndices[ee];
        const int ensIndexCount = ensIndexCounts[ee];
        const int ensEndIndex = ensStartIndex + ensIndexCount;

        // Obtain grid containing the second derivative along n (d²/dn²)
        QString varRequest = varRequests[0];
        MStructuredGrid *gridDDN = multiVarInputSources[0]->getData(varRequest);
        varRequests.remove(0);

        // Obtain grid containing the second derivative along z (d²/dz²)
        varRequest = varRequests[0];
        MStructuredGrid *gridDDZ = multiVarInputSources[1]->getData(varRequest);
        varRequests.remove(0);

        // Obtain grid containing the mixed partial derivative along n and z
        // (d²/dndz)
        varRequest = varRequests[0];
        MStructuredGrid *gridDNDZ = multiVarInputSources[2]->getData(varRequest);
        varRequests.remove(0);


#ifdef DEBUG_HESSIAN
        debugFile = std::ofstream("data/hessian_"
                                  + initTime.toStdString() + "-"
                                  + validTime.toStdString()
                                  + "_member" + member.toStdString() + ".txt");
#endif

        const int ensNewStartIndex = newStartIndices.size();
        int ensNewIndexCount;

        for (int i = ensStartIndex; i < ensEndIndex; ++i)
        {
            int startIndex = lineSelection->getStartIndices()[i];
            const int indexCount = lineSelection->getIndexCount()[i];
            const int endIndex = startIndex + indexCount;

            int newIndexCount = 0;

            QVector<bool> fulfilledArr(indexCount, false);

#ifndef DEBUG_HESSIAN
#pragma omp parallel for
#endif
            for (int j = startIndex; j < endIndex; ++j)
            {
                const QVector3D &p = lineSource->getVertices()[j];

                // Get the second derivatives.
                const float dnn = gridDDN->interpolateValue(p);
                const float dzz = gridDDZ->interpolateValue(p);
                const float dndz = gridDNDZ->interpolateValue(p);

                // Compute the eigenvalues
                //float lambdaN = std::numeric_limits<float>::max();
                //float lambdaZ = std::numeric_limits<float>::max();

                //computeEigenvalues(dnn, dzz, dndz, lambdaN, lambdaZ);

                // Since we only have two variables, instead of calculating the Eigenvalues, we calculate the
                // determinant of the hess matrix. (https://en.wikipedia.org/wiki/Hessian_matrix#Second-derivative_test)
                // Then we can simply compare the determinant with a threshold, and get,
                // whether we have a local maximum or not. (https://en.wikipedia.org/wiki/Second_partial_derivative_test)

                const float D = dnn * dzz - dndz * dndz;

                fulfilledArr[endIndex - 1 - j] =
                        dnn < lambdaThreshold && D > -lambdaThreshold;

#ifdef DEBUG_HESSIAN
                debugFile << std::fixed << std::setprecision(12);
                debugFile << "Point(" << p.x() << "," << p.y() << "," << p.z()
                          << "): \t" << "\t | dnn=" << dnn << "\t | dzz=" << dzz
                          << "\t | dndz=" << dndz << "\t" << "\t | lambdaN="
                          << lambdaN << "\t | lambdaZ=" << lambdaZ << "\n";
#endif

                // Check if the eigenvalues are below the user-defined threshold.
                // Save the results in a temporary array. This array is used to
                // fill line gaps if present caused by numerical issues.
//                fulfilledArr[endIndex - 1 - j] = lambdaN <= lambdaThreshold
//                        && lambdaZ <= lambdaThreshold;



            }

            // Search for line gaps (false accepted / rejected) and fill those.
            for (int j = startIndex; j < endIndex; ++j)
            {
                bool isFulfilled = fulfilledArr[endIndex - 1 - j];

                // Obtain the results for the two surrounding vertices.
                int prevJ = endIndex - 1 - std::max(j - 1, startIndex);
                int nextJ = endIndex - 1 - std::min(j + 1, endIndex - 1);

                // If the two surrounding values are classified as maximum
                // or minimum / saddle point, then this point was probably
                // falsely rejected / accepted.
                if (fulfilledArr[prevJ] != isFulfilled &&
                        fulfilledArr[nextJ] != isFulfilled)
                {
                    isFulfilled = fulfilledArr[prevJ];
                }

                if (isFulfilled)
                {
                    newIndexCount++;
                }
                else
                {
                    if (newIndexCount > 0)
                    {
                        newStartIndices.push_back(startIndex);
                        newIndexCounts.push_back(newIndexCount);
                        ++newNumTrajectories;
                    }

                    startIndex = j + 1;
                    newIndexCount = 0;
                }
            }

            // If the remaining vertices fulfill the filter criterion, push them
            // to the next index.
            if (newIndexCount > 1)
            {
                newStartIndices.push_back(startIndex);
                newIndexCounts.push_back(newIndexCount);
                ++newNumTrajectories;
            }
        }

        ensNewIndexCount = newNumTrajectories - ensNewStartIndex;

        newEnsStartIndices.push_back(ensNewStartIndex);
        newEnsIndexCounts.push_back(ensNewIndexCount);

        multiVarInputSources[0]->releaseData(gridDDN);
        multiVarInputSources[1]->releaseData(gridDDZ);
        multiVarInputSources[2]->releaseData(gridDNDZ);

#ifdef DEBUG_HESSIAN
        debugFile.close();
#endif
    }

    // Create the result of line selection and compute the new start indices /
    // index counts.
    auto *filterResult =
            new MWritableTrajectoryEnsembleSelection(
                    newNumTrajectories,
                    lineSelection->getTimes(),
                    lineSelection->getStartGridStride(),
                    members.size());

    for (int k = 0; k < newStartIndices.size(); ++k)
    {
        filterResult->setStartIndex(k, newStartIndices[k]);
        filterResult->setIndexCount(k, newIndexCounts[k]);
    }

    for (int e = 0; e < members.size(); ++e)
    {
        filterResult->setEnsembleStartIndex(e, newEnsStartIndices[e]);
        filterResult->setEnsembleIndexCount(e, newEnsIndexCounts[e]);
    }

    isoSurfaceIntersectionSource->releaseData(lineSource);
    inputSelectionSource->releaseData(lineSelection);
    return filterResult;
}


MTask *MHessianTrajectoryFilter::createTaskGraph(MDataRequest request)
{
    assert(isoSurfaceIntersectionSource != nullptr);
    assert(multiVarInputSources[0] != nullptr);
    assert(multiVarInputSources[1] != nullptr);
    assert(multiVarInputSources[2] != nullptr);
    assert(lineRequest != "");

    auto *task = new MTask(request, this);
    MDataRequestHelper rh(request);

    const QStringList members = rh.value("HESSIANFILTER_MEMBERS").split("/");

    for (int i = 0; i < 3; i++)
    {
        QString prefix = "HESSIANFILTER_VAR%1_";
        prefix = prefix.arg(i + 1);

        MDataRequestHelper rhVar = rh.subRequest(prefix);

        for (const auto &member : members)
        {
            rhVar.insert("MEMBER", member);

            varRequests.push_back(rhVar.request());
            task->addParent(multiVarInputSources[i]->getTaskGraph(rhVar.request()));
        }
    }

    rh.removeAll(locallyRequiredKeys());

    // Get previous line selection.
    task->addParent(inputSelectionSource->getTaskGraph(rh.request()));

    task->addParent(isoSurfaceIntersectionSource
                            ->getTaskGraph(lineRequest));

    return task;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MHessianTrajectoryFilter::locallyRequiredKeys()
{
    return (QStringList()
            << "HESSIANFILTER_VALUE"
            << "HESSIANFILTER_MEMBERS");
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

bool MHessianTrajectoryFilter::computeEigenvalues(const float dnn,
                                                  const float dzz,
                                                  const float dndz,
                                                  float &lambda1,
                                                  float &lambda2)
{
    // Compute the characteristic poly
    const double sumUV = dnn + dzz;
    const double prodUV = dnn * dzz;

    const double rootVal = sumUV * sumUV - 4 * (prodUV - dndz * dndz);

    // If the root is zero / positive, than the solution is non-complex and
    // we are able to obtain two real eigenvalues.
    if (rootVal >= 0)
    {
        const double root = std::sqrt(rootVal);

        // Obtain both eigenvalues
        lambda1 = float((sumUV + root) * 0.5);
        lambda2 = float((sumUV - root) * 0.5);

        return true;
    }
    else
    {
        return false;
    }
}

} // namespace Met3D
