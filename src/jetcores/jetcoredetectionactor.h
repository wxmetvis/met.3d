/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017 Michael Kern [+]
**  Copyright 2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef JETCOREDETECTIONACTOR_H
#define JETCOREDETECTIONACTOR_H

// standard library imports

// related third party imports

// local application imports
#include "isosurfaceintersectionactor.h"
#include "hessiantrajectoryfilter.h"
#include "angletrajectoryfilter.h"
#include "endpressuredifferencetrajectoryfilter.h"
#include "trajectoryarrowheadssource.h"


namespace Met3D
{
class MJetcoreDetectionActor : public MIsosurfaceIntersectionActor
{
Q_OBJECT

public:
    // Do not allow implicit conversion.
    explicit MJetcoreDetectionActor();

    ~MJetcoreDetectionActor() override;

    static QString staticActorType() { return "Jetcore Detection Actor"; }

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "JetcoreDetectionActor"; }

    static QString staticIconFileName() { return "jetcore.png"; }

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    bool synchronizationEvent(MSynchronizationType syncType,
                              QVector<QVariant> data) override;

    void onAddActorVariable(Met3D::MNWPActorVariable *var) override;

    void onDeleteActorVariable(Met3D::MNWPActorVariable *var) override;

    void onChangeActorVariable(Met3D::MNWPActorVariable *var) override;

public slots:

    void asynchronousArrowsAvailable(Met3D::MDataRequest request);
    void requestLineUpdate();

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    /**
     * Generate all GPU resources required to display the results.
     */
    void buildFilterChain(MDataRequestHelper &rh) override;

    /**
     * Called after the filter chain pipeline was finished.
     */
    void onFilterChainEnd() override;

    /**
     * Asynchronously request the crossing lines from the input source.
     */
    void requestIsoSurfaceIntersectionLines() override;

    /**
      Implements MNWPActor::dataFieldChangedEvent() to update the target grid
      if the data field has changed.
      */
    void dataFieldChangedEvent() override;

    /** Settings to select the geo-potential height variable. */
    struct VariableSettingsJetcores
    {
        explicit VariableSettingsJetcores(MJetcoreDetectionActor *hostActor,
                                 MProperty *groupProp);

        /**
         * 2nd derivative variables.
         * [0] = d²/dn²
         * [1] = d²/dz²
         * [2] = d²/dndz
         */

        std::array<MNWPActorVarProperty, 3> vars;

        /**
         * U and V wind variables.
         */

        MNWPActorVarProperty uVarProp;
        MNWPActorVarProperty vVarProp;
    };

    /**
     * Settings to filter the jet-cores with respect to their Hessian
     * eigenvalue, line-segment angle, and pressure difference.
     */
    struct FilterSettingsJetcores
    {
        FilterSettingsJetcores(MJetcoreDetectionActor *hostActor,
                               MProperty *groupProp);

        MSciFloatProperty lambdaThresholdProp;
        MFloatProperty angleThresholdProp;
        MFloatProperty pressureDiffThresholdProp;
    };

    /** Appearance settings for jet cores, in particular arrows at the end. */
    struct AppearanceSettingsJetcores
    {
        AppearanceSettingsJetcores(MJetcoreDetectionActor *hostActor,
                                   MProperty *groupProp);

        MBoolProperty arrowsEnabledProp;
    };

    /** Pointers to the settings instances. */
    std::shared_ptr<VariableSettingsJetcores> variableSettingsCores;
    std::shared_ptr<FilterSettingsJetcores> lineFilterSettingsCores;
    std::shared_ptr<AppearanceSettingsJetcores> appearanceSettingsCores;

    /** Points to the current Hessian eigenvalue filter. */
    std::shared_ptr<MHessianTrajectoryFilter> hessianFilter;

    /** Points to the current line segment angle filter. */
    std::shared_ptr<MAngleTrajectoryFilter> angleFilter;

    /** Points to the current pressure difference filter. */
    std::shared_ptr<MEndPressureDifferenceTrajectoryFilter> pressureDiffFilter;

    /** Points to the current arrow head filter. */
    std::shared_ptr<MTrajectoryArrowHeadsSource> arrowHeadsSource;

    /** Vertex buffer object of arrow heads. */
    GL::MVertexBuffer *arrowsVertexBuffer;

    /** Raw data of arrow heads. */
    MTrajectoryArrowHeads *arrowHeads;

    /** Current request for the arrows */
    QString arrowRequest;

};

} // namespace Met3D

#endif //JETCOREDETECTIONACTOR_H
