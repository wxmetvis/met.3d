/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Michael Kern [+]
**  Copyright 2017 Marc Rautenhaus [+]
**  Copyright 2017 Christoph Heidelmann [+]
**  Copyright 2017 Bianca Tost [+]
**  Copyright 2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "isosurfaceintersectionactor.h"

#include <utility>

// standard library imports

// related third party imports

// local application imports
#include "gxfw/memberselectiondialog.h"
#include "mainwindow.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MIsosurfaceIntersectionActor::MIsosurfaceIntersectionActor()
        : MNWPMultiVarIsolevelActor(),
          MBoundingBoxInterface(this, MBoundingBoxConnectionType::VOLUME),
          MSynchronizedObject(),
          isosurfaceSource(nullptr),
          intersectionLines(nullptr),
          varTrajectoryFilter(nullptr),
          geomLengthTrajectoryFilter(nullptr),
          valueTrajectorySource(nullptr),
          lineSelection(nullptr),
          linesVertexBuffer(nullptr),
          isCalculating(false),
          poleActor(nullptr),
          shadowMapTexUnit(-1),
          vboBoundingBox(nullptr),
          iboBoundingBox(0),
          isSynchronizing(false)
{
    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    computeClickProp = MButtonProperty("Compute intersection", "Compute");
    computeClickProp.registerValueCallback([=]()
    {
        if (getBBoxConnection()->getBoundingBox() == nullptr)
        {
            QMessageBox::information(
                    nullptr, "Compute intersection",
                    "You need to select a bounding box to compute"
                    " intersections");
        }
        else if (!suppressActorUpdates())
        {
            requestIsoSurfaceIntersectionLines();
            emitActorChangedSignal();
        }
    });
    actorPropertiesSupGroup.addSubProperty(computeClickProp);

    enableAutoComputationProp = MBoolProperty("Recompute on property change", true);
    enableAutoComputationProp.setConfigKey("recompute_on_property_change");
    enableAutoComputationProp.registerValueCallback([=]()
    {
        enableActorUpdates(false);
        computeClickProp.setEnabled(!enableAutoComputationProp);
        enableActorUpdates(true);

        if (enableAutoComputationProp && !suppressActorUpdates())
        {
            requestIsoSurfaceIntersectionLines();

            emitActorChangedSignal();
        }
    });
    actorPropertiesSupGroup.addSubProperty(enableAutoComputationProp);

    computeClickProp.setEnabled(!enableAutoComputationProp);

    // Bounding box.
    boundingBoxSettings = std::make_shared<BoundingBoxSettings>();
    insertBoundingBoxProperty(actorPropertiesSupGroup);
    boundingBoxSettings->enabledProp = MBoolProperty("Draw bounding box", true);
    boundingBoxSettings->enabledProp.setConfigKey("draw_bounding_box");
    boundingBoxSettings->enabledProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(boundingBoxSettings->enabledProp);

    variableSettings = std::make_shared<VariableSettings>(this);
    actorPropertiesSupGroup.addSubProperty(variableSettings->groupProp);

    lineFilterSettings = std::make_shared<LineFilterSettings>(this);
    actorPropertiesSupGroup.addSubProperty(lineFilterSettings->groupProp);

    appearanceSettings = std::make_shared<AppearanceSettings>(this);
    actorPropertiesSupGroup.addSubProperty(appearanceSettings->groupProp);

    tubeThicknessSettings = std::make_shared<TubeThicknessSettings>(this);
    appearanceSettings->groupProp.addSubProperty(
            tubeThicknessSettings->groupProp);
    tubeThicknessSettings->groupProp.setEnabled(
            appearanceSettings->colorModeProp.value() == 2);

    ensembleSelectionSettings = std::make_shared<EnsembleSelectionSettings>(
            this);
    actorPropertiesSupGroup.addSubProperty(
            ensembleSelectionSettings->enabledProp);

    // Keep an instance of MovablePoleActor as a "subactor" to place poles
    // along jetstream core lines. This makes it easier for scientists to
    // infer the actual height of these lines in pressure.
    // TODO (tv, 05Jan2024): This is another case, why the poles of the movable pole actor should be reworked to be actor components.
    poleActor = std::make_shared<MMovablePoleActor>();
    poleActor->setName("Dropline appearance");
    poleActor->setMovement(false);

    appearanceSettings->dropModeProp.addSubProperty(poleActor->getPropertyGroup());
    poleActor->setIndividualPoleHeightsEnabled(true);
    // Redraw the actor if its properties have been modified by the user.
    connect(poleActor.get(), SIGNAL(actorChanged()), this,
            SLOT(onPoleActorChanged()));
}


MIsosurfaceIntersectionActor::~MIsosurfaceIntersectionActor()
{
    synchronizeWith(nullptr, false);

    auto glRM = MGLResourcesManager::getInstance();

    if (linesVertexBuffer)
    {
        glRM->releaseGPUItem(linesVertexBuffer);
    }

    if (vboBoundingBox)
    {
        glRM->releaseGPUItem(vboBoundingBox);
    }

    if (appearanceSettings->textureUnitTransferFunction >= 0)
    {
        releaseTextureUnit(appearanceSettings->textureUnitTransferFunction);
    }

    if (shadowMapTexUnit >= 0)
    {
        releaseTextureUnit(shadowMapTexUnit);
    }
}


/******************************************************************************
***                        SETTINGS CONSTRUCTORS                            ***
*******************************************************************************/

MIsosurfaceIntersectionActor::VariableSettings::VariableSettings(
        MIsosurfaceIntersectionActor *hostActor)
        : varsProps(), varsIsovalueProps()
{
    auto const refreshLines = [=]()
    {
        if (hostActor->enableAutoComputationProp && !hostActor->suppressActorUpdates())
        {
            hostActor->requestIsoSurfaceIntersectionLines();
        }

        hostActor->emitActorChangedSignal();
    };

    groupProp = MProperty("Intersection variables");

    varsProps[0] = MNWPActorVarProperty("Variable a");
    varsProps[0].setConfigKey("var_a");
    varsProps[0].registerValueCallback(refreshLines);
    groupProp.addSubProperty(varsProps[0]);

    varsIsovalueProps[0] = MSciFloatProperty("Isovalue a", 0.0f);
    varsIsovalueProps[0].setConfigKey("isovalue_a");
    varsIsovalueProps[0].setMinimum(0.0f);
    varsIsovalueProps[0].setSignificantDigits(5);
    varsIsovalueProps[0].setStep(1.0f);
    varsIsovalueProps[0].registerValueCallback(refreshLines);
    groupProp.addSubProperty(varsIsovalueProps[0]);

    varsProps[1] = MNWPActorVarProperty("Variable b");
    varsProps[1].setConfigKey("var_b");
    varsProps[1].registerValueCallback(refreshLines);
    groupProp.addSubProperty(varsProps[1]);

    varsIsovalueProps[1] = MSciFloatProperty("Isovalue b", 0.0f);
    varsIsovalueProps[1].setConfigKey("isovalue_b");
    varsIsovalueProps[1].setMinimum(0.0f);
    varsIsovalueProps[1].setSignificantDigits(5);
    varsIsovalueProps[1].setStep(1.0f);
    varsIsovalueProps[1].registerValueCallback(refreshLines);
    groupProp.addSubProperty(varsIsovalueProps[1]);
}


MIsosurfaceIntersectionActor::LineFilterSettings::LineFilterSettings(
        MIsosurfaceIntersectionActor *hostActor)
{
    groupProp = MProperty("Filtering");

    const auto refreshLines = [=]()
    {
        if (hostActor->enableAutoComputationProp && !hostActor->suppressActorUpdates())
        {
            hostActor->requestIsoSurfaceIntersectionLines();
        }

        hostActor->emitActorChangedSignal();
    };

    filterVarProp = MNWPActorVarProperty("Variable");
    filterVarProp.setConfigKey("filter_var");
    filterVarProp.addVariable(nullptr); // Add none option.
    filterVarProp.registerValueCallback(refreshLines);
    groupProp.addSubProperty(filterVarProp);

    valueFilterProp = MSciFloatProperty("Variable threshold", 0);
    valueFilterProp.setConfigKey("filter_var_threshold");
    valueFilterProp.setMinimum(0);
    valueFilterProp.setSignificantDigits(5);
    valueFilterProp.setStep(1);
    valueFilterProp.setTooltip("Variable values need to be larger than this threshold for a feature to be shown; cf. Kern et al. (2018) Sect. 3.5");
    valueFilterProp.registerValueCallback(refreshLines);
    groupProp.addSubProperty(valueFilterProp);

    lineLengthFilterProp = MIntProperty("Geometric length", 0);
    lineLengthFilterProp.setConfigKey("geometric_length");
    lineLengthFilterProp.setMinimum(0);
    lineLengthFilterProp.setSuffix(" km");
    lineLengthFilterProp.setTooltip("Features must extend over at least the specified distance; cf. Kern et al. (2018) Sect. 3.5.2");
    lineLengthFilterProp.registerValueCallback(refreshLines);
    groupProp.addSubProperty(lineLengthFilterProp);
}


MIsosurfaceIntersectionActor::AppearanceSettings::AppearanceSettings(
        MIsosurfaceIntersectionActor *hostActor) :
        textureUnitTransferFunction(-1)
{
    groupProp = MProperty("Line appearance");

    const auto onNeedRecompute = [=]()
    {
        if (hostActor->enableAutoComputationProp && !hostActor->suppressActorUpdates())
        {
            hostActor->requestIsoSurfaceIntersectionLines();
        }

        // Enable tube constant tube color when constant color mode is active.
        tubeColorProp.setEnabled(colorModeProp.value() == 0);

        hostActor->emitActorChangedSignal();
    };

    QStringList colorModes;
    colorModes << "Constant" << "Map pressure (hPa)" << "Map variable";
    colorModeProp = MEnumProperty("Colour mode", colorModes);
    colorModeProp.setConfigKey("colour_mode");
    colorModeProp.registerValueCallback(onNeedRecompute);
    groupProp.addSubProperty(colorModeProp);

    tubeColorProp = MColorProperty("Colour", QColor(255, 0, 0));
    tubeColorProp.setConfigKey("colour");
    tubeColorProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(tubeColorProp);

    colorVariableProp = MNWPActorVarProperty("Mapped variable");
    colorVariableProp.setConfigKey("mapped_var");
    colorVariableProp.addVariable(nullptr);
    colorVariableProp.registerValueCallback(onNeedRecompute);
    groupProp.addSubProperty(colorVariableProp);

    transferFunctionProp = MTransferFunction1DProperty("Transfer function");
    transferFunctionProp.setConfigKey("transfer_function");
    transferFunctionProp.registerValueCallback(hostActor,
                                               &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(transferFunctionProp);

    QStringList dropModes;
    dropModes << "Off" << "Start" << "End" << "Start / End" << "Centre" << "Maximum"
              << "Start / Centre / End" << "Start / Max / End";
    dropModeProp = MEnumProperty("Droplines", dropModes, 0);
    dropModeProp.setConfigKey("drop_lines_mode");
    dropModeProp.setTooltip("The mode for the drop lines. "
                            "This is where the vertical poles will be placed.");
    dropModeProp.registerValueCallback([=]()
    {
        if (hostActor->intersectionLines != nullptr)
        {
            hostActor->placePoleActors();
        }

        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(dropModeProp);

    QStringList thicknessModes;
    thicknessModes << "Constant" << "Map variable";
    thicknessModeProp = MEnumProperty("Thickness mode", thicknessModes);
    thicknessModeProp.setConfigKey("thickness_mode");
    thicknessModeProp.registerValueCallback([=]()
    {
        hostActor->tubeThicknessSettings->groupProp.setEnabled(thicknessModeProp == 1);

        if (hostActor->enableAutoComputationProp && !hostActor->suppressActorUpdates())
        {
            hostActor->requestIsoSurfaceIntersectionLines();
        }

        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(thicknessModeProp);

    tubeRadiusProp = MFloatProperty("Tube thickness", 0.2f);
    tubeRadiusProp.setConfigKey("tube_thickness");
    tubeRadiusProp.setMinMax(0.01f, 10.f);
    tubeRadiusProp.setDecimals(2);
    tubeRadiusProp.setStep(0.01);
    tubeRadiusProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(tubeRadiusProp);
}


MIsosurfaceIntersectionActor::TubeThicknessSettings::TubeThicknessSettings(
        MIsosurfaceIntersectionActor *hostActor)
        : valueRange(50.0, 85.0),
          thicknessRange(0.01, 0.5)
{
    groupProp = MProperty("Thickness mapping");

    mappedVariableProp = MNWPActorVarProperty("Mapped variable");
    mappedVariableProp.setConfigKey("thickness_mapped_variable");
    mappedVariableProp.addVariable(nullptr);
    mappedVariableProp.registerValueCallback([=]()
    {
        if (hostActor->enableAutoComputationProp && !hostActor->suppressActorUpdates())
        {
            hostActor->requestIsoSurfaceIntersectionLines();
        }

        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(mappedVariableProp);

    minValueProp = MFloatProperty("Minimum value", valueRange.x());
    minValueProp.setConfigKey("min_value");
    minValueProp.setMinMax(0.0f, 2000.0f);
    minValueProp.setDecimals(2);
    minValueProp.setStep(10);
    minValueProp.registerValueCallback([=]()
    {
        valueRange.setX(minValueProp);
        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(minValueProp);

    maxValueProp = MFloatProperty("Maximum value", valueRange.y());
    maxValueProp.setConfigKey("max_value");
    maxValueProp.setMinMax(0.0f, 2000.0f);
    maxValueProp.setDecimals(2);
    maxValueProp.setStep(10);
    maxValueProp.registerValueCallback([=]()
    {
        valueRange.setY(maxValueProp);
        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(maxValueProp);

    minProp = MFloatProperty("Minimum thickness", thicknessRange.x());
    minProp.setConfigKey("min_thickness");
    minProp.setMinMax(0.0f, 10.0f);
    minProp.setDecimals(2);
    minProp.setStep(0.1f);
    minProp.registerValueCallback([=]()
    {
        thicknessRange.setX(minProp);
        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(minProp);

    maxProp = MFloatProperty("Maximum thickness", thicknessRange.y());
    maxProp.setConfigKey("max_thickness");
    maxProp.setMinMax(0.0f, 10.0f);
    maxProp.setDecimals(2);
    maxProp.setStep(0.1f);
    maxProp.registerValueCallback([=]()
    {
        thicknessRange.setY(maxProp);
        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(maxProp);
}


MIsosurfaceIntersectionActor::EnsembleSelectionSettings
::EnsembleSelectionSettings(MIsosurfaceIntersectionActor *hostActor)
{
    enabledProp = MBoolProperty("Spaghetti plot", false);
    enabledProp.setConfigKey("spaghetti_plot_enabled");
    enabledProp.registerValueCallback([=]()
    {
        hostActor->enableActorUpdates(false);
        ensembleMultiMemberSelectionProp.setEnabled(enabledProp);
        hostActor->enableActorUpdates(true);

        if (hostActor->enableAutoComputationProp && !hostActor->suppressActorUpdates())
        {
            hostActor->requestIsoSurfaceIntersectionLines();
        }

        hostActor->emitActorChangedSignal();
    });

    ensembleMultiMemberSelectionProp = MButtonProperty("Select members", "Select");
    ensembleMultiMemberSelectionProp.setTooltip(
            "Select which ensemble members this variable should utilize");
    ensembleMultiMemberSelectionProp.setEnabled(true);
    ensembleMultiMemberSelectionProp.registerValueCallback([=]()
    {
        if (hostActor->suppressActorUpdates()
                || !enabledProp)
        {
            return;
        }

        MMemberSelectionDialog dlg;
        dlg.setAvailableEnsembleMembers(hostActor->variables.at(0)->dataSource
                                                 ->availableEnsembleMembers(
                                                         hostActor->variables
                                                                  .at(0)
                                                                  ->levelType,
                                                         hostActor->variables
                                                                  .at(0)
                                                                  ->variableName));
        dlg.setSelectedMembers(selectedEnsembleMembers);

        if (dlg.exec() == QDialog::Accepted)
        {
            // Get set of selected members from dialog, update
            // ensembleMultiMemberProperty to display set to user and, if
            // necessary, request new data field.
            QSet<unsigned int> selMembers = dlg.getSelectedMembers();
            if (!selMembers.isEmpty())
            {
                selectedEnsembleMembers = selMembers;
                // Update ensembleMultiMemberProperty to display the currently
                // selected list of ensemble members.
                QString s = MDataRequestHelper::uintSetToString(selectedEnsembleMembers);
                ensembleMultiMemberProp = s;
                ensembleMultiMemberProp.setTooltip(s);

                if (hostActor->enableAutoComputationProp && !hostActor->suppressActorUpdates())
                {
                    hostActor->requestIsoSurfaceIntersectionLines();
                }
            }
            else
            {
                // The user has selected an emtpy set of members. Display a
                // warning and do NOT accept the empty set.
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setText("You need to select at least one member.");
                msgBox.exec();
            }
        }
    });
    enabledProp.addSubProperty(ensembleMultiMemberSelectionProp);

    ensembleMultiMemberProp = MStringProperty("Selected members", "");
    ensembleMultiMemberProp.setConfigKey("selected_members");
    ensembleMultiMemberProp.setEnabled(false);
    enabledProp.addSubProperty(ensembleMultiMemberProp);

    ensembleMultiMemberSelectionProp.setEnabled(enabledProp);

    selectedEnsembleMembers.clear();
    selectedEnsembleMembers << 0;

    ensembleMultiMemberProp = MDataRequestHelper::uintSetToString(
                                            selectedEnsembleMembers);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE 0
#define SHADER_NORMAL_ATTRIBUTE 1


void MIsosurfaceIntersectionActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");

    beginCompileShaders(3);

    compileShadersFromFileWithProgressDialog(
            intersectionLinesShader, "src/glsl/trajectory_tubes.fx.glsl");
    compileShadersFromFileWithProgressDialog(
            boundingBoxShader, "src/glsl/simple_coloured_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(
            lineTubeShader, "src/glsl/simple_geometry_generation.fx.glsl");

    endCompileShaders();
}


void MIsosurfaceIntersectionActor::saveConfigurationHeader(QSettings *settings)
{
    MNWPMultiVarActor::saveConfigurationHeader(settings);

    settings->beginGroup(MIsosurfaceIntersectionActor::getSettingsID());

    // Store the properties of the pole subactor in a separate subgroup.
    settings->beginGroup("SubActor_Pole");
    poleActor->saveActorConfiguration(settings);
    settings->endGroup();

    settings->endGroup();
}


void MIsosurfaceIntersectionActor::loadConfigurationHeader(QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationHeader(settings);

    settings->beginGroup(MIsosurfaceIntersectionActor::getSettingsID());

    settings->beginGroup("SubActor_Pole");
    poleActor->loadActorConfiguration(settings);
    settings->endGroup();

    settings->endGroup();
}


void MIsosurfaceIntersectionActor::saveConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::saveConfiguration(settings);
    MBoundingBoxInterface::saveConfiguration(settings);
}


void MIsosurfaceIntersectionActor::loadConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::loadConfiguration(settings);
    MBoundingBoxInterface::loadConfiguration(settings);
}


void MIsosurfaceIntersectionActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationPrior_V_1_14(settings);
    MBoundingBoxInterface::loadConfiguration(settings);

    enableActorUpdates(false);
    settings->beginGroup(getSettingsID());
    int idx = settings->value("var1stIndex", -1).toInt();
    variableSettings->varsProps[0].setIndex(idx);

    idx = settings->value("var2ndIndex", -1).toInt();
    variableSettings->varsProps[1].setIndex(idx);

    variableSettings->varsIsovalueProps[0] = settings->value("var1stIsovalue", 0)
                                                     .toFloat();
    variableSettings->varsIsovalueProps[1] = settings->value("var2ndIsovalue", 0)
                                                     .toFloat();

    idx = settings->value("varFilterIndex", -1).toInt();
    lineFilterSettings->filterVarProp.setIndex(idx);

    lineFilterSettings->valueFilterProp = settings->value("filterValue",
                                                          0.f).toFloat();
    lineFilterSettings->lineLengthFilterProp = settings->value(
            "filterLineLength", 0.f).toInt();

    appearanceSettings->colorModeProp = settings->value("colorMode", 0).toInt();

    idx = settings->value(
            "varColorIndex", -1).toInt();
    appearanceSettings->colorVariableProp.setIndex(idx);

    appearanceSettings->tubeRadiusProp = settings->value("tubeRadius",
                                                         0.2f).toFloat();

    appearanceSettings->tubeColorProp =
            settings->value("tubeColor", QColor(255, 0, 0)).value<QColor>();

    QString tfName = settings->value("transferFunction", "None").toString();
    while (!setTransferFunction(tfName))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle(getName());
        msgBox.setText(QString("Actor '%1' requires a transfer function "
                               "'%2' that does not exist.\n"
                               "Would you like to load the transfer function "
                               "from file?")
                               .arg(getName()).arg(tfName));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.button(QMessageBox::Yes)->setText("Load transfer function");
        msgBox.button(QMessageBox::No)->setText("Discard dependency");
        msgBox.exec();
        if (msgBox.clickedButton() == msgBox.button(QMessageBox::Yes))
        {
            MSystemManagerAndControl *sysMC =
                    MSystemManagerAndControl::getInstance();
            // Create default actor to get name of actor factory.
            auto *defaultActor = new MTransferFunction1D();
            sysMC->getMainWindow()->getSceneManagementDialog()
                 ->loadRequiredActorFromFile(defaultActor->getName(),
                                             tfName,
                                             settings->fileName());

            for (auto tf : appearanceSettings->transferFunctionProp.getTransferFunctions())
            {
                if (tf->getName() == tfName)
                {
                    appearanceSettings->transferFunctionProp.setValue(tf);
                }
            }

            delete defaultActor;
        }
        else
        {
            break;
        }
    }

    appearanceSettings->thicknessModeProp = settings->value("thicknessMode", 0).toInt();

    appearanceSettings->dropModeProp = settings->value("dropMode", -1).toInt() + 1;

    idx = settings->value(
            "tubeThicknessVariableIndex", -1).toInt();
    tubeThicknessSettings->mappedVariableProp.setIndex(idx);
    tubeThicknessSettings->minValueProp = settings->value("tubeThicknessMinValue",
                                                          50.).toFloat();
    tubeThicknessSettings->maxValueProp = settings->value("tubeThicknessMaxValue",
                                                       85.).toFloat();
    tubeThicknessSettings->minProp = settings->value("tubeThicknessMin",
                                                          0.01).toFloat();
    tubeThicknessSettings->maxProp = settings->value("tubeThicknessMax",
                                                     0.5).toFloat();

    tubeThicknessSettings->groupProp.setEnabled(appearanceSettings->thicknessModeProp == 1);

    ensembleSelectionSettings->enabledProp = settings->value(
            "spaghettiPlotEnabled", false).toBool();

    ensembleSelectionSettings->selectedEnsembleMembers =
            MDataRequestHelper::uintSetFromString(
                    settings->value("ensembleMultiMemberProperty", "0")
                            .toString());

    ensembleSelectionSettings->ensembleMultiMemberSelectionProp
                             .setEnabled(ensembleSelectionSettings
                                                  ->enabledProp);

    enableAutoComputationProp = settings->value("enableAutoComputation",
                                                true).toBool();

    computeClickProp.setEnabled(!enableAutoComputationProp);

    // bounding box settings
    // =====================

    MBoundingBoxInterface::loadConfiguration(settings);
    boundingBoxSettings->enabledProp = settings->value("drawBBox", true).toBool();

    settings->beginGroup("SubActor_Pole");
    poleActor->loadActorConfiguration(settings);
    settings->endGroup();

    if (isInitialized())
    {
        generateVolumeBoxGeometry();
    }

    enableActorUpdates(true);

    settings->endGroup();

    emitActorChangedSignal();
}


QList<MVerticalLevelType>
MIsosurfaceIntersectionActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>()
            << HYBRID_SIGMA_PRESSURE_3D << PRESSURE_LEVELS_3D
            << AUXILIARY_PRESSURE_3D);
}


MNWPActorVariable *MIsosurfaceIntersectionActor::createActorVariable(
        const MSelectableDataVariable &dataSource)
{
    auto newVar = new MNWPActorVariable(this);

    newVar->dataSourceID = dataSource.dataSourceID;
    newVar->levelType = dataSource.levelType;
    newVar->variableName = dataSource.variableName;

    return newVar;
}


bool MIsosurfaceIntersectionActor::synchronizationEvent(
        MSynchronizationType syncType, QVector<QVariant> data)
{
    if (!enableAutoComputationProp || variables.size() < 2) return false;

    enableActorUpdates(false);

    // Forward sync event to all variables, so that they get the correct
    // data request, when we request the lines here.
    for (auto var : variables)
    {
        var->synchronizationEvent(syncType, data);
    }
    enableActorUpdates(true);

    startSynchronizing();
    requestIsoSurfaceIntersectionLines();

    return true;
}


void MIsosurfaceIntersectionActor::synchronizeWith(Met3D::MSyncControl *sync,
                                                   bool updateGUIProperties)
{
    if (synchronizationControl == sync) return;

    if (synchronizationControl != nullptr)
    {
        for (MSceneControl *scene : getScenes())
        {
            scene->variableDeletesSynchronizationWith(synchronizationControl);
        }

#ifdef DIRECT_SYNCHRONIZATION
        synchronizationControl->deregisterSynchronizedClass(this);
#else
        disconnect(synchronizationControl, SIGNAL(initDateTimeChanged(QDateTime)),
           this, SLOT(setInitDateTime(QDateTime)));
disconnect(synchronizationControl, SIGNAL(validDateTimeChanged(QDateTime)),
           this, SLOT(setStartDateTime(QDateTime)));
disconnect(synchronizationControl, SIGNAL(ensembleMemberChanged(int)),
           this, SLOT(setEnsembleMember(int)));
#endif
    }
    // Connect to new sync control and try to switch to its current times.
    synchronizationControl = sync;

    //TODO: (tv, 09Jan2023) Add sync properties. See MTrajectoryActor

    if (sync != nullptr)
    {
        for (MSceneControl *scene : getScenes())
        {
            scene->variableSynchronizesWith(sync);
        }

#ifdef DIRECT_SYNCHRONIZATION
        synchronizationControl->registerSynchronizedClass(this);
#else
        //TODO (tv, 09Jan2024) Implement indirect synchronization. Is it even still used?
#endif
    }
}


void MIsosurfaceIntersectionActor::setDataSource(
        MIsosurfaceIntersectionSource *ds)
{
    if (isosurfaceSource != nullptr)
    {
        disconnect(isosurfaceSource, &MIsosurfaceIntersectionSource::dataRequestCompleted,
                   this, &MIsosurfaceIntersectionActor::asynchronousDataAvailable);
    }

    isosurfaceSource = ds;
    if (isosurfaceSource != nullptr)
    {
        connect(isosurfaceSource, &MIsosurfaceIntersectionSource::dataRequestCompleted,
                this, &MIsosurfaceIntersectionActor::asynchronousDataAvailable);
    }
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MIsosurfaceIntersectionActor::onPoleActorChanged()
{
    emitActorChangedSignal();
}


void MIsosurfaceIntersectionActor::asynchronousDataAvailable(
        MDataRequest request)
{
    if (intersectionLines)
    {
        intersectionLines->releaseVertexBuffer();
        isosurfaceSource->releaseData(intersectionLines);
    }

    intersectionLines = isosurfaceSource->getData(std::move(request));

    MDataRequestHelper rh(lineRequest);

    buildFilterChain(rh);
    requestFilters();
}


void MIsosurfaceIntersectionActor::asynchronousFiltersAvailable(
        MDataRequest request)
{
    if (!intersectionLines)
    {
        endSynchronizing();
        return;
    }

    if (lineSelection != nullptr)
    {
        lineSelectionFilter->releaseData(lineSelection);
    }

    lineSelection = dynamic_cast<MTrajectoryEnsembleSelection *>(
            currentTrajectoryFilter->getData(std::move(request)));

    lineSelectionFilter = currentTrajectoryFilter;
    requestFilters();

    emitActorChangedSignal();
}


void MIsosurfaceIntersectionActor::asynchronousValuesAvailable(
        MDataRequest request)
{
    MTrajectoryValues *values = valueTrajectorySource
            ->getData(std::move(request));
    const QVector<float> &trajValues = values->getValues();

    // Finally build the GPU resources.
    linesData.clear();

    int counter = 0;

    // Obtain the line vertices of each line and write it to the data array.
    for (int i = 0; i < lineSelection->getNumTrajectories(); ++i)
    {
        const int startIndex = lineSelection->getStartIndices()[i];
        const int indexCount = lineSelection->getIndexCount()[i];
        const int endIndex = startIndex + indexCount;

        linesData.push_back({-1, -1, -1, -1, -1});

        for (int j = startIndex; j < endIndex; ++j)
        {
            const QVector3D &point = intersectionLines->getVertices().at(j);
            float varValue = trajValues[counter++];
            float varThicknessValue = trajValues[counter++];

            // Create new line vertex (x/y/z/value/thickness) and push it to
            // the raw data vector.
            linesData.push_back({static_cast<float>(point.x()),
                                 static_cast<float>(point.y()),
                                 static_cast<float>(point.z()),
                                 varValue, varThicknessValue});
        }

        linesData.push_back({-1, -1, -1, -1, -1});
    }

    // Place some poles
    if (appearanceSettings->dropModeProp > 0)
    {
        placePoleActors();
    }

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    glRM->makeCurrent();
    // Create vertex buffer for the intersection lines
    const QString vbKey = QString("intersection_lines_VB-%1").arg(myID);

    GL::MVertexBuffer *vb = dynamic_cast<GL::MVertexBuffer *>(glRM->getGPUItem(
            vbKey));

    if (vb)
    {
        auto buf = dynamic_cast<GL::MFloat5VertexBuffer *>(vb);
        buf->reallocate(nullptr, static_cast<unsigned int>(linesData.size()), 0,
                        true);
        buf->update(reinterpret_cast<GLfloat *>(linesData.data()),
                    linesData.size());
        linesVertexBuffer = vb;
    }
    else
    {
        auto newVb = new GL::MFloat5VertexBuffer(vbKey, linesData.size());

        if (glRM->tryStoreGPUItem(newVb))
        {
            newVb->upload(reinterpret_cast<GLfloat *>(linesData.data()),
                          linesData.size());
        }
        else
        {
            delete newVb;
        }
        linesVertexBuffer = dynamic_cast<GL::MVertexBuffer *>(glRM
                ->getGPUItem(vbKey));
    }

    enableActorUpdates(false);
    variableSettings->groupProp.setEnabled(true);
    ensembleSelectionSettings->enabledProp.setEnabled(true);

    isCalculating = false;

    // Re-enable the sync control.
    if (!isSynchronizing)
    {
        auto var2 = variableSettings->varsProps[1].value();
        if (var2->synchronizationControl != nullptr)
        {
            var2->synchronizationControl->setEnabled(true);
        }
    }

    valueTrajectorySource->releaseData(values);

    enableActorUpdates(true);

    endSynchronizing();

    emitActorChangedSignal();
}


void MIsosurfaceIntersectionActor::isoValueOfVariableChanged()
{
    if (enableAutoComputationProp)
    {
        requestIsoSurfaceIntersectionLines();
    }
}


void MIsosurfaceIntersectionActor::onAddActorVariable(MNWPActorVariable *var)
{
    enableActorUpdates(false);

    variableSettings->varsProps[0].addVariable(var);
    variableSettings->varsProps[1].addVariable(var);
    lineFilterSettings->filterVarProp.addVariable(var);
    appearanceSettings->colorVariableProp.addVariable(var);
    tubeThicknessSettings->mappedVariableProp.addVariable(var);

    enableActorUpdates(true);

    if (enableAutoComputationProp)
    {
        requestIsoSurfaceIntersectionLines();
    }
}


void MIsosurfaceIntersectionActor::onDeleteActorVariable(MNWPActorVariable *var)
{
    enableActorUpdates(false);

    variableSettings->varsProps[0].removeVariable(var);
    variableSettings->varsProps[1].removeVariable(var);
    lineFilterSettings->filterVarProp.removeVariable(var);
    appearanceSettings->colorVariableProp.removeVariable(var);
    tubeThicknessSettings->mappedVariableProp.removeVariable(var);

    enableActorUpdates(true);

    if (enableAutoComputationProp)
    {
        requestIsoSurfaceIntersectionLines();
    }
}


void MIsosurfaceIntersectionActor::onChangeActorVariable(MNWPActorVariable *var)
{
    enableActorUpdates(false);

    variableSettings->varsProps[0].changeVariable(var);
    variableSettings->varsProps[1].changeVariable(var);
    lineFilterSettings->filterVarProp.changeVariable(var);
    appearanceSettings->colorVariableProp.changeVariable(var);
    tubeThicknessSettings->mappedVariableProp.changeVariable(var);

    enableActorUpdates(true);

    if (enableAutoComputationProp)
    {
        requestIsoSurfaceIntersectionLines();
    }
}


void MIsosurfaceIntersectionActor::onBoundingBoxChanged()
{
    // Switching to no bounding box only needs a redraw, but no re-computation
    // because it disables rendering of the actor.
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        emitActorChangedSignal();
        return;
    }

    boundingBoxSettings->llcrnLat = static_cast<GLfloat>(getBBoxConnection()
            ->southLat());
    boundingBoxSettings->urcrnLat = static_cast<GLfloat>(getBBoxConnection()
            ->northLat());

    boundingBoxSettings->llcrnLon = static_cast<GLfloat>(getBBoxConnection()
            ->westLon());
    boundingBoxSettings->urcrnLon = static_cast<GLfloat>(getBBoxConnection()
            ->eastLon());

    boundingBoxSettings->pBot_hPa = static_cast<GLfloat>(getBBoxConnection()
            ->bottomPressure_hPa());
    boundingBoxSettings->pTop_hPa = static_cast<GLfloat>(getBBoxConnection()
            ->topPressure_hPa());

    if (isInitialized())
    {
        generateVolumeBoxGeometry();
    }

    if (enableAutoComputationProp)
    {
        requestIsoSurfaceIntersectionLines();
    }

    emitActorChangedSignal();
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MIsosurfaceIntersectionActor::requestIsoSurfaceIntersectionLines()
{
    // Only send request if actor is connected to a bounding box and at least
    // one scene view.
    if (getViews().empty() || getBBoxConnection()->getBoundingBox() == nullptr)
    {
        endSynchronizing();
        return;
    }

    if (isCalculating
            || variableSettings->varsProps[0].value() == variableSettings->varsProps[1].value())
    {
        endSynchronizing();
        return;
    }

    isCalculating = true;

    // If the user has selected an ensemble member and at least one variable,
    // then obtain all selected ensemble members.
    if (ensembleSelectionSettings->selectedEnsembleMembers.empty()
            && !variables.empty()
            && variables.at(0)->dataSource != nullptr)
    {
        ensembleSelectionSettings->selectedEnsembleMembers = variables.at(0)->
                dataSource->availableEnsembleMembers(variables.at(0)->levelType,
                                                     variables.at(
                                                             0)->variableName);
        QString s = MDataRequestHelper::uintSetToString(
                ensembleSelectionSettings->selectedEnsembleMembers);
        ensembleSelectionSettings->ensembleMultiMemberProp = s;
    }

    // Create a new instance of an iso-surface intersection source if not created.
    if (isosurfaceSource == nullptr)
    {
        MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
        MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");

        isosurfaceSource = new MIsosurfaceIntersectionSource();
        isosurfaceSource->setMemoryManager(memoryManager);

        setDataSource(isosurfaceSource);
        sysMC->registerDataSource("isosurfaceIntersectionlines",
                                  isosurfaceSource);
    }

    enableActorUpdates(false);
    variableSettings->groupProp.setEnabled(false);
    ensembleSelectionSettings->enabledProp.setEnabled(false);
    enableActorUpdates(true);

    // Obtain the two variables that should be intersected.
    auto var1st = variableSettings->varsProps[0];
    auto var2nd = variableSettings->varsProps[1];

    isosurfaceSource->setInputSourceFirstVar(var1st->dataSource);
    isosurfaceSource->setInputSourceSecondVar(var2nd->dataSource);

    // Disable the sync control during computation, when not caused by synchronization.
    if (!isSynchronizing)
    {
        if (var2nd->synchronizationControl != nullptr)
        {
            var2nd->synchronizationControl->setEnabled(false);
        }
        else
        {
            if (var1st->synchronizationControl != nullptr)
            {
                var1st->synchronizationControl->setEnabled(false);
            }
        }
    }

    // Set the line request.
    MDataRequestHelper rh;

    QString memberList = "";
    if (ensembleSelectionSettings->enabledProp)
    {
        memberList = MDataRequestHelper::uintSetToString(
                ensembleSelectionSettings->selectedEnsembleMembers);
    }
    else
    {
        memberList = QString::number(var1st->getEnsembleMember());
    }

    rh.insert("MEMBERS", memberList);

    // Set the variables and iso-values.

    MDataRequestHelper var1Request(var1st->buildDataRequest());
    MDataRequestHelper var2Request(var2nd->buildDataRequest());

    var1Request.addKeyPrefix("ISOX_VAR1");
    var2Request.addKeyPrefix("ISOX_VAR2");

    rh.unite(var1Request);
    rh.unite(var2Request);

    rh.insert("ISOX_VALUES", QString::number(variableSettings->varsIsovalueProps[0])
            + "/" + QString::number(variableSettings->varsIsovalueProps[1]));

    rh.insert("ISOX_BOUNDING_BOX",
              QString::number(boundingBoxSettings->llcrnLon) + "/"
                      + QString::number(boundingBoxSettings->llcrnLat) + "/"
                      + QString::number(boundingBoxSettings->pBot_hPa) + "/"
                      + QString::number(boundingBoxSettings->urcrnLon) + "/"
                      + QString::number(boundingBoxSettings->urcrnLat) + "/"
                      + QString::number(boundingBoxSettings->pTop_hPa));

    lineRequest = rh.request();

    // Request the crossing lines.
    isosurfaceSource->requestData(lineRequest);
}


void MIsosurfaceIntersectionActor::buildFilterChain(MDataRequestHelper &rh)
{
    MTrajectorySelectionSource *inputSource = isosurfaceSource;

    MNWPActorVariable *varSource = nullptr;

    if (lineFilterSettings->filterVarProp.value() != nullptr)
    {
        varSource = lineFilterSettings->filterVarProp;
    }

    MNWPActorVariable *varMapped = nullptr;

    if (appearanceSettings->colorVariableProp != nullptr)
    {
        varMapped = appearanceSettings->colorVariableProp;
    }

    MNWPActorVariable *varThickness = nullptr;

    if (tubeThicknessSettings->mappedVariableProp != nullptr)
    {
        varThickness = tubeThicknessSettings->mappedVariableProp;
    }

    // If the user has selected a variable to filter by, set the filter variable
    // and the corresponding filter value.
    if (varSource != nullptr)
    {
        MDataRequestHelper varFilterHelper = varSource->buildDataRequest();
        varFilterHelper.addKeyPrefix("VARFILTER_VAR_");
        rh.unite(varFilterHelper);

        rh.insert("VARFILTER_MEMBERS", rh.value("MEMBERS"));
        rh.insert("VARFILTER_VALUE",
                  QString::number(lineFilterSettings->valueFilterProp));

        varTrajectoryFilter->setIsosurfaceSource(isosurfaceSource);
        varTrajectoryFilter->setFilterVariableInputSource(
                varSource->dataSource);
        varTrajectoryFilter->setLineRequest(lineRequest);

        Request request = {varTrajectoryFilter, inputSource, rh.request()};

        filterRequests.push_back(request);
        inputSource = varTrajectoryFilter.get();
    }

    // Set the geometric length filter.
    geomLengthTrajectoryFilter->setLineRequest(lineRequest);
    geomLengthTrajectoryFilter->setIsosurfaceSource(isosurfaceSource);

    rh.insert("GEOLENFILTER_VALUE",
              QString::number(lineFilterSettings->lineLengthFilterProp));
    rh.insert("GEOLENFILTER_OP", "GREATER_OR_EQUAL");

    Request request = {geomLengthTrajectoryFilter, inputSource, rh.request()};

    filterRequests.push_back(request);

    inputSource = geomLengthTrajectoryFilter.get();

    // Set the value trajectory filter. The filter gathers the value information
    // at each intersection line vertex, especially for coloring and
    // thickness mapping.
    valueTrajectorySource->setIsosurfaceSource(isosurfaceSource);
    valueTrajectorySource->setLineRequest(lineRequest);
    valueTrajectorySource->setInputSelectionSource(inputSource);
    if (varMapped)
    {
        valueTrajectorySource->setInputSourceValueVar(varMapped->dataSource);

        MDataRequestHelper mappedHelper = varMapped->buildDataRequest();
        mappedHelper.addKeyPrefix("TRAJECTORYVALUES_VARMAPPED_");
        rh.unite(mappedHelper);
    }
    else
    {
        valueTrajectorySource->setInputSourceValueVar(nullptr);
    }
    if (varThickness)
    {
        valueTrajectorySource
                ->setInputSourceThicknessVar(varThickness->dataSource);

        MDataRequestHelper thicknessHelper = varThickness->buildDataRequest();
        thicknessHelper.addKeyPrefix("TRAJECTORYVALUES_VARTHICKNESS_");
        rh.unite(thicknessHelper);
    }
    else
    {
        valueTrajectorySource->setInputSourceThicknessVar(nullptr);
    }

    rh.insert("TRAJECTORYVALUES_MEMBERS", rh.value("MEMBERS"));

    valueRequest = rh.request();
}


void MIsosurfaceIntersectionActor::requestFilters()
{
    if (!intersectionLines)
    {
        return;
    }

    if (!filterRequests.empty())
    {
        const Request filter = filterRequests.first();
        filterRequests.pop_front();

        currentTrajectoryFilter = filter.filter;
        filter.filter->setInputSelectionSource(filter.inputSelectionSource);
        filter.filter->requestData(filter.request);
    }
    else
    {
        onFilterChainEnd();
    }
}


void MIsosurfaceIntersectionActor::buildGPUResources()
{
    valueTrajectorySource->requestData(valueRequest);
}


void MIsosurfaceIntersectionActor::onFilterChainEnd()
{
    buildGPUResources();
}


void MIsosurfaceIntersectionActor::dataFieldChangedEvent()
{
    if (enableAutoComputationProp && variables.size() >= 2)
    {
        if (synchronizationControl != nullptr
                && synchronizationControl->isSynchronizationInProgress()) return;

        requestIsoSurfaceIntersectionLines();
    }
}


void MIsosurfaceIntersectionActor::addFilter(
        const std::shared_ptr<MScheduledDataSource> &trajFilter)
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");
    trajFilter->setMemoryManager(memoryManager);

    connect(trajFilter.get(), &MScheduledDataSource::dataRequestCompleted,
            this, &MIsosurfaceIntersectionActor::asynchronousFiltersAvailable);
}


void MIsosurfaceIntersectionActor::placePoleActors()
{
    if (intersectionLines == nullptr) return;

    enableActorUpdates(false);

    poleActor->removeAllPoles();

    MNWPActorVariable *varSource = nullptr;

    if (appearanceSettings->colorVariableProp != nullptr)
    {
        varSource = appearanceSettings->colorVariableProp;
    }


    int numPoles = lineSelection->getNumTrajectories();
    auto handles = MProgressBar::getInstance()->addTasks(numPoles, "Placing droplines..");

    for (int i = 0; i < numPoles; ++i)
    {
        const int startIndex = lineSelection->getStartIndices()[i];
        const int indexCount = lineSelection->getIndexCount()[i];
        const int endIndex = startIndex + indexCount;

        // Obtain start, middle and endpoint of each intersection line.
        const QVector3D &startPoint = intersectionLines->getVertices()
                                                       .at(startIndex);
        const QVector3D &endPoint = intersectionLines->getVertices()
                                                     .at(endIndex - 1);
        const int midIndex = (endIndex + startIndex) / 2;
        const QVector3D &midPoint = intersectionLines->getVertices()
                                                     .at(midIndex);

        QVector3D maxPoint = midPoint;

        // If variable input source is given, look for the maximum along the
        // intersection line.
        if (varSource)
        {
            QVector3D point = intersectionLines->getVertices()
                                               .at(startIndex + 1);
            float maxValue = varSource->grid->interpolateValue(point);
            int maxIndex = startIndex + 1;

            for (int k = startIndex + 2; k < endIndex - 1; ++k)
            {
                point = intersectionLines->getVertices().at(k);
                float value = varSource->grid->interpolateValue(point);

                if (value > maxValue)
                {
                    maxValue = value;
                    maxIndex = k;
                }
            }

            maxPoint = intersectionLines->getVertices().at(maxIndex);
        }

        switch (appearanceSettings->dropModeProp.value())
        {
        case 1:
            poleActor->addPole(startPoint);
            break;

        case 2:
            poleActor->addPole(endPoint);
            break;

        case 3:
            poleActor->addPole(startPoint);
            poleActor->addPole(endPoint);
            break;

        case 4:
            poleActor->addPole(midPoint);
            break;

        case 5:
            poleActor->addPole(maxPoint);
            break;

        case 6:
            poleActor->addPole(startPoint);
            poleActor->addPole(endPoint);
            poleActor->addPole(midPoint);
            break;

        case 7:
            poleActor->addPole(startPoint);
            poleActor->addPole(endPoint);
            poleActor->addPole(maxPoint);
            break;
        }

        MProgressBar::getInstance()->firstTaskCompleted(handles);
    }

    enableActorUpdates(true);
}


void MIsosurfaceIntersectionActor::initializeActorResources()
{
    // Parent initialisation.
    MNWPMultiVarActor::initializeActorResources();

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    bool loadShaders = false;
    loadShaders |= glRM->generateEffectProgram("isosurfaceIntersectionlines",
                                               intersectionLinesShader);
    loadShaders |= glRM->generateEffectProgram("boundingbox_volume",
                                               boundingBoxShader);
    loadShaders |= glRM->generateEffectProgram("trajectory_line_tubes",
                                               lineTubeShader);

    if (loadShaders)
    {
        reloadShaderEffects();
    }

    // Create vertex shader of bounding box.
    generateVolumeBoxGeometry();

    varTrajectoryFilter = std::make_shared<MVariableTrajectoryFilter>();
    addFilter(varTrajectoryFilter);

    geomLengthTrajectoryFilter =
            std::make_shared<MGeometricLengthTrajectoryFilter>();
    addFilter(geomLengthTrajectoryFilter);

    valueTrajectorySource = std::make_shared<MTrajectoryValueSource>();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");
    valueTrajectorySource->setMemoryManager(memoryManager);

    connect(valueTrajectorySource.get(),
            &MTrajectoryValueSource::dataRequestCompleted,
            this, &MIsosurfaceIntersectionActor::asynchronousValuesAvailable);

    if (appearanceSettings->textureUnitTransferFunction >= 0)
    {
        releaseTextureUnit(appearanceSettings->textureUnitTransferFunction);
    }

    appearanceSettings->textureUnitTransferFunction = assignTextureUnit();

    if (shadowMapTexUnit < 0)
    {
        shadowMapTexUnit = assignTextureUnit();
    }

    // Explicitly initialize the pole actor.
    poleActor->initialize();

    // Start synchronization: TODO (tv, 09Jan2024): Move it and get correct sync control...
    synchronizeWith(MSystemManagerAndControl::getInstance()
                            ->getSyncControl("Synchronization"), false);
}


void MIsosurfaceIntersectionActor::generateVolumeBoxGeometry()
{
    // Define geometry for bounding box.
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    const int numVertices = 8;
    float vertexData[] =
            {
                    0, 0, 0, // node 0
                    0, 1, 0, // node 1
                    1, 1, 0, // node 2
                    1, 0, 0, // node 3
                    0, 0, 1, // node 4
                    0, 1, 1, // node 5
                    1, 1, 1, // node 6
                    1, 0, 1  // node 7
            };

    const int numIndices = 16 + 36;
    GLushort indexData[] =
            {
                    // volume box lines
                    0, 1, 2, 3, 0,
                    4, 7, 3,
                    7, 6, 2,
                    6, 5, 1,
                    5, 4,

                    // bottom
                    0, 3, 1,
                    3, 2, 1,
                    // front
                    0, 4, 7,
                    0, 7, 3,
                    // left
                    0, 1, 4,
                    1, 5, 4,
                    // right
                    3, 7, 2,
                    7, 6, 2,
                    // back
                    1, 2, 6,
                    1, 6, 5,
                    // top
                    5, 6, 7,
                    5, 7, 4
            };

    // Convert vertices to lat/lon/p space.
    for (int i = 0; i < numVertices; i++)
    {
        vertexData[i * 3 + 0] =
                boundingBoxSettings->llcrnLon + vertexData[i * 3 + 0]
                        * (boundingBoxSettings->urcrnLon
                                - boundingBoxSettings->llcrnLon);
        vertexData[i * 3 + 1] =
                boundingBoxSettings->urcrnLat - vertexData[i * 3 + 1]
                        * (boundingBoxSettings->urcrnLat
                                - boundingBoxSettings->llcrnLat);
        vertexData[i * 3 + 2] = (vertexData[i * 3 + 2] == 0)
                                ? boundingBoxSettings->pBot_hPa
                                : boundingBoxSettings->pTop_hPa;
    }

    if (vboBoundingBox)
    {
        auto buf = dynamic_cast<GL::MFloat3VertexBuffer *>(vboBoundingBox);
        buf->update(vertexData, numVertices);
    }
    else
    {
        const QString vboID = QString("vbo_bbox_actor#%1").arg(myID);

        auto buf = new GL::MFloat3VertexBuffer(vboID, numVertices);

        if (glRM->tryStoreGPUItem(buf))
        {
            buf->upload(vertexData, numVertices);
            vboBoundingBox = dynamic_cast<GL::MVertexBuffer *>(glRM
                    ->getGPUItem(vboID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "WARNING: cannot store buffer for volume"
                                 " bbox in GPU memory.");
            delete buf;
            return;
        }

    }

    glGenBuffers(1, &iboBoundingBox);
    CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboBoundingBox);
    CHECK_GL_ERROR;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 numIndices * sizeof(GLushort),
                 indexData,
                 GL_STATIC_DRAW);
    CHECK_GL_ERROR;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    CHECK_GL_ERROR;
}


void MIsosurfaceIntersectionActor::renderBoundingBox(
        MSceneViewGLWidget *sceneView)
{
    boundingBoxShader->bindProgram("Pressure");
    boundingBoxShader->setUniformValue(
            "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    boundingBoxShader->setUniformValue(
            "pToWorldZParams", sceneView->pressureToWorldZParameters());
    boundingBoxShader->setUniformValue(
            "colour", QColor(Qt::black));

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iboBoundingBox);
    CHECK_GL_ERROR;
    vboBoundingBox->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    CHECK_GL_ERROR;
    sceneView->setLineWidth(1);
    CHECK_GL_ERROR;

    glDrawElements(GL_LINE_STRIP, 16, GL_UNSIGNED_SHORT, nullptr);

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void MIsosurfaceIntersectionActor::renderToCurrentContext(
        MSceneViewGLWidget *sceneView)
{
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    { return; }

    if (boundingBoxSettings->enabledProp)
    { renderBoundingBox(sceneView); }

    if (intersectionLines != nullptr &&
            linesVertexBuffer != nullptr &&
            variableSettings->varsProps[0].value() != variableSettings->varsProps[1].value())
    {
        lineTubeShader->bindProgram("Trajectory");
        CHECK_GL_ERROR;

        lineTubeShader->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));

        lineTubeShader->setUniformValue("tubeRadius",
                                        appearanceSettings->tubeRadiusProp);
        lineTubeShader->setUniformValue("geometryColor",
                                        appearanceSettings->tubeColorProp);
        lineTubeShader->setUniformValue("colorMode",
                                        appearanceSettings->colorModeProp);

        if (appearanceSettings->transferFunctionProp != nullptr)
        {
            appearanceSettings->transferFunctionProp->getTexture()
                              ->bindToTextureUnit(
                                      static_cast<GLuint>(appearanceSettings
                                              ->textureUnitTransferFunction));

            lineTubeShader->setUniformValue(
                    "transferFunction",
                    appearanceSettings->textureUnitTransferFunction);
            lineTubeShader->setUniformValue(
                    "tfMinimum",
                    appearanceSettings->transferFunctionProp->getMinimumValue());
            lineTubeShader->setUniformValue(
                    "tfMaximum",
                    appearanceSettings->transferFunctionProp->getMaximumValue());
        }

        lineTubeShader->setUniformValue("thicknessMapping", appearanceSettings->thicknessModeProp == 1);
        lineTubeShader->setUniformValue("thicknessRange",
                                        tubeThicknessSettings->thicknessRange);
        lineTubeShader->setUniformValue("thicknessValueRange",
                                        tubeThicknessSettings->valueRange);

        lineTubeShader->setUniformValue("pToWorldZParams",
                                        sceneView
                                                ->pressureToWorldZParameters());

        sceneView->bindShadowMap(lineTubeShader, shadowMapTexUnit);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ARRAY_BUFFER,
                     linesVertexBuffer->getVertexBufferObject());
        CHECK_GL_ERROR;

        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                              nullptr);

        glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                              (const GLvoid *) (3 * sizeof(float)));

        glVertexAttribPointer(2, 1, GL_FLOAT, GL_FALSE, 5 * sizeof(float),
                              (const GLvoid *) (4 * sizeof(float)));

        glEnableVertexAttribArray(0);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(1);
        CHECK_GL_ERROR;
        glEnableVertexAttribArray(2);
        CHECK_GL_ERROR;

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        CHECK_GL_ERROR;
        glDrawArrays(GL_LINE_STRIP_ADJACENCY, 0, linesData.size());
        CHECK_GL_ERROR;

        glBindBuffer(GL_ARRAY_BUFFER, 0);
    }
    else
    {
        if (enableAutoComputationProp)
        {
            requestIsoSurfaceIntersectionLines();
        }
    }

    if (appearanceSettings->dropModeProp > 0)
    {
        // Render all placed poles.
        poleActor->render(sceneView);

        // And render the labels of all poles.
        MTextManager *tm = MGLResourcesManager::getInstance()->getTextManager();
        tm->renderLabelList(sceneView, poleActor->getLabelsToRender());
    }
}


bool MIsosurfaceIntersectionActor::setTransferFunction(const QString &tfName)
{
    return appearanceSettings->transferFunctionProp.setByName(tfName);
}


void MIsosurfaceIntersectionActor::startSynchronizing()
{
    isSynchronizing = true;
    LOG4CPLUS_TRACE(mlog, "START - Syncronizing");
}


void MIsosurfaceIntersectionActor::endSynchronizing()
{
    if (isSynchronizing && synchronizationControl != nullptr)
    {
        LOG4CPLUS_TRACE(mlog, "END - Syncronizing");
        isSynchronizing = false;
        synchronizationControl->synchronizationCompleted(this);
    }
}


} // namespace Met3D
