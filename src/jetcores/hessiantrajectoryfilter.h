/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef HESSIANTRAJECTORYFILTER_H
#define HESSIANTRAJECTORYFILTER_H

// standard library imports

// related third party imports

// local application imports
#include "trajectories/filter/trajectoryfilter.h"
#include "isosurfaceintersectionsource.h"

namespace Met3D
{
/**
 * Computes the Hessian matrix and its eigenvalues at each trajectory vertex
 * and filters out all lines that are maximal, or rather, that have negative
 * eigenvalues.
 * However, since only two variables are used, instead of calculating the raw
 * Eigenvalues, the filter calculates the determinant of the hess matrix.
 * (https://en.wikipedia.org/wiki/Hessian_matrix#Second-derivative_test)
 * Then the determinant is compared with a threshold, which yields information about a
 * present local maximum or not. (https://en.wikipedia.org/wiki/Second_partial_derivative_test)
 */
class MHessianTrajectoryFilter : public MTrajectoryFilter
{
public:
    explicit MHessianTrajectoryFilter();

    /** Input source for intersection lines. */
    void setIsosurfaceSource(MIsosurfaceIntersectionSource *s);

    /** Input source for partial derivative computation. */
    void setDerivedInputSource(
            MWeatherPredictionDataSource *src, int i);


    /** Set the request that produced the trajectories in the pipeline. */
    void setLineRequest(const QString &request)
    { lineRequest = request; }


    /**
     * Overloads @c MMemoryManagedDataSource::getData() to cast
     * the returned @c MAbstractDataItem to @c MTrajectoryEnsembleSelection
     * that contains the intersection lines filtered by the eigenvalues of the
     * Hessian matrix.
    */
    MTrajectoryEnsembleSelection *getData(MDataRequest request) override
    {
        return dynamic_cast<MTrajectoryEnsembleSelection *>
        (MTrajectoryFilter::getData(request));
    }


    /**
     * Computes the eigenvalues of the Hessian matrix at each line vertex
     * and returns the selection of lines with eigenvalues < threshold.
    */
    MTrajectoryEnsembleSelection *produceData(MDataRequest request) override;

    MTask *createTaskGraph(MDataRequest request) override;

    // TODO (cf, 2024-07-08): This filter implements the TrajectoryFilter
    //  interface, however, it can't be used at the moment in a dynamical
    //  pipeline setup for the trajectory actor, since it has the lineRequest
    //  as attribute. The functionality of this filter is currently hardcoded
    //  for the jetcores. This filter is therefore not registered as a
    //  trajectory filter in the factory.
    QString getFilterName() override { return "Hessian Trajectory Filter"; }
    QStringList getBaseRequestKeys() override { return {"TRAJ_FILTER_HESSIAN"}; }
protected:
    const QStringList locallyRequiredKeys() override;

private:

    /** Computes the eigenvalues of the 2x2 Hessian matrix. */
    inline bool computeEigenvalues(float dnn, float dzz,
                                   float dndz,
                                   float &lambda1, float &lambda2);

    /** Pointer to input source of intersection lines. */
    MIsosurfaceIntersectionSource *isoSurfaceIntersectionSource;

    /** Pointers to the partial derivative sources for three variables. */
    MWeatherPredictionDataSource * multiVarInputSources[3];

    /** Line producing request. */
    QString lineRequest;

    /** Request for each variable. */
    QVector<QString> varRequests;

};


} // namespace Met3D

#endif //HESSIANTRAJECTORYFILTER_H
