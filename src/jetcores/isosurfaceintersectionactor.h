/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017 Michael Kern [+]
**  Copyright 2017 Christoph Heidelmann [+]
**  Copyright 2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef ISOSURFACEINTERSECTIONACTOR_H
#define ISOSURFACEINTERSECTIONACTOR_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>

// local application imports
#include "actors/transferfunction1d.h"

#include "data/datarequest.h"
#include "trajectories/source/trajectorynormalssource.h"

#include "isosurfaceintersectionsource.h"
#include "variabletrajectoryfilter.h"
#include "geometriclengthtrajectoryfilter.h"
#include "trajectoryvaluesource.h"

#include "gxfw/nwpmultivaractor.h"
#include "gxfw/synccontrol.h"
#include "gxfw/gl/texture.h"
#include "gxfw/gl/shadereffect.h"

#include "gxfw/properties/mnwpactorvarproperty.h"


namespace Met3D
{
class MGLResourcesManager;

class MSceneViewGLWidget;

/**
 * @brief MIsosurfaceIntersectionActor displays intersection lines of two
 * crossed iso-surfaces co-located on the same grid.
 */
class MIsosurfaceIntersectionActor : public MNWPMultiVarIsolevelActor,
                                     public MBoundingBoxInterface,
                                     public MSynchronizedObject
{
Q_OBJECT

public:
    explicit MIsosurfaceIntersectionActor();

    ~MIsosurfaceIntersectionActor() override;

    void reloadShaderEffects() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "IsosurfaceIntersectionActor"; }

    static QString staticIconFileName() { return "isosurfaceintersection.png"; }

    static QString staticActorType() { return "Isosurface Intersection Actor"; }

    void saveConfigurationHeader(QSettings *settings) override;

    void loadConfigurationHeader(QSettings *settings) override;

    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    MNWPActorVariable *
    createActorVariable(const MSelectableDataVariable &dataSource) override;

    bool synchronizationEvent(MSynchronizationType syncType,
                              QVector<QVariant> data) override;

    void synchronizeWith(MSyncControl *sync, bool updateGUIProperties) override;

    /**
      Returns a list of the vertical level types that can be handled by this
      actor.
     */
    QList<MVerticalLevelType> supportedLevelTypes() override;

    /**
     * Set the input source that computes the intersection of two iso-surfaces.
     */
    void setDataSource(MIsosurfaceIntersectionSource *ds);

public slots:

    /**
     Called by the iso-surface intersection source when data request by @c
     requestIsoSurfaceIntersectionLines() is ready.
     */
    void asynchronousDataAvailable(Met3D::MDataRequest request);

    /**
     Called by the trajectory filter source when data request by @c
     requestFilters() is ready.
     */
    void asynchronousFiltersAvailable(Met3D::MDataRequest request);

    /**
     Called by the trajectory value source when data request by @c
     buildGPUResources() is ready.
     */
    void asynchronousValuesAvailable(Met3D::MDataRequest request);

    void isoValueOfVariableChanged() override;

    void onAddActorVariable(Met3D::MNWPActorVariable *var) override;

    void onDeleteActorVariable(Met3D::MNWPActorVariable *var) override;

    void onChangeActorVariable(Met3D::MNWPActorVariable *var) override;

    /**
     Emit an actor changed signal if pole actor has been modified by the user.
     This causes both, the jetcore and pole actor to redraw on the current scene.
     */
    void onPoleActorChanged();

    void onBoundingBoxChanged() override;

protected:

    /**
      Loads the shader programs and generates the graticule geometry. The
      geometry is uploaded to the GPU into a vertex buffer object.
     */
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    /**
     * Add a new filter to the intersection line filter pipeline.
     */
    void addFilter(const std::shared_ptr<MScheduledDataSource> &trajFilter);

    /**
     * Request each filter in the filter chain pipeline successively.
     */
    void requestFilters();

    /**
     * Build the filter chain and the corresponding filter request.
     */
    virtual void buildFilterChain(MDataRequestHelper &rh);

    /**
     * Called after the filter chain pipeline was finished.
     */
    virtual void onFilterChainEnd();

    /**
     * Generate all GPU resources required to display the results.
     */
    virtual void buildGPUResources();

    /**
     * Asynchronously request the crossing lines from the input source.
     */
    virtual void requestIsoSurfaceIntersectionLines();

    /**
     * Place pole actors (vertical drop lines) along the intersection lines.
     */
    void placePoleActors();

    bool setTransferFunction(const QString &tfName);

    void renderBoundingBox(MSceneViewGLWidget *sceneView);

    void generateVolumeBoxGeometry();

    /**
      Implements @c MNWPActor::dataFieldChangedEvent() to update the target grid
      if the data field has changed.
      */
    void dataFieldChangedEvent() override;

    /**
     * Must be called when the actor receives a synchronization event and wants to synchronize it.
     */
    void startSynchronizing();

    /**
     * Must be called to end the actors synchronization.
     */
    void endSynchronizing();

    /** Points to the current iso-surface intersection source. */
    MIsosurfaceIntersectionSource *isosurfaceSource;

    /** Points to the current intersection line data. */
    MIsosurfaceIntersectionLines *intersectionLines;

    /** Layout of generic filter pipeline request. */
    struct Request
    {
        std::shared_ptr<MTrajectoryFilter> filter;
        MTrajectorySelectionSource *inputSelectionSource;
        QString request;
    };

    /** Array of filter requests. */
    QVector<Request> filterRequests;

    /** Request to gather value information per intersection line vertex. */
    QString valueRequest;

    /** Points to the currently used trajectory filter. */
    std::shared_ptr<MTrajectoryFilter> currentTrajectoryFilter;

    /** Points to the by-value trajectory filter. */
    std::shared_ptr<MVariableTrajectoryFilter> varTrajectoryFilter;

    /** Points to the geometric length filter. */
    std::shared_ptr<MGeometricLengthTrajectoryFilter> geomLengthTrajectoryFilter;

    /** Points to the obtain-values trajectory filter / source. */
    std::shared_ptr<MTrajectoryValueSource> valueTrajectorySource;

    /** Points to the current selection of lines after filtering. */
    MTrajectoryEnsembleSelection *lineSelection;
    std::shared_ptr<MTrajectoryFilter> lineSelectionFilter;

    /** Vertex buffer object of intersection lines. */
    GL::MVertexBuffer *linesVertexBuffer;

    /** Layout of each line vertex. */
    struct lineVertex
    {
        float x, y, z, varColor, varThickness;
    };

    /** Raw data of intersection lines. */
    QVector<lineVertex> linesData;

    /** String containing the full line request passed to the input source. */
    QString lineRequest;

    /**
     * Settings to select the variables that should be intersected and filtered
     * by value.
     */
    struct VariableSettings
    {
        explicit VariableSettings(MIsosurfaceIntersectionActor *hostActor);

        MProperty groupProp;

        /** 1st and 2nd variable, value variable. */
        std::array<MNWPActorVarProperty, 2> varsProps;
        std::array<MSciFloatProperty, 2> varsIsovalueProps;
    };

    /** Settings to filter the lines by value and geometric length. */
    struct LineFilterSettings
    {
        explicit LineFilterSettings(MIsosurfaceIntersectionActor *hostActor);

        MProperty groupProp;

        MNWPActorVarProperty filterVarProp;
        MSciFloatProperty valueFilterProp;
        MIntProperty lineLengthFilterProp;
    };

    /** Settings to adjust the appearance of the lines. */
    struct AppearanceSettings
    {
        explicit AppearanceSettings(MIsosurfaceIntersectionActor *hostActor);

        MProperty groupProp;
        MEnumProperty colorModeProp;
        MNWPActorVarProperty colorVariableProp;
        MFloatProperty tubeRadiusProp;
        MColorProperty tubeColorProp;

        /** Pointer to transfer function object and corresponding texture unit. */
        MTransferFunction1DProperty transferFunctionProp;
        int textureUnitTransferFunction;

        MEnumProperty thicknessModeProp;
        MEnumProperty dropModeProp;
    };

    /** Settings to map variable values to tube thickness at each line vertex. */
    struct TubeThicknessSettings
    {
        explicit TubeThicknessSettings(MIsosurfaceIntersectionActor *hostActor);

        MProperty groupProp;

        /** Variable whose values are mapped to thickness at each vertex. */
        MNWPActorVarProperty mappedVariableProp;

        /** Min / max variable value. */
        MFloatProperty minValueProp;
        MFloatProperty maxValueProp;
        QVector2D valueRange;

        /** Min / max thickness value. */
        MFloatProperty minProp;
        MFloatProperty maxProp;
        QVector2D thicknessRange;
    };

    /**
     * Settings to select a set of ensemble members whose variables should
     * be intersected and displayed.
     */
    struct EnsembleSelectionSettings
    {
        explicit EnsembleSelectionSettings(
                MIsosurfaceIntersectionActor *hostActor);

        MBoolProperty enabledProp;
        MStringProperty ensembleMultiMemberProp;
        QSet<unsigned int> selectedEnsembleMembers;
        MButtonProperty ensembleMultiMemberSelectionProp;
    };

    /** Properties for volume bounding box. */
    struct BoundingBoxSettings
    {
        explicit BoundingBoxSettings() = default;

        // coordinates of bounding box
        GLfloat llcrnLon;
        GLfloat llcrnLat;
        GLfloat urcrnLon;
        GLfloat urcrnLat;
        // top and bottom pressure for bounding box
        GLfloat pBot_hPa;
        GLfloat pTop_hPa;

        // properties
        MBoolProperty enabledProp;
    };


    /** Buttons to control manual / auto-computation. */
    MButtonProperty computeClickProp;
    MBoolProperty enableAutoComputationProp;

    /** Pointers to the settings instances. */
    std::shared_ptr<VariableSettings> variableSettings;
    std::shared_ptr<LineFilterSettings> lineFilterSettings;
    std::shared_ptr<AppearanceSettings> appearanceSettings;
    std::shared_ptr<TubeThicknessSettings> tubeThicknessSettings;
    std::shared_ptr<EnsembleSelectionSettings> ensembleSelectionSettings;
    std::shared_ptr<BoundingBoxSettings> boundingBoxSettings;

    /** Suppress any actor updates until all properties are initialized. */
    bool isCalculating;

    /** Pole actor to relate jetstream cores to their actual height in pressure. */
    std::shared_ptr<MMovablePoleActor> poleActor;

    /** GLSL shader objects. */
    std::shared_ptr<GL::MShaderEffect> intersectionLinesShader;
    std::shared_ptr<GL::MShaderEffect> boundingBoxShader;
    std::shared_ptr<GL::MShaderEffect> lineTubeShader;
    int shadowMapTexUnit;
    GL::MVertexBuffer *vboBoundingBox;
    GLuint iboBoundingBox;

    // Whether the actor is currently synchronizing.
    bool isSynchronizing;
};

} // namespace Met3D

#endif // ISOSURFACEINTERSECTIONACTOR_H

