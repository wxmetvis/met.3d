/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017 Michael Kern [+]
**  Copyright 2025 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef ISOSURFACEINTERSECTIONSOURCE_H
#define ISOSURFACEINTERSECTIONSOURCE_H

// standard library imports
#include <array>

// related third party imports

// local application imports
#include "data/weatherpredictiondatasource.h"
#include "data/structuredgrid.h"
#include "data/datarequest.h"
#include "trajectories/source/trajectorydatasource.h"


namespace Met3D
{

class MIsosurfaceIntersectionSource : public MTrajectoryDataSource
{
public:
    typedef QVector<QVector3D> IsoLine;
    typedef QVector<IsoLine> IsoLines;

    explicit MIsosurfaceIntersectionSource();

    void setInputSourceFirstVar(MWeatherPredictionDataSource *s);

    void setInputSourceSecondVar(MWeatherPredictionDataSource *s);


    /**
     * Overloads @c MMemoryManagedDataSource::getData() to cast
     * the returned @c MAbstractDataItem to @c MTrajectories
     * that contains the isosurface intersection Lines.
     * @ref MMemoryManagedDataSource::getData()
     * @ref MAbstractDataItem
     * @ref MTrajectories
     */
    MIsosurfaceIntersectionLines *getData(MDataRequest request) override
    {
        return dynamic_cast<MIsosurfaceIntersectionLines *>
        (MTrajectoryDataSource::getData(request));
    }


    /**
     * Produces the data item corresponding to @p request.
     * @note This function needs to be implemented in a @em thread-safe
     * manner, i.e. all access to shared data/resources within this class needs
     * to be serialized @see
     * https://qt-project.org/doc/qt-4.8/threads-reentrancy.html.
     */
    MTrajectorySelection *produceData(MDataRequest request) override;

    IsoLines *getIntersectionLineForMember(
            MStructuredGrid *gridA, float isovalueA, MStructuredGrid *gridB,
            float isovalueB);

    MTask *createTaskGraph(MDataRequest request) override;


    /**
     * Returns a @c QList<QDateTime> containing the the available forecast
     * initialisation times (base times).
     * @ref QList<QDateTime>
     */
    QList<QDateTime> availableInitTimes() override
    { return {}; }


    /**
     * Valid times correspond to the trajectory start times available for the
     * specified initialisation time @p initTime.
     */
    QList<QDateTime> availableValidTimes(const QDateTime &initTime) override
    {
        return {};
    }


    /**
     * For a given init and valid time, returns the valid (=start) times of
     * those trajectories that overlap with the given valid time.
     */
    QList<QDateTime> validTimeOverlap(const QDateTime &initTime,
                                      const QDateTime &validTime) override
    {
        return {};
    }


    /**
     * Returns the available ensemble members.
     */
    QSet<unsigned int> availableEnsembleMembers() override
    { return {}; }


    /**
     * Auxiliary variables are not yet supported.
     */
    QStringList availableAuxiliaryVariables() override
    { return {}; }


protected:
    const QStringList locallyRequiredKeys() override;


private:
    /**
     * @enum Faces
     * @brief The enum make the face numbering human readable.
     */
    enum Faces : int
    {
        LEFT_FACE = 0,
        RIGHT_FACE = 1,
        FRONT_FACE = 2,
        BACK_FACE = 3,
        BOTTOM_FACE = 4,
        TOP_FACE = 5
    };

    /**
     * @brief Faces table describes the relation between corner points and faces.
     *
     * The following table describes which faces can have an intersection
     * depending on which of the 8 corner points of the cube is under the
     * regarding iso level.
     *
     * If the face number bit is set the face can have an intersection.
     *
     * The numbering of the 6 faces is as the following:
     * 0 : Left
     * 1 : Right
     * 2 : Front
     * 3 : Back
     * 4 : Bottom
     * 5 : Top
     */
    static const char faceTable[256];

    /**
     * @brief Precalculated power of two values.
     */
    static const int pow2[8];


    /**
     * @brief This array stores the Relation between face of a cube and the
     * corner points.
     *
     * This array describes which are the points of each face in the cube.
     *
     * You can access each face point by:
     * {facenumber * 4 + facepoint} with facepoint e [0..3]
     *
     * They are stored in the order:
     * 2------3
     * |      |
     * |      |
     * 0------1
     */
    static const int facePoints[24];

    /**
     * @brief This array stores the relation between the faces and the face
     * edges.
     *
     * In the following we abbreviate the eight corner points of the current cell
     * with p[0..7] and the corresponding values with v[0..7].
     *
     * This array describes which are the edges of each face in the cube.
     *
     * You can access each edge points by:
     * {facenumber * 4 + edgenum * 2 + (0 | 1)}
     */
    static const int edgePoints[48];

    /**
     * This array describes which combination of corner points that are under
     * the iso value results into which edges get cut.
     */
    static const char edgeTable[16];

    /**
     * @brief Calculates 3D interpolation between the two points p1 and p2.
     * The params @p valp1 and @p valp2 are the values of the grid at the points
     * @p p1 and @p p2. The param @p isolevel is the value that represents the
     * value at the returned point.
     */
    static QVector3D VertexInterp(float isolevel, const QVector3D &p1,
                                  const QVector3D &p2, float valp1, float valp2);

    /**
     * @brief Calculates the face of the opposite side.
     */
    static inline int opposite(int face)
    {
        switch (face)
        {
        case Faces::LEFT_FACE:
            return static_cast<int>(Faces::RIGHT_FACE);
        case Faces::RIGHT_FACE:
            return static_cast<int>(Faces::LEFT_FACE);
        case Faces::FRONT_FACE:
            return static_cast<int>(Faces::BACK_FACE);
        case Faces::BACK_FACE:
            return static_cast<int>(Faces::FRONT_FACE);
        case Faces::BOTTOM_FACE:
            return static_cast<int>(Faces::TOP_FACE);
        case Faces::TOP_FACE:
            return static_cast<int>(Faces::BOTTOM_FACE);
        default:
            return -1;
        }
    }

    /**
     * @brief Calculates if two points are close.
     * Is used to decide whether to close a cell or not.
     */
    static bool isClose(const QVector3D &a, const QVector3D &b);

    struct CellInfoInput
    {
        explicit CellInfoInput(QVector<float> &pressureArray)
                : actCellIndex(0),
                  actDataIndex(0),
                  gridA(nullptr),
                  isovalueA(0),
                  gridB(nullptr),
                  isovalueB(0),
                  pressures(pressureArray)
        {
        }


        int actCellIndex;
        int actDataIndex;
        MStructuredGrid *gridA;
        float isovalueA;
        MStructuredGrid *gridB;
        float isovalueB;
        const QVector<float> &pressures;
    };

    /**
     * @brief The struct stores all necessary information of one cell.
     */
    struct CellInformation
    {
        /**
         * @brief The values of the 8 corner points for variable 1.
         */
        float values1[8];

        /**
         * @brief The values of the 8 corner points for variable 2.
         */
        float values2[8];

        /**
         * @brief The positions of the 8 corner points.
         */
        QVector3D cellPoints[8];

        /**
         * @brief The cut faces of variable 1.
         */
        int faces1;

        /**
         * @brief The cut faces of variable 2.
         */
        int faces2;

        /**
         * @brief The index that this cell has in the cache array.
         */
        int index = -1;

        /**
         * Whether the cell was already visited and should no longer be used
         * in the line calculation.
         */
        bool visited = false;

        /**
         * @brief The cell segments found in the cell.
         */
        IsoLines segments;

        /**
         * @brief Describes to which faces one point of the segments belongs to.
         */
        QVector<int> pointFaceRelation;

        /**
         * @brief Describes which points belong to which face.
         */
        QVector<int> facePointRelation;

        int lon;
        int lat;
        int lev;

        CellInformation();

        CellInformation(MStructuredGrid *gridA, MStructuredGrid *gridB,
                        int cellIndex, int dataIndex,
                        const QVector<float> &pressures);

        ~CellInformation();

        /**
         * @brief Computes the cube index of variable 1.
         *
         * The cube index describes which values of the cube are below the
         * iso value. It stores this by switching on/off bytes in an integer
         * value.
         */
        int getCubeIndexes1(float isovalue);

        /**
         * @brief Computes the cube index of variable 2.
         *
         * The cube index describes which values of the cube are below the
         * iso value. It stores this by switching on/off bytes in an integer
         * value.
         */
        int getCubeIndexes2(float isovalue);


        void removeSegment(int i);


        void removeLastSegment()
        { removeSegment(segments.size() - 1); }


    private:

        /**
         * @brief Fills this cells corner points with their correct positions.
         */
        void fillCellPoints(MStructuredGrid *grid,
                            const QVector<float> &pressures);

        /**
         * @brief Fills these cells corner points with the correct values.
         * @param cellValues the cellValues to be filled (values1 or values2).
         * @param grid the grid where the values will be taken from.
         */
        void fillCellValues(float cellValues[], MStructuredGrid *grid) const;
    };

    /**
     * @brief The input sources of the variables.
     */
    std::array<MWeatherPredictionDataSource *, 2> inputSources;

    /**
     * @brief Stores the face that the last added segment ends in. Is used in
     * the tracing method.
     */
    int currentSegmentFace;

    /**
     * @brief Stores the vector direction of the last added segment.
     */
    QVector3D direction;

    /**
     * @brief Calculates the cell segments for one cell.
     */
    void getCellSegments(float isovalueA, float isovalueB, CellInformation *cell);

    /**
     * @brief Get all the cell information for one cell.
     * @param input cell input information.
     * @param info Memory to fill with the cell information.
     */
    void getCellInformation(const CellInfoInput &input, CellInformation *info);

    /**
     * @brief Adds a cell to the current processed line.
     */
    CellInformation *addCellToLastLine(CellInformation *cell, IsoLines *lines);

    /**
     * @brief Prepends a cell to the current processed line.
     */
    CellInformation *prependCellToLastLine(CellInformation *cell, IsoLines *lines);

    /**
     * @brief Traces a line starting at actCell.
     */
    void traceLine(CellInformation &actCell, MStructuredGrid *gridA, IsoLines *lines,
                   std::vector<CellInformation> *cells);

    /**
     * @brief Computes the next cell by using the startingCell and the
     * instance variable currentSegmentFace.
     */
    CellInformation *getNextCell(CellInformation &startingCell,
                                 MStructuredGrid *gridA,
                                 std::vector<CellInformation> *cells);


    static inline QVector3D getDirection(const QVector3D &a, const QVector3D &b)
    {
        return (b - a).normalized();
    }


    inline QVector3D lastPointOfLastLine(IsoLines *lines)
    {
        return lines->last().last();
    }


    inline QVector3D secondLastPointOfLastLine(IsoLines *lines)
    {
        return lines->last().at(lines->last().size() - 2);
    }


    inline QVector3D firstPointOfLastLine(IsoLines *lines)
    {
        return lines->last().first();
    }


    inline QVector3D secondPointOfLastLine(IsoLines *lines)
    {
        return lines->last().at(1);
    }
};


} // namespace Met3D

#endif // ISOSURFACEINTERSECTIONSOURCE_H
