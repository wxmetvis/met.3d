/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "isosurfaceintersectionsource.h"

//#define TIME_MEASUREMENT

// standard library imports

// related third party imports
#include <omp.h>

// local application imports
#include "util/metroutines.h"

#ifdef TIME_MEASUREMENT
#include "util/mstopwatch.h"
#endif


namespace Met3D
{

const char MIsosurfaceIntersectionSource::faceTable[256] = {
        0b000000, 0b010101, 0b010110, 0b010111, 0b011001, 0b011101, 0b011111,
        0b011111, 0b011010, 0b011111, 0b011110, 0b011111, 0b011011, 0b011111,
        0b011111, 0b001111, 0b100101, 0b110101, 0b110111, 0b110111, 0b111101,
        0b111101, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111,
        0b111111, 0b111111, 0b111111, 0b101111, 0b100110, 0b110111, 0b110110,
        0b110111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111110, 0b111111,
        0b111110, 0b111111, 0b111111, 0b111111, 0b111111, 0b101111, 0b100111,
        0b110111, 0b110111, 0b110011, 0b111111, 0b111111, 0b111111, 0b111011,
        0b111111, 0b111111, 0b111111, 0b111011, 0b111111, 0b111111, 0b111111,
        0b101011, 0b101001, 0b111101, 0b111111, 0b111111, 0b111001, 0b111101,
        0b111111, 0b111111, 0b111011, 0b111111, 0b111111, 0b111111, 0b111011,
        0b111111, 0b111111, 0b101111, 0b101101, 0b111101, 0b111111, 0b111111,
        0b111101, 0b111100, 0b111111, 0b111110, 0b111111, 0b111111, 0b111111,
        0b111111, 0b111111, 0b111110, 0b111111, 0b101110, 0b101111, 0b111111,
        0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111,
        0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b101111,
        0b101111, 0b111111, 0b111111, 0b111011, 0b111111, 0b111110, 0b111111,
        0b111010, 0b111111, 0b111111, 0b111111, 0b111011, 0b111111, 0b111110,
        0b111111, 0b101010, 0b101010, 0b111111, 0b111110, 0b111111, 0b111011,
        0b111111, 0b111111, 0b111111, 0b111010, 0b111111, 0b111110, 0b111111,
        0b111011, 0b111111, 0b111111, 0b101111, 0b101111, 0b111111, 0b111111,
        0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111,
        0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b101111, 0b101110,
        0b111111, 0b111110, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111,
        0b111110, 0b111111, 0b111100, 0b111101, 0b111111, 0b111111, 0b111101,
        0b101101, 0b101111, 0b111111, 0b111111, 0b111011, 0b111111, 0b111111,
        0b111111, 0b111011, 0b111111, 0b111111, 0b111101, 0b111001, 0b111111,
        0b111111, 0b111101, 0b101001, 0b101011, 0b111111, 0b111111, 0b111111,
        0b111011, 0b111111, 0b111111, 0b111111, 0b111011, 0b111111, 0b111111,
        0b111111, 0b110011, 0b110111, 0b110111, 0b100111, 0b101111, 0b111111,
        0b111111, 0b111111, 0b111111, 0b111110, 0b111111, 0b111110, 0b111111,
        0b111111, 0b111111, 0b111111, 0b110111, 0b110110, 0b110111, 0b100110,
        0b101111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111, 0b111111,
        0b111111, 0b111111, 0b111111, 0b111101, 0b111101, 0b110111, 0b110111,
        0b110101, 0b100101, 0b001111, 0b011111, 0b011111, 0b011011, 0b011111,
        0b011110, 0b011111, 0b011010, 0b011111, 0b011111, 0b011101, 0b011001,
        0b010111, 0b010110, 0b010101, 0b000000
};

const int MIsosurfaceIntersectionSource::pow2[8] = {1, 2, 4, 8, 16, 32, 64, 128};

const int MIsosurfaceIntersectionSource::facePoints[24] = {
        2, 0, 6, 4,
        1, 3, 5, 7,
        0, 1, 4, 5,
        3, 2, 7, 6,
        2, 3, 0, 1,
        4, 5, 6, 7
};

const int MIsosurfaceIntersectionSource::edgePoints[48] = {
        //left
        2, 0,
        0, 4,
        4, 6,
        6, 2,
        //right
        1, 3,
        3, 7,
        7, 5,
        5, 1,
        //front
        0, 1,
        1, 5,
        5, 4,
        4, 0,
        //back
        3, 2,
        2, 6,
        6, 7,
        7, 3,
        //bottom
        2, 3,
        3, 1,
        1, 0,
        0, 2,
        //top
        4, 5,
        5, 7,
        7, 6,
        6, 4
};

const char MIsosurfaceIntersectionSource::edgeTable[16] = {
        0b0000, 0b1001, 0b0011, 0b1010,
        0b1100, 0b0101, 0b1111, 0b0110,
        0b0110, 0b1111, 0b0101, 0b1100,
        0b1010, 0b0011, 0b1001, 0b0000
};

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MIsosurfaceIntersectionSource::MIsosurfaceIntersectionSource() :
        inputSources({nullptr, nullptr}),
        currentSegmentFace(-1)
{
}


/******************************************************************************
***                             PUBLIC METHODS                              ***
*******************************************************************************/

void MIsosurfaceIntersectionSource::setInputSourceFirstVar(
        MWeatherPredictionDataSource *s)
{
    if (inputSources[0] != nullptr)
    {
        deregisterInputSource(inputSources[0], "ISOX_VAR1");
    }

    inputSources[0] = s;
    registerInputSource(inputSources[0], "ISOX_VAR1");
    enablePassThrough(inputSources[0]);
}


void MIsosurfaceIntersectionSource::setInputSourceSecondVar(
        MWeatherPredictionDataSource *s)
{
    if (inputSources[1] != nullptr)
    {
        deregisterInputSource(inputSources[1], "ISOX_VAR2");
    }

    inputSources[1] = s;
    registerInputSource(inputSources[1], "ISOX_VAR2");
    enablePassThrough(inputSources[1]);
}


MTrajectorySelection *
MIsosurfaceIntersectionSource::produceData(MDataRequest request)
{
    assert(inputSources[0] != nullptr);
    assert(inputSources[1] != nullptr);
#ifdef TIME_MEASUREMENT
    MStopwatch stopwatch;
#endif

    MDataRequestHelper rh(request);

    GLint scount = 0;
    int linesCounter = 0;
    QVector<QVector3D> points;
    auto starts = QVector<GLint>();
    auto sizes = QVector<GLsizei>();
    QVector<GLint> ensembleStartIndices;
    QVector<GLsizei> ensembleLengths;

    QStringList isovalues = rh.value("ISOX_VALUES").split("/");
    QVector<MDataRequestHelper> varRequests = { rh.subRequest("ISOX_VAR1"),
                                                rh.subRequest("ISOX_VAR2") };

    float isovalueA = isovalues[0].toFloat();
    float isovalueB = isovalues[1].toFloat();

    auto intersectionLines = new MIsosurfaceIntersectionLines();
    intersectionLines->lines = new QVector<QVector<QVector3D> *>;

    const uint8_t lowerLineThreshold = 1;

    QStringList members = rh.value("MEMBERS").split("/");
    rh.remove("MEMBERS");
    rh.remove("ENS_OPERATION");

    QStringList bboxList = rh.value("ISOX_BOUNDING_BOX").split("/");
    const float llcrnlon = bboxList[0].toFloat();
    const float llcrnlat = bboxList[1].toFloat();
    const float pBot_hPa = bboxList[2].toFloat();
    const float urcrnlon = bboxList[3].toFloat();
    const float urcrnlat = bboxList[4].toFloat();
    const float pTop_hPa = bboxList[5].toFloat();

    rh.remove("ISOX_BOUNDING_BOX");

    for (const auto &member : members)
    {
        int m = member.toInt();
        rh.insert("MEMBER", QString::number(m));

        // Obtain the scalar field of the first variable.
        MDataRequestHelper rhLocal(varRequests[0]);
        rhLocal.insert("MEMBER", QString::number(m));
        MStructuredGrid *gridA = inputSources[0]->getData(rhLocal.request());

        // Obtain the scalar field of the second variable.
        rhLocal = MDataRequestHelper(varRequests[1]);
        rhLocal.insert("MEMBER", QString::number(m));
        MStructuredGrid *gridB = inputSources[1]->getData(rhLocal.request());

        float dx = std::abs(
                static_cast<float>(gridA->getLons()[0] - gridA->getLons()[1]));

        // Compute the intersection lines of the two grids for each
        // ensemble member.
        IsoLines *pLines = getIntersectionLineForMember(gridA, isovalueA,
                                              gridB, isovalueB);

        // Concat lines to one array.
        int actualLines = 0;
        int startIndex;
        int newIndexCount;

        for (auto &pLine : *pLines)
        {
            // This is the filter to remove small lines < threshold .
            if (pLine.size() <= lowerLineThreshold)
            {
                continue;
            }

            startIndex = scount;
            newIndexCount = 0;

            for (auto point : pLine)
            {
                if (gridA->gridIsCyclicInLongitude())
                {
                    float mixI0 = MMOD(llcrnlon - gridA->lons[0], 360.) / dx;
                    float mixI = MMOD(point.x() - gridA->lons[0], 360.) / dx;

                    float iprime = MMOD(mixI - mixI0, gridA->nlons);
                    point.setX(llcrnlon + iprime * dx);
                }

                points.append(point);

                if (point.x() >= llcrnlon && point.x() <= urcrnlon
                        && point.y() >= llcrnlat && point.y() <= urcrnlat
                        && point.z() >= pTop_hPa && point.z() <= pBot_hPa)
                {
                    newIndexCount++;
                }
                else
                {
                    if (newIndexCount > 0)
                    {
                        starts.append(startIndex);
                        sizes.append(newIndexCount);
                        actualLines++;
                    }

                    startIndex = scount + 1;
                    newIndexCount = 0;
                }

                scount++;
            }

            if (newIndexCount > 0)
            {
                starts.append((GLint) startIndex);
                sizes.append((GLsizei) newIndexCount);
                actualLines++;
            }
        }
        ensembleStartIndices.append(linesCounter);
        ensembleLengths.append(actualLines);
        linesCounter += actualLines;

        delete pLines;
    }


    // Build the instance of an intersections lines class, with all its help
    // arrays.
    intersectionLines->vertices = points;
    QVector<GLboolean> firstVerticesOfLines = QVector<GLboolean>(points.size());
    for (int start : starts)
    {
        firstVerticesOfLines[start] = GL_TRUE;
    }

    delete[] intersectionLines->startIndices;
    intersectionLines->startIndices = nullptr;

    intersectionLines->startIndices = new GLint[starts.length()];
    for (int i = 0; i < starts.length(); i++)
    {
        intersectionLines->startIndices[i] = starts.at(i);
    }

    delete[] intersectionLines->indexCount;
    intersectionLines->indexCount = nullptr;

    intersectionLines->indexCount = new GLint[sizes.length()];
    for (int i = 0; i < sizes.length(); i++)
    {
        intersectionLines->indexCount[i] = sizes.at(i);
    }

    intersectionLines->numTrajectories = linesCounter;
    intersectionLines->ensembleStartIndices = ensembleStartIndices;
    intersectionLines->ensembleIndexCount = ensembleLengths;
    intersectionLines->numEnsembleMembers = members.size();

    // Release variables.
    for (const auto &member : members)
    {
        int m = member.toInt();

        MDataRequestHelper rhLocal(varRequests[0]);
        rhLocal.insert("MEMBER", QString::number(m));
        inputSources[0]->releaseData(rhLocal.request());

        // Obtain the scalar field of the second variable.
        rhLocal = MDataRequestHelper(varRequests[1]);
        rhLocal.insert("MEMBER", QString::number(m));
        inputSources[1]->releaseData(rhLocal.request());
    }

#ifdef TIME_MEASUREMENT
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "intersection total computation time: "
                    << stopwatch.getLastSplitTime(MStopwatch::MILLISECONDS) << " ms");
#endif

    return intersectionLines;
}


MTask *MIsosurfaceIntersectionSource::createTaskGraph(MDataRequest request)
{
    assert(inputSources[0] != nullptr);
    assert(inputSources[1] != nullptr);

    auto task = new MTask(request, this);
    MDataRequestHelper rh(request);

    QVector<MDataRequestHelper> vars = { rh.subRequest("ISOX_VAR1"),
                                         rh.subRequest("ISOX_VAR2") };
    const QString members = rh.value("MEMBERS");

    rh.removeAll(locallyRequiredKeys());

    QStringList memberList = members.split("/");

    for (const auto &member : memberList)
    {
        int m = member.toInt();

        for (int i = 0; i < 2; ++i)
        {
            vars[i].insert("MEMBER", QString::number(m));
            task->addParent(inputSources[i]->getTaskGraph(vars[i].request()));
        }
    }

    return task;
}


/******************************************************************************
***                             PROTECTED METHODS                           ***
*******************************************************************************/

const QStringList MIsosurfaceIntersectionSource::locallyRequiredKeys()
{
    return (QStringList() << "ISOX_VALUES"
                          << "MEMBERS" << "ISOX_BOUNDING_BOX");
}


/******************************************************************************
***                             PRIVATE METHODS                             ***
*******************************************************************************/

MIsosurfaceIntersectionSource::IsoLines *
MIsosurfaceIntersectionSource::getIntersectionLineForMember(
        MStructuredGrid *gridA, float isovalueA, MStructuredGrid *gridB,
        float isovalueB)
{
    if (gridA == nullptr || gridB == nullptr)
    {
        return nullptr;
    }

    auto lines = new IsoLines();

    const int numCells = static_cast<int>((gridA->getNumLons() - 1)
            * (gridA->getNumLats() - 1)
            * (gridA->getNumLevels() - 1));

    // The following line implements the change from merge request
    // !564 to prevent memory being reserved and not freed due to
    // the glibc free implementation.
    // Also see: https://github.com/lattera/glibc/blob/master/malloc/malloc.c#L3093
    // The previous implementation used a QVector and pointers to 
    // the CellInformation structs. Freeing the points did not release
    // memory as their size was below the memory threshold used by free.
    // Instead we now store the CellInformation instances directly in the
    // vector, which allocates a large chunk of contigous memory.
    // Note that std::vector is used as QVector only supports up
    // to 2 GB.
    auto cells = new std::vector<CellInformation>(numCells);

    QVector<float> pressures(static_cast<int>(gridA->getNumValues()), 0);

    int numLatLons = static_cast<int>(gridA->getNumLats()
            * gridA->getNumLons());
    int numLons = static_cast<int>(gridA->getNumLons());

    int numCellLons = numLons - 1;
    int numCellLatLons = static_cast<int>((gridA->getNumLats() - 1)
            * (gridA->getNumLons() - 1));

    // Use the multithreaded CPU to compute the intersection task in parallel.
#pragma omp parallel default(shared)
    {
        // Obtain the pressure values at each grid point.
#pragma omp for schedule(static)
        for (unsigned int c = 0; c < gridA->getNumValues(); ++c)
        {
            const int i = static_cast<int>(c % gridA->nlons);
            const int j = static_cast<int>((c / numLons) % gridA->nlats);
            const int k = static_cast<int>(c / (numLatLons));

            pressures[static_cast<int>(c)] = gridA->getPressure(k, j, i);
        }

        // 1) Compute the intersection line segments at each voxel in parallel.
#pragma omp for schedule(dynamic, 2)
        for (uint k = 0; k < gridA->getNumLevels() - 1; ++k)
        {
            for (uint j = 0; j < gridA->getNumLats() - 1; ++j)
            {
                for (uint i = 0; i < gridA->getNumLons() - 1; ++i)
                {
                    const int dataIndex = static_cast<int>(i + j * numLons
                            + k * numLatLons);

                    const int index = static_cast<int>(i + j * numCellLons
                            + k * numCellLatLons);

                    CellInfoInput cInfoInput(pressures);
                    cInfoInput.actCellIndex = index;
                    cInfoInput.actDataIndex = dataIndex;
                    cInfoInput.gridA = gridA;
                    cInfoInput.isovalueA = isovalueA;
                    cInfoInput.gridB = gridB;
                    cInfoInput.isovalueB = isovalueB;

                    getCellInformation(cInfoInput, &(*cells)[index]);
                }
            }
        }
    }

    // 2) Collect all segments that belong to the same line and combine those
    // as one trajectory.
    for (uint k = 0; k < gridA->getNumLevels() - 1; ++k)
    {
        for (uint j = 0; j < gridA->getNumLats() - 1; ++j)
        {
            for (uint i = 0; i < gridA->getNumLons() - 1; ++i)
            {
                const int index = static_cast<int>(i + j * numCellLons
                        + k * numCellLatLons);

                CellInformation &cInfo = (*cells)[index];
                if (cInfo.visited)
                { continue; }

                cInfo.visited = true;
                traceLine(cInfo, gridA, lines, cells);
            }
        }
    }

    // Free the memory of all temporary, dynamically allocated data.
    delete cells;

    return lines;
}


QVector3D MIsosurfaceIntersectionSource::VertexInterp(
        float isolevel, const QVector3D &p1, const QVector3D &p2,
        float valp1, float valp2)
{
    const double mu = (isolevel - valp1) / (valp2 - valp1);
    return {static_cast<float>(p1.x() + mu * (p2.x() - p1.x())),
            static_cast<float>(p1.y() + mu * (p2.y() - p1.y())),
            static_cast<float>(p1.z() + mu * (p2.z() - p1.z()))};
}


void MIsosurfaceIntersectionSource::traceLine(CellInformation &startingCell,
                                              MStructuredGrid *gridA,
                                              IsoLines *lines,
                                              std::vector<CellInformation> *cells)
{
    // Take last cell line segment and build new line from it.
    lines->append(startingCell.segments.last());

    // Face of first point of starting segment => will be used when tracing
    // backwards.
    int currentSegmentFaceWhenTracingBackwards =
            startingCell.pointFaceRelation
                        .at(startingCell.segments.size() / 2);

    // Face of second point of starting segment.
    currentSegmentFace = startingCell.pointFaceRelation
                                     .at(startingCell.segments.size() / 2 + 1);

    // Current 3D direction of the line.
    direction = getDirection(secondLastPointOfLastLine(lines),
                             lastPointOfLastLine(lines));

    // Remove last segment from cell.
    startingCell.removeLastSegment();

    // Tracing forwards.
    CellInformation *next = addCellToLastLine(getNextCell(startingCell, gridA, cells), lines);

    while (next != nullptr)
    {
        next = addCellToLastLine(getNextCell(*next, gridA, cells), lines);
    }

    // Tracing backwards.
    currentSegmentFace = currentSegmentFaceWhenTracingBackwards;
    direction = getDirection(firstPointOfLastLine(lines), secondPointOfLastLine(lines));
    next = prependCellToLastLine(getNextCell(startingCell, gridA, cells), lines);

    while (next != nullptr)
    {
        next = prependCellToLastLine(getNextCell(*next, gridA, cells), lines);
    }

    // If the line is closable, close it.
    if (lines->last().size() > 3
            && isClose(lastPointOfLastLine(lines), firstPointOfLastLine(lines)))
    {
        lines->last().append(lines->last().at(1));
        lines->last().append(lines->last().at(2));
    }

    // If the line is shorter, then 2 remove it, because otherwise the normals
    // can not be calculated.
    if (lines->last().size() < 2)
    {
        lines->pop_back();
    }
}


MIsosurfaceIntersectionSource::CellInformation *
MIsosurfaceIntersectionSource::getNextCell(CellInformation &actCell,
                                           MStructuredGrid *gridA,
                                           std::vector<CellInformation> *cells)
{
    const int numCellsLat = static_cast<int>(gridA->getNumLats() - 1);
    const int numCellsLon = static_cast<int>(gridA->getNumLons() - 1);
    const int numCellsLev = static_cast<int>(gridA->getNumLevels() - 1);

    const int lon = actCell.lon;
    const int lat = actCell.lat;
    const int lev = actCell.lev;

    if (currentSegmentFace == LEFT_FACE)
    {
        const int curLon = lon - 1;

        if (curLon < 0)
        { return nullptr; }

        const int posIndex = INDEX3zyx(lev, lat, curLon, numCellsLat,
                                       numCellsLon);

        CellInformation &cell = (*cells)[posIndex];
        if (((cell.faces1 & cell.faces2) & pow2[RIGHT_FACE])
                == pow2[RIGHT_FACE])
        {
            cell.visited = true;
            return &cell;
        }
    }
    else if (currentSegmentFace == BACK_FACE)
    {
        const int curLat = lat + 1;

        if (curLat >= numCellsLat)
        { return nullptr; }

        const int posIndex = INDEX3zyx(lev, curLat, lon, numCellsLat,
                                       numCellsLon);

        CellInformation &cell = (*cells)[posIndex];
        if (((cell.faces1 & cell.faces2) & pow2[FRONT_FACE])
                == pow2[FRONT_FACE])
        {
            cell.visited = true;
            return &cell;
        }
    }
    else if (currentSegmentFace == BOTTOM_FACE)
    {
        const int curLev = lev - 1;

        if (curLev < 0)
        { return nullptr; }

        const int posIndex = INDEX3zyx(curLev, lat, lon, numCellsLat,
                                       numCellsLon);

        CellInformation &cell = (*cells)[posIndex];
        if (((cell.faces1 & cell.faces2) & pow2[TOP_FACE]) == pow2[TOP_FACE])
        {
            cell.visited = true;
            return &cell;
        }
    }
    else if (currentSegmentFace == RIGHT_FACE)
    {
        const int curLon = lon + 1;

        if (curLon >= numCellsLon)
        { return nullptr; }

        const int posIndex = INDEX3zyx(lev, lat, curLon, numCellsLat,
                                       numCellsLon);

        CellInformation &cell = (*cells)[posIndex];
        if (((cell.faces1 & cell.faces2) & pow2[LEFT_FACE])
                == pow2[LEFT_FACE])
        {
            cell.visited = true;
            return &cell;
        }
    }
    else if (currentSegmentFace == FRONT_FACE)
    {
        const int curLat = lat - 1;

        if (curLat < 0)
        { return nullptr; }

        const int posIndex = INDEX3zyx(lev, curLat, lon, numCellsLat,
                                       numCellsLon);

        CellInformation &cell = (*cells)[posIndex];
        if (((cell.faces1 & cell.faces2) & pow2[BACK_FACE])
                == pow2[BACK_FACE])
        {
            cell.visited = true;
            return &cell;
        }
    }
    else if (currentSegmentFace == TOP_FACE)
    {
        const int curLev = lev + 1;

        if (curLev >= numCellsLev)
        { return nullptr; }

        const int posIndex = INDEX3zyx(curLev, lat, lon, numCellsLat,
                                       numCellsLon);

        CellInformation &cell = (*cells)[posIndex];
        if (((cell.faces1 & cell.faces2) & pow2[BOTTOM_FACE])
                == pow2[BOTTOM_FACE])
        {
            cell.visited = true;
            return &cell;
        }
    }
    return nullptr;
}


MIsosurfaceIntersectionSource::CellInformation *
MIsosurfaceIntersectionSource::addCellToLastLine(CellInformation *cell, IsoLines *lines)
{
    if (cell == nullptr)
    {
        return nullptr;
    }

    const QVector3D *toAdd = nullptr;
    int indexToAdd = -1;
    int pointToAdd = -1;

    for (int i = 0; i < cell->segments.size(); i++)
    {
        const QVector3D &firstOuterPoint = cell->segments.at(i).at(0);
        const QVector3D &lastOuterPoint = cell->segments.at(i).at(1);

        if (cell->pointFaceRelation[(i * 2) + 1]
                == opposite(currentSegmentFace))
        {
            toAdd = &firstOuterPoint;
            pointToAdd = 0;
            indexToAdd = i;
        }
        else if (cell->pointFaceRelation[(i * 2)]
                == opposite(currentSegmentFace))
        {
            toAdd = &lastOuterPoint;
            pointToAdd = 1;
            indexToAdd = i;
        }
    }

    if (toAdd != nullptr)
    {
        lines->last().append(*toAdd);
        direction = getDirection(lastPointOfLastLine(lines),
                                 secondLastPointOfLastLine(lines));
        currentSegmentFace = cell->pointFaceRelation
                                 .at(indexToAdd / 2 + pointToAdd);
        cell->removeSegment(indexToAdd);

        return cell;
    }
    return nullptr;
}


MIsosurfaceIntersectionSource::CellInformation *
MIsosurfaceIntersectionSource::prependCellToLastLine(
        CellInformation *cell, IsoLines *lines)
{
    if (cell == nullptr)
    {
        return nullptr;
    }
    const QVector3D *toAdd = nullptr;
    int indexToAdd = -1;
    int pointToAdd = -1;
    for (int i = 0; i < cell->segments.size(); i++)
    {
        const QVector3D &firstOuterPoint = cell->segments.at(i).at(0);
        const QVector3D &lastOuterPoint = cell->segments.at(i).at(1);
        if (cell->pointFaceRelation[(i * 2)] == opposite(currentSegmentFace))
        {
            toAdd = &lastOuterPoint;
            indexToAdd = i;
            pointToAdd = 1;
        }
        else if (cell->pointFaceRelation[(i * 2) + 1] ==
                opposite(currentSegmentFace))
        {
            toAdd = &firstOuterPoint;
            indexToAdd = i;
            pointToAdd = 0;
        }
    }

    if (toAdd != nullptr)
    {
        lines->last().prepend(*toAdd);
        direction = getDirection(firstPointOfLastLine(lines),
                                 secondPointOfLastLine(lines));
        currentSegmentFace = cell->pointFaceRelation.at(
                indexToAdd / 2 + pointToAdd);
        cell->removeSegment(indexToAdd);
        return cell;
    }
    return nullptr;
}


void
MIsosurfaceIntersectionSource::getCellInformation(
        const MIsosurfaceIntersectionSource::CellInfoInput &input, CellInformation *info)
{
    *info = CellInformation(input.gridA,
                            input.gridB,
                            input.actCellIndex,
                            input.actDataIndex,
                            input.pressures);

    /*
     * Algorithms for marching cubes is taken from
     * http://paulbourke.net/geometry/polygonise/
     */

    // Get the faces that have a surface line in it.
    info->faces1 = static_cast<unsigned char>(faceTable[info
            ->getCubeIndexes1(input.isovalueA)]);

    // When the cube is entirely out or in surface skip to next cube.
    if (info->faces1 == 0)
    {
        info->visited = true;
    }

    // Same as above but with the second variable.
    info->faces2 = static_cast<unsigned char>(faceTable[info
            ->getCubeIndexes2(input.isovalueB)]);

    if (info->faces2 == 0)
    {
        info->visited = true;
    }

    // In the following the intersection points found in one block
    // are abbreviated by isP.
    getCellSegments(input.isovalueA, input.isovalueB, info);
    if (info->segments.empty())
    {
        info->visited = true;
    }
}


void MIsosurfaceIntersectionSource::getCellSegments(const float isovalueA,
                                                    const float isovalueB,
                                                    CellInformation *cell)
{
    QVector<QVector3D> intersectionPoints;
    QVector<int> pointFaceRelation;
    QVector<int> facePointRelation = QVector<int>({-1, -1, -1, -1, -1, -1});

    // These two variables are used when three intersections where found.
    // The description of this is written under the face loop.
    int faceWithTwoIntersects = -1, faceWithOneIntersect = -1;

    QVector<QVector3D> interp1, interp2;
    interp1.reserve(8);
    interp2.reserve(8);

    // Loop over the faces of the cell.
    for (int i = 0; i < 6; i++)
    {
        // These two variables store the interpolated points for each variable.
        interp1.resize(0); // Trick to shrink QVector without releasing memory.
        interp2.resize(0);

        // When both cubes have an intersecting face?
        if ((cell->faces1 & pow2[i]) == pow2[i]
                && (cell->faces2 & pow2[i]) == pow2[i])
        {

            // Get the points in that face that are smaller than the iso level,
            // then get the cut edges with lineArrangements[].
            const int facePoint0 = facePoints[i * 4];
            const int facePoint1 = facePoints[i * 4 + 1];
            const int facePoint2 = facePoints[i * 4 + 2];
            const int facePoint3 = facePoints[i * 4 + 3];

            int points1 = 0;
            points1 += (cell->values1[facePoint0] < isovalueA) ? 1 : 0;
            points1 += (cell->values1[facePoint1] < isovalueA) ? 2 : 0;
            points1 += (cell->values1[facePoint2] < isovalueA) ? 4 : 0;
            points1 += (cell->values1[facePoint3] < isovalueA) ? 8 : 0;
            int lines1 = static_cast<unsigned char>(edgeTable[points1]);

            int points2 = 0;
            points2 += (cell->values2[facePoint0] < isovalueB) ? 1 : 0;
            points2 += (cell->values2[facePoint1] < isovalueB) ? 2 : 0;
            points2 += (cell->values2[facePoint2] < isovalueB) ? 4 : 0;
            points2 += (cell->values2[facePoint3] < isovalueB) ? 8 : 0;
            int lines2 = static_cast<unsigned char>(edgeTable[points2]);

            // When the lines lay like this in the face, intersection is not
            // possible (with rotated cases).
            //     2---x--3
            //     x      *
            //     |      |
            //     0---*--1
            if ((lines1 == 3 && lines2 == 12) ||
                    (lines1 == 6 && lines2 == 9) ||
                    (lines1 == 9 && lines2 == 6) ||
                    (lines1 == 12 && lines2 == 3))
            {
                continue;
            }

            // Loop over the face edges.
            for (int l = 0; l < 4; l++)
            {
                // Is the edge cutting in the cube of the first variable?
                if ((lines1 & pow2[l]) == pow2[l])
                {
                    const int edgePointPrev = edgePoints[(i * 8 + l * 2)];
                    const int edgePointNext = edgePoints[(i * 8 + l * 2) + 1];

                    // Interpolate the point on the current edge.
                    float val1 = cell->values1[edgePointPrev];
                    float val2 = cell->values1[edgePointNext];
                    const QVector3D &point1 = cell->cellPoints[edgePointPrev];
                    const QVector3D &point2 = cell->cellPoints[edgePointNext];
                    interp1.append(VertexInterp(isovalueA, point1, point2, val1,
                                                val2));
                }
                // Is the edge cutting in the cube of the second variable?
                if ((lines2 & pow2[l]) == pow2[l])
                {
                    const int edgePointPrev = edgePoints[(i * 8 + l * 2)];
                    const int edgePointNext = edgePoints[(i * 8 + l * 2) + 1];

                    // Interpolate the point on the current edge.
                    float val1 = cell->values2[edgePointPrev];
                    float val2 = cell->values2[edgePointNext];
                    const QVector3D &point1 = cell->cellPoints[edgePointPrev];
                    const QVector3D &point2 = cell->cellPoints[edgePointNext];
                    interp2.append(VertexInterp(isovalueB, point1, point2, val1,
                                                val2));
                }
            }

            // When we have a bifurcation this counter is used to detect the
            // face with two points.
            int countOfIntersectsFace = 0;

            // When there are four points in one face two ambiguous cases occur.
            // The decision between these cases is done by the isovalue of one
            // point of the face.
            if (interp1.size() == 4)
            {
                if (cell->values1[1] < isovalueA)
                {
                    QVector3D p1 = interp1[0];
                    for (int j = 0; j < 3; ++j)
                    {
                        interp1[j] = interp1[j + 1];
                    }
                    interp1[3] = p1;
                }
            }
            if (interp2.size() == 4)
            {
                if (cell->values2[1] < isovalueB)
                {
                    QVector3D p1 = interp2[0];
                    for (int j = 0; j < 3; ++j)
                    {
                        interp2[j] = interp2[j + 1];
                    }
                    interp2[3] = p1;
                }
            }

            // Test the lines found for intersection and store the intersection
            // points in the variable intersectionPoints.
            for (int lc1 = 0; lc1 < interp1.size() - 1; lc1 += 2)
            {
                const QVector3D &a1 = interp1.at(lc1);
                const QVector3D &a2 = interp1.at(lc1 + 1);

                for (int lc2 = 0; lc2 < interp2.size() - 1; lc2 += 2)
                {
                    const QVector3D &b1 = interp2.at(lc2);
                    const QVector3D &b2 = interp2.at(lc2 + 1);
                    QVector3D intersection(0, 0, 0);

                    // Due to the fact that intersection in 3D is difficult to
                    // compute, we use the information of the current face to
                    // calculate the intersection in the 2D space.
                    if (i == TOP_FACE || i == BOTTOM_FACE)
                    {
                        QVector2D p(a1.x(), a1.y());
                        QVector2D p2(a2.x(), a2.y());
                        QVector2D q(b1.x(), b1.y());
                        QVector2D q2(b2.x(), b2.y());
                        QVector2D out;
                        bool intersects = getLineSegmentsIntersectionPoint(p,
                                                                           p2,
                                                                           q,
                                                                           q2,
                                                                           out);

                        if (intersects)
                        {
                            intersection = QVector3D(out.x(), out.y(), a1.z());
                        }
                    }
                    else if (i == LEFT_FACE || i == RIGHT_FACE)
                    {
                        QVector2D p(a1.y(), a1.z());
                        QVector2D p2(a2.y(), a2.z());
                        QVector2D q(b1.y(), b1.z());
                        QVector2D q2(b2.y(), b2.z());
                        QVector2D out;
                        bool intersects = getLineSegmentsIntersectionPoint(p,
                                                                           p2,
                                                                           q,
                                                                           q2,
                                                                           out);

                        if (intersects)
                        {
                            intersection = QVector3D(a1.x(), out.x(), out.y());
                        }
                    }
                    else if (i == BACK_FACE || i == FRONT_FACE)
                    {
                        QVector2D p(a1.x(), a1.z());
                        QVector2D p2(a2.x(), a2.z());
                        QVector2D q(b1.x(), b1.z());
                        QVector2D q2(b2.x(), b2.z());
                        QVector2D out;
                        bool intersects = getLineSegmentsIntersectionPoint(p,
                                                                           p2,
                                                                           q,
                                                                           q2,
                                                                           out);

                        if (intersects)
                        {
                            intersection = QVector3D(out.x(), a1.y(), out.y());
                        }
                    }

                    // If an intersection was found.
                    if (intersection != QVector3D(0, 0, 0))
                    {
                        pointFaceRelation.append(i);
                        facePointRelation.replace(i, intersectionPoints.size());
                        intersectionPoints.append(intersection);
                        countOfIntersectsFace++;
                    }
                }
            }

            // The following lines check if the last face or the first face
            // have an intersection this is used for detection of the direction
            // of a bifurcation.
            if (countOfIntersectsFace == 1)
            {
                faceWithOneIntersect++;
            }
            if (countOfIntersectsFace == 2)
            {
                faceWithTwoIntersects++;
            }
        }
    }

    // We decide which points are connected regarding on the point count and if
    // the first or the last face has two intersection points.

    QVector<int> retPointFaceRelation;
    QVector<int> retFacePointRelation;

    // Easy case: two points -> one line.
    if (intersectionPoints.size() == 2)
    {
        cell->segments.append(IsoLine());
        cell->segments[0].reserve(2);

        // If the two points are at the same position, move one a little to not
        // break the calculation of the normals.
        if (intersectionPoints[0] != intersectionPoints[1])
        {
            cell->segments[0].append(intersectionPoints.at(0));
        }
        else
        {
            cell->segments[0].append(intersectionPoints.at(0) + QVector3D(0, 0, 0.1));
        }
        cell->segments[0].append(intersectionPoints.at(1));
        cell->pointFaceRelation = pointFaceRelation;
        cell->facePointRelation = facePointRelation;
    }
        // When three points are found, this cube is a bifurcation.
        // Then we have to decide how to build the lines.
        // What we do is build two lines from the face that
        // has one intersection to the one with two intersections.
    else if (intersectionPoints.size() == 3)
    {
        retFacePointRelation = QVector<int>({-1, -1, -1, -1, -1, -1});
        cell->segments.reserve(2);
        cell->segments.append(IsoLine());
        cell->segments.append(IsoLine());
        cell->segments[0].reserve(2);
        cell->segments[1].reserve(2);
        retPointFaceRelation.reserve(4);

        if (faceWithOneIntersect < faceWithTwoIntersects)
        {
            cell->segments[0].append(intersectionPoints.first());
            retPointFaceRelation.append(pointFaceRelation.first());
            cell->segments[0].append(intersectionPoints.at(1));
            retPointFaceRelation.append(pointFaceRelation.at(1));
            cell->segments[1].append(intersectionPoints.first());
            retPointFaceRelation.append(pointFaceRelation.first());
            cell->segments[1].append(intersectionPoints.at(2));
            retPointFaceRelation.append(pointFaceRelation.at(2));
        }
        else
        {
            cell->segments[0].append(intersectionPoints.last());
            retPointFaceRelation.append(pointFaceRelation.last());
            cell->segments[0].append(intersectionPoints.at(1));
            retPointFaceRelation.append(pointFaceRelation.at(0));
            cell->segments[1].append(intersectionPoints.last());
            retPointFaceRelation.append(pointFaceRelation.last());
            cell->segments[1].append(intersectionPoints.at(1));
            retPointFaceRelation.append(pointFaceRelation.at(1));
        }

        retFacePointRelation.replace(retPointFaceRelation.first(), 0);
        retFacePointRelation.replace(retPointFaceRelation.at(1), 1);
        retFacePointRelation.replace(retPointFaceRelation.at(2), 2);
        retFacePointRelation.replace(retPointFaceRelation.last(), 3);

        cell->pointFaceRelation = retPointFaceRelation;
        cell->facePointRelation = retFacePointRelation;
    }
        // In this case 4 intersection points are found.
        // We resort the points to build the lines.
    else if (intersectionPoints.size() == 4)
    {
        retFacePointRelation = QVector<int>({-1, -1, -1, -1, -1, -1});
        cell->segments.reserve(2);
        cell->segments.append(IsoLine());
        cell->segments.append(IsoLine());
        cell->segments[0].reserve(2);
        cell->segments[1].reserve(2);
        retPointFaceRelation.reserve(4);

        // If we have a next face, we use the current direction of the line to
        // get the fitting line segment, if the line segments then cross,
        // swap the segments end points.
        if (currentSegmentFace != -1)
        {
            float closestDistance = 180;
            int closestIndex = facePointRelation
                    .at(opposite(currentSegmentFace));
            if (closestIndex == -1) closestIndex = 0;
            const QVector3D &closestPoint = intersectionPoints.at(closestIndex);
            int closestIndex2 = -1;

            // Find the segment with the least angle between current direction
            // and it.
            for (int l = 0; l < 4; l++)
            {
                if (closestIndex != l)
                {
                    QVector3D directionTemp = (intersectionPoints.at(l)
                            - closestPoint).normalized();
                    float angle = acos(
                            QVector3D::dotProduct(directionTemp, direction));
                    if (abs(angle) < closestDistance)
                    {
                        closestDistance = abs(angle);
                        closestIndex2 = l;
                    }
                }
            }
            int next1 = -1;
            int next2 = -1;
            for (int l = 0; l < 4; l++)
            {
                if (l != closestIndex && l != closestIndex2)
                {
                    next1 = l;
                }
            }
            for (int l = 0; l < 4; l++)
            {
                if (l != closestIndex && l != closestIndex2 && l != next1)
                {
                    next2 = l;
                }
            }

            QVector2D intersectionPoint;
            // If the line segments then intersects, switch the order.
            if (getLineSegmentsIntersectionPoint(
                    intersectionPoints.at(closestIndex).toVector2D(),
                    intersectionPoints.at(closestIndex2).toVector2D(),
                    intersectionPoints.at(next1).toVector2D(),
                    intersectionPoints.at(next2).toVector2D(),
                    intersectionPoint))
            {
                cell->segments[0].append(intersectionPoints.at(next1));
                retPointFaceRelation.append(pointFaceRelation.at(next1));
                cell->segments[0].append(intersectionPoints.at(next2));
                retPointFaceRelation.append(pointFaceRelation.at(next2));

                cell->segments[1].append(intersectionPoints.at(closestIndex));
                retPointFaceRelation.append(pointFaceRelation.at(closestIndex));
                cell->segments[1].append(intersectionPoints.at(closestIndex2));
                retPointFaceRelation
                        .append(pointFaceRelation.at(closestIndex2));
            }
            else
            {
                cell->segments[0].append(intersectionPoints.at(closestIndex));
                retPointFaceRelation.append(pointFaceRelation.at(closestIndex));
                cell->segments[0].append(intersectionPoints.at(closestIndex2));
                retPointFaceRelation
                        .append(pointFaceRelation.at(closestIndex2));

                cell->segments[1].append(intersectionPoints.at(next1));
                retPointFaceRelation.append(pointFaceRelation.at(next1));
                cell->segments[1].append(intersectionPoints.at(next2));
                retPointFaceRelation.append(pointFaceRelation.at(next2));
            }
        }
        else
        {
            QVector2D intersectionPoint;
            if (getLineSegmentsIntersectionPoint(
                    intersectionPoints.at(0).toVector2D(),
                    intersectionPoints.at(1).toVector2D(),
                    intersectionPoints.at(2).toVector2D(),
                    intersectionPoints.at(3).toVector2D(),
                    intersectionPoint))
            {

                cell->segments[0].append(intersectionPoints.at(1));
                retPointFaceRelation.append(pointFaceRelation.at(1));
                cell->segments[0].append(intersectionPoints.at(2));
                retPointFaceRelation.append(pointFaceRelation.at(2));

                cell->segments[1].append(intersectionPoints.at(0));
                retPointFaceRelation.append(pointFaceRelation.at(0));
                cell->segments[1].append(intersectionPoints.at(3));
                retPointFaceRelation.append(pointFaceRelation.at(3));
            }
            else
            {
                cell->segments[0].append(intersectionPoints.at(0));
                retPointFaceRelation.append(pointFaceRelation.at(0));
                cell->segments[0].append(intersectionPoints.at(1));
                retPointFaceRelation.append(pointFaceRelation.at(1));

                cell->segments[1].append(intersectionPoints.at(2));
                retPointFaceRelation.append(pointFaceRelation.at(2));
                cell->segments[1].append(intersectionPoints.at(3));
                retPointFaceRelation.append(pointFaceRelation.at(3));
            }
        }

        retFacePointRelation.replace(retPointFaceRelation.first(), 0);
        retFacePointRelation.replace(retPointFaceRelation.at(1), 1);
        retFacePointRelation.replace(retPointFaceRelation.at(2), 2);
        retFacePointRelation.replace(retPointFaceRelation.last(), 3);

        cell->pointFaceRelation = retPointFaceRelation;
        cell->facePointRelation = retFacePointRelation;
    }
}


bool
MIsosurfaceIntersectionSource::isClose(const QVector3D &a, const QVector3D &b)
{
    // Arbitrary value chosen by Christoph! Think about a generic value to
    // decide whether two points are closely located.
    if (abs(a.x() - b.x()) <= 5 &&
            abs(a.y() - b.y()) <= 5 &&
            abs(a.z() - b.z()) <= 5)
    {
        return true;
    }
    return false;
}


/*******************************************************************************
***                             CellInformation                              ***
********************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MIsosurfaceIntersectionSource::CellInformation::CellInformation()
        : values1(),
          values2(),
          cellPoints(),
          faces1(0),
          faces2(0),
          index(-1),
          lon(0),
          lat(0),
          lev(0)
{
}


MIsosurfaceIntersectionSource::CellInformation::CellInformation(
        MStructuredGrid *gridA, MStructuredGrid *gridB,
        int cellIndex, int dataIndex, const QVector<float> &pressures)
        : values1(),
          values2(),
          cellPoints(),
          faces1(0),
          faces2(0)
{
    index = cellIndex;
    lon = static_cast<int>(dataIndex % gridA->nlons);
    lat = static_cast<int>((dataIndex / gridA->nlons) % gridA->nlats);
    lev = static_cast<int>(dataIndex / (gridA->nlats * gridA->nlons));

    fillCellPoints(gridA, pressures);
    fillCellValues(values1, gridA);
    fillCellValues(values2, gridB);
}


MIsosurfaceIntersectionSource::CellInformation::~CellInformation()
{
    segments.clear();
}


/******************************************************************************
***                             PUBLIC METHODS                              ***
*******************************************************************************/

int
MIsosurfaceIntersectionSource::CellInformation::getCubeIndexes1(float isovalue)
{
    int cubeindex = 0;
    if (values1[0] < isovalue) cubeindex |= 1;
    if (values1[1] < isovalue) cubeindex |= 2;
    if (values1[2] < isovalue) cubeindex |= 4;
    if (values1[3] < isovalue) cubeindex |= 8;
    if (values1[4] < isovalue) cubeindex |= 16;
    if (values1[5] < isovalue) cubeindex |= 32;
    if (values1[6] < isovalue) cubeindex |= 64;
    if (values1[7] < isovalue) cubeindex |= 128;
    return cubeindex;
}


int MIsosurfaceIntersectionSource::CellInformation::getCubeIndexes2(
        float isovalue)
{
    int cubeindex = 0;
    if (values2[0] < isovalue) cubeindex |= 1;
    if (values2[1] < isovalue) cubeindex |= 2;
    if (values2[2] < isovalue) cubeindex |= 4;
    if (values2[3] < isovalue) cubeindex |= 8;
    if (values2[4] < isovalue) cubeindex |= 16;
    if (values2[5] < isovalue) cubeindex |= 32;
    if (values2[6] < isovalue) cubeindex |= 64;
    if (values2[7] < isovalue) cubeindex |= 128;
    return cubeindex;
}


void MIsosurfaceIntersectionSource::CellInformation::removeSegment(int i)
{
    segments.remove(i);
    pointFaceRelation.remove(i);
    pointFaceRelation.remove(i);
    visited = segments.isEmpty();
}


/******************************************************************************
***                             PRIVATE METHODS                             ***
*******************************************************************************/

void MIsosurfaceIntersectionSource::CellInformation::fillCellPoints(
        MStructuredGrid *grid, const QVector<float> &pressures)
{
    // The following lines store the actual lat, lons and
    // the sizes in lat and long direction
    auto actLon = static_cast<float>(grid->lons[lon]);
    auto actLat = static_cast<float>(grid->lats[lat]);
    auto lonDiv = static_cast<float>(grid->lons[lon + 1] - actLon);
    auto latDiv = static_cast<float>(grid->lats[lat + 1] - actLat);

    // The following lines set the 8 corner points positions.
    int counter = 0;
    const int numLatLons = static_cast<int>(grid->getNumLons()
            * grid->getNumLats());
    for (int k = 0; k <= 1; ++k)
    {
        for (int j = 0; j <= 1; ++j)
        {
            for (int i = 0; i <= 1; ++i)
            {
                int pressureIndex = static_cast<int>(lon + i
                        + (lat + j) * grid->getNumLons()
                        + (lev + k) * numLatLons);

                cellPoints[counter++] = QVector3D(
                        actLon + lonDiv * static_cast<float>(i),
                        actLat + latDiv * static_cast<float>(j),
                        pressures[pressureIndex]);
            }
        }
    }
}


void MIsosurfaceIntersectionSource::CellInformation::fillCellValues(
        float cellValues[], MStructuredGrid *grid) const
{
    // The following lines set the 8 corner points values

    int counter = 0;
    for (int k = 0; k <= 1; ++k)
    {
        for (int j = 0; j <= 1; ++j)
        {
            for (int i = 0; i <= 1; ++i)
            {
                cellValues[counter++] = grid->getValue(lev + k,
                                                       lat + j,
                                                       lon + i);
            }
        }
    }
}


} // namespace Met3D
