/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Susanne Fuchs
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MABSTRACTGRID_H
#define MABSTRACTGRID_H

// related third party imports
#include "GL/glew.h"
#include <QGLWidget>

// standard library imports
#include "data/abstractdataitem.h"
#include "gxfw/gl/texture.h"

namespace Met3D
{

/******************************************************************************
***                              MACROS                                     ***
*******************************************************************************/

// 4D (ensemble + 3D) index macro for x being the fast varying dimension.
#define INDEX4ezyx(e, z, y, x, nz, ny, nx)  ((e)*(nz)*(ny)*(nx) + (z)*(nx)*(ny) + (y)*(nx) + (x))

// The same for pre-multiplied nznynx and nynx (faster for loops).
#define INDEX4ezyx_2(e, z, y, x, nznynx, nynx, nx)  ((e)*(nznynx) + (z)*(nynx) + (y)*(nx) + (x))

// 3D index macro for x being the fast varying dimension.
#define INDEX3zyx(z, y, x, ny, nx)  ((z)*(nx)*(ny) + (y)*(nx) + (x))

// The same with pre-multiplied nynx (faster for loops).
#define INDEX3zyx_2(z, y, x, nynx, nx)  ((z)*(nynx) + (y)*(nx) + (x))

// 2D index macro for x being the fast varying dimension.
#define INDEX2yx(y, x, nx)  ((y)*(nx) + (x))

// 3D + component (i.e. r,g,b)
#define INDEX4zyxc(z, y, x, c, ny, nx, nc)  ((z)*(ny)*(nx)*(nc) + (y)*(nc)*(nx) + (x)*(nc) + (c))


/******************************************************************************
***                               ENUMS                                     ***
*******************************************************************************/

/**
  Vertical level types for that specialisations of @ref MStructuredGrid exist.
  */
enum MVerticalLevelType {
    SINGLE_LEVEL             = 0,
    PRESSURE_LEVELS_3D       = 1,
    HYBRID_SIGMA_PRESSURE_3D = 2,
    POTENTIAL_VORTICITY_2D   = 3,
    LOG_PRESSURE_LEVELS_3D   = 4,
    AUXILIARY_PRESSURE_3D    = 5,   // pressure in auxiliary variable
    MISC_LEVELS_3D           = 6,   // level types that do not fit any other
    RADAR_LEVELS_3D          = 7,
    SIZE_LEVELTYPES
};

/**
  Horizontal grid types for that specialisations of @ref MStructuredGrid exist.
  */
enum MHorizontalGridType
{
    REGULAR_LONLAT_GRID = 0,
    // Rotated north pole coordinates as used by COSMO.
    // (cf. http://www.cosmo-model.org/content/model/documentation/core/cosmoDyncsNumcs.pdf ,
    //  chapter 3.3)
    REGULAR_ROTATED_LONLAT_GRID = 1,
    // Proj-supported projection.
    REGULAR_PROJECTED_GRID = 2,
    // Radar volume scan grid consisting of scans at multiple elevation angles,
    // (cf. e.g. https://www.dwd.de/EN/research/observing_atmosphere/weather_radar/weather_radar_node.html).
    RADAR_GRID = 3,
    // Regular grid with geometric coordinates without geographical reference.
    REGULAR_GEOMETRIC_GRID
};

/******************************************************************************
***                             CLASSES                                     ***
*******************************************************************************/


/**

 */
template <typename T>
class MMemoryManagedArray : public MAbstractDataItem
{
public:
    MMemoryManagedArray(int n)
        : data(new T[n]), nvalues(n)
    { }

    ~MMemoryManagedArray()
    { delete[] data; }

    unsigned int getMemorySize_kb()
    { return (nvalues * sizeof(T)) / 1024.; }

    T *data;
    unsigned int nvalues;
};


/**

 */
class MAbstractGrid
        : public MAbstractDataItem
{
public:
    MAbstractGrid(MVerticalLevelType leveltype, MHorizontalGridType horizontalGridType);

    virtual unsigned int getMemorySize_kb() = 0;

    /** Returns the vertical level type of this grid instance. */
    MVerticalLevelType getLevelType() const { return leveltype; }

    /** Convert a numerical vertical level code to a string. */
    static QString verticalLevelTypeToString(MVerticalLevelType type);

    static MVerticalLevelType verticalLevelTypeFromString(const QString& str);

    static MVerticalLevelType verticalLevelTypeFromConfigString(const QString& str);

    /**
      Minimum value of the data grid.
     */
    virtual float min() = 0;

    /**
      Maximum value of the data grid.
     */
    virtual float max() = 0;

    /** Sets the values of all grid points to zero. */
    virtual void setToZero() = 0;

    /** Sets the values of all grid points to @p val. */
    virtual void setToValue(float val) = 0;

    inline void setHorizontalGridType(MHorizontalGridType horizontalGridType)
    { this->horizontalGridType = horizontalGridType; }

    inline MHorizontalGridType getHorizontalGridType()
    { return horizontalGridType; }

    virtual bool gridIsCyclicInLongitude() = 0;

    virtual float getAbsDistanceLon() const = 0;
    virtual float getAbsDistanceLat() const = 0;

    virtual QVector3D getNorthWestTopDataVolumeCorner_lonlatp() = 0;

    virtual QVector3D getSouthEastBottomDataVolumeCorner_lonlatp() = 0;

    /**
      Sample the data grid at lon, lat and p, using an interpolation method
      dependent on the grid type.

      Uses @ref interpolateGridColumnToPressure(). For derived grid classes
      that are only two-dimensional, the @p p_hPa parameter is ignored.
     */
    virtual float interpolateValue(float lon, float lat, float p_hPa) = 0;

    float interpolateValue(QVector3D vec3_lonLatP);

    /**
      Allows a number of texture parameters to be modified. Call this function
      before you call @ref getTexture().
     */
    void setTextureParameters(GLint  internalFormat = GL_R32F,
                              GLenum format = GL_RED,
                              GLint  wrap = GL_CLAMP_TO_EDGE,
                              GLint  minMaxFilter = GL_LINEAR);

    /**
      Returns the handle to a texture containing the grid data. The handle
      needs to be released with @ref releaseTexture() if not required anylonger
      (not released textures will stay in GPU memory forever). The texture is
      memory managed by @ref MGLResourcesManager.
     */
    virtual GL::MTexture *getTexture(QGLWidget *currentGLContext = nullptr, bool nullTexture = false) = 0;

    /** Release a texture acquired with getTexture(). */
    virtual void releaseTexture() = 0;

    /**
      Texture for empty space skipping: Creates (or returns if already created)
      a 3D grid of fixed size NIxNJxNK (specified in the method, e.g. 32x32x32)
      that subdivides the world space covered by the data volume into regular
      bricks. For each brick, the minimum/maximum values of the data points
      that overlap with the brick are stored in the red/green texture
      components. The texture can be used in the shader to skip regions in
      which an isosurface cannot be located.
    
      References: Krueger and Westermann (2003); Shirley, Fundamentals of
      Computer Graphics, 3rd ed. (2009), Ch. 12.2.3.
     */
    virtual GL::MTexture* getMinMaxAccelTexture3D(QGLWidget *currentGLContext = nullptr) = 0;

    virtual void releaseMinMaxAccelTexture3D() = 0;

protected:
    MHorizontalGridType horizontalGridType;
    MVerticalLevelType leveltype;

    QString minMaxAccelID;
    MMemoryManagedArray<float>* minMaxAccel;

    /** Texture parameters. **/
    GLint  textureInternalFormat;
    GLenum textureFormat;
    GLint  textureWrap;
    GLint  textureMinMaxFilter;

private:
};

} // namespace Met3D

#endif // MABSTRACTGRID_H
