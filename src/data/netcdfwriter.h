/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MNETCDFWRITER_H
#define MNETCDFWRITER_H

// standard library imports

// related third party imports

// local application imports
#include "gxfw/nwpactorvariable.h"

using namespace Met3D;

namespace netCDF
{
/**
 * This class contains static methods to easily write gridded data from the
 * Met.3D context to NetCDF files. Attributes are added following
 * CF conventions.
 */
class MNetCDFWriter
{
public:
    /**
     * Write a list of actor variables to a file. More specifically, the
     * data in the current @c vars[i]->grid fields are written, only at the
     * current time step. The grids should be ready and populated before
     * calling this method.
     * @param vars List of @ref MNWPActorVariable
     * @param filepath The filepath to save the file to.
     * @return True if the file could be successfully written.
     */
    static bool write(const QList<MNWPActorVariable *> &vars,
                      const QString &filepath);

    /**
     * Writes the structured grid to a NetCDF file. Note that meta data owned
     * by the computation source and not by the grid cannot be added this way
     * to the output file.
     * @param grid The pointer to the structured grid to be saved.
     * @param filepath Filepath to use when saving.
     * @return True if the file could be successfully written.
     */
    static bool write(const MStructuredGrid *grid, const QString &filepath);
};
}


#endif //MNETCDFWRITER_H
