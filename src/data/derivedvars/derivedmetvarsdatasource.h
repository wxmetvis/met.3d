/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020 Marcel Meyer [*]
**  Copyright 2024 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef DERIVEDMETVARSDATASOURCE_H
#define DERIVEDMETVARSDATASOURCE_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/processingwpdatasource.h"
#include "data/structuredgrid.h"
#include "data/datarequest.h"
#include "deriveddatafieldprocessor.h"


namespace Met3D
{

/**
  @brief MDerivedMetVarsDataSource derives meteorological variables from basic
  forecast parameters.
  */
class MDerivedMetVarsDataSource
        : public MProcessingWeatherPredictionDataSource
{
public:
    MDerivedMetVarsDataSource();
    ~MDerivedMetVarsDataSource();

    MStructuredGrid* produceData(MDataRequest request);

    MTask* createTaskGraph(MDataRequest request);

    void setInputSource(MWeatherPredictionDataSource* s);

    MWeatherPredictionDataSource* getInputSource() { return inputSource; }

    /**
      Defines a mapping from a CF standard name to an input variable name,
      e.g., "eastward_wind" to "u (an)". This is required to obtain a
      unique mapping of which input variables are used to derive new variables.
      (Otherwise, a case can easily occur in which the input source provides
      two variables with identical standard name. Then, the variable that
      would be used would be random.)

      This function needs to be called for all input variables that shall
      be used.
     */
    void setInputVariableMapping(QString standardName, QString inputVariableName);

    /**
      Registers a data field processor. Needs to be called for each variable
      that shall be derived.

      Ownership of the object that is passed is assumed to be handed over to
      this object; it is deleted when this object is deleted.

      @note Currently, data sources are registered in the constructor.
     */
    void registerDerivedDataFieldProcessor(MDerivedDataFieldProcessor *processor);

    QList<MVerticalLevelType> availableLevelTypes();

    QStringList availableVariables(MVerticalLevelType levelType);

    QSet<unsigned int> availableEnsembleMembers(MVerticalLevelType levelType,
                                                const QString& variableName);

    QList<QDateTime> availableInitTimes(MVerticalLevelType levelType,
                                        const QString& variableName);

    QList<QDateTime> availableValidTimes(MVerticalLevelType levelType,
                                         const QString& variableName,
                                         const QDateTime& initTime);

    /**
     * Methods looking for available variables, ensemble members, init times,
     * and valid times in this data source. The methods @c availableX()
     * above then just return the already cached result. Improves performance
     * as we have a lot of derived variables, which depend themselves on
     * other variables.
     */
    void identifyAvailableVariables(MVerticalLevelType levelType);

    void identifyAvailableEnsembleMembers(MVerticalLevelType levelType,
                                                const QString& variableName);

    void identifyAvailableInitTimes(MVerticalLevelType levelType,
                                        const QString& variableName);

    void identifyAvailableValidTimes(MVerticalLevelType levelType,
                                         const QString& variableName,
                                         const QDateTime& initTime);

    QString variableLongName(MVerticalLevelType levelType,
                             const QString&     variableName);

    QString variableStandardName(MVerticalLevelType levelType,
                             const QString&     variableName);

    QString variableUnits(MVerticalLevelType levelType,
                             const QString&     variableName);

    /**
     * @return A list of all standard names of required input variables in all
     * registered derived field processors. This is the set of variable names
     * a user can map their variables to for derived computations.
     */
    QStringList registeredStandardNames();

protected:
    const QStringList locallyRequiredKeys();

    /**
      Returns the defined input variable name for a given standard name,
      if this has been set with @ref setInputVariable(). Otherwise, returns
      an empty string.
     */
    QString getInputVariableNameFromStdName(QString stdName);

    /**
      Updates a passed standard name, and level type according to an enforced
      level type being encoded in the standard name, and init and valid times
      if a time difference is required.

      Examples:
      Passing a standard name of "air_temperature" and a leveltype of
      "HYBRID_SIGMA_PRESSURE_3D" will not change anything.

      Passing a standard name of "surface_geopotential/SURFACE_2D" and a
      leveltype of "HYBRID_SIGMA_PRESSURE_3D" will result in a standard name
      of "surface_geopotential" and a leveltype of "SURFACE_2D".

      Passing "lwe_thickness_of_precipitation_amount///-21600" will result
      in a standard name of "lwe_thickness_of_precipitation_amount" and a
      valid time shifted by -21600 seconds = 6 hours.
     */
    bool updateStdNameAndArguments(QString *stdName,
                                   MVerticalLevelType *levelType,
                                   QDateTime *initTime=nullptr,
                                   QDateTime *validTime=nullptr);

    /**
     * Checks if the derived variable is available and can be selected by the user.
     * It checks all its required input variables and if they are available.
     * If one of them is unavailable, the derived variable is unavailable.
     * If a required input is another derived variable, it is also checked for availability.
     * @param derivedVarName The name of the derived variable.
     * @param levelType The level type to check.
     * @return @c True if the derived variable is available.
     */
    bool isDerivedVariableAvailable(const QString& derivedVarName, MVerticalLevelType levelType);

    /**
     * Gets the data source of the given variable.
     * @param varStdName The standard name of the variable.
     * @param levelType The level type of the variable.
     * @return Either @c inputSource or @c this, in case of derived variables as an input.
     */
    MWeatherPredictionDataSource *getSourceOfInputVariable(const QString& varStdName, MVerticalLevelType levelType);

private:
    /**
     * Register additional derived processors from the current input data source.
     * This can be used when the target or source names of data fields are not known
     * before we have the input source. For example, this method adds processors to
     * convert height fields to pressure fields for all height fields detected in the
     * input source (by searching for 2-D fields with a unit of metres).
     */
    void registerDerivedProcessorsForInputDataSource();

    /**
     * Clears the caches of available variables, members, init times, and valid times. Is called
     * whenever an input source is changed.
     */
    void clearVariableCaches();

    // Caches for available variables, ensemble members, init times and valid times.
    QMap<MVerticalLevelType, QStringList> availableVariablesCache;
    QMap<MVerticalLevelType, QMap<QString, QSet<unsigned int>>> availableEnsembleMemberCache;
    // Available init times depend on level type and variable, the valid time additionally on the init time.
    QMap<MVerticalLevelType, QMap<QString, QList<QDateTime>>> availableInitTimesCache;
    QMap<MVerticalLevelType, QMap<QString, QMap<QDateTime, QList<QDateTime>>>> availableValidTimesCache;

    MWeatherPredictionDataSource* inputSource;

    QMap<QString, MDerivedDataFieldProcessor*> registeredDerivedDataProcessors;
    QMap<QString, QMap<MVerticalLevelType, QStringList>> requiredInputVariablesList;
    QMap<QString, QString> variableStandardNameToInputNameMapping;

};

} // namespace Met3D

#endif // DERIVEDMETVARSDATASOURCE_H
