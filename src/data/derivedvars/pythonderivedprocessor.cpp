/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pythonderivedprocessor.h"

#include <utility>

// standard library imports

// related third party imports

// local application imports
#include "derivedmetvarsdatasource.h"
#include "system/mpyinterface.h"


namespace Met3D
{
typedef MPythonDerivedProcessor::EndpointType PyEndpoint;

PYBIND11_EMBEDDED_MODULE(derived_var_bindings, module) {

    /** Specify enum endpoints in Python. Need to do this manually as
     * reflection is not possible.  */
    py::enum_<PyEndpoint>(module, "EndpointType")
            .value("METPY_PV_BAROCLINIC", PyEndpoint::METPY_PV_BAROCLINIC)
            .value("METPY_WET_BULB_TEMPERATURE", PyEndpoint::METPY_WET_BULB_TEMPERATURE)
            .value("METPY_WET_BULB_POTENTIAL_TEMPERATURE", PyEndpoint::METPY_WET_BULB_POTENTIAL_TEMPERATURE)
            .value("METPY_STATIC_STABILITY", PyEndpoint::METPY_STATIC_STABILITY)
            .value("METPY_INVALID", PyEndpoint::INVALID)
            .export_values();
};

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPythonDerivedProcessor::MPythonDerivedProcessor(QString standardName, QStringList requiredInputVariables,
                                                 PyEndpoint endpointType)
        : MDerivedDataFieldProcessor(std::move(standardName), std::move(requiredInputVariables))
{
    this->endpoint = endpointType;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MPythonDerivedProcessor::registerProcessorsTo(MDerivedMetVarsDataSource *dataSource)
{
    auto pvBaroclinicProcessor = new MPythonDerivedProcessor("potential_vorticity_baroclinic_metpy",
                                                             {"air_potential_temperature", "air_pressure", "eastward_wind", "northward_wind"},
                                                             PyEndpoint::METPY_PV_BAROCLINIC);

    auto wbtProcessor = new MPythonDerivedProcessor("wet_bulb_temperature_metpy",
                                                    {"air_pressure", "air_temperature", "dew_point_temperature"},
                                                    PyEndpoint::METPY_WET_BULB_TEMPERATURE);

    auto wbptProcessor = new MPythonDerivedProcessor("wet_bulb_potential_temperature_metpy",
                                                     {"air_pressure", "air_temperature", "dew_point_temperature"},
                                                     PyEndpoint::METPY_WET_BULB_POTENTIAL_TEMPERATURE);

    auto staticStabProcessor = new MPythonDerivedProcessor("static_stability_metpy",
                                                           {"air_pressure", "air_temperature"},
                                                           PyEndpoint::METPY_STATIC_STABILITY);

    dataSource->registerDerivedDataFieldProcessor(pvBaroclinicProcessor);
    dataSource->registerDerivedDataFieldProcessor(wbtProcessor);
    dataSource->registerDerivedDataFieldProcessor(wbptProcessor);
    dataSource->registerDerivedDataFieldProcessor(staticStabProcessor);
}


void MPythonDerivedProcessor::compute(QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    MPyInterface::computeVariable(endpoint, inputGrids, derivedGrid);
}

}