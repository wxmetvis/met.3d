/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_DERIVEDHEIGHTTOPRESSUREPROCESSOR_H
#define MET_3D_DERIVEDHEIGHTTOPRESSUREPROCESSOR_H

// standard library imports

// related third party imports

// local application imports
#include "deriveddatafieldprocessor.h"

namespace Met3D
{
class MStructuredGrid;
class MDerivedMetVarsDataSource;

/**
 * Computes the air pressure of a given 2-D height field in the atmosphere using the
 * ICAO standard atmosphere. The processor can be created by passing the name of the
 * available 2-D variable name to the constructor.
 */
class MHeightToPressureFromICAOStdAtmosphereProcessor
        : public MDerivedDataFieldProcessor
{
public:
    MHeightToPressureFromICAOStdAtmosphereProcessor(const QString& heightFieldName);

    void compute(QList<MStructuredGrid*>& inputGrids,
                 MStructuredGrid *derivedGrid) override;

    static void registerProcessorsTo(MDerivedMetVarsDataSource* source);
};


/**
 * Computes the air pressure of a given 2-D height field in the atmosphere using
 * a provided geopotential height field. The processor can be created by passing
 * the name of the available 2-D variable name to the constructor.
 */
class MHeightToPressureFromGeopotentialHeightProcessor
        : public MDerivedDataFieldProcessor
{
public:
    MHeightToPressureFromGeopotentialHeightProcessor(const QString& heightFieldName);

    void compute(QList<MStructuredGrid*>& inputGrids,
                 MStructuredGrid *derivedGrid) override;

    static void registerProcessorsTo(MDerivedMetVarsDataSource* source);
};

} // Met3D


#endif //MET_3D_DERIVEDHEIGHTTOPRESSUREPROCESSOR_H
