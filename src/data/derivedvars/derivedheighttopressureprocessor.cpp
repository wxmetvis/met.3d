/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "derivedheighttopressureprocessor.h"

// standard library imports

// related third party imports

// local application imports
#include "derivedmetvarsdatasource.h"
#include "util/metroutines.h"

namespace Met3D
{

MHeightToPressureFromICAOStdAtmosphereProcessor::MHeightToPressureFromICAOStdAtmosphereProcessor(const QString& heightFieldName)
        : MDerivedDataFieldProcessor("air_pressure_at_" + heightFieldName + "_from_standard_atmosphere",
                                     { heightFieldName })
{
    outputUnit = "Pa";
}


void MHeightToPressureFromICAOStdAtmosphereProcessor::compute(QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = height in metres.
    MStructuredGrid* heightGrid_m = inputGrids.at(0);

    for (unsigned int i = 0; i < derivedGrid->getNumValues(); i++)
    {
        float height_m = heightGrid_m->getValue(i);
        if (height_m == M_MISSING_VALUE)
        {
            derivedGrid->setValue(i, M_MISSING_VALUE);
        }
        else
        {
            float height_Pa = metre2pressure_standardICAO(height_m);
            derivedGrid->setValue(i, height_Pa);
        }
    }
}


void MHeightToPressureFromICAOStdAtmosphereProcessor::registerProcessorsTo(MDerivedMetVarsDataSource *derivedSource)
{
    // Register height to pressure processors. Look for all 2-D variables with
    // a unit in metres in the input data source of the derived data source.
    // For these variables, register a processor that converts that height to Pa.
    auto inputSource = derivedSource->getInputSource();
    if (!inputSource->availableLevelTypes().contains(SINGLE_LEVEL)) return;

    QStringList singleLevelVars = inputSource->availableVariables(SINGLE_LEVEL);

    for (const QString& varName : singleLevelVars)
    {
        QString unit = inputSource->variableUnits(SINGLE_LEVEL, varName);
        if (getSupportedLengthUnits().contains(unit))
        {
            // Is in metres, add processor.
            auto processor = new MHeightToPressureFromICAOStdAtmosphereProcessor(varName);
            derivedSource->registerDerivedDataFieldProcessor(processor);

        }
    }
}


MHeightToPressureFromGeopotentialHeightProcessor::MHeightToPressureFromGeopotentialHeightProcessor
        (const QString& heightFieldName)
        : MDerivedDataFieldProcessor("air_pressure_at_" + heightFieldName + "_from_geopotential_height",
                                     { heightFieldName, "geopotential_height/ANY"})
{
    outputUnit = "Pa";
}


void MHeightToPressureFromGeopotentialHeightProcessor::compute(QList<MStructuredGrid *> &inputGrids,
                                                               MStructuredGrid *derivedGrid)
{
    // input 0 = height in metres.
    auto *heightGrid_m = dynamic_cast<MRegularLonLatGrid *>(inputGrids.at(0));
    MStructuredGrid *geopotentialHeightGrid = inputGrids.at(1);

    auto *derivedPressureGrid = dynamic_cast<MRegularLonLatGrid *>(derivedGrid);
    int numLevels = geopotentialHeightGrid->getNumLevels();

    for (unsigned int j = 0; j < derivedGrid->getNumLats(); j++)
    {
        for (unsigned int i = 0; i < derivedGrid->getNumLons(); i++)
        {
            float height_m = heightGrid_m->getValue(j, i);
            if (height_m == M_MISSING_VALUE)
            {
                derivedPressureGrid->setValue(j, i, M_MISSING_VALUE);
                continue;
            }

            // Compute the geopotential height at this point in the 2-D height field.
            double geopotentialHeightAtPoint =
                    surfaceHeightToSurfaceGeopotential(height_m) / MetConstants::GRAVITY_ACCELERATION;

            // Perform a binary search: Look in the provided 3-D geopotential height
            // field for the values closest to the geopotential height of the provided
            // 2-D field. Do linear interpolation between the closest values. Get the
            // pressure at that specific point.
            int klower = 0;
            int kupper = numLevels - 1;

            // Perform the binary search.
            while ((kupper - klower) > 1)
            {
                // Element midway between klower and kupper.
                int kmid = (kupper + klower) / 2;
                // Look up geopotential height at (i, j, kmid).
                float geopotentialHeight_kmid = geopotentialHeightGrid->getValue(kmid, j, i);

                // Cut interval in half.
                if (geopotentialHeightAtPoint >= geopotentialHeight_kmid)
                {
                    kupper = kmid;
                } else
                {
                    klower = kmid;
                }
            }

            // Get enclosing geopotential heights, and enclosing pressure values.
            float geopotentialUpper = geopotentialHeightGrid->getValue(kupper, j, i);
            float geopotentialLower = geopotentialHeightGrid->getValue(klower, j, i);
            // Weight: Linear interpolation
            float w = (geopotentialHeightAtPoint - geopotentialLower) / (geopotentialUpper - geopotentialLower);

            float pUpper = geopotentialHeightGrid->getPressure(kupper, j, i);
            float pLower = geopotentialHeightGrid->getPressure(klower, j, i);
            float pInterpolated = MMIX(pLower, pUpper, w); // Interpolate pressure values.

            derivedPressureGrid->setValue(j, i, pInterpolated * 100.f); // to Pascal.
        }
    }
}


void MHeightToPressureFromGeopotentialHeightProcessor::registerProcessorsTo(MDerivedMetVarsDataSource *derivedSource)
{
    // Register height to pressure processors. Look for all 2-D variables with
    // a unit in metres in the input data source of the derived data source.
    // For these variables, register a processor that converts that height to Pa.
    auto inputSource = derivedSource->getInputSource();
    if (!inputSource->availableLevelTypes().contains(SINGLE_LEVEL)) return;

    QStringList singleLevelVars = inputSource->availableVariables(SINGLE_LEVEL);

    for (const QString& varName : singleLevelVars)
    {
        QString unit = inputSource->variableUnits(SINGLE_LEVEL, varName);
        if (getSupportedLengthUnits().contains(unit))
        {
            // Is in metres, add processor.
            auto processor = new MHeightToPressureFromGeopotentialHeightProcessor(varName);
            derivedSource->registerDerivedDataFieldProcessor(processor);

        }
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // Met3D