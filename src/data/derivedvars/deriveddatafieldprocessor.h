/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020 Marcel Meyer [*]
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef DERIVEDDATAFIELDPROCESSOR_H
#define DERIVEDDATAFIELDPROCESSOR_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/processingwpdatasource.h"
#include "data/structuredgrid.h"
#include "data/datarequest.h"



namespace Met3D
{

/**
 @brief The MDerivedDataFieldProcessor class is the abstract base class for all
 classes that derive a data field, e.g., wind speed or potential temperature.
 */
class MDerivedDataFieldProcessor
{
public:
    /**
     * Creates a data field processor given a target standard name, the required
     * input variables for the computation, and the target level types this processor
     * can produce data for.
     */
    MDerivedDataFieldProcessor(QString standardName,
                               QStringList requiredInputVariables,
                               QList<MVerticalLevelType> targetLevelTypes);

    /**
     * Creates a data field processor given a target standard name, the required
     * input variables for the computation, and all level types.
     */
    MDerivedDataFieldProcessor(QString standardName,
                               QStringList requiredInputVariables);

    virtual ~MDerivedDataFieldProcessor() {}

    QString getStandardName() const { return standardName; }

    QString getUnitOfResult() const { return outputUnit; }

    QStringList getRequiredInputVariables() const { return requiredInputVariables; }

    QList<MVerticalLevelType> getSupportedLevelTypes() { return supportedLevelTypes; }

    /**
     * This method adds processing information to the given grid. It can be
     * overwritten by the processors to include custom information of the
     * derived computation (e.g., units).
     * Processing informations are written as attributes to exported NetCDF files.
     */
    void addProcessingInformation(MStructuredGrid* grid) const;

    /**
      This method computes the derived data field and needs to be implemented
      in any derived class.

      It is called from @ref MDerivedMetVarsDataSource::produceData() if the
      corresponding variable is requested.

      @p inputGrids contains the required input data fields in the order
      specified in @ref requiredInputVariables. @p derivedGrid contains
      a pre-initialized result grid that already contains lon/lat/lev etc.
      information copied from the first grid in @p inputGrids.
     */
    virtual void compute(QList<MStructuredGrid*>& inputGrids,
                         MStructuredGrid *derivedGrid) = 0;

private:
    QString standardName;
    QStringList requiredInputVariables;
    QList<MVerticalLevelType> supportedLevelTypes;

protected:
    QString outputUnit;
};

}

#endif // DERIVEDDATAFIELDPROCESSOR_H
