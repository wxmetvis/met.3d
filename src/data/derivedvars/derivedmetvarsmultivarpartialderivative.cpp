/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Michael Kern
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "derivedmetvarsmultivarpartialderivative.h"

// standard library imports

// related third party imports

// local application imports
#include "util/metroutines.h"

namespace Met3D
{

MWindVectorPartialDerivativeProcessor::MWindVectorPartialDerivativeProcessor(
        const QString &standardName,
        const QStringList &requiredInputVariables)
        : MDerivedDataFieldProcessor(standardName, requiredInputVariables)
{}


double
MWindVectorPartialDerivativeProcessor::computeVs(MStructuredGrid *gridU,
                                                 MStructuredGrid *gridV, int k,
                                                 int j, int i,
                                                 const QVector2D &s)
{
    QVector2D v(gridU->getValue(k, j, i),
                gridV->getValue(k, j, i));

    return QVector2D::dotProduct(v, s);
}


MWindVectorPartialDerivativeProcessor_dUdV_dN::MWindVectorPartialDerivativeProcessor_dUdV_dN()
        : MWindVectorPartialDerivativeProcessor("dVs_dn", {"eastward_wind",
                                                            "northward_wind"})
{
}


void
MWindVectorPartialDerivativeProcessor_dUdV_dN::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);

    // Compute the distances between two grid points in longitude and latitude
    // direction.
    const auto dx = gridU->getDeltaLonSigned();
    const auto dy = gridU->getDeltaLatSigned();

    // Actual geometric distance between two grids in latitudinal direction
    // ~111km.
    const double deltaLat = MetConstants::deltaLatM;

    double deriv;

#pragma omp parallel for
    for (int k = 0; k < static_cast<int>(derivedGrid->getNumLevels()); ++k)
    {
        for (int j = 0; j < static_cast<int>(derivedGrid->getNumLats()); ++j)
        {
            for (int i = 0;
                 i < static_cast<int>(derivedGrid->getNumLons()); ++i)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // The vector normal to V = s.
                QVector2D n;

                // Create the 2D-normal perpendicular to the tangent.
                n.setX(-V.y());
                n.setY(V.x());
                n.normalize();

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Compute the indices of the surrounding grid points.
                int iPrev = derivedGrid->offsetLonIndex(i, -1);
                int iNext = derivedGrid->offsetLonIndex(i, 1);
                int jPrev = std::max(j - 1, 0);
                int jNext = std::min(j + 1, int(derivedGrid->getNumLats() - 1));

                // Get the velocities of the 4 surrounding points and
                // resolve the velocities into the positive direction of s.
                // Compute V_s at all surrounding points.
                double VsPrevLon = computeVs(gridU, gridV, k, j, iPrev, s);
                double VsNextLon = computeVs(gridU, gridV, k, j, iNext, s);
                double VsPrevLat = computeVs(gridU, gridV, k, jPrev, i, s);
                double VsNextLat = computeVs(gridU, gridV, k, jNext, i, s);

                // Compute the first derivatives d/dx and d/dy.
                double deltaX =
                        dx * static_cast<float>(iNext - iPrev)
                                * cos(gridU->getLats()[j] / 180.0 * M_PI)
                                * deltaLat;
                double deltaY =
                        dy * static_cast<float>(jNext - jPrev) * deltaLat;

                double dVsdx = (VsNextLon - VsPrevLon) / deltaX;
                double dVsdy = (VsNextLat - VsPrevLat) / deltaY;

                // Compute the first derivative in direction n.
                deriv = (n.x() * dVsdx + n.y() * dVsdy);

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}


MWindVectorPartialDerivativeProcessor_dUdV_dZ::MWindVectorPartialDerivativeProcessor_dUdV_dZ()
        : MWindVectorPartialDerivativeProcessor("dVs_dz", {"eastward_wind",
                                                            "northward_wind",
                                                            "geopotential_height"})
{
}


void
MWindVectorPartialDerivativeProcessor_dUdV_dZ::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"
    // input 2 = "geopotential_height"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);
    MStructuredGrid *gridGeoP = inputGrids.at(2);

    double deriv;

#pragma omp parallel for
    for (unsigned int j = 0; j < derivedGrid->getNumLats(); ++j)
    {
        for (unsigned int i = 0; i < derivedGrid->getNumLons(); ++i)
        {
            for (unsigned int k = 0; k < derivedGrid->getNumLevels(); ++k)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                const unsigned int kNext = std::min(k + 1,
                                                    derivedGrid->getNumLevels()
                                                            - 1);
                const unsigned int kPrev = static_cast<unsigned int>(std::max(
                        static_cast<int>(k - 1), 0));

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Get the velocities of the 2 surrounding points along the
                // model levels and resolve the velocities into the
                // positive direction of s.
                double VsPrevLev = computeVs(gridU, gridV,
                                             static_cast<int>(kPrev),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);
                double VsNextLev = computeVs(gridU, gridV,
                                             static_cast<int>(kNext),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);

                // Obtain geopotential height in m for every grid point.
                double geoHeightPrev = gridGeoP->getValue(kPrev, j, i);
                double geoHeightNext = gridGeoP->getValue(kNext, j, i);

                // Compute the height distance between the two grid points.
                auto deltaZ = static_cast<float>(geoHeightNext - geoHeightPrev);

                // Compute the first derivative with central differences.
                deriv = (VsNextLev - VsPrevLev) / (deltaZ);

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}


MWindVectorPartialDerivativeProcessor_dUdV_dP::MWindVectorPartialDerivativeProcessor_dUdV_dP()
        : MWindVectorPartialDerivativeProcessor("dVs_dp", {"eastward_wind",
                                                            "northward_wind"})
{

}


void MWindVectorPartialDerivativeProcessor_dUdV_dP::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);

    double deriv;

#pragma omp parallel for
    for (unsigned int j = 0; j < derivedGrid->getNumLats(); ++j)
    {
        for (unsigned int i = 0; i < derivedGrid->getNumLons(); ++i)
        {
            for (unsigned int k = 0; k < derivedGrid->getNumLevels(); ++k)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                const unsigned int kNext = std::min(k + 1,
                                                    derivedGrid->getNumLevels()
                                                            - 1);
                const unsigned int kPrev = static_cast<unsigned int>(std::max(
                        static_cast<int>(k - 1), 0));

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Get the velocities of the 2 surrounding points along the
                // model levels and resolve the velocities into the positive
                // direction of s.
                double VsPrevLev = computeVs(gridU, gridV,
                                             static_cast<int>(kPrev),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);
                double VsNextLev = computeVs(gridU, gridV,
                                             static_cast<int>(kNext),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);

                // Set deltaZ according to boundary conditions.
                const float dp = gridU->getPressure(kNext, j, i)
                        - gridU->getPressure(kPrev, j, i);

                deriv = (VsNextLev - VsPrevLev) / dp;

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}


MWindVectorPartialDerivativeProcessor_d2Ud2V_dN2::MWindVectorPartialDerivativeProcessor_d2Ud2V_dN2()
        : MWindVectorPartialDerivativeProcessor("d2Vs_dn2", {"eastward_wind",
                                                               "northward_wind"})
{
}


void MWindVectorPartialDerivativeProcessor_d2Ud2V_dN2::compute(
        QList<MStructuredGrid *> &inputGrids,
        MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);

    // Compute the distances between two grid points in longitude and latitude
    // direction.
    const auto dx = gridU->getDeltaLonSigned();
    const auto dy = gridU->getDeltaLatSigned();

    // Actual geometric distance between two grids in latitudinal direction
    // ~111km.
    const double deltaLat = MetConstants::deltaLatM;

    double deriv;

#pragma omp parallel for
    for (int k = 0; k < static_cast<int>(derivedGrid->getNumLevels()); ++k)
    {
        for (int j = 0; j < static_cast<int>(derivedGrid->getNumLats()); ++j)
        {
            for (int i = 0;
                 i < static_cast<int>(derivedGrid->getNumLons()); ++i)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // The current velocity.
                float Vs = V.length();

                // The vector normal to V = s.
                QVector2D n;

                // Create the 2D-normal perpendicular to the tangent.
                n.setX(-V.y());
                n.setY(V.x());
                n.normalize();

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Compute the indices of the surrounding grid points.
                int iPrev = derivedGrid->offsetLonIndex(i, -1);
                int iNext = derivedGrid->offsetLonIndex(i, 1);
                int jPrev = std::max(j - 1, 0);
                int jNext = std::min(j + 1, int(derivedGrid->getNumLats() - 1));

                // Get the velocities of the 4 surrounding points and
                // resolve the velocities into the positive direction of s.
                // Compute V_s at all surrounding points.
                double VsPrevLon = computeVs(gridU, gridV, k, j, iPrev, s);
                double VsNextLon = computeVs(gridU, gridV, k, j, iNext, s);
                double VsPrevLat = computeVs(gridU, gridV, k, jPrev, i, s);
                double VsNextLat = computeVs(gridU, gridV, k, jNext, i, s);

                // Compute the first derivatives d/dx and d/dy.
                double deltaX =
                        dx * static_cast<float>(iNext - iPrev)
                                * cos(gridU->getLats()[j] / 180.0 * M_PI)
                                * deltaLat;
                double deltaY =
                        dy * static_cast<float>(jNext - jPrev) * deltaLat;

                // Get the velocities of the 4 surrounding points along
                // the diagonal (X-neighbours) and resolve the
                // velocities into the positive direction of s.
                double VsINJN = computeVs(gridU, gridV, k, jNext, iNext, s);
                double VsIPJN = computeVs(gridU, gridV, k, jNext, iPrev, s);
                double VsINJP = computeVs(gridU, gridV, k, jPrev, iNext, s);
                double VsIPJP = computeVs(gridU, gridV, k, jPrev, iPrev, s);

                double dVs2dx2 = (VsNextLon - 2 * Vs + VsPrevLon)
                        / (deltaX * deltaX / 4);
                double dVs2dy2 = (VsNextLat - 2 * Vs + VsPrevLat)
                        / (deltaY * deltaY / 4);
                double dVs2dxdy =
                        (VsINJN - VsIPJN - VsINJP + VsIPJP) / (deltaX * deltaY);

                // Compute the second derivative in direction n.
                deriv = n.x() * n.x() * dVs2dx2 + 2 * n.x() * n.y() * dVs2dxdy
                        + n.y() * n.y() * dVs2dy2;

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}


MWindVectorPartialDerivativeProcessor_d2Ud2V_dZ2::MWindVectorPartialDerivativeProcessor_d2Ud2V_dZ2()
        : MWindVectorPartialDerivativeProcessor("d2Vs_dz2",
                                                {"eastward_wind",
                                                 "northward_wind",
                                                 "geopotential_height"})
{
}


void MWindVectorPartialDerivativeProcessor_d2Ud2V_dZ2::compute(
        QList<MStructuredGrid *> &inputGrids,
        MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"
    // input 2 = "geopotential_height"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);
    MStructuredGrid *gridGeoP = inputGrids.at(2);

    double deriv;

#pragma omp parallel for
    for (unsigned int j = 0; j < derivedGrid->getNumLats(); ++j)
    {
        for (unsigned int i = 0; i < derivedGrid->getNumLons(); ++i)
        {
            for (unsigned int k = 0; k < derivedGrid->getNumLevels(); ++k)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                const unsigned int kNext = std::min(k + 1,
                                                    derivedGrid->getNumLevels()
                                                            - 1);
                const unsigned int kPrev = static_cast<unsigned int>(std::max(
                        static_cast<int>(k - 1), 0));

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // Compute the current velocity.
                float Vs = V.length();

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Get the velocities of the 2 surrounding points along the
                // model levels and resolve the velocities into the
                // positive direction of s.
                double VsPrevLev = computeVs(gridU, gridV,
                                             static_cast<int>(kPrev),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);
                double VsNextLev = computeVs(gridU, gridV,
                                             static_cast<int>(kNext),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);

                // Obtain geopotential height in m for every grid point.
                double geoHeightPrev = gridGeoP->getValue(kPrev, j, i);
                double geoHeightNext = gridGeoP->getValue(kNext, j, i);

                // Compute the height distance between the two grid points.
                auto deltaZ = static_cast<float>(geoHeightNext - geoHeightPrev);

                // Compute the second derivative.
                deriv = (VsNextLev - 2 * Vs + VsPrevLev)
                        / (deltaZ * deltaZ / 4);

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}


MWindVectorPartialDerivativeProcessor_d2Ud2V_dP2::MWindVectorPartialDerivativeProcessor_d2Ud2V_dP2()
        : MWindVectorPartialDerivativeProcessor("d2Vs_dp2",
                                                {"eastward_wind",
                                                 "northward_wind"})
{

}


void MWindVectorPartialDerivativeProcessor_d2Ud2V_dP2::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);

    double deriv;

#pragma omp parallel for
    for (unsigned int j = 0; j < derivedGrid->getNumLats(); ++j)
    {
        for (unsigned int i = 0; i < derivedGrid->getNumLons(); ++i)
        {
            for (unsigned int k = 0; k < derivedGrid->getNumLevels(); ++k)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                const unsigned int kNext = std::min(k + 1,
                                                    derivedGrid->getNumLevels()
                                                            - 1);
                const unsigned int kPrev = static_cast<unsigned int>(std::max(
                        static_cast<int>(k - 1), 0));

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // Compute the current velocity.
                double Vs = V.length();

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Get the velocities of the 2 surrounding points along the
                // model levels and resolve the velocities into the positive
                // direction of s.
                double VsPrevLev = computeVs(gridU, gridV,
                                             static_cast<int>(kPrev),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);
                double VsNextLev = computeVs(gridU, gridV,
                                             static_cast<int>(kNext),
                                             static_cast<int>(j),
                                             static_cast<int>(i), s);

                // Set deltaZ according to boundary conditions.
                const float dp = gridU->getPressure(kNext, j, i)
                        - gridU->getPressure(kPrev, j, i);

                // Assume that deltaPressure = h / 2
                deriv = (VsNextLev - 2 * Vs + VsPrevLev) / (dp * dp / 4);

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}


MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdZ::MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdZ()
        : MWindVectorPartialDerivativeProcessor("d2Vs_dndz",
                                                {"eastward_wind",
                                                 "northward_wind",
                                                 "geopotential_height"})
{
}


void MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdZ::compute(
        QList<MStructuredGrid *> &inputGrids,
        MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"
    // input 2 = "geopotential_height"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);
    MStructuredGrid *gridGeoP = inputGrids.at(2);

    // Compute the distances between two grid points in longitude and latitude
    // direction.
    const auto dx = gridU->getDeltaLonSigned();
    const auto dy = gridU->getDeltaLatSigned();

    const double deltaLat = MetConstants::deltaLatM;

    double deriv;

#pragma omp parallel for
    for (int k = 0; k < static_cast<int>(derivedGrid->getNumLevels()); ++k)
    {
        for (int j = 0; j < static_cast<int>(derivedGrid->getNumLats()); ++j)
        {
            for (int i = 0;
                 i < static_cast<int>(derivedGrid->getNumLons()); ++i)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // The vector normal to V = s.
                QVector2D n;

                // Create the 2D-normal perpendicular to the tangent.
                n.setX(-V.y());
                n.setY(V.x());
                n.normalize();

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Compute the indices of the surrounding grid points.
                int iPrev = derivedGrid->offsetLonIndex(i, -1);
                int iNext = derivedGrid->offsetLonIndex(i, 1);
                int jPrev = std::max(j - 1, 0);
                int jNext = std::min(j + 1, int(derivedGrid->getNumLats() - 1));

                // Compute the first derivatives d/dx and d/dy.
                double deltaX =
                        dx * static_cast<float>(iNext - iPrev)
                                * cos(gridU->getLats()[j] / 180.0 * M_PI)
                                * deltaLat;
                double deltaY =
                        dy * static_cast<float>(jNext - jPrev) * deltaLat;

                int kNext = std::min(k + 1, int(gridU->getNumLevels() - 1));
                int kPrev = std::max(k - 1, 0);

                const double geoHeightPrev = gridGeoP->getValue(kPrev, j, i);
                const double geoHeightNext = gridGeoP->getValue(kNext, j, i);

                double dp = geoHeightNext - geoHeightPrev;

                // Get the velocities of the 8 surrounding points along
                // the diagonal (X-neighbours) for the adjacent model
                // level and resolve the velocities into the positive
                // direction of s.
                double vsKPJIP = computeVs(gridU, gridV, kPrev, j, iPrev, s);
                double vsKNJIP = computeVs(gridU, gridV, kNext, j, iPrev, s);

                double vsKPJIN = computeVs(gridU, gridV, kPrev, j, iNext, s);
                double vsKNJIN = computeVs(gridU, gridV, kNext, j, iNext, s);

                double vsKPJPI = computeVs(gridU, gridV, kPrev, jPrev, i, s);
                double vsKNJPI = computeVs(gridU, gridV, kNext, jPrev, i, s);

                double vsKPJNI = computeVs(gridU, gridV, kPrev, jNext, i, s);
                double vsKNJNI = computeVs(gridU, gridV, kNext, jNext, i, s);

                // Compute the mixed partial derivative.
                double d2Vsdpdx =
                        (vsKNJIN - vsKPJIN - vsKNJIP + vsKPJIP) / (deltaX * dp);
                double d2Vsdpdy =
                        (vsKNJNI - vsKPJNI - vsKNJPI + vsKPJPI) / (deltaY * dp);

                deriv = n.x() * d2Vsdpdx + n.y() * d2Vsdpdy;

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}


MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdP::MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdP()
        : MWindVectorPartialDerivativeProcessor("d2Vs_dndp",
                                                {"eastward_wind",
                                                 "northward_wind"})
{

}


void MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdP::compute(
        QList<MStructuredGrid *> &inputGrids, MStructuredGrid *derivedGrid)
{
    // input 0 = "eastward_wind"
    // input 1 = "northward_wind"

    MStructuredGrid *gridU = inputGrids.at(0);
    MStructuredGrid *gridV = inputGrids.at(1);

    // Compute the distances between two grid points in longitude and latitude
    // direction.
    const auto dx = gridU->getDeltaLonSigned();
    const auto dy = gridU->getDeltaLatSigned();

    const double deltaLat = MetConstants::deltaLatM;

    // Contains the value of the current derivative.
    double deriv;

#pragma omp parallel for
    for (int k = 0; k < static_cast<int>(derivedGrid->getNumLevels()); ++k)
    {
        for (int j = 0; j < static_cast<int>(derivedGrid->getNumLats()); ++j)
        {
            for (int i = 0;
                 i < static_cast<int>(derivedGrid->getNumLons()); ++i)
            {
                float uValue = gridU->getValue(k, j, i);
                float vValue = gridV->getValue(k, j, i);

                // Forward missing value if source grid has a missing value.
                if (IS_MISSING(uValue) || IS_MISSING(vValue))
                {
                    derivedGrid->setValue(k, j, i, M_MISSING_VALUE);
                    continue;
                }

                // Current wind vector at sample grid point (k, j, i).
                QVector2D V;

                // Obtain the tangent at the grid point.
                V.setX(uValue);
                V.setY(vValue);

                // The vector normal to V = s.
                QVector2D n;

                // Create the 2D-normal perpendicular to the tangent.
                n.setX(-V.y());
                n.setY(V.x());
                n.normalize();

                // Set vector s parallel to the tangent (V).
                QVector2D s = V;
                s.normalize();

                // Compute the indices of the surrounding grid points.
                int iPrev = derivedGrid->offsetLonIndex(i, -1);
                int iNext = derivedGrid->offsetLonIndex(i, 1);
                int jPrev = std::max(j - 1, 0);
                int jNext = std::min(j + 1, int(derivedGrid->getNumLats() - 1));

                // Compute the first derivatives d/dx and d/dy.
                double deltaX =
                        dx * static_cast<float>(iNext - iPrev)
                                * cos(gridU->getLats()[j] / 180.0 * M_PI)
                                * deltaLat;
                double deltaY =
                        dy * static_cast<float>(jNext - jPrev) * deltaLat;

                // Mixed-partial derivative.
                int kNext = std::min(k + 1,
                                     int(gridU->getNumLevels() - 1));
                int kPrev = std::max(k - 1, 0);

                double dp = gridU->getPressure(kNext, j, i)
                        - gridU->getPressure(kPrev, j, i);

                // Get the velocities of the 8 surrounding points along
                // the diagonal (X-neighbours) for the adjacent model
                // level and resolve the velocities into the positive
                // direction of s.
                double vsKPJIP =
                        computeVs(gridU, gridV, kPrev, j, iPrev, s);
                double vsKNJIP =
                        computeVs(gridU, gridV, kNext, j, iPrev, s);

                double vsKPJIN =
                        computeVs(gridU, gridV, kPrev, j, iNext, s);
                double vsKNJIN =
                        computeVs(gridU, gridV, kNext, j, iNext, s);

                double vsKPJPI =
                        computeVs(gridU, gridV, kPrev, jPrev, i, s);
                double vsKNJPI =
                        computeVs(gridU, gridV, kNext, jPrev, i, s);

                double vsKPJNI =
                        computeVs(gridU, gridV, kPrev, jNext, i, s);
                double vsKNJNI =
                        computeVs(gridU, gridV, kNext, jNext, i, s);

                // Compute the mixed partial derivative.
                double d2Vsdpdx =
                        (vsKNJIN - vsKPJIN - vsKNJIP + vsKPJIP)
                                / (deltaX * dp);
                double d2Vsdpdy =
                        (vsKNJNI - vsKPJNI - vsKNJPI + vsKPJPI)
                                / (deltaY * dp);

                deriv = n.x() * d2Vsdpdx + n.y() * d2Vsdpdy;

                // If result is NaN, set it to our missing value.
                if (qIsNaN(deriv))
                {
                    deriv = M_MISSING_VALUE;
                }

                derivedGrid->setValue(k, j, i, static_cast<float>(deriv));
            }
        }
    }
}
} // Met3D