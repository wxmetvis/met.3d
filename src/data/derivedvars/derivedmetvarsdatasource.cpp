/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020 Marcel Meyer [*]
**  Copyright 2024 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "derivedmetvarsdatasource.h"

// standard library imports
#include <iostream>
#include "assert.h"

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/metroutines.h"
#include "util/metroutines_experimental.h"
#include "derivedmetvars_standard.h"
#include "derivedmetvars_mcaoindicator.h"
#include "derivedmetvarsmultivarpartialderivative.h"
#include "derivedheighttopressureprocessor.h"
#include "pythonderivedprocessor.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MDerivedMetVarsDataSource::MDerivedMetVarsDataSource()
    : MProcessingWeatherPredictionDataSource(),
      inputSource(nullptr)
{
    // Register data field processors.
    // ===============================
//!Todo (mr, 11Mar2018). This could possibly be moved out of this constructor
//! and be done outside of the class as a configuration/plug-in mechanism.

    registerDerivedDataFieldProcessor(new MHorizontalWindSpeedProcessor());
    registerDerivedDataFieldProcessor(new MMagnitudeOfAirVelocityProcessor());
    registerDerivedDataFieldProcessor(new MPotentialTemperatureProcessor());
    registerDerivedDataFieldProcessor(new MEquivalentPotentialTemperatureProcessor());
    registerDerivedDataFieldProcessor(new MRelativeHumdityProcessor());
    registerDerivedDataFieldProcessor(new MGeopotentialHeightProcessor());
    registerDerivedDataFieldProcessor(new MGeopotentialHeightFromGeopotentialProcessor());
    registerDerivedDataFieldProcessor(new MDewPointTemperatureProcessor());
    registerDerivedDataFieldProcessor(new MPressureProcessor());
    registerDerivedDataFieldProcessor(new MOmegaHydrostaticProcessor());
    registerDerivedDataFieldProcessor(new MSurfaceGeopotentialFromGeometricHeightProcessor());
    registerDerivedDataFieldProcessor(new MPotentialVorticityProcessor_LAGRANTOcalvar);

    registerDerivedDataFieldProcessor(
                new MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor(
                    "HYBRID_SIGMA_PRESSURE_3D"));
//!TODO (mr, 13Mar2018) -- there needs to be a more elegant way to handle
//! cases in which the returned data field is of different type than all
//! required input fields. The current solution appends the input level type
//! to the variable name, which is not very elegant...
    registerDerivedDataFieldProcessor(
                new MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor(
                    "PRESSURE_LEVELS_3D"));
    registerDerivedDataFieldProcessor(
                new MMagnitudeOfVerticallyIntegratedMoistureFluxProcessor(
                    "AUXILIARY_PRESSURE_3D"));

    registerDerivedDataFieldProcessor(new MMagnitudeOfHorizontalMoistureFluxProcessor());

    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(1));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(3));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(6));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(12));
    registerDerivedDataFieldProcessor(new MTHourlyTotalPrecipitationProcessor(24));

    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_dUdV_dN());
    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_dUdV_dZ());
    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_dUdV_dP());
    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_d2Ud2V_dN2());
    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_d2Ud2V_dZ2());
    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_d2Ud2V_dP2());
    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdZ());
    registerDerivedDataFieldProcessor(new MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdP());

    registerDerivedDataFieldProcessor(new MHourlyMeanOfSurfaceLatentHeatFluxProcessor_ERA5());

    // Register Python sources.
    MPythonDerivedProcessor::registerProcessorsTo(this);

    // Register experimental data field processors.
    // ============================================

#ifdef ENABLE_EXPERIMENTAL_DERIVEDVARS
    registerDerivedDataFieldProcessor(new MMCAOIndexProcessor_Papritz2015());
     registerDerivedDataFieldProcessor(new MMCAOIndexProcessor_Papritz2015_nonMasked());
    registerDerivedDataFieldProcessor(new MMCAOIndexProcessor_Kolstad2008());
    // MMCAOIndexProcessor_BracegirdleGray2008 needs to be fixed -- do not use!
    // registerDerivedDataFieldProcessor(new MMCAOIndexProcessor_BracegirdleGray2008());
    registerDerivedDataFieldProcessor(new MMCAOIndexProcessor_Michel2018());
    registerDerivedDataFieldProcessor(
                new MMCAOIndex2DProcessor_YuliaP(
                    "HYBRID_SIGMA_PRESSURE_3D"));
    registerDerivedDataFieldProcessor(
                new MMCAOIndex2DProcessor_YuliaP(
                    "PRESSURE_LEVELS_3D"));
    registerDerivedDataFieldProcessor(
                new MMCAOIndex2DProcessor_YuliaP(
                    "AUXILIARY_PRESSURE_3D"));
#endif

    // ... <add registration commands here> ...
}


MDerivedMetVarsDataSource::~MDerivedMetVarsDataSource()
{
    for (MDerivedDataFieldProcessor* p : registeredDerivedDataProcessors)
    {
        delete p;
    }
}




/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MDerivedMetVarsDataSource::setInputSource(MWeatherPredictionDataSource* s)
{
    inputSource = s;
    registerInputSource(inputSource);

    registerDerivedProcessorsForInputDataSource();
    clearVariableCaches();
}


void MDerivedMetVarsDataSource::clearVariableCaches()
{
    availableVariablesCache.clear();
    availableEnsembleMemberCache.clear();
    availableInitTimesCache.clear();
    availableValidTimesCache.clear();
}


void MDerivedMetVarsDataSource::setInputVariableMapping(
        QString standardName, QString inputVariableName)
{
    variableStandardNameToInputNameMapping[standardName] = inputVariableName;

    // If there is already a processor with the standardName as a target output name,
    // we have a conflict: We can get the output variable by either forwarding the mapping
    // specified by the user, or by using the processor. Then, we prefer just forwarding
    // the mapping as requested by the user.
    if (registeredDerivedDataProcessors.contains(standardName))
    {
        // Remove the processor.
        registeredDerivedDataProcessors.remove(standardName);
        requiredInputVariablesList.remove(standardName);
    }
}


void MDerivedMetVarsDataSource::registerDerivedDataFieldProcessor(
        MDerivedDataFieldProcessor *processor)
{
    if (processor != nullptr)
    {
        // Check if the output variable name of the processor to register is already
        // set in a direct mapping. Then, prefer the direct mapping over the computation,
        // and do not register this processor.
        if (!variableStandardNameToInputNameMapping.contains(processor->getStandardName()))
        {
            registeredDerivedDataProcessors.insert(
                    processor->getStandardName(), processor);

            // Set input variables for each level type separately.
            for (MVerticalLevelType lType: processor->getSupportedLevelTypes())
            {
                requiredInputVariablesList[processor->getStandardName()][lType] =
                        processor->getRequiredInputVariables();
            }
        }
    }
}


MStructuredGrid* MDerivedMetVarsDataSource::produceData(MDataRequest request)
{
#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif

    assert(inputSource != nullptr);

    // Parse request.
    MDataRequestHelper rh(request);
    QString derivedVarName = rh.value("VARIABLE");
    MVerticalLevelType levelType = MVerticalLevelType(rh.intValue("LEVELTYPE"));
    QDateTime initTime  = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    rh.removeAll(locallyRequiredKeys()); // removes "VARIABLE"

    // Get input fields.
    QList<MStructuredGrid*> inputGrids;
    for (QString requiredVarStdName : requiredInputVariablesList[derivedVarName][levelType])
    {
        // Handle enforced level types (cf. updateStdNameAndLevelType()).
        MVerticalLevelType ltype = levelType;

        bool argsChanged = updateStdNameAndArguments(
                    &requiredVarStdName, &ltype, &initTime, &validTime);

        QString inputVarName = getInputVariableNameFromStdName(
                    requiredVarStdName);
        rh.insert("VARIABLE", inputVarName);
        rh.insert("LEVELTYPE", ltype); // update if changed
        rh.insert("INIT_TIME", initTime); // update if changed
        rh.insert("VALID_TIME", validTime); // update if changed

        MWeatherPredictionDataSource *source = getSourceOfInputVariable(requiredVarStdName, ltype);

        // Check if if times have been changed the requested data fields are
        // available.
        bool everythingAvailable = true;
        if ( argsChanged && !source->availableInitTimes(
                 ltype, inputVarName).contains(initTime) )
        {
            everythingAvailable = false;
        }
        else if ( argsChanged && !source->availableValidTimes(
                      ltype, inputVarName, initTime).contains(validTime) )
        {
            everythingAvailable = false;
        }

        if (everythingAvailable)
        {
            inputGrids << source->getData(rh.request());
        }
        else
        {
            // If an input request fails (e.g. if the field 6-h earlier is
            // also requested but not available), add a nullptr so that
            // the number of entries in the list is consistent. It is the
            // responsibility of the processor module to check...
            inputGrids << nullptr;
        }
    }

    // Initialize result grid.
    MStructuredGrid *derivedGrid = nullptr;
    if ( !inputGrids.isEmpty() && inputGrids.at(0) != nullptr )
    {
        derivedGrid = createAndInitializeResultGrid(inputGrids.at(0));
        derivedGrid->setMetaData(inputGrids.at(0)->getInitTime(),
                                 inputGrids.at(0)->getValidTime(),
                                 derivedVarName,
                                 inputGrids.at(0)->getEnsembleMember());
    }

    QStringList inputGridsVarNames;
    for (const auto& inputGrid : inputGrids)
    {
        if (inputGrid == nullptr) continue;
        inputGridsVarNames.append(inputGrid->getVariableName());
    }

    // Compute derived grid.
    if ((derivedGrid != nullptr) &&
            registeredDerivedDataProcessors.contains(derivedVarName))
    {
        registeredDerivedDataProcessors[derivedVarName]->compute(
                    inputGrids, derivedGrid);

        // Add source variables to processing information.
        derivedGrid->addProcessingInformation("source variables",
            inputGridsVarNames.join(", "));
        // Let derived processors add their own processing information.
        registeredDerivedDataProcessors[derivedVarName]->
            addProcessingInformation(derivedGrid);
    }

    // Release input fields.
    for (int i = 0; i < requiredInputVariablesList[derivedVarName][levelType].size(); i++)
    {
        if (inputGrids[i] == nullptr) continue;

        // Get the correct source, either the data reader or the derived
        // data source.
        QString varName = requiredInputVariablesList[derivedVarName][levelType][i];
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&varName, &ltype);
        MWeatherPredictionDataSource *source = getSourceOfInputVariable(varName, ltype);

        source->releaseData(inputGrids[i]);
    }

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_INFO(mlog, "Computed derived data field " << derivedVarName
                    << " in " << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                    << " seconds.");
#endif

    return derivedGrid;
}


MTask* MDerivedMetVarsDataSource::createTaskGraph(MDataRequest request)
{
    assert(inputSource != nullptr);

    MTask *task = new MTask(request, this);

    MDataRequestHelper rh(request);
    QString derivedVarName = rh.value("VARIABLE");
    MVerticalLevelType levelType = MVerticalLevelType(rh.intValue("LEVELTYPE"));
    QDateTime initTime  = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    rh.removeAll(locallyRequiredKeys()); // removes "VARIABLE"

    for (QString requiredVarStdName :
             requiredInputVariablesList[derivedVarName][levelType])
    {
        // Handle enforced level types (cf. updateStdNameAndLevelType()).
        MVerticalLevelType ltype = levelType;
        bool argsChanged = updateStdNameAndArguments(
                    &requiredVarStdName, &ltype, &initTime, &validTime);

        QString inputVarName = getInputVariableNameFromStdName(
                    requiredVarStdName);
        rh.insert("VARIABLE", inputVarName);
        rh.insert("LEVELTYPE", ltype); // update if changed
        rh.insert("INIT_TIME", initTime); // update if changed
        rh.insert("VALID_TIME", validTime); // update if changed

        MWeatherPredictionDataSource *source = getSourceOfInputVariable(inputVarName, ltype);

        // Check if if times have been changed the requested data fields are
        // available.
        bool everythingAvailable = true;
        if ( argsChanged && !source->availableInitTimes(
                 ltype, inputVarName).contains(initTime) )
        {
            everythingAvailable = false;
        }
        else if ( argsChanged && !source->availableValidTimes(
                      ltype, inputVarName, initTime).contains(validTime) )
        {
            everythingAvailable = false;
        }

        if (everythingAvailable)
        {
            task->addParent(source->getTaskGraph(rh.request()));
        }
    }

    return task;
}


QList<MVerticalLevelType> MDerivedMetVarsDataSource::availableLevelTypes()
{
    assert(inputSource != nullptr);
    return inputSource->availableLevelTypes();
}


void MDerivedMetVarsDataSource::identifyAvailableVariables(
        MVerticalLevelType levelType)
{
    assert(inputSource != nullptr);

    QStringList availableVars;

    // For each variable that can be derived, check if required input variables
    // are available. If yes, add the derived variable to the list of available
    // variables.
    for (const QString &derivedVarName : requiredInputVariablesList.keys())
    {
        // Check if variable is available from the input source instead.
        if (inputSource->availableVariables(levelType).contains(derivedVarName)) continue;

        bool requiredInputVarsAvailable = isDerivedVariableAvailable(derivedVarName, levelType);

        if (requiredInputVarsAvailable)
            availableVars << derivedVarName;
    }

    availableVariablesCache[levelType] = availableVars;
}


QStringList MDerivedMetVarsDataSource::availableVariables(
        MVerticalLevelType levelType)
{
    if (!availableVariablesCache.contains(levelType))
    {
        identifyAvailableVariables(levelType);
    }
    return availableVariablesCache.value(levelType);
}


void MDerivedMetVarsDataSource::identifyAvailableEnsembleMembers(
        MVerticalLevelType levelType, const QString& variableName)
{
    assert(inputSource != nullptr);

    QSet<unsigned int> members;
    for (QString inputVarStdName : requiredInputVariablesList[variableName][levelType])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        MWeatherPredictionDataSource *source = getSourceOfInputVariable(inputVarStdName, ltype);
        if (source == nullptr) continue;

        if (members.isEmpty())
        {
            members = source->availableEnsembleMembers(ltype,
                                                       getInputVariableNameFromStdName(inputVarStdName));
        }
        else
        {
            members = members.intersect(
                    source->availableEnsembleMembers(ltype,
                                                     getInputVariableNameFromStdName(inputVarStdName)));
        }
    }

    availableEnsembleMemberCache[levelType][variableName] = members;
}


QSet<unsigned int> MDerivedMetVarsDataSource::availableEnsembleMembers(
        MVerticalLevelType levelType, const QString& variableName)
{
    if (!availableEnsembleMemberCache.contains(levelType)
        || !availableEnsembleMemberCache[levelType].contains(variableName))
    {
        identifyAvailableEnsembleMembers(levelType, variableName);
    }
    return availableEnsembleMemberCache.value(levelType).value(variableName);
}


void MDerivedMetVarsDataSource::identifyAvailableInitTimes(
        MVerticalLevelType levelType, const QString& variableName)
{
    assert(inputSource != nullptr);

//NOTE: Qt4.8 does not yet support QList<QDateTime> with the intersect()
// method (qHash doesn't support QDateTime types).
//TODO (mr, 2016May17) -- Change this to QSet when switching to Qt 5.X.
    QList<QDateTime> times;
    for (QString inputVarStdName : requiredInputVariablesList[variableName][levelType])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        QString inputVarMapped = getInputVariableNameFromStdName(inputVarStdName);
        MWeatherPredictionDataSource *source = getSourceOfInputVariable(inputVarMapped, ltype);
        if (source == nullptr) continue;

        if (times.isEmpty())
        {
            times = source->availableInitTimes(ltype, inputVarMapped);
        }
        else
        {
            QList<QDateTime> inputTimes = source->availableInitTimes(ltype,inputVarMapped);

            for (const QDateTime &dt : times)
            {
                if ( !inputTimes.contains(dt) )
                {
                    times.removeOne(dt);
                }
            }
        }
    }

    availableInitTimesCache[levelType][variableName] = times;
}


QList<QDateTime> MDerivedMetVarsDataSource::availableInitTimes(
        MVerticalLevelType levelType, const QString& variableName)
{
    if (!availableInitTimesCache.contains(levelType)
        || !availableInitTimesCache[levelType].contains(variableName))
    {
        identifyAvailableInitTimes(levelType, variableName);
    }
    return availableInitTimesCache.value(levelType).value(variableName);
}


void MDerivedMetVarsDataSource::identifyAvailableValidTimes(
        MVerticalLevelType levelType,
        const QString& variableName, const QDateTime& initTime)
{
    assert(inputSource != nullptr);

    QList<QDateTime> times;
    for (QString inputVarStdName : requiredInputVariablesList[variableName][levelType])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        QString inputVarMapped = getInputVariableNameFromStdName(inputVarStdName);
        MWeatherPredictionDataSource *source = getSourceOfInputVariable(inputVarMapped, ltype);
        if (source == nullptr) continue;

        if (times.isEmpty())
        {
            times = source->availableValidTimes(ltype, inputVarMapped, initTime);
        }
        else
        {
            QList<QDateTime> inputTimes =
                    source->availableValidTimes(ltype, inputVarMapped, initTime);

            for (const QDateTime &dt : times)
            {
                if ( !inputTimes.contains(dt) )
                {
                    times.removeOne(dt);
                }
            }
        }
    }

    availableValidTimesCache[levelType][variableName][initTime] = times;
}


QList<QDateTime> MDerivedMetVarsDataSource::availableValidTimes(
        MVerticalLevelType levelType,
        const QString& variableName, const QDateTime& initTime)
{
    if (!availableValidTimesCache.contains(levelType)
        || !availableValidTimesCache[levelType].contains(variableName)
        || !availableValidTimesCache[levelType][variableName].contains(initTime))
    {
        identifyAvailableValidTimes(levelType, variableName, initTime);
    }
    return availableValidTimesCache.value(levelType).value(variableName).value(initTime);
}


QString MDerivedMetVarsDataSource::variableLongName(
        MVerticalLevelType levelType,
        const QString& variableName)
{
    Q_UNUSED(levelType);

    QString longName = QString("%1, computed from ").arg(variableName);

    for (QString inputVarStdName : requiredInputVariablesList[variableName][levelType])
    {
        // Handle enforced level types.
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&inputVarStdName, &ltype);

        longName += QString("%1/").arg(
                    getInputVariableNameFromStdName(inputVarStdName));
    }

    // Remove last "/" character.
    longName.resize(longName.length()-1);

    return longName;
}


QString MDerivedMetVarsDataSource::variableStandardName(
        MVerticalLevelType levelType,
        const QString& variableName)
{
    Q_UNUSED(levelType);

    // Special property of this data source: variable names equal CF standard
    // names.
    return variableName;
}


QString MDerivedMetVarsDataSource::variableUnits(
        MVerticalLevelType levelType,
        const QString& variableName)
{
    if (registeredDerivedDataProcessors.contains(variableName))
    {
        return registeredDerivedDataProcessors[variableName]->getUnitOfResult();
    }
    return QString();
}



/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MDerivedMetVarsDataSource::locallyRequiredKeys()
{
    return (QStringList() << "LEVELTYPE" << "VARIABLE" << "INIT_TIME"
            << "VALID_TIME");
}


QString MDerivedMetVarsDataSource::getInputVariableNameFromStdName(
        QString stdName)
{
    if (variableStandardNameToInputNameMapping.contains(stdName))
    {
        return variableStandardNameToInputNameMapping[stdName];
    }
    return stdName;
}


bool MDerivedMetVarsDataSource::updateStdNameAndArguments(
        QString *stdName, MVerticalLevelType *levelType,
        QDateTime *initTime, QDateTime *validTime)
{
    bool changedArguments = false;

    // Assume somthing like "surface_geopotential/SURFACE_2D" passed in stdName.
    // If only a variable name is passed (e.g., ""surface_geopotential"),
    // nothing is changed.
    QStringList definitionsList = stdName->split("/");
    if (definitionsList.size() >= 2)
    {
        *stdName = definitionsList.at(0);

        MVerticalLevelType newLevelType =
                MStructuredGrid::verticalLevelTypeFromConfigString(
                    definitionsList.at(1));

        // If a valid leveltype has been defined, update the passed variables.
        if (newLevelType != SIZE_LEVELTYPES)
        {
            *levelType = newLevelType;
            changedArguments = true;
        }
        else if (definitionsList.at(1) == "ANY")
        {
            // /ANY wildcard: Iterate over all level types, and check if one
            // of them is available.
            for (auto lType : availableLevelTypes())
            {
                QString stdNameCpy = *stdName;
                if (isDerivedVariableAvailable(stdNameCpy, lType))
                {
                    *levelType = lType;
                    changedArguments = true;
                    break;
                }
            }
        }
    }

    // Assume something like "lwe_thickness_of_precipitation_amount//-43200"
    // is passed. This will substract 43200 seconds = 12 hours from the
    // INIT_TIME.
    if (definitionsList.size() >= 3 && initTime != nullptr)
    {
        bool ok;
        int timeDifference_sec = definitionsList.at(2).toInt(&ok);
        if (ok)
        {
            *initTime = initTime->addSecs(timeDifference_sec);
            changedArguments = true;
        }
    }

    // Assume something like "lwe_thickness_of_precipitation_amount///-21600"
    // is passed. This will substract 21600 seconds = 6 hours from the
    // VALID_TIME.
    if (definitionsList.size() == 4 && validTime != nullptr)
    {
        bool ok;
        int timeDifference_sec = definitionsList.at(3).toInt(&ok);
        if (ok)
        {
            *validTime = validTime->addSecs(timeDifference_sec);
            changedArguments = true;
        }
    }

    return changedArguments;
}


bool MDerivedMetVarsDataSource::isDerivedVariableAvailable(const QString& derivedVarName, MVerticalLevelType levelType)
{
    // If a mapping is specified for this variable, check if the mapped variable is available.
    if (variableStandardNameToInputNameMapping.contains(derivedVarName))
    {
        bool avail = isDerivedVariableAvailable(variableStandardNameToInputNameMapping[derivedVarName], levelType);
        return avail;
    }

    // Check whether the derived variable can be computed for this level type.
    if (!requiredInputVariablesList.contains(derivedVarName) ||
        !requiredInputVariablesList[derivedVarName].contains(levelType))
    {
        return false;
    }

    if (requiredInputVariablesList[derivedVarName][levelType].isEmpty())
    {
        // No required input variables specified. This allows us to illustrate that
        // any of the input fields can be utilized, since we only access the metadata
        // from the input grid rather than the actual values of the grid.
        QStringList availableVars = inputSource->availableVariables(levelType);
        if (! availableVars.isEmpty())
        {
            QString dummyVariable = availableVars[0];
            requiredInputVariablesList[derivedVarName][levelType].append(dummyVariable);
        }
        else
        {
            // No variable of that level type available, can't compute derived variable
            // as no metadata present for this level type.
            return false;
        }
    }

    for (QString requiredVarStdName : requiredInputVariablesList[derivedVarName][levelType])
    {
        // Handle enforced level types (cf. comments above).
        MVerticalLevelType ltype = levelType;
        updateStdNameAndArguments(&requiredVarStdName, &ltype);

        if (isDerivedVariableAvailable(requiredVarStdName, ltype))
        {
            continue;
        }

        // If one of the required variables is not available from the
        // input source, skip. Search either by the enforced level type,
        // or by the one from the derived variable itself.
        if ( !inputSource->availableLevelTypes().contains(ltype) ||
                 !inputSource->availableVariables(ltype).contains(
                         getInputVariableNameFromStdName(requiredVarStdName)) )
        {
            return false;
        }
    }
    return true;
}


MWeatherPredictionDataSource *MDerivedMetVarsDataSource::getSourceOfInputVariable(const QString& varStdName, MVerticalLevelType levelType)
{
    if (this->availableVariables(levelType).contains(varStdName))
    {
        return this;
    }
    if (inputSource->availableVariables(levelType).contains(varStdName))
    {
        return inputSource;
    }
    if (variableStandardNameToInputNameMapping.contains(varStdName))
    {
        QString mappedVariable = variableStandardNameToInputNameMapping[varStdName];
        auto source = getSourceOfInputVariable(mappedVariable, levelType);
        return source;
    }

    return nullptr;
}


QStringList MDerivedMetVarsDataSource::registeredStandardNames()
{
    QSet<QString> registeredStandardNames;

    for (const QMap<MVerticalLevelType, QStringList> &inputNamesPerLevelTypePerProcessor : requiredInputVariablesList)
    {
        for (const QStringList &inputNames : inputNamesPerLevelTypePerProcessor)
        {
            for (QString inputName : inputNames)
            {
                MVerticalLevelType ltype = SIZE_LEVELTYPES;
                updateStdNameAndArguments(&inputName, &ltype);

                registeredStandardNames.insert(inputName);
            }
        }
    }

    return registeredStandardNames.values();
}


void MDerivedMetVarsDataSource::registerDerivedProcessorsForInputDataSource()
{
    if (inputSource == nullptr) return;

    MHeightToPressureFromICAOStdAtmosphereProcessor::registerProcessorsTo(this);
    MHeightToPressureFromGeopotentialHeightProcessor::registerProcessorsTo(this);
}

} // namespace Met3D
