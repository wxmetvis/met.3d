/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017 Marc Rautenhaus
**  Copyright 2017 Michael Kern
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_DERIVEDMETVARSMULTIVARPARTIALDERIVATIVE_H
#define MET_3D_DERIVEDMETVARSMULTIVARPARTIALDERIVATIVE_H

// standard library imports

// related third party imports

// local application imports
#include "deriveddatafieldprocessor.h"

namespace Met3D
{

/**
 * A derived variable processor for wind vector partial derivative calculation.
 * It should not be used directly, use its subclasses instead.
 */
class MWindVectorPartialDerivativeProcessor : public MDerivedDataFieldProcessor
{
public:
    MWindVectorPartialDerivativeProcessor(const QString &standardName,
                                          const QStringList &requiredInputVariables);

protected:
    /**
     * Lambda function to compute the velocity projected onto vector s at a
     * grid point.
     *
     * Computes the velocity projected onto the local wind vector direction s.
     */
    static inline double
    computeVs(MStructuredGrid *gridU, MStructuredGrid *gridV,
              int k, int j, int i, const QVector2D &s);
};

/**
 * A derived variable processor to calculate dVs / dn from the u wind and v wind variables.
 */
class MWindVectorPartialDerivativeProcessor_dUdV_dN
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_dUdV_dN();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};

/**
 * A derived variable processor to calculate dVs / dz from the u wind, v wind and geo-potential height variables.
 */
class MWindVectorPartialDerivativeProcessor_dUdV_dZ
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_dUdV_dZ();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};

/**
 * A derived variable processor to calculate dVs / dp from the u wind, v wind and geo-potential height variables.
 */
class MWindVectorPartialDerivativeProcessor_dUdV_dP
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_dUdV_dP();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};

/**
 * A derived variable processor to calculate d²Vs / dn² from the u wind and v wind variables.
 */
class MWindVectorPartialDerivativeProcessor_d2Ud2V_dN2
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_d2Ud2V_dN2();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};

/**
 * A derived variable processor to calculate d²Vs / dz² from the u wind, v wind and geo-potential height variables.
 */
class MWindVectorPartialDerivativeProcessor_d2Ud2V_dZ2
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_d2Ud2V_dZ2();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};

/**
 * A derived variable processor to calculate d²Vs / dp² from the u wind, v wind and geo-potential height variables.
 */
class MWindVectorPartialDerivativeProcessor_d2Ud2V_dP2
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_d2Ud2V_dP2();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};


/**
 * A derived variable processor to calculate d²Vs / dz²dn² from the u wind, v wind and geo-potential height variables.
 */
class MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdZ
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdZ();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};

/**
 * A derived variable processor to calculate d²Vs / dp²dn² from the u wind, v wind and geo-potential height variables.
 */
class MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdP
        : public MWindVectorPartialDerivativeProcessor
{
public:
    MWindVectorPartialDerivativeProcessor_d2Ud2V_dNdP();

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;
};

} // Met3D

#endif //MET_3D_DERIVEDMETVARSMULTIVARPARTIALDERIVATIVE_H
