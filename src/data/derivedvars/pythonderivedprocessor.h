/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPYTHONDERIVEDPROCESSOR_H
#define MET_3D_MPYTHONDERIVEDPROCESSOR_H

// standard library imports

// related third party imports

// local application imports
#include "data/derivedvars/deriveddatafieldprocessor.h"

namespace Met3D
{
class MDerivedMetVarsDataSource;

/**
 * Class for derived processors which call the Python interface to
 * perform the task.
 */
class MPythonDerivedProcessor : public MDerivedDataFieldProcessor
{
public:
    enum EndpointType
    {
        METPY_PV_BAROCLINIC = 0,
        METPY_WET_BULB_TEMPERATURE = 1,
        METPY_WET_BULB_POTENTIAL_TEMPERATURE = 2,
        METPY_STATIC_STABILITY = 3,
        INVALID
    };

    MPythonDerivedProcessor(QString standardName, QStringList requiredInputVariables,
                            EndpointType endpointType);

    void compute(QList<MStructuredGrid *> &inputGrids,
                 MStructuredGrid *derivedGrid) override;

    static void registerProcessorsTo(MDerivedMetVarsDataSource *dataSource);

private:
    EndpointType endpoint;
};

}

#endif //MET_3D_MPYTHONDERIVEDPROCESSOR_H
