/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Susanne Fuchs
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "abstractgrid.h"

namespace Met3D
{

/******************************************************************************
***                             MAbstractGrid                             ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/


MAbstractGrid::MAbstractGrid(MVerticalLevelType leveltype, MHorizontalGridType horizontalGridType)
    : MAbstractDataItem(),
      horizontalGridType(horizontalGridType),
      leveltype(leveltype),
      minMaxAccel(nullptr)
{

}

QString MAbstractGrid::verticalLevelTypeToString(MVerticalLevelType type)
{
    switch (type)
    {
    case SINGLE_LEVEL:
        return QString("Single Level");
    case PRESSURE_LEVELS_3D:
        return QString("Pressure Levels");
    case HYBRID_SIGMA_PRESSURE_3D:
        return QString("Hybrid Sigma Pressure Model Levels");
    case POTENTIAL_VORTICITY_2D:
        return QString("Potential Vorticity Levels");
    case LOG_PRESSURE_LEVELS_3D:
        return QString("Log(Pressure) Levels");
    case AUXILIARY_PRESSURE_3D:
        return QString("Model Levels with Auxiliary Pressure");
    case RADAR_LEVELS_3D:
        return QString("Radar Elevation");
    default:
        return QString("UNDEFINED");
    }
}

Met3D::MVerticalLevelType MAbstractGrid::verticalLevelTypeFromString(const QString &str)
{
    if (str == "Surface" || str == "Single Level") return SINGLE_LEVEL;
    if (str == "Pressure Levels") return PRESSURE_LEVELS_3D;
    if (str == "Hybrid Sigma Pressure Model Levels") return HYBRID_SIGMA_PRESSURE_3D;
    if (str == "Potential Vorticity Levels") return POTENTIAL_VORTICITY_2D;
    if (str == "Log(Pressure) Levels") return LOG_PRESSURE_LEVELS_3D;
    if (str == "Model Levels with Auxiliary Pressure") return AUXILIARY_PRESSURE_3D;
    if (str == "Radar Elevation") return RADAR_LEVELS_3D;

    return SIZE_LEVELTYPES;
}

MVerticalLevelType MAbstractGrid::verticalLevelTypeFromConfigString(const QString &str)
{
    if (str == "SURFACE_2D" || str == "SINGLE_LEVEL") return SINGLE_LEVEL;
    if (str == "PRESSURE_LEVELS_3D") return PRESSURE_LEVELS_3D;
    if (str == "HYBRID_SIGMA_PRESSURE_3D") return HYBRID_SIGMA_PRESSURE_3D;
    if (str == "POTENTIAL_VORTICITY_2D") return POTENTIAL_VORTICITY_2D;
    if (str == "LOG_PRESSURE_LEVELS_3D") return LOG_PRESSURE_LEVELS_3D;
    if (str == "AUXILIARY_PRESSURE_3D") return AUXILIARY_PRESSURE_3D;
    if (str == "RADAR_LEVELS_3D") return RADAR_LEVELS_3D;

    return SIZE_LEVELTYPES;
}

void MAbstractGrid::setTextureParameters(GLint  internalFormat,
                                           GLenum format,
                                           GLint  wrap,
                                           GLint  minMaxFilter)
{
    textureInternalFormat = internalFormat;
    textureFormat = format;
    textureWrap = wrap;
    textureMinMaxFilter = minMaxFilter;
}

float MAbstractGrid::interpolateValue(QVector3D vec3_lonLatP)
{
    return interpolateValue(vec3_lonLatP.x(), vec3_lonLatP.y(), vec3_lonLatP.z());
}


} // namespace Met3D
