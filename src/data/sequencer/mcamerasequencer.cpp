/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mcamerasequencer.h"
#include "ui_mcamerasequencer.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "gxfw/camera.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/msystemcontrol.h"
#include "util/mutil.h"
#include "util/mfiletypes.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MCameraSequencer::MCameraSequencer(QWidget *parent)
        : QWidget(parent),
          ui(new Ui::MCameraSequencer)
{
    ui->setupUi(this);
    ui->sequenceTable->horizontalHeader()->setStretchLastSection(true);
    ui->sequenceTable->setDragEnabled(true);
    ui->sequenceTable->setAcceptDrops(true);
    ui->sequenceTable->setDropIndicatorShown(true);
    ui->deleteButton->setEnabled(false);
    ui->sequenceComboBox->setEnabled(false);

    sequenceModel = new MSequencerTableModel();

    ui->sequenceTable->setModel(sequenceModel);
    ui->sequenceTable->resizeColumnsToContents();
    ui->sequenceTable->horizontalHeader()->setSectionResizeMode(
            QHeaderView::Stretch);

    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequenceRegistered, this,
            &MCameraSequencer::onSequenceRegistered);
    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequencesCleared, this,
            &MCameraSequencer::onSequencesCleared);
    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequenceRemoved, this,
            &MCameraSequencer::onSequenceRemoved);
    connect(MSystemManagerAndControl::getInstance(),
            &MSystemManagerAndControl::cameraSequenceRenamed, this,
            &MCameraSequencer::onSequenceRenamed);
}


MCameraSequencer::~MCameraSequencer()
{ delete ui; }


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MCameraSequencer::addDefaultSequence()
{
    auto *defaultSeq = new MCameraSequence(false);

    defaultSeq->registerSequence();

    // Select newest sequence
    ui->sequenceComboBox->setCurrentIndex(ui->sequenceComboBox->count() - 1);
}


/******************************************************************************
***                           PRIVATE SLOTS                                 ***
*******************************************************************************/

void MCameraSequencer::openSequence()
{
    QString fileName = MFileUtils::getOpenFileName(
            this, "Open Camera Sequence", FileTypes::M_CAMERA_SEQUENCE_CONFIG);
    if (!fileName.isEmpty())
    {
        auto *seq = new MCameraSequence(false);
        seq->loadFromFile(fileName);
        seq->registerSequence();

        ui->sequenceComboBox->setCurrentIndex(ui->sequenceComboBox->count() - 1);
    }
}


void MCameraSequencer::saveSequence()
{
    if (this->sequenceModel->getSequence() == nullptr)
    {
        QMessageBox::warning(this, "No camera sequence found", "No camera sequence was found that can be saved.");
        return;
    }

    if (this->sequenceModel->getSequence()->getFileName() == nullptr)
    {
        saveAsSequence();
        return;
    }

    this->sequenceModel->getSequence()->saveToFile(this->sequenceModel->getSequence()->getFileName());
}


void MCameraSequencer::saveAsSequence()
{
    if (this->sequenceModel->getSequence() == nullptr)
    {
        QMessageBox::warning(this, "No camera sequence found", "No camera sequence was found that can be saved.");
        return;
    }

    QString fileName = MFileUtils::getSaveFileName(
            this, "Save Camera Sequence", FileTypes::M_CAMERA_SEQUENCE_CONFIG);

    if (fileName.isEmpty())
    {
        QMessageBox::warning(this, "Filename is empty", "No filename was specified.");
        return;
    }

    this->sequenceModel->getSequence()->saveToFile(fileName);
}


void MCameraSequencer::addNewKey()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    // When invalid, add as last
    int row = currentIndex.isValid() ? currentIndex.row() + 1
                                     : sequenceModel->rowCount();

    if (this->sequenceModel->getSequence() == nullptr)
    {
        addDefaultSequence();
    }

    this->sequenceModel->insertRows(row, 1, currentIndex, nullptr);
}


void MCameraSequencer::addNewKeyFromCamera()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    // When invalid, add as last
    int row = currentIndex.isValid() ? currentIndex.row() + 1
                                     : sequenceModel->rowCount();

    if (this->sequenceModel->getSequence() == nullptr)
    {
        addDefaultSequence();
    }

    auto *key = new MSequenceKey();
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MSceneViewGLWidget *sceneView = sysMC->getLastInteractedSceneView();

    if (sceneView)
    {
        MCamera *camera = sceneView->getCamera();

        if (camera)
        {
            key->position = camera->getOrigin();
            key->rotation = QVector3D(camera->getPitch(), camera->getYaw(),
                                      camera->getRoll());
            key->transition = MCameraTransition::SPLINE;
        }
    }

    QList<MSequenceKey *> keys;
    keys.append(key);

    this->sequenceModel->insertRows(row, 1, currentIndex, &keys);
}


void MCameraSequencer::setKeyFromCamera()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    int row = currentIndex.isValid() ? currentIndex.row() : 0;

    if (this->sequenceModel->getSequence() == nullptr)
    {
        addDefaultSequence();
    }

    MSequenceKey *key = this->sequenceModel->getKeyForRow(row);

    if (key == nullptr) return;

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MSceneViewGLWidget *sceneView = sysMC->getLastInteractedSceneView();

    if (sceneView)
    {
        MCamera *camera = sceneView->getCamera();

        if (camera)
        {
            key->position = camera->getOrigin();
            key->rotation = QVector3D(camera->getPitch(), camera->getYaw(),
                                      camera->getRoll());
        }
    }

    QList<MSequenceKey *> keys;
    keys.append(key);

    this->sequenceModel->updateRow(currentIndex);
}


void MCameraSequencer::viewKeyInCamera()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid())
        return;

    int row = currentIndex.row();

    MSequenceKey *key = sequenceModel->getKeyForRow(row);

    if (!key)
        return;

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MSceneViewGLWidget *sceneView = sysMC->getLastInteractedSceneView();

    if (sceneView)
    {
        MCamera *camera = sceneView->getCamera();

        if (camera)
        {
            camera->setOrigin(key->position);

            QVector3D rot = key->rotation;
            camera->setRotation(rot.x(), rot.y(), rot.z());

            sceneView->updateCamera();
        }
    }
}


void MCameraSequencer::removeKey()
{
    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid())
        return;

    int row = currentIndex.row();

    if (!sequenceModel)
        return;

    this->sequenceModel->removeRows(row, 1, currentIndex);
}


void MCameraSequencer::removeSequence()
{
    QMessageBox::StandardButton result = QMessageBox::question(nullptr,
                                                               "Remove sequence?",
                                                               "Do you want to delete the current sequence?");

    if (result == QMessageBox::Yes)
    {
        MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
        sysMC->removeCameraSequence(ui->sequenceComboBox->currentIndex());
    }
}


void MCameraSequencer::loopChecked(int state)
{
    if (sequenceModel->getSequence() == nullptr) return;

    if (state > 0)
    {
        sequenceModel->getSequence()->loopSequence = true;
    }
    else
    {
        sequenceModel->getSequence()->loopSequence = false;
    }
}


void MCameraSequencer::sequenceChanged(int index)
{
    if (index < 0)
    {
        this->sequenceModel->setSequence(nullptr);
        ui->sequenceComboBox->setCurrentIndex(index);
        ui->runtimeSpinBox->setValue(10);
        ui->loopCheckbox->setChecked(false);
        ui->tensionSpinBox->setValue(0);
        ui->frameTimeSpinbox->setValue(10);
        return;
    }

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    MCameraSequence *sequence = sysMC->getCameraSequence(index);

    if (sequence == nullptr)
    {
        // Refresh list of sequences
        onSequenceRegistered();
        sequence = sysMC->getCameraSequence(0);
        ui->sequenceComboBox->setCurrentIndex(0);
        return;
    }

    this->sequenceModel->setSequence(sequence);
    ui->runtimeSpinBox->setValue(static_cast<int>(sequenceModel->getSequence()->sequenceRuntime));
    ui->loopCheckbox->setChecked(sequenceModel->getSequence()->loopSequence);
    ui->tensionSpinBox->setValue(sequence->sequenceSplineTension);
    ui->frameTimeSpinbox->setValue(sequence->sequenceFrameTime);
}


void MCameraSequencer::runtimeChanged(int value)
{
    auto sequence = sequenceModel->getSequence();
    if (sequence)
        sequence->sequenceRuntime = (float)value;
}


void MCameraSequencer::tensionChanged(double value)
{
    auto sequence = sequenceModel->getSequence();
    if (sequence)
        sequence->sequenceSplineTension = (float)value;
}


void MCameraSequencer::frameTimeChanged(int value)
{
    auto sequence = sequenceModel->getSequence();
    if (sequence)
        sequence->sequenceFrameTime = value;
}


void MCameraSequencer::moveKeyUp()
{
    if (this->sequenceModel->getSequence() == nullptr) return;

    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid()) return;

    int row = currentIndex.row();

    // Cant move up if we are in the first row.
    if (row <= 0) return;

    this->sequenceModel->moveRows(currentIndex.parent(), row, 1, currentIndex.parent(), row - 1);
}


void MCameraSequencer::moveKeyDown()
{
    if (this->sequenceModel->getSequence() == nullptr) return;

    QModelIndex currentIndex = this->ui->sequenceTable->currentIndex();

    if (!currentIndex.isValid()) return;

    int row = currentIndex.row();

    // Cant move down if we are in the last row.
    if (row >= this->sequenceModel->rowCount() - 1) return;

    this->sequenceModel->moveRows(currentIndex.parent(), row + 1, 1, currentIndex.parent(), row);
}


void MCameraSequencer::onSequenceRegistered()
{
    int selected = ui->sequenceComboBox->currentIndex();

    ui->sequenceComboBox->clear();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    auto sequences = sysMC->getCameraSequences();

    for (int i = 0; i < sequences.length(); i++)
    {
        QString label = sequences.at(i)->sequenceName;
        ui->sequenceComboBox->addItem(label, QVariant(i));
    }

    int count = ui->sequenceComboBox->count();

    if (selected >= count)
    {
        selected = count - 1;
    }
    if (selected < 0)
    {
        selected = 0;
    }

    ui->deleteButton->setEnabled(count > 0);
    ui->sequenceComboBox->setEnabled(count > 0);

    ui->sequenceComboBox->setCurrentIndex(selected);
}


void MCameraSequencer::onSequencesCleared()
{
    ui->sequenceComboBox->clear();
    ui->sequenceComboBox->setCurrentIndex(-1);
    this->sequenceModel->setSequence(nullptr);
    ui->runtimeSpinBox->setValue(10);
    ui->loopCheckbox->setChecked(false);
    ui->deleteButton->setEnabled(false);
    ui->tensionSpinBox->setValue(0);
    ui->frameTimeSpinbox->setValue(0);
}


void MCameraSequencer::onSequenceRemoved(int index)
{
    int selected = ui->sequenceComboBox->currentIndex();

    if (selected < 0)
        return;

    ui->sequenceComboBox->clear();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    auto sequences = sysMC->getCameraSequences();

    for (int i = 0; i < sequences.length(); i++)
    {
        QString label = sequences.at(i)->sequenceName;
        ui->sequenceComboBox->addItem(label, QVariant(i));
    }

    int count = ui->sequenceComboBox->count();

    if (selected > index)
    {
        selected--;
    }
    else if (selected == index)
    {
        selected = count > 0 ? 0 : -1;
    }

    ui->deleteButton->setEnabled(count > 0);
    ui->sequenceComboBox->setEnabled(count > 0);

    ui->sequenceComboBox->setCurrentIndex(selected);
}


void MCameraSequencer::onRenameSequence(QString newName)
{
    int index = ui->sequenceComboBox->currentIndex();
    if (index < 0)
        return;

    if (this->sequenceModel->getSequence() == nullptr)
        return;

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    sysMC->renameCameraSequence(ui->sequenceComboBox->currentIndex(), newName);
}


void MCameraSequencer::onSequenceRenamed(int index, QString name)
{
    ui->sequenceComboBox->setItemText(index, name);
}


void MCameraSequencer::onCreateCameraSequence()
{ addDefaultSequence(); }


} // namespace Met3D
