/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MCAMERASEQUENCE_H
#define MCAMERASEQUENCE_H

// standard library imports

// related third party imports
#include <QVector3D>
#include <QSettings>

// local application imports

namespace Met3D
{

/**
  Enum specifying different transition types between camera sequence keys.
 */
enum MCameraTransition : uint
{
    TELEPORT = 0,
    SPLINE = 1
};

/**
  MSequenceKey represents a single key in a sequence of keys forming the camera animation sequence.
 */
struct MSequenceKey
{
    /**
     * An optional label that the user can use to identify
     * keys and their respective views more easily.
     */
    QString label;

    /**
      Position of the camera key in lon-lat-z space.
     */
    QVector3D position; // Lon / Lat / Z

    /**
      Rotation of the camera key in Pitch, Yaw, Roll.
      Yaw is applied first, then pitch, then roll.
     */
    QVector3D rotation; // Pitch / Yaw / Roll

    /**
      The type of transition to the next key.
     */
    MCameraTransition transition = SPLINE;

    /**
      Advance the scene a time-step forward when reaching this key.
     */
    bool advanceTimestep = false;
};

/**
  The MCameraSequence class represents a sequence of camera keys for animation of camera paths.
 */
class MCameraSequence
{
public:
    /**
      Creates a new camera sequence class object and registers it in system control if no parameter is used.
      @param registerInSysControl Whether to register this sequence with system control, true by default.
     */
    explicit MCameraSequence(bool registerInSysControl = true);

    /**
     * Copy constructor. Constructs a copy of this camera sequence.
     * Will also create copies of all animation keys.
     * @param other The camera sequence to copy.
     */
    MCameraSequence(const MCameraSequence& other);

    ~MCameraSequence();

    /**
      Save the camera sequence to the specified file at @param fileName.
     */
    void saveToFile(const QString& fileName);

    /**
      Load the camera sequence from the specified file at @param fileName.
     */
    void loadFromFile(const QString& fileName);

    /**
      Save the camera sequence to the specified @param settings.
     */
    void saveToSettings(QSettings *settings) const;

    /**
      Load the camera sequence from the specified @param settings.
     */
    void loadFromSettings(QSettings *settings);

    /**
      @return The last used filename associated with this camera sequence.
     */
    QString getFileName() const;

    /**
      Registers this sequence in system control.
      This can be called more than once, and will register the sequence more than once if done so.
     */
    void registerSequence();

public:
    /**
      List of keys in order representing the camera sequence.
     */
    QList<MSequenceKey *> keys;

    /**
      Name of the camera sequence.
     */
    QString sequenceName;

    /**
      Whether to loop the sequence or not.
      When true, the last key transitions to the first key.
     */
    bool loopSequence;

    /**
      Runtime of the sequence in seconds.
      When @ref loopSequence is true, this corresponds to the time
      taken for one animation loop.
     */
    float sequenceRuntime;

    /**
      Tension of the sequence spline.
     */
    float sequenceSplineTension;

    /**
      The time in ms between the different animation steps.
      Even if the real render time of Met.3D is slower than this value,
      the resulting frames will be made with this frame time.
      The number and length of the steps is also dependent on the
      runtime.
     */
    int sequenceFrameTime;

private:
    /**
      Last associated filename of this sequence.
      The sequence was last saved to this file.
     */
    QString lastFileName;
};

}

#endif // MCAMERASEQUENCE_H
