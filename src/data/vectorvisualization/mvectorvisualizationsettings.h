/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MVECTORVISUALIZATIONSETTINGS_H
#define MVECTORVISUALIZATIONSETTINGS_H

// standard library imports

// related third party imports

// local application imports
#include "glyphs/mvectorglyphssettings.h"
#include "streambarbs/streambarbssettings.h"
#include "gxfw/nwpactorvariable.h"
#include "gxfw/properties/mnwpactorvarproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mnumberproperty.h"

namespace Met3D
{
/**
 * This class encapsualtes the settings available to the vector visualization
 * for 2-D sections. It contains shared settings (e.g. latitudinal and
 * longitudinal vector components and transformation information), and owns
 * the settings of the underlying vector visualization techniques (glyphes,
 * streambarbs).
 */
class MVectorVisualizationSettings
{
public:
    MVectorVisualizationSettings(MNWPHorizontalSectionActor *hostActor);
    ~MVectorVisualizationSettings();

    void loadConfigurationPrior_V_1_14(QSettings *settings);

    /**
     * @return The currently selected longitude variable.
     */
    MNWPActorVariable *getSelectedLongitudeComponentVar() const;

    /**
     * @return The currently selected latitude variable.
     */
    MNWPActorVariable *getSelectedLatitudeComponentVar() const;

    /**
     * @return True if the currently selected vector components (longitudinal
     * and latitudinal) are valid.
     */
    bool areVectorComponentsValid() const;

    /**
     * @return True if rendering streambarbs is enabled.
     */
    bool areStreambarbsEnabled() const;

    /**
     * @return True if rendering vector glyphs is enabled.
     */
    bool areVectorGlyphsEnabled() const;

    /**
     * @param property The property to check.
     * @return True if the given @p property belongs to the streambarbs
     * settings.
     */
    bool isStreambarbProperty(const MProperty *property) const;

    /**
     * @param property The property to check.
     * @return True if the given @p property belongs to the vector glyph
     * settings.
     */
    bool isVectorGlyphProperty(const MProperty *property) const;

    /**
     * @param property The property to check.
     * @return True if the given @p property is a shared property for
     * vector visualization.
     */
    bool isGeneralVectorVisProperty(const MProperty *property) const;

    MNWPHorizontalSectionActor *hostActor;

    MNWPActorVarProperty lonComponentVarProp;
    MNWPActorVarProperty latComponentVarProp;

    QList<QString> varNameList;

    MBoolProperty rotateUVComponentsToMapProjectionProp;
    MSciDoubleProperty lengthProjectedBasisVectorsProp;

    MVectorGlyphsSettings *vectorGlyphsSettings;
    MStreamBarbsSettings *streambarbsSettings;

    MProperty vectorVisProp;
};
}


#endif //MVECTORVISUALIZATIONSETTINGS_H
