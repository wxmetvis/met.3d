/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mvectorvisualizationsettings.h"

// standard library imports

// related third party imports

// local application imports
#include "actors/nwphorizontalsectionactor.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MVectorVisualizationSettings::MVectorVisualizationSettings(
    MNWPHorizontalSectionActor *hostActor)
    : hostActor(hostActor),
      vectorGlyphsSettings(nullptr),
      streambarbsSettings(nullptr)
{
    vectorVisProp = MProperty("Vector visualization");

    lonComponentVarProp = MNWPActorVarProperty("Longitudinal component");
    lonComponentVarProp.setConfigKey("lon_component");
    lonComponentVarProp.setTooltip("example: eastward component of wind");
    vectorVisProp.addSubProperty(lonComponentVarProp);

    latComponentVarProp = MNWPActorVarProperty("Latitudinal component");
    latComponentVarProp.setConfigKey("lat_component");
    latComponentVarProp.setTooltip("example: northward component of wind");
    vectorVisProp.addSubProperty(latComponentVarProp);

    auto vectorCompCallback = [=]()
    {
        if (areVectorComponentsValid())
        {
            hostActor->streambarbsSource
                     ->setNWPVariables(lonComponentVarProp.value(),
                                       latComponentVarProp.value());

            streambarbsSettings->updateLonLatComponentVars();
        }
    };

    lonComponentVarProp.registerValueCallback(vectorCompCallback);
    latComponentVarProp.registerValueCallback(vectorCompCallback);

    rotateUVComponentsToMapProjectionProp = MBoolProperty("Rotate to graticule projection", false);
    rotateUVComponentsToMapProjectionProp.setConfigKey("rotate_to_graticule_projection");
    rotateUVComponentsToMapProjectionProp.setTooltip(
        "If u/v wind components are given as eastward and northward "
        "wind and not relative to the projected grid, use the "
        "grid projection specified in the graticule to rotate the "
        "wind barbs.");
    rotateUVComponentsToMapProjectionProp.registerValueCallback([=]()
    {
        if (!rotateUVComponentsToMapProjectionProp)
        {
            hostActor->streambarbsSource->setVectorCorrForProjMapsGrid(nullptr);
        }
        else
        {
            hostActor->streambarbsSource->setVectorCorrForProjMapsGrid(
                    hostActor->vectorCorrForProjMapsGrid);
        }
    });
    vectorVisProp.addSubProperty(rotateUVComponentsToMapProjectionProp);

    lengthProjectedBasisVectorsProp =
            MSciDoubleProperty("Length of basis vectors", 0.01);
    lengthProjectedBasisVectorsProp.setConfigKey("length_of_basis_vectors");
    lengthProjectedBasisVectorsProp.setTooltip(
            "Length of the wind basis vectors used to rotate wind "
            "barbs to map projection.");
    lengthProjectedBasisVectorsProp.setSignificantDigits(1);
    lengthProjectedBasisVectorsProp.setStep(0.01);
    lengthProjectedBasisVectorsProp.registerValueCallback([=]()
    {
        hostActor->updateMapProjectionVectorCorrectionField();
    });
    vectorVisProp.addSubProperty(lengthProjectedBasisVectorsProp);

    // Wind barbs.
    vectorGlyphsSettings = new MVectorGlyphsSettings(hostActor);
    vectorVisProp.addSubProperty(vectorGlyphsSettings->enabledProp);

    // Streambarbs.
    streambarbsSettings = new MStreamBarbsSettings(hostActor);
    vectorVisProp.addSubProperty(streambarbsSettings->enabledProp);
}


MVectorVisualizationSettings::~MVectorVisualizationSettings()
{
    delete vectorGlyphsSettings;
    delete streambarbsSettings;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MVectorVisualizationSettings::loadConfigurationPrior_V_1_14(
    QSettings *settings)
{
    QVersionNumber version = readConfigVersion(settings);

    // Before 1.11, core parameters for vector visualization (wind variables,
    // projection info) were saved in the "windbarbs" sub menu. With the
    // addition of the "streambarbs" sub menu, these core parameters moved to
    // a shared level. Add backwards compatibility here.
    bool beforeVersion1_11 = version < QVersionNumber(1, 11);
    if (beforeVersion1_11)
    {
        settings->beginGroup("Windbarbs");
    }

    LOG4CPLUS_DEBUG(mlog, "Loading visualization settings using compatibility mode.");
    lonComponentVarProp.setIndex(settings->value("uComponent", 0).toInt());
    latComponentVarProp.setIndex(settings->value("vComponent", 0).toInt());

    rotateUVComponentsToMapProjectionProp = settings->value(
            "rotateUVComponentsToMapProjection",
            false).toBool();
    lengthProjectedBasisVectorsProp = settings->value(
            "lengthProjectedBasisVectors",
            0.01).toDouble();

    if (beforeVersion1_11)
    {
        settings->endGroup(); // Windbarbs
    }

    vectorGlyphsSettings->loadConfigurationPrior_V_1_14(settings);
    streambarbsSettings->loadConfigurationPrior_V_1_14(settings);
}


bool MVectorVisualizationSettings::areVectorComponentsValid() const
{
    return lonComponentVarProp.value() != nullptr
           && latComponentVarProp.value() != nullptr;
}


bool MVectorVisualizationSettings::areStreambarbsEnabled() const
{
    return streambarbsSettings->enabledProp;
}


bool MVectorVisualizationSettings::areVectorGlyphsEnabled() const
{
    return vectorGlyphsSettings->enabledProp;
}


MNWPActorVariable *
MVectorVisualizationSettings::getSelectedLongitudeComponentVar() const
{
    return lonComponentVarProp;
}


MNWPActorVariable *
MVectorVisualizationSettings::getSelectedLatitudeComponentVar() const
{
    return latComponentVarProp;
}


bool MVectorVisualizationSettings::isGeneralVectorVisProperty(
    const MProperty *property) const
{
    return property == &lonComponentVarProp
           || property == &latComponentVarProp
           || property == &rotateUVComponentsToMapProjectionProp
           || property == &lengthProjectedBasisVectorsProp;
}


bool MVectorVisualizationSettings::isStreambarbProperty(
    const MProperty *property) const
{
    return isGeneralVectorVisProperty(property) || streambarbsSettings->
           isProperty(property);
}


bool MVectorVisualizationSettings::isVectorGlyphProperty(
    const MProperty *property) const
{
    return isGeneralVectorVisProperty(property) || vectorGlyphsSettings->
           isProperty(property);
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
}