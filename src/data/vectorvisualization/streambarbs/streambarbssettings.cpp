/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024      Christoph Fischer [*]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "streambarbssettings.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "actors/nwphorizontalsectionactor.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MStreamBarbsSettings::MStreamBarbsSettings(MNWPHorizontalSectionActor *hostActor)
    : hostActor(hostActor),
      seedingMode(SeedingMode::JobardAndLefer),
      textureUnitTransferFunction(-1)
{
    auto valueCallback = [=]()
    {
        if (hostActor->suppressActorUpdates()) return;

        hostActor->asynchronousStreamBarbsRequest();
        if (batchComputationProp)
        {
            hostActor->asynchronousRequestStreamBarbsBatch();
        }
        hostActor->emitActorChangedSignal();
    };

    enabledProp = MBoolProperty("Streambarbs", false);
    enabledProp.setConfigKey("streambarbs_enabled");
    enabledProp.setConfigGroup("streambarbs");
    enabledProp.registerValueCallback(valueCallback);
    groupAppearanceProp = MProperty("Appearance");
    enabledProp.addSubProperty(groupAppearanceProp);

    lineWidthProp = MFloatProperty("Line width", 3.f);
    lineWidthProp.setConfigKey("line_width");
    lineWidthProp.setMinMax(0.1f, 10.0f);
    lineWidthProp.setDecimals(2);
    lineWidthProp.setStep(0.1f);
    lineWidthProp.registerValueCallback(valueCallback);
    groupAppearanceProp.addSubProperty(lineWidthProp);

    colorProp = MColorProperty("Line colour", QColor(48, 112, 179));
    colorProp.setConfigKey("line_color");
    colorProp.registerValueCallback(valueCallback);
    groupAppearanceProp.addSubProperty(colorProp);

    fadingProp = MBoolProperty("Fade out", true);
    fadingProp.setConfigKey("fade_out");
    fadingProp.setTooltip(
        "Set opacity dependent on wind speed (for constant colour)");
    fadingProp.registerValueCallback(valueCallback);
    groupAppearanceProp.addSubProperty(fadingProp);

    transferFunctionProp = MTransferFunction1DProperty("Transfer function");
    transferFunctionProp.setConfigKey("transfer_function");
    transferFunctionProp.registerValueCallback(valueCallback);
    groupAppearanceProp.addSubProperty(transferFunctionProp);

    showBarbsProp = MBoolProperty("Wind barbs", true);
    showBarbsProp.setConfigKey("wind_barbs_enabled");
    showBarbsProp.registerValueCallback(valueCallback);
    enabledProp.addSubProperty(showBarbsProp);

    barbSpacingProp = MIntProperty("Spacing", 5);
    barbSpacingProp.setConfigKey("barb_spacing");
    barbSpacingProp.setSuffix(" °");
    barbSpacingProp.setMinMax(1, 20);
    barbSpacingProp.registerValueCallback([=]()
    {
        if (hostActor->currentStreamBarbs != nullptr)
        {
          hostActor->currentStreamBarbs->updateStreamlineVertexData();
          hostActor->currentStreamBarbs->updateStreambarbsBarbVertexData(this);
          hostActor->uploadStreamBarbs();
        }
    });
    showBarbsProp.addSubProperty(barbSpacingProp);

    barbWidthProp = MFloatProperty("Width", 0.04);
    barbWidthProp.setConfigKey("barb_width");
    barbWidthProp.setMinMax(0.01, 0.1);
    barbWidthProp.setDecimals(3);
    barbWidthProp.setStep(0.01);
    barbWidthProp.registerValueCallback([=]()
    {
       if (hostActor->currentStreamBarbs != nullptr)
       {
           hostActor->currentStreamBarbs->updateStreamlineVertexData();
           hostActor->currentStreamBarbs->updateStreambarbsBarbVertexData(this);
           hostActor->uploadStreamBarbs();
       }
    });
    showBarbsProp.addSubProperty(barbWidthProp);

    computationGroupProp = MProperty("Computation");
    enabledProp.addSubProperty(computationGroupProp);

    QStringList seedingModeNames;
    seedingModeNames << "Jobard & Lefer (1997)" << "Regular Grid (dense seeding)"
        << "Regular Grid (density dependent seeding)";
    seedingModeProp = MEnumProperty("Seed mode", seedingModeNames);
    seedingModeProp.setConfigKey("seed_mode");
    seedingModeProp.registerValueCallback([=]()
    {
        seedingMode = static_cast<SeedingMode>(seedingModeProp.value());
        valueCallback();
    });
    computationGroupProp.addSubProperty(seedingModeProp);

    spacingProp = MFloatProperty("Spacing", 2.f);
    spacingProp.setConfigKey("spacing");
    spacingProp.setMinMax(1.0, 25.0);
    spacingProp.setDecimals(3);
    spacingProp.setStep(1.00);
    spacingProp.registerValueCallback(valueCallback);
    computationGroupProp.addSubProperty(spacingProp);

    stepSizeProp = MFloatProperty("Step size", 1.0f);
    stepSizeProp.setConfigKey("step_size");
    stepSizeProp.setMinMax(0.01, spacingProp);
    stepSizeProp.setDecimals(2);
    stepSizeProp.setStep(0.01);
    stepSizeProp.registerValueCallback(valueCallback);
    computationGroupProp.addSubProperty(stepSizeProp);

    differenceSpacingProp = MFloatProperty("Min. allowed spacing", 0.5f);
    differenceSpacingProp.setConfigKey("min_allowed_spacing");
    differenceSpacingProp.setMinMax(0.0, 1.0);
    differenceSpacingProp.setDecimals(1);
    differenceSpacingProp.setStep(0.1);
    differenceSpacingProp.setTooltip(
        "As multiple of spacing how close streamlines can approach each other before being terminated.");
    differenceSpacingProp.registerValueCallback(valueCallback);
    computationGroupProp.addSubProperty(differenceSpacingProp);

    maxStreambarbLengthProp = MIntProperty("Maximum length", 200);
    maxStreambarbLengthProp.setConfigKey("max_length");
    maxStreambarbLengthProp.setMinMax(1, 2000);
    maxStreambarbLengthProp.setStep(10);
    maxStreambarbLengthProp.setTooltip("Maximum length in world units.");
    maxStreambarbLengthProp.registerValueCallback(valueCallback);
    computationGroupProp.addSubProperty(maxStreambarbLengthProp);

    loopDetectionProp = MBoolProperty("Loop detection", true);
    loopDetectionProp.setConfigKey("loop_detection");
    loopDetectionProp.registerValueCallback(valueCallback);
    computationGroupProp.addSubProperty(loopDetectionProp);

    batchComputationProp = MBoolProperty("Precompute all levels", false);
    batchComputationProp.setConfigKey("precompute_all_levels");
    batchComputationProp.registerValueCallback(valueCallback);
    computationGroupProp.addSubProperty(batchComputationProp);

    computeOnlyOnReleaseProp = MBoolProperty("Update only on mouse release", false);
    computeOnlyOnReleaseProp.setConfigKey("update_only_on_mouse_release");
    computeOnlyOnReleaseProp.setTooltip(
        "Only updates the streambarbs when the mouse is released from the section elevation handle");
    computeOnlyOnReleaseProp.registerValueCallback(valueCallback);
    computationGroupProp.addSubProperty(computeOnlyOnReleaseProp);
}


MStreamBarbsSettings::~MStreamBarbsSettings()
{
    // Delete allocated resources for streambarbs.
    releaseTextureUnits();
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MStreamBarbsSettings::releaseTextureUnits() const
{
    if (textureUnitTransferFunction >= 0)
    {
        hostActor->releaseTextureUnit(textureUnitTransferFunction);
    }
}


QString MStreamBarbsSettings::seedingModeToString(SeedingMode mode)
{
    switch (mode)
    {
    case SeedingMode::JobardAndLefer:
    {
        return "JobardAndLefer";
    }
    case SeedingMode::RegularGridDense:
    {
        return "RegularGridDense";
    }
    case SeedingMode::RegGrid2:
    {
        return "RegGrid2";
    }
    case SeedingMode::GPU01:
    {
        return "GPU01";
    }
    case SeedingMode::GPUDense:
    {
        return "GPUDense";
    }
    default:
    {
        return "";
    }
    }
}


MStreamBarbsSettings::SeedingMode MStreamBarbsSettings::stringToSeedingMode(
    const QString &modeString)
{
    if (modeString == "JobardAndLefer")
    {
        return SeedingMode::JobardAndLefer;
    }
    else if (modeString == "RegularGridDense")
    {
        return SeedingMode::RegularGridDense;
    }
    else if (modeString == "RegGrid2")
    {
        return SeedingMode::RegGrid2;
    }
    else if (modeString == "GPU01")
    {
        return SeedingMode::GPU01;
    }
    else if (modeString == "GPUDense")
    {
        return SeedingMode::GPUDense;
    }
    else
    {
        return SeedingMode::Invalid;
    }
}

void MStreamBarbsSettings::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    settings->beginGroup("Streambarbs");

    enabledProp = settings->value("enabled", false).toBool();
    lineWidthProp = settings->value("lineWidth", 3.).toFloat();
    colorProp = settings->value("color", QColor(48, 112, 179)).value<QColor>();
    fadingProp = settings->value("fading", true).toBool();

    QString tfName = settings->value("transferFunction").toString();
    transferFunctionProp.setByName(tfName);

    showBarbsProp = settings->value("showBarbs", true).toBool();
    barbSpacingProp = settings->value("barbSpacing", 5).toInt();
    barbWidthProp = settings->value("barbWidth", 0.04).toFloat();

    seedingModeProp = static_cast<int>(stringToSeedingMode(
                                          settings->value(
                                                  "seedingMode",
                                                  "JobardAndLefer").toString()));
    spacingProp = settings->value("spacing", 2.).toFloat();
    stepSizeProp = settings->value("stepSize", 1.).toFloat();
    differenceSpacingProp = settings->value("differenceSpacing", 0.5).
                                            toFloat();
    maxStreambarbLengthProp = settings->value("maxStreambarbLength", 0.5).
                                            toInt();
    loopDetectionProp = settings->value("loopDetection", true).toBool();
    batchComputationProp = settings->value("batchComputation", false).
                                          toBool();
    computeOnlyOnReleaseProp = settings->value("computeOnlyOnRelease", false).
                                          toBool();

    settings->endGroup(); // Streamlines
}


bool MStreamBarbsSettings::isProperty(const MProperty* property) const
{
    return property == &enabledProp
        || property == &seedingModeProp
        || property == &barbSpacingProp
        || property == &barbWidthProp
        || property == &stepSizeProp
        || property == &maxStreambarbLengthProp
        || property == &loopDetectionProp
        || property == &batchComputationProp
        || property == &computeOnlyOnReleaseProp
        || property == &spacingProp
        || property == &differenceSpacingProp
        || property == &showBarbsProp
        || property == &lineWidthProp
        || property == &colorProp
        || property == &fadingProp
        || property == &transferFunctionProp;
}


void MStreamBarbsSettings::updateLonLatComponentVars()
{
    hostActor->asynchronousStreamBarbsRequest();
    if (batchComputationProp)
    {
        hostActor->asynchronousRequestStreamBarbsBatch();
    }
    hostActor->emitActorChangedSignal();
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
}
