/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "streambarbscomputation.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/gl/typedvertexbuffer.h"
#include "util/metroutines.h"


namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MStreamBarbsComputation::MStreamBarbsComputation()
    : uGrid(nullptr),
      vGrid(nullptr),
      projCorrGrid(nullptr),
      seedingMode(MStreamBarbsSettings::SeedingMode::JobardAndLefer),
      slicePos_hPa(0.0),
      seedSpacing(1.0),
      minimumAllowedDistanceFactor(1.0),
      stepSize(1.0),
      loopDetection(true),
      maxLength(200),
      regionSizeAdjustedSpacing(1.0),
      cellPointsNumLats(0),
      cellPointsNumLons(0),
      lonBoundariesDegrees(0.0, 0.0),
      latBoundariesDegrees(0.0, 0.0),
      result(nullptr)
{
    cellPoints.clear();
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MStreamBarbsComputation::setSeedingMode(
    MStreamBarbsSettings::SeedingMode seedingMode)
{
    this->seedingMode = seedingMode;
}


void MStreamBarbsComputation::setU(MStructuredGrid *u)
{
    this->uGrid = u;
}


void MStreamBarbsComputation::setV(MStructuredGrid *v)
{
    this->vGrid = v;
}


void MStreamBarbsComputation::setSlicePos_hPa(float level)
{
    this->slicePos_hPa = level;
}


void MStreamBarbsComputation::setSpacing(float spacing)
{
    this->seedSpacing = spacing;

    // Use an adjusted spacing depending on the size of the domain.
    float xExtent = abs(lonBoundariesDegrees.y() - lonBoundariesDegrees.x());
    float yExtent = abs(latBoundariesDegrees.y() - latBoundariesDegrees.x());
    this->regionSizeAdjustedSpacing = spacing * std::max(xExtent, yExtent) / 360.0;
}


void MStreamBarbsComputation::setDifferenceSpacing(float diffSpacing)
{
    this->minimumAllowedDistanceFactor = diffSpacing;
}


void MStreamBarbsComputation::setStepSize(float stepSize)
{
    this->stepSize = stepSize;
}


void MStreamBarbsComputation::setLoopDetection(bool loopDetection)
{
    this->loopDetection = loopDetection;
}


void MStreamBarbsComputation::setMaximumLength(int len)
{
    this->maxLength = len;
}


bool MStreamBarbsComputation::compute()
{
    // Check if inputs are valid.
    if (! uGrid || ! vGrid)
    {
        return false;
    }

    if (uGrid->getLevelType() != vGrid->getLevelType())
    {
        LOG4CPLUS_WARN(
            mlog, "Vector field of longitudinal and latitudinal"
            " variables must have the same vertical level type."
            " Can't generate streambarbs.");
        return false;
    }

    if (uGrid->getNumLats() != vGrid->getNumLats() ||
        uGrid->getNumLons() != vGrid->getNumLons())
    {
        LOG4CPLUS_WARN(
            mlog, "U and V field must have the same horizontal extent."
            " Can't generate streambarbs.");
        return false;
    }

    // OK, lets compute.
    result = new MStreamBarbsData();
    result->
        setComputationSource(std::unique_ptr<MStreamBarbsComputation>(this));

    // No streamlines if horizontal slice position is outside the
    // data domain (assuming longitudial/latitudial are on the same grid).
    if (vGrid->getLevelType() != SINGLE_LEVEL &&
        (vGrid->getBottomDataVolumePressure_hPa() < slicePos_hPa
         || vGrid->getTopDataVolumePressure_hPa() > slicePos_hPa))
    {
        return result;
    }

    unsigned int lats = uGrid->getNumLats();
    unsigned int lons = uGrid->getNumLons();
    const QVector3D northWest = uGrid->
        getNorthWestTopDataVolumeCorner_lonlatp();
    const QVector3D southEast = uGrid->
        getSouthEastBottomDataVolumeCorner_lonlatp();

    // Set the longitude/latitude boundaries of the input field.
    lonBoundariesDegrees = QVector2D(northWest.x(), southEast.x());
    latBoundariesDegrees = QVector2D(southEast.y(), northWest.y());
    if (uGrid->gridIsCyclicInLongitude())
    {
        // If cyclic, add a delta so we wrap around.
        lonBoundariesDegrees.setY(southEast.x() + uGrid->getDeltaLon());
    }
    setSpacing(seedSpacing);

    // Initialize cellPoints field, where each grid cell contains references to
    // close streamlines (to efficiently check whether two are too close).
    cellPointsNumLats = ceil(lats / seedSpacing);
    cellPointsNumLons = ceil(lons / seedSpacing);
    cellPoints.resize(cellPointsNumLats * cellPointsNumLons);

    // Generate the streamlines based on the set seeding mode.
    switch (seedingMode)
    {
    case MStreamBarbsSettings::SeedingMode::JobardAndLefer:
        generateStreamlinesJobardAndLefer();
        break;
    case MStreamBarbsSettings::SeedingMode::RegularGridDense:
    case MStreamBarbsSettings::SeedingMode::RegGrid2:
        generateStreamlinesRegularGrid();
        break;
    default:
        break;
    }
    // Fix the streamlines with respect to the boundaries.
    cutStreamlinesAtBoundaries();
    return true;
}


MStreamBarbsData *MStreamBarbsComputation::getResult() const
{
    return result;
}


void MStreamBarbsComputation::updateBarbVertices(
    float barbWidth, int barbSpacing) const
{
    result->clearBarbsVertices();
    const float MS_TO_KNOTS = 3600.0 / MetConstants::NAUTICAL_MILE_IN_METRES;

    // Make size of barbs depending on line spacing, introduce here some
    // reasonable size parameters, similar to the vector glyphs.
    float deltaGrid = std::min<double>(regionSizeAdjustedSpacing * 2, 6.0);

    // Adapt line width of glyph elements.
    float adaptedLineWidth = deltaGrid * barbWidth;
    // Barb length.
    float barbLength = 0.75 * deltaGrid;

    // Set posOffset.
    float posOffset = barbLength / 9.0;

    // Set pennant triangle offset.
    float pennantPeakOffset = 0.3f * posOffset;
    // Set offset of flags and pennant.
    float pennantOffset = 2.f * 0.95f * posOffset;
    // Set flag offset.
    float flagOffset = pennantOffset;

    // Compute width of small flag.
    float smallWidth = 0.2f * 0.6f * deltaGrid;
    // Compute width of large flag.
    float largeWidth = 0.2f * deltaGrid;

    // Create the base line of the wind barb glyph.
    for (const QVector<QVector2D>& point : result->getStreamlines())
    {
        float barbStartingPos = 0;

        while (barbStartingPos < point.size() * stepSize - barbSpacing)
        {
            barbStartingPos += barbSpacing;
            if (barbStartingPos > point.size() * stepSize - barbSpacing)
            {
                break;
            }

            float posAlongBarb = barbStartingPos;

            float index = floor(barbStartingPos / stepSize);
            if (index >= point.size() - 2)
            {
                break;
            }

            QVector2D dir = QVector2D(
                point.at(index + 1).x() - point.at(index).x(),
                point.at(index + 1).y() - point.at(index).y());
            dir.normalize();
            QVector2D posWorld = streamlineAt(point, barbStartingPos);

            // Sample both wind textures, obtain wind speed in u and v direction.
            float varLongitudinal = uGrid->interpolateValue(
                posWorld.x(), posWorld.y(), slicePos_hPa);
            float varLatitudinal = vGrid->interpolateValue(
                posWorld.x(), posWorld.y(), slicePos_hPa);

            // If no data is available, do not draw any glyphs.
            if (varLongitudinal == M_MISSING_VALUE
                || varLatitudinal == M_MISSING_VALUE)
            {
                break;
            }

            // Wind direction.
            QVector2D windDir = QVector2D(varLongitudinal, varLatitudinal);

            // Normalized wind direction.
            QVector2D normal = QVector2D(-dir.y(), dir.x());
            normal.normalize();

            // wind velocity in m/s.
            const float velocity = windDir.length();
            // Wind velocity in kt.
            const float totalKnots = velocity * MS_TO_KNOTS;
            float modKnots = totalKnots;

            // Only draw if there is enough wind.
            if (modKnots >= 5.0)
            {
                posAlongBarb += posOffset / 2.f;

                // Wind barbs consist of full triangles for each 50 knots,
                // long lines for the remainder, each 10 knots,
                // small line for the remainder, each 5 knots.

                // Create the pennants at the current wind barb position.
                int num50knotBarbs = static_cast<int>(floor(modKnots / 50));
                for (int i = 0; i < num50knotBarbs; ++i)
                {
                    posAlongBarb += adaptedLineWidth / 2.f;

                    QVector2D pos0 = streamlineAt(point, posAlongBarb);
                    QVector2D pos1 = pos0 - dir * posOffset / 2.f
                                     + dir * pennantPeakOffset + normal *
                                     largeWidth;
                    QVector2D pos2 = streamlineAt(
                        point, posAlongBarb - posOffset / 2.f
                               + (pennantOffset + pennantPeakOffset));

                    result->addBarbsVertex(QVector3D(pos0, totalKnots));
                    result->addBarbsVertex(QVector3D(pos1, totalKnots));
                    result->addBarbsVertex(QVector3D(pos2, totalKnots));

                    posAlongBarb += 2 * posOffset; // advance along streamline
                }

                modKnots = fmod(modKnots, 50);
                posAlongBarb += adaptedLineWidth / 2.0;

                // Create the full flag triangle at the current wind barb
                // position.
                for (int j = 0; j < floor(modKnots / 10); ++j)
                {
                    posAlongBarb += adaptedLineWidth / 2.f;

                    // Left bottom.
                    QVector2D pos0 = streamlineAt(
                        point, posAlongBarb - adaptedLineWidth / 2.f);
                    // Left top.
                    QVector2D pos1 =
                        pos0 - dir * flagOffset + normal * largeWidth;
                    // Right bottom.
                    QVector2D pos2 = streamlineAt(
                        point, posAlongBarb + adaptedLineWidth / 2.f);
                    // Right top.
                    QVector2D pos3 =
                        pos2 - dir * flagOffset + normal * largeWidth;

                    result->addBarbsVertex(QVector3D(pos0, totalKnots));
                    result->addBarbsVertex(QVector3D(pos1, totalKnots));
                    result->addBarbsVertex(QVector3D(pos2, totalKnots));

                    result->addBarbsVertex(QVector3D(pos1, totalKnots));
                    result->addBarbsVertex(QVector3D(pos2, totalKnots));
                    result->addBarbsVertex(QVector3D(pos3, totalKnots));

                    posAlongBarb += posOffset; // advance along streamline
                }

                modKnots = fmod(modKnots, 10);

                // Create the half flag triangle at the current wind barb
                // position.
                for (int k = 0; k < floor(modKnots / 5); ++k)
                {
                    posAlongBarb += adaptedLineWidth / 2.f;

                    // Left bottom.
                    QVector2D pos0 = streamlineAt(
                        point, posAlongBarb - adaptedLineWidth / 2.f);
                    // Left top.
                    QVector2D pos1 =
                        pos0 - dir * flagOffset * 0.6 + normal * smallWidth;
                    // Right bottom.
                    QVector2D pos2 = streamlineAt(
                        point, posAlongBarb + adaptedLineWidth / 2.f);
                    // Right top.
                    QVector2D pos3 =
                        pos2 - dir * flagOffset * 0.6 + normal * smallWidth;

                    result->addBarbsVertex(QVector3D(pos0, totalKnots));
                    result->addBarbsVertex(QVector3D(pos1, totalKnots));
                    result->addBarbsVertex(QVector3D(pos2, totalKnots));

                    result->addBarbsVertex(QVector3D(pos1, totalKnots));
                    result->addBarbsVertex(QVector3D(pos2, totalKnots));
                    result->addBarbsVertex(QVector3D(pos3, totalKnots));

                    posAlongBarb += posOffset; // advance along streamline
                }

                barbStartingPos = posAlongBarb;
            }
        }
    }
}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MStreamBarbsComputation::cutStreamlinesAtBoundaries() const
{
    if (! uGrid->gridIsCyclicInLongitude())
    {
        return; // Nothing to do.
    }

    // In cyclic grids, streamlines might have been computed on a field
    // ranging from 0..360, but the visualization uses -180..180.
    // Roll over the points to the -180/180 range.
    for (auto &streamline : result->getStreamlines())
    {
        for (auto &pointAlongStreamline : streamline)
        {
            if (pointAlongStreamline.x() > 180.0)
            {
                pointAlongStreamline.setX(pointAlongStreamline.x() - 360.f);
            }
        }
    }

    // ... and split up streamlines that cross the boundary (e.g. 180 -> -180).
    // This creates a new set of more streamlines.
    QVector<QVector<QVector2D>> fixedStreamlines;
    for (auto streamline : result->getStreamlines())
    {
        QVector<QVector2D> fixedStreamline;
        for (int i = 0; i < streamline.size() - 1; i++)
        {
            fixedStreamline.push_back(streamline[i]);
            // check for boundary
            if (abs(streamline[i].x() - streamline[i + 1].x()) > 180.0)
            {
                // line segment crosses boundary, finish this streamline and
                // start a new one
                if (fixedStreamline.size() > 1)
                {
                    fixedStreamlines.push_back(fixedStreamline);
                }
                fixedStreamline.clear();
            }
        }

        // Finished processing this streamline, we still need to add the last segment currently in fixedStreamline buffer
        fixedStreamline.push_back(streamline[streamline.size() - 1]); // add last vertex, boundary checked before
        if (fixedStreamline.size() > 1)
        {
            fixedStreamlines.push_back(fixedStreamline); // add it
        }
    }
    // Update the result.
    result->setStreamlines(fixedStreamlines);
}


void MStreamBarbsComputation::computeStreamlineVertexData() const
{
    float minKnots = INFINITY;
    float maxKnots = 0;
    const float MS_TO_KNOTS = 3600.f / MetConstants::NAUTICAL_MILE_IN_METRES;

    result->clearStreamlineVertices();

    for (QVector<QVector2D> &streamline : result->getStreamlines())
    {
        for (int i = 0; i < streamline.size(); i++)
        {
            const QVector2D &slPoint = streamline.at(i);
            // For each 2-D point, get its wind speed, at push it as
            // 3-D vertex (pos, speed) to the buffer.
            float dirLon = uGrid->interpolateValue(slPoint.x(), slPoint.y(),
                                                   slicePos_hPa);
            float dirLat = vGrid->interpolateValue(slPoint.x(), slPoint.y(),
                                                   slicePos_hPa);
            QVector2D direction = QVector2D(dirLon, dirLat);
            float velocity = direction.length();  // wind velocity in m/s
            float knots = velocity * MS_TO_KNOTS; // wind velocity in kt

            if (dirLon == M_MISSING_VALUE || dirLat == M_MISSING_VALUE)
            {
                knots = 0;
            }

            if (knots < minKnots)
            {
                minKnots = knots;
            }
            if (knots > maxKnots)
            {
                maxKnots = knots;
            }
            // Push vertex, twice if it is one in the middle (start of next segment).
            result->addStreamlineVertex(
                QVector3D(slPoint.x(), slPoint.y(), knots));
            if (i != 0 && i != streamline.size() - 1)
            {
                result->addStreamlineVertex(
                    QVector3D(slPoint.x(), slPoint.y(), knots));
            }
        }
    }

    // Set the results.
    result->setMinDataKnots(minKnots);
    result->setMaxDataKnots(maxKnots);
}


void MStreamBarbsComputation::correctVectorsInProjection(MStructuredGrid *grid)
{
    this->projCorrGrid = grid;
}


unsigned int MStreamBarbsComputation::getMemorySize_kb() const
{
    unsigned int totalSize = 0;
    for (const QVector<QVector2D>& cellPoint : cellPoints)
    {
        totalSize += cellPoint.size() * sizeof(QVector2D);
    }
    return totalSize / 1024;
}


bool MStreamBarbsComputation::checkIfMissingValue(QVector2D point) const
{
    float directionLon =
        uGrid->interpolateValue(point.x(), point.y(), slicePos_hPa);
    float directionLat =
        vGrid->interpolateValue(point.x(), point.y(), slicePos_hPa);

    if (directionLon == M_MISSING_VALUE || directionLat == M_MISSING_VALUE)
    {
        return true;
    }
    if (projCorrGrid)
    {
        // Is there a missing value in the projection correction field?
        float projCorrVal = projCorrGrid->interpolateValueOnLevel(
            point.x(), point.y(), 0);
        if (projCorrVal == M_MISSING_VALUE)
        {
            return true;
        }
    }
    return false;
}


QVector2D MStreamBarbsComputation::getDirectionAt(QVector2D point) const
{
    float speedU =
        uGrid->interpolateValue(point.x(), point.y(), slicePos_hPa);
    float speedV =
        vGrid->interpolateValue(point.x(), point.y(), slicePos_hPa);

    QVector2D direction;
    if (projCorrGrid)
    {
        // Correct by transforming the vector.
        float column[4];
        projCorrGrid->interpolateValuesInVerticalColumn(point.x(), point.y(), column);

        direction = speedU * QVector2D(column[0], column[1])
                    + speedV * QVector2D(column[2], column[3]);
    }
    else
    {
        direction = QVector2D(speedU, speedV);
    }
    direction.normalize();
    return direction;
}


void MStreamBarbsComputation::generateStreamlinesJobardAndLefer()
{
    // Start from center of domain.
    QVector2D start;
    start.setX((lonBoundariesDegrees.x() + lonBoundariesDegrees.y()) / 2.f);
    start.setY((latBoundariesDegrees.x() + latBoundariesDegrees.y()) / 2.f);

    const auto &streamlines = result->getStreamlines();

    // Compute first streamline.
    computeStreamline(start);
    int startIteration = 0;
    while (streamlines.empty())
    {
        // Jobard & Lefer just cancel streamline production if the first one
        // fails to generate. We keep on trying a few times at 9 evenly spaced
        // positions since there might be missing values at the starting
        // location.
        float iterLon = startIteration / 3.f + 0.5;
        float iterLat = startIteration % 3 + 0.5;
        start.setX(lonBoundariesDegrees.x() + iterLon *
            (lonBoundariesDegrees.y() - lonBoundariesDegrees.x()) / 3.f);
        start.setY(latBoundariesDegrees.x() + iterLat *
            (latBoundariesDegrees.y() - latBoundariesDegrees.x()) / 3.f);

        computeStreamline(start);
        startIteration++;
    }
    if (streamlines.empty())
        return; // Cant generate at any of the starting points.

    // Compute new seed points based on Jobard and Lefer.
    // Basically move a step orthogonal along the last streamline and start
    // from there.
    int streamLineIndex = 0;

    while (streamLineIndex < streamlines.size())
    {
        QVector<QVector2D> current = streamlines.at(streamLineIndex);

        for (int i = 0; i < current.size() - 1; i++)
        {
            // Calculate new points (orthogonal to current direction).
            QVector2D dir = QVector2D(
                current.at(i + 1).x() - current.at(i).x(),
                current.at(i + 1).y() - current.at(i).y());
            QVector2D ortho1 = QVector2D(-dir.y(), dir.x());
            QVector2D ortho2 = QVector2D(dir.y(), -dir.x());
            ortho1.normalize();
            ortho2.normalize();
            QVector2D seedPoint1 = current.at(i) + regionSizeAdjustedSpacing * ortho1;
            QVector2D seedPoint2 = current.at(i) + regionSizeAdjustedSpacing * ortho2;

            // Check if points valid.
            if (checkSamplePoint(seedPoint1, regionSizeAdjustedSpacing))
            {
                computeStreamline(seedPoint1);
            }

            if (checkSamplePoint(seedPoint2, regionSizeAdjustedSpacing))
            {
                computeStreamline(seedPoint2);
            }
        }
        streamLineIndex++;
    }
}


void MStreamBarbsComputation::generateStreamlinesRegularGrid()
{
    uint32_t lats = uGrid->getNumLats();
    uint32_t lons = uGrid->getNumLons();

    QVector<QVector2D> seedPoints;

    // Generate uniform seed points.
    for (uint32_t i = 0; i < lons; i++)
    {
        for (uint32_t j = 0; j < lats; j++)
        {
            QVector2D seedPoint = QVector2D(
                    lonBoundariesDegrees.x() + i * uGrid->getDeltaLon(),
                    latBoundariesDegrees.x() + j * uGrid->getDeltaLat());

            // Push seed point if dense seeding, or density dependent at every
            // @c spacing positions.
            if (seedingMode == MStreamBarbsSettings::SeedingMode::RegGrid2
                && fmod(i, seedSpacing) == 0.f && fmod(j, seedSpacing) == 0.f)
            {
                seedPoints.push_back(seedPoint);
            }
            else if (seedingMode == MStreamBarbsSettings::SeedingMode::RegularGridDense)
            {
                seedPoints.push_back(seedPoint);
            }
        }
    }

    // Compute streamlines for the seed points.
    for (const QVector2D point : seedPoints)
    {
        if (checkSamplePoint(point, regionSizeAdjustedSpacing))
        {
            computeStreamline(point);
        }
    }
}


void MStreamBarbsComputation::computeStreamlineInDirection(
        const QVector2D start, QVector<QVector2D> &buffer, int maxLength,
        bool forward)
{
    float step = forward ? stepSize : -stepSize;

    buffer.push_back(start);

    int length = 0;
    while (length < maxLength)
    {
        QVector2D current = buffer.back();

        // 4 step runge kutta
        QVector2D p0 = getDirectionAt(current) * step;
        QVector2D p1 = getDirectionAt(current + (p0 / 2)) * step;
        QVector2D p2 = getDirectionAt(current + (p1 / 2)) * step;
        QVector2D p3 = getDirectionAt(current + p2) * step;
        QVector2D point = current + (p0 / 6) + (p1 / 3) + (p2 / 3) + (p3 / 6);

        // If grid is cyclic wrap around the longitudes.
        bool lonCyclic = uGrid->gridIsCyclicInLongitude();

        if (lonCyclic && point.x() < lonBoundariesDegrees.x())
        {
            point.setX(point.x() + 360.f);
        }
        if (lonCyclic && point.x() >= lonBoundariesDegrees.y())
        {
            point.setX(point.x() - 360.f);
        }

        // Stop in this direction, if we encounter a missing value ...
        if (checkIfMissingValue(current))
        {
            break;
        }

        // Minimum allowed distance between streamlines: the seed spacing times
        // the differenceSpacing factor (e.g., 0.5)
        float minAllowedDistance = minimumAllowedDistanceFactor * regionSizeAdjustedSpacing;
        // ... or if we are too close to existing streamline ...
        if (!checkSamplePoint(point, minAllowedDistance))
        {
            break;
        }
        // ... or if we detect a loop in the current streamline.
        if (loopDetection && detectLoop(buffer, point))
        {
            break;
        }

        length++;
        buffer.push_back(point);
    }
}


void MStreamBarbsComputation::computeStreamline(const QVector2D start)
{
    if (checkIfMissingValue(start))
    {
        return;
    }
    // Buffer for all the points along the current streamline.
    QVector<QVector2D> buffer;
    int maxSteps = static_cast<int>(maxLength / stepSize);
    buffer.reserve(maxSteps);

    // Compute in negative direction.
    computeStreamlineInDirection(start, buffer, maxSteps / 2, false);
    // Reverse order of vertices along streamline.
    std::reverse(buffer.begin(), buffer.end());
    buffer.pop_back();
    // Append computation result in positive direction.
    computeStreamlineInDirection(start, buffer, maxSteps, true);

    // Final buffer.
    if (buffer.size() >= 2)
    {
        streamlineFinished(buffer);
    }
}


bool MStreamBarbsComputation::detectLoop(const QVector<QVector2D> &buffer,
                                         const QVector2D point) const
{
    float minAllowedDistance =
        minimumAllowedDistanceFactor * regionSizeAdjustedSpacing;
    float minAllowedDistanceSquared = minAllowedDistance * minAllowedDistance;

    // Check whether within this streamline we were already close to the current
    // point. However, do not check with recently generated points along this
    // streamline, as they are probably closer than the threshold for small
    // step sizes. Take here twice the amount of steps taken to reach the
    // minimum allowed distance.

    // Do not check the last numSteps points along streamline, this depends on
    // the step size.
    float numSteps = 2 * minAllowedDistance / stepSize;
    int bufferEnd = buffer.size() - static_cast<int>(numSteps);
    if (bufferEnd < 0)
    {
        // Too short, no loop.
        return false;
    }

    return isAnyPointWithinDistanceOf(buffer, 0, bufferEnd, point,
                                      minAllowedDistanceSquared);
}


bool MStreamBarbsComputation::isAnyPointWithinDistanceOf(
    const QVector<QVector2D> &buffer, const QVector2D& point,
    float maxDistSquared)
{
    return isAnyPointWithinDistanceOf(buffer, 0, buffer.size(), point,
                                      maxDistSquared);
}


bool MStreamBarbsComputation::isAnyPointWithinDistanceOf(
    const QVector<QVector2D> &buffer, int start, int end, const QVector2D& point,
    float maxDistSquared)
{
    end = std::min(end, buffer.size() - 1);
    for (int i = start; i < end; i++)
    {
        const QVector2D &pointInBuffer = buffer[i];
        float dx = point.x() - pointInBuffer.x();
        float dy = point.y() - pointInBuffer.y();

        float distanceSquared = dx * dx + dy * dy;
        if (distanceSquared < maxDistSquared)
        {
            return true;
        }
    }
    return false;
}


bool MStreamBarbsComputation::checkSamplePoint(QVector2D point, float spacing)
{
    // Check if point outside of data field.
    if (point.x() <= lonBoundariesDegrees.x())
    {
        return false;
    }
    if (point.x() >= lonBoundariesDegrees.y())
    {
        return false;
    }
    if (point.y() <= latBoundariesDegrees.x())
    {
        return false;
    }
    if (point.y() >= latBoundariesDegrees.y())
    {
        return false;
    }

    // Transform the point into indices for the cellPoints field
    QVector2D tmp = coordsToGrid(point);

    int x = static_cast<int>(floor(tmp.x() / this->seedSpacing));
    int y = static_cast<int>(floor(tmp.y() / this->seedSpacing));

    if ((x > cellPointsNumLons) || (y > cellPointsNumLats))
    {
        return false;
    }

    // Check all surrounding 8 cells if the distance to one point is smaller
    // than spacing.
    float spacingSquared = spacing * spacing;
    const int numCellPts = cellPoints.size();
    for (int i = -1; i <= 1; i++)
    {
        for (int j = -1; j <= 1; j++)
        {
            int cellId = (x + i) * cellPointsNumLats + (y + j);

            if ((cellId >= 0) && (cellId < numCellPts))
            {
                const QVector<QVector2D> &localCell = cellPoints.at(cellId);
                if (isAnyPointWithinDistanceOf(localCell, point,
                                               spacingSquared))
                {
                    return false;
                }
            }
        }
    }
    return true;
}


QVector2D MStreamBarbsComputation::coordsToGrid(const QVector2D point) const
{
    return {point.x() - lonBoundariesDegrees.x(),
            point.y() - latBoundariesDegrees.x()};
}


void MStreamBarbsComputation::streamlineFinished(
    const QVector<QVector2D> &buffer)
{
    result->addStreamline(buffer);
    updateCellPoints(buffer);
}


void MStreamBarbsComputation::updateCellPoints(
    const QVector<QVector2D> &buffer)
{
    // update cellPoints
    // For each point of the streamline store it in the cartesian grid.
    for (QVector2D elem : buffer)
    {
        QVector2D tmp = coordsToGrid(elem);
        int x = floor(tmp.x() / seedSpacing);
        int y = floor(tmp.y() / seedSpacing);

        int num = x * cellPointsNumLats + y;

        if (num >= 0 && num < static_cast<int>(cellPoints.size()))
        {
            cellPoints[x * cellPointsNumLats + y].push_back(elem);
        }
    }
}


QVector2D MStreamBarbsComputation::streamlineAt(
    const QVector<QVector2D> &streamline, float value) const
{
    int index = floor(value / stepSize);
    float rest = value - index * stepSize;

    if (index >= streamline.size() - 1)
    {
        index = streamline.size() - 2;
    }

    QVector2D dir = QVector2D(
        streamline.at(index + 1).x() - streamline.at(index).x(),
        streamline.at(index + 1).y() - streamline.at(index).y());
    dir.normalize();
    QVector2D posWorld = streamline.at(index) + rest * dir;

    return posWorld;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
};
