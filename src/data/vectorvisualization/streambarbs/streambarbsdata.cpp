/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "streambarbsdata.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "streambarbscomputation.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MStreamBarbsData::MStreamBarbsData()
    : minDataKnots(0.0),
      maxDataKnots(0.0)
{
    streamlines.clear();
    barbsVertexData.clear();
    streamlineVertexData.clear();
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MStreamBarbsData::setMinDataKnots(float minKnots)
{
    this->minDataKnots = minKnots;
}


void MStreamBarbsData::setMaxDataKnots(float maxKnots)
{
    this->maxDataKnots = maxKnots;
}


float MStreamBarbsData::getMinDataKnots() const
{
    return minDataKnots;
}


float MStreamBarbsData::getMaxDataKnots() const
{
    return maxDataKnots;
}


QVector<QVector3D> &MStreamBarbsData::getBarbsVertices()
{
    return barbsVertexData;
}


QVector<QVector3D> &MStreamBarbsData::getStreamlineVertices()
{
    return streamlineVertexData;
}


void MStreamBarbsData::setComputationSource(
    std::unique_ptr<MStreamBarbsComputation> source)
{
    computationSource = std::move(source);
}


void MStreamBarbsData::updateStreambarbsBarbVertexData(
    MStreamBarbsSettings* settings) const
{
    computationSource->updateBarbVertices(settings->barbWidthProp,
        settings->barbSpacingProp);
}


void MStreamBarbsData::updateStreamlineVertexData() const
{
    computationSource->computeStreamlineVertexData();
}


void MStreamBarbsData::clearBarbsVertices()
{
    barbsVertexData.clear();
}


void MStreamBarbsData::clearStreamlineVertices()
{
    streamlineVertexData.clear();
}


bool MStreamBarbsData::isEmpty() const
{
    return streamlines.empty();
}


bool MStreamBarbsData::areVerticesComputed() const
{
    return ! streamlineVertexData.isEmpty();
}


void MStreamBarbsData::addStreamline(const QVector<QVector2D> &streamline)
{
    streamlines.push_back(streamline);
}


QVector<QVector<QVector2D>> &MStreamBarbsData::getStreamlines()
{
    return streamlines;
}


void MStreamBarbsData::setStreamlines(
    const QVector<QVector<QVector2D>> &streamlines)
{
    this->streamlines = streamlines;
}


void MStreamBarbsData::addStreamlineVertex(QVector3D vertex)
{
    streamlineVertexData.push_back(vertex);
}


void MStreamBarbsData::addBarbsVertex(QVector3D vertex)
{
    barbsVertexData.push_back(vertex);
}


unsigned int MStreamBarbsData::getMemorySize_kb()
{
    unsigned long totalSize = 0;

    // Compute size of streamlines.
    for (QVector<QVector2D> &streamline : streamlines)
    {
        totalSize += streamline.size() * sizeof(QVector2D);
    }
    totalSize += streamlineVertexData.size() * sizeof(QVector3D);
    totalSize += barbsVertexData.size() * sizeof(QVector3D);

    return totalSize / 1024 + computationSource->getMemorySize_kb();
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
}
