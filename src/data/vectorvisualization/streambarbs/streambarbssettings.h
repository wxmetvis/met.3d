/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024      Christoph Fischer [*]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSTREAMBARBSSETTINGS_H
#define MSTREAMBARBSSETTINGS_H

// standard library imports

// related third party imports

// local application imports
#include "actors/transferfunction1d.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mtransferfunctionproperty.h"


namespace Met3D {
class MNWPHorizontalSectionActor;
/**
 * This class encapsualtes the settings available to the StreamBarbs rendering
 * in 2-D sections.
 */
class MStreamBarbsSettings {

public:
    /**
     * Creates a new settings object.
     * @param hostActor The host actor for the streambarbs. Used to set the
     * properties on this actor.
     */
    MStreamBarbsSettings(MNWPHorizontalSectionActor *hostActor);
    ~MStreamBarbsSettings();

    void releaseTextureUnits() const;

    /**
     * The different types of supported seeding modes.
     * @c JobardAndLefer: http://dx.doi.org/10.1007/978-3-7091-6876-9_5
     * @c RegularGridDense: Dense seeding starting with every grid cell.
     * @c RegGrid2: Similar to RegularGridDense but only starting at every
     *      n-th grid cell, depending on the set @c density.
     * @c GPU01: Same as @c RegGrid2, but on GPU.
     * @c GPUDense: Same as @c RegularGridDense, but on GPU.
     */
    enum class SeedingMode
    {
        Invalid = -1,
        JobardAndLefer = 0,
        RegularGridDense = 1,
        RegGrid2 = 2,
        GPU01 = 3,
        GPUDense = 4
    };

    /**
     * Convert the seeding mode to its string representation.
     * @param mode The seeding mode to convert.
     * @return The string representation.
     */
    static QString seedingModeToString(SeedingMode mode);

    /**
     * Converts the string representation to the appropriate seeding mode.
     * @param modeString The string to convert.
     * @return The corresponding seeding mode, or @c SeedingMode::Invalid if the
     * string does not belong to a seeding mode.
     */
    static SeedingMode stringToSeedingMode(const QString& modeString);

    /**
     * Load the configuration of streambarbs from the @p settings object.
     * @param settings The QSettings object to draw the configuration from.
     * @param properties Reference to the properties of the main actor.
     */
    void loadConfigurationPrior_V_1_14(QSettings* settings);

    /**
     * @param property The property to check.
     * @return True if the given @p property is one of the properties
     * handled by the StreamBarbs.
     */
    bool isProperty(const MProperty* property) const;

    /**
     * Called when the lon lat component variables are updated from the
     * vector visualization settings.
     */
    void updateLonLatComponentVars();

    MNWPHorizontalSectionActor* hostActor;

    MBoolProperty enabledProp;

    SeedingMode seedingMode;
    MEnumProperty seedingModeProp;

    MProperty groupAppearanceProp;
    MFloatProperty lineWidthProp;
    MColorProperty colorProp;
    MBoolProperty fadingProp;
    MTransferFunction1DProperty transferFunctionProp;
    int textureUnitTransferFunction;

    MBoolProperty showBarbsProp;
    MIntProperty barbSpacingProp;
    MFloatProperty barbWidthProp;

    MProperty computationGroupProp;
    MFloatProperty spacingProp;
    MFloatProperty stepSizeProp;
    MFloatProperty differenceSpacingProp;
    MIntProperty maxStreambarbLengthProp;
    MBoolProperty loopDetectionProp;
    MBoolProperty batchComputationProp;
    MBoolProperty computeOnlyOnReleaseProp;
};

};

#endif //MSTREAMBARBSSETTINGS_H
