/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSTREAMBARBSCOMPUTATION_H
#define MSTREAMBARBSCOMPUTATION_H

// standard library imports

// related third party imports

// local application imports
#include "streambarbsdata.h"
#include "streambarbssettings.h"
#include "data/structuredgrid.h"

namespace Met3D
{
class MStreamBarbsData;

/**
 * This class encapsulates the computation of streamlines on a 2-D plane with
 * attached wind barbs. This class specifically does not do any memory
 * management, and is solely responsible for the computation. Use appropriate
 * data sources (e.g., @c MHorizontalStreamBarbsDataSource) for memory
 * management and as part of a pipeline.
 */
class MStreamBarbsComputation
{
public:
    /**
     * Creates a new computation object with default values for the settings.
     */
    MStreamBarbsComputation();

    /**
     * Set the field of the U wind (longitudinal wind).
     * @param u The U field as structured grid.
     */
    void setU(MStructuredGrid* u);

    /**
     * Set the field of the V wind (latitudinal wind).
     * @param v The V field as structured grid.
     */
    void setV(MStructuredGrid* v);

    /**
     * Set the method used to seed streamlines (determine where to start them).
     * @param seedingMode The type of seeding mode to use.
     */
    void setSeedingMode(MStreamBarbsSettings::SeedingMode seedingMode);

    /**
     * The level on which the streambarbs should be computed.
     * @param level The level in hPa.
     */
    void setSlicePos_hPa(float level);

    /**
     * Set the spacing of the streamlines.
     * @param spacing The spacing to use.
     */
    void setSpacing(float spacing);

    /**
     * Set the difference spacing of the streamlines: This is a factor between
     * 0 and 1, indicating that if the distance of a streamline to a
     * neighbouring one becomes less than @c diffSpacing*spacing it is stopped.
     * @param diffSpacing The difference spacing factor to use.
     */
    void setDifferenceSpacing(float diffSpacing);

    /**
     * Set the step size of the streamline tracing.
     * @param stepSize The step size to use.
     */
    void setStepSize(float stepSize);

    /**
     * Toggle the loop detection feature. Stops the continuation of streamlines
     * if a loop is detected in the current streamline.
     * @param loopDetection The loop detection flag.
     */
    void setLoopDetection(bool loopDetection);

    /**
     * Set the maximum length of a streamline as multiple of @c stepSize.
     * @param len The maximum length.
     */
    void setMaximumLength(int len);

    /**
     * Trigger the computation based on the currently set parameters.
     * @return True if the computation was successful, false otherwise (for
     * example, if the input grids do not align or are not set).
     */
    bool compute();

    /**
     * Get the result of the computation
     * @return The result object, or nullptr if no computation has been performed.
     */
    MStreamBarbsData* getResult() const;

    /**
     * Compute the vertex data for the streamlines to upload to the GPU.
     */
    void computeStreamlineVertexData() const;

    /**
     * Updates the vertex data for the barbs along the streamlines based on the
     * given setting. This does not require a recomputation of streamlines.
     * @param barbWidth The width of the barbs.
     * @param barbSpacing The spacing between adjacent barbs.
     */
    void updateBarbVertices(float barbWidth, int barbSpacing) const;

    /**
     * Setting a grid corrects the direction of the computed vectors for the
     * streamlines according to the given base vectors in @p grid. This
     * needs to be set if a projection is used that distorts the wind vectors.
     * @param grid The grid containing the base vectors for the projection
     * transformation. See @c updateMapProjectionVectorCorrectionField() in the
     * horizontal section actor for details.
     */
    void correctVectorsInProjection(MStructuredGrid* grid);

    /**
     * Computes the size of this object in kb. Since the computation object is
     * part of the data object, we need to keep track of this as well.
     * @return The size.
     */
    unsigned int getMemorySize_kb() const;

private: // Internal computation methods
    /**
      Creates streamlines using the CPU by seeding them evenly-spaced using the
      algorithm developed by Jobard and Lefer.
     */
    void generateStreamlinesJobardAndLefer();

    /**
      Creates streamlines using the CPU by seeding them regularly on the
      variables' grid.
     */
    void generateStreamlinesRegularGrid();

    /**
     * Computes a new streamline starting at the given @p start point. Does
     * a forward and backward computation using the helper function
     * @c computeStreamlineInDirection.
     * @param start The point where the streamline starts.
     */
    void computeStreamline(QVector2D start);

    /**
     * Computes a streamline starting from @p start in the direction given
     * by @p forward (forward or backward) and emplaces all generated points
     * in @p buffer. The line is computed until the length threshold
     * @c maxLength is met, the line goes out of bounds, it gets too close to
     * other lines, or a loop is detected.
     * @param start The point where the streamline starts.
     * @param buffer The buffer where to emplace the new vertices.
     * @param maxLength The maximum length of the buffer.
     * @param forward If true the line is computed along the wind, false into
     * the opposite direction.
     */
    void computeStreamlineInDirection(QVector2D start,
                                      QVector<QVector2D> &buffer,
                                      int maxLength, bool forward);

    /**
     * Get direction of 2D vector field (U/V) at @p point. Does compensate for
     * the projection if the correction field is set.
     * @param point The point where the direction is requested.
     */
    QVector2D getDirectionAt(QVector2D point) const;

    /**
     * Checks if the given @p point fulfills the criterion to start
     * a streamline based on the given maximum spacing to other
     * streamlines.
     * @param point The point where the direction is requested.
     * @param spacing The point where the direction is requested.
     * @return True if no other streamline is too closeby.
     */
    bool checkSamplePoint(QVector2D point, float spacing);

    /**
     * Converts the given point to the internal grid going from [0,0] to
     * [lonExtent,latExtent].
     */
    QVector2D coordsToGrid(QVector2D point) const;

    /**
     * Called for a finished streamline.
     * @param buffer The list of points making up the streamline.
     */
    void streamlineFinished(const QVector<QVector2D>& buffer);

    /**
     * This method updates the cell field with a streamline given in the buffer.
     * @param buffer The list of points making up the streamline.
     */
    void updateCellPoints(const QVector<QVector2D>& buffer);

    /**
     * Check if operations on the @c point would access a missing value.
     * @param point The point to check.
     * @return True if a missing value is encountered in any of the fields.
     */
    bool checkIfMissingValue(QVector2D point) const;

    /**
     * Get the position along the streamline at a distance @p value from the
     * start of the streamline.
     * @param streamline The streamline.
     * @param value The distance from the start of the streamline to obtain.
     * @return The position at distance @p value along the @p streamline.
     */
    QVector2D streamlineAt(const QVector<QVector2D>& streamline, float value) const;

    /**
     * Fixes the streamlines with respect to the data boundaries.
     * For example, a streamline crossing -180/180E will be split at this
     * position into multiple separate streamlines.
     */
    void cutStreamlinesAtBoundaries() const;

    /**
     * Checks if a loop in the streamline stored in @p buffer can be detected
     * if @p point was added. A loop is detected if the distance between a point
     * in @p buffer and @p point is smaller than half of the @c stepSize
     * @param buffer The streamline in question.
     * @param point The new point to check.
     * @return True if the new point would lead to a loop.
     */
    bool detectLoop(const QVector<QVector2D>& buffer, QVector2D point) const;

    /**
     * Checks if any point given in @p buffer is closer to @p point based
     * on the given squared distance. Optionally takes a range of indices
     * to check within the buffer.
     * @param buffer The buffer of points.
     * @param start Start index in the buffer.
     * @param end End index in the buffer.
     * @param point The point to compare the buffer to.
     * @param maxDistSquared The squared distance threshold.
     * @return True if any points in the buffer is closer than the given distance
     * to the point.
    */
    static bool isAnyPointWithinDistanceOf(const QVector<QVector2D> &buffer,
                                           int start, int end,
                                           const QVector2D& point, float maxDistSquared);
    static bool isAnyPointWithinDistanceOf(const QVector<QVector2D>& buffer,
                                           const QVector2D& point, float maxDistSquared);

    // The grids for the computation.
    MStructuredGrid* uGrid;
    MStructuredGrid* vGrid;
    MStructuredGrid* projCorrGrid;

    // Computation settings.
    MStreamBarbsSettings::SeedingMode seedingMode;
    float slicePos_hPa;
    float seedSpacing;
    // Factor of seed spacing how close streamlines can approach each other.
    float minimumAllowedDistanceFactor;
    float stepSize;
    bool loopDetection;
    int maxLength;

    // Internal computation parameters.
    float regionSizeAdjustedSpacing;

    // Flattened cartesian grid with each entry pointing towards a list of
    // (lon,lat) points, which represent stream line vertices within that cell.
    QVector<QVector<QVector2D>> cellPoints;
    int cellPointsNumLats, cellPointsNumLons;

    // Boundaries of the bounding box (W,E), (S,N).
    QVector2D lonBoundariesDegrees, latBoundariesDegrees;

    // The result of the computation.
    MStreamBarbsData* result;
};
}

#endif //MSTREAMBARBSCOMPUTATION_H
