/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "horizontalstreambarbsdatasource.h"

// standard library imports

// related third party imports

// local application imports
#include "streambarbscomputation.h"
#include "streambarbssettings.h"
#include "gxfw/nwpmultivaractor.h"


namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MHorizontalStreamBarbsDataSource::MHorizontalStreamBarbsDataSource()
    : MScheduledDataSource(),
      uVar(nullptr),
      vVar(nullptr),
      vectorCorrForProjectionGrid(nullptr)
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MDataRequest MHorizontalStreamBarbsDataSource::buildDataRequest
(const MStreamBarbsSettings *settings, const MSyncControl *syncControl,
 float slice_hPa, const QString& projId) const
{
    MDataRequestHelper rh;

    MDataRequestHelper uVarRequest(uVar->buildDataRequest());
    MDataRequestHelper vVarRequest(vVar->buildDataRequest());

    uVarRequest.addKeyPrefix("SB_UVAR_");
    vVarRequest.addKeyPrefix("SB_VVAR_");

    rh.unite(uVarRequest);
    rh.unite(vVarRequest);

    // Request specific level if 3-D, otherwise just set a constant level.
    if (uVar->levelType != SINGLE_LEVEL)
    {
        rh.insert("SB_SLICE_HPA", QString::number(slice_hPa));
    }
    else
    {
        rh.insert("SB_SLICE_HPA", 0.0);
    }

    // Add computation information from settings and variables.
    rh.insert("SB_SEEDING_MODE",
              MStreamBarbsSettings::seedingModeToString(settings->seedingMode));
    rh.insert("SB_LON_WIND_COMPONENT", uVar->variableName);
    rh.insert("SB_LAT_WIND_COMPONENT", vVar->variableName);
    rh.insert("SB_SPACING", QString::number(settings->spacingProp));
    rh.insert("SB_DIFFERENCE_SPACING",
              QString::number(settings->differenceSpacingProp));
    rh.insert("SB_STEP_SIZE", QString::number(settings->stepSizeProp));
    rh.insert("SB_LOOP_DETECTION", settings->loopDetectionProp);
    rh.insert("SB_MAX_LENGTH", settings->maxStreambarbLengthProp.value());

    // Add projection identifier.
    if (vectorCorrForProjectionGrid)
    {
        rh.insert("SB_PROJ_STRING", projId);
    }
    else
    {
        rh.insert("SB_PROJ_STRING", "");
    }
    return rh.request();
}


bool MHorizontalStreamBarbsDataSource::canRequestData(
    const MSyncControl *syncControl) const
{
    if (! isInitialized() || syncControl == nullptr) return false;

    // Check whether both init and valid times are available.
    if (! uVar->getAvailableInitTimes().contains(syncControl->initDateTime()) ||
        ! vVar->getAvailableInitTimes().contains(syncControl->initDateTime()))
    {
        return false;
    }
    if (! uVar->getAvailableValidTimes().contains(syncControl->validDateTime()) ||
        ! vVar->getAvailableValidTimes().contains(syncControl->validDateTime()))
    {
        return false;
    }
    return true;
}


void MHorizontalStreamBarbsDataSource::setNWPVariables(
    MNWPActorVariable *uVar, MNWPActorVariable *vVar)
{
    this->uVar = uVar;
    this->vVar = vVar;
    if (! uVar || ! vVar) return;

    registerInputSource(uVar->dataSource, "SB_UVAR_");
    registerInputSource(vVar->dataSource, "SB_VVAR_");

    // Get memory manager from one of the sources.
    if (! memoryManager && uVar->dataSource)
        setMemoryManager(uVar->dataSource->getMemoryManager());
}


void MHorizontalStreamBarbsDataSource::setVectorCorrForProjMapsGrid(
    MStructuredGrid *grid)
{
    vectorCorrForProjectionGrid = grid;
}


bool MHorizontalStreamBarbsDataSource::isInitialized() const
{
    return (uVar != nullptr && vVar != nullptr);
}


MStreamBarbsData *MHorizontalStreamBarbsDataSource::produceData(
    MDataRequest request)
{
    assert(isInitialized());
    MDataRequestHelper rh(request);

    QVector<MDataRequestHelper> vars = { rh.subRequest("SB_UVAR_"),
                                         rh.subRequest("SB_VVAR_") };

    // Build fitting requests to get the requested source u/v data.
    MStructuredGrid *uGrid = uVar->dataSource->getData(vars[0].request());
    MStructuredGrid *vGrid = vVar->dataSource->getData(vars[1].request());

    // Create computation object and populate it with the info from the request.
    auto *streamlineComputation = new MStreamBarbsComputation();

    QString seedingModeStr = rh.value("SB_SEEDING_MODE");
    float level = rh.floatValue("SB_SLICE_HPA");
    float spacing = rh.floatValue("SB_SPACING");
    float differenceSpacing = rh.floatValue("SB_DIFFERENCE_SPACING");
    float stepSize = rh.floatValue("SB_STEP_SIZE");
    bool enableLoopDetection = rh.intValue("SB_LOOP_DETECTION") != 0;
    int maxLength = rh.intValue("SB_MAX_LENGTH");

    streamlineComputation->setU(uGrid);
    streamlineComputation->setV(vGrid);

    streamlineComputation->setSeedingMode(
        MStreamBarbsSettings::stringToSeedingMode(seedingModeStr));
    streamlineComputation->setSlicePos_hPa(level);
    streamlineComputation->setSpacing(spacing);
    streamlineComputation->setDifferenceSpacing(differenceSpacing);
    streamlineComputation->setStepSize(stepSize);
    streamlineComputation->setLoopDetection(enableLoopDetection);
    streamlineComputation->setMaximumLength(maxLength);

    streamlineComputation->correctVectorsInProjection(
        vectorCorrForProjectionGrid);

    // Trigger the streambarbs computation.
    streamlineComputation->compute();

    uVar->dataSource->releaseData(uGrid);
    vVar->dataSource->releaseData(vGrid);

    return streamlineComputation->getResult();
}


MTask *MHorizontalStreamBarbsDataSource::createTaskGraph(MDataRequest request)
{
    assert(isInitialized());

    // Create a new task.
    MDataRequestHelper rh(request);
    MTask *task = new MTask(request, this);

    QVector<MDataRequestHelper> vars = { rh.subRequest("SB_UVAR_"),
                                         rh.subRequest("SB_VVAR_") };

    rh.removeAll(locallyRequiredKeys());

    task->addParent(uVar->dataSource->getTaskGraph(vars[0].request()));
    task->addParent(vVar->dataSource->getTaskGraph(vars[1].request()));

    return task;
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MHorizontalStreamBarbsDataSource::locallyRequiredKeys()
{
    return QStringList() << "SB_SEEDING_MODE"
                         << "SB_LON_WIND_COMPONENT"
                         << "SB_LAT_WIND_COMPONENT"
                         << "SB_SLICE_HPA"
                         << "SB_SPACING"
                         << "SB_DIFFERENCE_SPACING"
                         << "SB_STEP_SIZE"
                         << "SB_LOOP_DETECTION"
                         << "SB_PROJ_STRING"
                         << "SB_MAX_LENGTH";
}

}; // namespace Met3D
