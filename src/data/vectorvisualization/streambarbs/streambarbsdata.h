/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSTREAMBARBSDATA_H
#define MSTREAMBARBSDATA_H

// standard library imports

// related third party imports

// local application imports
#include "streambarbssettings.h"
#include "data/abstractdataitem.h"


namespace Met3D
{

class MStreamBarbsComputation;

/**
 * This class encapsulates the streambarb data objects that are a result of a
 * @c MStreamCarbsComputation execution. These are memory managed, and contain
 * besides the streamlines and barbs themselves also required rendering
 * information and objects.
 */
class MStreamBarbsData : public MAbstractDataItem
{
public:
    /**
     * Creates a new (empty) data object.
     */
    MStreamBarbsData();

    /**
     * Add a streamline to the result.
     * @param streamline The streamline to add.
     */
    void addStreamline(const QVector<QVector2D> &streamline);

    /**
     * Get the streamlines currently stored in the object.
     * @return Streamlines as list, where each streamline is a list of 2-D pts.
     */
    QVector<QVector<QVector2D>>& getStreamlines();

    /**
     * Set the streamlines of the streambarbs.
     * @param streamlines The list of streamlines.
     */
    void setStreamlines(const QVector<QVector<QVector2D>>& streamlines);

    /**
     * Add a vertex to the vertex buffer for streamlines.
     * @param vertex The vertex to add.
     */
    void addStreamlineVertex(QVector3D vertex);

    /**
     * Get the vertices making up the streamlines, represented as line segments.
     * @return The buffer containing the vertices.
     */
    QVector<QVector3D> &getStreamlineVertices();

    /**
     * Add a vertex to the vertex buffer for barbs.
     * @param vertex The vertex to add.
     */
    void addBarbsVertex(QVector3D vertex);

    /**
     * Get the vertices making up the barbs.
     * @return The buffer containing the vertices.
     */
    QVector<QVector3D> &getBarbsVertices();

    /**
     * Computes and returns the memory size of this object
     * @return the memory size in kb.
     */
    unsigned int getMemorySize_kb() override;

    /**
     * Set the minimum knots in the data.
     * @param minKnots The speed in knots.
     */
    void setMinDataKnots(float minKnots);

    /**
     * Set the maximum knots in the data.
     * @param maxKnots The speed in knots.
     */
    void setMaxDataKnots(float maxKnots);

    /**
     * Get the min speed in the data in knots.
     * @return The min speed.
     */
    float getMinDataKnots() const;

    /**
     * Get the max speed in the data in knots.
     * @return The max speed.
     */
    float getMaxDataKnots() const;

    /**
     * Set the computation source that generated this result. This gets invoked
     * when only some of the data has to be recomputed. This also gives
     * ownership, so the computation source is deleted once the result is
     * deleted.
     * @param source A pointer to the computation source.
     */
    void setComputationSource(std::unique_ptr<MStreamBarbsComputation> source);

    /**
     * Update the barb vertices based on the given settings. Barb width and
     * spacing is used for the update.
     * @param settings The current settings.
     */
    void updateStreambarbsBarbVertexData(MStreamBarbsSettings* settings) const;

    /**
     * Regenerate the streamline vertex buffer based on the current state of
     * the object.
     */
    void updateStreamlineVertexData() const;

    /**
     * Clears the barb vertex buffer.
     */
    void clearBarbsVertices();

    /**
     * Clears the streamline vertex buffer.
     */
    void clearStreamlineVertices();

    /**
     * @return True if the data object is empty.
     */
    bool isEmpty() const;

    /**
     * Checks whether the vertices of the streambarbs have been computed alredy.
     * @return True if they are computed.
     */
    bool areVerticesComputed() const;

private:
    // The list of streamlines.
    QVector<QVector<QVector2D>> streamlines;

    // 3-D vertex buffers.
    QVector<QVector3D> streamlineVertexData;
    QVector<QVector3D> barbsVertexData;

    // Min and max wind speed in the data.
    float minDataKnots, maxDataKnots;

    std::unique_ptr<MStreamBarbsComputation> computationSource;
};
}


#endif //MSTREAMBARBSDATA_H
