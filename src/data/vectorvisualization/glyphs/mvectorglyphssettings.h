/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MVECTORGLYPHSSETTINGS_H
#define MVECTORGLYPHSSETTINGS_H

// standard library imports

// related third party imports

// local application imports
#include "actors/transferfunction1d.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mcolorproperty.h"


namespace Met3D
{
class MNWPHorizontalSectionActor;

/**
 * This class encapsualtes the settings available to the StreamBarbs rendering
 * in 2-D sections.
 */
class MVectorGlyphsSettings
{
public:
    MVectorGlyphsSettings(MNWPHorizontalSectionActor *hostActor);

    void loadConfigurationPrior_V_1_14(QSettings* settings);

    /**
     * @param property The property to check.
     * @return True if the given @p property is one of the properties handled
     * by the StreamBarbs.
     */
    bool isProperty(const MProperty* property) const;

    /**
     * Compute the delta (lat and lon) between adjacent glyphs for the given scene
     * based on the current settings. If automatic scaling is turned on, the delta
     * depends on the distance of the camera to the horizontal slice height.
     * @param sceneView The scene view.
     * @param slice_hPa The slice height.
     * @return The optimal glyph delta to use.
     */
    float computeGlyphDeltaForScene(MSceneViewGLWidget* sceneView, float slice_hPa) const;

    MBoolProperty enabledProp;

    MProperty appearanceGroupProp;

    enum class GlyphType
    {
        Invalid = -1,
        WindBarbs = 0,
        Arrows = 1,
        Arrows3D = 2
    };

    enum class PivotPos
    {
        Invalid = -1,
        Tip = 0,
        Centre = 1,
        Tail = 2
    };

    GlyphType glyphType;
    MEnumProperty glyphTypeProp;

    PivotPos pivotPosition;
    MEnumProperty pivotPositionProp;

    MFloatProperty lineWidth;

    MColorProperty color;

    MProperty windBarbsGroupProp;
    MBoolProperty showCalmGlyphsProp;
    MIntProperty pennantTiltProp;

    MProperty arrowsGroupProp;
    MDoubleProperty arrowHeadHeightProp;
    MBoolProperty scaleArrowWithMagnitudeProp;
    MDoubleProperty scaleArrowMinScalarProp;
    MDoubleProperty scaleArrowMinLengthProp;
    MBoolProperty scaleArrowDiscardBelowProp;
    MDoubleProperty scaleArrowMaxScalarProp;
    MDoubleProperty scaleArrowMaxLengthProp;
    MBoolProperty scaleArrowDiscardAboveProp;

    MBoolProperty arrowDrawOutlineProp;
    MDoubleProperty arrowOutlineWidthProp;
    MColorProperty arrowOutlineColourProp;

    MBoolProperty arrows3DShadowEnabledProp;

    MFloatProperty deltaGlyphsLonLatProp;
    // Glyph delta for currently cached glyphs.
    float computedDelta;

    MBoolProperty clampDeltaGlyphsToGridProp;
    MBoolProperty automaticScalingEnabledProp;

    MProperty scalingGroupProp;

    MFloatProperty reduceFactorProp;

    MFloatProperty reduceSlopeProp;

    MFloatProperty sensitivityProp;
};
}

#endif //MVECTORGLYPHSSETTINGS_H
