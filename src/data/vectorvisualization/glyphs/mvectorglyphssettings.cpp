/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mvectorglyphssettings.h"

// standard library imports

// related third party imports

// local application imports
#include "actors/nwphorizontalsectionactor.h"

namespace Met3D {

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/
MVectorGlyphsSettings::MVectorGlyphsSettings(MNWPHorizontalSectionActor *hostActor) :
      glyphType(GlyphType::WindBarbs),
      pivotPosition(PivotPos::Tip),
      computedDelta(-1)
{
    enabledProp = MBoolProperty("Vector glyphs", false);
    enabledProp.setConfigKey("vector_glyphs_enabled");
    enabledProp.setConfigGroup("Windbarbs");
    enabledProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);

    appearanceGroupProp = MProperty("Appearance");
    enabledProp.addSubProperty(appearanceGroupProp);

    QStringList glyphTypes;
    glyphTypes << "Wind barbs" << "Arrows" << "3D arrows";
    glyphTypeProp = MEnumProperty("Glyph type", glyphTypes, int(glyphType));
    glyphTypeProp.setConfigKey("glyph_type");
    glyphTypeProp.registerValueCallback([=]()
    {
        glyphType = static_cast<GlyphType>(glyphTypeProp.value());
        hostActor->emitActorChangedSignal();
    });
    appearanceGroupProp.addSubProperty(glyphTypeProp);

    QStringList pivotPositions;
    pivotPositions << "Glyph tip" << "Glyph centre" << "Glyph tail";
    pivotPositionProp = MEnumProperty("Pivot position", pivotPositions,
                                      int(pivotPosition));
    pivotPositionProp.setConfigKey("pivot_position");
    appearanceGroupProp.addSubProperty(pivotPositionProp);

    lineWidth = MFloatProperty("Line width", 0.04);
    lineWidth.setConfigKey("line_width");
    lineWidth.setMinMax(0.001, 0.30);
    lineWidth.setDecimals(3);
    lineWidth.setStep(0.001);
    lineWidth.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    appearanceGroupProp.addSubProperty(lineWidth);

    color = MColorProperty("Glyph colour", QColor(0, 0, 127));
    color.setConfigKey("glyph_colour");
    color.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    appearanceGroupProp.addSubProperty(color);

    windBarbsGroupProp = MProperty("Wind barbs");
    appearanceGroupProp.addSubProperty(windBarbsGroupProp);

    showCalmGlyphsProp = MBoolProperty("Calm glyphs", false);
    showCalmGlyphsProp.setConfigKey("calm_glyphs_enabled");
    showCalmGlyphsProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    windBarbsGroupProp.addSubProperty(showCalmGlyphsProp);

    pennantTiltProp = MIntProperty("Pennant tilt", 9);
    pennantTiltProp.setConfigKey("pennant_tilt");
    pennantTiltProp.setMinMax(1, 20);
    pennantTiltProp.setStep(1);
    pennantTiltProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    windBarbsGroupProp.addSubProperty(pennantTiltProp);

    arrowsGroupProp = MProperty("Arrows");
    appearanceGroupProp.addSubProperty(arrowsGroupProp);

    arrowHeadHeightProp = MDoubleProperty("Head size", 0.08);
    arrowHeadHeightProp.setConfigKey("head_size");
    arrowHeadHeightProp.setMinMax(0.001, 0.30);
    arrowHeadHeightProp.setDecimals(3);
    arrowHeadHeightProp.setStep(0.001);
    arrowHeadHeightProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    arrowsGroupProp.addSubProperty(arrowHeadHeightProp);

    scaleArrowWithMagnitudeProp = MBoolProperty("Scale with magnitude", true);
    scaleArrowWithMagnitudeProp.setConfigKey("scale_with_magnitude");
    scaleArrowWithMagnitudeProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    arrowsGroupProp.addSubProperty(scaleArrowWithMagnitudeProp);

    scaleArrowMinScalarProp = MDoubleProperty("Min. magnitude", 0.);
    scaleArrowMinScalarProp.setConfigKey("min_magnitude");
    scaleArrowMinScalarProp.setMinimum(0.);
    scaleArrowMinScalarProp.setDecimals(2);
    scaleArrowMinScalarProp.setStep(1);
    scaleArrowMinScalarProp.registerValueCallback([=]()
    {
        scaleArrowMaxScalarProp.setMinimum(scaleArrowMinScalarProp);
        hostActor->emitActorChangedSignal();
    });
    scaleArrowWithMagnitudeProp.addSubProperty(scaleArrowMinScalarProp);

    scaleArrowMinLengthProp = MDoubleProperty("Min. length", 0.1);
    scaleArrowMinLengthProp.setConfigKey("min_length");
    scaleArrowMinLengthProp.setMinimum(0.001);
    scaleArrowMinLengthProp.setDecimals(3);
    scaleArrowMinLengthProp.setStep(0.001);
    scaleArrowMinLengthProp.registerValueCallback([=]()
    {
        scaleArrowMaxLengthProp.setMinimum(scaleArrowMinLengthProp);
        hostActor->emitActorChangedSignal();
    });
    scaleArrowWithMagnitudeProp.addSubProperty(scaleArrowMinLengthProp);

    scaleArrowDiscardBelowProp = MBoolProperty("Discard below minimum", true);
    scaleArrowDiscardBelowProp.setConfigKey("discard_below_min");
    scaleArrowDiscardBelowProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    scaleArrowWithMagnitudeProp.addSubProperty(scaleArrowDiscardBelowProp);

    scaleArrowMaxScalarProp = MDoubleProperty("Max. magnitude", 50.);
    scaleArrowMaxScalarProp.setConfigKey("max_magnitude");
    scaleArrowMaxScalarProp.setDecimals(2);
    scaleArrowMaxScalarProp.setStep(1);
    scaleArrowMaxScalarProp.setMinimum(scaleArrowMinScalarProp);
    scaleArrowMaxScalarProp.registerValueCallback([=]()
    {
        scaleArrowMinScalarProp.setMaximum(scaleArrowMaxScalarProp);
        hostActor->emitActorChangedSignal();
    });
    scaleArrowWithMagnitudeProp.addSubProperty(scaleArrowMaxScalarProp);

    scaleArrowMinScalarProp.setMaximum(scaleArrowMaxScalarProp);

    scaleArrowMaxLengthProp = MDoubleProperty("Max. length", .75);
    scaleArrowMaxLengthProp.setConfigKey("max_length");
    scaleArrowMaxLengthProp.setMinMax(scaleArrowMinLengthProp, 2.);
    scaleArrowMaxLengthProp.setDecimals(3);
    scaleArrowMaxLengthProp.setStep(0.001);
    scaleArrowMaxLengthProp.registerValueCallback([=]()
    {
        scaleArrowMinLengthProp.setMaximum(scaleArrowMaxLengthProp);
        hostActor->emitActorChangedSignal();
    });
    scaleArrowWithMagnitudeProp.addSubProperty(scaleArrowMaxLengthProp);

    scaleArrowMinLengthProp.setMaximum(scaleArrowMaxLengthProp);

    scaleArrowDiscardAboveProp = MBoolProperty("Discard above maximum", false);
    scaleArrowDiscardAboveProp.setConfigKey("discard_above_max");
    scaleArrowDiscardAboveProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    scaleArrowWithMagnitudeProp.addSubProperty(scaleArrowDiscardAboveProp);

    arrowDrawOutlineProp = MBoolProperty("Outline of 2D arrows", true);
    arrowDrawOutlineProp.setConfigKey("2d_arrows_outline_enabled");
    arrowDrawOutlineProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    arrowsGroupProp.addSubProperty(arrowDrawOutlineProp);

    arrowOutlineWidthProp = MDoubleProperty("Outline width", 0.05);
    arrowOutlineWidthProp.setConfigKey("outline_width");
    arrowOutlineWidthProp.setMinMax(0.001, 0.30);
    arrowOutlineWidthProp.setDecimals(3);
    arrowOutlineWidthProp.setStep(0.001);
    arrowOutlineWidthProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    arrowDrawOutlineProp.addSubProperty(arrowOutlineWidthProp);

    arrowOutlineColourProp = MColorProperty("Outline colour", QColor(0, 0, 0));
    arrowOutlineColourProp.setConfigKey("outline_colour");
    arrowOutlineColourProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    arrowDrawOutlineProp.addSubProperty(arrowOutlineColourProp);

    arrows3DShadowEnabledProp = MBoolProperty("3D arrows shadow", true);
    arrows3DShadowEnabledProp.setConfigKey("3d_arrows_shadow_enabled");
    arrows3DShadowEnabledProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    arrowsGroupProp.addSubProperty(arrows3DShadowEnabledProp);

    automaticScalingEnabledProp = MBoolProperty("Automatic scaling", true);
    automaticScalingEnabledProp.setConfigKey("automatic_scaling");
    automaticScalingEnabledProp.registerValueCallback([=]()
    {
        deltaGlyphsLonLatProp.setEnabled(!automaticScalingEnabledProp);
        clampDeltaGlyphsToGridProp.setEnabled(!automaticScalingEnabledProp);
        hostActor->emitActorChangedSignal();
    });
    enabledProp.addSubProperty(automaticScalingEnabledProp);

    deltaGlyphsLonLatProp = MFloatProperty("Glyph distance", 1.);
    deltaGlyphsLonLatProp.setConfigKey("glyph_distance_deg");
    deltaGlyphsLonLatProp.setSuffix(" °");
    deltaGlyphsLonLatProp.setMinMax(0.05, 45.);
    deltaGlyphsLonLatProp.setStep(0.01);
    deltaGlyphsLonLatProp.setDecimals(2);
    deltaGlyphsLonLatProp.setTooltip(
                "Manually specify the distance between the vector glyphs. If "
                "the distance if below grid point spacing, nearest-neighbour "
                "interpolation is used.");
    deltaGlyphsLonLatProp.setEnabled(! automaticScalingEnabledProp);
    deltaGlyphsLonLatProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    enabledProp.addSubProperty(deltaGlyphsLonLatProp);

    clampDeltaGlyphsToGridProp = MBoolProperty("Restrict glyph distance to grid", true);
    clampDeltaGlyphsToGridProp.setConfigKey("restrict_glyph_distance_to_grid");
    clampDeltaGlyphsToGridProp.setTooltip(
                "If enabled the manually set glyph distance cannot be smaller "
                "than the grid point spacing of the vector field.");
    clampDeltaGlyphsToGridProp.setEnabled(! automaticScalingEnabledProp);
    clampDeltaGlyphsToGridProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    enabledProp.addSubProperty(clampDeltaGlyphsToGridProp);

    scalingGroupProp = MProperty("Scaling");
    enabledProp.addSubProperty(scalingGroupProp);

    reduceFactorProp = MFloatProperty("Scaling factor", 15.0f);
    reduceFactorProp.setConfigKey("scaling_factor");
    reduceFactorProp.setMinMax(1., 400.);
    reduceFactorProp.setDecimals(1);
    reduceFactorProp.setStep(0.1);
    reduceFactorProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    scalingGroupProp.addSubProperty(reduceFactorProp);

    reduceSlopeProp = MFloatProperty("Scaling slope", 0.0175f);
    reduceSlopeProp.setConfigKey("scaling_slope");
    reduceSlopeProp.setMinMax(0.001, 1.0);
    reduceSlopeProp.setDecimals(4);
    reduceSlopeProp.setStep(0.0001);
    reduceSlopeProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    scalingGroupProp.addSubProperty(reduceSlopeProp);

    sensitivityProp = MFloatProperty("Scaling sensitivity", 1.0f);
    sensitivityProp.setConfigKey("scaling_sensitivity");
    sensitivityProp.setMinMax(1., 200);
    sensitivityProp.setDecimals(1);
    sensitivityProp.setStep(1);
    sensitivityProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    scalingGroupProp.addSubProperty(sensitivityProp);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MVectorGlyphsSettings::loadConfigurationPrior_V_1_14(QSettings* settings)
{
    settings->beginGroup("Windbarbs");

    enabledProp = settings->value("enabled", false).toBool();
    automaticScalingEnabledProp = settings->value("automatic", false).toBool();
    glyphTypeProp = settings->value("glyphType",
                                    int(GlyphType::WindBarbs)).toInt();
    pivotPositionProp = settings->value("pivotPosition", int(PivotPos::Tip)).toInt();
    lineWidth = settings->value("lineWidth", 0.04).toFloat();
    pennantTiltProp =  settings->value("pennantTilt", 9).toInt();
    color = settings->value("color", QColor(0,0,127)).value<QColor>();
    showCalmGlyphsProp = settings->value("showCalmGlyphs", false).toBool();
    arrowHeadHeightProp = settings->value("arrowHeadHeight", 0.08).toDouble();
    arrowDrawOutlineProp = settings->value("arrowDrawOutline", true).toBool();
    arrowOutlineWidthProp = settings->value("arrowOutlineWidth", 0.05).toDouble();
    arrowOutlineColourProp = settings->value("arrowOutlineColour",
                                             QColor(0, 0, 0)).value<QColor>();
    scaleArrowWithMagnitudeProp = settings->value("scaleArrowWithMagnitude", true).toBool();
    scaleArrowMinScalarProp = settings->value("scaleArrowMinScalar", 0.).toDouble();
    scaleArrowMinLengthProp = settings->value("scaleArrowMinLength", 0.1).toDouble();
    scaleArrowDiscardBelowProp = settings->value("scaleArrowDiscardBelow", true).toBool();
    scaleArrowMaxScalarProp = settings->value("scaleArrowMaxScalar", 50.).toDouble();
    scaleArrowMaxLengthProp = settings->value("scaleArrowMaxLength", .75).toDouble();
    scaleArrowDiscardAboveProp = settings->value("scaleArrowDiscardAbove", false).toBool();
    arrows3DShadowEnabledProp = settings->value("drawShadow", true).toBool();
    deltaGlyphsLonLatProp = settings->value("deltaBarbsLonLat", 1.).toFloat();
    clampDeltaGlyphsToGridProp = settings->value("clampDeltaBarbsToGrid", true).toBool();
    reduceFactorProp = settings->value("reduceFactor", 15.).toFloat();
    reduceSlopeProp = settings->value("reduceSlope", 0.0175).toFloat();
    sensitivityProp = settings->value("sensitivity", 1.).toFloat();

    settings->endGroup(); // Windbarbs = Vector Glyphs
}


bool MVectorGlyphsSettings::isProperty(const MProperty *property) const
{
    return property == &scaleArrowMinScalarProp
             || property == &scaleArrowMinLengthProp
             || property == &scaleArrowMaxScalarProp
             || property == &scaleArrowMaxLengthProp
             || property == &enabledProp
             || property == &glyphTypeProp
             || property == &pivotPositionProp
             || property == &automaticScalingEnabledProp
             || property == &lineWidth
             || property == &pennantTiltProp
             || property == &color
             || property == &showCalmGlyphsProp
             || property == &arrowHeadHeightProp
             || property == &arrowDrawOutlineProp
             || property == &arrowOutlineWidthProp
             || property == &arrowOutlineColourProp
             || property == &scaleArrowWithMagnitudeProp
             || property == &scaleArrowDiscardBelowProp
             || property == &scaleArrowDiscardAboveProp
             || property == &arrows3DShadowEnabledProp
             || property == &deltaGlyphsLonLatProp
             || property == &clampDeltaGlyphsToGridProp
             || property == &reduceFactorProp
             || property == &reduceSlopeProp
             || property == &sensitivityProp;
}


float MVectorGlyphsSettings::computeGlyphDeltaForScene(MSceneViewGLWidget* sceneView, float slice_hPa) const
{
    float delta;

    // Get the scale and glyph delta to be used for this frame's glyphs.
    if (!automaticScalingEnabledProp)
    {
        delta = deltaGlyphsLonLatProp;
    }
    else
    {
        // Compute automatic glyph scaling.
        // Define view ray.
        const QVector3D rayDir = sceneView->getSceneCamera()->getZAxis();
        const QVector3D rayOrig = sceneView->getSceneCamera()->getOrigin();

        // Define horizontal plane.
        const QVector3D planeNormal(0, 0, 1);
        float worldZ = static_cast<float>(sceneView->worldZfromPressure(slice_hPa));
        const float D = -worldZ;

        // Compute intersection point between ray and plane.
        const float s = -(planeNormal.z() * rayOrig.z() + D) /
                        (planeNormal.z() * rayDir.z());
        const QVector3D P = rayOrig + s * rayDir;
        float t = (P - rayOrig).length();

        // Quantize distance.
        const float step = sensitivityProp;
        t = step * std::floor(t / step);

        // Try to handle camera distance to glyphs via logistical function.
        const float c = reduceFactorProp;
        const float b = reduceSlopeProp;
        const float a = c - 1;
        float newScale = c / (1.0f + a * std::exp(-b * (t)));

        // Quantize scale to get only power-of-two scales.
        newScale = std::pow(2, std::floor(std::log2(newScale) + 0.5));
        newScale = clamp(newScale, 1.0f, 1024.0f);

        delta = newScale / 4.f;
    }
    return delta;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

}