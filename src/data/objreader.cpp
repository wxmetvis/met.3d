/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Alexander Klassen
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "objreader.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "data/lrumemorymanager.h"
#include "gxfw/mglresourcesmanager.h"
#include "util/mutil.h"
#include "util/metroutines.h"
#include "gxfw/msceneviewglwidget.h"

namespace Met3D
{

MObjReader::MObjReader()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager *memoryManager = sysMC->getMemoryManager("NWP");
    if (!memoryManager)
    {
        LOG4CPLUS_ERROR(mlog, "Could not initialize OBJ reader, no NWP memory manager found.");
    }
    setMemoryManager(memoryManager);
}


MMesh *MObjReader::produceData(MDataRequest request)
{
    MDataRequestHelper rh(request);

    QString filename = rh.value("FILENAME");
    if (filename.isEmpty())
    {
        LOG4CPLUS_ERROR(mlog, "Given OBJ filename does not exist. "
                              "Cannot read file.");
        return nullptr;
    }

    // Read obj file.
    std::shared_ptr<MObjMesh> objMesh = readObj(filename);

    if (objMesh == nullptr)
    {
        return nullptr;
    }

    // Read attributes from optional .txt file.
    QString attFile = rh.value("ATTRIBUTE_FILE");
    if (!attFile.isEmpty())
    {
        readAttributes(attFile, objMesh);
    }

    // Convert our obj mesh to our MMesh object.
    MMesh *mesh = convertToMMesh(objMesh);

    int convertMToHPa = rh.value("CONVERT_Z_M_TO_HPA").toInt();
    int convertHPaTpZ = rh.value("CONVERT_Z_P_TO_Z").toInt();
    double meterScale = rh.value("METER_SCALE").toDouble();
    QStringList args = rh.value("P_TO_Z_PARAMS").split("/");
    double log_pBottom_hPa  = args[0].toDouble();
    double deltaZ_deltaLogP = args[1].toDouble();

    // Only convert m to pressure, then to z.
    if (convertMToHPa)
    {
        // Convert vertices
        for (QVector3D& vertex : mesh->vertices)
        {
            vertex.setZ(static_cast<float>(metre2pressure_standardICAO(vertex.z() * meterScale) / 100.0));
            vertex.setZ(static_cast<float>(MSceneViewGLWidget::worldZfromPressure(
                    vertex.z(), log_pBottom_hPa, deltaZ_deltaLogP)));
        }
    }

    // If we convert from HPa to z.
    if (convertHPaTpZ)
    {
        // Convert vertices
        for (QVector3D& vertex : mesh->vertices)
        {
            vertex.setZ(static_cast<float>(MSceneViewGLWidget::worldZfromPressure(
                    vertex.z(), log_pBottom_hPa, deltaZ_deltaLogP)));
        }
    }

    // Generate normals
    if (mesh->normals.isEmpty())
    {
        LOG4CPLUS_INFO(mlog, "No normals found on mesh, generating new ones.");
        generateSmoothNormals(mesh);
    }
    else
    {
        LOG4CPLUS_INFO(mlog, "Mesh already contains normals, using them instead"
                             " of generating new ones.");
    }

    return mesh;
}


MTask *MObjReader::createTaskGraph(MDataRequest request)
{
    // No dependencies, so we create a plain task.
    auto *task = new MTask(request, this);
    // However, this task accesses the hard drive.
    task->setDiskReaderTask();

    QString filePath = MDataRequestHelper(request).value("FILENAME");
    QString fileName = QFileInfo(filePath).fileName();
    task->setProcessingLabel("Reading file '" + fileName + "'...");
    
    return task;
}


const QStringList MObjReader::locallyRequiredKeys()
{
    return {"FILENAME", "CONVERT_Z_M_TO_HPA", "CONVERT_Z_P_TO_Z",
            "METER_SCALE", "ATTRIBUTE_FILE", "P_TO_Z_PARAMS"};
}


std::shared_ptr<MObjReader::MObjMesh> MObjReader::readObj(const QString &path)
{
    LOG4CPLUS_INFO(mlog, "Reading model from OBJ file " << path.toStdString() << "...");

    QFile file(path);
    if (!file.open(QIODevice::ReadOnly))
    {
        LOG4CPLUS_ERROR(mlog, "Could not read OBJ file " << path.toStdString());
        return nullptr;
    }

    auto mesh = std::make_shared<MObjMesh>();

    QTextStream in(&file);

    while (!in.atEnd())
    {
        QString line = in.readLine();
        if (line.startsWith("vn")) // Vertex normal
        {
            QStringList fields = line.split(" ");
            QVector3D vector(fields[1].toFloat(),
                             fields[2].toFloat(),
                             fields[3].toFloat());

            mesh->vn.append(vector);
        }
        else if (line.startsWith("vt")) // Vertex texture coordinate
        {
            QStringList fields = line.split(" ");
            QVector2D vector(fields[1].toFloat(),
                             fields[2].toFloat());

            mesh->vt.append(vector);
        }
        else if (line.startsWith('v')) // Vertex
        {
            QStringList fields = line.split(" ");
            QVector3D vector(fields[1].toFloat(),
                             fields[2].toFloat(),
                             fields[3].toFloat());

            mesh->v.append(vector);
        }
        else if (line.startsWith('f')) // Face
        {
            QStringList fields = line.split(" ");
            fields.removeAt(0); // Remove 'f' at the beginning

            MObjFace face;
            for (const QString& corner : fields)
            {
                if (corner.isEmpty()) continue;

                QStringList indicesStr = corner.split('/');
                QVector<int> indices;
                for (const QString& indexStr : indicesStr)
                {
                    if (indexStr.isEmpty()) // No index for this attribute given
                    {
                        indices.append(-1);
                        continue;
                    }

                    bool ok = false;
                    int index = indexStr.toInt(&ok);

                    if (ok)
                    {
                        indices.append(index - 1);
                    }
                    else
                    {
                        indices.append(-1);
                    }
                }

                // Fill remaining indices with -1,
                // so that length is always 3.
                for (int i = indices.length(); i < 3; i++)
                {
                    indices.append(-1);
                }

                face.indices.append(indices);
            }

            mesh->f.append(face);
        }
    }

    file.close();

    LOG4CPLUS_INFO(mlog, "Successfully read OBJ file " << path.toStdString());

    return mesh;
}


void MObjReader::readAttributes(const QString &attributeFile, const std::shared_ptr<MObjMesh>& obj)
{
    LOG4CPLUS_INFO(mlog, "Reading attributes from txt file " << attributeFile.toStdString() << "...");

    QFile attFileObject(attributeFile);
    if (attFileObject.open(QIODevice::ReadOnly))
    {
        QTextStream reader(&attFileObject);

        // The AMIRA ASCII file attributes start with @n.
        // This is a crude implementation that should be replaced some time.
        bool foundStart = false;
        while (!reader.atEnd())
        {
            QString line = reader.readLine();
            if (line == "@1")
            {
                foundStart = true;
                continue;
            }
            if (!foundStart)
            {
                continue;
            }

            bool ok;
            double val = line.toDouble(&ok);

            if (ok)
            {
                obj->attr.append(static_cast<float>(val));
            }
        }

        attFileObject.close();

        LOG4CPLUS_INFO(mlog, "Finished reading attributes.");
    }
    else
    {
        LOG4CPLUS_ERROR(mlog, "Could not read attributes. Could not open file.");
        return;
    }

    if (obj->attr.length() != obj->v.length())
    {
        LOG4CPLUS_ERROR(mlog, "The amount of attributes does not match the amount of vertices. Discarding them...");
        obj->attr.clear();
    }
}


MMesh *MObjReader::convertToMMesh(const std::shared_ptr<MObjMesh>& obj)
{
    LOG4CPLUS_INFO(mlog, "Converting OBJ mesh to internal format...");
    auto *mesh = new MMesh();

    QMap<QVector<int>, int> visited;

    int currentIndex = 0;
    for (const MObjFace& face : obj->f)
    {
        // Restructure face corners, so that we get triangles.
        // This allows the reader to load meshes with quads etc.
        QVector<QVector<int>> triangulatedIndices;

        // Loop over all face indices, create triangles from
        // 0, i, i+1. If i+1 doesn't exist, we have finished.
        for (int i = 1; i < face.indices.length() - 1; i++)
        {
            triangulatedIndices.append(face.indices[0]);
            triangulatedIndices.append(face.indices[i]);
            triangulatedIndices.append(face.indices[i + 1]);
        }

        // Loop over all corners and get the indices for it.
        // The indices are all 3 elements long.
        // [0] is vertex index, [1] is uv index and [2] is normal index.
        for (const QVector<int>& indices : triangulatedIndices)
        {
            int i;
            if (visited.contains(indices))
            {
                i = visited[indices];
            }
            else // New index, add vertex, uv, attribute and normal to mesh.
            {
                i = currentIndex++;

                // Add vertex and vertex attribute to mesh.
                if (indices[0] != -1 && !obj->v.isEmpty())
                {
                    mesh->vertices.append(obj->v[indices[0]]);

                    if (!obj->attr.isEmpty())
                    {
                        mesh->vertexAttributes.append(obj->attr[indices[0]]);
                    }
                }

                // Add texture coordinates to mesh.
                if (indices[1] != -1 && !obj->vt.isEmpty())
                {
                    mesh->textureCoords.append(obj->vt[indices[1]]);
                }

                // Add normal to mesh.
                if (indices[2] != -1 && !obj->vn.isEmpty())
                {
                    mesh->normals.append(obj->vn[indices[2]]);
                }

                visited[indices] = i;
            }
            // Add index to mesh.
            mesh->indices.append(i);
        }
    }

    LOG4CPLUS_INFO(mlog, "Finished converting OBJ mesh to internal format.");

    return mesh;
}


void MObjReader::generateSmoothNormals(MMesh *mesh)
{
    LOG4CPLUS_INFO(mlog, "Generating smooth normals for the mesh...");

    // Reset normals.
    mesh->normals.clear();

    // Resize normals to fit vertices, so that we can
    // access them via vertex index.
    mesh->normals.resize(mesh->vertices.size());

    // Generate face normals and add them to the vertex normal list for
    // each face vertex.
    for (int i = 0; i < mesh->indices.length(); i += 3)
    {
        /** Polygon:
                 a
                / \
               b - c
        */

        int idxA = mesh->indices[i];
        int idxB = mesh->indices[i + 1];
        int idxC = mesh->indices[i + 2];

        QVector3D a = mesh->vertices[idxA];
        QVector3D b = mesh->vertices[idxB];
        QVector3D c = mesh->vertices[idxC];

        // Calculate normal.
        QVector3D normal = QVector3D::crossProduct(b - a, c - a);

        // Normalize normal vector.
        normal.normalize();

        mesh->normals[idxA] += normal;
        mesh->normals[idxB] += normal;
        mesh->normals[idxC] += normal;
    }

    // Normalize the normals for each vertex.
    for(QVector3D& normal : mesh->normals)
    {
        normal.normalize();
    }

    LOG4CPLUS_INFO(mlog, "Finished generating smooth normals for the mesh.");
}


} // namespace Met3D
