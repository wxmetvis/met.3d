/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2024 Marc Rautenhaus [*, previously +]
**  Copyright 2020-2023 Andreas Beckert [*]
**  Copyright 2017 Michael Kern [+]
**  Copyright 2024      Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "partialderivativefilter.h"

// standard library imports
#include "assert.h"

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "gxfw/nwpactorvariableproperties.h"
#include "util/mutil.h"
#include "util/mexception.h"
#include "util/metroutines.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPartialDerivativeFilter::MPartialDerivativeFilter()
        : MSingleInputProcessingWeatherPredictionDataSource()
{

}
/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MStructuredGrid *MPartialDerivativeFilter::produceData(Met3D::MDataRequest request)
{
    assert(inputSource != nullptr);

    MDataRequestHelper rh(request);
    // Parse request.
    // Examples: DERIVATIVE=D/LON
    QStringList parameterList = rh.value("PARTIALDERIVATIVE").split("/");
    rh.removeAll(locallyRequiredKeys());
    // The first parameter passes the filter type.
    MPartialDerivativeProperties::PartialDerivativeModeTypes filterType =
            static_cast<MPartialDerivativeProperties::PartialDerivativeModeTypes>(
                parameterList[0].toInt());

    MStructuredGrid* inputGrid = inputSource->getData(rh.request());

    MStructuredGrid *resultGrid = createAndInitializeResultGrid(inputGrid);

    QString partialDerivativeModeName = MPartialDerivativeProperties::partialDerivativeModeToString(
                filterType);
    LOG4CPLUS_INFO(mlog, "Partial derivative filter: computing "
                    << partialDerivativeModeName);

    QString levelType = MStructuredGrid::verticalLevelTypeToString(
                inputGrid->getLevelType());
    LOG4CPLUS_DEBUG(mlog, "Vertical level type:"
                    << levelType);

    switch (filterType)
    {
    case MPartialDerivativeProperties::DLON:
    {
        resultGrid->addProcessingInformation("_variable_prefix", "eastward_derivative_of_");
        computePartialDerivativeLongitude(inputGrid, resultGrid);
        computePressureCoordinateCorrectionLongitude(inputGrid, resultGrid);
        break;
    }
    case MPartialDerivativeProperties::DLON_LAGRANTO:
    {
        resultGrid->addProcessingInformation("_variable_prefix", "eastward_derivative_of_");
        resultGrid->addProcessingInformation("partial_derivative_mode", "lagranto");
        callLibcalvarPartialDerivativeRoutine(inputGrid, resultGrid,
                                               "lon");
        break;
    }

    case MPartialDerivativeProperties::DLAT:
    {
        resultGrid->addProcessingInformation("_variable_prefix", "northward_derivative_of_");
        computePartialDerivativeLatitude(inputGrid, resultGrid);
        computePressureCoordinateCorrectionLatitude(inputGrid, resultGrid);
        break;
    }
    case MPartialDerivativeProperties::DLAT_LAGRANTO:
    {
        resultGrid->addProcessingInformation("_variable_prefix", "northward_derivative_of_");
        resultGrid->addProcessingInformation("partial_derivative_mode", "lagranto");
        callLibcalvarPartialDerivativeRoutine(inputGrid, resultGrid,
                                               "lat");
        break;
    }
    case MPartialDerivativeProperties::DP:
    {
        resultGrid->addProcessingInformation("_variable_prefix", "upward_derivative_of_");
        computePartialDerivativeVertical(inputGrid, resultGrid);
        break;
    }
    case MPartialDerivativeProperties::MAG_OF_HORIZ_GRAD:
    {
        resultGrid->addProcessingInformation("_variable_prefix",
                                             "magnitude_of_horizontal_gradient_of_");
        computeMagnitudeOfHorizontalGradient(inputGrid, resultGrid);
        break;
    }
    case MPartialDerivativeProperties::DLON_OF_MAG_OF_HORIZ_GRAD:
    {
        resultGrid->addProcessingInformation("_variable_prefix",
                                             "eastward_derivative_of_magnitude_of_horizontal_gradient_of_");
        computePartialDerivativeLongitudeOfMagnitudeOfHorizontalGradient(
            inputGrid, resultGrid);
        break;
    }
    case MPartialDerivativeProperties::DLAT_OF_MAG_OF_HORIZ_GRAD:
    {
        resultGrid->addProcessingInformation("_variable_prefix",
                                             "northward_derivative_of_magnitude_of_horizontal_gradient_of_");
        computePartialDerivativeLatitudeOfMagnitudeOfHorizontalGradient(
            inputGrid, resultGrid);
        break;
    }
    case MPartialDerivativeProperties::MAG_OF_HORIZ_GRAD_OF_MAG_OF_HORIZ_GRAD:
    {
        resultGrid->addProcessingInformation("_variable_prefix",
                                             "magnitude_of_horizontal_gradient_of_magnitude_of_horizontal_gradient_of_");
        computeMagnitudeOfHorizontalGradientOfMagnitudeOfHorizontalGradient(
            inputGrid, resultGrid);
        break;
    }

    default:
        LOG4CPLUS_WARN(mlog, "This partial derivative filter does not exist: "
                        << partialDerivativeModeName);
    }
    inputSource->releaseData(inputGrid);

    return resultGrid;
}


MTask *MPartialDerivativeFilter::createTaskGraph(MDataRequest request)
{
    assert(inputSource != nullptr);
    MTask* task = new MTask(request, this);
    // Simply request the variable that was requested from this data source
    MDataRequestHelper rh(request);

    // Parse request.
    // Examples: DERIVATIVE=D/LON
    QStringList parameterList = rh.value("PARTIALDERIVATIVE").split("/");
    rh.removeAll(locallyRequiredKeys());
    // The first parameter passes the filter type.
    MPartialDerivativeProperties::PartialDerivativeModeTypes filterType =
            static_cast<MPartialDerivativeProperties::PartialDerivativeModeTypes>(
                parameterList[0].toInt());
    QString partialDerivativeModeName = MPartialDerivativeProperties::partialDerivativeModeToString(
                filterType);
    LOG4CPLUS_TRACE(mlog, "Partial derivative filter: request "
                    << partialDerivativeModeName);
    task->addParent(inputSource->getTaskGraph(rh.request()));
    return task;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MPartialDerivativeFilter::locallyRequiredKeys()
{
    return (QStringList() << "PARTIALDERIVATIVE");
}


void MPartialDerivativeFilter::computePartialDerivativeLongitude(
        MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    const int nLon = inputGrid->getNumLons();
    const int nLat = inputGrid->getNumLats();
    const int nLev = inputGrid->getNumLevels();
    // compute flags for pole and periodic treatment
    const QList<bool> periodicBC = periodicBoundaryTreatment(inputGrid);
#pragma omp parallel for
    for (int k = 0; k < nLev; k++)
    {
        int iP, iN;
        double dx;
        double dfdx;
        for (int j = 0; j < nLat; j++)
        {
            dx = inputGrid->getDeltaLon_km(j);
            for (int i = 0; i < nLon; i++)
            {
                iP = std::max(i - 1, 0);
                iN = std::min(i + 1, nLon - 1);
                dfdx = (inputGrid->getValue(k, j, iN)
                      - inputGrid->getValue(k, j, iP))
                      / (dx * double(abs(iN - iP)));
                resultGrid->setValue(k, j, i, dfdx);
            }
            if (periodicBC[0])
            {
                dfdx = (inputGrid->getValue(k, j, 1)
                      - inputGrid->getValue(k, j, nLon - 1)) / (2 * dx);
                resultGrid->setValue(k, j, 0, dfdx);
                dfdx = (inputGrid->getValue(k, j, 0)
                      - inputGrid->getValue(k, j, nLon - 2))  / (2 * dx);
                resultGrid->setValue(k, j, nLon - 1, dfdx);
            }
        }
    }
}


void MPartialDerivativeFilter::computePartialDerivativeLatitude(
        MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    const int nLon = inputGrid->getNumLons();
    const int nLat = inputGrid->getNumLats();
    const int nLev = inputGrid->getNumLevels();
    const double dy = inputGrid->getDeltaLat_km();
    // compute flags for pole and periodic treatment
    const QList<bool> periodicBC = periodicBoundaryTreatment(inputGrid);

#pragma omp parallel for
    for (int k = 0; k < nLev; k++)
    {
        int jP, jN, iOpp;
        double dfdy;
        for (int j = 0; j < nLat; j++)
        {
            jP = std::max(j - 1, 0);
            jN = std::min(j + 1, nLat - 1);
            for (int i = 0; i < nLon; i++)
            {
                dfdy = (inputGrid->getValue(k, jN, i)
                      - inputGrid->getValue(k, jP, i))
                      / (dy * double(abs(jN - jP)));
                resultGrid->setValue(k, j, i, -dfdy);
            }
        }
        if (periodicBC[1])
        {
            for (int i = 0; i < nLon; i++)
            {
                iOpp = ((i + (nLon / 2)) % (nLon));
                dfdy = (inputGrid->getValue(k, 1, i)
                    - inputGrid->getValue(k, 0, iOpp)) / (2 * dy);
                resultGrid->setValue(k, 0, i, -dfdy);
            }
        }
        if (periodicBC[2])
        {
            for (int i = 0; i < nLon; i++)
            {
                iOpp = ((i + (nLon / 2)) % (nLon));
                dfdy = (inputGrid->getValue(k, nLat - 1, iOpp)
                    - inputGrid->getValue(k, nLat -  2, i)) / (2 * dy);
                resultGrid->setValue(k, nLat - 1, i, -dfdy);
            }
        }
    }
}


void MPartialDerivativeFilter::computePartialDerivativeVertical(
        MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    const int nLon = inputGrid->getNumLons();
    const int nLat = inputGrid->getNumLats();
    const int nLev = inputGrid->getNumLevels();

#pragma omp parallel for
    for (int k = 0; k < nLev; k++)
    {
        int kP, kN;
        double df, dp;
        for (int j = 0; j < nLat; j++)
        {
            for (int i = 0; i < nLon; i++)
            {
                kP = std::max(k - 1, 0);
                kN = std::min(k + 1, nLev - 1);
                df = inputGrid->getValue(kN, j, i)
                        - inputGrid->getValue(kP, j, i);
                dp = inputGrid->getPressure(kN, j, i)
                        - inputGrid->getPressure(kP, j, i);
                resultGrid->setValue(k, j, i, df / dp);
            }
        }
    }
}


void MPartialDerivativeFilter::computePartialDerivativePressureLongitude(
            MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    const int nLon = inputGrid->getNumLons();
    const int nLat = inputGrid->getNumLats();
    const int nLev = inputGrid->getNumLevels();
    // compute flags for pole and periodic treatment
    const QList<bool> periodicBC = periodicBoundaryTreatment(inputGrid);

#pragma omp parallel for
    for (int k = 0; k < nLev; k++)
    {
        int iP, iN;
        double dx;
        double dpdx;
        for (int j = 0; j < nLat; j++)
        {
            dx = inputGrid->getDeltaLon_km(j);
            for (int i = 0; i < nLon; i++)
            {
                iP = std::max(i - 1, 0);
                iN = std::min(i + 1, nLon - 1);
                dpdx = (inputGrid->getPressure(k, j, iN)
                      - inputGrid->getPressure(k, j, iP))
                      / (dx * double(abs(iN - iP)));
                resultGrid->setValue(k, j, i, dpdx);
            }
            if (periodicBC[0])
            {
                dpdx = (inputGrid->getPressure(k, j, 1)
                      - inputGrid->getPressure(k, j, nLon - 1)) / (2 * dx);
                resultGrid->setValue(k, j, 0, dpdx);
                dpdx = (inputGrid->getPressure(k, j, 0)
                      - inputGrid->getPressure(k, j, nLon - 2))  / (2 * dx);
                resultGrid->setValue(k, j, nLon - 1, dpdx);
            }
        }
    }
}


void MPartialDerivativeFilter::computePartialDerivativePressureLatitude(
        MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    const int nLon = inputGrid->getNumLons();
    const int nLat = inputGrid->getNumLats();
    const int nLev = inputGrid->getNumLevels();
    const double dy = inputGrid->getDeltaLat_km();
    // compute flags for pole and periodic treatment
    const QList<bool> periodicBC = periodicBoundaryTreatment(inputGrid);

#pragma omp parallel for
    for (int k = 0; k < nLev; k++)
    {
        double dpdy;
        int jP, jN, iOpp;
        for (int j = 0; j < nLat; j++)
        {
            jP = std::max(j - 1, 0);
            jN = std::min(j + 1, nLat - 1);
            for (int i = 0; i < nLon; i++)
            {
                dpdy = (inputGrid->getPressure(k, jN, i)
                      - inputGrid->getPressure(k, jP, i))
                      / (dy * double(abs(jN - jP)));
                resultGrid->setValue(k, j, i, dpdy);
            }
        }
        if (periodicBC[1])
        {
            for (int i = 0; i < nLon; i++)
            {
                iOpp = ((i + (nLon / 2)) % (nLon));
                dpdy = (inputGrid->getPressure(k, 1, i)
                    - inputGrid->getPressure(k, 0, iOpp)) / (2 * dy);
                resultGrid->setValue(k, 0, i, dpdy);
            }
        }
        if (periodicBC[2])
        {
            for (int i = 0; i < nLon; i++)
            {
                iOpp = ((i + (nLon / 2)) % (nLon));
                dpdy = (inputGrid->getPressure(k, nLat - 1, iOpp)
                    - inputGrid->getPressure(k, nLat -  2, i)) / (2 * dy);
                resultGrid->setValue(k, nLat - 1, i, dpdy);
            }
        }
    }
}


void MPartialDerivativeFilter::computePressureCoordinateCorrectionLongitude(
        MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    MStructuredGrid *dfdp = createAndInitializeResultGrid(inputGrid);
    MStructuredGrid *dpdx = createAndInitializeResultGrid(inputGrid);


    const int nLon = inputGrid->getNumLons();
    const int nLat = inputGrid->getNumLats();
    const int nLev = inputGrid->getNumLevels();

    computePartialDerivativeVertical(inputGrid, dfdp);
    computePartialDerivativePressureLongitude(inputGrid, dpdx);

#pragma omp parallel for
    for (int k = 0; k < nLev; k++)
    {
        double dfdxPGrid;
        for (int j = 0; j < nLat; j++)
        {
            for (int i = 0; i < nLon; i++)
            {
                dfdxPGrid = resultGrid->getValue(k, j, i)
                        - dfdp->getValue(k, j, i) * dpdx->getValue(k, j, i);
                resultGrid->setValue(k, j, i, dfdxPGrid);
            }
        }
    }
    delete dfdp;
    delete dpdx;
}


void MPartialDerivativeFilter::computePressureCoordinateCorrectionLatitude(
        MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    MStructuredGrid *dfdp = createAndInitializeResultGrid(inputGrid);
    MStructuredGrid *dpdy = createAndInitializeResultGrid(inputGrid);

    const int nLon = inputGrid->getNumLons();
    const int nLat = inputGrid->getNumLats();
    const int nLev = inputGrid->getNumLevels();

    computePartialDerivativeVertical(inputGrid, dfdp);
    computePartialDerivativePressureLatitude(inputGrid, dpdy);

#pragma omp parallel for
    for (int k = 0; k < nLev; k++)
    {
        double dfdyPGrid;
        for (int j = 0; j < nLat; j++)
        {
            for (int i = 0; i < nLon; i++)
            {
                dfdyPGrid = resultGrid->getValue(k, j, i)
                        + dfdp->getValue(k, j, i) * dpdy->getValue(k, j, i);
                resultGrid->setValue(k, j, i, dfdyPGrid);
            }
        }
    }
    delete dfdp;
    delete dpdy;
}


// NO PERIODIC BOUNDARY THREATMENT
void MPartialDerivativeFilter::callLibcalvarPartialDerivativeRoutine(
        MStructuredGrid *inputGrid, MStructuredGrid *resultGrid,
        QString direction)
{
    LOG4CPLUS_INFO(mlog, "You are using the libcalvar LAGRANTO library. "
                         << "This library is implemented for testing purpose only. "
                         << "No periodic boundary treatment!");
    // Cast the input grid to hybrid sigma pressure grid to access ak/bk
    // coefficients; convert the float arrays in MLonLatHybridSigmaPressureGrid
    // to float arrays.
    MLonLatHybridSigmaPressureGrid *hybridInputGrid =
            dynamic_cast<MLonLatHybridSigmaPressureGrid*>(inputGrid);

    // This method uses the LAGRANTO.ECMWF libcalvar function "ddh3" to
    // compute partial derivatives. To call the FORTRAN function "ddh3",
    // the data contained in the MStructuredGrid classes needs to be
    // restructured:
    // * libcalvar requires float arrays that contain the full 3D variable
    //   fields.
    // * libcalvar requires the lat dimension to be reversed (in increasing
    //   lat order) in all spatial fields.
    // * surface pressure needs to be passed in hPa.
    // * ak and bk coefficients need to be passed as float arrays.
    // * dps surface pressure partial derivative needs to be precomputed and passed in
    //   hPa
    // Also compare to libcalvar usage in ppecmwf.py in the met.dp repository.

    // Grid sizes.
    int nlev = inputGrid->getNumLevels();
    int nlat = inputGrid->getNumLats();
    int nlon = inputGrid->getNumLons();
    int nlatnlon = nlat*nlon;

    // Convert surface pressure from Pa to hPa; reverse lat dimension.
    float *psfc_hPa_revLat = new float[nlat*nlon];
    for (int j = 0; j < nlat; j++)
        for (int i = 0; i < nlon; i++)
        {
            psfc_hPa_revLat[INDEX2yx(j, i, nlon)] =
                    hybridInputGrid->getSurfacePressure(nlat-1-j, i) / 100.;
        }


    float *ak_hPa_float = new float[nlev];
    float *bk_float = new float[nlev];
    for (int k = 0; k < nlev; k++)
    {
        ak_hPa_float[k] = hybridInputGrid->getAkCoeff(k);
        bk_float[k] = hybridInputGrid->getBkCoeff(k);
    }


    // compute the inputGrid with reverse lat dimension as a for "ddh3"
    float *a_revLat = new float[nlev*nlat*nlon];
    for (int k = 0; k < nlev; k++)
        for (int j = 0; j < nlat; j++)
            for (int i = 0; i < nlon; i++)
            {
                a_revLat[INDEX3zyx_2(k, j, i, nlatnlon, nlon)] =
                        inputGrid->getValue(k, nlat-1-j, i);

            }

    // Compute 2D field of cos(lat). Reverse lat
    // dimension.
    float *coslat_revLat = new float[nlat*nlon];
    for (int j = 0; j < nlat; j++)
        for (int i = 0; i < nlon; i++)
        {
            coslat_revLat[INDEX2yx(j, i, nlon)] =
                    cos(inputGrid->getLats()[nlat-1-j] / 180. * M_PI);
        }


    // "ddh3" requires two 4-element vectors that contain the lon/lat range.
    float *varmin = new float[4];
    varmin[0] = inputGrid->getLons()[0]; // min lon
    varmin[1] = inputGrid->getLats()[nlat-1]; // min lat
    varmin[2] = 0.;
    varmin[3] = 0.;
    float *varmax = new float[4];
    varmax[0] = inputGrid->getLons()[nlon-1]; // max lon
    varmax[1] = inputGrid->getLats()[0]; // max lat
    varmax[2] = 0.;
    varmax[3] = 0.;

    char dir;
    float df;
    float dx, dy;
    int i, j;


    // compute "dps" surface pressure partial derivative according to the
    // chosen direction and convert pressure from Pa to hPa
    // convert direction to an integer, reverse lat and direction
    // of latitudinal partial derivative
    float *dps_revLat = new float[nlat*nlon];
    if (direction == "lon")
    {
        dir = 'X';
        for (j = 0; j < nlat; j++)
        {
            // convert to meter
            dx = inputGrid->getDeltaLon_km(nlat-1-j) * 1000;
            // central differences
            for (i = 1; i < nlon - 1; i++)
            {
                df = (hybridInputGrid->getSurfacePressure(nlat-1-j, i + 1)
                        - hybridInputGrid->getSurfacePressure(nlat-1-j, i - 1))
                        / (2. * 100.);
                dps_revLat[INDEX2yx(j, i, nlon)] = df / dx;
            };
            // forward differences for the first index
            i = 0;
            df = (hybridInputGrid->getSurfacePressure(nlat-1-j, i + 1)
                    - hybridInputGrid->getSurfacePressure(nlat-1-j, i))
                    / (1. * 100.);
            dps_revLat[INDEX2yx(j, i, nlon)] = df / dx;
            // backward differences for the last index
            i = nlon - 1;
            df = (hybridInputGrid->getSurfacePressure(nlat-1-j, i)
                    - hybridInputGrid->getSurfacePressure(nlat-1-j, i - 1))
                    / (1. * 100.);
            dps_revLat[INDEX2yx(j, i, nlon)] = df / dx;
        };
    }
    else if (direction == "lat")
    {
        dir = 'Y';
        // convert to meter
        dy = inputGrid->getDeltaLat_km() * 1000;
        for (i = 0; i < nlon; i++)
        {
            // central differences
            for (j = 1; j < nlat - 1; j++)
            {
                df = (hybridInputGrid->getSurfacePressure(nlat-1-j + 1, i)
                        - hybridInputGrid->getSurfacePressure(nlat-1-j - 1, i))
                        / (2. * 100.);
                dps_revLat[INDEX2yx(j, i, nlon)] = -(df / dy);
            };
            // backward differences for the last index, revers lat
            j = 0;
            df = (hybridInputGrid->getSurfacePressure(nlat - 1, i)
                    - hybridInputGrid->getSurfacePressure(nlat - 2, i))
                    / (1. * 100.);
            dps_revLat[INDEX2yx(j, i, nlon)] = -(df / dy);
            // forward differences for the first index, revers lat
            j = nlat - 1;
            df = (hybridInputGrid->getSurfacePressure(1, i)
                    - hybridInputGrid->getSurfacePressure(0, i))
                    / (1. * 100.);
            dps_revLat[INDEX2yx(j, i, nlon)] = -(df / dy);
        };
    }
    else
    {
        LOG4CPLUS_WARN(mlog, "Upsi, something went wrong! This direction to "
                           << "calculate the partial derivatives does not exists."
                           << "Please try with another direction again.");
        return;
    }

    // Call the "ddh3" LAGRANTO function inside the FORTRAN libcalvar library.
    float *partialD_revLat = new float[nlev*nlat*nlon];
    horizontalPartialDerivative_calvar(
                a_revLat, partialD_revLat, psfc_hPa_revLat, dps_revLat,
                coslat_revLat, dir, nlon, nlat, nlev, varmin, varmax,
                ak_hPa_float, bk_float);

    // Reverse the lat dimension of the computed partial derivative field and store the
    // partial derivative field in the result grid
    // convert the pressure from hPa to Pa and the distance from meter to km (*1000)
    for (int k = 0; k < nlev; k++)
        for (int j = 0; j < nlat; j++)
            for (int i = 0; i < nlon; i++)
            {
                resultGrid->setValue(k, j, i,
                                      partialD_revLat[INDEX3zyx_2(
                            k, nlat-1-j, i, nlatnlon, nlon)] * 1000);
            }


    // Delete temporary memory.
    delete[] psfc_hPa_revLat;
    delete[] ak_hPa_float;
    delete[] bk_float;
    delete[] coslat_revLat;
    delete[] a_revLat;
    delete[] varmin;
    delete[] varmax;
    delete[] partialD_revLat;
    delete[] dps_revLat;
}


QList<bool> MPartialDerivativeFilter::periodicBoundaryTreatment(
        MStructuredGrid *inputGrid)
{
    // compute flags for pole and periodic treatment
    bool southpl = false;
    bool northpl = false;
    bool lonper  = false;
    QList<bool> periodic;
    double phiTotal = inputGrid->getLons()[inputGrid->getNumLons() - 1]
            - inputGrid->getLons()[0]
            + inputGrid->getDeltaLon();
    // periodic boundaries test (x-dir)
    // TODO (ab, Mar2020) test periodic boundary conditions
    if (phiTotal >= 360.)
    {
        lonper = true;
        if ((inputGrid->getLats()[inputGrid->getNumLats() - 1]
             - inputGrid->getDeltaLat()) <= -90.)
        {
            southpl = true;
        }
        if ((inputGrid->getLats()[0] + inputGrid->getDeltaLat()) >= 90)
        {
            northpl = true;
        }
    }
    periodic.append(lonper);
    periodic.append(northpl);
    periodic.append(southpl);
    return periodic;
}


void MPartialDerivativeFilter::computeMagnitudeOfHorizontalGradient(
    MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    MStructuredGrid *dLonResultGrid = createAndInitializeResultGrid(inputGrid);
    MStructuredGrid *dLatResultGrid = createAndInitializeResultGrid(inputGrid);

    // Compute partial derivative along longitudes.
    computePartialDerivativeLongitude(inputGrid, dLonResultGrid);
    computePressureCoordinateCorrectionLongitude(inputGrid, dLonResultGrid);
    // Compute partial derivative along latitudes.
    computePartialDerivativeLatitude(inputGrid, dLatResultGrid);
    computePressureCoordinateCorrectionLatitude(inputGrid, dLatResultGrid);

    // Compute magnitude of output fields.
    computeMagnitude(dLonResultGrid, dLatResultGrid, resultGrid);

    // Delete temporary grids.
    delete dLonResultGrid;
    delete dLatResultGrid;
}


void
MPartialDerivativeFilter::computePartialDerivativeLongitudeOfMagnitudeOfHorizontalGradient(
    MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    // Compute the horizontal gradient.
    MStructuredGrid *horizGradGrid = createAndInitializeResultGrid(inputGrid);
    computeMagnitudeOfHorizontalGradient(inputGrid, horizGradGrid);

    // Compute partial derivative of this gradient along longitudes.
    computePartialDerivativeLongitude(horizGradGrid, resultGrid);
    computePressureCoordinateCorrectionLongitude(horizGradGrid, resultGrid);

    // Delete temporary grids.
    delete horizGradGrid;
}


void
MPartialDerivativeFilter::computePartialDerivativeLatitudeOfMagnitudeOfHorizontalGradient(
    MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    // Compute the horizontal gradient.
    MStructuredGrid *horizGradGrid = createAndInitializeResultGrid(inputGrid);
    computeMagnitudeOfHorizontalGradient(inputGrid, horizGradGrid);

    // Compute partial derivative of this gradient along latitudes.
    computePartialDerivativeLatitude(horizGradGrid, resultGrid);
    computePressureCoordinateCorrectionLatitude(horizGradGrid, resultGrid);

    // Delete temporary grids.
    delete horizGradGrid;
}


void
MPartialDerivativeFilter::computeMagnitudeOfHorizontalGradientOfMagnitudeOfHorizontalGradient(
    MStructuredGrid *inputGrid, MStructuredGrid *resultGrid)
{
    MStructuredGrid *dLonOfHorizGradGrid = createAndInitializeResultGrid(
        inputGrid);
    MStructuredGrid *dLatOfHorizGradGrid = createAndInitializeResultGrid(
        inputGrid);

    // Compute magnitude of horizontal gradient.
    MStructuredGrid *magOfHorizGradGrid = createAndInitializeResultGrid(
        inputGrid);
    computeMagnitudeOfHorizontalGradient(inputGrid, magOfHorizGradGrid);

    // Compute the derivative along longitudes of the magnitude.
    computePartialDerivativeLongitude(magOfHorizGradGrid, dLonOfHorizGradGrid);
    computePressureCoordinateCorrectionLongitude(
        magOfHorizGradGrid, dLonOfHorizGradGrid);

    // Compute the derivative along latitudes of the magnitude.
    computePartialDerivativeLatitude(magOfHorizGradGrid, dLatOfHorizGradGrid);
    computePressureCoordinateCorrectionLatitude(magOfHorizGradGrid,
                                                dLatOfHorizGradGrid);

    // Compute magnitude of output fields.
    computeMagnitude(dLonOfHorizGradGrid, dLatOfHorizGradGrid, resultGrid);

    // Delete temporary grids.
    delete dLonOfHorizGradGrid;
    delete dLatOfHorizGradGrid;
}


void MPartialDerivativeFilter::computeMagnitude(MStructuredGrid *g1,
                                                MStructuredGrid *g2,
                                                MStructuredGrid *output)
{
#pragma omp parallel for
    for (unsigned int k = 0; k < output->getNumLevels(); k++)
    {
        for (unsigned int j = 0; j < output->getNumLats(); j++)
        {
            for (unsigned int i = 0; i < output->getNumLons(); i++)
            {
                double magnitude = 0;

                // sqrt(dLon² + dLat²)
                float dLonValue = g1->getValue(k, j, i);
                float dLatValue = g2->getValue(k, j, i);

                magnitude = std::sqrt(
                    dLonValue * dLonValue + dLatValue * dLatValue);
                output->setValue(k, j, i, magnitude);
            }
        }
    }
}
}  // namespace Met3D
