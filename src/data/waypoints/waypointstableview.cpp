/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2016 Marc Rautenhaus
**  Copyright 2016      Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "waypointstableview.h"
#include "ui_waypointstableview.h"

// standard library imports
#include <iostream>

// related third party imports

// local application imports
#include "util/mfiletypes.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MWaypointsView::MWaypointsView(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MWaypointsView)
{
    ui->setupUi(this);
    ui->tableView->horizontalHeader()->setStretchLastSection(true);
}


MWaypointsView::~MWaypointsView()
{
    delete ui;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MWaypointsView::duplicateWaypoint()
{
    QModelIndex currentIndex = this->ui->tableView->currentIndex();
    int row = currentIndex.isValid() ? currentIndex.row() : 0;
    auto *cmd = new MWaypointsTableModel::InsertWaypointCommand(waypointsModel, row);

    MUndoStack::run(cmd);
}


void MWaypointsView::deleteSelectedWaypoint()
{
    // A minimum of two waypoints need to be retained in the model.
    if (waypointsModel->size() <= 2) return;

    QModelIndex currentIndex = this->ui->tableView->currentIndex();
    if (! currentIndex.isValid()) return;
    int row = currentIndex.row();

    auto *cmd = new MWaypointsTableModel::RemoveWaypointCommand(waypointsModel, row);
    MUndoStack::run(cmd);
}


void MWaypointsView::saveTrack()
{
    if (this->waypointsModel->getFileName().length() == 0)
    {
        saveAsTrack();
    }
    else
    {
        this->waypointsModel->saveToFile(this->waypointsModel->getFileName());
    }
}


void MWaypointsView::saveAsTrack()
{
    QString fileName = MFileUtils::getSaveFileName(
                this, "Save Flight Track", FileTypes::M_FLIGHT_TRACK_CONFIG);
    if (fileName.length() > 0)
        this->waypointsModel->saveToFile(fileName);
}


void MWaypointsView::openTrack()
{
    QString fileName = MFileUtils::getOpenFileName(
                this, "Open Flight Track", FileTypes::M_FLIGHT_TRACK_CONFIG);
    if ( !fileName.isEmpty() )
    {
        auto cmd = new MWaypointsTableModel::LoadFileCommand(waypointsModel, fileName);
        MUndoStack::run(cmd);
    }
}


void MWaypointsView::setWaypointsTableModel(MWaypointsTableModel *model)
{
    waypointsModel = model;
    ui->tableView->setModel(waypointsModel);
    ui->tableView->resizeColumnsToContents();
}

} // namespace Met3D
