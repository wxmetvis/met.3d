/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "selectabledatavariable.h"

#include <utility>

// standard library imports

// related third party imports

// local application imports
#include "weatherpredictiondatasource.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectableDataVariable::MSelectableDataVariable()
    : levelType(SIZE_LEVELTYPES)
{
}


MSelectableDataVariable::MSelectableDataVariable(const QSettings *settings)
{
    // Forecast variable name and data source.
    dataSourceID = settings->value("dataLoaderID").toString();
    levelType = static_cast<MVerticalLevelType>(settings->value("levelType").
        toInt());
    variableName = settings->value("variableName").toString();
    uuid = settings->value("uuid", QUuid()).toUuid();
}


MSelectableDataVariable::MSelectableDataVariable(const QString &dataSourceID,
                                             const MVerticalLevelType levelType,
                                             const QString &varName)
    : dataSourceID(dataSourceID),
      levelType(levelType),
      variableName(varName),
      uuid()
{
}


MSelectableDataVariable::MSelectableDataVariable(const QString &dataSourceID,
                                             const QString &levelType,
                                             const QString &varName)
    : MSelectableDataVariable(dataSourceID,
                            MStructuredGrid::verticalLevelTypeFromString(
                                levelType), varName)
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

bool MSelectableDataVariable::isInSystemManagerAvailable()
{
    static const QString ENS_FILTER_SUFFIX = " ENSFilter";
    if (dataSourceID.endsWith(ENS_FILTER_SUFFIX))
    {
        static const QString errMsg =
            "Loading actor variable from file that was "
            "saved with an older version of Met.3D. "
            "The dataset ID '%1' contains the ' ENSFilter' suffix, "
            "which is no longer used. Removing it. "
            "Please save the actor or session again to avoid "
            "this warning in the future.";

        LOG4CPLUS_WARN(mlog, errMsg.arg(dataSourceID));

        dataSourceID.replace(dataSourceID.length() - ENS_FILTER_SUFFIX.length(),
                             ENS_FILTER_SUFFIX.length(), QString());
    }

    // Check if variable name contains old grib variable suffix.
    static const QString errMsg = "Loading actor variable from file that was "
        "saved with an older version of Met.3D. "
        "The variable name '%1' contains the '%2' suffix, "
        "which is no longer used. Removing it. "
        "Please save the actor or session again to avoid "
        "this warning in the future.";

    for (const QString &suffix : QStringList({" (fc)", " (an)", " (ens)"}))
    {
        if (variableName.endsWith(suffix))
        {
            LOG4CPLUS_WARN(mlog, errMsg.arg(variableName, suffix));

            // Get index of the suffix.
            int idx = variableName.lastIndexOf(suffix);

            variableName.remove(idx, suffix.length());
        }
    }

    // Check if data source and variable are available in the current
    // configuration.
    auto *source = dynamic_cast<MWeatherPredictionDataSource *>(
        MSystemManagerAndControl::getInstance()
        ->getDataSource(dataSourceID));

    bool dataSourceIsAvailable = ((source != nullptr)
                                  && source->availableLevelTypes().contains(
                                      levelType)
                                  && source->availableVariables(levelType).
                                             contains(variableName)
    );
    return dataSourceIsAvailable;
}


bool MSelectableDataVariable::isValid() const
{
    return levelType != MVerticalLevelType::SIZE_LEVELTYPES;
}


QByteArray MSelectableDataVariable::serialize() const
{
    QJsonObject jsonObject;
    jsonObject["dataSourceID"] = dataSourceID;
    jsonObject["levelType"] = levelType;
    jsonObject["variableName"] = variableName;
    jsonObject["uuid"] = uuid.toString();

    return QJsonDocument(jsonObject).toJson();
}


MSelectableDataVariable MSelectableDataVariable::deserialize(
    const QByteArray &byteArray)
{
    QJsonDocument document = QJsonDocument::fromJson(byteArray);
    QJsonObject jsonObject = document.object();

    MSelectableDataVariable selectableDataVariable;

    selectableDataVariable.dataSourceID = jsonObject["dataSourceID"].toString();
    selectableDataVariable.levelType = static_cast<MVerticalLevelType>(jsonObject[
        "levelType"].toInt());
    selectableDataVariable.variableName = jsonObject["variableName"].toString();
    selectableDataVariable.uuid = QUuid(jsonObject["uuid"].toString());
    return selectableDataVariable;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D