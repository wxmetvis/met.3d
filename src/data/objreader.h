/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Alexander Klassen
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_OBJREADER_H
#define MET_3D_OBJREADER_H

// standard library imports
#include <memory>

// related third party imports

// local application imports
#include "scheduleddatasource.h"
#include "mmesh.h"

namespace Met3D {

class MObjReader : public MScheduledDataSource
{
public:

    /**
     * A single face of an @c MObjMesh.
     * Contains the indices of each corner vertex,
     * normal and uv.
     * A face can have more than 3 vertices.
     */
    struct MObjFace
    {
        QVector<QVector<int>> indices;
    };

    /**
     * A simple helper struct to load meshes from
     * obj files. Reflects the obj specification
     * in a simplified manner.
     */
    struct MObjMesh
    {
        QVector<QVector3D> v;
        QVector<QVector3D> vn;
        QVector<QVector2D> vt;
        QVector<MObjFace> f;
        QVector<float> attr;
    };

    MObjReader();

    MMesh* produceData(MDataRequest request) override;
    MTask* createTaskGraph(MDataRequest request) override;
protected:
    const QStringList locallyRequiredKeys() override;

    /**
     * Reads the obj file @p path and outputs it in an @c MObjMesh.
     * @param path The path to the obj file.
     * @return A pointer to the obj mesh object.
     */
    static std::shared_ptr<MObjMesh> readObj(const QString& path);

    /**
     * Reads the attributes from the attribute file @p attributeFile,
     * and puts them into the @c MObjMesh object @p obj.
     * @param attributeFile The path to the attribute file.
     * @param obj The obj mesh object.
     */
    static void readAttributes(const QString& attributeFile, const std::shared_ptr<MObjMesh>& obj);

    /**
     * Converts an @c MObjMesh object to an @c MMesh object for rendering.
     * As obj meshes contain only unique elements for each vertex, normal, uv etc.
     * they need to be converted. OpenGL expects the same amount of
     * vertex, normal and uv elements, which is not guaranteed with obj.
     * This method ensures, that for each combination of vertex, normal and uv,
     * there is a single index exists to access each, allowing OpenGL to render the mesh.
     * @param obj The @c MObjMesh that should be converted.
     * @return The MMesh object, that results from the conversion.
     */
    static MMesh* convertToMMesh(const std::shared_ptr<MObjMesh>& obj);

    /**
     * Generates smooth normals for the given @c MMesh @p mesh.
     * If the mesh has duplicated vertices for sharp edges, the resulting
     * normals will also look flat, as this method does not remove duplicate
     * vertices.
     * @param mesh The mesh to smooth.
     */
    static void generateSmoothNormals(MMesh* mesh);
};

}
#endif //MET_3D_OBJREADER_H
