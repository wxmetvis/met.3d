/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2024 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2023-2024 Thorwin Vogt [*]
**  Copyright 2023-2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef GRIBREADER_H
#define GRIBREADER_H

// standard library imports

// related third party imports
#include <QtCore>
#include "eccodes.h"

// local application imports
#include "data/weatherpredictionreader.h"
#include "data/structuredgrid.h"


namespace Met3D
{

enum MECMWFForecastType
{
    INVALID_TYPE           = 0,
    ANALYSIS               = 1,
    DETERMINISTIC_FORECAST = 2,
    ENSEMBLE_FORECAST      = 3
};

/** Information specific to a variable and timestep of a single grib file */
struct MGribDatafieldFileInfo
{
    /**
     * @brief filename Filename for the file that contains some levels for a variable.
     */
    QString filename;

    /**
     * @brief offsetForLevel byte offset at which the grib message for a given vertical level is stored in this grib file
     */
    QMap<long, long> offsetForLevel;

    MGribDatafieldFileInfo() : filename(""){}
    MGribDatafieldFileInfo(QString fileName) : filename(fileName) {}
};

/** Information specific to a variable and timestep. */
struct MGribDatafieldInfo
{
    QMap<QString, MGribDatafieldFileInfo> dataFiles;// Grib files where data of this datafield lies
    bool applyExp;                                  // indicates whether to apply exponatial
                                                    // function to data field or not.
};

// Define a hierarchy of dictionaries that provide fast access to where a
// specific datafield is stored.
typedef QMap<int, MGribDatafieldInfo> MGribEnsembleMemberMap;
typedef QMap<QDateTime, MGribEnsembleMemberMap> MGribValidTimeMap;
typedef QMap<QDateTime, MGribValidTimeMap> MGribInitTimeMap;

struct MGribVariableInfo
{
    MGribVariableInfo() : nlons(0), nlats(0),
        lon0(0), lat0(0), lon1(0), lat1(0), dlon(0), dlat(0),
        availableMembers_bitfield(0),
        gridIsCyclicInLongitude(false),
        longitudinalIndexShiftForCyclicGrid(0),
        lon0ShiftedReference(0.) {}

    // This struct stores variable-specific information, the hierarchy of
    // maps is continued in the field "timeMap".
    MGribInitTimeMap timeMap;
    QString variablename;        // variable name
    QString longname;            // CF-conventions
    QString standardname;        // CF-conventions
    QString units;               // CF-conventions
    MECMWFForecastType fcType;   // forecast type of this variable
    QString surfacePressureName; // for variables on hybrid model levels
                                 // the name of the var containing the
                                 // corresponding sfc pressure field
    QString auxiliaryPressureName; // for variables with auxiliary pressure
                                   // levels; the name of the var containing the
                                   // corresponding 3D pressure field
    MHorizontalGridType horizontalGridType;  // Enum representing the type of the grid.

    long nlons, nlats;
    double lon0, lat0, lon1, lat1, dlon, dlat;

    // Latitude scan direction of data. Default is south to north with value 0.
    bool jScanPositively;

    // Numbers of vertical levels in general vertical level types.
    double nlev;
    // Number of vertical gird used in general vertical level types.
    double numberOfVGridUsed;
    // UUID of vertical gird used in general vertical level types.
    QVector<uint8_t> uuidOfVGrid;

    QVector<double> levels, lats, lons, aki_hPa, bki, ak_hPa, bk;
    QSet<unsigned int> availableMembers;
    quint64 availableMembers_bitfield;

    // In grib files that contain hemispheric data, different variable may
    // be stored on grids in which the longitudinal dimension is shifted
    // (e.g. some vars on 0..360deg, others on -180..180deg). The following
    // parameters control shifting the grids so that internally all are
    // available on the same grid.
    bool gridIsCyclicInLongitude; // the grid spans entire parallels
    int longitudinalIndexShiftForCyclicGrid; // on data read, shift the lon dimension
    double lon0ShiftedReference; // in case of shift: shift lon to this lon0
};

typedef QMap<QString, MGribVariableInfo*> MGribVariableNameMap;
typedef QMap<MVerticalLevelType, MGribVariableNameMap> MGribLevelTypeMap;

struct MGribFileInfo
{
    FILE *gribFile;

    // Mutex to lock access to the file.
    QMutex accessMutex;
};

typedef QHash<QString, MGribFileInfo*> MGribOpenFileMap;

// Current version for grib index. This is moved here for convenience,
// As manually changing different hardcoded lines of code to update the grib index version
// can introduce errors.
#define GRIB_INDEX_VERSION 4
/**
  Struct that stores a grib message's "important" data fields for the file
  index (the index consists of structs of this type).
 */
struct MGribMessageIndexInfo
{
    MVerticalLevelType levelType;

    QString variablename;        // variable name
    QString longname;            // CF-conventions
    QString standardname;        // CF-conventions
    QString units;               // CF-conventions
    MECMWFForecastType fcType;   // forecast type of this variable
    QString surfacePressureName; // for variables on hybrid model levels
                                 // the name of the var containing the
                                 // corresponding sfc pressure field

    quint64 nlons, nlats;
    double lon0, lat0, lon1, lat1, dlon, dlat;

    QVector<double> lats, lons, aki_hPa, bki, ak_hPa, bk;

    // Latitude scan direction of data.
    bool jScanPositively;

    // Numbers of vertical levels in general vertical level types.
    double nlev;
    // Number of vertical gird used in general vertical level types.
    double numberOfVGridUsed;
    // UUID of vertical gird used in general vertical level types.
    QVector<uint8_t> uuidOfVGrid;

    qint64 ensMember;
    QDateTime initTime;
    QDateTime validTime;

    quint64 level;
    quint64 filePosition;

    void writeToDataStream(QDataStream *dataStream);
    void readFromDataStream(QDataStream *dataStream);
};


/**
  @brief Reader for ECMWF Grib files that are retrieved from the ECMWF MARS
  system (or passed from Metview).

  For 3D fields, Met.3D checks if all required model levels are available for
  all time steps.
  If levels are missing, the corresponding variable is discarded.

  @todo Move availableXYZ methods up in class hierarchy.
  */
class MGribReader : public MWeatherPredictionReader
{
public:
    /**
      The GRIB reader takes an argument @p surfacePressureFieldType that
      specified which surface pressure field is used for reconstruction
      of pressure from hybrid coordinated. Can be "sp", "lnsp" or "auto".
      If set to "auto", the reader tries to detect the available field--this
      unfortunately currently requires scanning through all messages: SLOW...
     */
    MGribReader(QString identifier, QString surfacePressureFieldType, QString consistencyCheckReferenceVars,
                QString auxiliary3DPressureField, bool disableGridConsistencyCheck=false);
    ~MGribReader();

    QList<MVerticalLevelType> availableLevelTypes();

    QStringList availableVariables(MVerticalLevelType levelType);

    QSet<unsigned int> availableEnsembleMembers(MVerticalLevelType levelType,
                                                const QString&     variableName);

    QList<QDateTime> availableInitTimes(MVerticalLevelType levelType,
                                        const QString&     variableName);

    QList<QDateTime> availableValidTimes(MVerticalLevelType levelType,
                                         const QString&     variableName,
                                         const QDateTime&   initTime);

    QString variableLongName(MVerticalLevelType levelType,
                             const QString&     variableName);

    QString variableStandardName(MVerticalLevelType levelType,
                                 const QString&     variableName);

    QString variableUnits(MVerticalLevelType levelType,
                          const QString&     variableName);

protected:
    QString variableSurfacePressureName(MVerticalLevelType levelType,
                                        const QString&     variableName);

    QString variableAuxiliaryPressureName(MVerticalLevelType levelType,
                                          const QString&     variableName);

    MHorizontalGridType variableHorizontalGridType(MVerticalLevelType levelType,
                                                   const QString&     variableName);

    /**
      Helper method for @ref readGrid() to copy lon/lat coordinate data
      into grid objects.
     */
    void copyLonLatCoordinateDataToGridObject(
            MStructuredGrid *grid, MGribVariableInfo* vinfo);

    MStructuredGrid* readGrid(MVerticalLevelType levelType,
                              const QString&     variableName,
                              const QDateTime&   initTime,
                              const QDateTime&   validTime,
                              unsigned int       ensembleMember) override;

    void scanDataRoot() override;

    void createGribFileIndex(const QString &filePath, const QString &fileIndexPath, QStringList &availableFiles,
                             int &ensembleIDFromFile, const QString &gribFileName);

    bool isValidGribFile(QString path);

    QStringList getGribIndexStringKeyList(grib_index* gribIndex, QString key);

    QList<long> getGribIndexLongKeyList(grib_index* gribIndex, QString key);

    QList<double> getGribIndexDoubleKeyList(grib_index* gribIndex, QString key);

    template<typename T> QString keyListToString(QList<T> keyList);

    bool isGribKeyMissing(grib_handle* gh, QString key);

    QString getGribStringKey(grib_handle* gh, QString key);

    long getGribLongKey(grib_handle* gh, QString key);

    QList<quint8> getGribByteArrayKey(grib_handle* gh, const QString& key, size_t length);

    double getGribDoubleKey(grib_handle* gh, QString key);

    void debugPrintLevelTypeMap(MGribLevelTypeMap& m);

    bool checkInternalConsistencyOfVariableIndex(MGribVariableInfo* vinfo);

    QString forecastTypeToString(MECMWFForecastType type);

    /**
     Detect type of surface pressure field used for reconstruction of pressure
     for hybrid sigma-pressure levels. Possible options: surface pressure is
     stored in Pa in a field "sp", or as the logarithm of sp in a field "lnsp".
     To make things complicated, the latter is stored at ECMWF as a single
     model level, sp is stored as a surface field.

     Detection is currently implemented by searching all grib messages for
     either "sp" or "lnsp" fields.

     We need the sp/lnsp information to correctly set the "surfacePressureName"
     variable of hybrid sigma pressure grids. The information is stored in
     the internal variable->grib message mapping as well as in the index files.
     Hence, sp/lnsp currently needs to be detected BEFORE any hybrid grids
     are scanned in @ref scanDataRoot().
     */
    void detectSurfacePressureFieldType(QStringList *availableFiles);


    /**
     Checks consistency of horizontal geographical region data stored in
     @p referenceVInfo and @p currentVInfo.

     Horizontal geographical region data includes start and end lons and lats,
     grid spacing in lon and lat direction, number of lons and lats and one
     vector containing all longitudes and one containing all latitudes.
     */
    bool checkHorizontalConsistencyOfVariableToReference(
            MGribVariableInfo *referenceVInfo,
            MGribVariableInfo *currentVInfo,
            bool determineLonIndexShiftForCyclicGrid=false);

    /**
     * Checks the consistency of vertical grids used for general vertical layer level type variables.
     * They are loaded as auxiliary pressure variables and this method checks @p currentVInfo against the
     * @p auxPressureVInfo for vertical grid consistency.
     * Both variables need to reference the same vertical grid used when they were created.
     */
    bool checkAuxiliaryPressureVerticalGridConsistency(MGribVariableInfo *currentVInfo,
                                                        MGribVariableInfo *auxPressureVInfo);

    /**
     * Check whether the given variable @p varName on the given level type @p levelType has matching levels
     * with the levels of the reference variable. Does compare a sorted copy of the level vectors.
     */
    bool checkLevelConsistency(MVerticalLevelType levelType, QString varName, QVector<double> referenceLevels) const;

    MGribLevelTypeMap availableDataFields;
    MGribLevelTypeMap availableDataFieldsByStdName;
    QReadWriteLock availableItemsLock;

    MGribOpenFileMap openFiles;
    QMutex openFilesMutex;

    QStringList consistencyCheckRefVars;
    bool disableGridConsistencyCheck;
    bool ensembleIDIsSpecifiedInFileName;

    static const QString GRIB_INDEX_FILE_EXT_VERSIONED;
    static const QString GRIB_INDEX_FILE_EXT;
};


} // namespace Met3D

#endif // GRIBREADER_H
