/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "netcdffile.h"

// standard library imports

// related third party imports
#include <netcdf_mem.h>

// local application imports
#include <data/structuredgrid.h>

using namespace Met3D;

namespace netCDF
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNcFile::MNcFile(const QString &path)
    : NcFile(path.toStdString(), NcFile::replace),
      levelType(SIZE_LEVELTYPES)
{
    // Write some global attributes according to CF conventions.
    (void)putAtt("Conventions", "CF-1.11");
    (void)putAtt("source", "Exported by Met.3D " +
                           met3dVersionString.toStdString());
    const QString curTime =
        QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    (void)putAtt("export_time", curTime.toStdString());
}


MNcFile::MNcFile() : NcFile(),
      levelType(SIZE_LEVELTYPES)
{
    int fileID;
    // Create a memory space for a NetCDF file. This functionality is hidden in the C API and
    // not directly exposed to the C++ API. Therefore, we need this wrapper.
    int result = nc_create_mem("dummy", NC_NETCDF4, 4096, &fileID);
    if (result)
    {
        // Could not create a diskless file object. Keep this object invalid.
        return;
    }

    // Make the file valid.
    myId = fileID;
    nullObject = false;

    // Write some global attributes according to CF conventions.
    (void)putAtt("Conventions", "CF-1.11");
    (void)putAtt("source", "Exported by Met.3D " +
                               met3dVersionString.toStdString());
    const QString curTime =
        QDateTime::currentDateTime().toString("yyyy-MM-dd hh:mm:ss");
    (void)putAtt("export_time", curTime.toStdString());
}


MNcFile::MNcFile(NC_memio memio) : NcFile(),
                     levelType(SIZE_LEVELTYPES)
{
    int fileID;
    // Create a memory space for a NetCDF file and open the memory buffer as NetCDF file.
    // This functionality is hidden in the C API and not directly exposed to the C++ API.
    // Therefore, we need this wrapper.
    int result = nc_open_memio("dummy", NC_NETCDF4, &memio, &fileID);

    if (result)
    {
        // Could not create a diskless file object. Keep this object invalid.
        return;
    }

    // Make the file valid.
    myId = fileID;
    nullObject = false;
}


MNcFile::MNcFile(void* memory, size_t size) : NcFile(),
                                              levelType(SIZE_LEVELTYPES)
{
    int fileID;
    // Create a memory space for a NetCDF file and open the memory buffer as NetCDF file.
    // This functionality is hidden in the C API and not directly exposed to the C++ API.
    // Therefore, we need this wrapper.
    int result = nc_open_mem("dummy", NC_NETCDF4, size, memory, &fileID);

    if (result)
    {
        // Could not create a diskless file object. Keep this object invalid.
        return;
    }

    // Make the file valid.
    myId = fileID;
    nullObject = false;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MNcFile *MNcFile::createOrOpenFile(const QString &path)
{
    try
    {
        auto *file = new MNcFile(path);
        return file;
    }
    catch (exceptions::NcException &)
    {
        LOG4CPLUS_ERROR(mlog, "Failed to open NetCDF file '"
                        << path << "', aborting write process.");
        return nullptr;
    }
}


MNcFile *MNcFile::createFromMemory(NC_memio io)
{
    try
    {
        auto *file = new MNcFile(io);
        return file;
    }
    catch (exceptions::NcException &)
    {
        LOG4CPLUS_ERROR(mlog,
                        "Failed to open NetCDF file from memory address " << io.memory << ".");
        return nullptr;
    }
}


MNcFile *MNcFile::createFromMemory(void* memory, size_t size)
{
    try
    {
        // Create a memory struct and locking the memory so no reallocation happens.
        NC_memio memio;
        memio.memory = memory;
        memio.size = size;
        memio.flags = NC_MEMIO_LOCKED;

        auto *file = new MNcFile(memio);
        return file;
    }
    catch (exceptions::NcException &)
    {
        LOG4CPLUS_ERROR(mlog,
                        "Failed to open NetCDF file from memory address " << memory << ".");
        return nullptr;
    }
}


MNcFile *MNcFile::createDisklessFile()
{
    auto *file = new MNcFile();
    if (file->isNull())
    {
        delete file;
        return nullptr;
    }
    return file;
}


bool MNcFile::addStructuredGrid(const MStructuredGrid *grid, NcVar &var)
{
    QString vLevelType = MStructuredGrid::verticalLevelTypeToString(
        grid->getLevelType());

    QString varName = grid->getVariableName();

    // Create data variable for the actor variable.
    if (varName.isEmpty()) varName = "unknown_grid";

    // Add info from data sources along the pipeline.
    QVector<QPair<QString, QString>> procInfos = grid->
        getProcessingInformation();

    for (int procInfIdx = procInfos.size() - 1; procInfIdx >= 0; procInfIdx--)
    {
        QString key = procInfos[procInfIdx].first;
        QString value = procInfos[procInfIdx].second;
        // Rename variable if it has the special prefix.
        if (key == "_variable_prefix")
        {
            varName = value + varName;
            // Consumed, remove it from the map.
            procInfos.remove(procInfIdx);
        }
    }

    // Quietly skip this variable if it is the auxiliary pressure field, but
    // it already exists. A previous 3-D variable might have added it as
    // dependent grid already.
    if (! getVar(varName.toStdString()).isNull())
    {
        if (grid->getLevelType() == AUXILIARY_PRESSURE_3D)
        {
            auto auxGrid = dynamic_cast<const MLonLatAuxiliaryPressureGrid *>(
                grid);
            if (auxGrid->getAuxiliaryPressureFieldGrid() == auxGrid)
            {
                // This is the pressure field and it already exists.
                var = getVar(varName.toStdString());
                return true;
            }
        }
    }

    // This is the variable we are adding.
    LOG4CPLUS_INFO(
        mlog, "Adding grid variable '" << varName
        << "' (" << vLevelType << ") to NetCDF file.");

    // Check if we support the grid type.
    if (! isVerticalGridTypeSupported(grid->getLevelType()))
    {
        LOG4CPLUS_ERROR(mlog, "Writing to NetCDF file: Vertical grid type "
                        << MStructuredGrid::verticalLevelTypeToString(grid->
                            getLevelType()) << " is not supported. Aborting.");
        return false;
    }
    if (grid->getHorizontalGridType() != REGULAR_LONLAT_GRID)
    {
        LOG4CPLUS_ERROR(mlog, "Writing to NetCDF file: Currently only regular "
                        "longitude latitude grids can be saved as NetCDF file. "
                        "Aborting.");
        return false;
    }

    if (! setVerticalLevelType(grid->getLevelType()))
    {
        LOG4CPLUS_ERROR(mlog, "Writing to NetCDF file: Different "
                        "vertical dimension types detected. Make sure the fields to "
                        "write are either 2-D fields or of the same 3-D type.");
        return false;
    }

    std::vector<NcDim> dims = addDimensions(grid);
    if (dims.empty())
    {
        return false;
    }

    if (! getVar(varName.toStdString()).isNull())
    {
        LOG4CPLUS_WARN(mlog, "Writing to NetCDF file: A grid with name "
                        << varName << " should be added, but that name "
                        "already exists. Picking a different one.");
        // Adding a number at the end of the variable name. Use the first free
        // variable name as the new variable name.
        int appendIndex = 0;
        QString tmpVarName;
        do
        {
            appendIndex += 1;
            tmpVarName = varName + QString::number(appendIndex);
        }
        while (! getVar(tmpVarName.toStdString()).isNull());

        LOG4CPLUS_WARN(
            mlog, "Using variable name '" << tmpVarName <<
            "' for variable '" << varName << "'.");
        varName = tmpVarName;
    }

    var = addVar(varName.toStdString(), ncFloat, dims);
    (void)var.putAtt("standard_name", varName.toStdString());

    // And set the data.
    var.setFill(true, M_MISSING_VALUE);
    var.putVar(grid->getData());

    for (auto &procInfo : procInfos)
    {
        // Don't allow spaces in NetCDF attribute keys.
        QString key = procInfo.first.replace(' ', '_');
        QString value = procInfo.second;

        // Handle it as attribute.
        (void)var.putAtt(key.toStdString(), value.toStdString());
    }

    addDependentGrids(grid);
    return true;
}


bool MNcFile::addActorVariable(const MNWPActorVariable *var)
{
    NcVar ncVar;
    if (! addStructuredGrid(var->grid, ncVar))
    {
        return false;
    }

    // Set the unit from the data source, if it is not a protected unit.
    // Protected units contain fields where we convert the units internally in
    // Met.3D, making the original unit from the file pointless.
    if (! variableNamesWithProtectedUnits.contains(
        QString::fromStdString(ncVar.getName())))
    {
        QString units = var->dataSource->variableUnits(
            var->levelType, var->variableName);
        (void)ncVar.putAtt("units", units.toStdString());
    }
    return true;
}


bool MNcFile::addStructuredGrid(const MStructuredGrid *grid)
{
    NcVar dummyVar;
    return addStructuredGrid(grid, dummyVar);
}


void MNcFile::fillStructuredGrid(MStructuredGrid* grid) const
{
    auto *tmpData = new float[grid->getNumValues()];

    // Load from a 4D NetCDF variable (time, vertical, lat, lon).
    std::vector<size_t> start(4); start.assign(4,0);
    start[0] = 0;
    std::vector<size_t> count(4); count.assign(4,1);
    count[1] = grid->getNumLevels();
    count[2] = grid->getNumLats();
    count[3] = grid->getNumLons();

    // Expect data to be ordered as given along the interface
    NcVar gridVar = getVar(grid->getVariableName().toStdString());
    gridVar.getVar(start, count, tmpData);

    // Check whether target grid and netCDF data have opposite orders of lats/levs.
    std::vector<float> lats;
    lats.resize(getVar("latitude").getDim(0).getSize());
    getVar("latitude").getVar(lats.data());
    bool reversedLatsInNcFile = (lats[1] - lats[0] < 0);

    // Get the correct type of vertical level. This supports level types that are
    // written by the exporter in this class.
    NcVar verticalVar;
    if (! getVar("pressure_level").isNull())
    {
        verticalVar = getVar("pressure_level");
    }
    else if (! getVar("level").isNull())
    {
        verticalVar = getVar("level");
    }
    else if (! getVar("hybrid").isNull())
    {
        verticalVar = getVar("hybrid");
    }

    std::vector<float> levs;
    levs.resize(verticalVar.getDim(0).getSize());
    verticalVar.getVar(levs.data());
    bool reversedLevsInNcFile = levs.size() > 1 && (levs[1] - levs[0] < 0);

    bool reversedLatsInGrid = (grid->getLats()[1] - grid->getLats()[0] < 0);
    bool reversedLevelsInGrid = grid->getNumLevels() > 1 && (grid->getLevels()[1] - grid->getLevels()[0] < 0);

    for (uint k = 0; k < grid->getNumLevels(); k++)
    {
        for (uint j = 0; j < grid->getNumLats(); j++)
        {
            for (uint i = 0; i < grid->getNumLons(); i++)
            {
                // Get the correct index.
                int levelIndex = reversedLevsInNcFile != reversedLevelsInGrid ? grid->getNumLevels() - 1 - k : k;
                int latIndex = reversedLatsInNcFile != reversedLatsInGrid ? grid->getNumLats() - 1 - j : j;

                grid->setValue(k, j, i, tmpData[INDEX3zyx_2(
                        levelIndex, latIndex, i,
                        grid->getNumLats() * grid->getNumLons(), grid->getNumLons())]);
            }
        }
    }

    delete[] tmpData;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MNcFile::addDependentGrids(const MStructuredGrid *grid)
{
    // Add the 3-D pressure field, if we export 3-D auxiliary pressure fields.
    if (grid->getLevelType() == AUXILIARY_PRESSURE_3D)
    {
        auto auxGrid = dynamic_cast<const MLonLatAuxiliaryPressureGrid *>(grid);
        const MLonLatAuxiliaryPressureGrid* pGrid =
            auxGrid->getAuxiliaryPressureFieldGrid();
        if (pGrid == nullptr)
        {
            return;
        }

        if (auxGrid == pGrid) // This 3-D field is the pressure grid.
        {
            // NOTE (cf, Feb2024): Surface pressure fields currently in Met.3D
            // are internally handled in units of [hPa]; they are converted upon
            // reading if they are stored in different units. Hence the field
            // will be in [hPa] when written to a file.
            (void)getVar(pGrid->getVariableName().toStdString()).putAtt(
                "units", "hPa");
            variableNamesWithProtectedUnits.push_back(pGrid->getVariableName());
        }
        else if (getVar(pGrid->getVariableName().toStdString()).isNull())
        {
            // Add pressure field if it does not exist yet in file.
            addStructuredGrid(pGrid);
        }
    }

    // Add the surface pressure field, if we export hybrid sigma pressure.
    if (grid->getLevelType() == HYBRID_SIGMA_PRESSURE_3D)
    {
        auto hybGrid = dynamic_cast<const MLonLatHybridSigmaPressureGrid *>(grid);
        auto sfcGrid = hybGrid->getSurfacePressureGrid();

        if (getVar(sfcGrid->getVariableName().toStdString()).isNull())
        {
            addStructuredGrid(sfcGrid);
        }
        // NOTE (cf, Feb2024): Surface pressure fields currently in Met.3D
        // are internally handled in units of [Pa]; they are converted upon
        // reading if they are stored in different units. Hence the field
        // will be in [Pa] when written to a file.
        (void)getVar(sfcGrid->getVariableName().toStdString()).putAtt(
            "units", "Pa");
        variableNamesWithProtectedUnits.push_back(sfcGrid->getVariableName());
    }
}


void MNcFile::addDimensionAttributes(const QString &dimName) const
{
    NcVar var = getVar(dimName.toStdString());
    if (dimName == "longitude")
    {
        (void)var.putAtt("units", "degrees_east");
        (void)var.putAtt("long_name", "longitude");
    }
    else if (dimName == "latitude")
    {
        (void)var.putAtt("units", "degrees_north");
        (void)var.putAtt("long_name", "latitude");
    }
    else if (dimName == "pressure_level")
    {
        (void)var.putAtt("units", "hPa");
        (void)var.putAtt("long_name", "pressure_level");
        (void)var.putAtt("positive", "down");
    }
    else if (dimName == "time")
    {
        (void)var.putAtt("calendar", "gregorian");
        (void)var.putAtt("long_name", "time");
    }
    else if (dimName == "ens0")
    {
        (void)var.putAtt("_CoordinateAxisType", "Ensemble");
    }
    else if (dimName == "hybrid")
    {
        (void)var.putAtt("units", "sigma");
        (void)var.putAtt("long_name", "Hybrid level");
        (void)var.putAtt("positive", "down");
        (void)var.putAtt("standard_name",
                         "atmosphere_hybrid_sigma_pressure_coordinate");
        (void)var.putAtt("formula",
                         "p(time,level,lat,lon) = ap(level) + b(level)*ps(time,lat,lon)");
    }
    else if (dimName == "hyam")
    {
        (void)var.putAtt("units", "hPa");
        (void)var.putAtt("long_name",
                         "hybrid A coefficient at layer midpoints");
    }
    else if (dimName == "hybm")
    {
        (void)var.putAtt("units", "1");
        (void)var.putAtt("long_name",
                         "hybrid B coefficient at layer midpoints");
    }
    else if (dimName == "level")
    {
        (void)var.putAtt("axis", "Z");
        (void)var.putAtt("long_name",
                         "generalized height coordinate, level number");
        (void)var.putAtt("standard_name",
                         "atmosphere_generalized_height_based_coordinate");
        (void)var.putAtt("positive", "down");
    }
    else if (dimName == "y1")
    {
        (void)var.putAtt("axis", "Y");
        (void)var.putAtt("long_name",
                         "y-coordinate in rotated latitude/longitude coordinate system");
        (void)var.putAtt("standard_name", "grid_latitude");
        (void)var.putAtt("units", "degree");
    }
    else if (dimName == "x1")
    {
        (void)var.putAtt("axis", "X");
        (void)var.putAtt("long_name",
                         "x-coordinate in rotated latitude/longitude coordinate system");
        (void)var.putAtt("standard_name", "grid_longitude");
        (void)var.putAtt("units", "degree");
    }
}


std::vector<NcDim> MNcFile::addDimensions(const MStructuredGrid *grid)
{
    // Order of dimensions according to CF Conventions: ..., time, z, lat, lon
    std::vector<NcDim> dataDims;

    // Get the time since epoch in seconds.
    QDateTime startDateTime = grid->getInitTime();
    // Get the reference time
    QDateTime thisTime = grid->getValidTime();
    int hoursSinceInitTime = static_cast<int>(
        startDateTime.secsTo(thisTime) / 3600);

    // Get time dimension of data set or add it if it does not exist.
    NcDim timeDim;
    if (! addDimension<int, NcType::nc_INT>("time", &hoursSinceInitTime, 1,
                                            timeDim))
    {
        // The times mismatch, abort.
        dataDims.clear();
        return dataDims;
    }
    // Set the time since init time.
    QString formattedInitTime = startDateTime.toString("yyyy-MM-dd hh:mm:ss");
    getVar(timeDim.getName()).putAtt("units",
                                     "hours since " + formattedInitTime.
                                     toStdString());

    dataDims.push_back(timeDim);

    // Ensemble dimension
    if (grid->getEnsembleMember() > 0)
    {
        NcDim ensDim;
        long member = grid->getEnsembleMember();
        if (! addDimension<long, NcType::nc_INT64>("ens0", &member, 1, ensDim))
        {
            // Mismatch with existing ensemble dimension, abort.
            dataDims.clear();
            return dataDims;
        }
        dataDims.push_back(ensDim);
    }

    // Vertical dimension
    NcDim levDim;
    if (grid->getLevelType() == PRESSURE_LEVELS_3D)
    {
        if (! addDimension<double, NcType::nc_FLOAT>(
            "pressure_level", grid->getLevels(), grid->getNumLevels(), levDim))
        {
            // Mismatch with existing vertical dimension, abort.
            dataDims.clear();
            return dataDims;
        }
    }
    else if (grid->getLevelType() == HYBRID_SIGMA_PRESSURE_3D)
    {
        if (! addDimension<double, NcType::nc_FLOAT>(
            "hybrid", grid->getLevels(), grid->getNumLevels(), levDim))
        {
            // Mismatch with existing vertical dimension, abort.
            dataDims.clear();
            return dataDims;
        }

        auto hybGrid = dynamic_cast<const MLonLatHybridSigmaPressureGrid *>(
            grid);

        // Add the hybrid field variables if they do not exist yet.
        if (getVar("hyam").isNull())
        {
            NcVar hyamVar = addVar("hyam", ncFloat, levDim);
            hyamVar.putVar(hybGrid->getAkCoeffs());
            addDimensionAttributes("hyam");
        }
        if (getVar("hybm").isNull())
        {
            NcVar hybmVar = addVar("hybm", ncFloat, levDim);
            hybmVar.putVar(hybGrid->getBkCoeffs());
            addDimensionAttributes("hybm");
        }

        // Set the hybrid formula based on the surface pressure field name.
        NcVar var = getVar("hybrid");
        auto sfcGrid = hybGrid->getSurfacePressureGrid();
        (void)var.putAtt("formula_terms",
                         "ap: hyam b: hybm ps: " + sfcGrid->getVariableName().
                         toStdString());
    }
    else if (grid->getLevelType() == AUXILIARY_PRESSURE_3D)
    {
        if (! addDimension<double, NcType::nc_FLOAT>(
            "level", grid->getLevels(), grid->getNumLevels(), levDim))
        {
            // Mismatch with existing vertical dimension, abort.
            dataDims.clear();
            return dataDims;
        }
    }
    if (! levDim.isNull())
    {
        // Only add vertical dimension to list of dimensions if it exists,
        // so 2-D data is also supported.
        dataDims.push_back(levDim);
    }

    NcDim latDim;
    NcDim lonDim;

    if (! addDimension<double, NcType::nc_FLOAT>(
        "latitude", grid->getLats(), grid->getNumLats(), latDim))
    {
        // Mismatch in latitude data, abort.
        dataDims.clear();
        return dataDims;
    }

    if (! addDimension<double, NcType::nc_FLOAT>(
        "longitude", grid->getLons(), grid->getNumLons(), lonDim))
    {
        // Mismatch in longitude data, abort.
        dataDims.clear();
        return dataDims;
    }

    dataDims.push_back(latDim);
    dataDims.push_back(lonDim);

    return dataDims;
}


bool MNcFile::setVerticalLevelType(MVerticalLevelType type)
{
    // Check for integrity with current state of file.
    if (levelType == SIZE_LEVELTYPES)
    {
        levelType = type;
        return true;
    }

    if (type != levelType)
    {
        // Mismatch in vertical level types.
        if (type == SINGLE_LEVEL)
        {
            // Ok, existing type is 3-D, we can also handle 2-D data then.
        }
        else if (levelType == SINGLE_LEVEL)
        {
            levelType = type; // Ok, upgrade file type from 2-D to 3-D.
        }
        else
        {
            return false; // We can't handle different 3-D types.
        }
    }
    return true;
}


bool MNcFile::isVerticalGridTypeSupported(MVerticalLevelType type)
{
    if (type == POTENTIAL_VORTICITY_2D || type == LOG_PRESSURE_LEVELS_3D
        || type == MISC_LEVELS_3D)
        return false;
    return true;
}


NC_memio MNcFile::getDataPointer()
{
    NC_memio memIO;
    nc_close_memio(myId, &memIO);
    nullObject = true; // Set to invalid as it is closed now.

    // Keep a copy of the memory struct. We have to delete it later manually
    // once this object is destroyed.
    inMemoryFile = memIO;
    return memIO;
}


MNcFile::~MNcFile()
{
    // Explicitly free the memory if it has been closed via the
    // @c nc_close_memio() call.
    if (inMemoryFile.memory != nullptr)
    {
        free(inMemoryFile.memory);
    }
}

}