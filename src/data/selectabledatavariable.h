/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSELECTABLEDATAVARIABLE_H
#define MSELECTABLEDATAVARIABLE_H

// standard library imports

// related third party imports

// local application imports
#include "structuredgrid.h"

namespace Met3D
{
/**
 * This class represents the selectable data variables. They are defined by the
 * data source identifier, the vertical level type, and the variable name.
 */
class MSelectableDataVariable
{
public:
    /**
     * Create a selectable data variable from a @p settings object, or from the
     * individual components.
     */
    MSelectableDataVariable();
    explicit MSelectableDataVariable(const QSettings *settings);
    MSelectableDataVariable(const QString &dataSourceID,
                          MVerticalLevelType levelType, const QString &varName);
    MSelectableDataVariable(const QString &dataSourceID, const QString &levelType,
                          const QString &varName);

    /**
     * @return True if this selectable data variable is registered in the @c
     * MSystemManagerAndControl instance.
     */
    bool isInSystemManagerAvailable();

    /**
     * @return True if the selectable data variable represents a valid structure,
     * thus, the fields have been initialized.
     */
    bool isValid() const;

    /**
     * Implementations of comparison operators so objects can be used in maps.
     */
    bool operator==(const MSelectableDataVariable &rhs) const
    {
        return dataSourceID == rhs.dataSourceID && levelType == rhs.levelType &&
               variableName == rhs.variableName;
    }


    bool operator<(const MSelectableDataVariable &other) const
    {
        if (dataSourceID < other.dataSourceID) return true;
        if (dataSourceID > other.dataSourceID) return false;

        if (levelType < other.levelType) return true;
        if (levelType > other.levelType) return false;

        if (variableName < other.variableName) return true;
        if (variableName > other.variableName) return false;

        return false;
    }

    /**
     * Creates string representation of the selectable data variable.
     * @return The serialized byte array.
     */
    QByteArray serialize() const;

    /**
     * Parses a selectable data variable from the given @p byteArray.
     * @param byteArray The byteArray to parse.
     */
    static MSelectableDataVariable deserialize(const QByteArray &byteArray);

    QString dataSourceID;
    MVerticalLevelType levelType;
    QString variableName;

    /**
     * UUID saved in the variable.
     * This value is only saved here since the data variable loads the actor variable.
     * It should not be used to determine equality of two data variables.
     */
    QUuid uuid;
};

} // Met3D

#endif //MSELECTABLEDATAVARIABLE_H
