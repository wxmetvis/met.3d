/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2024 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2023-2024 Thorwin Vogt [*]
**  Copyright 2023-2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "gribreader.h"

// standard library imports
#include <iostream>
#include <limits>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "util/mstopwatch.h"
#include "gxfw/mglresourcesmanager.h"

namespace Met3D
{

const QString MGribReader::GRIB_INDEX_FILE_EXT = QString("met3d_grib_index");
const QString MGribReader::GRIB_INDEX_FILE_EXT_VERSIONED =
        GRIB_INDEX_FILE_EXT + QString("_v%1").arg(GRIB_INDEX_VERSION);

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MGribReader::MGribReader(QString identifier, QString surfacePressureFieldType, QString consistencyCheckReferenceVars,
                         QString auxiliary3DPressureField, bool disableGridConsistencyCheck)
    : MWeatherPredictionReader(identifier, auxiliary3DPressureField, surfacePressureFieldType),
      disableGridConsistencyCheck(disableGridConsistencyCheck),
      ensembleIDIsSpecifiedInFileName(false)
{
    if(!disableGridConsistencyCheck)
    {
        consistencyCheckRefVars =
                consistencyCheckReferenceVars.split("/", Qt::SkipEmptyParts);
    }
}


MGribReader::~MGribReader()
{
    QMutexLocker openFilesLocker(&openFilesMutex);

    // Close open Grib files & their indices.
    for (MGribFileInfo *finfo : openFiles) fclose(finfo->gribFile);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QList<MVerticalLevelType> MGribReader::availableLevelTypes()
{
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    return availableDataFields.keys();
}


QStringList MGribReader::availableVariables(
        MVerticalLevelType levelType)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).keys();
}


QSet<unsigned int> MGribReader::availableEnsembleMembers(
        MVerticalLevelType levelType,
        const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)
            ->availableMembers;
}


QList<QDateTime> MGribReader::availableInitTimes(
        MVerticalLevelType levelType,
        const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)
            ->timeMap.keys();
}


QList<QDateTime> MGribReader::availableValidTimes(
        MVerticalLevelType levelType,
        const QString&     variableName,
        const QDateTime&   initTime)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).value(variableName)->timeMap.keys().contains(initTime))
        throw MBadDataFieldRequest(
                "unkown init time requested: " +
                initTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)->timeMap.value(initTime).keys();
}


QString MGribReader::variableLongName(
        MVerticalLevelType levelType,
        const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)->longname;
}


QString MGribReader::variableStandardName(
        MVerticalLevelType levelType,
        const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)
            ->standardname;
}


QString MGribReader::variableUnits(
        MVerticalLevelType levelType,
        const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)->units;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

QString MGribReader::variableSurfacePressureName(
        MVerticalLevelType levelType,
        const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)
            ->surfacePressureName;
}


QString MGribReader::variableAuxiliaryPressureName(
        MVerticalLevelType levelType,
        const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)
            ->auxiliaryPressureName;
}


MHorizontalGridType MGribReader::variableHorizontalGridType(MVerticalLevelType levelType,
                                   const QString&     variableName)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableDataFields.keys().contains(levelType))
        throw MBadDataFieldRequest(
                "unkown level type requested: " +
                MStructuredGrid::verticalLevelTypeToString(levelType).toStdString(),
                __FILE__, __LINE__);
    if (!availableDataFields.value(levelType).keys().contains(variableName))
        throw MBadDataFieldRequest(
                "unkown variable requested: " + variableName.toStdString(),
                __FILE__, __LINE__);
    return availableDataFields.value(levelType).value(variableName)
            ->horizontalGridType;
}


inline int shiftedLonIndex(int i, MGribVariableInfo* vinfo)
{
    // Compute the shifted index for the lon array. Variables need to be cast
    // to double so that MMOD works (the fraction computed in floor() could
    // otherwise be zero instead of -1 for negative longitudinalIndexShiftForCyclicGrid).
    // Equation: (i + i_offset) % nlons
    //   with i_offset = longitudinalIndexShiftForCyclicGrid
    return MMOD(double(i + vinfo->longitudinalIndexShiftForCyclicGrid),
                double(vinfo->nlons));
}


inline double adaptShiftedLonToReferenceLon0(double lon, MGribVariableInfo* vinfo)
{
    // Adapt shifted lon to range [lon0_ref .. lon0_ref+360.] so that the
    // shifted lons also start with lon0_ref (e.g., if the old lon range was
    // from 0..360, the new could be from -180..180 or from 90..450).
    // Equation: [ (lon - lon0_ref) % 360. ] + lon0_ref
    return MMOD(lon - vinfo->lon0ShiftedReference, 360.) + vinfo->lon0ShiftedReference;
}


void MGribReader::copyLonLatCoordinateDataToGridObject(
        MStructuredGrid *grid, MGribVariableInfo* vinfo)
{
    // Copy lon coordinte data. If grid is cyclic in longitude and needs to
    // be shifted, apply shift.
    if (vinfo->gridIsCyclicInLongitude
            && vinfo->longitudinalIndexShiftForCyclicGrid != 0)
    {
        for (uint i = 0; i < grid->nlons; i++)
        {
            double shiftedLon = vinfo->lons[shiftedLonIndex(i, vinfo)];
            grid->lons[i] = adaptShiftedLonToReferenceLon0(shiftedLon, vinfo);
        }
    }
    else
    {
        grid->setLongitudesFrom(vinfo->lons.data());
    }

    // Copy lat coordinte data.
    grid->setLatitudesFrom(vinfo->lats.data());
}


MStructuredGrid *MGribReader::readGrid(
        MVerticalLevelType levelType,
        const QString &variableName,
        const QDateTime &initTime,
        const QDateTime &validTime,
        unsigned int ensembleMember)
{
#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif

    // Read-lock the entire method; read calls to items of availableDataFields
    // are sprinkled throughout the method.
    QReadLocker availableItemsReadLocker(&availableItemsLock);

    // Determine file name of data file that holds the requested field.
    MGribVariableInfo* vinfo = availableDataFields
            .value(levelType).value(variableName);
    MGribDatafieldInfo dinfo = vinfo->timeMap.value(initTime)
            .value(validTime).value(ensembleMember);

    if (dinfo.dataFiles.count() == 0)
    {
        LOG4CPLUS_ERROR(mlog, "Invalid data field requested.");
        throw;
    }

    // Return value.
    MStructuredGrid *grid = nullptr;

    if (levelType == SINGLE_LEVEL)
    {
        grid = new MRegularLonLatGrid(vinfo->nlats, vinfo->nlons);

        // Copy lon/lat coordinate data.
        copyLonLatCoordinateDataToGridObject(grid, vinfo);

        // Store metadata in grid object.
        grid->setMetaData(initTime, validTime, variableName, ensembleMember);
        grid->setAvailableMembers(vinfo->availableMembers_bitfield);
    }
    else if (levelType == PRESSURE_LEVELS_3D)
    {
        grid = new MRegularLonLatStructuredPressureGrid(
                vinfo->levels.size(), vinfo->nlats, vinfo->nlons);

        // Copy lon/lat/lev coordinate data.
        copyLonLatCoordinateDataToGridObject(grid, vinfo);
        for (unsigned int i = 0; i < grid->nlevs; i++)
        {
            grid->levels[i] = vinfo->levels[i];
        }

        // Store metadata in grid object.
        grid->setMetaData(initTime, validTime, variableName, ensembleMember);
        grid->setAvailableMembers(vinfo->availableMembers_bitfield);
    }
    else if (levelType == HYBRID_SIGMA_PRESSURE_3D)
    {
        auto sigpgrid = new MLonLatHybridSigmaPressureGrid(vinfo->levels.size(), vinfo->nlats, vinfo->nlons);

        grid = sigpgrid;

        // Copy lon/lat/lev coordinate data.
        copyLonLatCoordinateDataToGridObject(grid, vinfo);
        for (uint i = 0; i < grid->nlevs; i++)
        {
            grid->levels[i] = vinfo->levels[i];
        }

        // Copy ak/bk coefficients. If not all levels are stored in the dataset,
        // make sure that the correct ak/bk are copied (-> levelOffset).
        // NOTE: Met.3D only supports continous levels, no support for missing
        // levels is currently implemented.
        uint levelOffset = grid->levels[0] - 1;
        sigpgrid->allocateInterfaceCoefficients();
        for (uint i = 0; i <= sigpgrid->nlevs; i++)
        {
            sigpgrid->aki_hPa[i] = vinfo->aki_hPa[i + levelOffset];
            sigpgrid->bki[i] = vinfo->bki[i + levelOffset];

            if (i < sigpgrid->nlevs)
            {
                sigpgrid->ak_hPa[i] = vinfo->ak_hPa[i + levelOffset];
                sigpgrid->bk[i] = vinfo->bk[i + levelOffset];
            }
        }

        // Store metadata in grid object.
        grid->setMetaData(initTime, validTime, variableName, ensembleMember);
        grid->setAvailableMembers(vinfo->availableMembers_bitfield);
    }

    else if (levelType == POTENTIAL_VORTICITY_2D)
    {
        LOG4CPLUS_ERROR(mlog, "Reading GRIB potential vorticity fields has not "
                              "been implemented yet.");
    }

    else if (levelType == LOG_PRESSURE_LEVELS_3D)
    {
        LOG4CPLUS_ERROR(mlog, "Reading GRIB log(pressure) fields has not "
                              "been implemented yet.");
    }

    else if (levelType == AUXILIARY_PRESSURE_3D)
    {
        // TODO (tv, 22May2023): reverseLevels needs to be set correctly. For now set to false.
        //  The NetCDF reader checks, whether or not aux pressure fields should be reversed,
        //  by checking, whether pressure is ascending or descending within a data column.
        //  This is not really possible to know at this point in the Grib reader,
        //  as the data itself is read after creating the grid here. This means, we dont know whether
        //  or not to reverse the aux pressure grid levels.
        auto auxGrid = new MLonLatAuxiliaryPressureGrid(vinfo->levels.size(), vinfo->lats.size(),
                                                        vinfo->lons.size(),
                                                        false);

        if (variableName == auxiliary3DPressureField)
        {
            auxGrid->auxPressureField_hPa = auxGrid;
        }

        grid = auxGrid;

        // Copy lon/lat/lev coordinate data.
        copyLonLatCoordinateDataToGridObject(grid, vinfo);
        for (unsigned int i = 0; i < grid->nlevs; i++)
        {
            grid->levels[i] = vinfo->levels[i];
        }

        // Store metadata in grid object.
        grid->setMetaData(initTime, validTime, variableName, ensembleMember);
        grid->setAvailableMembers(vinfo->availableMembers_bitfield);
    }

    switch (levelType)
    {
    case PRESSURE_LEVELS_3D:
    case HYBRID_SIGMA_PRESSURE_3D:
    case AUXILIARY_PRESSURE_3D:
    {
        // Iterate over files of the datafield
        for (auto fileIterator = dinfo.dataFiles.begin(); fileIterator != dinfo.dataFiles.end(); ++fileIterator)
        {
            QString filename = fileIterator->filename;

            if (filename.length() == 0)
            {
                LOG4CPLUS_ERROR(mlog, "Invalid data field requested.");
                throw;
            }

            QString filePath = dataRoot.filePath(filename);

            LOG4CPLUS_INFO(mlog, "Reading Grib data from file "
                    << filePath << "...");

            // Is this file opened for the first time?
            QMutexLocker openFilesLocker(&openFilesMutex);
            MGribFileInfo *finfo = nullptr;
            if (openFiles.contains(filename))
            {
                finfo = openFiles[filename];
            }
            else
            {
                // The file is accessed for the first time -- open.
                finfo = new MGribFileInfo();
                QByteArray ba = filePath.toLocal8Bit();
                finfo->gribFile = fopen(ba.data(), "r");

                if (!finfo->gribFile)
                {
                    LOG4CPLUS_ERROR(mlog, "Cannot open GRIB file " << filePath);
                    throw;
                }

                openFiles.insert(filename, finfo);
            }

            // Shortcut to the file handle.
            FILE *gribFile = finfo->gribFile;

            // Lock access to "filename" and associated data until the end of this
            // method.
            QMutexLocker accessMutexLocker(&(finfo->accessMutex));

            openFilesLocker.unlock();

            bool jScanPositively = false;

            // Loop over levels, read each level and store in grid.
            for (int il = 0; il < vinfo->levels.size(); il++)
            {
                long level = vinfo->levels[il];

                if (!fileIterator->offsetForLevel.contains(level))
                {
                    LOG4CPLUS_DEBUG(mlog, "Level " << level
                                                   << " is not within the currently read file. skipping this level");
                    continue;
                }

                // Seek current message in file and get grib handle to message.
                fseek(gribFile, (*fileIterator).offsetForLevel[level], SEEK_SET);
                int err = 0;
                grib_handle *gribHandle = grib_handle_new_from_file(0, gribFile, &err);
                GRIB_CHECK(err, nullptr);

                if (gribHandle != nullptr)
                {
                    // Read lat scan direction
                    jScanPositively = vinfo->jScanPositively;

                    // Read data values into grid object.

                    // Set the missing value number.
                    GRIB_CHECK(grib_set_double(gribHandle, "missingValue",
                                               M_MISSING_VALUE), nullptr);

                    // Get the size of the values array
                    size_t nGribValues = 0;
                    GRIB_CHECK(grib_get_size(gribHandle, "values", &nGribValues), nullptr);
                    if (nGribValues != grid->nlatsnlons)
                    {
                        LOG4CPLUS_ERROR(mlog, "Number of data values in grib message "
                                              "does not correspond to expected data size. "
                                              "Cannot read data values.");
                    }

                    // Get data.
                    double *values = new double[nGribValues];
                    GRIB_CHECK(grib_get_double_array(gribHandle, "values", values,
                                                     &nGribValues), nullptr);

                    // Copy double data to MStructuredGrid float array.

                    for (uint j = 0; j < grid->nlats; j++)
                    {
                        for (uint i = 0; i < grid->nlons; i++)
                        {
                            uint latIndex = jScanPositively ? (grid->nlats - 1 - j) : j;
                            uint lonIndex = i;

                            // Shift in longitude required.
                            if (vinfo->gridIsCyclicInLongitude
                                    && vinfo->longitudinalIndexShiftForCyclicGrid != 0)
                            {
                                lonIndex = shiftedLonIndex(int(i), vinfo);
                            }

                            int gridIndex = INDEX3zyx_2(il, latIndex, i, grid->nlatsnlons, grid->nlons);
                            int valueIndex = INDEX2yx(j, lonIndex, grid->nlons);
                            grid->data[gridIndex] = (float) values[valueIndex];
                        }
                    }

                    delete[] values;

                    grib_handle_delete(gribHandle);
                }
                else
                {
                    LOG4CPLUS_ERROR(mlog, "Could not read grib message.");
                }
            }
        }
        break;
    }
    case SINGLE_LEVEL:
    {
        // Iterate over files of the datafield
        for (const MGribDatafieldFileInfo& fileInfo : dinfo.dataFiles)
        {
            QString filename = fileInfo.filename;

            if (filename.length() == 0)
            {
                LOG4CPLUS_ERROR(mlog, "Invalid data field requested.");
                throw;
            }

            QString filePath = dataRoot.filePath(filename);

            LOG4CPLUS_INFO(mlog, "Reading Grib data from file " << filePath << "...");

            // Is this file opened for the first time?
            QMutexLocker openFilesLocker(&openFilesMutex);
            MGribFileInfo *finfo = nullptr;
            if (openFiles.contains(filename))
            {
                finfo = openFiles[filename];
            }
            else
            {
                // The file is accessed for the first time -- open.
                finfo = new MGribFileInfo();
                QByteArray ba = filePath.toLocal8Bit();
                finfo->gribFile = fopen(ba.data(), "r");

                if (!finfo->gribFile)
                {
                    LOG4CPLUS_ERROR(mlog, "Cannot open GRIB file " << filePath);
                    throw;
                }

                openFiles.insert(filename, finfo);
            }

            // Shortcut to the file handle.
            FILE *gribFile = finfo->gribFile;

            // Lock access to "filename" and associated data until the end of this
            // method.
            QMutexLocker accessMutexLocker(&(finfo->accessMutex));

            openFilesLocker.unlock();

            bool jScanPositively = false;

            // Seek current message in file and get grib handle to message.
            fseek(gribFile, fileInfo.offsetForLevel[0], SEEK_SET);
            int err = 0;
            grib_handle *gribHandle = grib_handle_new_from_file(0, gribFile, &err);
            GRIB_CHECK(err, nullptr);

            if (gribHandle != nullptr)
            {
                // Read lat scan direction
                jScanPositively = vinfo->jScanPositively;

                // Read data values into grid object.

                // Set the missing value number.
                GRIB_CHECK(grib_set_double(gribHandle, "missingValue",
                                           M_MISSING_VALUE), nullptr);

                // Get the size of the values array
                size_t nGribValues = 0;
                GRIB_CHECK(grib_get_size(gribHandle, "values", &nGribValues), nullptr);
                if (nGribValues != grid->nvalues)
                {
                    LOG4CPLUS_ERROR(mlog, "Number of data values in grib message "
                                          "does not correspond to expected data size. "
                                          "Cannot read data values.");
                }

                // Get data.
                double *values = new double[nGribValues];
                GRIB_CHECK(grib_get_double_array(gribHandle, "values", values,
                                                 &nGribValues), nullptr);

                if (dinfo.applyExp)
                {
                    // If surface pressure is specified as lnsp, apply exponential
                    // function to reconstruct surface pressure "sp".
                    for (uint n = 0; n < nGribValues; n++)
                    {
                        values[n] = exp(values[n]);
                    }
                }

                // Copy double data to MStructuredGrid float array.
                for (uint j = 0; j < grid->nlats; j++)
                {
                    for (uint i = 0; i < grid->nlons; i++)
                    {
                        uint latIndex = jScanPositively ? grid->nlats - 1 - j : j;
                        uint lonIndex = i;

                        // Shift in longitude required.
                        if (vinfo->gridIsCyclicInLongitude
                                && vinfo->longitudinalIndexShiftForCyclicGrid != 0)
                        {
                            lonIndex = shiftedLonIndex(i, vinfo);
                        }

                        int dataIndex = INDEX2yx(latIndex, i, grid->nlons);
                        int valueIndex = INDEX2yx(j, lonIndex, grid->nlons);

                        // Check scan direction
                        grid->data[dataIndex] = (float) values[valueIndex];
                    }
                }

                delete[] values;

                grib_handle_delete(gribHandle);
            }
            else
            {
                LOG4CPLUS_ERROR(mlog, "Could not read grib message.");
            }
        }
        break;
    }
    default:
        break;
    } // switch


    if (!auxiliary3DPressureField.isEmpty() && variableName == auxiliary3DPressureField)
    {
        float toHpaFactor = scaleFactorToHPa(vinfo->units.toStdString());
        if (!isnan(toHpaFactor))
        {
            for (unsigned int i = 0; i < grid->nvalues; i++)
            {
                grid->setValue(i, grid->getValue(i) * toHpaFactor);
            }
        }
        else
        {
            QString err = QString(
                    "invalid units '%1' for pressure levels of auxiliary "
                    "pressure field, expected one of: (%2)").arg(vinfo->units,
                    getSupportedPressureUnits().join(", "));
            throw MGribError(err.toStdString(), __FILE__, __LINE__);
        }

        // Check for zero pressure values in grid. If these are present,
        // problems will likely arise when the data is used (e.g. interpolation
        // or shaders including the raycaster may crash).
        if (grid->min() == 0.)
        {
            LOG4CPLUS_ERROR(mlog,
                           "Auxiliary pressure field "
                           "contains '0' values. This may lead to rendering "
                           "problems up to program crashes. Please fix your "
                           "data and remove all entries with '0' pressure. "
                           "If the '0' entries are located along a domain "
                           "border, you can use the 'ncks' utility, e.g. "
                           "'ncks -d lat,<min>,<max> in.nc out.nc'.");
        }
    }

    // TODO (cf, 2024-01-11)
    // Surface pressure fields are expected in Pa in grid classes and shaders.
    // ak/bk coefficients are converted to hPa, same with auxiliary 3-d pressure fields.
    // Should make it more consistent: everything in hPa, both in grib reader and netCDF.
    if (!surfacePressureFieldName.isEmpty() && variableName == surfacePressureFieldName)
    {
        float toHpaFactor = scaleFactorToHPa(vinfo->units.toStdString());
        if (!isnan(toHpaFactor))
        {
            for (unsigned int i = 0; i < grid->nvalues; i++)
            {
                grid->setValue(i, grid->getValue(i) * toHpaFactor * 100.0); // to Pa
            }
        }
        else
        {
            QString err = QString(
                        "invalid units '%1' for pressure levels of surface pressure "
                        "field, expected one of: (%2)").arg(vinfo->units,
                        getSupportedPressureUnits().join(", "));
            throw MGribError(err.toStdString(), __FILE__, __LINE__);
        }
    }

    grid->setHorizontalGridType(variableHorizontalGridType(levelType, variableName));

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_INFO(mlog, "Single member GRIB data field read in "
                    << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                    << " seconds.");
#endif

    // Return data field.
    return grid;
}


void MGribReader::scanDataRoot()
{
#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif

    // Lock access to all availableXX data fields.
    QWriteLocker availableItemsWriteLocker(&availableItemsLock);

    LOG4CPLUS_INFO(mlog, "Scanning directory "
                    << dataRoot.absolutePath() << " "
                    << "for grib files with forecast data.");
    LOG4CPLUS_INFO(mlog, "Using file filter: " << dirFileFilters);
    LOG4CPLUS_INFO(mlog, "Available files:");

    // Get a list of all files in the directory that match the wildcard name
    // filter given in "dirFileFilters".

    QStringList availableFiles;
    getAvailableFilesFromFilters(availableFiles);

    ensembleIDIsSpecifiedInFileName = dirFileFilters.contains("%m");
    int  ensembleIDFromFile = -1;

    // Create progress bar tasks.
    auto progressBar = MProgressBar::getInstance();
    auto taskHandles = progressBar->addTasks(availableFiles.size(), "Reading GRIB file...");

    // Scan all grib files contained in the directory.
    for (const QString& gribFileName : availableFiles)
    {
        // Mark file from last iteration as completed so we don't have to
        // do this at every 'continue'.
        if (gribFileName != availableFiles[0])
        {
            progressBar->firstTaskCompleted(taskHandles);
        }
        // (Skip index files.)
        if (gribFileName.contains(GRIB_INDEX_FILE_EXT)) continue;

        LOG4CPLUS_INFO(mlog, "Scanning file "
                        << gribFileName << "...");

        // First, read or create the grib index for fast access to messages.
        // =================================================================

        QString filePath = dataRoot.filePath(gribFileName);
        QString fileIndexPath = QString("%1." + GRIB_INDEX_FILE_EXT_VERSIONED).arg(filePath);

        // A) GRIB index file exists. Read from index.
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        // If the grib index exists, read it. Otherwise, create it and store it.
        if (QFile::exists(fileIndexPath))
        {
            LOG4CPLUS_INFO(mlog, "Reading grib index for file "
                            << gribFileName << "...");

            // Open index file.
            QFile indexFile(fileIndexPath);
            indexFile.open(QIODevice::ReadOnly);
            QDataStream indexDataStream(&indexFile);

            // Read index file header (only version number "3" supported for
            // now; this may be changed in the future when the index format
            // changes).
            qint32 indexVersion;
            indexDataStream >> indexVersion;
            if (indexVersion != GRIB_INDEX_VERSION)
            {
                LOG4CPLUS_ERROR(mlog, "ERROR: grib index has version "
                                << indexVersion << "; this version of Met.3D "
                                "only supports version " << GRIB_INDEX_VERSION <<
                                ". Skipping file -- remove the index file if you want the grib "
                                "file to be considered.");
                continue;
            }

            indexDataStream.setVersion(QDataStream::Qt_4_8);

            MGribMessageIndexInfo gmiInfo;
            while ( !indexDataStream.atEnd() )
            {
                gmiInfo.readFromDataStream(&indexDataStream);

                MVerticalLevelType levelType = gmiInfo.levelType;

                // Create a new MVariableInfo struct and store available
                // variable information in this field.
                MGribVariableInfo* vinfo;
                if (availableDataFields[levelType].contains(gmiInfo.variablename))
                {
                    vinfo = availableDataFields[levelType].value(gmiInfo.variablename);

                    // Domain checks have already been run on index creation;
                    // skip them here (see below).
                }
                else
                {
                    vinfo = new MGribVariableInfo();
                    vinfo->variablename = gmiInfo.variablename;
                    vinfo->longname = gmiInfo.longname;
                    vinfo->standardname = gmiInfo.standardname;
                    vinfo->units = gmiInfo.units;
                    vinfo->fcType = gmiInfo.fcType;

                    vinfo->nlons = gmiInfo.nlons;
                    vinfo->nlats = gmiInfo.nlats;
                    vinfo->lon0 = gmiInfo.lon0;
                    vinfo->lat0 = gmiInfo.lat0;
                    vinfo->lon1 = gmiInfo.lon1;
                    vinfo->lat1 = gmiInfo.lat1;
                    vinfo->dlon = gmiInfo.dlon;
                    vinfo->dlat = gmiInfo.dlat;

                    // Fill lat/lon arrays (QVector copy).
                    vinfo->lons = gmiInfo.lons;
                    vinfo->lats = gmiInfo.lats;
                    // TODO (bt, 08FEB2017) Adapt to set type according to the data read.
                    vinfo->horizontalGridType = MHorizontalGridType::REGULAR_LONLAT_GRID;

                    // Check if grid spans globe in longitude (cyclic in lon).
                    double lonWest = MMOD(vinfo->lon0, 360.);
                    double lonEast = MMOD(vinfo->lon1 + vinfo->dlon, 360.);
                    vinfo->gridIsCyclicInLongitude =
                            floatIsAlmostEqualRelativeAndAbs(
                                lonWest, lonEast, M_LONLAT_RESOLUTION);
                    vinfo->longitudinalIndexShiftForCyclicGrid = 0;

                    vinfo->jScanPositively = gmiInfo.jScanPositively;

                    if (levelType == HYBRID_SIGMA_PRESSURE_3D)
                    {
                        vinfo->surfacePressureName = gmiInfo.surfacePressureName;
                        vinfo->aki_hPa = gmiInfo.aki_hPa;
                        vinfo->bki = gmiInfo.bki;
                        vinfo->ak_hPa = gmiInfo.ak_hPa;
                        vinfo->bk = gmiInfo.bk;
                    }
                    else if (levelType == AUXILIARY_PRESSURE_3D)
                    {
                        vinfo->auxiliaryPressureName = auxiliary3DPressureField;
                        vinfo->nlev = gmiInfo.nlev;
                        vinfo->numberOfVGridUsed = gmiInfo.numberOfVGridUsed;
                        vinfo->uuidOfVGrid = gmiInfo.uuidOfVGrid;
                    }

                    // Insert the new MVariableInfo struct into the variable
                    // name map..
                    availableDataFields[levelType].insert(
                                vinfo->variablename, vinfo);
                    // ..and, if a CF standard name is available, into the std
                    // name map.
                    if (vinfo->standardname != "")
                        availableDataFieldsByStdName[levelType].insert(
                                    vinfo->standardname, vinfo);
                }

                long ensMember = gmiInfo.ensMember;
                vinfo->availableMembers.insert(ensMember);
                vinfo->availableMembers_bitfield |= (Q_UINT64_C(1) << ensMember);

                // Get time values of this message.
                QDateTime initTime = gmiInfo.initTime;
                QDateTime validTime = gmiInfo.validTime;

                // Store filename and offset of grib message in index.
                MGribDatafieldInfo *info = nullptr;
                MGribDatafieldFileInfo *fileInfo = nullptr;
                if (vinfo->timeMap[initTime][validTime].contains(ensMember))
                {
                    info = &(vinfo->timeMap[initTime][validTime][ensMember]);
                    if(info->dataFiles.contains(gribFileName))
                    {
                        fileInfo = &(info->dataFiles[gribFileName]);
                    }
                    else
                    {
                        info->dataFiles.insert(gribFileName, MGribDatafieldFileInfo(gribFileName));
                        fileInfo = &(info->dataFiles[gribFileName]);
                    }
                }
                else
                {
                    info = &(vinfo->timeMap[initTime][validTime][ensMember]);
                    info->dataFiles.insert(gribFileName, MGribDatafieldFileInfo(gribFileName));
                    fileInfo = &(info->dataFiles[gribFileName]);
                }

                // Get vertical level.
                long level = gmiInfo.level;

                // Distinguish between ln surface pressure fields and surface
                // pressure fields (in index files both are stored as
                // surface_2D).
                if (levelType == SINGLE_LEVEL)
                {
                    if (vinfo->variablename.startsWith("lnsp"))
                    {
                        setSurfacePressureFieldName("lnsp");
                        level = 0;
                        info->applyExp = true;
                    }
                    else if (vinfo->variablename.startsWith("sp"))
                    {
                        setSurfacePressureFieldName("sp");
                        info->applyExp = false;
                    }
                }
                else
                {
                    info->applyExp = false;
                }

                fileInfo->offsetForLevel[level] = gmiInfo.filePosition;

                // Insert level into list of vertical levels for this
                // variable.
                if (!vinfo->levels.contains(level))
                {
                    vinfo->levels << level;
                }

                // That's it!
            }
        } // read index

        // B) GRIB index file does not exist. Scan grib file and create index.
        // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

        else
        { // read from grib file and create new index
            QFileInfo fInfo(filePath);
            QString filter = QString("%1." + GRIB_INDEX_FILE_EXT + "*").arg(fInfo.fileName());
            auto dir = QDir(filePath);
            dir.cdUp();
            QStringList availableIndices = dir.entryList({filter}, QDir::Files);

            if (!availableIndices.isEmpty())
            {
                LOG4CPLUS_INFO(mlog,
                               "Note that this version of Met.3D requires "
                               "GRIB index files of version "
                               << GRIB_INDEX_VERSION
                               << ". There are index files available for "
                               "this GRIB file, but they were created "
                               "using a different version.");

                LOG4CPLUS_INFO(mlog,
                               "Rescanning GRIB file to create a suitable index file, please be patient.");
            }

            createGribFileIndex(filePath, fileIndexPath, availableFiles, ensembleIDFromFile, gribFileName);
        } // if (create new index)

#ifdef ENABLE_MET3D_STOPWATCH
        stopwatch.split();
        LOG4CPLUS_INFO(mlog, "File scanned in "
                        << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                        << " seconds.\n");
#endif
    } // for (files)

    // Last file completed.
    progressBar->tasksCompleted(taskHandles);

    // Perform checks, e.g. to make sure that for each data field all levels
    // are present.
    // =====================================================================

    LOG4CPLUS_INFO(mlog, "Checking fields with currently unsupported vertical "
                          "level types...");

    QVector<MGribVariableInfo*> unknown3DGridTypesVars;

    // Check both unsupported level types (MISC_LEVELS_3D) and 3D fields if
    // they are only present on one level: Load these as single level types
    // instead.
    QList<MVerticalLevelType> levelTypes3d = {
        MISC_LEVELS_3D, PRESSURE_LEVELS_3D, HYBRID_SIGMA_PRESSURE_3D,
        AUXILIARY_PRESSURE_3D};

    for (MVerticalLevelType levelType : levelTypes3d)
    {
        if (!availableDataFields.contains(levelType)) continue;

        // Remember which variables we have to remove for this level type.
        QList<MGribVariableInfo*> varsToRemoveForLevelType;
        for (MGribVariableInfo *var : availableDataFields[levelType])
        {
            if (var->levels.size() == 1)
            {
                if (levelType == MISC_LEVELS_3D)
                {
                    LOG4CPLUS_INFO(
                        mlog, "Variable "
                                  << var->variablename
                                  << " has a currently unsupported vertical "
                                     "level type but seems to be a 2D field. "
                                     "Loading as a single level field.");
                }
                else
                {
                    LOG4CPLUS_INFO(
                        mlog, "Variable "
                                  << var->variablename
                                  << " has a 3D level type "
                                  << MAbstractGrid::verticalLevelTypeToString(
                                         levelType)
                                  << " but seems to be a 2D field. Loading as "
                                     "a single level field.");
                }

                // We set the level to 0 for this field here.
                // surface type variables have level 0 by default,
                // but we also want to load all other 2D fields, which might
                // have different levels, such as 2 for the 2t variable.
                // The consistency check expects the levels of all 2D variables
                // to be the same, which is why we set them to 0 here.
                // At the same time, we need to adjust the offset for level
                // map of each data file.
                double level = var->levels[0];

                if (level != 0)
                {
                    for (MGribValidTimeMap &vt : var->timeMap)
                    {
                        for (MGribEnsembleMemberMap &e : vt)
                        {
                            for (MGribDatafieldInfo &info : e)
                            {
                                for (MGribDatafieldFileInfo &file :
                                     info.dataFiles)
                                {
                                    file.offsetForLevel[0] =
                                        file.offsetForLevel[level];

                                    file.offsetForLevel.remove(level);
                                }
                            }
                        }
                    }

                    var->levels[0] = 0;
                }
                availableDataFields[SINGLE_LEVEL].insert(var->variablename,
                                                         var);
                varsToRemoveForLevelType.append(var);
            }
            else if (levelType == MISC_LEVELS_3D)
            {
                unknown3DGridTypesVars.append(var);
            }
        }
        for (auto varToRemove : varsToRemoveForLevelType)
        {
            availableDataFields[levelType].remove(varToRemove->variablename);
        }
    }

    if (!availableDataFields.contains(AUXILIARY_PRESSURE_3D) ||
         availableDataFields[AUXILIARY_PRESSURE_3D].isEmpty())
    {
        for (MGribVariableInfo *var : unknown3DGridTypesVars)
        {
            availableDataFields[AUXILIARY_PRESSURE_3D].insert(var->variablename, var);
            var->auxiliaryPressureName = auxiliary3DPressureField;
        }

        unknown3DGridTypesVars.clear();
    }
    else
    {
        // The MISC_LEVELS_3D variables that are still present cannot be assigned
        // to a supported level types.
        if (!unknown3DGridTypesVars.isEmpty())
        {
            LOG4CPLUS_WARN(mlog, "The vertical level type of the following fields "
                                 "cannot be detected. The fields are discarded.");
            for (MGribVariableInfo *var : unknown3DGridTypesVars)
            {
                LOG4CPLUS_WARN(mlog, " - " + var->variablename);
                delete var;
            }
            unknown3DGridTypesVars.clear();
        }
    }



    // Discard the variables of unsupported vertical level type.
    availableDataFields[MISC_LEVELS_3D].clear();
    availableDataFields.remove(MISC_LEVELS_3D);

    // Vector for time invariant fields.
    QVector<MGribVariableInfo*> timeInvariantFields;
    // Map of all init times to valid times of this data set.
    QMap<QDateTime, QList<QDateTime>> timeMap;

    LOG4CPLUS_INFO(mlog, "Scanning dataset for time invariant fields (fields with only a single time step).");
    for (const MGribVariableNameMap& nameMap : availableDataFields)
    {
        for (MGribVariableInfo* var : nameMap)
        {
            if (var->timeMap.count() == 1)
            {
                if (var->timeMap.first().count() == 1)
                {
                    // Variable is time invariant.
                    timeInvariantFields.append(var);
                    LOG4CPLUS_INFO(mlog, "Found time invariant field " << var->variablename);
                    continue;
                }
            }
            // For time variant variables, record the init and valid times
            // so that we can set them later in the time invariant fields.
            for (auto i = var->timeMap.begin(); i != var->timeMap.end(); i++)
            {
                const QDateTime& initTime = i.key();
                if (!timeMap.contains(initTime))
                {
                    timeMap.insert(initTime, var->timeMap[initTime].keys());
                    continue;
                }

                for (auto v = var->timeMap[initTime].begin(); v != var->timeMap[initTime].end(); v++)
                {
                    const QDateTime& validTime = i.key();

                    if (timeMap[initTime].contains(validTime)) continue;

                    timeMap[initTime].append(validTime);
                }
            }
        }
    }

    if (timeInvariantFields.isEmpty())
    {
        LOG4CPLUS_INFO(mlog, "No time invariant fields found.");
    }
    else
    {
        LOG4CPLUS_INFO(mlog, "Init and valid times of time invariant fields are set to "
                             "equal the other fields in the dataset.");
        for (MGribVariableInfo* var : timeInvariantFields)
        {
            MGribEnsembleMemberMap ensMap = var->timeMap.first().first();
            for (auto i = timeMap.begin(); i != timeMap.end(); i++)
            {
                for (const QDateTime& validTime : i.value())
                {
                    var->timeMap[i.key()][validTime] = ensMap;
                }
            }
        }
        LOG4CPLUS_INFO(mlog, "All time invariant fields are initialized.");
    }

    QString horizontalRefVarName = "";
    MVerticalLevelType referenceLevelType;
    LOG4CPLUS_INFO(mlog, "Checking consistency of indexed data fields...");
    for (const MVerticalLevelType& levelType : availableDataFields.keys())
    {
        if (levelType == AUXILIARY_PRESSURE_3D)
        {
            if (!availableDataFields[levelType].contains(auxiliary3DPressureField))
            {
                LOG4CPLUS_WARN(mlog,
                               "No pressure field with the specified name '"
                               << auxiliary3DPressureField
                               << "' was found. Cannot check consistency of 3D "
                                  "auxiliary-pressure fields.");
                LOG4CPLUS_WARN(mlog,
                               "These fields have been found. Please specify one "
                               "of these fields when loading the dataset again.");

                for (auto auxPressureVar : availableDataFields[levelType])
                {
                    LOG4CPLUS_WARN(mlog, " - " << auxPressureVar->variablename);
                }

                LOG4CPLUS_WARN(mlog,
                               "For now, all these 3D variables using an auxiliary "
                               "pressure field are discarded.");

                while (!availableDataFields[levelType].isEmpty())
                {
                    QString key = availableDataFields[levelType].firstKey();
                    delete availableDataFields[levelType].take(key);
                }
                continue;
            }
        }

        QStringList varNames = availableDataFields[levelType].keys();
        QString referenceVarName = "";
        QVector<double> referenceLevels;

        for (const QString& refVar : consistencyCheckRefVars)
        {
            if(availableDataFields[levelType].contains(refVar))
            {
                referenceVarName = refVar;
                referenceLevels = availableDataFields[levelType][
                        referenceVarName]->levels;
                break;
            }
        }

        for (const QString& varName : varNames)
        {
            // 1. Check if variable index is consistent in itself (i.e.
            // the variable's vertical levels, time steps, etc. are all
            // consistent.
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if (!checkInternalConsistencyOfVariableIndex(
                        availableDataFields[levelType][varName]))
            {
                delete availableDataFields[levelType].take(varName);
                continue;
            }

            // 2. Check if the variable is consistent to the "reference
            // variable" (i.e. the first variable encountered) in this dataset
            // (to make sure that all data fields of the data set are defined
            // on the same grid etc.).
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            else if ( !disableGridConsistencyCheck )
            {
                if (horizontalRefVarName.isEmpty()
                        || !availableDataFields[levelType]
                                .contains(horizontalRefVarName))
                {
                    horizontalRefVarName = varName;
                    referenceLevelType = levelType;
                }

                if (referenceLevels.isEmpty())
                {
                    referenceVarName = varName;
                    referenceLevels = availableDataFields[levelType][
                            referenceVarName]->levels; // QVector copy
                }

                LOG4CPLUS_INFO(mlog, "Checking if variable '"
                                << varName << "' (on "
                                << MStructuredGrid::verticalLevelTypeToString(
                                    levelType)
                                << ") is consistent to the reference variable(s) '"
                                << horizontalRefVarName << "'/'"
                                << referenceVarName << "'...");

                // Check consistency of vertical levels (QVector comparison).
                if (!checkLevelConsistency(levelType, varName, referenceLevels))
                {
                    LOG4CPLUS_ERROR(mlog,
                                    "Found difference in vertical levels to"
                                    " reference variable '" << referenceVarName
                                    << "'; discarding variable: '"
                                    << varName << "' [Dataset: "
                                    << getIdentifier() << "]");
                    delete availableDataFields[levelType].take(varName);
                    continue;
                }

                // Check consistency of horizontal coordinates.
                // NOTE: Here we also determine a possible index shift in
                // longitude for cyclic grids (last parameter set to "true").
                else if (!checkHorizontalConsistencyOfVariableToReference(
                            availableDataFields[referenceLevelType]
                            [horizontalRefVarName],
                            availableDataFields[levelType][varName],
                            true))
                {
                    LOG4CPLUS_ERROR(mlog,
                                    "Found difference to reference"
                                    " variable '" << horizontalRefVarName
                                    << "'; discarding variable: '" << varName
                                    << "' [Dataset: " << getIdentifier()
                                    << "]");
                    delete availableDataFields[levelType].take(varName);
                    continue;
                }

                LOG4CPLUS_INFO(mlog, "... OK: variable '" << varName
                                << "' is consistent to the reference variable(s).");
            }

            // 3. For auxiliary pressure fields, check that the vertical
            // grid used is consistent with the auxiliary pressure variable.
            // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
            if (levelType == AUXILIARY_PRESSURE_3D && !disableGridConsistencyCheck)
            {
                MGribVariableInfo *auxPressureInfo = availableDataFields[levelType].value(auxiliary3DPressureField);

                LOG4CPLUS_INFO(mlog, "Checking if vertical grid used by variable '"
                        << varName << "' (on "
                        << MStructuredGrid::verticalLevelTypeToString(levelType)
                        << ") is consistent to the auxiliary pressure variable(s) '"
                        << auxPressureInfo->variablename << "'...");

                if (!checkAuxiliaryPressureVerticalGridConsistency(availableDataFields[levelType][varName],
                                                                   auxPressureInfo))
                {
                    LOG4CPLUS_ERROR(mlog,
                                    "Found difference to auxiliary pressure"
                                    " variable '"
                                            << auxPressureInfo->variablename
                                            << "'; discarding variable: '"
                                            << varName << "' [Dataset: "
                                            << getIdentifier() << "]");
                    delete availableDataFields[levelType].take(varName);
                    continue;
                }

                LOG4CPLUS_INFO(mlog, "... OK: variable '" << varName
                        << "' uses same vertical grid as the auxiliary pressure variable.");
            }
        }
    }

    // debugPrintLevelTypeMap(availableDataFields);

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_INFO(mlog, "Directory scanned in "
                    << stopwatch.getElapsedTime(MStopwatch::SECONDS)
                    << " seconds.");
#endif

    //    gribReader->readGrid(
    //                SURFACE_2D, "sp",
    //                QDateTime::fromString("2012-10-17T00:00:00Z", Qt::ISODate),
    //                QDateTime::fromString("2012-10-19T18:00:00Z", Qt::ISODate), 2
    //                )->dumpGridData();
    //    gribReader->readGrid(
    //                PRESSURE_LEVELS_3D, "t",
    //                QDateTime::fromString("2012-10-17T00:00:00Z", Qt::ISODate),
    //                QDateTime::fromString("2012-10-19T18:00:00Z", Qt::ISODate), 2
    //                )->dumpGridData();
    //    gribReader->readGrid(
    //                HYBRID_SIGMA_PRESSURE_3D, "t",
    //                QDateTime::fromString("2012-10-15T00:00:00Z", Qt::ISODate),
    //                QDateTime::fromString("2012-10-19T18:00:00Z", Qt::ISODate), 2
    //                )->dumpGridData();
}


void MGribReader::createGribFileIndex(const QString &filePath, const QString &fileIndexPath,
                                      QStringList &availableFiles, int &ensembleIDFromFile,
                                      const QString &gribFileName)
{
    LOG4CPLUS_INFO(mlog, "Creating new index... please wait.");

    // Open the file.
    FILE* gribfile = nullptr;
    QByteArray ba = filePath.toLocal8Bit();
    gribfile = fopen(ba.data(), "r");
    if (!gribfile)
    {
        LOG4CPLUS_WARN(mlog, "Cannot open GRIB file "
                << filePath << ", skipping.");
        return;
    }

    if (ensembleIDIsSpecifiedInFileName)
    {
        ensembleIDFromFile = getEnsembleMemberIDFromFileName(gribFileName);

        if (ensembleIDFromFile == -1)
        {
            LOG4CPLUS_ERROR(mlog, "Ensemble tag found in file filter "
                                  " but filter did not match file \""
                    << gribFileName
                    << "\" (currently only integer values are allowed"
                       " as ensemble member specifiers).");
            return;
        }
    }

    // Open a new index file.
    QFile indexFile(fileIndexPath);
    indexFile.open(QIODevice::WriteOnly);
    QDataStream indexDataStream(&indexFile);
    // Write index file header (version number "3" for now; this may
    // be changed in the future when the index format changes).
    indexDataStream << (qint32)GRIB_INDEX_VERSION;
    indexDataStream.setVersion(QDataStream::Qt_4_8);

    // lnsp model level fix (ECMWF inconsistency: lnsp is stored on
    // a single model level, not as a surface field...).
    bool fixLNSPmodelLevel = false;

    // Loop over grib messages contained in the file.
    grib_handle* gribHandle = nullptr;
    int err = 0;
    int messageCount = 0;

    // Get current file position to store in the index for this message
    // (see end of loop).
    long filePosition = ftell(gribfile);
    // LOG4CPLUS_DEBUG(mlog, "file position: " << filePosition);

    while ((gribHandle =
                    grib_handle_new_from_file(0, gribfile, &err)) != nullptr)
    {
        fixLNSPmodelLevel = false;
        // LOG4CPLUS_DEBUG(mlog, "=========> Indexing message "
        //                 << messageCount);

        MGribMessageIndexInfo gmiInfo;

        // Determine type of data fields (analysis, deterministic,
        // ensemble). Append type to variable name so variables with
        // the same name but different types can be distinguished).
        // If ensemble members are stored implicitly, all variables
        // read are of ensemble data type.
        // Set it to the default 'fc' in case that the 'ls.dataType' field is not set.
        QString dataType = "fc";
        if (!ensembleIDIsSpecifiedInFileName)
        {
            try {
                dataType = getGribStringKey(gribHandle, "ls.dataType");
            }
            catch (MGribError& e)
            {
                LOG4CPLUS_WARN(mlog, "Unable to determine dataType of "
                                     "grid (ECMWF uses an, fc, pf, cf). "
                                     "Setting it to fc for compatibility.");
            }
            // LOG4CPLUS_DEBUG(mlog, "ls.dataType: " << dataType);
            // Perturbed (pf) and control (cf) forecasts are combined into
            // "ensemble" (ens) forecasts.
            if ( (dataType == "pf") || (dataType == "cf"))
            {
                dataType = "ens";
            }
        }
        else
        {
            dataType = "ens";
        }

        // Currently Met.3D can only handle data fields on a regular
        // lat/lon grid in the horizontal.
        QString gridType = getGribStringKey(gribHandle, "geography.gridType");
        // LOG4CPLUS_DEBUG(mlog, "geography.gridType: " << gridType);
        if (gridType != "regular_ll" && gridType != "rotated_ll")
        {
            LOG4CPLUS_WARN(mlog, "Met.3D can only handle 'regular_ll' or 'rotated_ll' "
                                 "grids, skipping this field.");
            grib_handle_delete(gribHandle);
            continue;
        }

        // Determine the variable name.
        QString shortName = getGribStringKey(gribHandle, "parameter.shortName");

        if(isGribKeyMissing(gribHandle, "vertical.level")){
            LOG4CPLUS_WARN(mlog, "Vertical level key is missing for variable " << shortName << ". Skipping it.");
            continue;
        }

        // Get vertical level.
        long level = getGribLongKey(gribHandle, "vertical.level");

        // Determine the type of the vertical level of the variable.

        QString typeOfLevel = getGribStringKey(gribHandle, "vertical.typeOfLevel");
        // LOG4CPLUS_DEBUG(mlog, "vertical.typeOfLevel: " << typeOfLevel);

        MVerticalLevelType levelType;
        if (typeOfLevel == "surface")
        {
            levelType = SINGLE_LEVEL;

            // Set level to 0. It's a workaround for the consistency check.
            level = 0;
        }
        else if (typeOfLevel == "isobaricInhPa")
            levelType = PRESSURE_LEVELS_3D;
        else if (typeOfLevel == "hybrid")
            levelType = HYBRID_SIGMA_PRESSURE_3D;
        else if (typeOfLevel == "potentialVorticity")
            levelType = POTENTIAL_VORTICITY_2D;
        else if (typeOfLevel == "generalVerticalLayer" || typeOfLevel == "generalVertical")
        {
            // For now, load general vertical layer level type as auxiliary pressure level type.
            // Let the user decide, which variable is the aux pressure.
            // TODO (tv, 30may23): Add vertical grid id to grib index and variable info, so that only aux pressure grids with the same
            //                     vertical grid id get used.
            levelType = AUXILIARY_PRESSURE_3D;

            double nlev = 0;
            double numberOfVGridUsed = 0;
            QList<quint8> id;
            if (!isGribKeyMissing(gribHandle, "nlev"))
            {
                nlev = getGribDoubleKey(gribHandle, "nlev");
            }
            if (!isGribKeyMissing(gribHandle, "numberOfVGridUsed"))
            {
                numberOfVGridUsed = getGribDoubleKey(gribHandle, "numberOfVGridUsed");
            }
            if (!isGribKeyMissing(gribHandle, "uuidOfVGrid"))
            {
                id << getGribByteArrayKey(gribHandle, "uuidOfVGrid", 16);
            }
            else
            {
                LOG4CPLUS_WARN(mlog, "No vertical grid UUID found for a general vertical layer or general vertical grid.");
            }

            gmiInfo.nlev = nlev;
            gmiInfo.numberOfVGridUsed = numberOfVGridUsed;
            gmiInfo.uuidOfVGrid = QVector<uint8_t>::fromList(id);
        }
        else
        {
            // If neither of the above choices could be matched.
            // Read all levels that are currently unsupported as MISC_LEVELS_3D,
            // so that we can filter out 2D fields, as they can be read.
            levelType = MISC_LEVELS_3D;
        }
        gmiInfo.levelType = levelType;
        gmiInfo.level = level;

        if (!isGribKeyMissing(gribHandle, "jScansPositively"))
        {
            gmiInfo.jScanPositively = (bool)getGribLongKey(gribHandle, "jScansPositively");
        }
        else
        {
            gmiInfo.jScanPositively = false;
        }

        QString varName = shortName;

        // Handle special case "lnsp". "lnsp" fields at ECMWF are
        // stored on model level 1 (not as a surface field). In Met.3D,
        // we re-cast as a surface field (which it is...).
        if (shortName == "lnsp" && levelType == HYBRID_SIGMA_PRESSURE_3D)
        {
            levelType = SINGLE_LEVEL;
            gmiInfo.levelType = levelType;
            fixLNSPmodelLevel = true;
            setSurfacePressureFieldName("lnsp");

        }
        else if (shortName == "sp")
        {
            setSurfacePressureFieldName("sp");
        }

        // Create a new MVariableInfo struct and store availabe
        // variable information in this field.

        MGribVariableInfo* vinfo;
        if (availableDataFields[levelType].contains(varName))
        {
            vinfo = availableDataFields[levelType].value(varName);

            MGribVariableInfo currentVInfo;

            currentVInfo.nlons = getGribLongKey(gribHandle, "Ni");
            currentVInfo.nlats = getGribLongKey(gribHandle, "Nj");
            currentVInfo.lon0 = getGribDoubleKey(
                    gribHandle, "longitudeOfFirstGridPointInDegrees");
            currentVInfo.lat0 = getGribDoubleKey(
                    gribHandle, "latitudeOfFirstGridPointInDegrees");
            currentVInfo.lon1 = getGribDoubleKey(
                    gribHandle, "longitudeOfLastGridPointInDegrees");
            currentVInfo.lat1 = getGribDoubleKey(
                    gribHandle, "latitudeOfLastGridPointInDegrees");
            currentVInfo.dlon = getGribDoubleKey(
                    gribHandle, "iDirectionIncrementInDegrees");
            currentVInfo.dlat = getGribDoubleKey(
                    gribHandle, "jDirectionIncrementInDegrees");

            if(gmiInfo.jScanPositively)
            {
                currentVInfo.lat1 = getGribDoubleKey(
                        gribHandle, "latitudeOfFirstGridPointInDegrees");
                currentVInfo.lat0 = getGribDoubleKey(
                        gribHandle, "latitudeOfLastGridPointInDegrees");
            }
            else
            {
                currentVInfo.lat0 = getGribDoubleKey(
                        gribHandle, "latitudeOfFirstGridPointInDegrees");
                currentVInfo.lat1 = getGribDoubleKey(
                        gribHandle, "latitudeOfLastGridPointInDegrees");
            }

            if (dataType == "an") currentVInfo.fcType = ANALYSIS;
            else if (dataType == "fc") currentVInfo.fcType = DETERMINISTIC_FORECAST;
            else if (dataType == "ens") currentVInfo.fcType = ENSEMBLE_FORECAST;
            else currentVInfo.fcType = INVALID_TYPE;

            QString previousDataType = "";
            if (vinfo->fcType == ANALYSIS) previousDataType = "an";
            if (vinfo->fcType == DETERMINISTIC_FORECAST) previousDataType = "fc";
            if (vinfo->fcType == ENSEMBLE_FORECAST) previousDataType = "ens";

            // Check, that existing variable info has the same forecast type.
            if (vinfo->fcType != currentVInfo.fcType)
            {
                LOG4CPLUS_WARN(mlog,
                               "Grib message has different GRIB key 'ls.dataType' ("
                                       << dataType
                                       << ") than previously found for "
                                          "variable '" << varName
                                       << "'. Previously found was ("
                                       << previousDataType
                                       << "); skipping grib message.");

                grib_handle_delete(gribHandle);
                continue;
            }

            if (!checkHorizontalConsistencyOfVariableToReference(
                    vinfo, &currentVInfo))
            {
                LOG4CPLUS_WARN(mlog, "Found different geographical "
                                      "region than previously used for "
                                      "variable '" << varName
                        << "'; skipping grib message.");
                grib_handle_delete(gribHandle);
                continue;
            }

            // Copy data to gmiInfo.
            // NOTE: Only "variablename" is required as map key
            // for vinfo when the index is read; all other variables
            // can be ommitted -- they are only read from the first
            // message of the current variable (when a new vinfo is
            // created). This saves a huge amount of data in the index!
            gmiInfo.variablename = vinfo->variablename;
            // end copy
        }
        else
        {
            vinfo = new MGribVariableInfo();
            vinfo->variablename = gmiInfo.variablename = varName;
            vinfo->longname = gmiInfo.longname =
                    getGribStringKey(gribHandle, "parameter.name");
//TODO (mr, 02Oct2014): fill standard name field.
            vinfo->standardname = gmiInfo.standardname = "";
            vinfo->units = gmiInfo.units =
                    getGribStringKey(gribHandle, "parameter.units");

            // If the surface pressure field is in logarithmic surface pressure,
            // the unit is in some GRIB files reflected as the number 25, which
            // gets parsed as a tilde '~'. Set it manually to 'Pa'.
            if (varName == "lnsp" && vinfo->units == "~")
            {
                vinfo->units = gmiInfo.units = "Pa";
            }

            if (dataType == "an") vinfo->fcType = ANALYSIS;
            else if (dataType == "fc") vinfo->fcType = DETERMINISTIC_FORECAST;
            else if (dataType == "ens") vinfo->fcType = ENSEMBLE_FORECAST;
            else vinfo->fcType = INVALID_TYPE;
            gmiInfo.fcType = vinfo->fcType;

            vinfo->nlons = gmiInfo.nlons = getGribLongKey(
                    gribHandle, "Ni");
            vinfo->nlats = gmiInfo.nlats = getGribLongKey(
                    gribHandle, "Nj");
            vinfo->lon0 = gmiInfo.lon0 = getGribDoubleKey(
                    gribHandle, "longitudeOfFirstGridPointInDegrees");
            vinfo->lat0 = gmiInfo.lat0 = getGribDoubleKey(
                    gribHandle, "latitudeOfFirstGridPointInDegrees");
            vinfo->lon1 = gmiInfo.lon1 = getGribDoubleKey(
                    gribHandle, "longitudeOfLastGridPointInDegrees");
            vinfo->lat1 = gmiInfo.lat1 = getGribDoubleKey(
                    gribHandle, "latitudeOfLastGridPointInDegrees");
            vinfo->dlon = gmiInfo.dlon = getGribDoubleKey(
                    gribHandle, "iDirectionIncrementInDegrees");
            vinfo->dlat = gmiInfo.dlat = getGribDoubleKey(
                    gribHandle, "jDirectionIncrementInDegrees");

            if(gmiInfo.jScanPositively)
            {
                vinfo->lat1 = gmiInfo.lat1 = getGribDoubleKey(
                        gribHandle, "latitudeOfFirstGridPointInDegrees");
                vinfo->lat0 = gmiInfo.lat0 = getGribDoubleKey(
                        gribHandle, "latitudeOfLastGridPointInDegrees");
            }
            else
            {
                vinfo->lat0 = gmiInfo.lat0 = getGribDoubleKey(
                        gribHandle, "latitudeOfFirstGridPointInDegrees");
                vinfo->lat1 = gmiInfo.lat1 = getGribDoubleKey(
                        gribHandle, "latitudeOfLastGridPointInDegrees");
            }

            // Check if grid spans globe in longitude (cyclic in lon).
            double lonWest = MMOD(vinfo->lon0, 360.);
            double lonEast = MMOD(vinfo->lon1 + vinfo->dlon, 360.);
            vinfo->gridIsCyclicInLongitude =
                    floatIsAlmostEqualRelativeAndAbs(
                            lonWest, lonEast, M_LONLAT_RESOLUTION);

            // Fill lat/lon arrays.
            vinfo->lons.resize(vinfo->nlons);
            double lon0 = (vinfo->lon0 > vinfo->lon1) ?
                          vinfo->lon0 - 360. : vinfo->lon0;
            for (int ilon = 0; ilon < vinfo->nlons; ilon ++)
                vinfo->lons[ilon] = lon0 + ilon*vinfo->dlon;
            gmiInfo.lons = vinfo->lons;

            vinfo->lats.resize(vinfo->nlats);
            for (int ilat = 0; ilat < vinfo->nlats; ilat ++)
                vinfo->lats[ilat] = vinfo->lat0 - ilat*vinfo->dlat;
            gmiInfo.lats = vinfo->lats;

            vinfo->jScanPositively = gmiInfo.jScanPositively;

            if (levelType == HYBRID_SIGMA_PRESSURE_3D)
            {
                detectSurfacePressureFieldType(&availableFiles);

                vinfo->surfacePressureName =
                gmiInfo.surfacePressureName = surfacePressureFieldName;

                // Read hybrid level coefficients.
                double *akbk = new double[300];
                size_t akbk_len = 300;

                // NOTE: Grib stores half level interface coefficients.
                GRIB_CHECK(grib_get_double_array(gribHandle, "pv",
                                                 akbk, &akbk_len), 0);

                int numLevels = (akbk_len / 2) - 1; // -1 to exclude surface
                vinfo->aki_hPa.resize(numLevels+1);
                vinfo->bki.resize(numLevels+1);
                vinfo->ak_hPa.resize(numLevels);
                vinfo->bk.resize(numLevels);

                for (int il = 0; il <= numLevels; il++)
                {
                    vinfo->aki_hPa[il] = akbk[il] / 100.;
                    vinfo->bki[il] = akbk[il + numLevels+1];

                    if (il < numLevels)
                    {
                        // Compute full level coefficients.
                        vinfo->ak_hPa[il] =
                                akbk[il] + (akbk[il+1]-akbk[il]) / 2.;
                        vinfo->ak_hPa[il] /= 100.; // convert to hPa
                        vinfo->bk[il] =
                                akbk[il + numLevels+1]
                                        + ( akbk[il+1 + numLevels+1]
                                                - akbk[il + numLevels+1] ) / 2.;
                    }
                }

                delete[] akbk;

                gmiInfo.aki_hPa = vinfo->aki_hPa;
                gmiInfo.bki = vinfo->bki;
                gmiInfo.ak_hPa = vinfo->ak_hPa;
                gmiInfo.bk = vinfo->bk;
            }

            if (levelType == AUXILIARY_PRESSURE_3D)
            {
                vinfo->auxiliaryPressureName = auxiliary3DPressureField;

                vinfo->nlev = gmiInfo.nlev;
                vinfo->numberOfVGridUsed = gmiInfo.numberOfVGridUsed;
                vinfo->uuidOfVGrid = gmiInfo.uuidOfVGrid;
            }

            // Insert the new MVariableInfo struct into the variable
            // name map..
            availableDataFields[levelType].insert(
                    vinfo->variablename, vinfo);
            // ..and, if a CF standard name is available, into the std
            // name map.
            if (vinfo->standardname != "")
                availableDataFieldsByStdName[levelType].insert(
                        vinfo->standardname, vinfo);
        }

        // Determine ensemble member of this data field. Deterministic
        // and analysis datafields are stored as member "0".
        long ensMember = 0;
        if (vinfo->fcType == ENSEMBLE_FORECAST)
        {
            if (ensembleIDIsSpecifiedInFileName)
            {
                ensMember = ensembleIDFromFile;
            }
            else
            {
                ensMember = getGribLongKey(gribHandle,
                                           "perturbationNumber");
                // LOG4CPLUS_DEBUG(mlog, "perturbationNumber: " << ensMember);
            }
        }
        vinfo->availableMembers.insert(ensMember);
        vinfo->availableMembers_bitfield |= (Q_UINT64_C(1) << ensMember);
        gmiInfo.ensMember = ensMember;

        // Get time values of this variable.
        long dataDate = getGribLongKey(gribHandle, "time.dataDate");
        // LOG4CPLUS_DEBUG(mlog, "time.dataDate: " << dataDate);
        long dataTime = getGribLongKey(gribHandle, "time.dataTime");
        // LOG4CPLUS_DEBUG(mlog, "time.dataTime: " << dataTime);

        QString initTimeStr = QString("%1_%2").arg(dataDate)
                                              .arg(dataTime, 4, 10, QChar('0'));
        QDateTime initTime = QDateTime::fromString(initTimeStr,
                                                   "yyyyMMdd_hhmm");
        initTime.setTimeSpec(Qt::UTC);
        gmiInfo.initTime = initTime;

        long validityDate = getGribLongKey(gribHandle, "time.validityDate");
        // LOG4CPLUS_DEBUG(mlog, "time.validityDate: " << validityDate);

        long validityTime = getGribLongKey(gribHandle, "time.validityTime");
        // LOG4CPLUS_DEBUG(mlog, "time.validityTime: " << validityTime);

        QString validTimeStr = QString("%1_%2").arg(validityDate)
                                               .arg(validityTime, 4, 10, QChar('0'));
        QDateTime validTime = QDateTime::fromString(validTimeStr,
                                                    "yyyyMMdd_hhmm");
        validTime.setTimeSpec(Qt::UTC);
        gmiInfo.validTime = validTime;

        // Store filename and offset of grib message in index.
        MGribDatafieldInfo *info = nullptr;
        MGribDatafieldFileInfo *fileInfo = nullptr;
        if (vinfo->timeMap[initTime][validTime].contains(ensMember))
        {
            info = &(vinfo->timeMap[initTime][validTime][ensMember]);
            if(info->dataFiles.contains(gribFileName))
            {
                fileInfo = &(info->dataFiles[gribFileName]);
            }
            else
            {
                info->dataFiles.insert(gribFileName, MGribDatafieldFileInfo(gribFileName));
                fileInfo = &(info->dataFiles[gribFileName]);
            }
        }
        else
        {
            info = &(vinfo->timeMap[initTime][validTime][ensMember]);
            info->dataFiles.insert(gribFileName, MGribDatafieldFileInfo(gribFileName));
            fileInfo = &(info->dataFiles[gribFileName]);
        }

        if (fixLNSPmodelLevel)
        {
            level = 0;
        }
        info->applyExp = fixLNSPmodelLevel;

        // LOG4CPLUS_DEBUG(mlog, "vertical.level: " << level);

        if (info->dataFiles[gribFileName].offsetForLevel.keys().contains(level))
        {
            LOG4CPLUS_WARN(mlog, "Level " << level << " of data field "
                                << varName
                                << " already exists; skipping grib message.");
            grib_handle_delete(gribHandle);
            continue;
        }
        fileInfo->offsetForLevel[level] = gmiInfo.filePosition =
                filePosition;

        // Insert level into list of vertical levels for this
        // variable.
        if (!vinfo->levels.contains(level))
        {
            vinfo->levels << level;
        }

        // That's it!
        grib_handle_delete(gribHandle);
        messageCount++;

        // Write index struct to index file.
        gmiInfo.writeToDataStream(&indexDataStream);

        // Update current file position.
        filePosition = ftell(gribfile);
    } // for (grib messages)

    if (messageCount == 0)
    {
        LOG4CPLUS_INFO(mlog, "No grib messages found.");
    }
    else
    {
        LOG4CPLUS_INFO(mlog, "Indexed " << messageCount
                                         << " grib messages.");
    }

    fclose(gribfile);
}


bool MGribReader::isValidGribFile(QString path)
{
    // Try to open the file.
    FILE* gribfile = NULL;
    QByteArray ba = path.toLocal8Bit();
    gribfile = fopen(ba.data(), "r");
    if (!gribfile) return false;

    // Try to get grib handle to first message.
    grib_handle* gribHandle = NULL;
    int err = 0;
    gribHandle = grib_handle_new_from_file(0, gribfile, &err);
    if (gribHandle == NULL) return false;

    // Grib handle could be created; this is indeed a grib file.
    grib_handle_delete(gribHandle);
    fclose(gribfile);
    return true;
}


QStringList MGribReader::getGribIndexStringKeyList(
        grib_index* gribIndex, QString key)
{
    // Convert QString to c string for grip_api.
    QByteArray ba = key.toLocal8Bit();
    char* keyAsCString = ba.data();

    // Get number of index entries for this key.
    size_t gribKeySize;
    GRIB_CHECK(grib_index_get_size(gribIndex, keyAsCString, &gribKeySize), 0);

    // Get keys.
    char** keys = (char**) malloc(gribKeySize * 128 * sizeof(char*));
    GRIB_CHECK(grib_index_get_string(gribIndex, keyAsCString, keys, &gribKeySize), 0);
    QStringList keyList;
    for (size_t n = 0; n < gribKeySize; n++) keyList << keys[n];
    free(keys);

    return keyList;
}


QList<long> MGribReader::getGribIndexLongKeyList(
        grib_index* gribIndex, QString key)
{
    // Convert QString to c string for grip_api.
    QByteArray ba = key.toLocal8Bit();
    char* keyAsCString = ba.data();

    // Get number of index entries for this key.
    size_t gribKeySize;
    GRIB_CHECK(grib_index_get_size(gribIndex, keyAsCString, &gribKeySize), 0);

    // Get keys.
    long* keys = (long*) malloc(gribKeySize * sizeof(long));
    GRIB_CHECK(grib_index_get_long(gribIndex, keyAsCString, keys, &gribKeySize), 0);
    QList<long> keyList;
    for (size_t n = 0; n < gribKeySize; n++) keyList << keys[n];
    free(keys);

    return keyList;
}


QList<double> MGribReader::getGribIndexDoubleKeyList(
        grib_index* gribIndex, QString key)
{
    // Convert QString to c string for grip_api.
    QByteArray ba = key.toLocal8Bit();
    char* keyAsCString = ba.data();

    // Get number of index entries for this key.
    size_t gribKeySize;
    GRIB_CHECK(grib_index_get_size(gribIndex, keyAsCString, &gribKeySize), 0);

    // Get keys.
    double* keys = (double*) malloc(gribKeySize * sizeof(double));
    GRIB_CHECK(grib_index_get_double(gribIndex, keyAsCString, keys, &gribKeySize), 0);
    QList<double> keyList;
    for (size_t n = 0; n < gribKeySize; n++) keyList << keys[n];
    free(keys);

    return keyList;
}

bool MGribReader::isGribKeyMissing(grib_handle* gh, QString key)
{
    int err;
    return grib_is_missing(gh, key.toStdString().c_str(), &err);
}

QString MGribReader::getGribStringKey(grib_handle *gh, QString key)
{
    // Convert QString to c string for grip_api.
    QByteArray ba = key.toLocal8Bit();
    char* keyAsCString = ba.data();

    const int MAX_CHAR_LEN = 128;
    char cval[MAX_CHAR_LEN];
    size_t vlen = MAX_CHAR_LEN;

    int errorCode = grib_get_string(gh, keyAsCString, cval, &vlen);
    if (errorCode != 0)
    {
        QString errorMsg = QString(
                    "Cannot access grib key '%1'. Met.3D requires the "
                    "value of this key, please make sure that the "
                    "key/value pair is correctly defined in your grib "
                    "file. ECCODES error code is %2, message is: %3")
                .arg(keyAsCString).arg(errorCode)
                .arg(grib_get_error_message(errorCode));

        LOG4CPLUS_ERROR(mlog, errorMsg);
        throw MGribError(errorMsg.toStdString(), __FILE__, __LINE__);
    }

    return QString(cval);
}


long MGribReader::getGribLongKey(grib_handle *gh, QString key)
{
    // Convert QString to c string for grip_api.
    QByteArray ba = key.toLocal8Bit();
    char* keyAsCString = ba.data();

    long value;

    int errorCode = grib_get_long(gh, keyAsCString, &value);
    if (errorCode != 0)
    {
        QString errorMsg = QString(
                    "Cannot access grib key '%1'. Met.3D requires the "
                    "value of this key, please make sure that the "
                    "key/value pair is correctly defined in your grib "
                    "file. ECCODES error code is %2, message is: %3")
                .arg(keyAsCString).arg(errorCode)
                .arg(grib_get_error_message(errorCode));

        LOG4CPLUS_ERROR(mlog, errorMsg);
        throw MGribError(errorMsg.toStdString(), __FILE__, __LINE__);
    }

    return value;
}


QList<quint8> MGribReader::getGribByteArrayKey(grib_handle *gh, const QString &key, size_t length)
{
    // Convert QString to c string for grip_api.
    QByteArray ba = key.toLocal8Bit();
    char *keyAsCString = ba.data();

    unsigned char value[length];
    int errorCode = codes_get_bytes(gh, keyAsCString, value, &length);
    if (errorCode != 0)
    {
        QString errorMsg = QString(
                "Cannot access grib key '%1'. Met.3D requires the "
                "value of this key, please make sure that the "
                "key/value pair is correctly defined in your grib "
                "file. ECCODES error code is %2, message is: %3")
                .arg(keyAsCString).arg(errorCode)
                .arg(grib_get_error_message(errorCode));

        LOG4CPLUS_ERROR(mlog, errorMsg);
        throw MGribError(errorMsg.toStdString(), __FILE__, __LINE__);
    }

    QList<uint8_t> res;

    for (size_t i = 0; i < length; i++)
    {
        res << value[i];
    }

    return res;
}


double MGribReader::getGribDoubleKey(grib_handle *gh, QString key)
{
    // Convert QString to c string for grip_api.
    QByteArray ba = key.toLocal8Bit();
    char* keyAsCString = ba.data();

    double value;

    int errorCode = grib_get_double(gh, keyAsCString, &value);
    if (errorCode != 0)
    {
        QString errorMsg = QString(
                    "Cannot access grib key '%1'. Met.3D requires the "
                    "value of this key, please make sure that the "
                    "key/value pair is correctly defined in your grib "
                    "file. ECCODES error code is %2, message is: %3")
                .arg(keyAsCString).arg(errorCode)
                .arg(grib_get_error_message(errorCode));

        LOG4CPLUS_ERROR(mlog, errorMsg);
        throw MGribError(errorMsg.toStdString(), __FILE__, __LINE__);
    }

    return value;
}


void MGribReader::debugPrintLevelTypeMap(MGribLevelTypeMap &m)
{
    QString str;

    for (int ilevt = 0; ilevt < m.keys().size(); ilevt++)
    {
        MVerticalLevelType levt = m.keys()[ilevt];
        str += "\n>" + MStructuredGrid::verticalLevelTypeToString(levt) + "\n";

        for (int ivar = 0; ivar < m[levt].keys().size(); ivar++)
        {
            QString var = m[levt].keys()[ivar];
            str += "\t>" + var + "\n";

            MGribVariableInfo *vi = m[levt][var];
            str += "\t\tvariablename: " + vi->variablename + "\n";
            str += "\t\tlongname: " + vi->longname + "\n";
            str += "\t\tstandardname: " + vi->standardname + "\n";
            str += "\t\tunits: " + vi->units + "\n";
            str += "\t\tsurfacePressureName: " + vi->surfacePressureName + "\n";

            str += QString("\t\tnlons/nlats: %1/%2; ")
                    .arg(vi->nlons).arg(vi->nlats);
            str += QString("lon0/lat0: %1/%2; lon1/lat1: %3/%4; ")
                    .arg(vi->lon0).arg(vi->lat0).arg(vi->lon1).arg(vi->lat1);
            str += QString("dlon/dlat: %1/%2\n")
                    .arg(vi->dlon).arg(vi->dlat);

            str += QString("\t\tlevels -- %1\n")
                    .arg(keyListToString<double>(vi->levels.toList()));
            str += QString("\t\tlons -- %1\n")
                    .arg(keyListToString<double>(vi->lons.toList()));
            str += QString("\t\tlats -- %1\n")
                    .arg(keyListToString<double>(vi->lats.toList()));
            str += QString("\t\taki -- %1\n")
                    .arg(keyListToString<double>(vi->aki_hPa.toList()));
            str += QString("\t\tbki -- %1\n")
                    .arg(keyListToString<double>(vi->bki.toList()));
            str += QString("\t\tavailableMembers -- %1\n")
                    .arg(keyListToString<unsigned int>(vi->availableMembers.values()));

            for (int iit = 0; iit < vi->timeMap.keys().size(); iit++)
            {
                QDateTime it = vi->timeMap.keys()[iit];
                str += "\t\t>" + it.toString(Qt::ISODate) + "\n";
                for (int ivt = 0; ivt < vi->timeMap[it].keys().size(); ivt++)
                {
                    QDateTime vt = vi->timeMap[it].keys()[ivt];
                    str += "\t\t\t>" + vt.toString(Qt::ISODate) + "\n";

                    MGribEnsembleMemberMap &em = vi->timeMap[it][vt];
                    for (int im = 0; im < em.keys().size(); im++)
                    {
                        int member = em.keys()[im];
                        str += QString("\t\t\t\t>%1\n").arg(member);
                        str +=  QString("\t\t\t\t\tfilenames: \n");

                        for(auto fit = em[member].dataFiles.begin(); fit != em[member].dataFiles.end(); ++fit)
                        {
                            str += QString("\t\t\t\t\t\tfilename: %1\n")
                                    .arg((*fit).filename);
                            str += QString("\t\t\t\t\t\tlevel offsets -- ");

                            for (int il = 0; il < (*fit).offsetForLevel
                                 .keys().size(); il++)
                            {
                                int level = (*fit).offsetForLevel.keys()[il];
                                str += QString("%1:%2/")
                                        .arg(level)
                                        .arg((*fit).offsetForLevel[level]);
                            }
                            str += "\n";
                        }
                    }
                }
            }
        }
    }

    LOG4CPLUS_DEBUG(mlog, "==================> MGribLevelTypeMap:" << str);
}


bool MGribReader::checkInternalConsistencyOfVariableIndex(
        MGribVariableInfo *vinfo)
{
    LOG4CPLUS_INFO(mlog, "Checking if variable '"
                    << vinfo->variablename << "' ("
                    << vinfo->longname << ") is consistent "
                    "in itself (vertical levels, time steps, etc.)...");

    // Sort discovered levels.
    std::sort(vinfo->levels.begin(), vinfo->levels.end());

    // If this is a hybrid sigmal pressure model levels field (i.e. if
    // ak/bk coefficients are available): Check if levels are continous
    // -- Met.3D cannot handle missing levels. We don't need all model
    // levels, though, missing levels at top and/or bottom of the
    // domain are ok.
    if ( !(vinfo->ak_hPa.empty()) )
    {
        bool levelsAreContinous = true;
        for (long ilev = 0; ilev < vinfo->levels.size()-1; ilev++)
            if (vinfo->levels[ilev] != vinfo->levels[ilev+1] - 1)
            {
                levelsAreContinous = false;
                break;
            }

        if (!levelsAreContinous)
        {
            LOG4CPLUS_ERROR(mlog, "Variable '"
                            << vinfo->longname
                            << "' has missing levels ... discarding variable.");
            return false;
        }

        // Get variable info of corresponding surface pressure.
        MGribVariableInfo *sPVInfo = availableDataFields
                .value(SINGLE_LEVEL).value(vinfo->surfacePressureName);
        // Discard variable if surface pressure variable is missing.
        if ((sPVInfo) == nullptr)
        {
            LOG4CPLUS_ERROR(mlog, "Variable '"
                            << vinfo->longname
                            << "' has missing surface pressure field "
                            "... discarding variable.");
            return false;
        }

        // Defining error message to reduce code length.
        QString errorMessage0 = QString("Variable '" + vinfo->longname
                + "' has an inconsistency with its surface pressure variable '"
                + vinfo->surfacePressureName) + "'. Surface pressure field is "
                                                "missing ";
        std::string errorMessage1 =  " ... discarding variable.";

        // Check consistency of surface pressure variable and variable for each
        // member at each valid time step of the given variabel.
        for (const QDateTime &initTime : vinfo->timeMap.keys())
        {
            MGribValidTimeMap validTimeMap = vinfo->timeMap.value(initTime);
            MGribValidTimeMap spValidTimeMap =
                    sPVInfo->timeMap.value(initTime, MGribValidTimeMap());
            if (spValidTimeMap.empty())
            {
                LOG4CPLUS_ERROR(mlog, errorMessage0 << "at initTime "
                                << initTime.toString() << errorMessage1);
                return false;
            }
            for (const QDateTime &validTime : validTimeMap.keys())
            {
                MGribEnsembleMemberMap ensembleMemberMap =
                        validTimeMap.value(validTime);
                MGribEnsembleMemberMap spEnsembleMemberMap =
                        spValidTimeMap.value(validTime,
                                             MGribEnsembleMemberMap());
                if (spEnsembleMemberMap.empty())
                {
                    LOG4CPLUS_ERROR(mlog, errorMessage0 << "at validTime "
                                    << validTime.toString() << " of initTime "
                                    << initTime.toString() << errorMessage1);
                    return false;
                }
                for (int member : ensembleMemberMap.keys())
                {
                    MGribDatafieldInfo datafieldInfo =
                            ensembleMemberMap.value(member);
                    MGribDatafieldInfo spDatafieldInfo =
                            spEnsembleMemberMap.value(member,
                                                      MGribDatafieldInfo());
                    if (spDatafieldInfo.dataFiles.empty())
                    {
                        LOG4CPLUS_ERROR(mlog, errorMessage0
                                        << "for member " << member
                                        << " at validTime "
                                        << validTime.toString()
                                        << " of initTime "
                                        << initTime.toString()
                                        << errorMessage1);
                        return false;
                    }

                    for(auto &spDataFile : spDatafieldInfo.dataFiles)
                    {
                        if (!spDataFile.offsetForLevel.keys().contains(0))
                        {
                            LOG4CPLUS_ERROR(mlog, errorMessage0
                                            << "at level " << 0
                                            << " for member " << member
                                            << " at validTime "
                                            << validTime.toString()
                                            << " of initTime "
                                            << initTime.toString()
                                            << errorMessage1);
                            return false;
                        }
                        if(spDataFile.filename == "")
                        {
                            LOG4CPLUS_ERROR(mlog, errorMessage0
                                            << "for member " << member
                                            << " at validTime "
                                            << validTime.toString()
                                            << " of initTime "
                                            << initTime.toString()
                                            << errorMessage1);
                            return false;
                        }
                    }

                    for (long level = 0; level < vinfo->levels.size() - 1;
                         level++)
                    {
                        bool containedOnce = false;
                        // Loop over all files for this datafield and check, if the level is contained in one of the files.
                        // If not, variable is inconsistent
                        for(auto &dataFile : datafieldInfo.dataFiles)
                        {
                            if (dataFile.offsetForLevel.keys().contains(
                                     vinfo->levels[level]))
                            {
                                containedOnce = true;
                                break;
                            }
                        }

                        if(!containedOnce)
                        {
                            LOG4CPLUS_ERROR(mlog, "Variable '"
                                            << vinfo->longname
                                            << "' has an inconsistency for level "
                                            << vinfo->levels[level]
                                            << " ... discarding variable.");
                            return false;
                        }
                    }
                }
            }
        }
    }
    else
    {
        for (const MGribValidTimeMap& validTimeMap : vinfo->timeMap)
        {
            for (const MGribEnsembleMemberMap& ensembleMemberMap : validTimeMap)
            {
                for (const MGribDatafieldInfo& datafieldInfo : ensembleMemberMap)
                {
                    for (int level = 0; level < vinfo->levels.size() - 1; level++)
                    {
                        // Whether the checked level is contained in any of the files the variable is read from.
                        bool containedOnce = false;

                        // Loop over all files for this datafield and check, if the level is contained in one of the files.
                        // If not, the variable is inconsistent.
                        for (auto &dataFile: datafieldInfo.dataFiles)
                        {
                            if (dataFile.offsetForLevel.contains((long)vinfo->levels[level]))
                            {
                                containedOnce = true;
                                break;
                            }
                        }

                        if(!containedOnce)
                        {
                            LOG4CPLUS_ERROR(mlog, "Variable '"
                                            << vinfo->longname
                                            << "' has an inconsistency for level "
                                            << vinfo->levels[level]
                                            << " ... discarding variable.");

                            containedOnce = false;

                            return false;
                        }
                    }
                }
            }
        }
    }


    // Everything is ok.
    LOG4CPLUS_INFO(mlog, "... OK: variable '"
                    << vinfo->variablename
                    << "' is is consistent in itself.");
    return true;
}


QString MGribReader::forecastTypeToString(MECMWFForecastType type)
{
    switch (type)
    {
    case ANALYSIS:
        return QString("ANA");
    case DETERMINISTIC_FORECAST:
        return QString("DET");
    case ENSEMBLE_FORECAST:
        return QString("ENS");
    default:
        return QString("---");
    }
}


template<typename T> QString MGribReader::keyListToString(QList<T> keyList)
{
    QString str = QString("%1 item(s): ").arg(keyList.size());
    for (int n = 0; n < keyList.size(); n++)
        str += QString("%1/").arg(keyList[n]);
    return str;
}


void MGribReader::detectSurfacePressureFieldType(QStringList *availableFiles)
{
    if (surfacePressureFieldName == "")
    {
#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif
        LOG4CPLUS_INFO(mlog, "Searching for surface pressure field type..."
                              " this may take a while... please wait.");
        // Scan all grib files contained in the directory and search for
        // a surface pressure field. (Only use grib files directly
        // since there is no speed up when using index files.)

        auto progressBar = MProgressBar::getInstance();
        auto taskHandles = progressBar->addTasks(availableFiles->size(),
                                                 "Scanning GRIB files to determine surface pressure variable...");

        for (const QString& gribFileName : *availableFiles)
        {
            if (gribFileName != (*availableFiles)[0])
            {
                progressBar->firstTaskCompleted(taskHandles);
            }

            // (Skip index files.)
            if (gribFileName.contains(GRIB_INDEX_FILE_EXT)) continue;
            QString filePath = dataRoot.filePath(gribFileName);
            // Open the file.
            FILE* gribfile = NULL;
            QByteArray ba = filePath.toLocal8Bit();
            gribfile = fopen(ba.data(), "r");
            if (!gribfile)
            {
                continue;
            }
            // Loop over grib messages contained in the file.
            grib_handle* gribHandle = NULL;
            int err = 0;
            while ((gribHandle =
                    grib_handle_new_from_file(0, gribfile, &err)) != NULL)
            {
                QString shortName = getGribStringKey(gribHandle,
                                                     "parameter.shortName");
                if (shortName == "lnsp")
                {
                    setSurfacePressureFieldName("lnsp");
                    break;
                }
                else if (shortName == "sp")
                {
                    setSurfacePressureFieldName("sp");
                    break;
                }
                grib_handle_delete(gribHandle);
            } // while loop grib handles.
            if (surfacePressureFieldName != "")
            {
                break;
            }
            fclose(gribfile);
        } // foreach loop grib files

        progressBar->tasksCompleted(taskHandles);

        // No surface pressure field found.
        if (surfacePressureFieldName == "")
        {
            LOG4CPLUS_INFO(mlog, "Could not find surface pressure field.");
            // Avoid another search for the surface pressure field type by
            // setting surfacePressureFieldName to "none".
            surfacePressureFieldName = "none";
        }

#ifdef ENABLE_MET3D_STOPWATCH
        stopwatch.split();
        LOG4CPLUS_INFO(mlog, "Surface pressure field type detected in "
                        << stopwatch.getElapsedTime(MStopwatch::SECONDS)
                        << " seconds.");
#endif
    } // if Surface pressure field type empty.
}


bool MGribReader::checkHorizontalConsistencyOfVariableToReference(
        MGribVariableInfo *referenceVInfo,  MGribVariableInfo *currentVInfo,
        bool determineLonIndexShiftForCyclicGrid)
{
    // Get geographical region of the data field & check that
    // all messages of this 3D field have the same bounds.
    if ( referenceVInfo->nlons != currentVInfo->nlons )
    {
        LOG4CPLUS_ERROR(mlog, "Detected inconsistency in 'number of longitudes'.");
        return false;
    }

    if ( referenceVInfo->nlats != currentVInfo->nlats )
    {
        LOG4CPLUS_ERROR(mlog, "Detected inconsistency in 'number of latitudes'.");
        return false;
    }

    if (currentVInfo->gridIsCyclicInLongitude && determineLonIndexShiftForCyclicGrid)
    {
        // If both grids are cyclic in longitude, test if the current grid
        // is shifted in lon w.r.t. reference grid. If yes, determine the
        // index shift that is required to shift the current grid to the lon
        // range of the reference grid.

        if (!referenceVInfo->gridIsCyclicInLongitude)
        {
            LOG4CPLUS_ERROR(mlog, "Grid is cyclic, but reference grid is not.");
            return false;
        }

        if (currentVInfo->lon0 != referenceVInfo->lon0)
        {
            // Determine index shift and store reference lon0. Required in
            // readGrid().
            currentVInfo->longitudinalIndexShiftForCyclicGrid =
                    (referenceVInfo->lon0 - currentVInfo->lon0)
                    / currentVInfo->dlon;

            currentVInfo->lon0ShiftedReference = referenceVInfo->lon0;

            LOG4CPLUS_DEBUG(mlog,
                            "Longitudinal shift required to match reference "
                            "grid (this western lon = " << currentVInfo->lon0
                            << " vs. reference western lon = "
                            << currentVInfo->lon0ShiftedReference
                            << "), index shift is computed to be "
                            << currentVInfo->longitudinalIndexShiftForCyclicGrid
                            << " (dlon is " << currentVInfo->dlon << ")");
        }
    }
    else if ( MMOD(referenceVInfo->lon0, 360.) != MMOD(currentVInfo->lon0, 360.) )
    {
        LOG4CPLUS_ERROR(mlog,
                        "Detected inconsistency in 'longitude of first grid"
                        " point'");
        return false;
    }

    if ( referenceVInfo->lat0 != currentVInfo->lat0 )
    {
        LOG4CPLUS_ERROR(mlog,
                        "Detected inconsistency in 'latitude of first grid"
                        " point'");
        return false;
    }

    if (currentVInfo->gridIsCyclicInLongitude)
    {
        double currentLon1 = currentVInfo->lon1 + (
                    currentVInfo->longitudinalIndexShiftForCyclicGrid
                    * currentVInfo->dlon);
        if ( MMOD(referenceVInfo->lon1, 360.) != MMOD(currentLon1, 360.) )
            {
                LOG4CPLUS_ERROR(mlog,
                                "Detected inconsistency in 'longitude of last grid"
                                " point'");
                return false;
            }
    }
    else if ( MMOD(referenceVInfo->lon1, 360.) != MMOD(currentVInfo->lon1, 360.) )
    {
        LOG4CPLUS_ERROR(mlog,
                        "Detected inconsistency in 'longitude of last grid"
                        " point'");
        return false;
    }

    if ( referenceVInfo->lat1 != currentVInfo->lat1 )
    {
        LOG4CPLUS_ERROR(mlog,
                        "Detected inconsistency in 'latitude of last grid"
                        " point'");
        return false;
    }

    if ( referenceVInfo->dlon != currentVInfo->dlon )
    {
        LOG4CPLUS_ERROR(mlog, "Detected inconsistency in 'i direction increment'");
        return false;
    }

    if ( referenceVInfo->dlat != currentVInfo->dlat )
    {
        LOG4CPLUS_ERROR(mlog, "Detected inconsistency in 'j direction increment'");
        return false;
    }

    return true;
}


bool MGribReader::checkAuxiliaryPressureVerticalGridConsistency(Met3D::MGribVariableInfo *currentVInfo,
                                                                Met3D::MGribVariableInfo *auxPressureVInfo)
{
    if (auxPressureVInfo->nlev != currentVInfo->nlev)
    {
        LOG4CPLUS_ERROR(mlog, "Detected inconsistency in number of vertical levels 'nlev'");
        return false;
    }

    if (auxPressureVInfo->numberOfVGridUsed != currentVInfo->numberOfVGridUsed)
    {
        LOG4CPLUS_ERROR(mlog, "Detected inconsistency in number of vertical grid used 'numberOfVGridUsed'");
        return false;
    }

    // Compare uuidOfVGrid length
    if (auxPressureVInfo->uuidOfVGrid.length() != currentVInfo->uuidOfVGrid.length())
    {
        LOG4CPLUS_ERROR(mlog, "Detected inconsistency in 'uuid of vertical grid'");
        return false;
    }
    // Compare uuidOfVGrid contents if they are equal length.
    for (int i = 0; i < auxPressureVInfo->uuidOfVGrid.length(); i++)
    {
        if (auxPressureVInfo->uuidOfVGrid[i] != currentVInfo->uuidOfVGrid[i])
        {
            LOG4CPLUS_ERROR(mlog, "Detected inconsistency in 'uuid of vertical grid'");
            return false;
        }
    }

    return true;
}


bool MGribReader::checkLevelConsistency(MVerticalLevelType levelType, QString varName, QVector<double> referenceLevels) const
{
    QVector<double> varLevels = availableDataFields[levelType][varName]->levels;
    std::sort(referenceLevels.begin(), referenceLevels.end());
    std::sort(varLevels.begin(), varLevels.end());
    return referenceLevels == varLevels;
}

/******************************************************************************
***                        MGribMessageIndexInfo                            ***
*******************************************************************************/

void MGribMessageIndexInfo::writeToDataStream(QDataStream *dataStream)
{
    (*dataStream) << (quint32)levelType;
    (*dataStream) << variablename;
    (*dataStream) << longname;
    (*dataStream) << standardname;
    (*dataStream) << units;
    (*dataStream) << (quint32)fcType;
    (*dataStream) << surfacePressureName;
    (*dataStream) << nlons;
    (*dataStream) << nlats;
    (*dataStream) << lon0;
    (*dataStream) << lat0;
    (*dataStream) << lon1;
    (*dataStream) << lat1;
    (*dataStream) << dlon;
    (*dataStream) << dlat;
    (*dataStream) << lats;
    (*dataStream) << lons;
    (*dataStream) << aki_hPa;
    (*dataStream) << bki;
    (*dataStream) << ak_hPa;
    (*dataStream) << bk;
    (*dataStream) << jScanPositively;
    (*dataStream) << nlev;
    (*dataStream) << numberOfVGridUsed;
    (*dataStream) << uuidOfVGrid;
    (*dataStream) << ensMember;
    (*dataStream) << initTime;
    (*dataStream) << validTime;
    (*dataStream) << level;
    (*dataStream) << filePosition;

//    QString msg = QString("Wrote %1/IT%2/VT%3/MEM%4/LEV%5")
//            .arg(variablename)
//            .arg(initTime.toString(Qt::ISODate))
//            .arg(validTime.toString(Qt::ISODate))
//            .arg(ensMember)
//            .arg(level);
//    LOG4CPLUS_DEBUG(mlog, msg);
}


void MGribMessageIndexInfo::readFromDataStream(QDataStream *dataStream)
{
    lats.clear();
    lons.clear();
    aki_hPa.clear();
    bki.clear();
    ak_hPa.clear();
    bk.clear();

    // Be careful that the variables appear in the same order as in
    // writeToDataStream()!
    quint32 lT; (*dataStream) >> lT; levelType = MVerticalLevelType(lT);
    (*dataStream) >> variablename;
    (*dataStream) >> longname;
    (*dataStream) >> standardname;
    (*dataStream) >> units;
    quint32 fcT; (*dataStream) >> fcT; fcType = MECMWFForecastType(fcT);
    (*dataStream) >> surfacePressureName;
    (*dataStream) >> nlons;
    (*dataStream) >> nlats;
    (*dataStream) >> lon0;
    (*dataStream) >> lat0;
    (*dataStream) >> lon1;
    (*dataStream) >> lat1;
    (*dataStream) >> dlon;
    (*dataStream) >> dlat;
    (*dataStream) >> lats;
    (*dataStream) >> lons;
    (*dataStream) >> aki_hPa;
    (*dataStream) >> bki;
    (*dataStream) >> ak_hPa;
    (*dataStream) >> bk;
    (*dataStream) >> jScanPositively;
    (*dataStream) >> nlev;
    (*dataStream) >> numberOfVGridUsed;
    (*dataStream) >> uuidOfVGrid;
    (*dataStream) >> ensMember;
    (*dataStream) >> initTime;
    (*dataStream) >> validTime;
    (*dataStream) >> level;
    (*dataStream) >> filePosition;

//    QString msg = QString("Read %1/IT%2/VT%3/MEM%4/LEV%5")
//            .arg(variablename)
//            .arg(initTime.toString(Qt::ISODate))
//            .arg(validTime.toString(Qt::ISODate))
//            .arg(ensMember)
//            .arg(level);
//    LOG4CPLUS_DEBUG(mlog, msg);
}


} // namespace Met3D
