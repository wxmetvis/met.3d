/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MNETCDFFILE_H
#define MNETCDFFILE_H

// standard library imports

// related third party imports
#include <ncDim.h>
#include <ncFile.h>
#include <ncVar.h>
#include <netcdf_mem.h>

// local application imports
#include "gxfw/nwpactorvariable.h"


using namespace Met3D;

namespace netCDF
{
/**
 * This class encapsulates a NetCDF file used for creating it and writing it
 * to the disk. A file can be created, and variables or grids can be added
 * imperatively.
 */
class MNcFile : public NcFile
{
    /**
     * Creates a new file handle for the given path.
     * @param path The file path.
     */
    explicit MNcFile(const QString &path);

    /**
     * Loads a NetCDF file from the given data buffer struct.
     * @param memio The NC_memio buffer struct, containing a pointer to the memory.
     */
    explicit MNcFile(NC_memio memio);

    /**
     * Create a NetCDF file from the data at @p data, with a given size. The data is
     * a NetCDF file buffer in memory.
     */
    explicit MNcFile(void* data, size_t size);

    /**
     * Creates a diskless file. It will not be written to a file path, but
     * instead be available in memory when closing the file, at a given pointer.
     */
    explicit MNcFile();

public:
    ~MNcFile();

public:
    /**
     * Use this static method to create a new file object. Handles exceptions
     * when creating or opening that file.
     * @param path The file path.
     * @return Pointer to the file. Needs to be deleted by the caller!
     */
    static MNcFile* createOrOpenFile(const QString& path);

    /**
     * Create a NetCDF file object from a memory buffer. This can be used, if a
     * diskless NetCDF file has been created and should be loaded by this endpoint.
     * @param io The memory buffer struct.
     * @return The NetCDF file object or nullptr if it could not be created.
     */
    static MNcFile* createFromMemory(NC_memio io);

    /**
     * Create a NetCDF file object from a memory buffer.
     * @param memory The memory buffer
     * @param size The size of the buffer
     * @return The NetCDF file object or nullptr if it could not be created.
     */
    static MNcFile* createFromMemory(void* memory, size_t size);

    /**
     * Create a diskless NetCDF file.
     * The pointer to the file can be accessed via @c getDataPointer().
     * @return The diskless NetCDF file or nullptr if it could not be created.
     */
    static MNcFile* createDisklessFile();

    /**
     * Get the data pointer to the file in memory. This will automatically
     * close the file, no more data can be appended. When this object is
     * destructed, the underlying memory object will be freed.
     * @return The @c NC_memio struct containing pointer to the data, as well
     * as its size.
     */
    NC_memio getDataPointer();

    /**
     * Add an actor variable to the file. This adds the dimensions and performs
     * consistency checks with existing dimensions, and reads the data from the
     * grid to the file.
     * @param var The @ref MNWPActorVariable to add.
     * @return True if the actor variable has been added successfully.
     */
    bool addActorVariable(const MNWPActorVariable *var);

    /**
     * Adds the data from the given structured grid to the file. Metadata is
     * taken from its @ref MWeatherPredictionMetaData super class, and checked
     * for integrity with existing dimensions in the file. If the grid does not
     * have a variable name, it is named "unknown_grid".
     * @param grid The @ref MStructuredGrid to add.
     * @param var A reference populated with the created NC variable.
     * @return True if the grid has been added successfully.
     */
    bool addStructuredGrid(const MStructuredGrid *grid, NcVar &var);

    /**
     * Convenience function calling @c addStructuredGrid(grid, var) without
     * returning a reference to the created variable.
     */
    bool addStructuredGrid(const MStructuredGrid *grid);

    /**
     * Fill a structured grid with data from this file. This method tries to match
     * the variable names in the NetCDF file with the one of the grid. This is a
     * rudimentary reader and should only be used for data which has been written
     * with the exporter in this class.
     * @param grid The grid to populate.
     */
    void fillStructuredGrid(MStructuredGrid* grid) const;

private:
    /**
     * Adds the dimensions of the given grid to the file. If these already exist,
     * perform integrity checks that the coordinates match.
     * @param grid The grid with the dimensions to add.
     * @return A list of dimensions found in the grid. Empty if the dimensions
     * could not be added to the file.
     */
    std::vector<NcDim> addDimensions(const MStructuredGrid *grid);

    /**
     * Adds a dimension to the file with the given data and size. Performs
     * integrity check if such a dimension already exists.
     * @tparam T the type of data to add as given in @c data
     * @tparam ncType the @c netCDF::NcType::ncType to write the data in
     * @param dimName The dimension name to add.
     * @param data The data to add.
     * @param size The size of the data to add.
     * @param dim A reference of the dimension to populate.
     * @return True if integrity holds. The @var dim of the either newly created
     * dimension or the already existing dimension is returned.
     */
    template<typename T, NcType::ncType ncType>
    bool addDimension(const QString &dimName, const T *data,
                      unsigned int size, NcDim &dim) const;

    /**
     * Add standard attributes based on CF conventions to the given dimension.
     * @param dimName Name of the dimension to add attributes to.
     */
    void addDimensionAttributes(const QString &dimName) const;

    /**
     * Add grids depending on the given @c grid to the file. This includes
     * - The surface pressure field if @c grid is a hybrid sigma-pressure field.
     * - The 3-D auxiliary pressure field if @c is a 3-D field on model levels.
     * @param grid The grid to be checked for dependencies to be added.
     */
    void addDependentGrids(const MStructuredGrid* grid);

    /**
     * Sets the vertical level type, performing integrity checks with the
     * already added data before. 2-D and 3-D fields can be co-existent, but not
     * two different types of 3-D fields.
     * @param type The type to check
     * @return True if integrity holds.
     */
    bool setVerticalLevelType(MVerticalLevelType type);

    /**
     * @return Whether the writer supports writing the given level type.
     */
    static bool isVerticalGridTypeSupported(MVerticalLevelType type);

    MVerticalLevelType levelType;
    // List of variable names whose units should be protected and not
    // overwritten by the data from the actor variable. This includes dependent
    // grids like surface pressure and 3-D auxiliary pressure fields, which
    // are transformed in Met.3D to predefined units (Pa/hPa).
    QVector<QString> variableNamesWithProtectedUnits;

    // The memory object created if the NC file is diskless. Need this handle
    // to free the diskless file at destruction time.
    NC_memio inMemoryFile = {0, 0, 0};
};


// template implementation needs to go into header
template<typename T, NcType::ncType ncType>
bool MNcFile::addDimension(const QString& dimName, const T *data,
                           unsigned int size, NcDim &dim) const
{
    NcVar currentVar = getVar(dimName.toStdString());
    if (! currentVar.isNull())
    {
        // integrity check: check if dimension has same size and values as existing one.
        size_t numElements = getDim(dimName.toStdString()).getSize();
        if (numElements != size)
        {
            LOG4CPLUS_ERROR(mlog, "Unable to write netCDF file: Different "
                            "sizes for dimension " << dimName << " detected: "
                            << numElements << " and " << size << ".");
            return false;
        }

        double refData[size];
        currentVar.getVar(refData);

        // Check values with epsilon.
        for (unsigned int i = 0; i < size; i++)
        {
            if (! floatIsAlmostEqualRelativeAndAbs(
                refData[i], data[i], M_LONLAT_RESOLUTION))
            {
                LOG4CPLUS_ERROR(mlog, "Unable to write netCDF file: Different "
                                "values for dimension " << dimName
                                << " detected: "
                                << refData[i] << " and " << data[i] <<
                                " at index " << i << ".");
                return false;
            }
        }

        // Ok, we can add it.
        dim = getDim(dimName.toStdString());
        return true;
    }

    // Create a new dimension and associated variable.
    dim = addDim(dimName.toStdString(), size);
    const NcVar newVar = addVar(dimName.toStdString(), ncType, dim);
    // Populate it with the data and attributes.
    newVar.putVar(data);
    addDimensionAttributes(dimName);
    return true;
}

}

#endif //MNETCDFFILE_H
