/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MMESH_H
#define MET_3D_MMESH_H

// standard library imports

// related third party imports
#include "QVector"
#include "QVector3D"
#include "QVector2D"

// local application imports
#include "abstractdataitem.h"

namespace Met3D
{

/**
 * A data item representing a simple mesh.
 * It has vertices, normals and texture coordinates.
 * It also contains the indices for the triangles, that make up
 * the mesh.
 * Texture coordinates can also be empty, if the mesh does not use
 * textures.
 * Additionally, a float vertex attribute can be added to each vertex.
 */
class MMesh : public MAbstractDataItem
{
public:
    unsigned int getMemorySize_kb() override;
    QVector<QVector3D> vertices;
    QVector<QVector3D> normals;
    QVector<QVector2D> textureCoords;
    QVector<int> indices;
    QVector<float> vertexAttributes;
};

} // Met3D

#endif //MET_3D_MMESH_H
