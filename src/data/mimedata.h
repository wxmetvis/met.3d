/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MMIMETYPE_H
#define MMIMETYPE_H

// standard library imports

// related third party imports
#include <qevent.h>
#include <qmimedata.h>
#include <qmimetype.h>

// local application imports

namespace Met3D
{
/**
  MMimeType implements handling for custom mime types used in Met3D.
  Mime Types are used when dragging and dropping information. For example, when
  dragging a file into a program, data of mime type "text/plain" gets passed to
  the application, with the mime data being the path to the file.

  This class extends on the already existing types to also support our custom
  types, e.g. dragging actors, transfer functions, variables and so on.
 */
class MMimeType : public QMimeType
{
public:
    /**
     * Different types of drag/drop objects supported in Met3D. Currently
     * only actors, but will be extended later with ActorVariables, Data files
     * and so on.
     */
    enum Type : uint8_t
    {
        ACTOR,                      // Dragging an actor.
        ACTOR_FILE,                 // Dragging an actor file (from file explorer).
        LOAD_ACTOR_BUTTON,          // Dragging the load actor button.
        DATASET_CONFIG_FILE,        // Load a dataset config file.
        SELECTABLE_DATA_VARIABLE,   // Dragging a MSelectableDataVariable
        SIZE                        // INVALID
    };

    MMimeType() = default;

    MMimeType(Type t)
        : type(t)
    {
    }

    /**
     * Get our custom met3d prefix for each type of data we handle in
     * mime data structs.
     */
    QString prefix() const
    {
        switch (type)
        {
        case ACTOR:
            return "Met3D:Actor:";
        case ACTOR_FILE:
            return "Met3D:ActorFile:";
        case LOAD_ACTOR_BUTTON:
            return "Met3D:LoadActorButton:";
        case DATASET_CONFIG_FILE:
            return "Met3D:DatasetConfigFile:";
        case SELECTABLE_DATA_VARIABLE:
            return "Met3D:SelectableDataVariable:";
        default:
            return "";
        }
    }

    /**
     * For non-verbose access to the enum.
     */
    constexpr bool operator==(const MMimeType &t) const
    {
        return type == t.type;
    }

    /**
     * This function returns true if this type accepts the mime data
     * received.
     * @param mimeData The mime data to accept or reject.
     * @param truncatedData The truncated data in the mime data object is set
     * by this method if the type is accepted.
     * For example, if @var mimeData data contains "Met3D:Actor:Basemap", then
     * this method returns true if the type is @var MMimeType::ACTOR and the
     * truncatedData is set to "Basemap".
     */
    bool accept(const QMimeData* mimeData, QString &truncatedData) const;

private:
    Type type;
};

/**
 * This implements mime data objects, which are basically objects that
 * can be transfered via drag/drop. They contain information about the format
 * (MimeType), and the data as byte array.
 */
class MMimeData : public QMimeData
{
public:
    /**
     * Constructs a new mime data object from a custom Met3D mime type.
     * @param type The custom mime type, see @var MMimeType.
     * @param data The data associated with the Mime Object.
     */
    MMimeData(const MMimeType& type, const QByteArray& data);
    MMimeData(const MMimeType& type, const QString& data);

    /**
     * Parse from the given @var mimeData the custom @var MMimeType type
     * and the actual data associated with the @var mimeData object.
     * @param mimeData The mime data to parse.
     * @param data Set if parsed successfully to the actual data.
     * @return the MimeType, or MMimeType::SIZE if unable to parse.
     */
    static MMimeType parseMimeData(const QMimeData *mimeData, QString& data);

    /**
     * Helper function if we are not interested in the data itself.
     */
    static MMimeType parseMimeData(const QMimeData *mimeData);

    static const QString TEXT_TYPE; // Accepting type.
};

}; // Met3D

#endif //MMIMETYPE_H
