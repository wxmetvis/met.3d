/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "netcdfwriter.h"

// standard library imports

// related third party imports
#include <ncDim.h>
#include <ncVar.h>

// local application imports
#include "nccfvar.h"
#include "netcdffile.h"

namespace netCDF
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

bool MNetCDFWriter::write(const QList<MNWPActorVariable *> &vars,
                          const QString &filepath)
{
    LOG4CPLUS_INFO(mlog, "Writing NetCDF file to " << filepath);
    if (vars.empty())
    {
        LOG4CPLUS_WARN(mlog, "No variables to write, aborting.");
        return false;
    }

    MNcFile *file = MNcFile::createOrOpenFile(filepath);
    if (! file) return false;

    for (const MNWPActorVariable *met3dVar : vars)
    {
        if (! file->addActorVariable(met3dVar))
        {
            delete file;
            return false;
        }
    }

    delete file; // Automatically closes the file
    LOG4CPLUS_INFO(mlog, "Successfully written the NetCDF file!");
    return true;
}


bool MNetCDFWriter::write(const MStructuredGrid *grid, const QString &filepath)
{
    LOG4CPLUS_INFO(mlog, "Writing NetCDF file to " << filepath.toStdString());

    MNcFile *file = MNcFile::createOrOpenFile(filepath);
    if (! file) return false;

    if (! file->addStructuredGrid(grid)) return false;

    delete file; // Automatically closes the file
    LOG4CPLUS_INFO(mlog, "Successfully written the NetCDF file!");
    return true;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
}