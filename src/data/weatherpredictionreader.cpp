/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus
**  Copyright 2017-2018 Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "weatherpredictionreader.h"

// standard library imports
#include "assert.h"

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"


namespace Met3D
{


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MWeatherPredictionReader::MWeatherPredictionReader(
        QString identifier, QString auxiliary3DPressureField, QString surfacePressureFieldName)
    : MWeatherPredictionDataSource(),
      MAbstractDataReader(identifier),
      auxiliary3DPressureField(auxiliary3DPressureField),
      surfacePressureFieldName(surfacePressureFieldName)
{
    // Name of surface pressure field to reconstruct pressure for hybrid
    // coordinates. If set to "auto" set the string to empty here
    // (will trigger search of GRIB messages for sp/lnsp field in
    // detectSurfacePressureField()). Keep this auto -> "" for backward
    // compatibility of config files as well.
    if (surfacePressureFieldName == "auto")
    {
        this->surfacePressureFieldName = "";
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MStructuredGrid* MWeatherPredictionReader::produceData(MDataRequest request)
{
    MDataRequestHelper rh(request);

    MVerticalLevelType levtype = MVerticalLevelType(rh.intValue("LEVELTYPE"));
    QString variable           = rh.value("VARIABLE");
    QDateTime initTime         = rh.timeValue("INIT_TIME");
    QDateTime validTime        = rh.timeValue("VALID_TIME");
    unsigned int member        = rh.intValue("MEMBER");

    if ((levtype == HYBRID_SIGMA_PRESSURE_3D) && (variable.endsWith("/PSFC")))
    {
        // Special request ("/PSFC" has been appended to the name of a hybrid
        // variable): Return the surface pressure field instead of the variable
        // field.
        variable = variableSurfacePressureName(levtype,
                                               variable.replace("/PSFC", ""));
        levtype  = SINGLE_LEVEL;
    }

    MStructuredGrid* result = nullptr;
    try
    {
        result = readGrid(levtype, variable, initTime, validTime, member);
    }
    catch (const std::exception& e)
    {
        LOG4CPLUS_ERROR(mlog, "Failed to read variable data from file "
                              "-- " << e.what());
        result = nullptr;
    }

    if (result == nullptr)
    {
        // For some reason (invalid datafield requested, file corrupt) no
        // grid could be read. Return nullptr.
        return nullptr;
    }

    if (levtype == HYBRID_SIGMA_PRESSURE_3D)
    {
        // Special treatment for hybrid model levels: Also load the required
        // surface pressure field and set a link to it.
        QString psfcVar = variableSurfacePressureName(levtype, variable);
        rh.insert("LEVELTYPE", SINGLE_LEVEL);
        rh.insert("VARIABLE", psfcVar);

        MDataRequest psfcRequest = rh.request();
        if ( !memoryManager->containsData(this, psfcRequest) )
        {
            // Data field needs to be loaded from disk.
            MRegularLonLatGrid *psfc = static_cast<MRegularLonLatGrid*>(
                        readGrid(SINGLE_LEVEL, psfcVar, initTime,
                                 validTime, member)
                        );
            psfc->setGeneratingRequest(psfcRequest);
            if ( !memoryManager->storeData(this, psfc) )
            {
                // In rare cases another thread could have generated and stored
                // the same data field in the mean time. In such a case the
                // store request will fail. Delete the psfc object, it is not
                // required anymore.
                delete psfc;
            }
        }

        // Get a pointer to the surface pressure field from the memory manger.
        // The field's reference counter was increased by either containsData()
        // or storeData() above. NOTE: The field is released in the destructor
        // of "result" -- the reference is kept for the entire lifetime of
        // "result" to make sure the psfc field is not deleted while "result"
        // is still in memory (notes 09Oct2013).
        static_cast<MLonLatHybridSigmaPressureGrid*>(
                    result
                    )->surfacePressure =
                static_cast<MRegularLonLatGrid*>(
                    memoryManager->getData(this, psfcRequest)
                    );
    } // HYBRID_SIGMA_PRESSURE_3D

    else if (levtype == AUXILIARY_PRESSURE_3D)
    {
        // Special case for auxiliary pressure 3d levels: Link the pressure
        // field to the variable. It has already been requested based on the
        // created task graph below.
        QString pressureVar =
                variableAuxiliaryPressureName(levtype, variable);

        // Do not request the aux-p grid, if we are currently producing it.
        // This would overwrite its self-reference with nullptr.
        if (pressureVar == variable) return result;

        rh.insert("LEVELTYPE", AUXILIARY_PRESSURE_3D);
        rh.insert("VARIABLE", pressureVar);
        
        MDataRequest auxPressureFieldRequest = rh.request();
        auto auxPGrid = dynamic_cast<MLonLatAuxiliaryPressureGrid*>(
                    memoryManager->getData(this, auxPressureFieldRequest));

        // TODO who owns the pressure field?
        // TODO also for surface pressure if hybrid sigma?
        dynamic_cast<MLonLatAuxiliaryPressureGrid*>(result)->auxPressureField_hPa = auxPGrid;

    } // AUXILIARY_PRESSURE_3D

    return result;
}


MTask* MWeatherPredictionReader::createTaskGraph(MDataRequest request)
{
    MTask* task = new MTask(request, this);
    // This task accesses the hard drive.
    task->setDiskReaderTask();

    MDataRequestHelper rh(request);
    MVerticalLevelType levtype = MVerticalLevelType(rh.intValue("LEVELTYPE"));
    QString variable = rh.value("VARIABLE");

    task->setProcessingLabel("Reading variable '" + variable + "'...");

    // If the requested field is of type AUXILIARY_PRESSURE_3D, we need the
    // pressure field as dependency. Otherwise return the plain task.
    if (levtype != AUXILIARY_PRESSURE_3D)
    {
        return task;
    }

    QString pressureVar = variableAuxiliaryPressureName(levtype, variable);
    if (variable == pressureVar)
    {
        // Pressure field itself does not require any other field.
        return task;
    }

    rh.insert("LEVELTYPE", AUXILIARY_PRESSURE_3D);
    rh.insert("VARIABLE", pressureVar);

    // Add the pressure task as parent.
    task->addParent(this->getTaskGraph(rh.request()));
    return task;
}


void MWeatherPredictionReader::setSurfacePressureFieldName(QString surfacePressureFieldName)
{
    // Set surface pressure field type once only.
    if (this->surfacePressureFieldName == "")
    {
        this->surfacePressureFieldName = surfacePressureFieldName;
        LOG4CPLUS_DEBUG(mlog, "Surface pressure field type detected as '"
                        << surfacePressureFieldName.toStdString() << "'");
    }
}



/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MWeatherPredictionReader::locallyRequiredKeys()
{
    return (QStringList() << "LEVELTYPE" << "VARIABLE" << "INIT_TIME"
            << "VALID_TIME" << "MEMBER");
}

} // namespace Met3D
