/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mimedata.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mfileutils.h"
#include "util/mfiletypes.h"
#include "util/mutil.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

const QString MMimeData::TEXT_TYPE = "text/plain";

MMimeData::MMimeData(const MMimeType& type, const QByteArray& data)
{
    // New custom mime data object, build data with type specific met3d-prefix.
    const QString dataWithType = type.prefix() + data;
    setData(TEXT_TYPE, dataWithType.toLocal8Bit());
}


MMimeData::MMimeData(const MMimeType& type, const QString& data)
    : MMimeData(type, data.toLocal8Bit())
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


bool MMimeType::accept(const QMimeData *mimeData, QString &truncatedData) const
{
    truncatedData = mimeData->data(MMimeData::TEXT_TYPE);

    // Check if data starts with one of our custom prefixes.
    if (truncatedData.startsWith(this->prefix()))
    {
        // Remove prefix from data.
        truncatedData.remove(0, this->prefix().size());
        return true; // One of our custom types.
    }

    // Non custom type, so data has entered from outside the application.
    if (mimeData->hasUrls())
    {
        // Files dragged.
        // TODO (cf, 12 Dec 23) Restrict to parse only single files right now.
        if (mimeData->urls().length() == 0) return false;
        QUrl dropFileUrl = mimeData->urls()[0];
        QString fPath(dropFileUrl.toLocalFile());

        // Is it an actor file?
        if (this->type == ACTOR_FILE && FileTypes::M_ACTOR_CONFIG.isType(fPath))
        {
            truncatedData = fPath;
            return true;
        }

        // Is it an dataset config file?
        if (this->type == DATASET_CONFIG_FILE && FileTypes::M_PIPELINE_CONFIG.isType(fPath))
        {
            truncatedData = fPath;
            return true;
        }
    }
    return false;
}


MMimeType MMimeData::parseMimeData(const QMimeData *mimeData, QString &data)
{
    // Only parse plain/text types.
    if (! mimeData->hasFormat(TEXT_TYPE)) return MMimeType::SIZE;

    data = mimeData->data(TEXT_TYPE);
    // Iterate over our types, and try to accept them.
    for (int i = 0; i != MMimeType::SIZE; ++i)
    {
        MMimeType dropType((MMimeType::Type)i);
        if (dropType.accept(mimeData, data))
        {
            return dropType;
        }
    }

    return MMimeType::SIZE;
}


MMimeType MMimeData::parseMimeData(const QMimeData *mimeData)
{
    QString data;
    return parseMimeData(mimeData, data);
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D
