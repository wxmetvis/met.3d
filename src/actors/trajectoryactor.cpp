/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectoryactor.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "mainwindow.h"
#include "trajectories/pointfilter/pointfilter.h"
#include "trajectories/pointfilter/pointfilterfactory.h"
#include "trajectories/pointsource/pointgeneratorinterfacesettings.h"
#include "trajectories/pointsource/pointsourcefactory.h"
#include "trajectories/filter/trajectoryfilterfactory.h"
#include "util/propertyutils.h"
#include "util/mfiletypes.h"
#include "util/mutil.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryActor::MTrajectoryActor()
    : MNWPMultiVarActor(),
      MBoundingBoxInterface(this, MBoundingBoxConnectionType::VOLUME),
      trajectoryDispatcher(nullptr),
      trajectoryDataCollections(nullptr),
      synchronizationControl(nullptr),
      dataSourceID(""),
      trajectoryMode(MTrajectoryMode::COMPUTED_IN_MET3D),
      renderMode(TRAJECTORY_TUBES),
      renderColorMode(CONSTANT),
      transferFunctionProp(nullptr),
      textureUnitTransferFunction(-1),
      textureUnitShadowMap(-1)
{
    // Create and initialise properties.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

     // Bounding box of the actor.
    renderingBoundingBoxIndex =
        insertBoundingBoxProperty(actorPropertiesSupGroup);

    // Synchronization properties.
    // ====================================
    synchronizationGroupProp = MProperty("Synchronization");
    actorPropertiesSupGroup.addSubProperty(synchronizationGroupProp);

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    synchronizationProp = MEnumProperty("Synchronize with",
                                        sysMC->getSyncControlIdentifiers());
    synchronizationProp.setConfigKey("synchronize_with");
    synchronizationProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;

        MSystemManagerAndControl *sysMC =
                MSystemManagerAndControl::getInstance();
        QString syncID = synchronizationProp.getSelectedEnumName();
        synchronizeWith(sysMC->getSyncControl(syncID));
    });
    synchronizationGroupProp.addSubProperty(synchronizationProp);

    synchronizeInitTimeProp = MBoolProperty("Sync. init time", true);
    synchronizeInitTimeProp.setConfigKey("sync_init_time");
    synchronizeInitTimeProp.registerValueCallback([=]()
    {
        updateTimeProperties();

        if (suppressActorUpdates()) return;

        if (synchronizeInitTimeProp && synchronizationControl != nullptr)
        {
            asynchronousDataRequest();
        }
    });
    synchronizationGroupProp.addSubProperty(synchronizeInitTimeProp);

    synchronizeStartTimeProp = MBoolProperty("Sync. valid with start time", true);
    synchronizeStartTimeProp.setConfigKey("sync_valid_with_start_time");
    synchronizeStartTimeProp.registerValueCallback([=]()
    {
        updateTimeProperties();

        if (suppressActorUpdates()) return;

        if (synchronizeStartTimeProp && synchronizationControl != nullptr)
        {
            asynchronousDataRequest();
        }
    });
    synchronizationGroupProp.addSubProperty(synchronizeStartTimeProp);

    synchronizeParticlePosTimeProp = MBoolProperty("Sync. valid with particles", true);
    synchronizeParticlePosTimeProp.setConfigKey("sync_valid_with_particles");
    synchronizeParticlePosTimeProp.registerValueCallback([=]()
    {
        updateTimeProperties();

        if (suppressActorUpdates()) return;

        if (synchronizeParticlePosTimeProp && synchronizationControl != nullptr)
        {
            asynchronousDataRequest();
        }
    });
    synchronizationGroupProp.addSubProperty(synchronizeParticlePosTimeProp);

    synchronizeEnsembleProp = MBoolProperty("Sync. ensemble", true);
    synchronizeEnsembleProp.setConfigKey("sync_ensemble");
    synchronizeEnsembleProp.registerValueCallback([=]()
    {
        updateEnsembleProperties();

        if (trajectoryDispatcher && trajectoryDispatcher->isInitialized())
        {
            updateSyncPropertyColourHints();
        }

        if (suppressActorUpdates()) return;

        if (synchronizeEnsembleProp && synchronizationControl != nullptr)
        {
            asynchronousDataRequest();
        }
    });
    synchronizationGroupProp.addSubProperty(synchronizeEnsembleProp);

    initTimeProp = MEnumProperty("Initialisation");
    initTimeProp.setConfigKey("init_time");
    initTimeProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;

        updateStartTimeProperty();
        asynchronousDataRequest();
    });
    actorPropertiesSupGroup.addSubProperty(initTimeProp);

    startTimeProp = MEnumProperty("Trajectory start");
    startTimeProp.setConfigKey("trajectory_start_time");
    startTimeProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;

        updateTrajectoryIntegrationTimeProperty();
        asynchronousDataRequest();
    });
    actorPropertiesSupGroup.addSubProperty(startTimeProp);

    particlePosTimeProp = MEnumProperty("Particle positions");
    particlePosTimeProp.setConfigKey("particle_positions");
    particlePosTimeProp.setTooltip("Not selectable for 'tubes' and 'all"
                                        " positions' render mode");
    particlePosTimeProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;

        updateParticlePosTimeProperty();
        asynchronousDataRequest();
    });
    actorPropertiesSupGroup.addSubProperty(particlePosTimeProp);

    // Ensemble.
    QStringList ensembleModeNames;
    ensembleModeNames << "Member" << "All";
    ensembleModeProp = MEnumProperty("Ensemble mode", ensembleModeNames);
    ensembleModeProp.setConfigKey("ensemble_mode");
    ensembleModeProp.setEnabled(false);
    actorPropertiesSupGroup.addSubProperty(ensembleModeProp);

    ensembleSingleMemberProp = MEnumProperty("Ensemble member");
    ensembleSingleMemberProp.setConfigKey("ensemble_member");
    ensembleSingleMemberProp.registerValueCallback([=]()
    {
        updateSyncPropertyColourHints();
        asynchronousDataRequest();
    });
    actorPropertiesSupGroup.addSubProperty(ensembleSingleMemberProp);

    sourceTypeProp = MEnumProperty("Computation source", {"Precomputed", "Computed within Met.3D"}, trajectoryMode);
    sourceTypeProp.setConfigKey("computation_source");
    sourceTypeProp.registerValueCallback([=]()
    {
        auto mode = MTrajectoryMode(sourceTypeProp.value());
        trajectoryMode = mode;
        updateEnabledProperties();

        releaseData();

        // Rebuild the pipelines.
        trajectoryDispatcher->updateSourceMode(mode);
        trajectoryDispatcher->rebuild(pointSourceUIs, pointFilterUIs,
                                      trajectoryFilterUIs);
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(sourceTypeProp);

    precomputationGroupProp = MProperty("Precomputation settings");
    actorPropertiesSupGroup.addSubProperty(precomputationGroupProp);

    // Data source selection.
    selectDataSourceProp = MButtonProperty("Select data source", "Select");
    selectDataSourceProp.registerValueCallback([=]()
    {
        // Let user select a data source for precomputed trajectories.
        if (selectDataSource())
        {

        }
    });
    precomputationGroupProp.addSubProperty(selectDataSourceProp);

    utilizedDataSourceProp = MStringProperty("Data source", "");
    utilizedDataSourceProp.setConfigKey("data_source");
    utilizedDataSourceProp.registerValueCallback([=]() {

        if (utilizedDataSourceProp.value().isEmpty()) return;
        this->dataSourceID = utilizedDataSourceProp.value();

        trajectoryDispatcher->setPrecomputedDataSource(this->dataSourceID);
        updateAuxiliaryVariables();

        updateTimeProperties();
        trajectoryDispatcher->rebuild(trajectoryFilterUIs);

        // Synchronise with synchronisation control if available.
        if (synchronizationControl != nullptr)
        {
            // Set synchronizationControl to nullptr and sync with its
            // original value since otherwise the synchronisation won't
            // be executed.
            MSyncControl *temp = synchronizationControl;
            synchronizationControl = nullptr;
            synchronizeWith(temp);
        }

    });

    precomputationGroupProp.addSubProperty(utilizedDataSourceProp);

    utilizedDataSourceProp.setEditable(false);

    // Property group: Trajectory computation.
    // ====================================
    auto emitDataRequest = [=]()
    {
        asynchronousDataRequest();
    };

    computationGroupProp = MProperty("Computation in Met.3D");
    actorPropertiesSupGroup.addSubProperty(computationGroupProp);

    computeClickProp = MButtonProperty("Compute", "Compute");
    computeClickProp.registerValueCallback([=]()
    {
        forceComputationRequested = true;
        asynchronousDataRequest();
    });
    computationGroupProp.addSubProperty(computeClickProp);

    enableAutoComputationProp = MBoolProperty("Auto computation", true);
    enableAutoComputationProp.setConfigKey("auto_computation_enabled");
    enableAutoComputationProp.registerValueCallback([=]()
    {
        enableActorUpdates(false);
        computeClickProp.setEnabled(!enableAutoComputationProp);
        enableActorUpdates(true);

        if (enableAutoComputationProp && !suppressActorUpdates())
        {
            asynchronousDataRequest();
        }
    });
    enableAutoComputationProp.addSubProperty(enableAutoComputationProp);
    computeClickProp.setEnabled(!enableAutoComputationProp);

    computationSeedGroupProp = MProperty("Seed points (starting positions)");
    computationGroupProp.addSubProperty(computationSeedGroupProp);

    computationAddSeedSourceProp = MButtonProperty("Add seed source", "Add");
    computationAddSeedSourceProp.registerValueCallback([=]()
    {
        // Let the user select the interface from which to generate points.
        MPointGeneratorInterface *selectedInterface =
                MPointSourceFactory::showCreationDialog();
        if (!selectedInterface)
        {
            return;
        }

        // Create a new point source UI instance from this interface.
        MPointGeneratorInterfaceSettings *sourceUI =
                MPointSourceFactory::getInstance()->createUI(
                        selectedInterface, this, &computationSeedGroupProp);

        addPointSource(sourceUI);
    });
    computationSeedGroupProp.addSubProperty(computationAddSeedSourceProp);

    computationPointFilterGroupProp = MProperty("Seed point filters");
    computationGroupProp.addSubProperty(computationPointFilterGroupProp);

    computationAddPointFilterProp = MButtonProperty("Add point filter", "Add");
    computationAddPointFilterProp.registerValueCallback([=]()
    {
        // Create a new point filter if user requests it in dialog.
        QString filterType = MPointFilterFactory::showCreationDialog();
        if (filterType.isEmpty())
        {
            return;
        }

        // Create the UI elements for the filter.
        MPointFilterSettings *filterUI =
                MPointFilterFactory::getInstance()->createUI(
                        filterType, this, &computationPointFilterGroupProp);
        addPointFilter(filterUI);

        if (suppressActorUpdates())
        {
            return;
        }
        releaseData();
        asynchronousDataRequest();
    });
    computationPointFilterGroupProp.addSubProperty(computationAddPointFilterProp);

    uVarProp = MNWPActorVarProperty("Eastward component (m/s)");
    uVarProp.setConfigKey("eastward_component_ms");
    uVarProp.registerValueCallback([=]()
    {
        releaseData();
        trajectoryDispatcher->setWindVariableU(uVarProp);

        synchronizeWith(synchronizationControl, true);
        asynchronousDataRequest();
        emitActorChangedSignal();
    });
    computationGroupProp.addSubProperty(uVarProp);

    vVarProp = MNWPActorVarProperty("Northward component (m/s)");
    vVarProp.setConfigKey("northward_component_ms");
    vVarProp.registerValueCallback([=]()
    {
        releaseData();
        trajectoryDispatcher->setWindVariableV(vVarProp);

        synchronizeWith(synchronizationControl, true);
        asynchronousDataRequest();
        emitActorChangedSignal();
    });
    computationGroupProp.addSubProperty(vVarProp);

    wVarProp = MNWPActorVarProperty("Vertical component (Pa/s)");
    wVarProp.setConfigKey("vertical_component_pas");
    wVarProp.registerValueCallback([=]()
    {
        releaseData();
        trajectoryDispatcher->setWindVariableW(wVarProp);
        synchronizeWith(synchronizationControl, true);
        if (suppressActorUpdates()) return;
        asynchronousDataRequest();
        emitActorChangedSignal();
    });
    computationGroupProp.addSubProperty(wVarProp);

    uVarProp.addVariable(nullptr);
    vVarProp.addVariable(nullptr);
    wVarProp.addVariable(nullptr);

    computationLineTypeProp = MEnumProperty("Line type",
                                            {"Trajectories (path lines)",
                                                 "Streamlines"}, 0);
    computationLineTypeProp.setConfigKey("line_type");
    computationLineTypeProp.registerValueCallback([=]()
    {
        releaseData();
        auto lineType = MTrajectoryComputationLineType(computationLineTypeProp.value());

        // Enable/disable associated properties.
        enableActorUpdates(false);
        switch (lineType)
        {
        case PATH_LINE:
            computationNumSubTimeStepsProp.setEnabled(true);
            computationVertexAtIntegrationTimeStepsProp.setEnabled(true);
            computationIntegrateInitAndValidTimeProp.setEnabled(true);
            computationBackwardIntegrationTimeProp.setEnabled(true);
            computationForwardIntegrationTimeProp.setEnabled(true);
            computationBackwardIntegrationTargetProp.setEnabled(true);
            computationForwardIntegrationTargetProp.setEnabled(true);
            computationStreamlineDeltaSProp.setEnabled(false);
            computationStreamlineLengthProp.setEnabled(false);
            break;
        case STREAM_LINE:
            computationNumSubTimeStepsProp.setEnabled(false);
            computationVertexAtIntegrationTimeStepsProp.setEnabled(false);
            computationIntegrateInitAndValidTimeProp.setEnabled(false);
            computationBackwardIntegrationTimeProp.setEnabled(false);
            computationForwardIntegrationTimeProp.setEnabled(false);
            computationBackwardIntegrationTargetProp.setEnabled(false);
            computationForwardIntegrationTargetProp.setEnabled(false);
            computationStreamlineDeltaSProp.setEnabled(true);
            computationStreamlineLengthProp.setEnabled(true);
            break;
        }
        enableActorUpdates(true);

        if (suppressActorUpdates()) return;
        asynchronousDataRequest();
    });
    computationGroupProp.addSubProperty(computationLineTypeProp);

    computationIntegrationMethodProp = MEnumProperty("Integration method",
                                                     {"Euler (3 iter.)",
                                                          "Runge-Kutta"}, 0);
    computationIntegrationMethodProp.setConfigKey("integration_method");
    computationIntegrationMethodProp.registerValueCallback(emitDataRequest);
    computationGroupProp.addSubProperty(computationIntegrationMethodProp);

    computationInterpolationMethodProp = MEnumProperty(
            "Interpolation method",
            {"as LAGRANTO v2", "trilinear (in lon-lat-lnp)"}, 0);
    computationInterpolationMethodProp.setConfigKey("interpolation_method");
    computationInterpolationMethodProp.registerValueCallback(emitDataRequest);
    computationGroupProp
            .addSubProperty(computationInterpolationMethodProp);

    computationNumSubTimeStepsProp = MIntProperty(
            "Sub-steps per data time step", 12);
    computationNumSubTimeStepsProp.setConfigKey("substeps_per_data_timestep");
    computationNumSubTimeStepsProp.setMinMax(1, 1000);
    computationNumSubTimeStepsProp.setTooltip(
            "internal timestep for the trajectory integrations is taken "
            "to be 1/<this value> of the wind data time interval (default "
            "in LAGRANTO v2 is 1/12); wind data is interpolated in time "
            "to times between available wind data timesteps");
    computationNumSubTimeStepsProp.registerValueCallback(emitDataRequest);
    computationGroupProp.addSubProperty(computationNumSubTimeStepsProp);

    computationIntegrateInitAndValidTimeProp = MBoolProperty(
            "Integrate both init and valid time", false);
    computationIntegrateInitAndValidTimeProp.setConfigKey("integrate_both_init_and_valid_time");
    computationIntegrateInitAndValidTimeProp.setTooltip(
            "Integrate both init and valid times for trajectory computation");
    computationIntegrateInitAndValidTimeProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;
        // Release the data on change of this mode.
        releaseData();

        bool integrateInitAndValidTime = computationIntegrateInitAndValidTimeProp;
        trajectoryDispatcher->setIntegrateInitAndValidTime(integrateInitAndValidTime);

        if (suppressActorUpdates())
        {
            return;
        }

        updateTimeProperties();
        asynchronousDataRequest();
        emitActorChangedSignal();
    });
    computationGroupProp
            .addSubProperty(computationIntegrateInitAndValidTimeProp);

    computationVertexAtIntegrationTimeStepsProp =
        MBoolProperty("Generate vertices for each integration step", false);
    computationVertexAtIntegrationTimeStepsProp.setConfigKey("generate_vertices_for_each_integration_step");
    computationVertexAtIntegrationTimeStepsProp.setTooltip(
        "Compute trajectory vertices for each integration timestep"
        " as defined by \"timestep per data t.interv.\". If disabled, "
        "trajectory vertices are computed for timesteps of the wind "
        "field only.");
    computationVertexAtIntegrationTimeStepsProp.registerValueCallback(emitDataRequest);
    computationGroupProp.addSubProperty(computationVertexAtIntegrationTimeStepsProp);

    // Backward trajectories.

    computationBackwardIntegrationTimeProp = MEnumProperty("Backward integration times");
    computationBackwardIntegrationTimeProp.setTooltip(
        "Backward trajectory integration time relative to "
        "\"trajectory start\" time specified above. Select a "
        "target integration time in the box below.");
    computationBackwardIntegrationTimeProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;
        updateParticlePosTimeProperty();
        asynchronousDataRequest();
    });
    computationGroupProp.addSubProperty(computationBackwardIntegrationTimeProp);

    computationBackwardIntegrationTargetProp = MFloatProperty("Target backward integration time", 0.0);
    computationBackwardIntegrationTargetProp.setConfigKey("backward_integration_time");
    computationBackwardIntegrationTargetProp.setSuffix(" hrs");
    computationBackwardIntegrationTargetProp.setTooltip(
            "Target backward integration time in hours.");
    computationBackwardIntegrationTargetProp.setMinMax(-1000, 0);
    computationBackwardIntegrationTargetProp.registerValueCallback([=]()
    {
        updateTrajectoryIntegrationTimeProperty();
        if (suppressActorUpdates()) return;
        asynchronousDataRequest();
    });
    computationGroupProp.addSubProperty(computationBackwardIntegrationTargetProp);

    // Forward trajectories.

    computationForwardIntegrationTimeProp = MEnumProperty("Forward integration times");
    computationForwardIntegrationTimeProp.setTooltip(
            "Forward trajectory integration time relative to "
            "\"trajectory start\" time specified above. Select a "
            "target integration time in the box below.");
    computationForwardIntegrationTimeProp.registerValueCallback([=]() {
        if (suppressActorUpdates()) return;
        updateParticlePosTimeProperty();
        asynchronousDataRequest();
    });
    computationGroupProp.addSubProperty(computationForwardIntegrationTimeProp);

    computationForwardIntegrationTargetProp = MFloatProperty("Target forward integration time", 0.0);
    computationForwardIntegrationTargetProp.setConfigKey("forward_integration_time");
    computationForwardIntegrationTargetProp.setSuffix(" hrs");
    computationForwardIntegrationTargetProp.setTooltip(
            "Target forward integration time in hours.");
    computationForwardIntegrationTargetProp.setMinMax(0, 1000);
    computationForwardIntegrationTargetProp.registerValueCallback([=]()
    {
        updateTrajectoryIntegrationTimeProperty();
        if (suppressActorUpdates()) return;
        asynchronousDataRequest();
    });
    computationGroupProp.addSubProperty(computationForwardIntegrationTargetProp);


    computationStreamlineDeltaSProp = MDoubleProperty("Segment length (Δs)", 1000.);
    computationStreamlineDeltaSProp.setConfigKey("segment_length_delta_s");
    computationStreamlineDeltaSProp.setMinimum(0.001);
    computationStreamlineDeltaSProp.setDecimals(1);
    computationStreamlineDeltaSProp.setStep(1);
    computationStreamlineDeltaSProp.setTooltip(
        "For streamline computations: segment length (delta s) for "
        "integration (dx(s)/ds = v(x); x = position, v = wind)");
    computationStreamlineDeltaSProp.setEnabled(false);
    computationStreamlineDeltaSProp.registerValueCallback(emitDataRequest);
    computationGroupProp.addSubProperty(computationStreamlineDeltaSProp);

    computationStreamlineLengthProp = MIntProperty("Streamline length", 500);
    computationStreamlineLengthProp.setConfigKey("streamline_length");
    computationStreamlineLengthProp.setMinimum(1);
    computationStreamlineLengthProp.setMaximum(1000000);
    computationStreamlineLengthProp.setTooltip(
        "Length of streamline in terms of number of segments");
    computationStreamlineLengthProp.setEnabled(false);
    computationStreamlineLengthProp.registerValueCallback(emitDataRequest);
    computationGroupProp.addSubProperty(computationStreamlineLengthProp);

    computationRecomputeIntervalProp = MIntProperty("Re-computation interval during interaction", 100);
    computationRecomputeIntervalProp.setConfigKey("recomputation_interval_during_interaction_ms");
    computationRecomputeIntervalProp.setSuffix(" ms");
    computationRecomputeIntervalProp.setTooltip(
        "Minimum time interval between data requests in milliseconds.");
    computationRecomputeIntervalProp.setMinMax(1, 2000);
    computationGroupProp.addSubProperty(computationRecomputeIntervalProp);


    // Trajectory filtering property group.
    // ====================================
    filtersGroupProp = MProperty("Trajectory filters");
    actorPropertiesSupGroup.addSubProperty(filtersGroupProp);

    addTrajectoryFilterProp = MButtonProperty("Add trajectory filter", "Add");
    addTrajectoryFilterProp.registerValueCallback([=]()
    {
        QString selectedFilter = MTrajectoryFilterFactory::showCreationDialog();
        if (selectedFilter.isEmpty()) return;

        MTrajectoryFilterSettings *filterUI =
                MTrajectoryFilterFactory::getInstance()->createUI(
                        selectedFilter, this, &filtersGroupProp);
        addTrajectoryFilter(filterUI);
    });
    filtersGroupProp.addSubProperty(addTrajectoryFilterProp);

    // Property group: Rendering.
    // ====================================
    renderingGroupProp = MProperty("Rendering");
    actorPropertiesSupGroup.addSubProperty(renderingGroupProp);

    // Render mode.
    QStringList renderModeNames;
    renderModeNames << "Entire trajectories as tubes"
                    << "All particle positions" << "Single time positions"
                    << "Tubes and single time positions"
                    << "Backward tubes and single time positions";
    renderModeProp = MEnumProperty("Render mode", renderModeNames, renderMode);
    renderModeProp.setConfigKey("render_mode");
    renderModeProp.registerValueCallback([=]()
    {
        renderMode = static_cast<MTrajectoryRenderType>(renderModeProp.value());

        // The trajectory time property is not needed when the entire
        // trajectories are rendered.
        updateTimeProperties();
        asynchronousDataRequest();
    });
    renderingGroupProp.addSubProperty(renderModeProp);

    // Render colors.
    QStringList renderColorModeNames;
    renderColorModeNames << "Constant" << "Map pressure (hPa)" << "Map variable";
    renderColorModeProp = MEnumProperty("Colour mode", renderColorModeNames, renderColorMode);
    renderColorModeProp.setConfigKey("color_mode");
    renderColorModeProp.registerValueCallback([=]()
    {
        renderColorMode = static_cast<TrajectoryRenderColorMode>(renderColorModeProp.value());

        // Disable/enable properties depending on the mode.
        if (renderColorMode == CONSTANT)
        {
            renderColourProp.setEnabled(true);
            auxVarIndexProp.setEnabled(false);
        }
        else if (renderColorMode == CONSTANT || renderColorMode == PRESSURE)
        {
            renderColourProp.setEnabled(false);
            auxVarIndexProp.setEnabled(false);
        }
        else if (renderColorMode == AUXDATA)
        {
            renderColourProp.setEnabled(false);
            auxVarIndexProp.setEnabled(true);
        }
        asynchronousDataRequest();
        emitActorChangedSignal();
    });
    renderingGroupProp.addSubProperty(renderColorModeProp);

    // Constant colour selector.
    renderColourProp = MColorProperty("Colour", {220, 20, 20});
    renderColourProp.setConfigKey("colour");
    renderColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    renderingGroupProp.addSubProperty(renderColourProp);

    // Choose auxiliary data variable.
    auxVarIndexProp = MEnumProperty("Mapped variable");
    auxVarIndexProp.setConfigKey("mapped_var");
    auxVarIndexProp.setEnabled(false);
    auxVarIndexProp.registerValueCallback([=]()
    {
        releaseData();
        QStringList availableAuxVars = getAvailableAuxiliaryVariables();
        if (auxVarIndexProp <= 0)
        {
            trajectoryDispatcher->setRenderingAuxiliaryVariable(nullptr);
        }
        else if (auxVarIndexProp <= variables.size())
        {
            trajectoryDispatcher->setRenderingAuxiliaryVariable(
                    variables.at(auxVarIndexProp));
        }

        trajectoryDispatcher->updatePipelines(false);
        if (suppressActorUpdates())
        {
            return;
        }
        asynchronousDataRequest();
        emitActorChangedSignal();
    });
    renderingGroupProp.addSubProperty(auxVarIndexProp);

    updateAuxiliaryVariables();

    transferFunctionProp = MTransferFunction1DProperty("Transfer function");
    transferFunctionProp.setConfigKey("transfer_function");
    transferFunctionProp.setTooltip("This transfer function is used "
                                         "for mapping either pressure or the "
                                         "selected auxiliary variable to "
                                         "the trajectory's colour.");
    transferFunctionProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    renderingGroupProp.addSubProperty(transferFunctionProp);

    // Render mode and parameters.
    tubeRadiusProp = MFloatProperty("Tube radius", 0.1);
    tubeRadiusProp.setConfigKey("tube_radius");
    tubeRadiusProp.setMinMax(0.01, 1);
    tubeRadiusProp.setDecimals(2);
    tubeRadiusProp.setStep(0.1);
    tubeRadiusProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    renderingGroupProp.addSubProperty(tubeRadiusProp);

    sphereRadiusProp = MFloatProperty("Sphere radius", 0.01);
    sphereRadiusProp.setConfigKey("sphere_radius");
    sphereRadiusProp.setMinMax(0.01, 1);
    sphereRadiusProp.setDecimals(2);
    sphereRadiusProp.setStep(0.1);
    sphereRadiusProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    renderingGroupProp.addSubProperty(sphereRadiusProp);

    // Property group: Analysis.
    // ====================================
    analysisGroupProp = MProperty("Analysis");
    actorPropertiesSupGroup.addSubProperty(analysisGroupProp);

    outputAsLagrantoASCIIProp = MButtonProperty("Export to Lagranto ASCII file", "execute");
    outputAsLagrantoASCIIProp.registerValueCallback([=]()
    {
        outputAsLagrantoASCIIFile();
    });
    analysisGroupProp.addSubProperty(outputAsLagrantoASCIIProp);

    // ====================================

    auto glRM = MGLResourcesManager::getInstance();

    // Observe name changes of actors.
    connect(glRM, SIGNAL(actorRenamed(MActor*,QString)),
            SLOT(onActorRenamed(MActor*,QString)));

    // Observe change of scene layout. This could change the normal computation
    // of trajectories.
    connect(sysMC->getMainWindow(), SIGNAL(sceneViewLayoutChanged()), this,
            SLOT(onSceneViewLayoutChanged()));
}


MTrajectoryActor::~MTrajectoryActor()
{
    releaseData();
    if (synchronizationControl)
    {
        synchronizationControl->deregisterSynchronizedClass(this);
    }
    delete trajectoryDispatcher;

    if (textureUnitTransferFunction >= 0)
    {
        releaseTextureUnit(textureUnitTransferFunction);
    }

    if (textureUnitShadowMap >= 0)
    {
        releaseTextureUnit(textureUnitShadowMap);
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE  0
#define SHADER_NORMAL_ATTRIBUTE  1
#define SHADER_AUXDATA_ATTRIBUTE 2

void MTrajectoryActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");

    beginCompileShaders(2);

    compileShadersFromFileWithProgressDialog(
        tubeShader, "src/glsl/trajectory_tubes.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                positionSphereShader,
                "src/glsl/trajectory_positions.fx.glsl");

    endCompileShaders();
}


void MTrajectoryActor::saveConfigurationHeader(QSettings *settings)
{
    MNWPMultiVarActor::saveConfigurationHeader(settings);

    settings->beginGroup(MTrajectoryActor::getSettingsID());

    settings->setValue("sourceType", static_cast<int>(trajectoryMode));
    settings->setValue("dataSourceID", dataSourceID);

    saveTrajectoryPipelineConfig(settings);

    settings->endGroup();
}


void MTrajectoryActor::saveConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::saveConfiguration(settings);
    MBoundingBoxInterface::saveConfiguration(settings);
}


void MTrajectoryActor::loadConfigurationHeader(QSettings *settings)
{
    if (!trajectoryDispatcher)
    {
        // Initialize early so loaded actor variables can be set correctly.
        initializeRequestDispatcher();
    }

    MNWPMultiVarActor::loadConfigurationHeader(settings);
    settings->beginGroup(MTrajectoryActor::getSettingsID());

    // Create dynamical pipeline elements specified in session file.
    loadTrajectoryPipelineConfig(settings, false);

    settings->endGroup();

    updateInitTimeProperty();
    updateStartTimeProperty();
    updateEnsembleSingleMemberProperty();
}


void MTrajectoryActor::loadConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::loadConfiguration(settings);
    MBoundingBoxInterface::loadConfiguration(settings);

    if (!trajectoryDispatcher)
    {
        // Initialize early so loaded actor variables can be set correctly.
        initializeRequestDispatcher();
    }
}


void MTrajectoryActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationPrior_V_1_14(settings);
    MBoundingBoxInterface::loadConfiguration(settings);

    if (!trajectoryDispatcher)
    {
        // Initialize early so loaded actor variables can be set correctly.
        initializeRequestDispatcher();
    }

    settings->beginGroup(MTrajectoryActor::getSettingsID());

    MTrajectoryMode mode = static_cast<MTrajectoryMode>(
            settings->value("sourceType", 0).toInt());
    QString loadedDataSourceID = settings->value("dataSourceID", "").toString();

    // Switch to the mode (precomputed or real-time).
    trajectoryMode = mode;
    sourceTypeProp = static_cast<int>(trajectoryMode);

    trajectoryDispatcher->updateSourceMode(mode);

    bool dataSourceAvailable = MSelectDataSourceTable::checkForTrajectoryDataSource(
            loadedDataSourceID);

    if (mode == MTrajectoryMode::PRECOMPUTED && !dataSourceAvailable)
    {
        QString errorMessage =
                QString("Trajectory actor '%1': Data source '%2' loaded from "
                        "settings does not exist. Skipping.")
                        .arg(getName(), loadedDataSourceID);
        LOG4CPLUS_ERROR(mlog, errorMessage);
    }
    else if (mode == MTrajectoryMode::PRECOMPUTED)
    {
        dataSourceID = loadedDataSourceID;
        trajectoryDispatcher->setPrecomputedDataSource(dataSourceID);
        utilizedDataSourceProp = dataSourceID;
        // Data source changed -> auxiliary variables might have changed.
        updateAuxiliaryVariables();
    }

    // Set default values for rendering and coloring mode.
    renderModeProp = settings->value("renderMode", 0).toInt();

    renderColorModeProp = settings->value("renderColorMode", 0).toInt();
    renderColourProp = settings->value("renderColour", QColor(200, 0, 0, 255))
                               .value<QColor>();

    // Load synchronization properties (after the ensemble mode properties
    // have been loaded; sync may overwrite some settings).
    // ===================================================================
    synchronizeInitTimeProp = settings->value("synchronizeInitTime", true).toBool();
    synchronizeStartTimeProp = settings->value("synchronizeStartTime", true)
                                       .toBool();
    synchronizeParticlePosTimeProp = settings
            ->value("synchronizeParticlePosTime", true).toBool();
    synchronizeEnsembleProp = settings->value("synchronizeEnsemble", true).toBool();

    QString syncID = settings->value("synchronizationID", "").toString();

    enableAutoComputationProp = settings->value("enableAutoComputation",
                                                true).toBool();

    computeClickProp.setEnabled(!enableAutoComputationProp);

    QString tfName = settings->value("transferFunction").toString();
    while (!setTransferFunction(tfName))
    {
        if (!MTransferFunction::loadMissingTransferFunction(
                tfName, MTransferFunction1D::staticActorType(),
                "Trajectories Actor ", getName(), settings))
        {
            break;
        }
    }

    tubeRadiusProp = settings->value("tubeRadius", 0.1).toDouble();

    sphereRadiusProp = settings->value("sphereRadius", 0.2).toDouble();

    // Need to load init/valid integration type early before accessing the data
    // source and looking for valid time steps.
    computationIntegrateInitAndValidTimeProp =
            settings->value("integrateInitAndValidTime", false).toBool();
    trajectoryDispatcher->setIntegrateInitAndValidTime(
            computationIntegrateInitAndValidTimeProp.value());

    // Load computation properties.
    int8_t varUIndex = static_cast<int8_t>(settings->value("varUIndex")
                                                   .toInt());
    uVarProp.setIndex(varUIndex);
    int8_t varVIndex = static_cast<int8_t>(settings->value("varVIndex")
                                                   .toInt());
    vVarProp.setIndex(varVIndex);
    int8_t varWIndex = static_cast<int8_t>(settings->value("varWIndex")
                                                   .toInt());
    wVarProp.setIndex(varWIndex);
    auxVarIndexProp = static_cast<int8_t>(settings->value("varAuxIndex").toInt());

    computationLineTypeProp = settings
            ->value("computationLineTypeProperty", 0).toInt();
    computationIntegrationMethodProp = settings
            ->value("computationIterationMethodProperty", 0).toInt();
    computationInterpolationMethodProp = settings
            ->value("computationInterpolationMethodProperty", 0).toInt();
    computationNumSubTimeStepsProp = settings
            ->value("computationNumSubTimeStepsProperty", 12).toInt();
    computationVertexAtIntegrationTimeStepsProp = settings
            ->value("computationVertexAtIntegrationTimeStepsProperty", false)
            .toBool();
    computationForwardIntegrationTimeProp = settings
            ->value("computationIntegrationTimeProperty", 0).toInt();
    computationStreamlineDeltaSProp = settings
            ->value("computationStreamlineDeltaSProperty", 1000.).toDouble();
    computationStreamlineLengthProp = settings
            ->value("computationStreamlineLengthProperty", 500).toInt();
    computationRecomputeIntervalProp = settings
            ->value("computationRecomputeIntervalProperty", 100).toInt();


    loadTrajectoryPipelineConfig(settings, true);

    // Load bounding boxes after initializing pipeline as it requires the
    // bounding box properties to be initialized.
    MBoundingBoxInterface::loadConfiguration(settings);

    settings->endGroup();

    updateEnabledProperties();
    trajectoryDispatcher->updatePipelines();
    trajectoryDispatcher->rebuild(pointSourceUIs, pointFilterUIs,
                                  trajectoryFilterUIs);

    updateInitTimeProperty();
    updateStartTimeProperty();
    updateEnsembleSingleMemberProperty();

    if (!syncID.isEmpty())
    {
        MSystemManagerAndControl *sysMC =
                MSystemManagerAndControl::getInstance();
        if (sysMC->getSyncControlIdentifiers().contains(syncID))
        {
            synchronizeWith(sysMC->getSyncControl(syncID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog,
                           QString(
                                   "Trajectory actor '%1': Synchronization control '%2' does "
                                   "not exist. Setting synchronization control to 'None'.")
                                   .arg(getName(), syncID));

            synchronizeWith(nullptr);
        }
    }

    if (isInitialized())
    {
        // Initial request.
        asynchronousDataRequest();
    }
}


void MTrajectoryActor::onBoundingBoxChanged()
{
    labels.clear();
    if (suppressActorUpdates())
    {
        return;
    }
    asynchronousDataRequest();
}


bool MTrajectoryActor::isConnectedTo(MActor *actor)
{
    if (MNWPMultiVarActor::isConnectedTo(actor))
    {
        return true;
    }

    // This actor is connected to the argument actor if the argument actor is
    // the transfer function of this actor.
    if (transferFunctionProp == actor)
    {
        return true;
    }

    // This actor is connected to the argument actor if the argument actor is
    // a point generating interface for our trajectories.
    auto actorInterface = dynamic_cast<MPointGeneratorInterface *>(actor);
    if (! actorInterface)
    {
        return false; // Actor cannot generate points.
    }

    for (MPointGeneratorInterfaceSettings* sourceUI : pointSourceUIs)
    {
        if (actorInterface == sourceUI->getInterface())
        {
            return true;
        }
    }

    return false;
}


MNWPActorVariable *
MTrajectoryActor::createActorVariable(const MSelectableDataVariable &dataVar)
{
    auto *newVar = new MNWPActorVariable(this);

    newVar->dataSourceID = dataVar.dataSourceID;
    newVar->levelType = dataVar.levelType;
    newVar->variableName = dataVar.variableName;

    return newVar;
}


QList<MVerticalLevelType> MTrajectoryActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>()
            << HYBRID_SIGMA_PRESSURE_3D << PRESSURE_LEVELS_3D
            << LOG_PRESSURE_LEVELS_3D << AUXILIARY_PRESSURE_3D);
}


void MTrajectoryActor::onAddActorVariable(MNWPActorVariable *var)
{
    // Notify the filters in case they display variables.
    enableActorUpdates(false);
    for (MPointFilterSettings* filterUI : pointFilterUIs)
    {
        filterUI->onAddActorVariable(var);
    }
    for (MTrajectoryFilterSettings* filterUI : trajectoryFilterUIs)
    {
        filterUI->onAddActorVariable(var);
    }

    uVarProp.addVariable(var);
    vVarProp.addVariable(var);
    wVarProp.addVariable(var);
    updateAuxiliaryVariables(auxVarIndexProp);

    if (trajectoryDispatcher)
    {
        trajectoryDispatcher->updatePipelines();
    }
    enableActorUpdates(true);
}


void MTrajectoryActor::onChangeActorVariable(MNWPActorVariable *var)
{
    // Notify the filters in case they display variables.
    for (MPointFilterSettings* filterUI : pointFilterUIs)
    {
        filterUI->onChangeActorVariable(var);
    }
    for (MTrajectoryFilterSettings* filterUI : trajectoryFilterUIs)
    {
        filterUI->onChangeActorVariable(var);
    }

    uVarProp.changeVariable(var);
    vVarProp.changeVariable(var);
    wVarProp.changeVariable(var);

    updateAuxiliaryVariables(auxVarIndexProp);
}


void MTrajectoryActor::onDeleteActorVariable(MNWPActorVariable *var)
{
    // Notify the filters in case they display variables.
    for (MPointFilterSettings* filterUI : pointFilterUIs)
    {
        filterUI->onDeleteActorVariable(var);
    }
    for (MTrajectoryFilterSettings* filterUI : trajectoryFilterUIs)
    {
        filterUI->onDeleteActorVariable(var);
    }

    uVarProp.removeVariable(var);
    vVarProp.removeVariable(var);
    wVarProp.removeVariable(var);

    updateAuxiliaryVariables(auxVarIndexProp);
}


void MTrajectoryActor::onSceneViewAdded()
{
    updateSyncPropertyColourHints();
    if (isInitialized())
    {
        asynchronousDataRequest();
    }
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MTrajectoryActor::trajectoryPipelinesChanged()
{
    // Cannot update time information if trajectory dispatcher is not ready.
    if (trajectoryDispatcher && trajectoryDispatcher->isInitialized())
    {
        updateInitTimeProperty();
        updateStartTimeProperty();
        updateEnsembleSingleMemberProperty();
    }
    asynchronousDataRequest();
}


void MTrajectoryActor::onSceneViewLayoutChanged()
{
    asynchronousDataRequest();
}


void MTrajectoryActor::onActorRenamed(MActor *actor, const QString &oldName)
{
    // Rename the point generating source if it is an actor type.
    auto actorInterface = dynamic_cast<MPointGeneratorInterface *>(actor);
    if (! actorInterface)
    {
        return;
    }

    // Rename the group property of the corresponding point source if present.
    for (auto sourceUI : pointSourceUIs)
    {
        if (actorInterface == sourceUI->getInterface())
        {
            sourceUI->renameGroup(actor->getName());
        }
    }
}


void MTrajectoryActor::synchronizeWith(MSyncControl *sync,
                                             bool updateGUIProperties)
{
    // Reset connection to current synchronization control.
    // ====================================================

    // If the variable is currently connected to a sync control, reset the
    // background colours of the start and init time properties (they have
    // been set to red/green from this class to indicate time sync status,
    // see setStartDateTime()) and disconnect the signals.
    if (synchronizationControl != nullptr)
    {
        for (MSceneControl *scene: scenes)
        {
            scene->variableDeletesSynchronizationWith(synchronizationControl);
        }
        synchronizationControl->deregisterSynchronizedClass(this);
    }

    // Connect to new sync control and try to switch to its current times.
    synchronizationControl = sync;

    if (sync != nullptr)
    {
        // Tell the actor's scenes that this variable synchronized with this
        // sync control.
        for (MSceneControl *scene: scenes)
        {
            scene->variableSynchronizesWith(sync);
        }
        sync->registerSynchronizedClass(this);
    }

    // Update "synchronizationProperty".
    // =================================
    if (updateGUIProperties)
    {
        QString displayedSyncID = synchronizationProp.getSelectedEnumName();
        QString newSyncID = (sync == nullptr) ? "None" : sync->getID();
        if (displayedSyncID != newSyncID)
        {
            enableActorUpdates(false);
            synchronizationProp.setEnumItem(newSyncID);
            enableActorUpdates(true);
        }
    }

    // Connect to new sync control and synchronize.
    // ============================================
    if (sync != nullptr)
    {
        if (updateGUIProperties)
        {
            enableActorUpdates(false);
            synchronizeInitTimeProp.setEnabled(true);
            synchronizeStartTimeProp.setEnabled(true);
            synchronizeParticlePosTimeProp.setEnabled(true);
            synchronizeEnsembleProp.setEnabled(true);
            enableActorUpdates(true);
        }

        if (synchronizeInitTimeProp)
        {
            setInitDateTime(sync->initDateTime());
        }
        if (synchronizeStartTimeProp)
        {
            setStartDateTime(sync->validDateTime());
        }
        if (synchronizeParticlePosTimeProp)
        {
            setParticleDateTime(sync->validDateTime());
        }
        if (synchronizeEnsembleProp)
        {
            setEnsembleMember(sync->ensembleMember());
        }

        updateSyncPropertyColourHints();
    }
    else
    {
        // No synchronization. Reset property colours and disable sync
        // checkboxes.
        initTimeProp.resetBackgroundColor();
        startTimeProp.resetBackgroundColor();
        ensembleSingleMemberProp.resetBackgroundColor();
        particlePosTimeProp.resetBackgroundColor();

        if (updateGUIProperties)
        {
            enableActorUpdates(false);
            synchronizeInitTimeProp.setEnabled(false);
            synchronizeStartTimeProp.setEnabled(false);
            synchronizeEnsembleProp.setEnabled(false);
            synchronizeParticlePosTimeProp.setEnabled(false);
            enableActorUpdates(true);
        }
    }

    // Update "synchronize xyz" GUI properties.
    // ========================================
    if (updateGUIProperties)
    {
        updateTimeProperties();
        updateEnsembleProperties();
    }
}


bool MTrajectoryActor::synchronizationEvent(MSynchronizationType syncType,
                                   QVector<QVariant> data)
{
    releaseData();
    switch (syncType)
    {
    case SYNC_INIT_TIME:
    {
        if (! synchronizeInitTimeProp)
        {
            return false;
        }
        enableActorUpdates(false);
        auto datetime = data.at(0).toDateTime();
        bool newInitTimeSet = setInitDateTime(datetime);
        enableActorUpdates(true);

        bool isRequestValid = isCurrentInitAndValidTimeAvailable();
        if (newInitTimeSet && isRequestValid)
        {
            asynchronousDataRequest(true);
        }
        else
        {
            synchronizationControl->synchronizationCompleted(this);
        }
        return newInitTimeSet && isRequestValid;
    }
    case SYNC_VALID_TIME:
    {
        if (! synchronizeStartTimeProp && ! synchronizeParticlePosTimeProp)
        {
            return false;
        }
        enableActorUpdates(false);

        bool newStartTimeSet = false;
        bool newParticleTimeSet = false;
        auto datetime = data.at(0).toDateTime();
        if (synchronizeStartTimeProp)
        {
            newStartTimeSet = setStartDateTime(datetime);
        }
        if (synchronizeParticlePosTimeProp)
        {
            newParticleTimeSet = setParticleDateTime(datetime);
        }
        enableActorUpdates(true);

        bool isRequestValid = isCurrentInitAndValidTimeAvailable();
        if (isRequestValid && (newStartTimeSet || newParticleTimeSet))
        {
            asynchronousDataRequest(true);
        }
        else
        {
            synchronizationControl->synchronizationCompleted(this);
        }
        return isRequestValid && (newStartTimeSet || newParticleTimeSet);
    }
    case SYNC_INIT_VALID_TIME:
    {
        enableActorUpdates(false);
        bool newInitTimeSet = false;
        bool newStartTimeSet = false;
        bool newParticleTimeSet = false;
        auto datetime = data.at(0).toDateTime();
        if (synchronizeInitTimeProp)
        {
            newInitTimeSet = setInitDateTime(datetime);
            updateStartTimeProperty(); // Start times of trajectories might have changed.
        }
        if (synchronizeStartTimeProp)
        {
            newStartTimeSet = setStartDateTime(datetime);
        }
        if (synchronizeParticlePosTimeProp)
        {
            newParticleTimeSet = setParticleDateTime(datetime);
        }
        enableActorUpdates(true);

        bool isRequestValid = isCurrentInitAndValidTimeAvailable();
        if (isRequestValid && (newInitTimeSet || newStartTimeSet || newParticleTimeSet))
        {
            asynchronousDataRequest(true);
        }
        else
        {
            synchronizationControl->synchronizationCompleted(this);
        }
        updateSyncPropertyColourHints();
        return isRequestValid && (newInitTimeSet || newStartTimeSet || newParticleTimeSet);
    }
    case SYNC_ENSEMBLE_MEMBER:
    {
        if (! synchronizeEnsembleProp)
        {
            return false;
        }
        enableActorUpdates(false);
        bool newEnsembleMemberSet = setEnsembleMember(data.at(0).toInt());
        enableActorUpdates(true);
        if (newEnsembleMemberSet)
        {
            asynchronousDataRequest(true);
        }
        else
        {
            synchronizationControl->synchronizationCompleted(this);
        }
        return newEnsembleMemberSet;
    }
    default:
        break;
    }

    return false;
}


void MTrajectoryActor::dataReady(MTrajectoryDataCollectionGroup *data)
{
    // Release old data.
    releaseData();
    trajectoryDataCollections = data;

    for (MTrajectoryDataCollection *collection :
         trajectoryDataCollections->getCollections())
    {
        LOG4CPLUS_TRACE(
            mlog, "Received "
                      << collection->getTrajectories()->getNumTrajectories()
                      << " trajectories from generator "
                      << collection->getPointGenerator());
        // Create vertex buffers for received data.
        collection->createVertexBuffers();
    }

    // We have to wait for data to return to get the actual time steps the
    // trajectory vertices are available.
    // If the available times changed, we might need to re-request the data with
    // a different single time step selection. This will only re-compute the
    // single time positions.
    if (updateParticlePosTimeProperty())
    {
        asynchronousDataRequest();
    }
    else if (synchronizationControl)
    {
        synchronizationControl->synchronizationCompleted(this);
    }

    emitActorChangedSignal();
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MTrajectoryActor::initializeActorResources()
{
    // Default sync control.
    if (! synchronizationControl)
    {
        initializeSyncControl();
    }

    // Initialise texture unit.
    if (textureUnitTransferFunction >= 0)
    {
        releaseTextureUnit(textureUnitTransferFunction);
    }
    textureUnitTransferFunction = assignTextureUnit();

    if (textureUnitShadowMap < 0)
    {
        textureUnitShadowMap = assignTextureUnit();
    }

    if (! trajectoryDispatcher)
    {
        initializeRequestDispatcher();
    }

    // Start synchronization.
    synchronizeWith(MSystemManagerAndControl::getInstance()
                            ->getSyncControl("Synchronization"), true);

    // Parent initialisation.
    MNWPMultiVarActor::initializeActorResources();

    // Load shader program if the returned program is new.
    bool loadShaders = false;
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    loadShaders |= glRM->generateEffectProgram("trajectory_tube",
                                               tubeShader);
    loadShaders |= glRM->generateEffectProgram("trajectory_spheres",
                                               positionSphereShader);

    if (loadShaders)
    {
        reloadShaderEffects();
    }

    connect(trajectoryDispatcher,
            SIGNAL(dispatchCompleted(MTrajectoryDataCollectionGroup*)), this,
            SLOT(dataReady(MTrajectoryDataCollectionGroup*)));
    connect(trajectoryDispatcher, SIGNAL(pipelineChangedSignal()), this,
            SLOT(trajectoryPipelinesChanged()));

    // Initialize names of actor variables and used UI dropdowns.
    for (MNWPActorVariable *variable : variables)
    {
        uVarProp.addVariable(variable);
        vVarProp.addVariable(variable);
        wVarProp.addVariable(variable);
    }

    updateAuxiliaryVariables(auxVarIndexProp);

    updateEnabledProperties();

    synchronizeWith(synchronizationControl, true);
    asynchronousDataRequest();
}


void MTrajectoryActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (! getBBoxConnection(renderingBoundingBoxIndex)->getBoundingBox())
    {
        return; // Bounding box for rendering is not available.
    }

    if (! trajectoryDataCollections || trajectoryDataCollections->isEmpty())
    {
        return; // No data.
    }
    if (sceneView->visualisationParametersHaveChanged())
    {
        // Need a new request if scene view parameters changed, e.g., the
        // vertical scaling of one of the views.
        asynchronousDataRequest();
        return;
    }

    // Current bounding box parameters.
    MBoundingBoxConnection *renderingBBox =
        getBBoxConnection(renderingBoundingBoxIndex);
    auto bboxSouthLat = static_cast<float>(renderingBBox->southLat());
    auto bboxEastLon = static_cast<float>(renderingBBox->eastLon());
    auto bboxNorthLat = static_cast<float>(renderingBBox->northLat());
    auto bboxWestLon = static_cast<float>(renderingBBox->westLon());

    double topP = renderingBBox->topPressure_hPa();
    double bottomP = renderingBBox->bottomPressure_hPa();
    auto pzBottom = static_cast<float>(sceneView->worldZfromPressure(bottomP));
    auto pzTop = static_cast<float>(sceneView->worldZfromPressure(topP));

    // Iterate over every data collection and render it.
    for (MTrajectoryDataCollection *trajectoryData :
         trajectoryDataCollections->getCollections())
    {
        MTrajectories *trajectories = trajectoryData->getTrajectories();
        MTrajectoryNormals *trajectoryNormals = trajectoryData->getNormals();
        if (! trajectories || ! trajectoryNormals)
        {
            // Essential data missing.
            continue;
        }

        MTrajectorySelection *trajectorySelection =
            trajectoryData->getSelection();
        if (! trajectorySelection)
        {
            // If no selection, use all trajectories as selection.
            trajectorySelection = trajectories;
        }
        MTrajectorySelection *singleTimeSelection =
            trajectoryData->getSingleTimeSelection();
        MFloatPerTrajectoryVertexSupplement *auxiliaryVariableData =
            trajectoryData->getAuxiliaryData();
        QString sceneName = sceneView->getScene()->getName();

        // Vertex buffer of auxiliary data.
        GL::MVertexBuffer* auxVB = nullptr;
        if (auxiliaryVariableData)
        {
            auxVB = auxiliaryVariableData->getVertexBuffer();
        }
        else if (trajectoryMode == PRECOMPUTED && vVarProp.value() != nullptr)
        {
            // If we have precomputed trajectories, and the auxiliary field
            // is present in the precomputed data source, take their VB.
            // Look for auxiliary data in precomputed trajectories.
            QString auxVariable = getAvailableAuxiliaryVariables().at(auxVarIndexProp);
            if (trajectories->getAuxDataVarNames().contains(auxVariable))
            {
                auxVB = trajectories->getAuxDataVertexBuffer(auxVariable);
            }
        }

        if ((renderMode == TRAJECTORY_TUBES)
            || (renderMode == TUBES_AND_SINGLETIME)
            || (renderMode == BACKWARDTUBES_AND_SINGLETIME))
        {
            tubeShader->bind();

            tubeShader->setUniformValue("mvpMatrix",
                *(sceneView->getModelViewProjectionMatrix()));
            tubeShader->setUniformValue("pToWorldZParams",
                sceneView->pressureToWorldZParameters());
            tubeShader->setUniformValue("radius", GLfloat(tubeRadiusProp));
            tubeShader->setUniformValue("numObsPerTrajectory",
                trajectories->getNumTimeStepsPerTrajectory());

            // Set shader uniform for defining the type of color rendering.
            bool renderAuxData = auxVB && renderColorMode == AUXDATA;
            tubeShader->setUniformValue("renderAuxData", renderAuxData);

            if (renderMode == BACKWARDTUBES_AND_SINGLETIME)
            {
                tubeShader->setUniformValue("renderTubesUpToIndex",
                                            particlePosTimeProp);
            }
            else
            {
                tubeShader->setUniformValue("renderTubesUpToIndex",
                    trajectories->getNumTimeStepsPerTrajectory());
            }

            // Texture bindings for transfer function for data scalar.
            if (transferFunctionProp)
            {
                transferFunctionProp->getTexture()->bindToTextureUnit(
                    textureUnitTransferFunction);
                tubeShader->setUniformValue("transferFunction",
                    textureUnitTransferFunction);
                tubeShader->setUniformValue("scalarMinimum",
                                            transferFunctionProp->getMinimumValue());
                tubeShader->setUniformValue("scalarMaximum",
                                            transferFunctionProp->getMaximumValue());
            }
            tubeShader->setUniformValue("useTransferFunction",
                                        renderColorMode != CONSTANT);
            tubeShader->setUniformValue("constColour", renderColourProp);

            tubeShader->setUniformValue("volumeBottomSECrnr",
                QVector3D(bboxEastLon, bboxSouthLat, pzBottom));
            CHECK_GL_ERROR;

            tubeShader->setUniformValue("volumeTopNWCrnr",
                QVector3D(bboxWestLon, bboxNorthLat, pzTop));
            CHECK_GL_ERROR;

            sceneView->bindShadowMap(tubeShader, textureUnitShadowMap);

            // Bind trajectories and normals vertex buffer objects.
            trajectories->getVertexBuffer()->attachToVertexAttribute(
                SHADER_VERTEX_ATTRIBUTE);
            trajectoryNormals->getVertexBuffer(sceneName)
                ->attachToVertexAttribute(SHADER_NORMAL_ATTRIBUTE);

            // Bind auxiliary data vertex buffer object.
            if (renderAuxData)
            {
                auxVB->attachToVertexAttribute(SHADER_AUXDATA_ATTRIBUTE);
            }

            glPolygonMode(GL_FRONT_AND_BACK,
                          renderAsWireFrameProp ? GL_LINE : GL_FILL);
            CHECK_GL_ERROR;
            sceneView->setLineWidth(1.0f / sceneView->getViewportUpscaleFactor());
            CHECK_GL_ERROR;
            // Draw the trajectories.
            glMultiDrawArrays(GL_LINE_STRIP_ADJACENCY,
                              trajectorySelection->getStartIndices(),
                              trajectorySelection->getIndexCount(),
                              trajectorySelection->getNumTrajectories());
            CHECK_GL_ERROR;

            // Unbind VBO.
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            CHECK_GL_ERROR;
        }

        // Render sphere points if requested.
        if ((renderMode == ALL_POSITION_SPHERES)
            || (renderMode == SINGLETIME_POSITIONS)
            || (renderMode == TUBES_AND_SINGLETIME)
            || (renderMode == BACKWARDTUBES_AND_SINGLETIME))
        {
            if (renderMode == ALL_POSITION_SPHERES)
            {
                if (trajectorySelection == nullptr)
                {
                    continue;
                }
            }
            else
            {
                if (singleTimeSelection == nullptr)
                {
                    continue;
                }
            }

            positionSphereShader->bindProgram("Normal");

            // Set MVP-matrix and parameters to map pressure to world space in
            // the vertex shader.
            positionSphereShader->setUniformValue("mvpMatrix",
                *(sceneView->getModelViewProjectionMatrix()));
            positionSphereShader->setUniformValue("pToWorldZParams",
                sceneView->pressureToWorldZParameters());
            positionSphereShader->setUniformValue("cameraPosition",
                sceneView->getCamera()->getOrigin());
            positionSphereShader->setUniformValue("cameraUpDir",
                sceneView->getCamera()->getYAxis());
            positionSphereShader->setUniformValue("radius",
                GLfloat(sphereRadiusProp));
            positionSphereShader->setUniformValue("scaleRadius",
                GLboolean(false));

            bool renderAuxData = auxVB && renderColorMode == AUXDATA;
            positionSphereShader->setUniformValue("renderAuxData", renderAuxData);

            sceneView->bindShadowMap(positionSphereShader, textureUnitShadowMap);

            // Texture bindings for transfer function for data scalar (1D
            // texture from transfer function class). The data scalar is stored
            // in the vertex.w component passed to the vertex shader.
            if (transferFunctionProp)
            {
                transferFunctionProp->getTexture()->bindToTextureUnit(
                    textureUnitTransferFunction);
                positionSphereShader->setUniformValue("transferFunction",
                    textureUnitTransferFunction);
                positionSphereShader->setUniformValue("scalarMinimum",
                                                      transferFunctionProp->getMinimumValue());
                positionSphereShader->setUniformValue("scalarMaximum",
                                                      transferFunctionProp->getMaximumValue());
            }
            positionSphereShader->setUniformValue("useTransferFunction",
                renderColorMode != CONSTANT);
            positionSphereShader->setUniformValue("constColour", renderColourProp);

            positionSphereShader->setUniformValue("volumeBottomSECrnr",
                QVector3D(bboxEastLon, bboxSouthLat, pzBottom));
            positionSphereShader->setUniformValue("volumeTopNWCrnr",
                QVector3D(bboxWestLon, bboxNorthLat, pzTop));

            positionSphereShader->setUniformValue("restrictToBoundingBox",
                                                  true);
            CHECK_GL_ERROR;

            // Bind vertex buffer object.
            trajectories->getVertexBuffer()->attachToVertexAttribute(
                SHADER_VERTEX_ATTRIBUTE);

            // Bind auxiliary data vertex buffer object.
            if (renderAuxData)
            {
                auxVB->attachToVertexAttribute(SHADER_AUXDATA_ATTRIBUTE);
            }

            glPolygonMode(GL_FRONT_AND_BACK,
                          renderAsWireFrameProp ? GL_LINE : GL_FILL);
            CHECK_GL_ERROR;
            sceneView->setLineWidth(1.0f / sceneView->getViewportUpscaleFactor());
            CHECK_GL_ERROR;

            if (renderMode == ALL_POSITION_SPHERES)
            {
                glMultiDrawArrays(GL_POINTS,
                                  trajectorySelection->getStartIndices(),
                                  trajectorySelection->getIndexCount(),
                                  trajectorySelection->getNumTrajectories());
            }
            else
            {
                glMultiDrawArrays(GL_POINTS,
                                  singleTimeSelection->getStartIndices(),
                                  singleTimeSelection->getIndexCount(),
                                  singleTimeSelection->getNumTrajectories());
            }
            CHECK_GL_ERROR;

            // Unbind VBO.
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            CHECK_GL_ERROR;
        }
    }
}


void MTrajectoryActor::outputAsLagrantoASCIIFile()
{
    LOG4CPLUS_INFO(mlog, "Writing trajectory data to an ASCII file in "
                         "'LAGANRTO.2' format.");

    if (trajectoryDataCollections->isEmpty())
    {
        LOG4CPLUS_ERROR(mlog, "No trajectory data available.");
        return;
    }

    // Let user specify filename.
    QString fileName = MFileUtils::getSaveFileName(
        nullptr, tr("Save trajectory file"), FileTypes::M_LAGRANTO_2);

    if (fileName.isEmpty())
    {
        LOG4CPLUS_ERROR(mlog, "No filename specified.");
        return;
    }

    QFile asciiFile(fileName);
    if (asciiFile.open(QFile::WriteOnly | QFile::Truncate))
    {
        QTextStream out(&asciiFile);

        // Shortcut to first trajectories for metadata information.
        MTrajectories *t =
            trajectoryDataCollections->getCollections()[0]->getTrajectories();

        // Write header line in the format
        // Reference date 20090129_0600 / Time range    2880 min
        out << "Reference date " << t->getValidTime().toString("yyyyMMdd_hhmm")
            << " / Time range "
            << QString("%1").arg(
                   static_cast<double>(t->getTimes().at(0).secsTo(
                       t->getTimes().at(t->getTimes().size() - 1)))
                       / 60.,
                   7, 'f', 0)
            << " min\n\n";

        // Trajectory data are written in the format:
        // (see https://www.geosci-model-dev.net/8/2569/2015/gmd-8-2569-2015-supplement.pdf)
        //
        //   time      lon     lat     p        TH       IWC     LABEL
        // -----------------------------------------------------------
        //    0.00   -65.42   42.61   859   293.010     0.000     0.000
        //    6.00   -58.64   47.48   713   296.745     0.208     1.000

        // Print header with vertex dimensions (time, lon, lat, pres).
        out << QString(" %1 ").arg("time [h]", -10);
        out << QString(" %1 ").arg("lon", -10);
        out << QString(" %1 ").arg("lat", -10);
        out << QString(" %1 ").arg("p", -20);
        QString underLineHeader = QString(" %1").arg(
            "----------------------------------------------------");

        // Add the auxiliary data variable names to the header line.
        QStringList allAuxDataVarNames = t->getAuxDataVarNames();
        for (int i = 0; i < allAuxDataVarNames.size(); i++)
        {
            out << QString(" %1 ").arg(allAuxDataVarNames.value(i), -20);
            underLineHeader = underLineHeader + "----------------------";
        }
        out << "\n";
        out << underLineHeader + "\n\n";

        // Loop over all available trajectory data objects and write their
        // contents to file.
        for (MTrajectoryDataCollection *collection :
             trajectoryDataCollections->getCollections())
        {
            t = collection->getTrajectories();
            if (t == nullptr)
            {
                continue;
            }

            // Loop over all trajectories.
            for (int iTraj = 0; iTraj < t->getNumTrajectories(); iTraj++)
            {
                // Loop over all times steps per trajectory.
                for (int iTime = 0; iTime < t->getTimes().size(); iTime++)
                {
                    // Get vertex coordinates of current trajectory and time
                    // step.
                    QVector3D v = t->getVertices().at(
                        t->getStartIndices()[iTraj] + iTime);

                    // Compute time difference to start time in hours.
                    double tDiff =
                        static_cast<double>(
                            t->getTimes().at(0).secsTo(t->getTimes().at(iTime)))
                        / 3600.;

                    // Write traj. vertices to file.
                    out << QString(" %1 ").arg(tDiff, -10, 'f', 2);
                    out << QString(" %1 ").arg(v.x(), -10, 'g', 6);
                    out << QString(" %1 ").arg(v.y(), -10, 'g', 6);
                    out << QString(" %1 ").arg(v.z(), -20, 'g', 6);

                    // Write auxiliary data values at this vertex to file.
                    if (t->getSizeOfAuxDataAtVertices() > 0)
                    {
                        for (int iAuxDataVar = 0;
                             iAuxDataVar < allAuxDataVarNames.size();
                             iAuxDataVar++)
                        {
                            float iAuxDataValue =
                                t->getAuxDataAtVertex(
                                     t->getStartIndices()[iTraj] + iTime)
                                    .at(iAuxDataVar);
                            out << QString(" %1 ").arg(iAuxDataValue, -20, 'g',
                                                       6);
                        }
                    }
                    out << Qt::endl;
                }

                out << Qt::endl;
            }
        }
    }

    LOG4CPLUS_INFO(mlog, "Writing trajectory data completed.");
}

void MTrajectoryActor::dataFieldChangedEvent()
{
    MNWPMultiVarActor::dataFieldChangedEvent();
    // Some grid changed. Here, sometimes, we want to trigger a new request.
    // For example, if the smoother has been changed and therefore, the grid
    // changed to a smoothed one, we need to recompute the trajectories.
    // But sync events also trigger this event, since they also change the
    // contents of the grid. So we check here if the actor variable state
    // changed.

    if (trajectoryDataCollections)
    {
        // Get the request that would be emitted currently.
        MTrajectoryDispatchInfo info = buildDispatchInfoFromCurrentState();
        MDataRequest currentRequest =
            trajectoryDispatcher->buildRequestFromDispatchInfo(info);

        // Compare the u/v/w variable parameters for changes.
        MDataRequestHelper currentRequestH(currentRequest);
        MDataRequestHelper lastRequestH(
            trajectoryDataCollections->getGeneratingRequest());

        if (currentRequestH.value("U_VAR") != lastRequestH.value("U_VAR") ||
            currentRequestH.value("V_VAR") != lastRequestH.value("V_VAR") ||
            currentRequestH.value("W_VAR") != lastRequestH.value("W_VAR"))
        {
            releaseData();
            asynchronousDataRequest();
        }
    }
}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

MTrajectoryDispatchInfo MTrajectoryActor::buildDispatchInfoFromCurrentState()
{
    MTrajectoryDispatchInfo info;

    QDateTime initTime = getPropertyTime(initTimeProp);
    QDateTime validTime = getPropertyTime(startTimeProp);
    int member = getEnsembleMember();

    unsigned int lineType = computationLineTypeProp;

    QDateTime endTimeForward, endTimeBackward;

    int startTimeIndex = availableStartTimes.indexOf(validTime);
    if (MTrajectoryComputationLineType(lineType) == PATH_LINE)
    {
        // Get end times forward and backward from enum selection index.
        endTimeForward = availableStartTimes.at(startTimeIndex + computationForwardIntegrationTimeProp);
        int backwardSelectionCount = computationBackwardIntegrationTimeProp.getEnumNames().size();
        // End time backward can be computed from the trajectory computation start subtracting the index
        // of the enum index. The enum is reversed wrt. time.
        endTimeBackward = availableStartTimes.at(
                startTimeIndex - backwardSelectionCount + 1 + computationBackwardIntegrationTimeProp);
    }
    else
    {
        endTimeForward = validTime;
        endTimeBackward = validTime;
    }

    unsigned int integrationMethod = computationIntegrationMethodProp;
    unsigned int interpolationMethod = computationInterpolationMethodProp;
    int numSubTimeSteps = computationNumSubTimeStepsProp;

    bool vertexAtSubTimeSteps =computationVertexAtIntegrationTimeStepsProp;
    double streamlineDeltaS = computationStreamlineDeltaSProp;
    int streamlineLength = computationStreamlineLengthProp;

    info.initTime = initTime;
    info.validTime = validTime;
    info.member = member;
    info.lineType = MTrajectoryComputationLineType(lineType);
    info.endTimeForward = endTimeForward;
    info.endTimeBackward = endTimeBackward;

    info.integrationMethod =
        MTrajectoryComputationIntegrationMethod(integrationMethod);
    info.interpolationMethod =
        MTrajectoryComputationInterpolationMethod(interpolationMethod);
    info.numSubTimeSteps = numSubTimeSteps;
    info.vertexAtSubTimeSteps = vertexAtSubTimeSteps;
    info.streamlineDeltaS = streamlineDeltaS;
    info.streamlineLength = streamlineLength;

    // Add scene specific information for normal computation.
    for (MSceneViewGLWidget *view : getViews())
    {
        QString viewName = view->getScene()->getName();
        QVector2D params = view->pressureToWorldZParameters();
        info.pressureToWorldZNormalsPerView.insert(viewName, params);
    }

    info.renderMode = renderMode;

    if (renderMode == SINGLETIME_POSITIONS || renderMode == TUBES_AND_SINGLETIME ||
        renderMode == BACKWARDTUBES_AND_SINGLETIME)
    {
        // Request single times.
        info.particlePosTimeStep = particlePosTimeProp;
    }
    else
    {
        info.particlePosTimeStep = -1;
    }

    return info;
}


void MTrajectoryActor::asynchronousDataRequest(bool synchronizationRequest)
{
    updateTrajectoryIntegrationTimeProperty();

    // No request if auto computation is off and signal did not get emitted from
    // the computation button.
    if (! enableAutoComputationProp && ! forceComputationRequested)
    {
        return;
    }
    forceComputationRequested = false;

    // No request if there is no view (thus, we cannot compute normals), or the
    // dispatcher is not yet fully initialized, or there are no times.
    if (getViews().empty() || trajectoryDispatcher == nullptr
        || ! trajectoryDispatcher->isInitialized()
        || availableStartTimes.isEmpty())
    {
        if (synchronizationRequest)
        {
            synchronizationControl->synchronizationCompleted(this);
        }
        return;
    }

    if (! isCurrentInitAndValidTimeAvailable())
    {
        // Don't request if user requests unavailable data.
        return;
    }

    // Only trigger data request every so often to keep interactivity. Since
    // mouse movements constantly trigger requests, this makes the display being
    // more and more behind otherwise.
    qint64 requestTime = QDateTime::currentMSecsSinceEpoch();
    int minTimeBetweenRequestsMs = computationRecomputeIntervalProp;
    if (requestTime - timeOfLastRequest < minTimeBetweenRequestsMs)
    {
        if (! timedRequestScheduled)
        {
            // Don't request now, emit the request later. (After the time frame)
            QTimer::singleShot(minTimeBetweenRequestsMs
                                   - (requestTime - timeOfLastRequest),
                               this, SLOT(triggerTimeoutRequest()));
            timedRequestScheduled = true;
        }
        if (synchronizationRequest)
        {
            synchronizationControl->synchronizationCompleted(this);
        }
        return;
    }
    timeOfLastRequest = requestTime;

    MTrajectoryDispatchInfo info =  buildDispatchInfoFromCurrentState();
    // Dispatch the request.
    trajectoryDispatcher->dispatch(info);
}


void MTrajectoryActor::addPointSource(
    MPointGeneratorInterfaceSettings *pointSourceUI)
{
    pointSourceUIs.append(pointSourceUI);

    connect(pointSourceUI, &MPipelineElementSettings::elementRemoved, [=]()
    {
        removePointSource(pointSourceUI);
        if (suppressActorUpdates())
        {
            return;
        }
        releaseData();
        asynchronousDataRequest();
    });

    connect(pointSourceUI, &MPipelineElementSettings::elementChanged, [=]()
    {
        if (suppressActorUpdates())
        {
            return;
        }
        releaseData();
        asynchronousDataRequest();
    });

    // Release the data here to avoid memory leaks when changing the pipeline.
    releaseData();
    trajectoryDispatcher->addPipelineForMet3DTrajectories(pointSourceUI, pointFilterUIs,
                                        trajectoryFilterUIs);
    trajectoryDispatcher->updatePipelines();
}


void MTrajectoryActor::addPointFilter(MPointFilterSettings *filterUI)
{
    // Forward the actor variables to the filter.
    for (MNWPActorVariable *nwpVar : getNWPVariables())
    {
        filterUI->onAddActorVariable(nwpVar);
    }

    pointFilterUIs.append(filterUI);
    // Release the data here to avoid memory leaks when changing the pipeline.
    releaseData();

    connect(filterUI, &MPipelineElementSettings::elementRemoved, [=]()
    {
        removePointFilter(filterUI);
        if (suppressActorUpdates())
        {
            return;
        }
        releaseData();
        asynchronousDataRequest();
    });

    connect(filterUI, &MPipelineElementSettings::elementChanged, [=]()
    {
        if (suppressActorUpdates())
        {
            return;
        }
        releaseData();
        asynchronousDataRequest();
    });

    // Update pipeline, and request new data.
    trajectoryDispatcher->addPointFilter(filterUI);
}


void MTrajectoryActor::addTrajectoryFilter(MTrajectoryFilterSettings *filterUI)
{
    trajectoryFilterUIs.append(filterUI);
    // Release the data here to avoid memory leaks when changing the pipeline.
    releaseData();

    connect(filterUI, &MPipelineElementSettings::elementRemoved, [=]()
    {
        removeTrajectoryFilter(filterUI);
        if (suppressActorUpdates())
        {
            return;
        }
        releaseData();
        asynchronousDataRequest();
    });

    connect(filterUI, &MPipelineElementSettings::elementChanged, [=]()
    {
        if (suppressActorUpdates())
        {
            return;
        }
        releaseData();
        asynchronousDataRequest();
    });

    trajectoryDispatcher->addTrajectoryFilter(filterUI);
}


void MTrajectoryActor::removePointSource(
    MPointGeneratorInterfaceSettings *pointSourceUI)
{
    for (int i = 0; i < pointSourceUIs.size(); i++)
    {
        if (pointSourceUIs[i] == pointSourceUI)
        {
            releaseData();
            // Remove from the pipeline.
            trajectoryDispatcher->removePointSourceByIndex(i);

            // Remove the UI elements and object.
            pointSourceUIs.removeOne(pointSourceUI);
            delete pointSourceUI;

            trajectoryDispatcher->updatePipelines();
            emitActorChangedSignal();
            return;
        }
    }
}


void MTrajectoryActor::removePointFilter(MPointFilterSettings *filterUI)
{
    for (int i = 0; i < pointFilterUIs.size(); i++)
    {
        if (pointFilterUIs[i] == filterUI)
        {
            releaseData();
            // Remove from the pipeline.
            trajectoryDispatcher->removePointFilterByIndex(i);

            // Remove the UI elements and object.
            pointFilterUIs.removeOne(filterUI);
            delete filterUI;

            trajectoryDispatcher->updatePipelines();
            return;
        }
    }
}


void MTrajectoryActor::removeTrajectoryFilter(
    MTrajectoryFilterSettings *filterUI)
{
    for (int i = 0; i < trajectoryFilterUIs.size(); i++)
    {
        if (trajectoryFilterUIs[i] == filterUI)
        {
            releaseData();
            // Remove from the pipeline.
            trajectoryDispatcher->removeTrajectoryFilterByIndex(i);

            // Remove UI.
            trajectoryFilterUIs.removeOne(filterUI);
            delete filterUI;

            trajectoryDispatcher->updatePipelines();
            return;
        }
    }
}


bool MTrajectoryActor::setTransferFunction(const QString &tfName)
{
    return transferFunctionProp.setByName(tfName);
}


void MTrajectoryActor::triggerTimeoutRequest()
{
    timedRequestScheduled = false;
    asynchronousDataRequest(synchronizationControl != nullptr);
}


void MTrajectoryActor::updateTimeProperties()
{
    enableActorUpdates(false);

    bool enableSync = synchronizationControl != nullptr;

    initTimeProp.setEnabled(! (enableSync && synchronizeInitTimeProp));
    startTimeProp.setEnabled(! (enableSync && synchronizeStartTimeProp));

    // Only enable the particle pos time property if we have a render mode where
    // we render particles.
    particlePosTimeProp.setEnabled(
        ! (enableSync && synchronizeParticlePosTimeProp) &&
            renderMode != TRAJECTORY_TUBES && renderMode != ALL_POSITION_SPHERES);

    if (trajectoryDispatcher && trajectoryDispatcher->isInitialized())
    {
        updateInitTimeProperty();
        updateStartTimeProperty();
        updateParticlePosTimeProperty();
        updateEnsembleSingleMemberProperty();
        updateSyncPropertyColourHints();
    }

    enableActorUpdates(true);
}


void MTrajectoryActor::updateEnsembleProperties()
{
    enableActorUpdates(false);

    // If the ensemble is synchronized, disable all properties (they are set
    // via the synchronization control).
    ensembleSingleMemberProp.setEnabled(! synchronizeEnsembleProp);

    enableActorUpdates(true);
}


void MTrajectoryActor::updateSyncPropertyColourHints(MSceneControl *scene)
{
    if (synchronizationControl == nullptr)
    {
        // No synchronization -- reset all property colours.
        initTimeProp.resetBackgroundColor();
        startTimeProp.resetBackgroundColor();
        particlePosTimeProp.resetBackgroundColor();
        ensembleSingleMemberProp.resetBackgroundColor();
    }
    else
    {
        // Init time.
        // ==========
        bool match = (getPropertyTime(initTimeProp)
                      == synchronizationControl->initDateTime());
        QColor colour = match ? QColor(0, 255, 0) : QColor(255, 0, 0);
        if (!synchronizeInitTimeProp)
        {
            initTimeProp.resetBackgroundColor();
        }
        else
        {
            initTimeProp.setBackgroundColor(colour);
        }

        // Valid time.
        // ===========
        match = (getPropertyTime(startTimeProp)
                 == synchronizationControl->validDateTime());
        colour = match ? QColor(0, 255, 0) : QColor(255, 0, 0);
        if (!synchronizeStartTimeProp)
        {
            startTimeProp.resetBackgroundColor();
        }
        else
        {
            startTimeProp.setBackgroundColor(colour);
        }

        // Particle position time.
        // ===========
        match = (getPropertyTime(particlePosTimeProp)
                 == synchronizationControl->validDateTime());
        colour = match ? QColor(0, 255, 0) : QColor(255, 0, 0);
        if (!synchronizeParticlePosTimeProp)
        {
            particlePosTimeProp.resetBackgroundColor();
        }
        else
        {
            particlePosTimeProp.setBackgroundColor(colour);
        }

        // Ensemble.
        // =========
        match =
            (getEnsembleMember() == synchronizationControl->ensembleMember());
        colour = match ? QColor(0, 255, 0) : QColor(255, 0, 0);
        if (!synchronizeEnsembleProp)
        {
            ensembleSingleMemberProp.resetBackgroundColor();
        }
        else
        {
            ensembleSingleMemberProp.setBackgroundColor(colour);
        }
    }
}


bool MTrajectoryActor::setEnsembleMember(int member)
{
    int prevEnsembleMode = ensembleModeProp;

    if (member < 0)
    {
        // Ensemble mean: member == -1. As there are no "mean trajectories"
        // the ensemble mean is interpreted as "render all trajectories".

        // If the ensemble mode is already set to "ALL" return false; nothing
        // needs to be done.
        if (prevEnsembleMode == 1)
        {
            return false;
        }

        // Else set the property.
        ensembleModeProp = 1;
    }
    else
    {
#ifdef DIRECT_SYNCHRONIZATION
        int prevEnsembleMember = getEnsembleMember();
#endif
        // Change ensemble member.
        setEnumPropertyClosest(availableEnsembleMembersAsSortedList, (unsigned int)member,
                               ensembleSingleMemberProp, synchronizeEnsembleProp, getScenes());
        ensembleModeProp = 0;

#ifdef DIRECT_SYNCHRONIZATION
        // Does a new data request need to be emitted?
        if (prevEnsembleMode == 1)
        {
            return true;
        }
        if (prevEnsembleMember != member)
        {
            return true;
        }
        return false;
#endif
    }

    return false;
}


bool MTrajectoryActor::setStartDateTime(const QDateTime &datetime)
{
    return internalSetDateTime(availableStartTimes, datetime,
                               startTimeProp);
}


bool MTrajectoryActor::setParticleDateTime(const QDateTime &datetime)
{
    return internalSetDateTime(availableParticlePosTimes, datetime,
                               particlePosTimeProp);
}


bool MTrajectoryActor::setInitDateTime(const QDateTime &datetime)
{
    return internalSetDateTime(availableInitTimes, datetime, initTimeProp);
}


int MTrajectoryActor::getEnsembleMember()
{
    QString memberString = ensembleSingleMemberProp.getSelectedEnumName();

    bool ok = true;
    int member = memberString.toInt(&ok);
    if (ok)
    {
        return member;
    }
    else
    {
        return -99999;
    }
}


bool MTrajectoryActor::isCurrentInitAndValidTimeAvailable()
{
    QDateTime initTime = getPropertyTime(initTimeProp);
    QDateTime validTime = getPropertyTime(startTimeProp);

    // Invalid if IT != VT but we integrate IT = VT.
    bool integrateInitAndValidTime = computationIntegrateInitAndValidTimeProp;
    if (integrateInitAndValidTime && initTime != validTime)
    {
        return false;
    }
    // Check if IT and VT exists.
    if (! trajectoryDispatcher->availableInitTimes().contains(initTime))
    {
        return false;
    }
    if (! trajectoryDispatcher->availableValidTimes(initTime).contains(
            validTime))
    {
        return false;
    }
    return true;
}


void MTrajectoryActor::updateInitTimeProperty()
{
    enableActorUpdates(false);

    // Get the current init time value.
    QDateTime initTime = getPropertyTime(initTimeProp);

    // Get available init times from the trajectory source. Convert the
    // QDateTime objects to strings for the enum manager.
    availableInitTimes = trajectoryDispatcher->availableInitTimes();
    QStringList timeStrings;
    for (const auto & availableInitTime : availableInitTimes)
    {
        timeStrings << availableInitTime.toString(Qt::ISODate);
    }

    initTimeProp.setEnumNames(timeStrings);

    int newIndex = std::max(0, availableInitTimes.indexOf(initTime));
    initTimeProp = newIndex;
    if (synchronizeInitTimeProp && synchronizationControl != nullptr)
    {
        setInitDateTime(synchronizationControl->initDateTime());
    }

    enableActorUpdates(true);
}


void MTrajectoryActor::updateStartTimeProperty()
{
    // Get the current time values.
    QDateTime initTime = getPropertyTime(initTimeProp);
    QDateTime startTime = getPropertyTime(startTimeProp);
    bool integrateInitAndValidTime = computationIntegrateInitAndValidTimeProp;

    // Get a list of the available start times for the new init time,
    // convert the QDateTime objects to strings for the enum manager.
    if (integrateInitAndValidTime)
    {
        // Advance both init and valid times when computing trajectories.
        // Reanalysis data might use both init and valid time as their
        // current time, therefore we cannot get all valid times for one
        // given init time.
        QList<QDateTime> startTimes;
        for (const auto &iterInitTime : availableInitTimes)
        {
            if (trajectoryDispatcher->availableValidTimes(iterInitTime)
                    .contains(iterInitTime))
            {
                // Init time has a corresponding valid time, add it.
                startTimes.append(iterInitTime);
            }
        }
        availableStartTimes = startTimes;
    }
    else
    {
        // Only advance valid times when computing trajectories.
        // Pick all valid times for given init time.
        availableStartTimes =
            trajectoryDispatcher->availableValidTimes(initTime);
    }

    QStringList startTimeStrings;
    for (const auto &availableStartTime : availableStartTimes)
    {
        startTimeStrings << availableStartTime.toString(Qt::ISODate);
    }

    startTimeProp.suppressValueEvent(true);
    startTimeProp.setEnumNames(startTimeStrings);
    startTimeProp.suppressValueEvent(false);

    int newIndex = std::max(0, availableStartTimes.indexOf(startTime));

    startTimeProp.suppressValueEvent(true);
    startTimeProp = newIndex;
    startTimeProp.suppressValueEvent(false);

    if (synchronizeStartTimeProp && synchronizationControl != nullptr)
    {
        setStartDateTime(synchronizationControl->validDateTime());
    }

    updateTrajectoryIntegrationTimeProperty();
}


bool MTrajectoryActor::updateParticlePosTimeProperty()
{
    enableActorUpdates(false);
    QStringList previousParticlePosTimes = particlePosTimeProp.getEnumNames();
    QStringList newParticlePosTimes;

    bool requiresUpdate = false;
    if (! trajectoryDataCollections
        || trajectoryDataCollections->getCollections().isEmpty())
    {
        particlePosTimeProp.setEnumNames({});
    }
    else
    {
        auto trajectories =
            trajectoryDataCollections->getCollections()[0]->getTrajectories();
        auto trajTimes = trajectories->getTimes();

        // Check whether newly received data has different particle position
        // times compared to the previous data. If so, we have to change the
        // available entries in the particle position list, and also request new
        // single time position data if there are changes.
        if (trajTimes.size() != availableParticlePosTimes.size())
        {
            requiresUpdate = true;
        }
        else
        {
            for (int i = 0; i < trajTimes.size(); i++)
            {
                if (trajTimes[i] != availableParticlePosTimes[i])
                {
                    requiresUpdate = true;
                    break;
                }
            }
        }

        availableParticlePosTimes = trajTimes.toList();
        QDateTime currentValue = getPropertyTime(particlePosTimeProp);

        QStringList particlePosTimeStrings;
        for (const auto &trajTime : trajTimes)
        {
            particlePosTimeStrings << trajTime.toString(Qt::ISODate);
        }

        enableEmissionOfActorChangedSignal(false);
        particlePosTimeProp.setEnumNames(particlePosTimeStrings);
        enableEmissionOfActorChangedSignal(true);
        newParticlePosTimes = particlePosTimeStrings;

        // Try to restore previous time value. If the previous value is not
        // available for the new trajectories, indexOf() returns -1. This is
        // changed to 0, i.e. the first available time value is selected.

        int newIndex = std::max(0, trajTimes.indexOf(currentValue));
        particlePosTimeProp = newIndex;

        // The trajectory time property is not needed when the entire
        // trajectories are rendered.
        switch (renderMode)
        {
        case TRAJECTORY_TUBES:
        case ALL_POSITION_SPHERES:
            particlePosTimeProp.setEnabled(false);
            break;
        case SINGLETIME_POSITIONS:
        case TUBES_AND_SINGLETIME:
        case BACKWARDTUBES_AND_SINGLETIME:
            particlePosTimeProp.setEnabled(! synchronizeParticlePosTimeProp);
            break;
        }
        if (synchronizeParticlePosTimeProp && synchronizationControl != nullptr)
        {
            // Synchronize particle pos with start time.
            QDateTime startTime = QDateTime::fromString(startTimeProp.getSelectedEnumName(),
                                                        Qt::ISODate);
            setParticleDateTime(startTime);
        }
    }
    enableActorUpdates(true);

    return requiresUpdate;
}


bool MTrajectoryActor::updateEnsembleSingleMemberProperty()
{
    // Remember currently set ensemble member in order to restore it below
    // (if getEnsembleMember() returns a value < 0 the list is currently
    // empty; however, since the ensemble members are represented by
    // unsigned ints below we cast this case to 0).
    int prevEnsembleMember = std::max(0, getEnsembleMember());

    // Update ensembleSingleMemberProperty so that the user can choose
    // from the list of available members. (Requires first sorting the set of
    // members as a list, which can then be converted  to a string list).
    availableEnsembleMembersAsSortedList =
        trajectoryDispatcher->availableEnsembleMembers().values();
    std::sort(availableEnsembleMembersAsSortedList.begin(),
              availableEnsembleMembersAsSortedList.end());

    QStringList availableMembersAsStringList;
    for (unsigned int member : availableEnsembleMembersAsSortedList)
    {
        availableMembersAsStringList << QString("%1").arg(member);
    }

    enableActorUpdates(false);
    ensembleSingleMemberProp.setEnumNames(availableMembersAsStringList);
    setEnumPropertyClosest(availableEnsembleMembersAsSortedList, (unsigned int)prevEnsembleMember,
                           ensembleSingleMemberProp, synchronizeEnsembleProp, getScenes());
    enableActorUpdates(true);

    bool displayedMemberHasChanged =
        (getEnsembleMember() != prevEnsembleMember);
    return displayedMemberHasChanged;
}


void MTrajectoryActor::updateTrajectoryIntegrationTimeProperty()
{
    enableActorUpdates(false);

    // Get current start time.
    QDateTime startTime = getPropertyTime(startTimeProp);

    // Determine possible integration times from available time steps
    // in the NWP data source.
    QStringList backwardTimeIntervals, forwardTimeIntervals;
    QList<double> backwardTimeIntervalsHrs, forwardTimeIntervalsHrs;
    for (QDateTime &time : availableStartTimes)
    {
        double timeDiffHrs = static_cast<double>(startTime.secsTo(time)) / 3600.0;
        // Add integration time "+0 hours" to both backward and forward lists.
        if (timeDiffHrs >= 0)
        {
            forwardTimeIntervalsHrs << timeDiffHrs;
            forwardTimeIntervals << QString("%1 hrs").arg(forwardTimeIntervalsHrs.last());
        }
        if (timeDiffHrs <= 0)
        {
            backwardTimeIntervalsHrs << timeDiffHrs;
            backwardTimeIntervals << QString("%1 hrs").arg(backwardTimeIntervalsHrs.last());
        }
    }

    // Update items to be closest to target integration.
    float backwardTargetHrs = computationBackwardIntegrationTargetProp.value();
    float forwardTargetHrs = computationForwardIntegrationTargetProp.value();

    // Find closest available integration times to target. Looking for element with minimum time difference.
    auto closestBackward = std::min_element(backwardTimeIntervalsHrs.begin(), backwardTimeIntervalsHrs.end(),
                                            [&](double x, double y) {
                                                return std::abs(x - backwardTargetHrs) <
                                                       std::abs(y - backwardTargetHrs);
                                            });
    auto closestForward = std::min_element(forwardTimeIntervalsHrs.begin(), forwardTimeIntervalsHrs.end(),
                                           [&](double x, double y) {
                                               return std::abs(x - forwardTargetHrs) < std::abs(y - forwardTargetHrs);
                                           });

    int updatedBackwardIndex = std::distance(backwardTimeIntervalsHrs.begin(), closestBackward);
    int updatedForwardIndex = std::distance(forwardTimeIntervalsHrs.begin(), closestForward);

    computationForwardIntegrationTimeProp.setEnumNames(forwardTimeIntervals, updatedForwardIndex);
    computationForwardIntegrationTimeProp.setSelectable(false);

    computationBackwardIntegrationTimeProp.setEnumNames(backwardTimeIntervals, updatedBackwardIndex);
    computationBackwardIntegrationTimeProp.setSelectable(false);

    enableActorUpdates(true);
}


bool MTrajectoryActor::internalSetDateTime(
    const QList<QDateTime> &availableTimesIn, const QDateTime &datetime,
    MEnumProperty &timeProperty)
{
    // Find the time closest to "datetime" in the list of available valid
    // times. Handle possibly reversed available times (e.g., backward trajectory
    // particle times). So check first if they are reversed.
    bool listReversed = false;
    QList<QDateTime> availableTimes = availableTimesIn;
    if (availableTimesIn.size() >= 2 && availableTimesIn[1] < availableTimesIn[0])
    {
        listReversed = true;
        std::reverse(availableTimes.begin(), availableTimes.end());
    }

    int i = -1; // use of "++i" below
    bool exactMatch = false;
    while (i < availableTimes.size() - 1)
    {
        // Loop as long as datetime is larger that the currently inspected
        // element (use "++i" to have the same i available for the remaining
        // statements in this block).
        if (datetime > availableTimes.at(++i))
        {
            continue;
        }

        // We'll only get here if datetime <= availableTimes.at(i). If we
        // have an exact match, break the loop. This is our time.
        if (availableTimes.at(i) == datetime)
        {
            exactMatch = true;
            break;
        }

        // If datetime cannot be exactly matched it lies between indices i-1
        // and i in availableTimes. Determine which is closer.
        if (i == 0)
        {
            break; // if there's no i-1 we're done
        }
        if (abs(datetime.secsTo(availableTimes.at(i - 1)))
            <= abs(datetime.secsTo(availableTimes.at(i))))
        {
            i--;
        }
        // "i" now contains the index of the closest available valid time.
        break;
    }

    if (i > -1)
    {
        if (listReversed)
        {
            i = availableTimesIn.size() - 1 - i;
        }

        // Update background colour of the valid time property in the connected
        // scene's property browser: green if the scene's valid time is an
        // exact match with one of the available valid time, red otherwise.
        if (synchronizationControl != nullptr)
        {
            QColor colour = exactMatch ? QColor(0, 255, 0) : QColor(255, 0, 0);
            timeProperty.setBackgroundColor(colour);
        }

        // Get the currently selected index.
        int currentIndex = timeProperty;

        if (i == currentIndex)
        {
            // Index i is already the current one. Nothing needs to be done.
            return false;
        }
        else
        {
            // Set the new valid time.
            timeProperty = i;
            // A new index was set. Return true.
            return true;
        }
    }

    return false;
}


void MTrajectoryActor::updateEnabledProperties()
{
    if (trajectoryMode == MTrajectoryMode::PRECOMPUTED)
    {
        precomputationGroupProp.setEnabled(true);
        precomputationGroupProp.setHidden(false);
        computationGroupProp.setEnabled(false);
        computationGroupProp.setHidden(true);
    }
    else
    {
        precomputationGroupProp.setEnabled(false);
        precomputationGroupProp.setHidden(true);
        computationGroupProp.setEnabled(true);
        computationGroupProp.setHidden(false);
    }
}


QStringList MTrajectoryActor::getAvailableAuxiliaryVariables()
{
    QStringList allAuxVars;

    for (auto var : variables)
    {
        allAuxVars.append(var->variableName);
    }

    if (trajectoryMode == PRECOMPUTED)
    {
        allAuxVars << trajectoryDispatcher->availableAuxiliaryVariables();
    }
    return allAuxVars;
}


void MTrajectoryActor::updateAuxiliaryVariables(int index)
{
    QStringList availableVars = getAvailableAuxiliaryVariables();

    auxVarIndexProp.setEnumNames(availableVars);
    auxVarIndexProp = index;
}


QDateTime MTrajectoryActor::getPropertyTime(MEnumProperty &enumProperty)
{
    QStringList dateStrings = enumProperty.getEnumNames();

    // If the list of date strings is empty return an invalid null time.
    if (dateStrings.empty())
    {
        return {};
    }

    int index = enumProperty;
    QDateTime dt = QDateTime::fromString(dateStrings.at(index), Qt::ISODate);
    dt.setTimeSpec(Qt::UTC);
    return dt;
}


bool MTrajectoryActor::selectDataSource()
{
    // Ask the user for a precomputed data source.
    MSelectDataSourceDialog dialog(MSelectDataSourceType::TRAJECTORY_SOURCE);
    if (dialog.exec() == QDialog::Rejected)
    {
        return false;
    }

    QString selectedDataSourceID = dialog.getTable()->getSelectedDataSourceID();
    if (selectedDataSourceID == "")
    {
        return false;
    }

    // Only change data sources if necessary.
    if (this->dataSourceID != selectedDataSourceID)
    {
        utilizedDataSourceProp = selectedDataSourceID;
        this->dataSourceID = selectedDataSourceID;
        return true;
    }

    return false;
}


bool MTrajectoryActor::tryLoadMissingPointSource(QSettings* settings, const QString& prefix)
{
    const QString pointSourceType =
            settings->value(prefix + "Type").toString();
    const QString pointSourceName =
            settings->value(prefix + "Name").toString();
    const QString reconstructionKey =
            settings->value(prefix + "ReconstructionKey").toString();

    MPointGeneratorInterfaceType type =
            MPointGeneratorInterface::stringToInterfaceType(pointSourceType);
    MPointGeneratorInterface *interface =
            MPointSourceFactory::getInterface(type, pointSourceName);

    if (type == ACTOR)
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setWindowTitle("Missing point source");
        msgBox.setText(
                QString("'%1' requires a point source actor with the name "
                        "'%2' and of type '%3' that does not exist.\n"
                        "Would you like to load the actor from file?")
                        .arg(getName())
                        .arg(pointSourceName)
                        .arg(reconstructionKey));
        msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        msgBox.button(QMessageBox::Yes)->setText("Load actor");
        msgBox.button(QMessageBox::No)->setText("Discard point source");
        msgBox.exec();

        if (msgBox.clickedButton() == msgBox.button(QMessageBox::Yes))
        {
            MSystemManagerAndControl *sysMC =
                    MSystemManagerAndControl::getInstance();
            // Ask the user for an actor to load. The reconstruction key is the
            // actor's static name type.
            sysMC->getMainWindow()->getSceneManagementDialog()
                 ->loadRequiredActorFromFile(reconstructionKey,
                                             pointSourceName, settings->fileName());
        }
        else
        {
            // User chose to discard the point source.
            return false;
        }

        // Try to get the interface again.
        interface = MPointSourceFactory::getInterface(type, pointSourceName);
        if (! interface)
        {
            // Loading failed, continue without this point source.
            LOG4CPLUS_WARN(mlog,
                           QString("Unable to load the point source with type "
                                   "'%1' and name '%2'. Skipping this source.")
                                   .arg(pointSourceType,pointSourceName));
            return false;
        }
    }
    return true;
}



void MTrajectoryActor::loadTrajectoryPipelineConfig(QSettings *settings, bool preV114Compatibility)
{
    // Amount of sources and filters
    const int pointSourceCount =
        settings->value("computationPointSourceSize", 0).toInt();
    const int pointFilterCount =
        settings->value("computationPointFilterSize", 0).toInt();
    const int trajectoryFilterCount =
        settings->value("trajectoryFilterChainSize", 0).toInt();

    for (int i = 0; i < pointSourceCount; i++)
    {
        QString prefix = QString("computationPointSource%1").arg(i);

        const QString pointSourceType =
            settings->value(prefix + "Type").toString();
        const QString pointSourceName =
            settings->value(prefix + "Name").toString();

        MPointGeneratorInterfaceType type =
            MPointGeneratorInterface::stringToInterfaceType(pointSourceType);
        MPointGeneratorInterface *interface =
            MPointSourceFactory::getInterface(type, pointSourceName);

        if (interface == nullptr)
        {
            // Can't create interface for point generation: The required source
            // is not loaded, e.g., the actor or the bounding box is missing.
            // Ask the user to load the missing point source.
            if (! tryLoadMissingPointSource(settings, prefix))
            {
                // Skip.
                continue;
            }
            interface = MPointSourceFactory::getInterface(type, pointSourceName);
        }

        // Create the source UI.
        MPointGeneratorInterfaceSettings *sourceUI =
            MPointSourceFactory::getInstance()->createUI(
                interface, this, &computationSeedGroupProp);
        sourceUI->setSavePrefix(prefix);
        if (preV114Compatibility)
            sourceUI->loadConfigurationPrior_V_1_14(settings);
        addPointSource(sourceUI);
    }

    for (int i = 0; i < pointFilterCount; i++)
    {
        QString prefix = QString("computationPointFilter%1").arg(i);

        const QString filterType = settings->value(prefix + "Name").toString();
        MPointFilterSettings *filterUI =
            MPointFilterFactory::getInstance()->createUI(
                filterType, this, &computationPointFilterGroupProp);

        if (filterUI == nullptr)
        {
            // Cant load point filter, skipping.
            LOG4CPLUS_WARN(mlog, QString("Unable to load point filter of "
                                         "type '%1'. Skipping this filter.")
                                     .arg(filterType));
            continue;
        }
        addPointFilter(filterUI);
        filterUI->setSavePrefix(prefix);
        if (preV114Compatibility)
            filterUI->loadConfigurationPrior_V_1_14(settings);
    }

    for (int i = 0; i < trajectoryFilterCount; i++)
    {
        QString prefix = QString("trajectoryFilter%1").arg(i);
        const QString filterType = settings->value(prefix + "Name").toString();

        MTrajectoryFilterSettings *filterUI =
            MTrajectoryFilterFactory::getInstance()->createUI(
                filterType, this, &filtersGroupProp);

        if (filterUI == nullptr)
        {
            // Cant load trajectory filter, skipping.
            LOG4CPLUS_WARN(mlog, QString("Unable to load trajectory filter of "
                                         "type '%1'. Skipping this filter.")
                                     .arg(filterType));
            continue;
        }
        filterUI->setSavePrefix(prefix);
        if (preV114Compatibility)
            filterUI->loadConfigurationPrior_V_1_14(settings);
        addTrajectoryFilter(filterUI);
    }
}


void MTrajectoryActor::saveTrajectoryPipelineConfig(QSettings *settings) const
{
    // Save the amount of sources and filters.
    settings->setValue("computationPointSourceSize", pointSourceUIs.size());
    settings->setValue("computationPointFilterSize", pointFilterUIs.size());
    settings->setValue("trajectoryFilterChainSize", trajectoryFilterUIs.size());

    for (int i = 0; i < pointSourceUIs.size(); i++)
    {
        MPointGeneratorInterfaceSettings *sourceUI = pointSourceUIs.at(i);
        QString prefix = QString("computationPointSource%1").arg(i);
        sourceUI->setSavePrefix(prefix);
        sourceUI->saveConfiguration(settings);
    }

    for (int i = 0; i < pointFilterUIs.size(); i++)
    {
        MPointFilterSettings *filterUI = pointFilterUIs.at(i);
        QString prefix = QString("computationPointFilter%1").arg(i);
        filterUI->setSavePrefix(prefix);
        filterUI->saveConfiguration(settings);
    }

    for (int i = 0; i < trajectoryFilterUIs.size(); i++)
    {
        auto filterUI = trajectoryFilterUIs[i];
        QString prefix = QString("trajectoryFilter%1").arg(i);
        filterUI->setSavePrefix(prefix);
        filterUI->saveConfiguration(settings);
    }
}


void MTrajectoryActor::releaseData()
{
    if (trajectoryDataCollections)
    {
        trajectoryDataCollections->releaseBuffers();
        trajectoryDispatcher->releaseData(trajectoryDataCollections);
    }
    trajectoryDataCollections = nullptr;
}


void MTrajectoryActor::initializeSyncControl()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    synchronizationControl =
        sysMC->getSyncControl(sysMC->getSyncControlIdentifiers()[0]);
}


void MTrajectoryActor::initializeRequestDispatcher()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager *memoryManager =
        sysMC->getMemoryManager("Trajectories");

    trajectoryDispatcher = new MTrajectoryRequestDispatcher();
    trajectoryDispatcher->setMemoryManager(memoryManager);
}

} // namespace Met3D
