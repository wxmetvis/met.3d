/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2015      Michael Kern [+]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2021-2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "nwpvolumeraycasteractor.h"

// standard library imports
#include <iostream>
#include <cmath>
#include <random>
#include <array>

// third party tools
#include <QObject>
#include <log4cplus/loggingmacros.h>

// local imports
#include "util/mutil.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "data/structuredgrid.h"
#include "gxfw/datasource/selectdatasourcedialog.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "glsl/shared/mvolumeraycasterconstants.h"
#include "actors/lightactor.h"
#include "util/mgputimer.h"

#define EARTH_PERIMETER_METERS 40030173.0f
//#define ENABLE_GPU_TIMING

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWPVolumeRaycasterActor::MNWPVolumeRaycasterActor()
    : MNWPMultiVarActor(),
      MBoundingBoxInterface(this, MBoundingBoxConnectionType::VOLUME),
      updateNextRenderFrame("111"),
      renderMode(RenderMode::Original),
      varProp(nullptr),
      shadingVarProp(nullptr),
      gl(), // initialize gl objects
      normalCurveNumVertices(0),
      numNormalCurveInitPoints(0)
{
    // Enable picking for the scene view's analysis mode. See
    // triggerAnalysisOfObjectAtPos().
    enablePicking(true);

    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    QStringList modesLst;
    modesLst << "Isosurface" << "Bitfield" << "DVR" << "DVR with volumetric lighting";
    renderModeProp = MEnumProperty("Render mode", modesLst);
    renderModeProp.setConfigKey("render_mode");
    renderModeProp.setValue(static_cast<int>(renderMode));
    renderModeProp.registerValueCallback([=]()
    {
        renderMode = static_cast<RenderMode::Type>(renderModeProp.value());

        if (renderMode == RenderMode::DVR_VOL_LIGHT)
        {
            renderMode = RenderMode::DVR;
            volumeLightingSettings->enabled = true;
        }
        else
        {
            volumeLightingSettings->enabled = false;
        }

        switch (renderMode)
        {
        case RenderMode::Original:
            normalCurveSettings->normalCurvesEnabledProp.setEnabled(true);

            if (varProp != nullptr)
            {
                varProp->ensembleSingleMemberProp.setEnabled(true);
                varProp->setEnsembleMember(varProp
                                                   ->ensembleSingleMemberProp.getSelectedEnumName().toInt());
                varProp->useFlags(false);
            }

            updateNextRenderFrame.set(UpdateShadowImage);

            shadingVarProp.setEnabled(true);
            dvrSettings->groupProp.setEnabled(false);
            rayCasterSettings->groupProp.setEnabled(true);

            volumeLightingSettings->groupProp.setEnabled(false);
            cloudSettings->groupProp.setEnabled(false);

            break;
        case RenderMode::DVR_VOL_LIGHT:
        case RenderMode::DVR:
            normalCurveSettings->normalCurvesEnabledProp.setEnabled(true);

            if (varProp != nullptr)
            {
                varProp->ensembleSingleMemberProp.setEnabled(true);
                varProp->setEnsembleMember(varProp
                                                   ->ensembleSingleMemberProp.getSelectedEnumName().toInt());
                varProp->useFlags(false);
            }
            if (dvrSettings->secVariableProp != nullptr)
            {
                dvrSettings->secVariableProp->ensembleSingleMemberProp.setEnabled(true);
                dvrSettings->secVariableProp->setEnsembleMember(
                        varProp->ensembleSingleMemberProp.getSelectedEnumName().toInt());
                dvrSettings->secVariableProp->useFlags(false);
            }
            if (dvrSettings->thirdVariableProp != nullptr)
            {
                dvrSettings->thirdVariableProp->ensembleSingleMemberProp.setEnabled(true);
                dvrSettings->thirdVariableProp->setEnsembleMember(
                        varProp->ensembleSingleMemberProp.getSelectedEnumName().toInt());
                dvrSettings->thirdVariableProp->useFlags(false);
            }

            updateNextRenderFrame.set(UpdateShadowImage);
            updateNextRenderFrame.set(RenderVolumeLighting);

            shadingVarProp.setEnabled(true);
            dvrSettings->groupProp.setEnabled(true);
            rayCasterSettings->groupProp.setEnabled(false);

            volumeLightingSettings->groupProp.setEnabled(volumeLightingSettings->enabled);
            cloudSettings->groupProp.setEnabled(true);

            break;
        case RenderMode::Bitfield:
            normalCurveSettings->normalCurvesEnabledProp.setEnabled(false);

            if (varProp != nullptr)
            {
                varProp->ensembleSingleMemberProp.setEnabled(true);
                varProp->setEnsembleMember(
                        varProp->ensembleSingleMemberProp.getSelectedEnumName().toInt());
                varProp->useFlags(true);
            }

            updateNextRenderFrame.set(UpdateShadowImage);

            shadingVarProp.setEnabled(true);
            dvrSettings->groupProp.setEnabled(false);
            rayCasterSettings->groupProp.setEnabled(true);

            volumeLightingSettings->groupProp.setEnabled(false);
            cloudSettings->groupProp.setEnabled(false);

            break;
        }

        if (suppressActorUpdates()) return;

        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(renderModeProp);

    varProp = MNWPActorVarProperty("Observed variable");
    varProp.setConfigKey("observed_var");
    varProp.registerValueCallback([=]()
    {
        if (varProp.value() == nullptr) return;
        if (varProp->gridType == MNWPActorVariable::RADAR_GRID)
        {
            renderModeProp.setEnumItem("DVR");
            renderModeProp.setEnabled(false);
        }
        else
        {
            renderModeProp.setEnabled(true);
        }

        updateNextRenderFrame.set(ComputeNCInitPoints);
        updateNextRenderFrame.set(RecomputeNCLines);
        updateNextRenderFrame.set(UpdateShadowImage);
        updateNextRenderFrame.set(RenderVolumeLighting);

        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(varProp);

    shadingVarProp = MNWPActorVarProperty("Shading variable");
    shadingVarProp.setConfigKey("shading_var");
    shadingVarProp.registerValueCallback([=]()
    {
        updateNextRenderFrame.set(ComputeNCInitPoints);
        updateNextRenderFrame.set(RecomputeNCLines);
        updateNextRenderFrame.set(UpdateShadowImage);
        updateNextRenderFrame.set(RenderVolumeLighting);

        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(shadingVarProp);

    // Bounding box of the actor.
    insertBoundingBoxProperty(&actorPropertiesSupGroup);
    bBoxEnabledProp = MBoolProperty("Bounding box", true);
    bBoxEnabledProp.setConfigKey("bounding_box_enabled");
    bBoxEnabledProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(bBoxEnabledProp);

    // Data sampling.
    dataSamplingSettings = new DataSamplingSettings(this);
    actorPropertiesSupGroup.addSubProperty(dataSamplingSettings->groupProp);

    // Raycaster.
    rayCasterSettings = new RayCasterSettings(this);
    actorPropertiesSupGroup.addSubProperty(rayCasterSettings->groupProp);

    // Direct volume rendering settings
    dvrSettings = new DVRSettings(this);
    actorPropertiesSupGroup.addSubProperty(dvrSettings->groupProp);

    // Volumetric lighting
    volumeLightingSettings = new DVRLightSettings(this);
    dvrSettings->groupProp.addSubProperty(volumeLightingSettings->groupProp);

    // Cloud settings
    cloudSettings = new CloudSettings(this);
    dvrSettings->groupProp.addSubProperty(cloudSettings->groupProp);
    dvrSettings->groupProp.setEnabled(renderMode == RenderMode::DVR);

    // Normal curves.
    normalCurveSettings = new NormalCurveSettings(this);
    actorPropertiesSupGroup.addSubProperty(normalCurveSettings->normalCurvesEnabledProp);

    // Set isovalue IDs for normal curves... this works because normal
    // curve properties have been initialized before the constructor is called.
    // NOTE: At least one isovalue needs to be defined!
    QStringList names;
    for (const auto & isoVal : rayCasterSettings->isoValueSetList)
    {
        names << isoVal->enabledProp.getName();
    }
    normalCurveSettings->startIsoSurfaceProp.setEnumNames(names, 0);
    normalCurveSettings->stopIsoSurfaceProp.setEnumNames(names, 0);

    normalCurveSettings->startIsoValue =
            rayCasterSettings->isoValueSetList.at(0)->isoValueProp;
    normalCurveSettings->stopIsoValue =
            rayCasterSettings->isoValueSetList.at(0)->isoValueProp;
}


MNWPVolumeRaycasterActor::OpenGL::OpenGL()
    : rayCasterEffect(nullptr),
      bitfieldRayCasterEffect(nullptr),
      boundingBoxShader(nullptr),
      shadowImageRenderShader(nullptr),
      normalCurveInitPointsShader(nullptr),
      normalCurveLineComputeShader(nullptr),
      normalCurveGeometryEffect(nullptr),

      vboBoundingBox(nullptr),
      iboBoundingBox(0),
      vboPositionCross(nullptr),
      vboIsoPositionBuffer(nullptr),
      vboShadowImageRender(nullptr),
      vboShadowImage(nullptr),
      ssboInitPoints(nullptr),
      ssboNormalCurves(nullptr),

      tex2DShadowImage(nullptr),
      texUnitShadowImage(-1),
      texUnitSceneShadow(-1),
      volumeLightMap3DBuffer(nullptr),
      texUnitVolumeLightMap3D(-1),
      textureDepthBufferMirror(nullptr),
      textureUnitDepthBufferMirror(-1),
      cascadedShadowUpdateGuard(false)
{
}


MNWPVolumeRaycasterActor::DVRLightSettings::DVRLightSettings(
    MNWPVolumeRaycasterActor *hostActor)
    : enabled(false)
{
    groupProp = MProperty("Volumetric lighting");

    lightmapResolutionGroupProp = MProperty("Lightmap resolution");
    groupProp.addSubProperty(lightmapResolutionGroupProp);

    lightmapLonResProp = MIntProperty("Longitude", 128);
    lightmapLonResProp.setConfigKey("lightmap_resolution_lon");
    lightmapLonResProp.setTooltip("Longitude resolution of the lightmap.");
    lightmapLonResProp.setMinMax(1, 2048);
    lightmapLonResProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    lightmapResolutionGroupProp.addSubProperty(lightmapLonResProp);

    lightmapLatResProp = MIntProperty("Latitude", 128);
    lightmapLatResProp.setConfigKey("lightmap_resolution_lat");
    lightmapLatResProp.setTooltip("Latitude resolution of the lightmap.");
    lightmapLatResProp.setMinMax(1, 2048);
    lightmapLatResProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    lightmapResolutionGroupProp.addSubProperty(lightmapLatResProp);

    lightmapZResProp = MIntProperty("Vertical", 64);
    lightmapZResProp.setConfigKey("lightmap_resolution_vertical");
    lightmapZResProp.setTooltip("Vertical resolution of the lightmap.");
    lightmapZResProp.setMinMax(1, 2048);
    lightmapZResProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    lightmapResolutionGroupProp.addSubProperty(lightmapZResProp);

    usePressureScaleProp = MBoolProperty("Pressure scale", true);
    usePressureScaleProp.setConfigKey("pressure_scale");
    usePressureScaleProp.setTooltip("Use pressure scale for vertical resolution of lightmap. "
                                     "In other words, instead of linearly distributing the lightmap,"
                                     "it is denser near the bottom and less dense at the top.");
    usePressureScaleProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(usePressureScaleProp);

    interactivityScaleFactorProp = MIntProperty("Interactivity resolution downscale factor", 4);
    interactivityScaleFactorProp.setConfigKey("interactivity_res_downscale_factor");
    interactivityScaleFactorProp.setTooltip(
        "A downscaling factor for the lightmap when interacting with lights in "
        "the scene for better performance.");
    interactivityScaleFactorProp.setMinMax(1, 16);
    interactivityScaleFactorProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(interactivityScaleFactorProp);

    powderDepthProp = MFloatProperty("Powder depth", 2.0f);
    powderDepthProp.setConfigKey("powder_depth");
    powderDepthProp.setTooltip(
        "The depth of the powder effect. The smaller the number, the deeper it "
        "affects the volume.");
    powderDepthProp.setMinMax(0.0f, 100.0f);
    powderDepthProp.setDecimals(2);
    powderDepthProp.setStep(1.0f);
    powderDepthProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(powderDepthProp);

    powderStrengthProp = MFloatProperty("Powder strength", 1.0f);
    powderStrengthProp.setConfigKey("powder_strength");
    powderStrengthProp.setTooltip("The strength of the powder effect.");
    powderStrengthProp.setMinMax(0.0f, 10.0f);
    powderStrengthProp.setDecimals(2);
    powderStrengthProp.setStep(0.05);
    powderStrengthProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(powderStrengthProp);

    densityScaleProp = MFloatProperty("Shadow density", 1.0f);
    densityScaleProp.setConfigKey("shadow_density");
    densityScaleProp.setTooltip("The density of shadows. The lower the "
                                 "number, the fewer shadows will be visible.");
    densityScaleProp.setMinMax(0.0f, 10.0f);
    densityScaleProp.setDecimals(3);
    densityScaleProp.setStep(0.1f);
    densityScaleProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(densityScaleProp);

    lightSamplingGroup = MProperty("Light ray data sampling");
    groupProp.addSubProperty(lightSamplingGroup);

    stepSizeProp = MFloatProperty("Light ray step size", 0.1f);
    stepSizeProp.setConfigKey("light_ray_step_size");
    stepSizeProp.setTooltip(
            "The size of the steps used to trace towards the light source when \n"
            "calculating the amount of light at any given point.");
    stepSizeProp.setMinimum(0);
    stepSizeProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    lightSamplingGroup.addSubProperty(stepSizeProp);

    interactiveStepSizeProp = MFloatProperty("Interaction light ray step size", 1.0f);
    interactiveStepSizeProp.setConfigKey("interactive_light_ray_step_size");
    interactiveStepSizeProp.setTooltip("Same as step size, but will be used "
                                       "instead of step size when interacting with the scene. "
                                       "A good value is 10 times the step size.");
    interactiveStepSizeProp.setMinimum(0);
    lightSamplingGroup.addSubProperty(interactiveStepSizeProp);

    lightMaxDistanceProp = MFloatProperty("Light ray termination distance", 10.0f);
    lightMaxDistanceProp.setConfigKey("light_ray_termination_distance");
    lightMaxDistanceProp.setTooltip("The termination distance of the light ray towards the light source "
                                    "from any given point.");
    lightMaxDistanceProp.setMinimum(0);
    lightMaxDistanceProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    lightSamplingGroup.addSubProperty(lightMaxDistanceProp);

}


MNWPVolumeRaycasterActor::CloudSettings::CloudSettings(
    MNWPVolumeRaycasterActor *hostActor)
    : ciwcSlot(-1),
      clwcSlot(-1)
{
    groupProp = MProperty("Cloud settings");

    ciwcSlotProp = MEnumProperty("Ice water content variable");
    ciwcSlotProp.setConfigKey("ice_water_content_var");
    ciwcSlotProp.setTooltip(
        "The variable to be used in the cloud algorithm as CIWC.");

    QStringList modesLst;
    modesLst << "None"
             << "Observed variable"
             << "Second observed variable"
             << "Third observed variable";
    ciwcSlotProp.setEnumNames(modesLst, ciwcSlot + 1);
    ciwcSlotProp.registerValueCallback([=]()
    {
        ciwcSlot = ciwcSlotProp.value() - 1;

        hostActor->updateVolumeLight();
    });
    groupProp.addSubProperty(ciwcSlotProp);

    clwcSlotProp = MEnumProperty("Liquid water content variable");
    clwcSlotProp.setConfigKey("liquid_water_content_var");
    clwcSlotProp.setTooltip(
        "The variable slot to be used in the cloud algorithm as CLWC.");
    clwcSlotProp.setEnumNames(modesLst, clwcSlot + 1);
    clwcSlotProp.registerValueCallback([=]()
    {
        clwcSlot = clwcSlotProp.value() - 1;

        hostActor->updateVolumeLight();
    });
    groupProp.addSubProperty(clwcSlotProp);

    clwcColorProp = MColorProperty("CLWC colour", QColor(255, 255, 255));
    clwcColorProp.setConfigKey("clwc_colour");
    clwcColorProp.setTooltip("color of clwc clouds.");
    clwcColorProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(clwcColorProp);

    ciwcColorProp = MColorProperty("CIWC colour", QColor(255, 255, 255));
    ciwcColorProp.setConfigKey("ciwc_colour");
    ciwcColorProp.setTooltip("color of ciwc clouds.");
    ciwcColorProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(ciwcColorProp);

    useShadingVarColorProp = MBoolProperty("Shading variable for cloud colour", false);
    useShadingVarColorProp.setConfigKey("shading_var_for_cloud_colour");
    useShadingVarColorProp.setTooltip(
        "Whether the shading variable is used to override the colouring of "
        "clouds.\nUses the data and transfer function of the shading variable "
        "to determine the colour of clouds.");
    useShadingVarColorProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(useShadingVarColorProp);

    earthCosineEnabledProp = MBoolProperty("Account for earth's curvature", true);
    earthCosineEnabledProp.setConfigKey("account_for_earth_curvature");
    earthCosineEnabledProp.setTooltip(
        "Whether to account for the Earth's curvature when determining distances."
        "in the volume.");
    earthCosineEnabledProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(earthCosineEnabledProp);

    realVerticalScaleProp = MBoolProperty("Real vertical scale", true);
    realVerticalScaleProp.setConfigKey("real_vertical_scale_enabled");
    realVerticalScaleProp.setTooltip(
        "Use the real vertical scale of atmosphere in meters when calculating "
        "distances in the volume.\nOtherwise it uses the distance of one "
        "degree at the equator in meters as the scale of one unit of the z "
        "axis.");
    realVerticalScaleProp.registerValueCallback([=]()
    {
        uniformScaleProp.setEnabled(realVerticalScaleProp);
        hostActor->updateVolumeLight();
    });
    groupProp.addSubProperty(realVerticalScaleProp);

    uniformScaleProp = MBoolProperty("Uniform scaling", true);
    uniformScaleProp.setConfigKey("uniform_scaling");
    uniformScaleProp.setTooltip(
        "Scales the longitude and latitude distances using the scaling factor "
        "of the z axis. Does not produce accurate clouds but is visually uniform.");
    uniformScaleProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(uniformScaleProp);

    spaceScaleProp = MFloatProperty("Cloud optical thickness scaling", 1.0f);
    spaceScaleProp.setConfigKey("could_optical_thickness_scaling");
    spaceScaleProp.setTooltip("Scales the optical thickness of clouds. The lower the value the "
                               "fewer clouds will be visible.");
    spaceScaleProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(spaceScaleProp);
}

MNWPVolumeRaycasterActor::IsoValueSettings::IsoValueSettings(
        MNWPVolumeRaycasterActor *hostActor,
        int index,
        bool _enabled,
        float _isoValue,
        int significantDigits,
        float singleStep,
        const QColor& _isoColor,
        IsoValueSettings::ColorType _colorType)
        : isoColourType(_colorType),
          index(index)
{
    QString propTitle = QString("Isovalue #%1").arg(index  + 1);
    enabledProp = MBoolProperty(propTitle, _enabled);
    enabledProp.setConfigKey("enabled");
    enabledProp.setConfigGroup(propTitle);
    enabledProp.registerValueCallback([=]()
    {
        if (hostActor->normalCurveSettings->normalCurvesEnabledProp)
        {
            hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
            hostActor->updateNextRenderFrame.set(RecomputeNCLines);
        }

        // sort list of isoValues
        hostActor->rayCasterSettings->sortIsoValues();

        hostActor->updateNextRenderFrame.set(UpdateShadowImage);

        hostActor->emitActorChangedSignal();
    });
    enabledProp.setConfigGroup(propTitle);

    isoValueProp = MSciFloatProperty("Isovalue", _isoValue);
    isoValueProp.setConfigKey("isovalue");
    isoValueProp.setSignificantDigits(significantDigits);
    isoValueProp.setStep(singleStep);
    isoValueProp.registerValueCallback([=]()
    {
        int startIsoIndex = hostActor->normalCurveSettings->startIsoSurfaceProp;
        int stopIsoIndex = hostActor->normalCurveSettings->stopIsoSurfaceProp;

        if (startIsoIndex == index)
        {
            hostActor->normalCurveSettings->startIsoValue = isoValueProp;
        }
        if (stopIsoIndex == index)
        {
            hostActor->normalCurveSettings->stopIsoValue = isoValueProp;
        }

        if (hostActor->normalCurveSettings->normalCurvesEnabledProp)
        {
            hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
            hostActor->updateNextRenderFrame.set(RecomputeNCLines);
        }

        hostActor->updateNextRenderFrame.set(UpdateShadowImage);

        // sort list of isoValues
        hostActor->rayCasterSettings->sortIsoValues();

        if (isoColourType == TransferFuncShadingVar ||
            isoColourType == TransferFuncShadingVarMaxNeighbour)
        {
            hostActor->shadingVarProp
                ->actorPropertyChangeEvent(MPropertyType::IsoValue, &isoValueProp);
        }

        hostActor->emitActorChangedSignal();
    });
    enabledProp.addSubProperty(isoValueProp);

    isoValueSignificantDigitsProp = MIntProperty("Significant digits",
                                                 significantDigits);
    isoValueSignificantDigitsProp.setConfigKey("isovalue_sig_digits");
    isoValueSignificantDigitsProp.setMinMax(1, 9);
    isoValueSignificantDigitsProp.registerValueCallback([=]()
    {
        isoValueProp.setSignificantDigits(isoValueSignificantDigitsProp);
        isoValueSingleStepProp.setSignificantDigits(isoValueSignificantDigitsProp);
    });
    isoValueProp.addSubProperty(isoValueSignificantDigitsProp);

    isoValueSingleStepProp = MSciFloatProperty("Step", singleStep);
    isoValueSingleStepProp.setConfigKey("isovalue_step");
    isoValueSingleStepProp.setSignificantDigits(significantDigits);
    isoValueSingleStepProp.setStep(singleStep);
    isoValueSingleStepProp.registerValueCallback([=]()
    {
        isoValueProp.setStep(isoValueSingleStepProp);
    });
    isoValueProp.addSubProperty(isoValueSingleStepProp);

    auto colorCallback = [=]()
    {
        isoColourType = static_cast<IsoValueSettings::ColorType>(isoColourTypeProp.value());

        hostActor->updateNextRenderFrame.set(UpdateShadowImage);
        hostActor->rayCasterSettings->sortIsoValues();
        hostActor->emitActorChangedSignal();
    };

    QStringList modesLst;
    modesLst.clear();
    modesLst << "Constant colour"
             << "Transfer function (observed variable)"
             << "Transfer function (shading variable)"
             << "Transfer function (max. neighbour shading variable)";
    isoColourTypeProp = MEnumProperty("Colour mode", modesLst);
    isoColourTypeProp.setConfigKey("colour_mode");
    isoColourTypeProp.setValue(_colorType);
    isoColourTypeProp.registerValueCallback(colorCallback);
    enabledProp.addSubProperty(isoColourTypeProp);

    isoColourProp = MColorProperty("Constant colour", _isoColor);
    isoColourProp.setConfigKey("constant_colour");
    isoColourProp.registerValueCallback(colorCallback);
    enabledProp.addSubProperty(isoColourProp);

    auto removeCallback = [=]()
    {
        if (hostActor->rayCasterSettings->isoValueSetList.size() == 1) return;
        hostActor->rayCasterSettings->isoValueSetList.removeAll(this);
        hostActor->rayCasterSettings->isoValuesProp.removeSubProperty(&this->enabledProp);

        int i = 0;
        for (auto &isoVal : hostActor->rayCasterSettings->isoValueSetList)
        {
            QString newName = QString("Isovalue #%1").arg(i + 1);
            isoVal->groupProperty()->setName(newName);
            isoVal->groupProperty()->setConfigGroup(newName);
            i++;
        }

        int startIsoIndex = hostActor->normalCurveSettings->startIsoSurfaceProp;
        int stopIsoIndex = hostActor->normalCurveSettings->stopIsoSurfaceProp;

        QStringList names;
        for (auto &isoVal : hostActor->rayCasterSettings->isoValueSetList)
        {
            names << isoVal->groupProperty()->getName();
        }

        if (startIsoIndex == index)
        {
            startIsoIndex = 0;
        }
        else if (startIsoIndex > index)
        {
            startIsoIndex--;
        }
        if (stopIsoIndex == index)
        {
            stopIsoIndex = 0;
        }
        else if (stopIsoIndex > index)
        {
            stopIsoIndex--;
        }

        hostActor->normalCurveSettings->startIsoSurfaceProp.setEnumNames(names, startIsoIndex);
        hostActor->normalCurveSettings->stopIsoSurfaceProp.setEnumNames(names, stopIsoIndex);

        hostActor->rayCasterSettings->sortIsoValues();

        hostActor->updateNextRenderFrame.set(UpdateShadowImage);

        hostActor->emitActorChangedSignal();

        delete this;
    };

    isoValueRemoveProp = MButtonProperty("Remove", "Remove");
    isoValueRemoveProp.registerValueCallback(removeCallback);
    enabledProp.addSubProperty(isoValueRemoveProp);

    auto *removeAction = new QAction("Remove");

    connect(removeAction, &QAction::triggered, [=](bool b)
    {
        removeCallback();
    });

    enabledProp.addContextMenuAction(removeAction);
}


MNWPVolumeRaycasterActor::DataSamplingSettings::DataSamplingSettings(
        MNWPVolumeRaycasterActor *hostActor)
        : hostActor(hostActor)
{
    groupProp = MProperty("Data sampling");

    stepSizeProp = MSciFloatProperty("Ray step size", 0.1f);
    stepSizeProp.setConfigKey("ray_step_size");
    stepSizeProp.setMinMax(1e-9f, 10.0f);
    stepSizeProp.setSignificantDigits(3);
    stepSizeProp.setMinExponent(9);
    stepSizeProp.setStep(0.01);
    stepSizeProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(stepSizeProp);

    interactionStepSizeProp = MSciFloatProperty("Interaction ray step size", 1.0f);
    interactionStepSizeProp.setConfigKey("interaction_ray_step_size");
    interactionStepSizeProp.setMinMax(1e-9f, 10.0f);
    interactionStepSizeProp.setSignificantDigits(3);
    interactionStepSizeProp.setMinExponent(9);
    interactionStepSizeProp.setStep(0.1);
    groupProp.addSubProperty(interactionStepSizeProp);

    bisectionStepsProp = MIntProperty("Isosurface bisection steps", 4);
    bisectionStepsProp.setConfigKey("isosurface_bisection_steps");
    bisectionStepsProp.setMinMax(0, 20);
    bisectionStepsProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(bisectionStepsProp);

    interactionBisectionStepsProp = MIntProperty("Interaction isosurface bisection steps", 4);
    interactionBisectionStepsProp.setConfigKey("interaction_isosurface_bisection_steps");
    interactionBisectionStepsProp.setMinMax(0, 20);
    groupProp.addSubProperty(interactionBisectionStepsProp);
}


const int MAX_ISOSURFACES = 10;

MNWPVolumeRaycasterActor::RayCasterSettings::RayCasterSettings(
        MNWPVolumeRaycasterActor *hostActor)
    : hostActor(hostActor),
      isoValueSetList(),
      isoEnabled(),
      isoValues(),
      isoColors(),
      isoColorTypes()
{

    groupProp = MProperty("Isosurface");

    isoValuesProp = MButtonProperty("Isovalues", "Add isovalue");
    isoValuesProp.setConfigGroup("iso_values");
    isoValuesProp.registerValueCallback([=]()
    {
        if (isoValueSetList.size() > MAX_ISOSURFACES) return;

        hostActor->enableEmissionOfActorChangedSignal(false);
        addIsoValue();

        // Update normal curve properties that link to isovalues.
        int startIsoIndex = hostActor->normalCurveSettings->startIsoSurfaceProp;
        int stopIsoIndex = hostActor->normalCurveSettings->stopIsoSurfaceProp;

        QStringList names;
        for (const auto & i : isoValueSetList)
        {
            names << i->enabledProp.getName();
        }

        hostActor->normalCurveSettings->startIsoSurfaceProp.setEnumNames(names, startIsoIndex);
        hostActor->normalCurveSettings->stopIsoSurfaceProp.setEnumNames(names, stopIsoIndex);

        hostActor->enableEmissionOfActorChangedSignal(true);
    });
    groupProp.addSubProperty(isoValuesProp);

    isoEnabled.reserve(MAX_ISOSURFACES);
    isoValues.reserve(MAX_ISOSURFACES);
    isoColors.reserve(MAX_ISOSURFACES);
    isoColorTypes.reserve(MAX_ISOSURFACES);

    // Add default isosurface value.
    addIsoValue();

    // Sort isovalues to ensure correct visualization via crossing levels.
    sortIsoValues();
}


void MNWPVolumeRaycasterActor::RayCasterSettings::addIsoValue(
        bool enabled, bool hidden,
        float isoValue, int decimals,
        float singleStep, QColor color,
        IsoValueSettings::ColorType colorType)
{
    auto isoSettings = new IsoValueSettings(hostActor, isoValueSetList.size(), enabled, isoValue,
                                 decimals, singleStep, color, colorType);
    isoValueSetList.push_back(isoSettings);
    isoEnabled.push_back(isoSettings->enabledProp);
    isoValues.push_back(isoSettings->isoValueProp);
    QVector4D vecColor = QVector4D(isoSettings->isoColourProp.value().redF(),
                                   isoSettings->isoColourProp.value().greenF(),
                                   isoSettings->isoColourProp.value().blueF(),
                                   isoSettings->isoColourProp.value().alphaF());
    isoColors.push_back(vecColor);
    isoColorTypes.push_back(static_cast<GLint>(isoSettings->isoColourType));
    if (!hidden)
    {
        isoValuesProp.addSubProperty(isoValueSetList.back()->groupProperty());
    }
}


void MNWPVolumeRaycasterActor::RayCasterSettings::sortIsoValues()
{
    for (int i = 0; i < isoValueSetList.size(); ++i)
    {
        isoEnabled[i] = static_cast<int>(isoValueSetList[i]->enabledProp);
        if (isoValueSetList[i]->enabledProp)
        {
            isoValues[i] = isoValueSetList[i]->isoValueProp;
        }
        else
        {
            isoValues[i] = std::numeric_limits<float>::max();
        }
        isoColors[i].setX(isoValueSetList[i]->isoColourProp.value().redF());
        isoColors[i].setY(isoValueSetList[i]->isoColourProp.value().greenF());
        isoColors[i].setZ(isoValueSetList[i]->isoColourProp.value().blueF());
        isoColors[i].setW(isoValueSetList[i]->isoColourProp.value().alphaF());
        isoColorTypes[i] = static_cast<GLint>(isoValueSetList[i]->isoColourType);
    }

    for (int i = 1; i < isoValueSetList.size(); ++i)
    {
        bool currEnabled = isoEnabled[i];
        GLfloat currIsoValue = isoValues[i];
        QVector4D currIsoColor = isoColors[i];
        GLint currIsoColorType = isoColorTypes[i];

        int c = i - 1;
        for (; c >= 0 && currIsoValue < isoValues[c]; --c)
        {
            isoEnabled[c + 1] = isoEnabled[c];
            isoValues[c + 1] = isoValues[c];
            isoColors[c + 1] = isoColors[c];
            isoColorTypes[c + 1] = isoColorTypes[c];
        }

        isoEnabled[c + 1] = currEnabled;
        isoValues[c + 1] = currIsoValue;
        isoColors[c + 1] = currIsoColor;
        isoColorTypes[c + 1] = currIsoColorType;
    }
}


MNWPVolumeRaycasterActor::NormalCurveSettings::NormalCurveSettings(
        MNWPVolumeRaycasterActor *hostActor)
    : glyph(GlyphType::Tube),
      threshold(Threshold::Steps),
      colour(CurveColor::ColorIsoValue),
      integrationDir(IntegrationDir::Backwards),
      startIsoValue(0),
      stopIsoValue(0)
{
    normalCurvesEnabledProp = MBoolProperty("Normal curves", false);
    normalCurvesEnabledProp.setConfigKey("normal_curves_enabled");
    normalCurvesEnabledProp.registerValueCallback([=]()
    {
       if (normalCurvesEnabledProp)
       {
           hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
           hostActor->updateNextRenderFrame.set(RecomputeNCLines);
       }

       hostActor->emitActorChangedSignal();
    });

    QStringList modesLst;

    modesLst.clear();
    modesLst << "Number of line segments" << "Stop at isosurface";
    thresholdProp = MEnumProperty("Curve length limited by", modesLst);
    thresholdProp.setConfigKey("curve_length_limited_by");
    thresholdProp.setValue(static_cast<int>(threshold));
    thresholdProp.registerValueCallback([=]()
    {
        threshold = static_cast<NormalCurveSettings::Threshold>(thresholdProp.value());

        if (threshold == NormalCurveSettings::Threshold::Steps)
        {
            integrationDirProp.setEnabled(true);
            numLineSegmentsProp.setEnabled(true);
            stopIsoSurfaceProp.setEnabled(false);
        }
        else
        {
            integrationDirProp.setEnabled(false);
            numLineSegmentsProp.setEnabled(false);
            stopIsoSurfaceProp.setEnabled(true);
//TODO (mr, 03Apr2016) -- this shouldn't be hard-coded!
            numLineSegmentsProp = 500;
        }

        hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
        hostActor->updateNextRenderFrame.set(RecomputeNCLines);

        hostActor->emitActorChangedSignal();
    });
    normalCurvesEnabledProp.addSubProperty(thresholdProp);

    auto startStopIsoCallback = [=]()
    {
        int startIndex = startIsoSurfaceProp;
        int stopIndex = stopIsoSurfaceProp;
        if (startIndex < hostActor->rayCasterSettings->isoValueSetList.size())
        {
            startIsoValue =
                    hostActor->rayCasterSettings->isoValueSetList.at(startIndex)->isoValueProp;
        }
        else
        {
            startIsoValue =
                    hostActor->rayCasterSettings->isoValueSetList.at(0)->isoValueProp;
        }

        if (stopIndex < hostActor->rayCasterSettings->isoValueSetList.size())
        {
            stopIsoValue =
                    hostActor->rayCasterSettings->isoValueSetList.at(stopIndex)->isoValueProp;
        }
        else
        {
            stopIsoValue =
                    hostActor->rayCasterSettings->isoValueSetList.at(0)->isoValueProp;
        }

        if (threshold != NormalCurveSettings::Threshold::Steps)
        {
            if (startIsoValue > stopIsoValue)
            {
                integrationDir = NormalCurveSettings::Backwards;
            }
            else
            {
                integrationDir = NormalCurveSettings::Forwards;
            }
        }

        if (normalCurvesEnabledProp)
        {
            hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
            hostActor->updateNextRenderFrame.set(RecomputeNCLines);
        }

        hostActor->updateNextRenderFrame.set(UpdateShadowImage);

        hostActor->emitActorChangedSignal();
    };

    startIsoSurfaceProp = MEnumProperty("Curves start at");
    startIsoSurfaceProp.setConfigKey("curves_start_at");
    startIsoSurfaceProp.registerValueCallback(startStopIsoCallback);
    normalCurvesEnabledProp.addSubProperty(startIsoSurfaceProp);

    stopIsoSurfaceProp = MEnumProperty("Curves stop at");
    stopIsoSurfaceProp.setConfigKey("curves_stop_at");
    stopIsoSurfaceProp.setEnabled(false);
    stopIsoSurfaceProp.registerValueCallback(startStopIsoCallback);
    normalCurvesEnabledProp.addSubProperty(stopIsoSurfaceProp);

    modesLst.clear();
    modesLst << "High to low values" << "Low to high values" << "Both";
    integrationDirProp = MEnumProperty("Integration direction", modesLst);
    integrationDirProp.setConfigKey("integration_direction");
    integrationDirProp.setValue(static_cast<int>(integrationDir));
    integrationDirProp.registerValueCallback([=]()
    {
        if (threshold == NormalCurveSettings::Threshold::Steps)
        {
            integrationDir =
                    static_cast<NormalCurveSettings::IntegrationDir>(
                            integrationDirProp.value());
        }
        else
        {
            if (startIsoValue >= stopIsoValue)
            {
                integrationDir = NormalCurveSettings::Backwards;
            }
            else
            {
                integrationDir = NormalCurveSettings::Forwards;
            }
        }

        if (normalCurvesEnabledProp)
        {
            hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
            hostActor->updateNextRenderFrame.set(RecomputeNCLines);
        }

        hostActor->updateNextRenderFrame.set(UpdateShadowImage);

        hostActor->emitActorChangedSignal();
    });
    normalCurvesEnabledProp.addSubProperty(integrationDirProp);

    numLineSegmentsProp = MIntProperty("Max number line segments", 20);
    numLineSegmentsProp.setConfigKey("max_num_line_segments");
    numLineSegmentsProp.setMinMax(1, 9999);
    numLineSegmentsProp.registerValueCallback([=]()
    {
        if (normalCurvesEnabledProp)
        {
            hostActor->updateNextRenderFrame.set(RecomputeNCLines);
        }

        hostActor->emitActorChangedSignal();
    });
    normalCurvesEnabledProp.addSubProperty(numLineSegmentsProp);

    stepSizeProp = MFloatProperty("Length of single line segment", 0.1f);
    stepSizeProp.setConfigKey("length_of_single_line_segment");
    stepSizeProp.setMinMax(0.001f, 100.0f);
    stepSizeProp.setDecimals(3);
    stepSizeProp.setStep(0.001f);
    stepSizeProp.registerValueCallback([=]()
    {
        hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
        hostActor->updateNextRenderFrame.set(RecomputeNCLines);

        hostActor->emitActorChangedSignal();
    });
    normalCurvesEnabledProp.addSubProperty(stepSizeProp);

    groupRenderingSettingsProp = MProperty("Rendering");
    normalCurvesEnabledProp.addSubProperty(groupRenderingSettingsProp);

    modesLst.clear();
    modesLst << "Lines" << "Boxes + Slices" << "Tubes";
    glyphProp = MEnumProperty("Glyph type", modesLst);
    glyphProp.setConfigKey("glyph_type");
    glyphProp.setValue(static_cast<int>(glyph));
    glyphProp.registerValueCallback([=]()
    {
        glyph = static_cast<NormalCurveSettings::GlyphType>(glyphProp.value());

        hostActor->emitActorChangedSignal();
    });
    groupRenderingSettingsProp.addSubProperty(glyphProp);

    modesLst.clear();
//TODO (mr, 03Apr2016) -- first two colour modes are broken; I've disabled them
// for now (hence the "-2" below).
//    modesLst << "ratio steps/max steps" << "ratio curve length/max length"
//             << "transfer function (observed variable)";
    modesLst << "transfer function (observed variable)";
    colourProp = MEnumProperty("Curve colour", modesLst);
    colourProp.setConfigKey("curve_colour");
    colourProp.setValue(static_cast<int>(colour) - 2);
    colourProp.registerValueCallback([=]()
    {
        colour = static_cast<NormalCurveSettings::CurveColor>(colourProp.value());

        hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
        hostActor->updateNextRenderFrame.set(RecomputeNCLines);

        hostActor->emitActorChangedSignal();
    });
    groupRenderingSettingsProp.addSubProperty(colourProp);

    auto updateCurvesAndShadow = [=]()
    {
        if (normalCurvesEnabledProp)
        {
            hostActor->updateNextRenderFrame.set(ComputeNCInitPoints);
            hostActor->updateNextRenderFrame.set(RecomputeNCLines);
        }

        hostActor->updateNextRenderFrame.set(UpdateShadowImage);

        hostActor->emitActorChangedSignal();
    };

    tubeRadiusProp = MFloatProperty("Curve radius", 0.05f);
    tubeRadiusProp.setConfigKey("curve_radius");
    tubeRadiusProp.setMinMax(0.01f, 0.5f);
    tubeRadiusProp.setDecimals(2);
    tubeRadiusProp.setStep(0.01);
    tubeRadiusProp.registerValueCallback(updateCurvesAndShadow);
    groupRenderingSettingsProp.addSubProperty(tubeRadiusProp);

    groupSeedSettingsProp = MProperty("Seed points");
    normalCurvesEnabledProp.addSubProperty(groupSeedSettingsProp);

    groupSeedSpacingProp = MProperty("Seed point spacing");
    groupSeedSettingsProp.addSubProperty(groupSeedSpacingProp);

    initPointResXProp = MFloatProperty("Longitude", 1.75f);
    initPointResXProp.setConfigKey("seed_spacing_lon");
    initPointResXProp.setMinMax(0.1f, 10.0f);
    initPointResXProp.setDecimals(3);
    initPointResXProp.setStep(0.1f);
    initPointResXProp.registerValueCallback(updateCurvesAndShadow);
    groupSeedSpacingProp.addSubProperty(initPointResXProp);

    initPointResYProp = MFloatProperty("Latitude", 1.75f);
    initPointResYProp.setConfigKey("seed_spacing_lat");
    initPointResYProp.setMinMax(0.1f, 10.0f);
    initPointResYProp.setDecimals(3);
    initPointResYProp.setStep(0.1f);
    initPointResYProp.registerValueCallback(updateCurvesAndShadow);
    groupSeedSpacingProp.addSubProperty(initPointResYProp);

    initPointResZProp = MFloatProperty("Vertical", 1.0f);
    initPointResZProp.setConfigKey("seed_spacing_vertical");
    initPointResZProp.setMinMax(0.1f, 10.0f);
    initPointResZProp.setDecimals(3);
    initPointResZProp.setStep(0.1f);
    initPointResZProp.registerValueCallback(updateCurvesAndShadow);
    groupSeedSpacingProp.addSubProperty(initPointResZProp);

    initPointVarianceProp = MFloatProperty("Variance", 0.3f);
    initPointVarianceProp.setConfigKey("seed_points_variance");
    initPointVarianceProp.setMinMax(0, 2);
    initPointVarianceProp.setDecimals(3);
    initPointVarianceProp.setStep(0.01);
    initPointVarianceProp.registerValueCallback(updateCurvesAndShadow);
    groupSeedSettingsProp.addSubProperty(initPointVarianceProp);
}

MNWPVolumeRaycasterActor::DVRSettings::DVRSettings(MNWPVolumeRaycasterActor *hostActor)
    : refScaleUnit(REF_SCALE_W),
      scaleFix(RenderMode::ScaleFixMode::Normal)
{
    groupProp = MProperty("Direct volume rendering");

    secondVarEnabledProp = MBoolProperty("Second variable", false);
    secondVarEnabledProp.setConfigKey("second_var_enabled");
    secondVarEnabledProp.setTooltip("Enables the second variable slot for DVR.");
    secondVarEnabledProp.registerValueCallback([=]()
    {
        secVariableProp.setEnabled(secondVarEnabledProp);

        hostActor->updateVolumeLight();
    });
    groupProp.addSubProperty(secondVarEnabledProp);

    secVariableProp = MNWPActorVarProperty("Second observed variable");
    secVariableProp.setConfigKey("second_observed_var");
    secVariableProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(secVariableProp);

    thirdVarEnabledProp = MBoolProperty("Third variable", false);
    thirdVarEnabledProp.setConfigKey("third_var_enabled");
    thirdVarEnabledProp.setTooltip("Enables the third variable slot for DVR.");
    thirdVarEnabledProp.registerValueCallback([=]()
    {
        thirdVariableProp.setEnabled(secondVarEnabledProp);

        hostActor->updateVolumeLight();
    });
    groupProp.addSubProperty(thirdVarEnabledProp);

    thirdVariableProp = MNWPActorVarProperty("Third observed variable");
    thirdVariableProp.setConfigKey("third_observed_var");
    thirdVariableProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    groupProp.addSubProperty(thirdVariableProp);

    secVariableProp.setEnabled(secondVarEnabledProp);
    thirdVariableProp.setEnabled(thirdVarEnabledProp);

    QStringList scaleFixModeList;
    scaleFixModeList << "None" << "Normal" << "Physical";
    scaleFixModeProp = MEnumProperty("Z scale transparency correction", scaleFixModeList);
    scaleFixModeProp.setConfigKey("z_scale_transparency_correction");
    scaleFixModeProp.setTooltip("A method on how to correct the transparency of "
                                 "transfer functions in DVR for different Z-Scales.");
    scaleFixModeProp.registerValueCallback([=]()
    {
        scaleFix = static_cast<RenderMode::ScaleFixMode>(scaleFixModeProp.value());

        hostActor->emitActorChangedSignal();
    });
    scaleFixModeProp.setValue(static_cast<int>(scaleFix));
    groupProp.addSubProperty(scaleFixModeProp);

    useDitheringProp = MBoolProperty("Dithering", true);
    useDitheringProp.setConfigKey("dithering");
    useDitheringProp.setTooltip("Enables dithering in DVR, which randomizes ray starting locations and reduces banding artifacts. "
                                 "This will also be used when calculating the volumetric lighting.");
    useDitheringProp.registerValueCallback([=]()
    {
        ditherStrengthProp.setEnabled(useDitheringProp);
        hostActor->emitActorChangedSignal();
    });
    groupProp.addSubProperty(useDitheringProp);

    ditherStrengthProp = MFloatProperty("Dithering strength", 1.0f);
    ditherStrengthProp.setConfigKey("dithering_strength");
    ditherStrengthProp.setTooltip("The strength of the dithering, when enabled.");
    ditherStrengthProp.setMinMax(0.0f, 4.0f);
    ditherStrengthProp.setDecimals(3);
    ditherStrengthProp.setStep(0.1f);
    ditherStrengthProp.registerValueCallback(hostActor, &MActor::emitActorChangedSignal);
    groupProp.addSubProperty(ditherStrengthProp);

    QStringList units;
    units << "M";
    units << "World";

    referenceScaleUnitProp = MEnumProperty("Reference scale unit", units);
    referenceScaleUnitProp.setConfigKey("reference_scale_unit");
    referenceScaleUnitProp.setTooltip("The unit for the step size reference.");
    referenceScaleUnitProp.setValue(refScaleUnit);
    referenceScaleUnitProp.registerValueCallback([=]()
    {
        refScaleUnit = referenceScaleUnitProp.value();
        hostActor->updateVolumeLight();
    });
    groupProp.addSubProperty(referenceScaleUnitProp);

    refScaleProp = MFloatProperty("Reference scale", 1.0f);
    refScaleProp.setConfigKey("reference_scale");
    refScaleProp.registerValueCallback(hostActor, &MNWPVolumeRaycasterActor::updateVolumeLight);
    refScaleProp.setTooltip("The reference scale variable is used when "
                                   "converting alpha values to extinction coefficients.");
    refScaleProp.setMinimum(0.0f);
    refScaleProp.setDecimals(1);
    refScaleProp.setStep(1.0f);
    groupProp.addSubProperty(refScaleProp);
}


MNWPVolumeRaycasterActor::~MNWPVolumeRaycasterActor()
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    for (GL::MTexture *buffer : gl.tex2DIsoPositionBuffer)
    {
        glRM->releaseGPUItem(buffer);
    }
    gl.tex2DIsoPositionBuffer.clear();

    if (gl.tex2DShadowImage) glRM->releaseGPUItem(gl.tex2DShadowImage);
    if (gl.vboShadowImageRender) glRM->releaseGPUItem(gl.vboShadowImageRender);
    if (gl.vboIsoPositionBuffer) glRM->releaseGPUItem(gl.vboIsoPositionBuffer);
    if (gl.vboBoundingBox) glRM->releaseGPUItem(gl.vboBoundingBox);
    if (gl.ssboInitPoints != nullptr) glRM->releaseGPUItem(gl.ssboInitPoints);
    if (gl.ssboNormalCurves != nullptr) glRM->releaseGPUItem(gl.ssboNormalCurves);
    if (gl.texUnitSceneShadow >= 0) releaseTextureUnit(gl.texUnitSceneShadow);
    if (gl.texUnitShadowImage >=0) releaseTextureUnit(gl.texUnitShadowImage);
    if (gl.volumeLightMap3DBuffer) glRM->releaseGPUItem(gl.volumeLightMap3DBuffer);
    if (gl.texUnitVolumeLightMap3D >= 0) releaseTextureUnit(gl.texUnitVolumeLightMap3D);

    for (GLuint fbo : gl.fboDepthBuffer)
    {
        // Delete framebuffers allocated in mirrorDepthBufferToTexture().
        glDeleteFramebuffers(1, &fbo); CHECK_GL_ERROR;
    }

    delete rayCasterSettings;
    delete normalCurveSettings;
    delete dvrSettings;
    delete volumeLightingSettings;
    delete cloudSettings;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

const uint8_t SHADER_VERTEX_ATTRIBUTE = 0;
const uint8_t SHADER_BORDER_ATTRIBUTE = 1;
const uint8_t SHADER_TEXCOORD_ATTRIBUTE = 1;
const uint8_t SHADER_VALUE_ATTRIBUTE = 1;

void MNWPVolumeRaycasterActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");

    beginCompileShaders(7);

    compileShadersFromFileWithProgressDialog(
                gl.boundingBoxShader,
                "src/glsl/simple_coloured_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                gl.rayCasterEffect,
                "src/glsl/volume_raycaster.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                gl.shadowImageRenderShader,
                "src/glsl/volume_image.fx.glsl");

    compileShadersFromFileWithProgressDialog(
                gl.normalCurveGeometryEffect,
                "src/glsl/volume_normalcurves_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                gl.normalCurveInitPointsShader,
                "src/glsl/volume_normalcurves_initpoints.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                gl.normalCurveLineComputeShader,
                "src/glsl/volume_compute_normalcurves.fx.glsl");

    compileShadersFromFileWithProgressDialog(
                gl.bitfieldRayCasterEffect,
                "src/glsl/volume_bitfield_raycaster.fx.glsl");

    endCompileShaders();
    initializeRenderInformation();
}


void MNWPVolumeRaycasterActor::saveConfigurationHeader(QSettings *settings)
{
    MNWPMultiVarActor::saveConfigurationHeader(settings);
    MBoundingBoxInterface::saveConfiguration(settings);

    settings->beginGroup(MNWPVolumeRaycasterActor::getSettingsID());
    settings->setValue("numIsoValues", rayCasterSettings->isoValueSetList.size());
    settings->endGroup();
}


void MNWPVolumeRaycasterActor::loadConfigurationHeader(QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationHeader(settings);
    MBoundingBoxInterface::loadConfiguration(settings);

    settings->beginGroup(MNWPVolumeRaycasterActor::getSettingsID());

    // Re-add the isovalues so that the property tree is in the same state
    // as it was when this config was saved. That way, the properties can load
    // all relevant settings.
    int numIsoVals = settings->value("numIsoValues", 0).toInt();
    // Start at index 1 when adding iso values, as the default ray caster actor
    // already contains one iso value by default.
    for (int i = 1; i < numIsoVals; i++)
    {
        rayCasterSettings->addIsoValue();
    }

    settings->endGroup();
}


void MNWPVolumeRaycasterActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationPrior_V_1_14(settings);

    // It is necessary to load the variable indices BEFORE loading the
    // variables since they might need to be adapted if a variable cannot be
    // loaded and the user does NOT choose a new one. (Store the loaded indices
    // separately from the used indices since these are changed when adding,
    // changing or deleting a variable (which is done during loading of
    // variables!).)
    settings->beginGroup(MNWPVolumeRaycasterActor::getSettingsID());
    int loadedVariableIndex = settings->value("varIndex").toInt();
    int loadedShadingVariableIndex = settings->value("shadingVarIndex")
                                             .toInt();
    settings->endGroup();

    int variableIndex = loadedVariableIndex;
    int shadingVariableIndex = loadedShadingVariableIndex;

    settings->beginGroup(MNWPVolumeRaycasterActor::getSettingsID());

    renderModeProp.setEnumItem(settings->value("renderMode").toString());
    varProp.setIndex(variableIndex);
    shadingVarProp.setIndex(shadingVariableIndex);
    bBoxEnabledProp = settings->value("bBoxEnabled", true).toBool();

    // dvr specific settings
    // =====================
    settings->beginGroup("DVR");

    int secVariableIndex = settings->value("varIndex2").toInt();
    int thirdVariableIndex = settings->value("varIndex3").toInt();

    dvrSettings->secVariableProp.setIndex(secVariableIndex);
    dvrSettings->thirdVariableProp.setIndex(thirdVariableIndex);

    dvrSettings->secondVarEnabledProp = settings->value("enableSecVar")
                                                .toBool();
    dvrSettings->thirdVarEnabledProp = settings->value("enableThirdVar")
                                               .toBool();
    dvrSettings->scaleFixModeProp.setEnumItem(
            settings->value("verticalScaleMode").toString());
    dvrSettings->useDitheringProp = settings->value("useDithering", true)
                                            .toBool();
    dvrSettings->ditherStrengthProp = settings->value("ditherStrength", 1.0)
                                              .toFloat();
    dvrSettings->referenceScaleUnitProp = settings->value("refScaleUnit", 0)
                                                  .toInt();
    dvrSettings->refScaleProp = settings->value("refScale", 1.0).toFloat();

    settings->endGroup(); // DVR

    // bounding box settings
    // =====================
    MBoundingBoxInterface::loadConfiguration(settings);

    // volume light settings
    // =====================

    settings->beginGroup("DVRLighting");

    volumeLightingSettings->lightmapLonResProp = settings->value("resX", 128)
                                                         .toInt();
    volumeLightingSettings->lightmapLatResProp = settings->value("resY", 128)
                                                         .toInt();
    volumeLightingSettings->lightmapZResProp = settings->value("resZ", 64)
                                                       .toInt();
    volumeLightingSettings->usePressureScaleProp = settings
            ->value("usePressureScale", true).toBool();
    volumeLightingSettings->interactivityScaleFactorProp = settings
            ->value("interactDownscaleFactor", 4).toInt();
    volumeLightingSettings->powderDepthProp = settings
            ->value("powderDepth", 2.0).toFloat();
    volumeLightingSettings->powderStrengthProp = settings
            ->value("powderStrength", 1.0).toFloat();
    volumeLightingSettings->densityScaleProp = settings
            ->value("densityScale", 1.0).toFloat();

    settings->endGroup();

    // cloud settings
    // ==============

    settings->beginGroup("CloudSettings");

    cloudSettings->ciwcSlotProp = settings->value("ciwcSlot",
                                                  -1).toInt() + 1;
    cloudSettings->clwcSlotProp = settings->value("clwcSlot",
                                                  -1).toInt() + 1;
    cloudSettings->ciwcColorProp = settings
            ->value("ciwcColor", QColor(255, 255, 255)).value<QColor>();
    cloudSettings->clwcColorProp = settings
            ->value("clwcColor", QColor(255, 255, 255)).value<QColor>();
    cloudSettings->useShadingVarColorProp = settings
            ->value("useShadingVarColor", false).toBool();
    cloudSettings->earthCosineEnabledProp = settings
            ->value("earthCosineEnabled", true).toBool();
    cloudSettings->realVerticalScaleProp = settings
            ->value("realVerticalScale", true).toBool();
    cloudSettings->uniformScaleProp = settings->value("uniformScale", true)
                                              .toBool();
    cloudSettings->spaceScaleProp = settings->value("spaceScale", 1.0)
                                            .toDouble();

    settings->endGroup();

    // raycaster settings
    // ==================
    settings->beginGroup("Raycaster");
    uint numIsoValues = settings->value("numIsoValues").toUInt();

    settings->beginGroup("IsoValues");

    // Remove current isovalue properties.
    for (auto &setting : rayCasterSettings->isoValueSetList)
    {
        rayCasterSettings->isoValuesProp
                         .removeSubProperty(setting->groupProperty());
        delete setting;
    }
    rayCasterSettings->isoValueSetList.clear();
    rayCasterSettings->isoEnabled.clear();
    rayCasterSettings->isoValues.clear();
    rayCasterSettings->isoColors.clear();
    rayCasterSettings->isoColorTypes.clear();

    // Load new isovalue properties from file.
    for (uint i = 0; i < numIsoValues; i++)
    {
        settings->beginGroup(QString("isoValue%1").arg(i));

        bool enabled = settings->value("enabled").toBool();
        float isoValue = settings->value("isoValue").toFloat();
        IsoValueSettings::ColorType isoColorType = IsoValueSettings::ColorType(
                settings->value("colourMode").toInt());
        QColor isoColor = settings->value("colour").value<QColor>();

        int significantDigits = 2;

        // Support old version of configuration.
        if (settings->contains("isoValueDecimals"))
        {
            significantDigits = settings->value("isoValueDecimals", 2)
                                        .toInt();
        }
        else
        {
            significantDigits = settings->value("isoValueSignificantDigits",
                                                2).toInt();
        }
        double singleStep = settings->value("isoValueSingleStep", 0.01)
                                    .toFloat();

        rayCasterSettings->addIsoValue(enabled, false,
                                       isoValue, significantDigits,
                                       singleStep,
                                       isoColor, isoColorType);

        settings->endGroup();
    }

    rayCasterSettings->sortIsoValues();

    // Update normal curves properties that link to isovalues.
    QStringList names;
    for (auto &isoVal : rayCasterSettings->isoValueSetList)
    {
        names << isoVal->groupProperty()->getName();
    }
    normalCurveSettings->startIsoSurfaceProp.setEnumNames(names);
    normalCurveSettings->stopIsoSurfaceProp.setEnumNames(names);

    settings->endGroup(); // isoValueSettings

    dataSamplingSettings->stepSizeProp = settings->value("stepSize").toFloat();
    dataSamplingSettings->interactionStepSizeProp =
            settings->value("interactionStepSize").toFloat();
    dataSamplingSettings->bisectionStepsProp =
            settings->value("bisectionSteps").toInt();
    dataSamplingSettings->interactionBisectionStepsProp =
            settings->value("interactionBisectionSteps").toInt();

    settings->endGroup();

    // normal curves
    // =============
    settings->beginGroup("NormalCurves");

    normalCurveSettings->normalCurvesEnabledProp =
            settings->value("enabled").toBool();
    normalCurveSettings->glyphProp = settings->value("glyphType").toInt();
    normalCurveSettings->thresholdProp = settings->value("threshold").toInt();
    normalCurveSettings->colour =
            NormalCurveSettings::CurveColor(
                    settings->value("colour").toInt());
    normalCurveSettings->tubeRadiusProp = settings->value("tubeRadius")
                                                  .toFloat();

    normalCurveSettings->integrationDir =
            NormalCurveSettings::IntegrationDir(
                    settings->value("integrationDir").toInt());

    normalCurveSettings->stepSizeProp =
            settings->value("stepSize").toFloat();
    normalCurveSettings->numLineSegmentsProp =
            settings->value("numLineSegments").toInt();
    normalCurveSettings->initPointResXProp =
            settings->value("initPointResX").toFloat();
    normalCurveSettings->initPointResYProp =
            settings->value("initPointResY").toFloat();
    normalCurveSettings->initPointResZProp =
            settings->value("initPointResZ").toFloat();
    normalCurveSettings->initPointVarianceProp =
            settings->value("initPointVariance").toFloat();

    normalCurveSettings->startIsoSurfaceProp =
            settings->value("startIsoSurfaceEnum").toInt();
    normalCurveSettings->startIsoValue =
            rayCasterSettings->isoValueSetList.at(
                                     settings->value("startIsoSurfaceEnum")
                                             .toInt())
                             ->isoValueProp;
    normalCurveSettings->stopIsoSurfaceProp =
            settings->value("stopIsoSurfaceEnum").toInt();
    normalCurveSettings->stopIsoValue =
            rayCasterSettings->isoValueSetList.at(
                                     settings->value("stopIsoSurfaceEnum")
                                             .toInt())
                             ->isoValueProp;

    settings->endGroup();

    settings->endGroup();

    // Update normal curves and shadow map on next render cycle.
    updateNextRenderFrame.set(ComputeNCInitPoints);
    updateNextRenderFrame.set(RecomputeNCLines);
    updateNextRenderFrame.set(UpdateShadowImage);
    updateNextRenderFrame.set(RenderVolumeLighting);

    if (isInitialized()) generateVolumeBoxGeometry();

    if (!variables.empty())
    {
        switch(renderMode)
        {
        case RenderMode::Original:
            varProp->useFlags(false); break;
        case RenderMode::Bitfield:
            varProp->useFlags(true); break;
        case RenderMode::DVR_VOL_LIGHT:
        case RenderMode::DVR:
            varProp->useFlags(false); break;
        }
    }
}


bool MNWPVolumeRaycasterActor::triggerAnalysisOfObjectAtPos(
        MSceneViewGLWidget *sceneView, float clipX, float clipY,
        float clipRadius)
{
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        LOG4CPLUS_TRACE(mlog, "No bounding box is set.");
        return false;
    }
    if (varProp == nullptr)
    {
        LOG4CPLUS_TRACE(mlog, "No variable selected.");
        return false;
    }

    Q_UNUSED(clipRadius);
    LOG4CPLUS_TRACE(mlog, "Triggering isosurface analysis.");

    QVector3D mousePosClipSpace = QVector3D(clipX, clipY, 0.);
    QVector3D mousePosWorldSpace = sceneView->clipSpaceToLonLatWorldZ(
                mousePosClipSpace);

    QVector3D rayOrigin = sceneView->getCamera()->getOrigin();
    QVector3D rayDirection = (mousePosWorldSpace - rayOrigin).normalized();

    // Compute the intersection points of the ray with the volume bounding
    // box. If the ray does not intersect with the box discard this fragment.
    QVector3D volumeTopNECrnr(
        getBBoxConnection()->eastLon(), getBBoxConnection()->northLat(),
                sceneView->worldZfromPressure(getBBoxConnection()->topPressure_hPa()));
    QVector3D volumeBottomSWCrnr(
        getBBoxConnection()->westLon(), getBBoxConnection()->southLat(),
                sceneView->worldZfromPressure(getBBoxConnection()->bottomPressure_hPa()));
    QVector2D lambdaNearFar;

    bool rayIntersectsRenderVolume = rayBoxIntersection(
                rayOrigin, rayDirection, volumeBottomSWCrnr, volumeTopNECrnr,
                &lambdaNearFar);
    if (!rayIntersectsRenderVolume)
    {
        LOG4CPLUS_TRACE(mlog, "Mouse position outside render volume.");
        return false;
    }

    // If the value for lambdaNear is < 0 the camera is located inside the
    // bounding box. It makes no sense to start the ray traversal behind the
    // camera, hence move lambdaNear to 0 to start in front of the camera.
    if (lambdaNearFar.x() < 0.01)
    {
        lambdaNearFar.setX(0.01);
    }

    float stepSize = dataSamplingSettings->stepSizeProp;
    float lambda = lambdaNearFar.x();
    float prevLambda = lambda;
    QVector3D rayPosition = rayOrigin + lambdaNearFar.x() * rayDirection;
    QVector3D rayPosIncrement = stepSize * rayDirection;
    QVector3D prevRayPosition = rayPosition;

    float scalar = varProp->grid->interpolateValue(
                rayPosition.x(), rayPosition.y(),
                sceneView->pressureFromWorldZ(rayPosition.z()));

    int crossingLevelBack = computeCrossingLevel(scalar);
    int crossingLevelFront = crossingLevelBack;

    while (lambda < lambdaNearFar.y())
    {
        scalar = varProp->grid->interpolateValue(
                    rayPosition.x(), rayPosition.y(),
                    sceneView->pressureFromWorldZ(rayPosition.z()));

        crossingLevelFront = computeCrossingLevel(scalar);

        if (crossingLevelFront != crossingLevelBack)
        {
            bisectionCorrection(sceneView, &rayPosition, &lambda,
                                prevRayPosition, prevLambda,
                                &crossingLevelFront, &crossingLevelBack);

            // Stop after first isosurface crossing.
            QVector3D lonLatP = rayPosition;
            lonLatP.setZ(sceneView->pressureFromWorldZ(rayPosition.z()));

            LOG4CPLUS_TRACE_FMT(mlog, "Isosurface hit at position %.2f "
                                "deg/%.2f deg/%.2f hPa",
                                lonLatP.x(), lonLatP.y(), lonLatP.z());

            updatePositionCrossGeometry(lonLatP);

            if (analysisControl)
            {
                MDataRequestHelper rh;
                rh.insert("POS_LONLATP", lonLatP);
                analysisControl->run(rh.request());
            }

            return true;
        }

        prevLambda  = lambda;
        prevRayPosition = rayPosition;

        lambda += stepSize;
        rayPosition += rayPosIncrement;

        crossingLevelBack = crossingLevelFront;
    } // raycaster loop

    // If we arrive here no isosurface has been hit.
    LOG4CPLUS_TRACE(mlog, "No isosurface could be identified at mouse position.");

    QVector3D lonLatP = rayPosition;
    lonLatP.setZ(sceneView->pressureFromWorldZ(rayPosition.z()));
    updatePositionCrossGeometry(lonLatP);

    return false;
}


QList<MVerticalLevelType> MNWPVolumeRaycasterActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>()
            << HYBRID_SIGMA_PRESSURE_3D
            << PRESSURE_LEVELS_3D
            << LOG_PRESSURE_LEVELS_3D
            << AUXILIARY_PRESSURE_3D
            << RADAR_LEVELS_3D);
}


MNWPActorVariable* MNWPVolumeRaycasterActor::createActorVariable(
        const MSelectableDataVariable& dataVariable)
{
    MNWP3DVolumeActorVariable* newVar = new MNWP3DVolumeActorVariable(this);

    newVar->dataSourceID = dataVariable.dataSourceID;
    newVar->levelType = dataVariable.levelType;
    newVar->variableName = dataVariable.variableName;

    return newVar;
}


void MNWPVolumeRaycasterActor::onBoundingBoxChanged()
{
    labels.clear();
    if (suppressActorUpdates())
    {
        return;
    }
    // Switching to no bounding box only needs a redraw, but no recomputation
    // because it disables rendering of the actor.
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        emitActorChangedSignal();
        return;
    }
    generateVolumeBoxGeometry();

    updateNextRenderFrame.set(UpdateShadowImage);
    updateNextRenderFrame.set(ComputeNCInitPoints);
    updateNextRenderFrame.set(RecomputeNCLines);
    updateNextRenderFrame.set(RenderVolumeLighting);

    emitActorChangedSignal();
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MNWPVolumeRaycasterActor::updateShadow()
{
    updateNextRenderFrame.set(UpdateShadowImage);
    updateNextRenderFrame.set(RenderVolumeLighting);
}

void MNWPVolumeRaycasterActor::updateVolumeLight()
{
    updateNextRenderFrame.set(UpdateShadowImage);
    updateNextRenderFrame.set(RenderVolumeLighting);
    emitActorChangedSignal();
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MNWPVolumeRaycasterActor::initializeActorResources()
{
    // Parent initialisation (triggers loading of initial data fields).
    MNWPMultiVarActor::initializeActorResources();

    // generate bounding box
    generateVolumeBoxGeometry();
    updatePositionCrossGeometry((QVector3D(0., 0., 1050.)));

    // generate and load shaders
    bool loadShaders = false;

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    loadShaders |= glRM->generateEffectProgram("multiactor_bbox",
                                               gl.boundingBoxShader);
    loadShaders |= glRM->generateEffectProgram("multiactor_raycaster",
                                               gl.rayCasterEffect);
    loadShaders |= glRM->generateEffectProgram("multiactor_shadowimage",
                                               gl.shadowImageRenderShader);
    loadShaders |= glRM->generateEffectProgram("multiactor_normalcurve_geom",
                                               gl.normalCurveGeometryEffect);
    loadShaders |= glRM->generateEffectProgram("multiactor_normalcurve_init",
                                               gl.normalCurveInitPointsShader);
    loadShaders |= glRM->generateEffectProgram("multiactor_normalcurve_comp",
                                               gl.normalCurveLineComputeShader);
    loadShaders |= glRM->generateEffectProgram("multiactor_racaster_bitfield",
                                               gl.bitfieldRayCasterEffect);

    if (loadShaders)
    {
        reloadShaderEffects();
    }
    else
    {
        initializeRenderInformation();
    }

    if (gl.texUnitShadowImage >=0) releaseTextureUnit(gl.texUnitShadowImage);
    gl.texUnitShadowImage = assignTextureUnit();

    if (gl.texUnitVolumeLightMap3D >= 0) releaseTextureUnit(gl.texUnitVolumeLightMap3D);
    gl.texUnitVolumeLightMap3D = assignTextureUnit();

    if (gl.textureUnitDepthBufferMirror >= 0)
    {
        releaseTextureUnit(gl.textureUnitDepthBufferMirror);
    }
    gl.textureUnitDepthBufferMirror = assignTextureUnit();

    if (gl.texUnitVolumeLightMap3D >= 0) releaseTextureUnit(gl.texUnitVolumeLightMap3D);
    gl.texUnitVolumeLightMap3D = assignTextureUnit();
}


void MNWPVolumeRaycasterActor::initializeRenderInformation()
{
    gl.rayCasterSubroutines.resize(MVerticalLevelType::SIZE_LEVELTYPES);
    gl.bitfieldRayCasterSubroutines.resize(MVerticalLevelType::SIZE_LEVELTYPES);
    gl.normalCompSubroutines.resize(MVerticalLevelType::SIZE_LEVELTYPES);
    gl.normalInitSubroutines.resize(MVerticalLevelType::SIZE_LEVELTYPES);

    gl.rayCasterSubroutines[PRESSURE_LEVELS_3D]
            << "samplePressureLevel"
            << "pressureLevelGradient";

    gl.rayCasterSubroutines[HYBRID_SIGMA_PRESSURE_3D]
            << "sampleHybridLevel"
            << "hybridLevelGradient";

    gl.rayCasterSubroutines[AUXILIARY_PRESSURE_3D]
            << "sampleAuxiliaryPressure"
            << "auxiliaryPressureGradient";

    gl.rayCasterSubroutines[RADAR_LEVELS_3D]
            << "sampleRadarGrid"
            << "radarGradient";

    gl.bitfieldRayCasterSubroutines[PRESSURE_LEVELS_3D]
            << "samplePressureLevelVolumeBitfield"
            << "samplePressureVolumeAllBits"
            << "pressureLevelGradientBitfield";

    gl.bitfieldRayCasterSubroutines[HYBRID_SIGMA_PRESSURE_3D]
            << "sampleHybridSigmaVolumeBitfield"
            << "sampleHybridVolumeAllBits"
            << "hybridLevelGradientBitfield";

    gl.bitfieldRayCasterSubroutines[AUXILIARY_PRESSURE_3D]
            << "sampleAuxiliaryPressureVolumeBitfield"
            << "sampleAuxiliaryPressureVolumeAllBits"
            << "auxiliaryPressureGradientBitfield";

    gl.normalCompSubroutines[PRESSURE_LEVELS_3D]
            << "samplePressureLevel"
            << "pressureLevelGradient";

    gl.normalCompSubroutines[HYBRID_SIGMA_PRESSURE_3D]
            << "sampleHybridLevel"
            << "hybridLevelGradient";

    gl.normalCompSubroutines[AUXILIARY_PRESSURE_3D]
            << "sampleAuxiliaryPressure"
            << "auxiliaryPressureGradient";

    gl.normalCompSubroutines[RADAR_LEVELS_3D]
        << "sampleRadarGrid"
        << "radarGradient";

    gl.normalInitSubroutines[PRESSURE_LEVELS_3D]
            << "samplePressureLevel";

    gl.normalInitSubroutines[HYBRID_SIGMA_PRESSURE_3D]
            << "sampleHybridLevel";

    gl.normalInitSubroutines[AUXILIARY_PRESSURE_3D]
            << "sampleAuxiliaryPressure";

    gl.normalInitSubroutines[RADAR_LEVELS_3D]
        << "sampleRadarGrid";

    // Re-compute normal curves and shadow image on next frame.
    updateNextRenderFrame.set(ComputeNCInitPoints);
    updateNextRenderFrame.set(RecomputeNCLines);
    updateNextRenderFrame.set(UpdateShadowImage);
    updateNextRenderFrame.set(RenderVolumeLighting);
}


void MNWPVolumeRaycasterActor::renderToCurrentContext(
        MSceneViewGLWidget* sceneView)
{
    // Check for available bounding box.
    // =================================
    if ( getBBoxConnection()->getBoundingBox() == nullptr )
    {
        return;
    }

    // Render volume bounding box
    // ==========================
    if (bBoxEnabledProp)
    {
        renderBoundingBox(sceneView);
    }

    // Check for valid actor variables.
    // ================================

    if ( variables.empty() )
    {
        return;
    }

    auto vVar = dynamic_cast<MNWP3DVolumeActorVariable*>(varProp.value());
    auto vShadingVar = dynamic_cast<MNWP3DVolumeActorVariable*>(shadingVarProp.value());
    auto secVar = dynamic_cast<MNWP3DVolumeActorVariable*>(dvrSettings->secVariableProp.value());
    auto thirdVar = dynamic_cast<MNWP3DVolumeActorVariable*>(dvrSettings->thirdVariableProp.value());

    // Are the variable grids valid objects?
    if (renderMode != RenderMode::DVR)
    {
        if (!vVar->hasData() || !vShadingVar->hasData()) return;
    }
    else
    {
        // Dont render if any variable has no data => variables are not ready yet
        if (!vVar->hasData()
                || !secVar->hasData()
                || !thirdVar->hasData()
                || !vShadingVar->hasData()) return;
    }


    // If the variable's bitfield shall be rendered, does the grid contain
    // valid flags?
    if (renderMode == RenderMode::Bitfield)
        if (!vVar->grid->flagsEnabled()) return;

    // In analysis mode, render a cross at the position where the user
    // has clicked.
    if (sceneView->analysisModeEnabled()) renderPositionCross(sceneView);

    // Compute (if requested) and render normal curves and shadow map.
    // ===============================================================
    if (normalCurveSettings->normalCurvesEnabledProp)
    {
        if (updateNextRenderFrame[RecomputeNCLines])
        {
            computeNormalCurves(sceneView);
        }
    }

    bool updateLighting = sceneView->sceneLightUpdated();

    // Create volume lightmap.
    if (!gl.volumeLightMap3DBuffer)
    {
        auto glRM = MGLResourcesManager::getInstance();
        QString id = QString("lighting_image_3D_actor_#%1").arg(myID);
        bool existed;

        const GLuint scaling = sceneView->interactionModeEnabled() ? volumeLightingSettings->interactivityScaleFactorProp : 1;
        GLint resX = volumeLightingSettings->lightmapLonResProp / scaling;
        GLint resY = volumeLightingSettings->lightmapLatResProp / scaling;
        GLint resZ = volumeLightingSettings->lightmapZResProp / scaling;

        gl.volumeLightMap3DBuffer = glRM->createTexture(id, &existed, GL_TEXTURE_3D, GL_RGBA8, resX, resY, resZ);
        gl.volumeLightMap3DBuffer->bindToTextureUnit(gl.texUnitVolumeLightMap3D);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA8, resX, resY, resZ, 0,  GL_RGBA, GL_UNSIGNED_BYTE, nullptr); CHECK_GL_ERROR;
    }

    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING)
    {
        // Render shadows of Iso surface into cascaded shadow maps.
        if (renderMode == RenderMode::Original)
        {
            if (!gl.cascadedShadowUpdateGuard &&
                    (updateNextRenderFrame[UpdateShadowImage] || updateLighting))
            {
                createIsoPositionBuffer(sceneView);
            }

            renderIsoPositionsToShadow(sceneView);
        }
        else if (renderMode == RenderMode::DVR)
        {
            if (!gl.cascadedShadowUpdateGuard &&
                (updateNextRenderFrame[RenderVolumeLighting] || updateLighting)
                    && volumeLightingSettings->enabled)
            {
                renderVolumeLighting(sceneView, sceneView->userIsInteractingWithScene());
            }

            if (!gl.cascadedShadowUpdateGuard && updateNextRenderFrame[UpdateShadowImage])
            {
                createShadowImage(sceneView);
            }

            gl.cascadedShadowUpdateGuard = true;
        }
    }

    if (normalCurveSettings->normalCurvesEnabledProp)
    {
        renderNormalCurves(sceneView);
    }

    // We do not render the volume ray-caster bbox during shadow mapping.
    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING)
    {
        return;
    }

    if (renderMode == RenderMode::DVR && castsShadows())
    {
        renderShadows(sceneView);
    }

    // Reset iso surface position buffer guard, as we are now allowed to update
    // it next frame.
    gl.cascadedShadowUpdateGuard = false;

#ifdef ENABLE_GPU_TIMING
    MGpuTimer timer;
    timer.start();
#endif

    // Raycaster.
    // ==========

    switch(renderMode)
    {
    case RenderMode::Original:
    case RenderMode::DVR_VOL_LIGHT:
    case RenderMode::DVR:
        renderRayCaster(gl.rayCasterEffect, sceneView);
        break;

    case RenderMode::Bitfield:
        renderRayCaster(gl.bitfieldRayCasterEffect, sceneView);
        break;
    }

#ifdef ENABLE_GPU_TIMING
    timer.stop();
    LOG4CPLUS_INFO(mlog, "Raycaster render time: " << timer.getElapsedTime(MStopwatch::MILLISECONDS));
#endif

    // OpenGL "cleanup".
    // =================

    // Disable polygon offset and face culling
    glDisable(GL_POLYGON_OFFSET_FILL); CHECK_GL_ERROR;
    glDisable(GL_CULL_FACE); CHECK_GL_ERROR;

    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
}


void MNWPVolumeRaycasterActor::dataFieldChangedEvent()
{
    if (!isInitialized()) return;

    // Re-compute normal curves and shadow image on next render frame if the
    // data field has changed.
    updateNextRenderFrame.set(ComputeNCInitPoints);
    updateNextRenderFrame.set(RecomputeNCLines);
    updateNextRenderFrame.set(UpdateShadowImage);
    updateNextRenderFrame.set(RenderVolumeLighting);

    emitActorChangedSignal();
}


bool MNWPVolumeRaycasterActor::rayBoxIntersection(
        QVector3D rayOrigin, QVector3D rayDirection,
        QVector3D boxCrnr1, QVector3D boxCrnr2, QVector2D *tNearFar)
{
    float tnear = 0.;
    float tfar  = 0.;

    QVector3D rayDirInv = QVector3D(1./rayDirection.x(), 1./rayDirection.y(),
                                    1./rayDirection.z());
    if (rayDirInv.x() >= 0.0)
    {
        tnear = (boxCrnr1.x() - rayOrigin.x()) * rayDirInv.x();
        tfar  = (boxCrnr2.x() - rayOrigin.x()) * rayDirInv.x();
    }
    else
    {
        tnear = (boxCrnr2.x() - rayOrigin.x()) * rayDirInv.x();
        tfar  = (boxCrnr1.x() - rayOrigin.x()) * rayDirInv.x();
    }

    if (rayDirInv.y() >= 0.0)
    {
        tnear = std::max(tnear, float((boxCrnr1.y() - rayOrigin.y()) * rayDirInv.y()));
        tfar  = std::min(tfar,  float((boxCrnr2.y() - rayOrigin.y()) * rayDirInv.y()));
    }
    else
    {
        tnear = std::max(tnear, float((boxCrnr2.y() - rayOrigin.y()) * rayDirInv.y()));
        tfar  = std::min(tfar,  float((boxCrnr1.y() - rayOrigin.y()) * rayDirInv.y()));
    }

    if (rayDirInv.z() >= 0.0)
    {
        tnear = std::max(tnear, float((boxCrnr1.z() - rayOrigin.z()) * rayDirInv.z()));
        tfar  = std::min(tfar,  float((boxCrnr2.z() - rayOrigin.z()) * rayDirInv.z()));
    }
    else
    {
        tnear = std::max(tnear, float((boxCrnr2.z() - rayOrigin.z()) * rayDirInv.z()));
        tfar  = std::min(tfar,  float((boxCrnr1.z() - rayOrigin.z()) * rayDirInv.z()));
    }

    tNearFar->setX(tnear);
    tNearFar->setY(tfar);
    return (tnear < tfar);
}


int MNWPVolumeRaycasterActor::computeCrossingLevel(float scalar)
{
    int level = 0;

//TODO (mr, 17Nov2014) -- replace by numEnabledIsoValues?
    for (int i = 0; i < rayCasterSettings->isoValueSetList.size(); i++)
    {
        if (rayCasterSettings->isoEnabled[i])
            level += int(scalar >= rayCasterSettings->isoValues[i]);
    }

    return level;
}


void MNWPVolumeRaycasterActor::bisectionCorrection(
        MSceneViewGLWidget *sceneView,
        QVector3D *rayPosition, float *lambda, QVector3D prevRayPosition,
        float prevLambda, int *crossingLevelFront, int *crossingLevelBack)
{
    QVector3D rayCenterPosition;
    float centerLambda;
    int crossingLevelCenter;

    for (int i = 0; i < dataSamplingSettings->bisectionStepsProp; i++)
    {
        rayCenterPosition = (*rayPosition + prevRayPosition) / 2.0f;
        centerLambda = (*lambda + prevLambda) / 2.0f;

        float scalar = varProp->grid->interpolateValue(
                    rayCenterPosition.x(), rayCenterPosition.y(),
                    sceneView->pressureFromWorldZ(rayCenterPosition.z()));

        crossingLevelCenter = computeCrossingLevel(scalar);

        if (crossingLevelCenter != *crossingLevelBack)
        {
            *rayPosition = rayCenterPosition;
            *lambda = centerLambda;
            *crossingLevelFront = crossingLevelCenter;
        }
        else
        {
            prevRayPosition = rayCenterPosition;
            prevLambda = centerLambda;
            *crossingLevelBack = crossingLevelCenter;
        }
    }
}


void MNWPVolumeRaycasterActor::onDeleteActorVariable(MNWPActorVariable *variable)
{
    varProp.removeVariable(variable);
    shadingVarProp.removeVariable(variable);
    dvrSettings->secVariableProp.removeVariable(variable);
    dvrSettings->thirdVariableProp.removeVariable(variable);

    updateNextRenderFrame.set(ComputeNCInitPoints);
    updateNextRenderFrame.set(RecomputeNCLines);
    updateNextRenderFrame.set(UpdateShadowImage);
    updateNextRenderFrame.set(RenderVolumeLighting);
}


void MNWPVolumeRaycasterActor::onAddActorVariable(MNWPActorVariable *variable)
{
    varProp.addVariable(variable);
    shadingVarProp.addVariable(variable);
    dvrSettings->secVariableProp.addVariable(variable);
    dvrSettings->thirdVariableProp.addVariable(variable);
}


void MNWPVolumeRaycasterActor::onChangeActorVariable(MNWPActorVariable *variable)
{
    enableActorUpdates(false);
    varProp.changeVariable(variable);
    shadingVarProp.changeVariable(variable);
    dvrSettings->secVariableProp.changeVariable(variable);
    dvrSettings->thirdVariableProp.changeVariable(variable);
    enableActorUpdates(true);
}


void MNWPVolumeRaycasterActor::onChangeActorVariableTransferFunction(MNWPActorVariable *variable)
{
    Q_UNUSED(varProp);
    updateNextRenderFrame.set(RenderVolumeLighting);
    updateNextRenderFrame.set(UpdateShadowImage);
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MNWPVolumeRaycasterActor::generateVolumeBoxGeometry()
{
    if ( getBBoxConnection()->getBoundingBox() == nullptr )
    {
        return;
    }

    // Define geometry for bounding box
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    const int numVertices = 8;
    float vertexData[] =
    {
        0, 0, 0, // node 0
        0, 1, 0, // node 1
        1, 1, 0, // node 2
        1, 0, 0, // node 3
        0, 0, 1, // node 4
        0, 1, 1, // node 5
        1, 1, 1, // node 6
        1, 0, 1  // node 7
    };

    const int numIndices = 16 + 36;
    GLushort indexData[] =
    {
        // volume box lines
        0, 1, 2, 3, 0,
        4, 7, 3,
        7, 6, 2,
        6, 5, 1,
        5, 4,

        // bottom
        0, 3, 1,
        3, 2, 1,
        // front
        0, 4, 7,
        0, 7, 3,
        // left
        0, 1, 4,
        1, 5, 4,
        // right
        3, 7, 2,
        7, 6, 2,
        // back
        1, 2, 6,
        1, 6, 5,
        // top
        5, 6, 7,
        5, 7, 4
    };

    // convert vertices to lat/lon/p space
    for (int i = 0; i < numVertices; i++) {
        vertexData[i * 3 + 0] = getBBoxConnection()->westLon() + vertexData[i * 3 + 0]
                * (getBBoxConnection()->eastLon() - getBBoxConnection()->westLon());
        vertexData[i * 3 + 1] = getBBoxConnection()->northLat() - vertexData[i * 3 + 1]
                * (getBBoxConnection()->northLat() - getBBoxConnection()->southLat());
        vertexData[i * 3 + 2] = (vertexData[i * 3 + 2] == 0)
                ? getBBoxConnection()->bottomPressure_hPa()
                : getBBoxConnection()->topPressure_hPa();
    }

    if (gl.vboBoundingBox)
    {
        GL::MFloat3VertexBuffer* buf = dynamic_cast<GL::MFloat3VertexBuffer*>(
                    gl.vboBoundingBox);
        buf->update(vertexData, numVertices);
    }
    else
    {
        const QString vboID = QString("vbo_bbox_actor#%1").arg(myID);

        GL::MFloat3VertexBuffer* buf =
                new GL::MFloat3VertexBuffer(vboID, numVertices);

        if (glRM->tryStoreGPUItem(buf))
        {
            buf->upload(vertexData, numVertices);
            gl.vboBoundingBox = static_cast<GL::MVertexBuffer*>(
                        glRM->getGPUItem(vboID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store buffer for volume"
                           " bbox in GPU memory.");
            delete buf;
            return;
        }

    }

    glGenBuffers(1, &gl.iboBoundingBox); CHECK_GL_ERROR;
    //cout << "uploading raycaster elements indices to IBO" << gl.iboBoundingBox << "\n" << flush;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl.iboBoundingBox); CHECK_GL_ERROR;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                 numIndices * sizeof(GLushort),
                 indexData,
                 GL_STATIC_DRAW); CHECK_GL_ERROR;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
}


void MNWPVolumeRaycasterActor::updatePositionCrossGeometry(
        QVector3D worldSpacePosition)
{
    float size = 2.;
    QVector<QVector3D> vertices;
    QVector3D &p = worldSpacePosition;
    vertices.append(QVector3D(p.x()-size, p.y(), p.z()));
    vertices.append(QVector3D(p.x()+size, p.y(), p.z()));
    vertices.append(QVector3D(p.x(), p.y()-size, p.z()));
    vertices.append(QVector3D(p.x(), p.y()+size, p.z()));
    vertices.append(QVector3D(p.x(), p.y(), p.z()-40.));
    vertices.append(QVector3D(p.x(), p.y(), p.z()+40.));

    const QString vboID = QString("vbo_positioncross_actor#%1").arg(myID);
    uploadVec3ToVertexBuffer(vertices, vboID, &gl.vboPositionCross);
}


void MNWPVolumeRaycasterActor::setBoundingBoxShaderVars(
        MSceneViewGLWidget* sceneView)
{
    gl.boundingBoxShader->bindProgram("Pressure");
    gl.boundingBoxShader->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    gl.boundingBoxShader->setUniformValue(
                "pToWorldZParams", sceneView->pressureToWorldZParameters());
    gl.boundingBoxShader->setUniformValue(
                "colour", QColor(Qt::black));
}


void MNWPVolumeRaycasterActor::setVarSpecificShaderVars(
        std::shared_ptr<GL::MShaderEffect>& shader,
        MSceneViewGLWidget* sceneView,
        MNWP3DVolumeActorVariable* var,
        const QString& structName,
        const QString& volumeName,
        const QString& transferFuncName,
        const QString& pressureTableName,
        const QString& surfacePressureName,
        const QString& hybridCoeffName,
        const QString& lonLatLevAxesName,
        const QString& pressureTexCoordTable2DName,
        const QString& minMaxAccelStructure3DName,
        const QString& dataFlagsVolumeName,
        const QString& auxPressureField3DName
        )
{
    // Reset optional textures to avoid draw errors.
    // =============================================

    // 1D textures...
    var->textureDummy1D->bindToTextureUnit(var->textureUnitDummy1D);
    shader->setUniformValue(pressureTableName, var->textureUnitDummy1D); CHECK_GL_ERROR;
    shader->setUniformValue(hybridCoeffName, var->textureUnitDummy1D); CHECK_GL_ERROR;
    shader->setUniformValue(transferFuncName, var->textureUnitDummy1D); CHECK_GL_ERROR;
    if (!transferFuncName.isNull() && var->transferFunction != nullptr)
    {
        shader->setUniformValue(transferFuncName, var->textureUnitTransferFunction); CHECK_GL_ERROR;
    }

    // 2D textures...
    var->textureDummy2D->bindToTextureUnit(var->textureUnitDummy2D);
    shader->setUniformValue(surfacePressureName, var->textureUnitDummy2D); CHECK_GL_ERROR;
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
    shader->setUniformValue(pressureTexCoordTable2DName, var->textureUnitDummy2D); CHECK_GL_ERROR;
#endif

    // 3D textures...
    var->textureDummy3D->bindToTextureUnit(var->textureUnitDummy3D);
    shader->setUniformValue(dataFlagsVolumeName, var->textureUnitDummy3D); CHECK_GL_ERROR;
    shader->setUniformValue(auxPressureField3DName, var->textureUnitDummy3D); CHECK_GL_ERROR;
    shader->setUniformValue(minMaxAccelStructure3DName, var->textureUnitDummy3D); CHECK_GL_ERROR;


    // Bind textures and set uniforms.
    // ===============================

    // Bind volume data
    var->textureDataField->bindToTextureUnit(
                var->textureUnitDataField);  CHECK_GL_ERROR;
    shader->setUniformValue(
                volumeName,
                var->textureUnitDataField); CHECK_GL_ERROR;

    // Texture bindings for transfer function for data field (1D texture from
    // transfer function class).
    if (var->transferFunction && !transferFuncName.isNull())
    {
        var->transferFunction->getTexture()->bindToTextureUnit(
                    var->textureUnitTransferFunction);
        shader->setUniformValue(
                    transferFuncName,
                    var->textureUnitTransferFunction); CHECK_GL_ERROR;

        shader->setUniformValue(
                    structName + ".tfMinimum",
                    var->transferFunction->getMinimumValue()); CHECK_GL_ERROR;
        shader->setUniformValue(
                    structName + ".tfMaximum",
                    var->transferFunction->getMaximumValue()); CHECK_GL_ERROR;
    }
    else
    {
        shader->setUniformValue(structName + ".tfMinimum", 0.); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".tfMaximum", 0.); CHECK_GL_ERROR;
    }

    if (var->gridType == MNWPActorVariable::STRUCTURED_GRID)
    {
        var->textureLonLatLevAxes->bindToTextureUnit(var->textureUnitLonLatLevAxes);
        shader->setUniformValue(lonLatLevAxesName, var->textureUnitLonLatLevAxes); CHECK_GL_ERROR;
    }

#ifdef ENABLE_RAYCASTER_ACCELERATION
    // Bind acceleration grid.
    var->textureMinMaxAccelStructure->bindToTextureUnit(
                var->textureUnitMinMaxAccelStructure);
    shader->setUniformValue(
                minMaxAccelStructure3DName,
                var->textureUnitMinMaxAccelStructure); CHECK_GL_ERROR;
#endif

    if (var->textureDataFlags != nullptr)
    {
        // The data flags texture will only be valid if the grid contains
        // a flags field and this actor's render mode requests the flags
        // bitfield.
        var->textureDataFlags->bindToTextureUnit(var->textureUnitDataFlags); CHECK_GL_ERROR;
        shader->setUniformValue("flagsVolume", var->textureUnitDataFlags);
    }

    // Set uniforms specific to data var level type.
    // =============================================

    // North-west & south-east corners.
    QVector3D dataNWCrnr;
    QVector3D dataSECrnr;

    if (var->gridType == MNWPActorVariable::RADAR_GRID)
    {
        dataNWCrnr = var->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataNWCrnr.setZ(sceneView->worldZfromPressure(dataNWCrnr.z()));
        dataSECrnr = var->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();
        dataSECrnr.setZ(sceneView->worldZfromPressure(dataSECrnr.z()));
    }
    else // Structured grid
    {
        dataNWCrnr = var->grid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataNWCrnr.setZ(sceneView->worldZfromPressure(dataNWCrnr.z()));
        dataSECrnr = var->grid->getSouthEastBottomDataVolumeCorner_lonlatp();
        dataSECrnr.setZ(sceneView->worldZfromPressure(dataSECrnr.z()));
    }

    shader->setUniformValue(structName + ".radarRange", 0); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".standardAzimuthGridsize", 0); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".lonLatHeightRadar", QVector3D()); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".cartesianCoordinatesRadar", QVector3D()); CHECK_GL_ERROR;


    if (var->grid->getLevelType() == PRESSURE_LEVELS_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(0)); CHECK_GL_ERROR;

        // Bind pressure to texture coordinate LUT.
        var->texturePressureTexCoordTable->bindToTextureUnit(
                    var->textureUnitPressureTexCoordTable);
        shader->setUniformValue(
                    pressureTableName,
                    var->textureUnitPressureTexCoordTable); CHECK_GL_ERROR;

        // Helper variables for texture coordinate LUT.
        const GLint nPTable = var->texturePressureTexCoordTable->getWidth();
        const GLfloat deltaZ_PTable = abs(dataSECrnr.z() - dataNWCrnr.z()) / (nPTable - 1);
        const GLfloat upperPTableBoundary = dataNWCrnr.z() + deltaZ_PTable / 2.0f;
        const GLfloat vertPTableExtent = abs(dataNWCrnr.z() - dataSECrnr.z()) + deltaZ_PTable;
        // shader->setUniformValue(structName + ".nPTable", nPTable); CHECK_GL_ERROR;
        // shader->setUniformValue(structName + ".deltaZ_PTable", deltaZ_PTable); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".upperPTableBoundary", upperPTableBoundary); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".vertPTableExtent", vertPTableExtent); CHECK_GL_ERROR;
    }

    else if (var->grid->getLevelType() == LOG_PRESSURE_LEVELS_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(2)); CHECK_GL_ERROR;
    }

    else if (var->grid->getLevelType() == HYBRID_SIGMA_PRESSURE_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(1)); CHECK_GL_ERROR;

        // Bind hybrid coefficients
        var->textureHybridCoefficients->bindToTextureUnit(var->textureUnitHybridCoefficients);
        shader->setUniformValue(
                    hybridCoeffName,
                    var->textureUnitHybridCoefficients); CHECK_GL_ERROR;

        // Bind surface pressure
        var->textureSurfacePressure->bindToTextureUnit(var->textureUnitSurfacePressure);
        shader->setUniformValue(
                    surfacePressureName,
                    var->textureUnitSurfacePressure); CHECK_GL_ERROR;

#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
        // Bind pressure to texture coordinate LUT.
        var->texturePressureTexCoordTable->bindToTextureUnit(
                    var->textureUnitPressureTexCoordTable);
        shader->setUniformValue(
                    pressureTexCoordTable2DName,
                    var->textureUnitPressureTexCoordTable); CHECK_GL_ERROR;
#endif
    }

    else if (var->grid->getLevelType() == AUXILIARY_PRESSURE_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(2)); CHECK_GL_ERROR;

        // Bind pressure field.
        var->textureAuxiliaryPressure
                ->bindToTextureUnit(var->textureUnitAuxiliaryPressure);
        shader->setUniformValue(
                    auxPressureField3DName,
                    var->textureUnitAuxiliaryPressure); CHECK_GL_ERROR;
    }
    else if (var->gridType == MNWPActorVariable::RADAR_GRID)
    {
        shader->setUniformValue(structName + ".levelType", (GLint(6))); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".radarRange", (float) var->radarGrid->getMaxRadarRange()); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".standardAzimuthGridsize", (float) var->radarGrid->getNrays()); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".lonLatHeightRadar", var->radarGrid->getRadarStationLonLatHeight()); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".cartesianCoordinatesRadar", var->radarGrid->getRadarStationCartesianCoordinates()); CHECK_GL_ERROR;
        var->textureRadarRayAngles->bindToTextureUnit(var->textureUnitRadarRayAngles); CHECK_GL_ERROR;

        // Stores radar grid metadata: nbins, nrays, elevationIndexKey, scanIndex, range, binDistance, elevationAngle,
        // verticalBeamwidth, horizontalBeamwidth.
        var->radarMetadataBuffer->bindToIndex(9); CHECK_GL_ERROR;
    }

    // Precompute data extent variables and store in uniform struct.
    // =============================================================
    const GLfloat westernBoundary = dataNWCrnr.x() - var->grid->getDeltaLon() / 2.0f;
    const GLfloat eastWestExtent = dataSECrnr.x() - dataNWCrnr.x() + var->grid->getDeltaLon();
    const GLfloat northernBoundary = dataNWCrnr.y() + var->grid->getDeltaLat() / 2.0f;
    const GLfloat northSouthExtent = dataNWCrnr.y() - dataSECrnr.y() + var->grid->getDeltaLat();

    shader->setUniformValue(structName + ".westernBoundary", westernBoundary); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".eastWestExtent", eastWestExtent); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".northernBoundary", northernBoundary); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".northSouthExtent", northSouthExtent); CHECK_GL_ERROR;

    // For radar variables, nLon, nLat, nLev in grid are zero.
    const GLint nLon = var->grid->nlons;
    const GLint nLat = var->grid->nlats;
    const GLint nLev = var->grid->nlevs;
    const GLfloat deltaLnP = std::abs((dataSECrnr.z() - dataNWCrnr.z()) / (nLev-1)); // = 0 for radar.
    const GLfloat upperBoundary = dataNWCrnr.z() + deltaLnP /2.0f; // = dataNWCrnr.z() for radar.
    const GLfloat verticalExtent = std::abs(dataNWCrnr.z() - dataSECrnr.z()) + deltaLnP;

    shader->setUniformValue(structName + ".upperBoundary", upperBoundary); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".verticalExtent", verticalExtent); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".dataSECrnr", dataSECrnr); CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".dataNWCrnr", dataNWCrnr); CHECK_GL_ERROR;

    if (var->gridType == MNWPActorVariable::RADAR_GRID)
    {
        QVector3D nwtCorner = var->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
        QVector3D sebCorner = var->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();
        float dlat = 1.0 * abs(nwtCorner.y() - sebCorner.y())/(2*var->radarGrid->getNbins(0));
        float dlon = 1.0 * abs(nwtCorner.x() - sebCorner.x())/(2*var->radarGrid->getNbins(0));

        shader->setUniformValue(structName + ".deltaLat", dlat); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".deltaLon", dlon); CHECK_GL_ERROR;
        shader->setUniformValue(
                    structName + ".gridIsCyclicInLongitude",
                    false); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".nLon", -1); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".nLat", -1); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".nLev", -1); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".deltaLnP", -1); CHECK_GL_ERROR;
    }
    else // Structured grid.
    {
        // Assume that lat/lon spacing is the same.
        shader->setUniformValue(structName + ".deltaLat", var->grid->getDeltaLat()); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".deltaLon", var->grid->getDeltaLon()); CHECK_GL_ERROR;
        shader->setUniformValue(
                    structName + ".gridIsCyclicInLongitude",
                    var->grid->gridIsCyclicInLongitude()); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".nLon", nLon); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".nLat", nLat); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".nLev", nLev); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".deltaLnP", deltaLnP); CHECK_GL_ERROR;
    }
}


void MNWPVolumeRaycasterActor::setCommonShaderVars(
        std::shared_ptr<GL::MShaderEffect>& shader, MSceneViewGLWidget* sceneView)
{
    // Set common shader variables.
    // ============================

    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING
            && renderMode == RenderMode::Original)
    {
        MLightActor *light = sceneView->getSceneDirectionalLight();
        shader->setUniformValue(
                "mvpMatrix", gl.isoDirLightMvp[light]);  CHECK_GL_ERROR;
        shader->setUniformValue(
                "mvpMatrixInverted",
                gl.isoDirLightMvp[light].inverted()); CHECK_GL_ERROR;
        shader->setUniformValue(
                "cameraPosition", gl.isoDirLightPos[light]);  CHECK_GL_ERROR;
        shader->setUniformValue("cameraDirection", gl.isoDirLightDir[light]); CHECK_GL_ERROR;
        shader->setUniformValue("isOrthographic", true); CHECK_GL_ERROR;
    }
    else
    {
        shader->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));  CHECK_GL_ERROR;
        shader->setUniformValue(
                "mvpMatrixInverted",
                *(sceneView->getModelViewProjectionMatrixInverted())); CHECK_GL_ERROR;
        shader->setUniformValue(
                "cameraPosition", sceneView->getCamera()->getOrigin());  CHECK_GL_ERROR;
        shader->setUniformValue("cameraDirection",sceneView->getCamera()->getZAxis()); CHECK_GL_ERROR;
        shader->setUniformValue("isOrthographic", sceneView->orthographicModeEnabled());
        CHECK_GL_ERROR;
    }

    shader->setUniformValue("shadowMode", false); CHECK_GL_ERROR;

    // In single member mode, current ensemble member (used to access single
    // bits from the bitfield in the shader).
    shader->setUniformValue("ensembleMember", varProp->getEnsembleMember()); CHECK_GL_ERROR;

    shader->setUniformValue(
                "pToWorldZParams",
                sceneView->pressureToWorldZParameters()); CHECK_GL_ERROR;

    float topP = getBBoxConnection()->topPressure_hPa();
    float bottomP = getBBoxConnection()->bottomPressure_hPa();

    shader->setUniformValue(
                "volumeBottomSECrnr",
                QVector3D(getBBoxConnection()->eastLon(), // upper-right == north-east
                  getBBoxConnection()->southLat(),
                          sceneView->worldZfromPressure(bottomP)));
    CHECK_GL_ERROR;

    shader->setUniformValue(
                "volumeTopNWCrnr",
                QVector3D(getBBoxConnection()->westLon(), // lower-left == south-west
                  getBBoxConnection()->northLat(),
                          sceneView->worldZfromPressure(topP)));
    CHECK_GL_ERROR;

    QVector3D renderRegionBottomSWCrnr;
    QVector3D renderRegionTopNECrnr;

    getRenderRegion(renderRegionBottomSWCrnr, renderRegionTopNECrnr, sceneView);

    shader->setUniformValue("renderRegionBottomSWCrnr", renderRegionBottomSWCrnr);
    CHECK_GL_ERROR;

    shader->setUniformValue("renderRegionTopNECrnr", renderRegionTopNECrnr);
    CHECK_GL_ERROR;

    shader->setUniformValue(
            "volumeTopP_hPa", topP);
    CHECK_GL_ERROR;

    shader->setUniformValue(
            "volumeBottomP_hPa", bottomP);
    CHECK_GL_ERROR;

    shader->setUniformValue(
            "volumePressureExtent_hPa", bottomP - topP);
    CHECK_GL_ERROR;

    shader->setUniformValue("enableVolLight", volumeLightingSettings->enabled); CHECK_GL_ERROR;
    shader->setUniformValue("usePressureScale", volumeLightingSettings->usePressureScaleProp); CHECK_GL_ERROR;

    shader->setUniformValue("useDithering", dvrSettings->useDitheringProp); CHECK_GL_ERROR;
    shader->setUniformValue("ditherStrength", dvrSettings->ditherStrengthProp); CHECK_GL_ERROR;
    shader->setUniformValue("refScaleUnit", dvrSettings->refScaleUnit); CHECK_GL_ERROR;
    shader->setUniformValue("refScale", dvrSettings->refScaleProp); CHECK_GL_ERROR;

    float worldZMeterScale;
    if (cloudSettings->realVerticalScaleProp)
    {
        worldZMeterScale = sceneView->worldZMeterScaling();
    }
    else
    {
        worldZMeterScale = EARTH_PERIMETER_METERS / 360.0f; // Earth perimeter / 360 degrees
    }


    shader->setUniformValue("worldZMeterScaling", worldZMeterScale); CHECK_GL_ERROR;
    shader->setUniformValue("verticalScaling", sceneView->getVerticalScaling()); CHECK_GL_ERROR;

    shader->setUniformValue("uniformViewScale", cloudSettings->uniformScaleProp && cloudSettings->realVerticalScaleProp); CHECK_GL_ERROR;

    shader->setUniformValue("ciwcSlot", cloudSettings->ciwcSlot);
    shader->setUniformValue("clwcSlot", cloudSettings->clwcSlot);
    shader->setUniformValue("ciwcColor", cloudSettings->ciwcColorProp); CHECK_GL_ERROR;
    shader->setUniformValue("clwcColor", cloudSettings->clwcColorProp); CHECK_GL_ERROR;
    shader->setUniformValue("useShadingVarForCloudColor", cloudSettings->useShadingVarColorProp); CHECK_GL_ERROR;
    shader->setUniformValue("earthCurvatureEnabled", cloudSettings->earthCosineEnabledProp); CHECK_GL_ERROR;
    shader->setUniformValue("spaceScale", cloudSettings->spaceScaleProp); CHECK_GL_ERROR;

    if (gl.volumeLightMap3DBuffer)
    {
        gl.volumeLightMap3DBuffer->bindToTextureUnit(gl.texUnitVolumeLightMap3D); CHECK_GL_ERROR;
        shader->setUniformValue("lightingVolume", gl.texUnitVolumeLightMap3D); CHECK_GL_ERROR;
    }

    if (volumeLightingSettings->enabled)
    {
        shader->setUniformValue("powderStrength", volumeLightingSettings->powderStrengthProp); CHECK_GL_ERROR;
        shader->setUniformValue("powderDepth", volumeLightingSettings->powderDepthProp); CHECK_GL_ERROR;
        shader->setUniformValue("densityScale", volumeLightingSettings->densityScaleProp); CHECK_GL_ERROR;
    }

    auto vVar = dynamic_cast<MNWP3DVolumeActorVariable*>(varProp.value());
    if (vVar->hasData())
    {
        setVarSpecificShaderVars(shader, sceneView, vVar, "dataExtent",
                                 "dataVolume","transferFunction",
                                 "pressureTable", "surfacePressure",
                                 "hybridCoefficients", "lonLatLevAxes",
                                 "pressureTexCoordTable2D", "minMaxAccel3D",
                                 "flagsVolume", "auxPressureField3D_hPa");
    }

    auto vShadingVar = dynamic_cast<MNWP3DVolumeActorVariable*>(shadingVarProp.value());
    if (vShadingVar->hasData())
    {
        setVarSpecificShaderVars(shader, sceneView, vShadingVar, "dataExtentShV",
                                 "dataVolumeShV","transferFunctionShV",
                                 "pressureTableShV", "surfacePressureShV",
                                 "hybridCoefficientsShV", "lonLatLevAxesShV",
                                 "pressureTexCoordTable2DShV", "minMaxAccel3DShV",
                                 "flagsVolumeShV", "auxPressureField3DShV_hPa");
    }

    auto secVar = dynamic_cast<MNWP3DVolumeActorVariable*>(dvrSettings->secVariableProp.value());
    if (secVar->hasData())
    {
        setVarSpecificShaderVars(shader, sceneView, secVar, "dataExtentSec",
                                 "dataVolumeSec", "transferFunctionSec",
                                 "pressureTableSec", "surfacePressureSec",
                                 "hybridCoefficientsSec", "lonLatLevAxesSec",
                                 "pressureTexCoordTable2DSec", "minMaxAccel3DSec",
                                 "flagsVolumeLWC", "auxPressureField3DSec_hPa");
    }

    auto thirdVar = dynamic_cast<MNWP3DVolumeActorVariable*>(dvrSettings->thirdVariableProp.value());
    if (thirdVar->hasData())
    {
        setVarSpecificShaderVars(shader, sceneView, thirdVar, "dataExtentThird",
                                 "dataVolumeThird", "transferFunctionThird",
                                 "pressureTableThird", "surfacePressureThird",
                                 "hybridCoefficientsThird", "lonLatLevAxesThird",
                                 "pressureTexCoordTable2DThird", "minMaxAccel3DThird",
                                 "flagsVolumeIWC", "auxPressureField3DThird_hPa");
    }

    GLboolean enabledVariableSlots[3];
    enabledVariableSlots[0] = vVar->transferFunction != nullptr
            || cloudSettings->ciwcSlot == 0
            || cloudSettings->clwcSlot == 0;
    enabledVariableSlots[1] = dvrSettings->secondVarEnabledProp && (secVar->transferFunction != nullptr
            || cloudSettings->ciwcSlot == 1
            || cloudSettings->clwcSlot == 1);
    enabledVariableSlots[2] = dvrSettings->thirdVarEnabledProp && (thirdVar->transferFunction != nullptr
            || cloudSettings->ciwcSlot == 2
            || cloudSettings->clwcSlot == 2);
    shader->setUniformValueArray("enabledVariableSlots", enabledVariableSlots, 3, 1);
    shader->setUniformValue("areVariableDataExtentsAligned", areVariableDataExtentsAligned(sceneView));

    bool isInteractive = sceneView->userIsInteractingWithScene();
    float stepSize = isInteractive ? volumeLightingSettings->interactiveStepSizeProp : volumeLightingSettings->stepSizeProp;
    float maxDist = volumeLightingSettings->lightMaxDistanceProp;

    int dli = -1; // Directional light index
    int pli = -1; // Point light index
    int sli = -1; // Spot light index
    int rli = -1; // Rect light index
    for (const MLightActor* light : sceneView->getScene()->getSceneLights())
    {
        if (light->isEnabled())
        {
            if (light->intensity() <= 0.0f || stepSize <= 0.0f) continue;

            QString uniformName;
            switch (light->getType())
            {
            case MLightActor::LightType::Directional:
                ++dli;
                if (dli >= MAX_NUM_DIRECTIONAL_LIGHTS)
                {
                    LOG4CPLUS_WARN(mlog, "Maximum number of active directional lights reached. Ignoring "
                                   << light->getName());
                    continue;
                }
                uniformName = QString("dirLights[%1].").arg(QString::number(dli));
                shader->setUniformValue(uniformName + "dir", light->direction());
                shader->setUniformValue(uniformName + "col", light->color());
                shader->setUniformValue(uniformName + "intensity", light->intensity());
                shader->setUniformValue(uniformName + "stepSize", stepSize);
                shader->setUniformValue(uniformName + "maxDist", maxDist);

                break;
            case MLightActor::LightType::Point:
                ++pli;
                if (pli >= MAX_NUM_POINT_LIGHTS)
                {
                    LOG4CPLUS_WARN(mlog, "Maximum number of active point lights reached. Ignoring "
                                   << light->getName());
                    continue;
                }
                uniformName = QString("pointLights[%1].").arg(QString::number(pli));
                shader->setUniformValue(uniformName + "pos", light->position());
                shader->setUniformValue(uniformName + "col", light->color());
                shader->setUniformValue(uniformName + "intensity", light->intensity());
                shader->setUniformValue(uniformName + "stepSize", stepSize);
                shader->setUniformValue(uniformName + "maxDist", maxDist);

                break;
            case MLightActor::LightType::Spot:
                if (light->spotlightHalfAngle() <= 0.0f) continue;
                ++sli;
                if (sli >= MAX_NUM_SPOT_LIGHTS)
                {
                    LOG4CPLUS_WARN(mlog, "Maximum number of active spot lights reached. Ignoring "
                                   << light->getName());
                    continue;
                }
                uniformName = QString("spotLights[%1].").arg(QString::number(sli));
                shader->setUniformValue(uniformName + "pos", light->position());
                shader->setUniformValue(uniformName + "dir", light->direction());
                shader->setUniformValue(uniformName + "col", light->color());
                shader->setUniformValue(uniformName + "intensity", light->intensity());
                shader->setUniformValue(uniformName + "stepSize", stepSize);
                shader->setUniformValue(uniformName + "maxDist", maxDist);
                shader->setUniformValue(uniformName + "angleCos", cos(light->spotlightHalfAngle()));

                break;
            case MLightActor::LightType::Rect:
                ++rli;
                if (rli >= MAX_NUM_RECT_LIGHTS)
                {
                    LOG4CPLUS_WARN(mlog, "Maximum number of active rectangular lights reached. Ignoring "
                                   << light->getName());
                    continue;
                }
                uniformName = QString("rectLights[%1].").arg(QString::number(rli));
                shader->setUniformValue(uniformName + "pos", light->position());
                shader->setUniformValue(uniformName + "dir", light->direction());
                shader->setUniformValue(uniformName + "col", light->color());
                shader->setUniformValue(uniformName + "intensity", light->intensity());
                shader->setUniformValue(uniformName + "stepSize", stepSize);
                shader->setUniformValue(uniformName + "maxDist", maxDist);
                shader->setUniformValue(uniformName + "rightW", QVector4D(light->right(), light->getRectW()));
                shader->setUniformValue(uniformName + "upH", QVector4D(light->up(), light->getRectH()));

                break;
            default:
                break;
            }
        }
    }

    shader->setUniformValue("numDirectionalLights", dli + 1);
    shader->setUniformValue("numPointLights", pli + 1);
    shader->setUniformValue("numSpotLights", sli + 1);
    shader->setUniformValue("numRectLights", rli + 1);
}


void MNWPVolumeRaycasterActor::setRayCasterShaderVars(
        std::shared_ptr<GL::MShaderEffect>& shader, MSceneViewGLWidget* sceneView)
{
    setCommonShaderVars(shader, sceneView);

    // 1) Bind the depth buffer texture to the current program.

    // OpenGL depth buffer, mirrored by mirrorDepthBufferToTexture(). [for DVR]
    if (gl.textureDepthBufferMirror)
    {
        gl.textureDepthBufferMirror->bindToTextureUnit(gl.textureUnitDepthBufferMirror);
        shader->setUniformValue("depthBufferMirror", gl.textureUnitDepthBufferMirror);
    }

    // 3) Set raycaster shader variables.

    // enhance performance when user is interacting with scene
    if (sceneView->userIsInteractingWithScene() || sceneView->userIsScrollingWithMouse())
    {
        shader->setUniformValue(
                    "stepSize", dataSamplingSettings->interactionStepSizeProp); CHECK_GL_ERROR;
        shader->setUniformValue(
                    "bisectionSteps", dataSamplingSettings->interactionBisectionStepsProp); CHECK_GL_ERROR;
    }
    else
    {
        shader->setUniformValue(
                    "stepSize", dataSamplingSettings->stepSizeProp); CHECK_GL_ERROR;
        shader->setUniformValue(
                    "bisectionSteps", dataSamplingSettings->bisectionStepsProp); CHECK_GL_ERROR;
    }

    shader->setUniformValueArray(
                "isoEnables", &rayCasterSettings->isoEnabled[0], MAX_ISOSURFACES); CHECK_GL_ERROR;
    shader->setUniformValueArray(
                "isoValues", &rayCasterSettings->isoValues[0], MAX_ISOSURFACES, 1); CHECK_GL_ERROR;
    shader->setUniformValueArray(
                "isoColors", &rayCasterSettings->isoColors[0], MAX_ISOSURFACES); CHECK_GL_ERROR;
    shader->setUniformValueArray(
                "isoColorModes", &rayCasterSettings->isoColorTypes[0], MAX_ISOSURFACES); CHECK_GL_ERROR;
    shader->setUniformValue(
                "numIsoValues", GLint(rayCasterSettings->isoValueSetList.size())); CHECK_GL_ERROR;

    shader->setUniformValue(
                "renderingMode", GLint(renderMode)); CHECK_GL_ERROR;

    shader->setUniformValue(
                "scalingFixMode", GLint(dvrSettings->scaleFix)); CHECK_GL_ERROR;

    GLint width = sceneView->getSceneResolutionWidth();
    GLint height = sceneView->getSceneResolutionHeight();
    shader->setUniformValue("viewPortWidth", GLint(width) ); CHECK_GL_ERROR;
    shader->setUniformValue("viewPortHeight", GLint(height) ); CHECK_GL_ERROR;

    if (gl.texUnitSceneShadow == -1)
    {
        gl.texUnitSceneShadow = assignTextureUnit();
    }
    sceneView->bindShadowMap(shader, gl.texUnitSceneShadow);
}


void MNWPVolumeRaycasterActor::setNormalCurveShaderVars(
        std::shared_ptr<GL::MShaderEffect>& shader, MSceneViewGLWidget* sceneView)
{
    shader->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix())); CHECK_GL_ERROR;
    shader->setUniformValue(
                "cameraPosition", sceneView->getCamera()->getOrigin()); CHECK_GL_ERROR;

    // Lighting direction from scene view.
    shader->setUniformValue(
                "lightDirection", sceneView->getLightDirection()); CHECK_GL_ERROR;

    if (varProp->transferFunction)
    {
        varProp->transferFunction->getTexture()->bindToTextureUnit(
                varProp->textureUnitTransferFunction);
        shader->setUniformValue(
                "transferFunction", varProp->textureUnitTransferFunction);

        shader->setUniformValue(
                "tfMinimum", varProp->transferFunction->getMinimumValue());
        shader->setUniformValue(
                "tfMaximum", varProp->transferFunction->getMaximumValue());
    }

    shader->setUniformValue(
                "normalized", GLboolean(
                    normalCurveSettings->colour != NormalCurveSettings::ColorIsoValue));

    shader->setUniformValue("tubeRadius", normalCurveSettings->tubeRadiusProp); CHECK_GL_ERROR;
}


void MNWPVolumeRaycasterActor::renderBoundingBox(
        MSceneViewGLWidget* sceneView)
{
    // We do not render the volume ray-caster bbox during shadow mapping.
    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING)
    {
        return;
    }
    setBoundingBoxShaderVars(sceneView);

    //glBindBuffer(GL_ARRAY_BUFFER, gl.vboBoundingBox); CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl.iboBoundingBox); CHECK_GL_ERROR;

    gl.vboBoundingBox->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
    sceneView->setLineWidth(1.0f / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

    glDrawElements(GL_LINE_STRIP, 16, GL_UNSIGNED_SHORT, 0);
}


void MNWPVolumeRaycasterActor::renderPositionCross(
        MSceneViewGLWidget *sceneView)
{
    // We do not render the volume ray-caster position cross during shadow mapping.
    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING)
    {
        return;
    }

    setBoundingBoxShaderVars(sceneView);

    gl.vboPositionCross->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
    sceneView->setLineWidth(2.0f / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

    glDrawArrays(GL_LINES, 0, 6); CHECK_GL_ERROR;
}


void MNWPVolumeRaycasterActor::renderRayCaster(
        std::shared_ptr<GL::MShaderEffect>& effect, MSceneViewGLWidget* sceneView)
{
    if (rayCasterSettings->isoValueSetList.empty()) return;

    QVector<GLboolean> activeVariables = getActiveVariables();

    // Don't render if all enabled variables are invisible.
    if (!activeVariables[0] && !activeVariables[1] && !activeVariables[2])
    {
        return;
    }

    effect->bindProgram("Volume");

    // Mirror current depth buffer for access in shader (cannot be accessed
    // directly) for correct ray termination in DVR.
    mirrorDepthBufferToTexture(sceneView);

    setRayCasterShaderVars(effect, sceneView); CHECK_GL_ERROR;

    //pEffect->printSubroutineInformation(GL_FRAGMENT_SHADER);

    switch(renderMode)
    {
    case RenderMode::Original:
    case RenderMode::DVR_VOL_LIGHT:
    case RenderMode::DVR:
        // set subroutine indices
        effect->setUniformSubroutineByName(
                    GL_FRAGMENT_SHADER,
                    gl.rayCasterSubroutines[varProp->grid->getLevelType()]);
        break;
    case RenderMode::Bitfield:
        effect->setUniformSubroutineByName(
                    GL_FRAGMENT_SHADER,
                    gl.bitfieldRayCasterSubroutines[varProp->grid->getLevelType()]);
        break;
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl.iboBoundingBox); CHECK_GL_ERROR;

    gl.vboBoundingBox->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR; // draw back faces..
    glCullFace(GL_FRONT); CHECK_GL_ERROR;            // ..and cull front faces
    glEnable(GL_CULL_FACE); CHECK_GL_ERROR;

    glPolygonOffset(.8f, 1.0f); CHECK_GL_ERROR;
    glEnable(GL_POLYGON_OFFSET_FILL); CHECK_GL_ERROR;

    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)(16 * sizeof(GLushort)));
    CHECK_GL_ERROR;
}

void MNWPVolumeRaycasterActor::renderVolumeLighting(
        MSceneViewGLWidget *sceneView, bool interactiveChange)
{
    if (!volumeLightingSettings->enabled) return;

    // Do not reset on interaction because we want this to be called
    // once more after interaction has ended, to upscale the light volume again.
    if (!interactiveChange)
    {
        updateNextRenderFrame.reset(RenderVolumeLighting);
    }
    else
    {
        updateNextRenderFrame.set(RenderVolumeLighting);
    }

#ifdef ENABLE_GPU_TIMING
    MGpuTimer timer;
    timer.start();
#endif

    const GLuint scaling = interactiveChange ? volumeLightingSettings->interactivityScaleFactorProp : 1;
    GLint resX = volumeLightingSettings->lightmapLonResProp / scaling;
    GLint resY = volumeLightingSettings->lightmapLatResProp / scaling;
    GLint resZ = volumeLightingSettings->lightmapZResProp / scaling;

    if (gl.volumeLightMap3DBuffer->getWidth() != resX || gl.volumeLightMap3DBuffer->getHeight() != resY || gl.volumeLightMap3DBuffer->getDepth() != resZ)
    {
        gl.volumeLightMap3DBuffer->updateSize(resX, resY, resZ);
        gl.volumeLightMap3DBuffer->bindToTextureUnit(gl.texUnitVolumeLightMap3D);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexImage3D(GL_TEXTURE_3D, 0, GL_RGBA8, resX, resY, resZ, 0,  GL_RGBA, GL_UNSIGNED_BYTE, nullptr); CHECK_GL_ERROR;
    }

    glBindImageTexture(0, gl.volumeLightMap3DBuffer->getTextureObject(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA8); CHECK_GL_ERROR;

    gl.rayCasterEffect->bindProgram("VolumeLighting");

    gl.rayCasterEffect->setUniformSubroutineByName(GL_COMPUTE_SHADER,
                                                   gl.rayCasterSubroutines[
                                                           varProp->grid->getLevelType()]);

    setCommonShaderVars(gl.rayCasterEffect, sceneView);

    glDispatchCompute(resX, resY, resZ); CHECK_GL_ERROR;
    glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT); CHECK_GL_ERROR;

#ifdef ENABLE_GPU_TIMING
    timer.stop();
    LOG4CPLUS_INFO(mlog, "Volume lightmap calculation time: " << timer.getElapsedTime(MStopwatch::MILLISECONDS));
#endif

    sceneView->makeCurrent();
}


void MNWPVolumeRaycasterActor::createIsoPositionBuffer(
        MSceneViewGLWidget* sceneView)
{
    updateNextRenderFrame.reset(UpdateShadowImage);

    MLightActor *sceneLight = sceneView->getSceneDirectionalLight();

    if (sceneLight == nullptr) return;

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    QVector3D bswc;
    QVector3D tnec;
    getRenderRegion(bswc, tnec, sceneView);

    // Get the max bbox extent, so that we dont accidentally cull the bbox from
    // rendering, resulting in incomplete information.
    MBoundingBox *bbox = getBBoxConnection()->getBoundingBox();
    float maxExtent = static_cast<float>(std::max(bbox->getEastWestExtent(),
                                                  bbox->getNorthSouthExtent()));

    QVector3D extent = (tnec - bswc) / 2.0;
    QVector3D center = bswc + extent;
    float radius = extent.length();

    maxExtent = std::max(maxExtent, radius * 2);


    QMatrix4x4 view;
    QMatrix4x4 proj;

    QVector3D pos = center - sceneLight->direction() * radius;
    view.lookAt(pos, center, sceneLight->up());

    proj.setToIdentity();
    proj.ortho(-radius, radius, -radius, radius, 0, maxExtent);

    gl.isoDirLightMvp[sceneLight] = proj * view;
    gl.isoDirLightPos[sceneLight] = pos;
    gl.isoDirLightDir[sceneLight] = sceneLight->direction();

    QVector3D bl = QVector3D(-1, -1, -1);
    QVector3D tr = QVector3D(1, 1, -1);
    QVector3D br = QVector3D(1, -1, -1);
    QVector3D tl = QVector3D(-1, 1, -1);

    bl = gl.isoDirLightMvp[sceneLight].inverted() * bl;
    tr = gl.isoDirLightMvp[sceneLight].inverted() * tr;
    br = gl.isoDirLightMvp[sceneLight].inverted() * br;
    tl = gl.isoDirLightMvp[sceneLight].inverted() * tl;

    float vertexDataShadowMap[] =
    {
            bl.x(), bl.y(), bl.z(),0.0f, 0.0f,

            tl.x(), tl.y(), tl.z(),0.0f, 1.0f,

            br.x(), br.y(), br.z(), 1.0f, 0.0f,

            tr.x(), tr.y(), tr.z(), 1.0f, 1.0f,
    };

    const GLint numVerticesShadowMap = 20;

    // Check buffer for shadow map bounding box
    if (gl.vboIsoPositionBuffer)
    {
        // Update buffer.
        auto* buf = dynamic_cast<GL::MFloatVertexBuffer*>(
                gl.vboIsoPositionBuffer);
        buf->update(vertexDataShadowMap, numVerticesShadowMap, 0,  0, sceneView);
    }
    else
    {
        // Create new buffer.
        const QString vboID = QString("vbo_iso_pos_buffer_raycaster_actor#%1").arg(myID);

        auto* newVB =
                new GL::MFloatVertexBuffer(vboID, numVerticesShadowMap);

        if (glRM->tryStoreGPUItem(newVB))
        {
            newVB->upload(vertexDataShadowMap, numVerticesShadowMap, sceneView);
            gl.vboIsoPositionBuffer = dynamic_cast<GL::MVertexBuffer*>(
                    glRM->getGPUItem(vboID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store buffer for iso surface position "
                                 " buffer bbox in GPU memory.");
            delete newVB;
            return;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;

    int resX = sceneView->getShadowMapResolution();
    int resY = resX;

    GLuint tempFBO = 0;
    glGenFramebuffers(1, &tempFBO); CHECK_GL_ERROR;
    glBindFramebuffer(GL_FRAMEBUFFER, tempFBO); CHECK_GL_ERROR;

    GLint oldResX = 0;
    GLint oldResY = 0;

    if (gl.tex2DIsoPositionBuffer.contains(sceneView))
    {
        gl.tex2DIsoPositionBuffer[sceneView]->bindToTextureUnit(0);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &oldResX);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &oldResY);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    // Create new framebuffer texture if none exists, or update its size if
    // the resolution has changed.
    if (!gl.tex2DIsoPositionBuffer.contains(sceneView)
            || oldResX != resX || oldResY != resY)
    {
        if (!gl.tex2DIsoPositionBuffer.contains(sceneView))
        {
            const QString isoPosBufferTextureID =
                    QString("iso_pos_buffer_image_2D_actor_#%1_scene_#%2").arg(myID).arg(sceneView->getID());

            gl.tex2DIsoPositionBuffer[sceneView] = new GL::MTexture(isoPosBufferTextureID,
                                                         GL_TEXTURE_2D, GL_RGBA32F,
                                                         resX, resY);

            if (glRM->tryStoreGPUItem(gl.tex2DIsoPositionBuffer[sceneView]))
            {
                gl.tex2DIsoPositionBuffer[sceneView] = dynamic_cast<GL::MTexture*>(
                            glRM->getGPUItem(isoPosBufferTextureID));

            }
            else
            {
                LOG4CPLUS_WARN(mlog, "Cannot store texture for iso surface position"
                               " buffer in GPU memory.");
                delete gl.tex2DIsoPositionBuffer[sceneView];
                gl.tex2DIsoPositionBuffer.remove(sceneView);
                return;
            }
        }
        else
        {
            gl.tex2DIsoPositionBuffer[sceneView]->updateSize(resX, resY);
        }

        gl.tex2DIsoPositionBuffer[sceneView]->bindToTextureUnit(0);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F,
                     resX, resY, 0, GL_RGBA,
                     GL_FLOAT, nullptr); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                               gl.tex2DIsoPositionBuffer[sceneView]->getTextureObject(), 0);
    }
    else
    {
        glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                               gl.tex2DIsoPositionBuffer[sceneView]->getTextureObject(), 0);
    }

    glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;

    unsigned int attachments[1] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, attachments); CHECK_GL_ERROR;

    // set viewport resolution
    glViewport(0,0,resX,resY); CHECK_GL_ERROR;

    // clear framebuffer
    glClearColor(0.0, 0.0, 0.0, 0.0); CHECK_GL_ERROR;
    glClear(GL_COLOR_BUFFER_BIT); CHECK_GL_ERROR;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gl.iboBoundingBox); CHECK_GL_ERROR;

    gl.vboBoundingBox->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    // select the mode, polygons have to be drawn. Here back faces and their surfaces are filled
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); CHECK_GL_ERROR;
    glCullFace(GL_FRONT); CHECK_GL_ERROR; // ..and cull front faces
    glEnable(GL_CULL_FACE); CHECK_GL_ERROR;

    // draw results to shadow image

    // set shader variables
    std::shared_ptr<GL::MShaderEffect> pEffect = nullptr;
    switch(renderMode)
    {
    case RenderMode::Original:
    case RenderMode::DVR_VOL_LIGHT:
    case RenderMode::DVR:
        pEffect = gl.rayCasterEffect;
        break;
    case RenderMode::Bitfield:
        pEffect = gl.bitfieldRayCasterEffect;
        break;
    }

    pEffect->bindProgram("Volume"); CHECK_GL_ERROR;
    glBindFramebuffer(GL_FRAMEBUFFER, tempFBO); CHECK_GL_ERROR;

    setRayCasterShaderVars(pEffect, sceneView); CHECK_GL_ERROR;
    pEffect->setUniformValue("shadowMode", true); CHECK_GL_ERROR;
    pEffect->setUniformValue("stepSize", dataSamplingSettings->stepSizeProp); CHECK_GL_ERROR;

    switch(renderMode)
    {
    case RenderMode::Original:
    case RenderMode::DVR_VOL_LIGHT:
    case RenderMode::DVR:
        // set subroutine indices
        pEffect->setUniformSubroutineByName(GL_FRAGMENT_SHADER,
                                            gl.rayCasterSubroutines[varProp->grid->getLevelType()]);
        break;
    case RenderMode::Bitfield:
        pEffect->setUniformSubroutineByName(GL_FRAGMENT_SHADER,
                                            gl.bitfieldRayCasterSubroutines[varProp->grid->getLevelType()]);
        break;
    }

    glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_SHORT, (void*)(16 * sizeof(GLushort))); CHECK_GL_ERROR;

    /*std::vector<float> pixelData(resX * resY * 4);

    glBindTexture(GL_TEXTURE_2D, gl.tex2DShadowImage->getTextureObject()); CHECK_GL_ERROR;
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_FLOAT, &pixelData[0]); CHECK_GL_ERROR;
    glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;*/

    /*QImage test_img((int)resX, (int)resY, QImage::Format_ARGB32_Premultiplied);

    for (int y = 0; y < resY; ++y)
    {
        for (int x = 0; x < resX; ++x)
        {
            test_img.setPixel(x,y, QColor(pixelData[(x + y * resX) * 4] * 255,
                                          pixelData[(x + y * resX) * 4 + 1] * 255,
                                          pixelData[(x + y * resX) * 4 + 2] * 255,
                                          pixelData[(x + y * resX) * 4 + 3] * 255).rgba());
        }
    }
    test_img.save("raycaster_topdown_screen.png");*/

    glBindFramebuffer(GL_FRAMEBUFFER, sceneView->getCurrentFBO()); CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, 0);  CHECK_GL_ERROR;

    // delete temporary fbo
    glDeleteFramebuffers(1, &tempFBO); CHECK_GL_ERROR;

    pEffect->setUniformValue("shadowMode", false); CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);  CHECK_GL_ERROR;
    glDisable(GL_CULL_FACE); CHECK_GL_ERROR;

    // Set the cascaded shadow update guard, to prevent it being redrawn for
    // each shadow cascade.
    gl.cascadedShadowUpdateGuard = true;
}


void MNWPVolumeRaycasterActor::createShadowImage(
        MSceneViewGLWidget* sceneView)
{
    updateNextRenderFrame.reset(UpdateShadowImage);

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    QVector3D bswc;
    QVector3D tnec;
    getRenderRegion(bswc, tnec, sceneView);

    float lonDist = tnec.x() - bswc.x();
    float latDist = tnec.y() - bswc.y();

    float zMax = sceneView->worldZfromPressure(getBBoxConnection()->topPressure_hPa());

    // create current vertex data (positions of bounding box lad)
    float vertexData[] =
    {
            -1,-1,
            float(bswc.x()), float(bswc.y()),
            zMax, -1 ,1,
            float(bswc.x()), float(tnec.y()),
            zMax, 1,-1,
            float(tnec.x()), float(bswc.y()),
            zMax, 1, 1,
            float(tnec.x()), float(tnec.y()),
            zMax,
    };

    const GLint numVertices = 20;

    if (gl.vboShadowImage)
    {
        auto* buf = dynamic_cast<GL::MFloatVertexBuffer*>(
                gl.vboShadowImage);
        buf->update(vertexData, numVertices, 0, 0, sceneView);
    }
    else
    {
        const QString vboID = QString("vbo_shadowimage_actor_#%1").arg(myID);

        auto* newVB =
                new GL::MFloatVertexBuffer(vboID, numVertices);

        if (glRM->tryStoreGPUItem(newVB))
        {
            newVB->upload(vertexData, numVertices, sceneView);
            gl.vboShadowImage = dynamic_cast<GL::MVertexBuffer*>(
                    glRM->getGPUItem(vboID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store buffer for shadow"
                                 " image bbox in GPU memory.");
            delete newVB;
            return;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);  CHECK_GL_ERROR;

    float vertexDataShadowMap[] =
    {
            float(bswc.x()), float(bswc.y()),
            0.0f, 0.0f, 0.0f,

            float(bswc.x()), float(tnec.y()),
            0.0f, 0.0f, 1.0f,

            float(tnec.x()), float(bswc.y()),
            0.0f, 1.0f, 0.0f,

            float(tnec.x()), float(tnec.y()),
            0.0f, 1.0f, 1.0f,
    };

    const GLint numVerticesShadowMap = 20;

    // Check buffer for shadow map bounding box
    if (gl.vboShadowImageRender)
    {
        // Update buffer.
        auto* buf = dynamic_cast<GL::MFloatVertexBuffer*>(
                gl.vboShadowImageRender);
        buf->update(vertexDataShadowMap, numVerticesShadowMap, 0,  0, sceneView);
    }
    else
    {
        // Create new buffer.
        const QString vboID = QString("vbo_shadowmap_bbox_actor#%1").arg(myID);

        auto* newVB =
                new GL::MFloatVertexBuffer(vboID, numVerticesShadowMap);

        if (glRM->tryStoreGPUItem(newVB))
        {
            newVB->upload(vertexDataShadowMap, numVerticesShadowMap, sceneView);
            gl.vboShadowImageRender = dynamic_cast<GL::MVertexBuffer*>(
                    glRM->getGPUItem(vboID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store buffer for shadow"
                                 " image bbox in GPU memory.");
            delete newVB;
            return;
        }
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;

    float ratio = lonDist / latDist;

    int resX = sceneView->getShadowMapResolution();
    int resY = static_cast<int>(std::ceil(resX / ratio));

    GLuint tempFBO = 0;
    glGenFramebuffers(1, &tempFBO);
    glBindFramebuffer(GL_FRAMEBUFFER, tempFBO);

    GLint oldResX = 0;
    GLint oldResY = 0;

    if (gl.tex2DShadowImage)
    {
        gl.tex2DShadowImage->bindToTextureUnit(gl.texUnitShadowImage);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_WIDTH, &oldResX);
        glGetTexLevelParameteriv(GL_TEXTURE_2D, 0, GL_TEXTURE_HEIGHT, &oldResY);
        glBindTexture(GL_TEXTURE_2D, 0);
    }

    // Create new framebuffer texture if none exists, or update its size if
    // the resolution has changed.
    if (!gl.tex2DShadowImage || oldResX != resX || oldResY != resY)
    {
        if (!gl.tex2DShadowImage)
        {
            const QString shadowImageTextureID =
                    QString("shadow_image_2D_actor_#%1").arg(myID);

            gl.tex2DShadowImage = new GL::MTexture(shadowImageTextureID,
                                                   GL_TEXTURE_2D, GL_RGBA32F,
                                                   resX, resY);

            if (glRM->tryStoreGPUItem(gl.tex2DShadowImage))
            {
                gl.tex2DShadowImage = dynamic_cast<GL::MTexture*>(
                        glRM->getGPUItem(shadowImageTextureID));

            }
            else
            {
                LOG4CPLUS_WARN(mlog, "Cannot store texture for shadow"
                                     " image in GPU memory.");
                delete gl.tex2DShadowImage;
                gl.tex2DShadowImage = nullptr;
                return;
            }
        }
        else
        {
            gl.tex2DShadowImage->updateSize(resX, resY);
        }

        gl.tex2DShadowImage->bindToTextureUnit(gl.texUnitShadowImage);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, resX, resY, 0, GL_RGBA,
                     GL_FLOAT, nullptr); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP); CHECK_GL_ERROR;
        glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                                gl.tex2DShadowImage->getTextureObject(), 0);
    }
    else
    {
        gl.tex2DShadowImage->bindToTextureUnit(gl.texUnitShadowImage);
        glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D,
                                gl.tex2DShadowImage->getTextureObject(), 0);
    }

    glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;

    // activate render to target 0
    GLenum DrawBuffers[] = { GL_COLOR_ATTACHMENT0 };
    glDrawBuffers(1, DrawBuffers);

    // set viewport resolution
    glViewport(0,0,resX,resY);

    // clear framebuffer
    glClearColor(0, 0, 0, 0);
    glClear(GL_COLOR_BUFFER_BIT);

    // bind current buffers
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    //glBindBuffer(GL_ARRAY_BUFFER, gl.vboShadowImage); CHECK_GL_ERROR;

    // bind vertex attributes
    gl.vboShadowImage->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE,
                                               2, false,
                                               5 * sizeof(float),
                                               (const GLvoid*)(0 * sizeof(float)));

    gl.vboShadowImage->attachToVertexAttribute(SHADER_BORDER_ATTRIBUTE,
                                               3, false,
                                               5 * sizeof(float),
                                               (const GLvoid*)(2 * sizeof(float)));

    // select the mode, polygons have to be drawn. Here back faces and their surfaces are filled
    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); CHECK_GL_ERROR;

    // draw results to shadow image

    // set shader variables
    std::shared_ptr<GL::MShaderEffect> pEffect = nullptr;
    switch(renderMode)
    {
    case RenderMode::Original:
    case RenderMode::DVR_VOL_LIGHT:
    case RenderMode::DVR:
        pEffect = gl.rayCasterEffect;
        break;
    case RenderMode::Bitfield:
        pEffect = gl.bitfieldRayCasterEffect;
        break;
    }

    pEffect->bindProgram("Shadow");

    glBindFramebuffer(GL_FRAMEBUFFER, tempFBO);

    // Unbind all textures from texture unit 0, as for some reason
    // it will not bind another texture to it during this render call.
    // This does not really make any sense, as be do re-bind slot 0
    // in "setCommonShaderVars", but in OpenGL it is not bound to the correct texture.
    // This also only happens here, no other call in the raycaster requires this.
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_1D, 0);
    glBindTexture(GL_TEXTURE_2D, 0);
    glBindTexture(GL_TEXTURE_3D, 0);

    setRayCasterShaderVars(pEffect, sceneView);
    pEffect->setUniformValue("shadowMode", true);
    pEffect->setUniformValue("stepSize", dataSamplingSettings->stepSizeProp);
    pEffect->setUniformValue("isOrthographic", true); CHECK_GL_ERROR;
    pEffect->setUniformValue("cameraDirection", QVector3D(0, 0, -1)); CHECK_GL_ERROR;

    switch(renderMode)
    {
    case RenderMode::Original:
    case RenderMode::DVR_VOL_LIGHT:
    case RenderMode::DVR:
        // set subroutine indices
        pEffect->setUniformSubroutineByName(GL_FRAGMENT_SHADER,
                                            gl.rayCasterSubroutines[
                                                    varProp->grid->getLevelType()]);
        break;
    case RenderMode::Bitfield:
        pEffect->setUniformSubroutineByName(GL_FRAGMENT_SHADER,
                                            gl.bitfieldRayCasterSubroutines[
                                                    varProp->grid->getLevelType()]);
        break;
    }

    glDrawArrays(GL_TRIANGLE_STRIP,0,4); CHECK_GL_ERROR;

    glBindFramebuffer(GL_FRAMEBUFFER, sceneView->getCurrentFBO());
    glViewport(0,0,sceneView->getSceneResolutionWidth(),sceneView->getSceneResolutionHeight());
    glBindBuffer(GL_ARRAY_BUFFER, 0);  CHECK_GL_ERROR;

    // delete temporary fbo
    glDeleteFramebuffers(1, &tempFBO);

    pEffect->setUniformValue("shadowMode", false);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);  CHECK_GL_ERROR;
}


void MNWPVolumeRaycasterActor::renderShadows(
        MSceneViewGLWidget* sceneView)
{
    // Don't render DVR shadow if observered variable doesn't have a transfer
    // function assigned.
    if ((renderMode == RenderMode::DVR)
            && ((varProp->transferFunction == nullptr
                    && cloudSettings->ciwcSlot != 0
                    && cloudSettings->clwcSlot != 0)
                    && ((dvrSettings->secVariableProp->transferFunction
                            == nullptr
                            && dvrSettings->secondVarEnabledProp &&
                            cloudSettings->ciwcSlot != 1
                            && cloudSettings->clwcSlot != 1)
                            || !dvrSettings->secondVarEnabledProp)
                    && ((dvrSettings->thirdVariableProp->transferFunction
                            == nullptr
                            && dvrSettings->thirdVarEnabledProp &&
                            cloudSettings->ciwcSlot != 2
                            && cloudSettings->clwcSlot != 2)
                            || !dvrSettings->thirdVarEnabledProp)))
    {
        return;
    }

    if (gl.tex2DShadowImage == nullptr) return;

    gl.shadowImageRenderShader->bindProgram("Shadow");

    gl.shadowImageRenderShader->setUniformValue("mvpMatrix",*(sceneView->getModelViewProjectionMatrix()));

    gl.tex2DShadowImage->bindToTextureUnit(gl.texUnitShadowImage);
    gl.shadowImageRenderShader->setUniformValue("texImage", GLint(gl.texUnitShadowImage));

    {
        gl.vboShadowImageRender->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE,
                                                         3, false,
                                                         5 * sizeof(float),
                                                         (const GLvoid*)(0 * sizeof(float)));

        gl.vboShadowImageRender->attachToVertexAttribute(SHADER_TEXCOORD_ATTRIBUTE,
                                                         2, false,
                                                         5 * sizeof(float),
                                                         (const GLvoid*)(3 * sizeof(float)));

        // save current blend func params
        GLint blendSrc;
        GLint blendDst;
        glGetIntegerv(GL_BLEND_SRC_ALPHA, &blendSrc);
        glGetIntegerv(GL_BLEND_DST_ALPHA, &blendDst);

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); CHECK_GL_ERROR;

        // Restore previous blend func params-
        glBlendFunc(blendSrc, blendDst);
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void MNWPVolumeRaycasterActor::renderIsoPositionsToShadow(
        MSceneViewGLWidget* sceneView)
{
    gl.shadowImageRenderShader->bindProgram("Standard");

    MLightActor *light = sceneView->getSceneDirectionalLight();
    if (light == nullptr) return;

    gl.shadowImageRenderShader->setUniformValue("mvpMatrix",*(sceneView->getModelViewProjectionMatrix()));

    gl.tex2DIsoPositionBuffer[sceneView]->bindToTextureUnit(1);
    gl.shadowImageRenderShader->setUniformValue("texImage", 1);

    gl.vboIsoPositionBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE,
                                                     3, false,
                                                     5 * sizeof(float),
                                                     (const GLvoid*)(0 * sizeof(float)));

    gl.vboIsoPositionBuffer->attachToVertexAttribute(SHADER_TEXCOORD_ATTRIBUTE,
                                                     2, false,
                                                     5 * sizeof(float),
                                                     (const GLvoid*)(3 * sizeof(float)));

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4); CHECK_GL_ERROR;

    glBindBuffer(GL_ARRAY_BUFFER, 0);
}


void MNWPVolumeRaycasterActor::computeNormalCurveInitialPoints(
        MSceneViewGLWidget* sceneView)
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    numNormalCurveInitPoints = 0;
    updateNextRenderFrame.reset(ComputeNCInitPoints);

    // Compute minimum and maximum z-values of the data.
    const float dataMinZ =
            sceneView->worldZfromPressure(getBBoxConnection()->bottomPressure_hPa());
    const float dataMaxZ =
            sceneView->worldZfromPressure(getBBoxConnection()->topPressure_hPa());

    // Determine seed points grid spacing.
    const float gridSpaceLon = normalCurveSettings->initPointResXProp;
    const float gridSpaceLat = normalCurveSettings->initPointResYProp;
    const float gridSpaceHeight = normalCurveSettings->initPointResZProp;

    // Compute data extent in lon, lat and height domain.
    const float dataExtentLon =
            std::abs(getBBoxConnection()->eastLon() - getBBoxConnection()->westLon());
    const float dataExtentLat =
            std::abs(getBBoxConnection()->northLat() - getBBoxConnection()->southLat());
    const float dataExtentHeight = std::abs(dataMaxZ - dataMinZ);

    // Compute the number of rays to be shot through the scene in X/Y/Z parallel
    // direction. Used for number of threads started on GPU (see below).
    const uint16_t numRaysLon = dataExtentLon / gridSpaceLon + 1;
    const uint16_t numRaysLat = dataExtentLat / gridSpaceLat + 1;
    const uint16_t numRaysHeight = dataExtentHeight / gridSpaceHeight + 1;

    const uint32_t numRays = numRaysLon * numRaysLat
                             + numRaysLon * numRaysHeight
                             + numRaysLat * numRaysHeight;

    // Make resource manager to the current context.
    glRM->makeCurrent();

    // Create a 3D texture storing the ghost grid over the domain (to avoid
    // multiple curves seeded close to each other).
    QString ghostTexID = QString("normalcurves_ghost_grid_#%1").arg(myID);

    GL::MTexture* ghostGridTex3D = nullptr;
    ghostGridTex3D = static_cast<GL::MTexture*>(glRM->getGPUItem(ghostTexID));

    if (ghostGridTex3D == nullptr)
    {
        ghostGridTex3D = new GL::MTexture(ghostTexID, GL_TEXTURE_3D, GL_R32UI,
                                          numRaysLon, numRaysLat, numRaysHeight);

        if (!glRM->tryStoreGPUItem(ghostGridTex3D))
        {
            LOG4CPLUS_WARN(mlog, "Cannot store texture for normal curves"
                           " ghost grid in GPU memory, skipping normal curves"
                           " computation.");
            delete ghostGridTex3D;
            return;
        }
    }
    else
    {
        ghostGridTex3D->updateSize(numRaysLon, numRaysLat, numRaysHeight);
    }

    // Initialise ghost grid with zeros.
    ghostGridTex3D->bindToLastTextureUnit(); CHECK_GL_ERROR;
    QVector<GLint> nullData(numRaysLon * numRaysLat * numRaysHeight, 0);

    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32I, numRaysLon, numRaysLat, numRaysHeight, 0,
                 GL_RED_INTEGER, GL_INT, nullData.data()); CHECK_GL_ERROR;

    const GLint ghostGridImageUnit = assignImageUnit();

    glBindTexture(GL_TEXTURE_3D, 0); CHECK_GL_ERROR;

    const GLuint MAX_ESTIMATE_CROSSINGS = 2;
    const int MAX_INITPOINTS = numRays * MAX_ESTIMATE_CROSSINGS;

    // Create a shader storage buffer containing all possible init points.
    QVector<QVector4D> initData(MAX_INITPOINTS, QVector4D(-1,-1,-1,-1));

    if (gl.ssboInitPoints == nullptr)
    {
        const QString ssboInitPointsID =
                QString("normalcurves_ssbo_init_points_#%1").arg(myID);
        gl.ssboInitPoints = new GL::MShaderStorageBufferObject(
                    ssboInitPointsID, sizeof(QVector4D), MAX_INITPOINTS);

        if (glRM->tryStoreGPUItem(gl.ssboInitPoints))
        {
            // Obtain reference to SSBO item.
            gl.ssboInitPoints = static_cast<GL::MShaderStorageBufferObject*>(
                        glRM->getGPUItem(ssboInitPointsID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store buffer for normal curves"
                           " init points in GPU memory, skipping normal curves"
                           " computation.");
            delete gl.ssboInitPoints;
            gl.ssboInitPoints = nullptr;
            return;
        }
    }
    else
    {
        gl.ssboInitPoints->updateSize(MAX_INITPOINTS);
    }

    gl.ssboInitPoints->upload(initData.data(), GL_DYNAMIC_COPY);

    // Create an atomic counter to control the writes to the SSBO.
    GLuint atomicCounter = 0;
    GLuint atomicBuffer = 0;
    glGenBuffers(1, &atomicBuffer); CHECK_GL_ERROR;
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomicBuffer); CHECK_GL_ERROR;
    glBufferData(GL_ATOMIC_COUNTER_BUFFER, sizeof(GLuint), &atomicCounter,
                 GL_DYNAMIC_DRAW); CHECK_GL_ERROR;
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, 0); CHECK_GL_ERROR;

    // Bind the compute shader and set required shader variables.
    gl.normalCurveInitPointsShader->bind();
    setCommonShaderVars(gl.normalCurveInitPointsShader, sceneView);

    gl.normalCurveInitPointsShader->setUniformSubroutineByName(
                GL_COMPUTE_SHADER,
                gl.normalInitSubroutines[varProp->grid->getLevelType()]);

    // Bind the atomic counter to the binding index 0.
    glBindBufferBase(GL_ATOMIC_COUNTER_BUFFER, 0, atomicBuffer);
    // Bind the SSBO to the binding index 0.
    gl.ssboInitPoints->bindToIndex(0);

    gl.normalCurveInitPointsShader->setUniformValue(
                    "isoValue", normalCurveSettings->startIsoValue);

    gl.normalCurveInitPointsShader->setUniformValue(
                "stepSize", dataSamplingSettings->stepSizeProp);
    gl.normalCurveInitPointsShader->setUniformValue(
                "bisectionSteps", dataSamplingSettings->bisectionStepsProp);

    const QVector3D initWorldPos(getBBoxConnection()->westLon(),
                                 getBBoxConnection()->southLat(), dataMinZ);
    gl.normalCurveInitPointsShader->setUniformValue(
                "initWorldPos", initWorldPos);
    gl.normalCurveInitPointsShader->setUniformValue(
                "bboxMin", QVector3D(getBBoxConnection()->westLon(),
                             getBBoxConnection()->southLat(),
                                     dataMinZ));
    gl.normalCurveInitPointsShader->setUniformValue(
                "bboxMax", QVector3D(getBBoxConnection()->eastLon(),
                             getBBoxConnection()->northLat(),
                                     dataMaxZ));

    // Set direction specific shader vars.

    // Different ray-casting directions.
    const QVector3D castDirection[] = {
        QVector3D(0, 0, 1), QVector3D(0, 1, 0), QVector3D(1, 0, 0) };

    // Maximum length of each ray.
    float maxRayLength[] = { dataExtentHeight, dataExtentLat, dataExtentLon };

    const QVector3D deltaGridX[] = { QVector3D(gridSpaceLon,0,0),
                                     QVector3D(gridSpaceLon,0,0),
                                     QVector3D(0,gridSpaceLat,0) };

    const QVector3D deltaGridY[] = { QVector3D(0,gridSpaceLat,0),
                                     QVector3D(0,0,gridSpaceHeight),
                                     QVector3D(0,0,gridSpaceHeight) };

    const GLuint dispatchXLonLat = numRaysLon / 64 + 1;
    const GLuint dispatchXLatHeight = numRaysLat / 64 + 1;
    const GLuint dispatchYLonLat = numRaysLat / 2 + 1;
    const GLuint dispatchYLonHeight = numRaysHeight / 2 + 1;

    const QPoint dispatches[] = { QPoint(dispatchXLonLat, dispatchYLonLat),
                                  QPoint(dispatchXLonLat, dispatchYLonHeight),
                                  QPoint(dispatchXLatHeight, dispatchYLonHeight) };

    const QPoint maxNumRays[] = { QPoint(numRaysLon, numRaysLat),
                                  QPoint(numRaysLon, numRaysHeight),
                                  QPoint(numRaysLat, numRaysHeight) };

    // Maximum extent of all 3D dimensions.
    const GLuint maxRes = std::max(numRaysLon, std::max(numRaysLat, numRaysHeight));

    // Create a texture to distort the start position of the rays.
    const QString distortTexID =
            QString("normalcurves_displacement_texture_#%1").arg(myID);
    GL::MTexture* distortTex2D = static_cast<GL::MTexture*>(
                glRM->getGPUItem(distortTexID));

    if (distortTex2D == nullptr)
    {
        distortTex2D = new GL::MTexture(distortTexID, GL_TEXTURE_2D, GL_RG32F,
                                        maxRes, maxRes);

        if (!glRM->tryStoreGPUItem(distortTex2D))
        {
            LOG4CPLUS_WARN(mlog, "Cannot store texture for normal curves"
                           " displacement grid in GPU memory, skipping normal curves"
                           " computation.");
            delete distortTex2D;
            return;
        }
    }
    else
    {
        distortTex2D->updateSize(maxRes, maxRes);
    }

    std::default_random_engine engine;
    std::uniform_real_distribution<float> distribution(
                    -normalCurveSettings->initPointVarianceProp,
                     normalCurveSettings->initPointVarianceProp);

    // Compute random distortion values.
    QVector<float> texels(maxRes * maxRes * 2);
    for (int i = 0; i < texels.size(); ++i) { texels[i] = distribution(engine); }

    // Set data of distort texture.
    const GLuint distortTexUnit = assignTextureUnit();

    distortTex2D->bindToTextureUnit(distortTexUnit);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RG32F, maxRes, maxRes, 0, GL_RG, GL_FLOAT,
                 texels.data());
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); CHECK_GL_ERROR;
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST); CHECK_GL_ERROR;
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
    glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
    glBindTexture(GL_TEXTURE_2D, 0);

    // Bind to texture unit.
    distortTex2D->bindToTextureUnit(distortTexUnit);
    gl.normalCurveInitPointsShader->setUniformValue("distortTex", distortTexUnit);
    gl.normalCurveInitPointsShader->setUniformValue("doubleIntegration",
                    (normalCurveSettings->integrationDir == NormalCurveSettings::Both));

    // Bind ghost grid as image3D to the shader.
    gl.normalCurveInitPointsShader->setUniformValue(
                "ghostGrid", ghostGridImageUnit); CHECK_GL_ERROR;

    glBindImageTexture(ghostGridImageUnit, // image unit
                       ghostGridTex3D->getTextureObject(), // texture object
                       0,                        // level
                       GL_TRUE,                  // layered
                       0,                        // layer
                       GL_READ_WRITE,            // shader access
                       GL_R32I); CHECK_GL_ERROR; // format

    // For each plane cast rays along a regular grid and search for intersection points
    // We compute the intersection points on GPU using Compute Shader (we do not need the rasterizer here).
    for (int i = 0; i < 3; ++i)
    {
        gl.normalCurveInitPointsShader->setUniformValue(
                    "castingDirection", castDirection[i]); CHECK_GL_ERROR;
        gl.normalCurveInitPointsShader->setUniformValue(
                    "maxRayLength", GLfloat(maxRayLength[i])); CHECK_GL_ERROR;

        gl.normalCurveInitPointsShader->setUniformValue(
                    "deltaGridX", deltaGridX[i]); CHECK_GL_ERROR;
        gl.normalCurveInitPointsShader->setUniformValue(
                    "deltaGridY", deltaGridY[i]); CHECK_GL_ERROR;

        gl.normalCurveInitPointsShader->setUniformValue(
                    "maxNumRays", maxNumRays[i]); CHECK_GL_ERROR;

        glDispatchCompute(GLint(dispatches[i].x()), GLint(dispatches[i].y()), 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
    }

    // DEBUGGING
//    glBindBuffer(GL_SHADER_STORAGE_BUFFER, gl.ssboNormalCurves);
//    QVector<NormalCurveLineSegment> lines(200);//numRays * (normalCurveSettings->numLineSegments + 2));

//    GLint bufMask = GL_MAP_READ_BIT;
//    NormalCurveLineSegment* vertices = (NormalCurveLineSegment*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0,
//                numRays * (normalCurveSettings->numLineSegments + 2) * sizeof(NormalCurveLineSegment), bufMask); CHECK_GL_ERROR;

//    for (int i = 0; i < 200; ++i)
//    {
//        lines[i] = vertices[i];
//    }

//    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    // Obtain the number of detected init points from the atomic counter.
    GLuint counter = 0;
    glBindBuffer(GL_ATOMIC_COUNTER_BUFFER, atomicBuffer);
    glGetBufferSubData(GL_ATOMIC_COUNTER_BUFFER, 0, sizeof(GLuint), &counter);
    //std::cout << "numInitPoints: " << counter << std::endl << std::flush;

    numNormalCurveInitPoints = counter;

    // Release temporary resources and texture/image units.
    glDeleteBuffers(1, &atomicBuffer);

    glRM->releaseGPUItem(distortTexID);
    releaseTextureUnit(distortTexUnit);

    glRM->releaseGPUItem(ghostTexID);
    releaseImageUnit(ghostGridImageUnit);

    // Set sceneView to the current context again.
    sceneView->makeCurrent();
}


void MNWPVolumeRaycasterActor::setNormalCurveComputeShaderVars(
        std::shared_ptr<GL::MShaderEffect>& shader, MSceneViewGLWidget* sceneView)
{
    setCommonShaderVars(shader, sceneView);

    // Set subroutine indices.
    //pEffect->printSubroutineInformation(GL_COMPUTE_SHADER);

    shader->setUniformSubroutineByName(
                GL_COMPUTE_SHADER,
                gl.normalCompSubroutines[varProp->grid->getLevelType()]);


    shader->setUniformValue(
                "integrationStepSize", normalCurveSettings->stepSizeProp); CHECK_GL_ERROR;
    shader->setUniformValue(
                "maxNumLines", GLint(numNormalCurveInitPoints)); CHECK_GL_ERROR;
    shader->setUniformValue(
                "maxNumLineSegments", GLint(normalCurveSettings->numLineSegmentsProp)); CHECK_GL_ERROR;
    shader->setUniformValue(
                "bisectionSteps", GLint(5)); CHECK_GL_ERROR;

    shader->setUniformValue(
                "isoValueStop", normalCurveSettings->stopIsoValue); CHECK_GL_ERROR;

    shader->setUniformValue(
                "colorMode", int(normalCurveSettings->colour)); CHECK_GL_ERROR;
    shader->setUniformValue(
                "abortCriterion", int(normalCurveSettings->threshold)); CHECK_GL_ERROR;
}


void MNWPVolumeRaycasterActor::computeNormalCurves(MSceneViewGLWidget* sceneView)
{
    if (rayCasterSettings->isoValueSetList.size() == 0) return;

    updateNextRenderFrame.reset(RecomputeNCLines);

    if (updateNextRenderFrame[ComputeNCInitPoints])
    {
        computeNormalCurveInitialPoints(sceneView);
    }

    if (numNormalCurveInitPoints == 0)
    {
        LOG4CPLUS_ERROR(mlog, "Could not find any normal curve init "
                        "points");
        return;
    }

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    glRM->makeCurrent();

    normalCurveNumVertices = (normalCurveSettings->numLineSegmentsProp + 2)
                             * numNormalCurveInitPoints;

    // Create the normal curve line buffer for every init point.
    if (gl.ssboNormalCurves == nullptr)
    {
        const QString ssboNCCurvesID =
                QString("normalcurves_ssbo_lines_#%1").arg(myID);

        gl.ssboNormalCurves = new GL::MShaderStorageBufferObject(
                    ssboNCCurvesID, sizeof(NormalCurveLineSegment),
                    normalCurveNumVertices);

        if (glRM->tryStoreGPUItem(gl.ssboNormalCurves))
        {
            gl.ssboNormalCurves = static_cast<GL::MShaderStorageBufferObject*>(
                        glRM->getGPUItem(ssboNCCurvesID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store buffer for normal curves"
                           " in GPU memory, skipping normal curves computation.");
            delete gl.ssboNormalCurves;
            gl.ssboNormalCurves = nullptr;
            return;
        }
    }
    else
    {
        gl.ssboNormalCurves->updateSize(normalCurveNumVertices);
    }

    QVector<QVector4D> initData(normalCurveNumVertices, QVector4D(-1,-1,-1,-1));

    gl.ssboNormalCurves->upload(initData.data(), GL_DYNAMIC_COPY);

    // Bind compute shader and shader storage buffer object and compute lines.
    if (normalCurveSettings->integrationDir == NormalCurveSettings::Both)
    {
        gl.normalCurveLineComputeShader->bindProgram("DoubleIntegration");
    } else {
        gl.normalCurveLineComputeShader->bindProgram("SingleIntegration");
    }

    setNormalCurveComputeShaderVars(gl.normalCurveLineComputeShader, sceneView);

    // Bind the ssbo's to the corresponding binding indices.
    gl.ssboInitPoints->bindToIndex(0);
    gl.ssboNormalCurves->bindToIndex(1);

    int dispatchX = numNormalCurveInitPoints / 128 + 1;

    switch(normalCurveSettings->integrationDir)
    {
    case NormalCurveSettings::IntegrationDir::Backwards:
        gl.normalCurveLineComputeShader->setUniformValue("integrationMode", int(-1)); CHECK_GL_ERROR;
        glDispatchCompute(dispatchX, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        break;

    case NormalCurveSettings::IntegrationDir::Forwards:
        gl.normalCurveLineComputeShader->setUniformValue("integrationMode", int(1)); CHECK_GL_ERROR;
        glDispatchCompute(dispatchX, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        break;

    case NormalCurveSettings::Both:
    default:
        glDispatchCompute(dispatchX / 2 + 1, 1, 1);
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        break;

    }

    // DEBUG
//    glBindBuffer(GL_SHADER_STORAGE_BUFFER, gl.ssboNormalCurves); CHECK_GL_ERROR;

//    QVector<NormalCurveLineSegment> lines(200);

//    GLint bufMask = GL_MAP_READ_BIT;
//    NormalCurveLineSegment* vertices = (NormalCurveLineSegment*) glMapBufferRange(GL_SHADER_STORAGE_BUFFER, 0,
//                  normalCurveNumVertices * sizeof(NormalCurveLineSegment), bufMask); CHECK_GL_ERROR;

//    for (GLuint i = 0; i < 200; ++i)
//    {
//        lines[i] = vertices[i];
//    }

//    glBindBuffer(GL_SHADER_STORAGE_BUFFER, 0);

    // Set sceneView to the current OpenGL context, again.
    sceneView->makeCurrent();

}


void MNWPVolumeRaycasterActor::renderNormalCurves(
        MSceneViewGLWidget* sceneView)
{
    if (normalCurveNumVertices                    == 0 ||
        rayCasterSettings->isoValueSetList.size() == 0)
    {
        return;
    }

    switch(normalCurveSettings->glyph)
    {
    case NormalCurveSettings::Line:
        gl.normalCurveGeometryEffect->bindProgram("Line");
        break;
    case NormalCurveSettings::Box:
        gl.normalCurveGeometryEffect->bindProgram("Box");
        break;
    case NormalCurveSettings::Tube:
        gl.normalCurveGeometryEffect->bindProgram("Tube");
        break;
    }

    setNormalCurveShaderVars(gl.normalCurveGeometryEffect, sceneView); CHECK_GL_ERROR;

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, gl.ssboNormalCurves->getBufferObject()); CHECK_GL_ERROR;

    glVertexAttribPointer(
                SHADER_VERTEX_ATTRIBUTE,
                3, GL_FLOAT,
                GL_FALSE,
                4 * sizeof(float),
                (const GLvoid *)0);

    glVertexAttribPointer(
                SHADER_VALUE_ATTRIBUTE,
                1, GL_FLOAT,
                GL_FALSE,
                4 * sizeof(float),
                (const GLvoid *)(3 * sizeof(float)));

    glEnableVertexAttribArray(SHADER_VERTEX_ATTRIBUTE);
    glEnableVertexAttribArray(SHADER_VALUE_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK,GL_FILL); CHECK_GL_ERROR;
    glDrawArrays(GL_LINE_STRIP_ADJACENCY, 0, normalCurveNumVertices); CHECK_GL_ERROR;

    glBindBuffer(GL_ARRAY_BUFFER, 0);  CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    glDisable(GL_CULL_FACE); CHECK_GL_ERROR;
    glDisable(GL_POLYGON_OFFSET_FILL); CHECK_GL_ERROR;
}


void MNWPVolumeRaycasterActor::mirrorDepthBufferToTexture(
        MSceneViewGLWidget *sceneView)
{
    // Get extent of current viewport.
    const GLint width = sceneView->getSceneResolutionWidth();
    const GLint height = sceneView->getSceneResolutionHeight();

    if (!gl.textureDepthBufferMirror)
    {
        // First call: no framebuffer object and no mirror texture exists.

        // Generate a texture into which the depth buffer is mirrored.
        const QString depthBufferID = QString("textureDepthBuffer_#%1").arg(myID);
        gl.textureDepthBufferMirror = new GL::MTexture(
                    depthBufferID, GL_TEXTURE_2D_MULTISAMPLE, GL_DEPTH_COMPONENT32,
                    width, height);

        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        if (glRM->tryStoreGPUItem(gl.textureDepthBufferMirror))
        {
            gl.textureDepthBufferMirror = dynamic_cast<GL::MTexture *>(
                        glRM->getGPUItem(depthBufferID));
        }
        else
        {
            delete gl.textureDepthBufferMirror;
            gl.textureDepthBufferMirror = nullptr;
        }
    }

    if (!gl.fboDepthBuffer.contains(sceneView))
    {
        // If not yet existing, generate a framebuffer object for this scene's
        // GL context to which the depth buffer can be bound.
        // NOTE: Do we really need a separate FBO for each scene view (i.e.
        // for each OpenGL context)?
        // See: https://www.khronos.org/opengl/wiki/Framebuffer_Object
        glGenFramebuffers(1, &gl.fboDepthBuffer[sceneView]);
        CHECK_GL_ERROR;
    }

    if (gl.textureDepthBufferMirror)
    {
        // Depth buffer mirror already exists, delete its contents.
        gl.textureDepthBufferMirror->bindToLastTextureUnit();
        glTexImage2DMultisample(GL_TEXTURE_2D_MULTISAMPLE, sceneView->getSampleCount(), GL_DEPTH_COMPONENT32, width, height, false);
        CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D_MULTISAMPLE, 0);
    }

    // Attach the "mirror" texture to the depth buffer and copy its content,
    // leaving all data on the GPU (we don't need the depth data on the CPU).
    glBindFramebuffer(GL_FRAMEBUFFER, gl.fboDepthBuffer[sceneView]);
    CHECK_GL_ERROR;
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D_MULTISAMPLE,
                           gl.textureDepthBufferMirror->getTextureObject(), 0);
    CHECK_GL_ERROR;
    glBlitNamedFramebuffer(sceneView->getCurrentFBO(), gl.fboDepthBuffer[sceneView], 0, 0, width, height, 0, 0,
                           width, height, GL_DEPTH_BUFFER_BIT, GL_NEAREST);
    CHECK_GL_ERROR;
    glBindFramebuffer(GL_FRAMEBUFFER, sceneView->getCurrentFBO()); CHECK_GL_ERROR;
}


bool MNWPVolumeRaycasterActor::areVariableDataExtentsAligned(MSceneViewGLWidget* sceneView) const
{
    QVector<GLboolean> enabledVariableSlots = getActiveVariables();

    // Trivial cases - none or exactly one variable exists.
    if (! enabledVariableSlots[0] && ! enabledVariableSlots[1]
        && ! enabledVariableSlots[2])
    {
        return false;
    }
    if (((enabledVariableSlots[0] != enabledVariableSlots[1])
         != enabledVariableSlots[2])
        && ! (enabledVariableSlots[0] && enabledVariableSlots[1]
              && enabledVariableSlots[2]))
    {
        return true;
    }

    bool differentGrids = false;
    QVector3D dataSECrnr[3];
    QVector3D dataNWCrnr[3];

    if (varProp->gridType == MNWPActorVariable::RADAR_GRID)
    {
        dataNWCrnr[0] = varProp->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataSECrnr[0] = varProp->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();

        dataNWCrnr[1] = dvrSettings->secVariableProp->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataSECrnr[1] = dvrSettings->secVariableProp->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();

        dataNWCrnr[2] = dvrSettings->thirdVariableProp->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataSECrnr[2] = dvrSettings->thirdVariableProp->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();
    }
    else // Structured grid
    {
        dataNWCrnr[0] = varProp->grid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataSECrnr[0] = varProp->grid->getSouthEastBottomDataVolumeCorner_lonlatp();

        dataNWCrnr[1] = dvrSettings->secVariableProp->grid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataSECrnr[1] = dvrSettings->secVariableProp->grid->getSouthEastBottomDataVolumeCorner_lonlatp();

        dataNWCrnr[2] = dvrSettings->thirdVariableProp->grid->getNorthWestTopDataVolumeCorner_lonlatp();
        dataSECrnr[2] = dvrSettings->thirdVariableProp->grid->getSouthEastBottomDataVolumeCorner_lonlatp();
    }
    dataNWCrnr[0].setZ(sceneView->worldZfromPressure(dataNWCrnr[0].z()));
    dataSECrnr[0] .setZ(sceneView->worldZfromPressure(dataSECrnr[0].z()));
    dataNWCrnr[1].setZ(sceneView->worldZfromPressure(dataNWCrnr[1].z()));
    dataSECrnr[1] .setZ(sceneView->worldZfromPressure(dataSECrnr[1].z()));
    dataNWCrnr[2].setZ(sceneView->worldZfromPressure(dataNWCrnr[2].z()));
    dataSECrnr[2] .setZ(sceneView->worldZfromPressure(dataSECrnr[2].z()));

    if (enabledVariableSlots[0])
    {
        if (enabledVariableSlots[1])
        {
            // Special case: If the grid is cyclic in longitude, shift the eastern
            // longitude one grid spacing east (e.g. make 359. 360.).
            float dataSECrnr_x = dataSECrnr[0].x();
            if (varProp->gridType == MNWPActorVariable::STRUCTURED_GRID
                && varProp->grid->gridIsCyclicInLongitude())
            {
                dataSECrnr_x += varProp->grid->getDeltaLon();
            }

            float dataSECrnrSec_x = dataSECrnr[1].x();
            if (dvrSettings->secVariableProp->gridType == MNWPActorVariable::STRUCTURED_GRID
                && dvrSettings->secVariableProp->grid->gridIsCyclicInLongitude())
            {
                dataSECrnrSec_x += dvrSettings->secVariableProp->grid->getDeltaLon();
            }

            if (dataNWCrnr[0].x() != dataNWCrnr[1].x())
            {
                differentGrids = true;
            }
            else if (dataSECrnr_x != dataSECrnrSec_x)
            {
                differentGrids = true;
            }
        }
        if (enabledVariableSlots[2])
        {
            // Special case: If the grid is cyclic in longitude, shift the eastern
            // longitude one grid spacing east (e.g. make 359. 360.).
            float dataSECrnr_x = dataSECrnr[0].x();
            if (varProp->gridType == MNWPActorVariable::STRUCTURED_GRID
                && varProp->grid->gridIsCyclicInLongitude())
            {
                dataSECrnr_x += varProp->grid->getDeltaLon();
            }

            float dataSECrnrThird_x = dataSECrnr[2].x();
            if (dvrSettings->thirdVariableProp->gridType == MNWPActorVariable::STRUCTURED_GRID
                && dvrSettings->thirdVariableProp->grid->gridIsCyclicInLongitude())
            {
                dataSECrnrThird_x += dvrSettings->thirdVariableProp->grid->getDeltaLon();
            }

            if (dataNWCrnr[0].x() != dataNWCrnr[2].x())
            {
                differentGrids = true;
            }
            else if (dataSECrnr_x != dataSECrnrThird_x)
            {
                differentGrids = true;
            }
        }
    }
    else
    {
        if (enabledVariableSlots[1] && enabledVariableSlots[2])
        {
            // Special case: If the grid is cyclic in longitude, shift the eastern
            // longitude one grid spacing east (e.g. make 359. 360.).
            float dataSECrnrSec_x = dataSECrnr[1].x();
            if (dvrSettings->secVariableProp->gridType == MNWPActorVariable::STRUCTURED_GRID
                && dvrSettings->secVariableProp->grid->gridIsCyclicInLongitude()) dataSECrnrSec_x += dvrSettings->secVariableProp->grid->getDeltaLon();

            float dataSECrnrThird_x = dataSECrnr[2].x();
            if (dvrSettings->thirdVariableProp->gridType == MNWPActorVariable::STRUCTURED_GRID
                && dvrSettings->thirdVariableProp->grid->gridIsCyclicInLongitude()) dataSECrnrThird_x += dvrSettings->thirdVariableProp->grid->getDeltaLon();

            if (dataNWCrnr[1].x() != dataNWCrnr[2].x())
            {
                differentGrids = true;
            }
            else if (dataSECrnrSec_x != dataSECrnrThird_x)
            {
                differentGrids = true;
            }
        }
    }

    return !differentGrids;
}


void MNWPVolumeRaycasterActor::getRenderRegion(QVector3D &renderRegionBottomSWCrnr, QVector3D &renderRegionTopNECrnr,
                                               MSceneViewGLWidget *sceneView) const
{
    using std::min;
    using std::max;

    QVector3D volumeTopNECrnr(
        getBBoxConnection()->eastLon(), getBBoxConnection()->northLat(),
            sceneView->worldZfromPressure(getBBoxConnection()->topPressure_hPa()));
    QVector3D volumeBottomSWCrnr(
        getBBoxConnection()->westLon(), getBBoxConnection()->southLat(),
            sceneView->worldZfromPressure(getBBoxConnection()->bottomPressure_hPa()));

    renderRegionBottomSWCrnr = volumeBottomSWCrnr;
    renderRegionTopNECrnr = volumeTopNECrnr;

    QVector<GLboolean> activeVariableSlots = getActiveVariables();

    QVector3D allVarBottomSWCrnr = renderRegionTopNECrnr;
    QVector3D allVarTopNECrnr = renderRegionBottomSWCrnr;
    allVarBottomSWCrnr.setZ(renderRegionBottomSWCrnr.z());
    allVarTopNECrnr.setZ(renderRegionTopNECrnr.z());

    QVector3D dataNWTCrnr = QVector3D(0, 0, 0);
    QVector3D dataSEBCrnr = QVector3D(0, 0, 0);
    if (activeVariableSlots[0])
    {
        if (varProp->gridType == MNWPActorVariable::STRUCTURED_GRID)
        {
            dataNWTCrnr = varProp->grid->getNorthWestTopDataVolumeCorner_lonlatp();
            dataSEBCrnr = varProp->grid->getSouthEastBottomDataVolumeCorner_lonlatp();
        }
        else if (varProp->gridType == MNWPActorVariable::RADAR_GRID)
        {
            dataNWTCrnr = varProp->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
            dataSEBCrnr = varProp->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();
        }

        dataNWTCrnr.setZ(sceneView->worldZfromPressure(dataNWTCrnr.z()));
        dataSEBCrnr.setZ(sceneView->worldZfromPressure(dataSEBCrnr.z()));

        allVarBottomSWCrnr.setX(min(dataNWTCrnr.x(), allVarBottomSWCrnr.x())); // lon
        allVarBottomSWCrnr.setY(min(dataSEBCrnr.y(), allVarBottomSWCrnr.y())); // lat
        allVarBottomSWCrnr.setZ(max(dataSEBCrnr.z(), allVarBottomSWCrnr.z())); // height

        allVarTopNECrnr.setX(max(dataSEBCrnr.x(), allVarTopNECrnr.x())); // lon
        allVarTopNECrnr.setY(max(dataNWTCrnr.y(), allVarTopNECrnr.y())); // lat
        allVarTopNECrnr.setZ(min(dataNWTCrnr.z(), allVarTopNECrnr.z())); // height
    }

    if (activeVariableSlots[1] && renderMode == RenderMode::DVR)
    {
        if (dvrSettings->secVariableProp->gridType == MNWPActorVariable::STRUCTURED_GRID)
        {
            dataNWTCrnr = dvrSettings->secVariableProp->grid->getNorthWestTopDataVolumeCorner_lonlatp();
            dataSEBCrnr = dvrSettings->secVariableProp->grid->getSouthEastBottomDataVolumeCorner_lonlatp();
        }
        else if (dvrSettings->secVariableProp->gridType == MNWPActorVariable::RADAR_GRID)
        {
            dataNWTCrnr = dvrSettings->secVariableProp->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
            dataSEBCrnr = dvrSettings->secVariableProp->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();
        }

        dataNWTCrnr.setZ(sceneView->worldZfromPressure(dataNWTCrnr.z()));
        dataSEBCrnr.setZ(sceneView->worldZfromPressure(dataSEBCrnr.z()));

        allVarBottomSWCrnr.setX(min(dataNWTCrnr.x(), allVarBottomSWCrnr.x())); // lon
        allVarBottomSWCrnr.setY(min(dataSEBCrnr.y(), allVarBottomSWCrnr.y())); // lat
        allVarBottomSWCrnr.setZ(max(dataSEBCrnr.z(), allVarBottomSWCrnr.z())); // height

        allVarTopNECrnr.setX(max(dataSEBCrnr.x(), allVarTopNECrnr.x())); // lon
        allVarTopNECrnr.setY(max(dataNWTCrnr.y(), allVarTopNECrnr.y())); // lat
        allVarTopNECrnr.setZ(min(dataNWTCrnr.z(), allVarTopNECrnr.z())); // height
    }

    if (activeVariableSlots[2] && renderMode == RenderMode::DVR)
    {
        if (dvrSettings->secVariableProp->gridType == MNWPActorVariable::STRUCTURED_GRID)
        {
            dataNWTCrnr = dvrSettings->thirdVariableProp->grid->getNorthWestTopDataVolumeCorner_lonlatp();
            dataSEBCrnr = dvrSettings->thirdVariableProp->grid->getSouthEastBottomDataVolumeCorner_lonlatp();
        }
        else if (dvrSettings->secVariableProp->gridType == MNWPActorVariable::RADAR_GRID)
        {
            dataNWTCrnr = dvrSettings->thirdVariableProp->radarGrid->getNorthWestTopDataVolumeCorner_lonlatp();
            dataSEBCrnr = dvrSettings->thirdVariableProp->radarGrid->getSouthEastBottomDataVolumeCorner_lonlatp();
        }
        dataNWTCrnr.setZ(sceneView->worldZfromPressure(dataNWTCrnr.z()));
        dataSEBCrnr.setZ(sceneView->worldZfromPressure(dataSEBCrnr.z()));

        allVarBottomSWCrnr.setX(min(dataNWTCrnr.x(), allVarBottomSWCrnr.x())); // lon
        allVarBottomSWCrnr.setY(min(dataSEBCrnr.y(), allVarBottomSWCrnr.y())); // lat
        allVarBottomSWCrnr.setZ(max(dataSEBCrnr.z(), allVarBottomSWCrnr.z())); // height

        allVarTopNECrnr.setX(max(dataSEBCrnr.x(), allVarTopNECrnr.x())); // lon
        allVarTopNECrnr.setY(max(dataNWTCrnr.y(), allVarTopNECrnr.y())); // lat
        allVarTopNECrnr.setZ(min(dataNWTCrnr.z(), allVarTopNECrnr.z())); // height
    }

    if (activeVariableSlots[0] || (renderMode == RenderMode::DVR
        && (activeVariableSlots[1] || activeVariableSlots[2])))
    {
        renderRegionBottomSWCrnr.setX(max(renderRegionBottomSWCrnr.x(), allVarBottomSWCrnr.x()));
        renderRegionBottomSWCrnr.setY(max(renderRegionBottomSWCrnr.y(), allVarBottomSWCrnr.y()));
        renderRegionBottomSWCrnr.setZ(max(renderRegionBottomSWCrnr.z(), allVarBottomSWCrnr.z()));

        renderRegionTopNECrnr.setX(min(renderRegionTopNECrnr.x(), allVarTopNECrnr.x()));
        renderRegionTopNECrnr.setY(min(renderRegionTopNECrnr.y(), allVarTopNECrnr.y()));
        renderRegionTopNECrnr.setZ(min(renderRegionTopNECrnr.z(), allVarTopNECrnr.z()));
    }
}


QVector<GLboolean> MNWPVolumeRaycasterActor::getActiveVariables() const
{
    QVector<GLboolean> active;
    active.resize(3);
    active.fill(false);

    // In iso-surface and bitfield mode, we only need to check the first variable,
    // as these modes currently do not support more.
    if (renderMode != RenderMode::DVR)
    {
        // Any variable with data can be rendered.
        active[0] = varProp->hasData();
    }
    else
    {
        // A variable is active in DVR, if it has data and a transfer function assigned,
        // OR is part of the cloud render algorithm.
        active[0] = varProp->hasData() && (varProp->transferFunction != nullptr ||
                cloudSettings->ciwcSlot == 0 || cloudSettings->clwcSlot == 0);

        active[1] = dvrSettings->secVariableProp->hasData()
                && (dvrSettings->secVariableProp->transferFunction != nullptr
                        || cloudSettings->ciwcSlot == 1
                        || cloudSettings->clwcSlot == 1);

        active[2] = dvrSettings->thirdVariableProp->hasData()
                && (dvrSettings->thirdVariableProp->transferFunction != nullptr
                        || cloudSettings->ciwcSlot == 2
                        || cloudSettings->clwcSlot == 2);

        // The second and third variable can be disabled by the user.
        active[1] == active[1] && dvrSettings->secondVarEnabledProp;
        active[2] == active[2] && dvrSettings->thirdVarEnabledProp;
    }

    return active;
}

} // namespace Met3D
