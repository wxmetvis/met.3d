/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2017 Bianca Tost [+]
**  Copyright 2024      Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef LONLATHYBVSECACTOR_H
#define LONLATHYBVSECACTOR_H

// standard library imports
#include <memory>


// related third party imports
#include <GL/glew.h>

// local application imports
#include "gxfw/nwpmultivaractor.h"
#include "gxfw/nwpactorvariable.h"
#include "gxfw/gl/texture.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "gxfw/gl/shadereffect.h"
#include "actors/transferfunction1d.h"
#include "actors/graticuleactor.h"
#include "data/waypoints/waypointstablemodel.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mstringproperty.h"

class MSceneViewGLWidget;

namespace Met3D
{

/**
  @brief MNWPVerticalSectionActor renders a vertical cross section from
  multiple model level or pressure level data variables.

  @todo Make the horizontal pressure isolines customizable.
  */
class MNWPVerticalSectionActor : public MNWPMultiVarActor,
                                 public MBoundingBoxInterface,
                                 public MPointGeneratorInterface
{
    Q_OBJECT

public:
    MNWPVerticalSectionActor();

    ~MNWPVerticalSectionActor() override;

    static QString staticActorType() { return "Vertical cross-section"; }

    static QString staticIconFileName() { return "vsec.png"; }

    void reloadShaderEffects() override;

    /**
      Implements MActor::checkIntersectionWithHandle().

      Checks is the mouse position in clip space @p clipX and @p clipY
      "touches" one of the waypoints or midpoints of this cross-section
      (midpoints are located between two waypoints; if a midpoint is moved the
      entire segment is moved). If a way- or midpoint is matched, its index is
      returned.

      Approach: Simply test each way-/midpoint. Loops over all way-/midpoints.
      The world coordinates of the waypoint are transformed to clip space using
      the scene view's MVP matrix and assuming the point to be located on the
      worldZ == 0 plane. If the distance between the waypoint's clip
      coordinates and the mouse position is smaller than @p clipRadius, the
      waypoint is considered to be matched. (@p clipRadius is typically on the
      order of a few pixels; set in the scene view.)
      */
    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                            float clipX, float clipY) override;

    void addPositionLabel(MSceneViewGLWidget *sceneView, int handleID,
                          float clipX, float clipY) override;

    /**
      Implements MActor::dragEvent().

      Drags the way-/midpoint at index @p handleID to the position on the
      worldZ == 0 plane that the mouse cursor points at, updates the vertical
      section path and triggers a redraw of the scene.

      The mouse position in world space is found by computing the intersection
      point of the ray (camera origin - mouse position) with the worldZ == 0
      plane. The section path is updated by calling @ref
      generatePathFromWaypoints(). Expensive, because the scene view's MVP
      matrix is inverted and the vertical section's path is interpolated.
      */
    void dragEvent(MSceneViewGLWidget *sceneView,
                           int handleID, float clipX, float clipY) override;

    /**
     Set the @ref MWaypointsTableModel instance from which the waypoints for
     the vertical section path are taken.
     */
    void setWaypointsModel(MWaypointsTableModel *model);

    MWaypointsTableModel* getWaypointsModel() const;

    double getBottomPressure_hPa() const;

    double getTopPressure_hPa() const;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "NWPVerticalSectionActor"; }

    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    QList<MVerticalLevelType> supportedLevelTypes() override;

    MNWPActorVariable *createActorVariable(
            const MSelectableDataVariable& dataVariable) override;

    void onBoundingBoxChanged() override;

    /** Implement MPointGeneratorInterface interface. */

    QString getPointInterfaceName() const override { return getName(); }

    QStringList getPointGeneratingSourceBaseRequestKeys() const override
    { return { "VSEC_POINT_SOURCE" }; }

    MPoints *
    generatePointsFromString(const QString& requestString) const override;

    QString generateStringForPointGeneration(
        MPointGeneratorInterfaceSettings* uiSrc) const override;

public slots:
    /**
      Generate a new set of interpolated points from the waypoints in
      the waypoints table model.

      @note This method assumes that all variables are on the same grid!
      @todo Make this work for multiple variables on different grids.
      @todo Switch between linear lat/lon connections and great circles.
      */
    void generatePathFromWaypoints(QModelIndex mindex1=QModelIndex(),
                                   QModelIndex mindex2=QModelIndex(),
                                   QGLWidget* currentGLContext = nullptr);

    void actOnWaypointInsertDelete(const QModelIndex &parent,
                                   int start, int end);

protected:
    void initializeActorResources() override;

    /**
      Renders
      A) the vertical section mesh, coloured by the data variable,
      B) contour lines (geometry shader marching squares implemenentation)
      C) isopressure lines along the vertical section,
      D) (only in modification mode) a circle to highlight a selected waypoint.
      */
    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    /**
     Implements MNWPActor::dataFieldChangedEvent(). If the data field rendered
     in this section changes, an update of the target grid is triggered and the
     range of vertical levels that are rendered is recomputed (@see
     updateVerticalLevelRange()).
     */
    void dataFieldChangedEvent() override;

    /**
     Computes a list of pressure levels at which iso-pressure lines are plotted
     along the vertical section. The pressure levels are uploaded to a texture
     buffer.
     */
    void generateIsoPressureLines();

    /**
     For each variable, the vertical levels that need to be rendered to cover
     the range p_bot .. p_top are computed. The computed bounds are used to
     discard non-visible levels in @ref renderToCurrentContext().
     */
    void updateVerticalLevelRange();

    void generateLabels();

    void onDeleteActorVariable(MNWPActorVariable* var) override;

    void onAddActorVariable(MNWPActorVariable* var) override;

    void onChangeActorVariable(MNWPActorVariable *var) override;

private:
    std::shared_ptr<GL::MShaderEffect> sectionGridShader;
    std::shared_ptr<GL::MShaderEffect> pressureLinesShader;
    std::shared_ptr<GL::MShaderEffect> marchingSquaresShader;
    std::shared_ptr<GL::MShaderEffect> simpleGeometryShader;
    std::shared_ptr<GL::MShaderEffect> positionSpheresShader;

    /**
      Each variable owns a "target grid", a 2D texture that stores the scalar
      values of the variable interpolated to the vertical section. It is used
      to speed up rendering (so that the interpolation doesn't have to be
      performed in every frame). If "targetGridToBeUpdated" is true,
      interpolation is carried out in the next frame and the target grid is
      re-computed.
      */
    bool targetGridToBeUpdated;

    MIntProperty labelDistanceProp;

    MEnumProperty waypointsModelProp;
    MWaypointsTableModel *waypointsModel;
    QVector<QVector2D> path;

    // This integer stores the ID of a waypoint to be highlighted. If the value
    // is -1, no waypoint will be highlighted. modifyWaypoint_worldZ stores the
    // worldZ coordinate of the selected waypoint, so that bot/top handles
    // can be distinguished.
    int modifyWaypoint;
    double modifyWaypoint_worldZ;

    GL::MTexture *textureVerticalSectionPath;
    int          textureUnitVerticalSectionPath;
    GL::MTexture *texturePressureLevels;
    int          textureUnitPressureLevels;

    int textureUnitShadowMap;

    GL::MVector3DVertexBuffer *vbVerticalWaypointLines;
    uint                       numVerticesVerticalWaypointLines;
    GL::MVector3DVertexBuffer *vbInteractionHandlePositions;
    uint                       numInteractionHandlePositions;
    /**
     * The vertex buffer containing the geometry for the flight path.
     * The first @c numFlightPathWaypointPositions values contain the flight path line,
     * while the rest of the vertices create the plane below the flight path.
     */
    GL::MVector3DVertexBuffer *vbFlightTrackWaypointPositions;
    uint                       numFlightTrackWaypointPositions;
    uint                       numFlightTrackWaypointVertices;
    GLuint                     flightTrackIndexBuffer;

    double p_top_hPa;
    double p_bot_hPa;
    QVector<float> pressureLineLevels;
    QVector<float> selectedPressureLineLevels;
    MStringProperty pressureLineLevelsProp;

    MFloatProperty opacityProp;
    MFloatProperty interpolationNodeSpacingProp;

    MBoolProperty enableFlightTrackProp;
    MColorProperty flightTrackLineColourProp;
    MFloatProperty flightTrackLineThicknessProp;
    MColorProperty flightTrackAreaColorProp;

    bool updatePath;

    // If the user picks the handle not in its centre, we cannot move the handle
    // by setting the centre point to the mouse position so we need this offset
    // to place the handle relative to the mouse position.
    QVector2D offsetPickPositionToHandleCentre;
};

} // namespace Met3D

#endif // LONLATHYBVSECACTOR_H
