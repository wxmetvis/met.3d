/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef TRAJECTORYACTOR_GS_H
#define TRAJECTORYACTOR_GS_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>

// local application imports
#include "gxfw/nwpmultivaractor.h"
#include "trajectories/trajectoryrequestdispatcher.h"
#include "gxfw/properties/mnwpactorvarproperty.h"

namespace Met3D
{

/**
  @brief MTrajectoryActor renders trajectories of a single ensemble member at a
  single timestep. The trajectories can be rendered as tubes or as spheres
  marking the positions of the air parcels. Spheres can be restricted to a
  single time of the trajectory.
  */
class MTrajectoryActor : public MNWPMultiVarActor, public MBoundingBoxInterface,
                         public MSynchronizedObject
{
    Q_OBJECT
public:
    MTrajectoryActor();

    ~MTrajectoryActor() override;

    static QString staticActorType() { return "Trajectories"; }

    static QString staticIconFileName() { return "trajectories.png"; }

    void reloadShaderEffects() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "TrajectoryActor"; }

    void saveConfigurationHeader(QSettings *settings) override;

    void loadConfigurationHeader(QSettings *settings) override;

    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    void onBoundingBoxChanged() override;

    bool isConnectedTo(MActor *actor) override;

    /** MultiVarActor interface */

    MNWPActorVariable *createActorVariable(
            const MSelectableDataVariable& dataSource) override;

    QList<MVerticalLevelType> supportedLevelTypes() override;

    void onAddActorVariable(MNWPActorVariable *var) override;

    void onChangeActorVariable(MNWPActorVariable *var) override;

    void onDeleteActorVariable(MNWPActorVariable *var) override;

    void onSceneViewAdded() override;

    /** Implement synchronization interface */

    void synchronizeWith(MSyncControl *sync, bool updateGUIProperties = true) override;

    bool synchronizationEvent(MSynchronizationType syncType,
                              QVector<QVariant> data) override;

public slots:
    /**
     * Slot called whenever a trajectory pipeline has changed. Triggers new
     * data request.
     */
    void trajectoryPipelinesChanged();

    /**
     * Slot called whenever the scene view layout changed. This requires
     * re-computation of the trajectory normals (e.g., vertical scaling changed).
     */
    void onSceneViewLayoutChanged();

    /**
     * Connects to the MGLResourcesManager::actorRenamed() signal. If the
     * renamed actor is a transfer function, it is renamed in the list of
     * transfer functions displayed by the transferFunctionProperty.
     */
    void onActorRenamed(MActor *actor, const QString& oldName);

    /**
     * Called whenever the trajectory computation dispatcher returns a finished
     * request. This contains trajectory data with their supplements for all
     * active seed point sources.
     * @param data The data collection group.
     */
    void dataReady(MTrajectoryDataCollectionGroup* data);

    /**
     * Set a transfer function by its name. Set to 'none' if @c tfName does
     * not exist.
     * @param tfName The name of the transfer function.
     * @return True if a valid transfer function has been set. False otherwise.
     */
    bool setTransferFunction(const QString& tfName);

    /**
     * Called whenever a delayed asynchronous data request should be executed.
     * Used to limit the amount of requests triggered.
     */
    void triggerTimeoutRequest();

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    /**
     * Outputs currently available trajectory data to an ASCII file in the
     * format used by the LAGRANTO model:
     *
      "
      Reference date 20090129_1200 / Time range 2880 min

        time      lon     lat     p
      -----------------------------
        0.00   -59.72   30.36   790
        6.00   -58.22   30.58   795
       12.00   -56.87   30.76   797
       ...
      "
     * See https://www.geosci-model-dev.net/8/2569/2015/gmd-8-2569-2015-supplement.pdf
     * for details on LAGRANTO output.
     */
    void outputAsLagrantoASCIIFile();

    virtual void dataFieldChangedEvent() override;

private:

    /**
     * Initialize the sync control for this actor.
     */
    void initializeSyncControl();

    /**
     * Initialize the dispatcher for emitting requests to the trajectory pipelines.
     */
    void initializeRequestDispatcher();

    /**
     * Try load a missing point source when the actor is loaded with settings
     * for the point sources. For example, this is called, when an actor config
     * file is loaded, and the actor depends on other actors as point sources.
     * @param settings The settings loaded.
     * @param prefix The prefix in the settings pointing to the point source.
     * @return True if loading was successful.
     */
    bool tryLoadMissingPointSource(QSettings *settings, const QString& prefix);

    /**
     * Build the @p MTrajectoryDispatchInfo struct to be sent to the dispatcher
     * based on the current state of the actor and the UI.
     */
    MTrajectoryDispatchInfo buildDispatchInfoFromCurrentState();

    /**
     * Request trajectory data, normals and supplements for current time and
     * member from the pipelines. @p synchronizationRequest is true if the
     * request originates from a synchronization event.
     */
    void asynchronousDataRequest(bool synchronizationRequest=false);

    /**
     * Adds a point source to the pipeline given the already generated
     * UI frontend.
     * @param pointSourceUI The point source user interface.
     */
    void addPointSource(MPointGeneratorInterfaceSettings *pointSourceUI);

    /**
     * Adds a point filter to the pipelines given the filter user interface.
     * @param filterUI The point filter user interface.
     */
    void addPointFilter(MPointFilterSettings *filterUI);

    /**
     * Adds a trajectory filter to the trajectory pipelines.
     * @param filterUI The trajctory filter user interface.
     */
    void addTrajectoryFilter(MTrajectoryFilterSettings *filterUI);

    /**
     * Removes a point source from the pipeline.
     * @param pointSourceUI The UI of the point source to be deleted.
     */
    void removePointSource(MPointGeneratorInterfaceSettings *pointSourceUI);

    /**
     * Removes a point filter from the pipeline.
     * @param filterUI The UI of the point filter to be deleted.
     */
    void removePointFilter(MPointFilterSettings *filterUI);

    /**
     * Removes a trajectory filter from the pipeline.
     * @param filterUI The UI of the trajectory filter to be deleted.
     */
    void removeTrajectoryFilter(MTrajectoryFilterSettings *filterUI);

    /**
     * Updates the time properties (and their colour hints) of the actor.
     */
    void updateTimeProperties();

    /**
     * Updates the ensemble properties of the actor.
     */
    void updateEnsembleProperties();

    /**
     * Updates colour hints for synchronization (green property background
     * for matching sync, red for not matching sync). If @p scene is specified,
     * the colour hints are only updated for the specified scene (e.g. used
     * when the variable's actor is added to a new scene).
     */
    void updateSyncPropertyColourHints(MSceneControl *scene = nullptr);

    /**
     * Programmatically change the current ensemble member. If @p member is -1,
     * the ensemble mode will be changed to "all".
     */
    bool setEnsembleMember(int member);

    /**
     * Set the current trajectory start time and update the scene.
     */
    bool setStartDateTime(const QDateTime &datetime);

    /**
     * Set the current particle position time and update the scene.
     */
    bool setParticleDateTime(const QDateTime &datetime);

    /**
     * Set the current forecast init time and update the scene.
     */
    bool setInitDateTime(const QDateTime &datetime);

    /**
     * Determine the current time value of the given enum property.
     */
    QDateTime getPropertyTime(MEnumProperty &enumProperty);

    /**
     * @return The currently selected ensemble member in the interface.
     */
    int getEnsembleMember();

    /**
     * Check from the dispatcher whether the currently set init and valid
     * times are available.
     * @return True if the time is available.
     */
    bool isCurrentInitAndValidTimeAvailable();

    /**
     * Update the init time property (init time refers to the base time of the
     * forecast on which the trajectory computations are based) from the current
     * data source.
     */
    void updateInitTimeProperty();

    /**
     * Update the start time property (listing the times at which trajectories
     * have been started) from the current init time and the current data source.
     */
    void updateStartTimeProperty();

    /**
     * Update the trajectory time property (available time steps for the current
     * trajectory) from the loaded trajectory data.
     */
    bool updateParticlePosTimeProperty();

    /**
     * Update the ensemble member property in the interface.
     */
    bool updateEnsembleSingleMemberProperty();

    /**
     * Updates the property displaying available integration times for
     * trajectory computation from the current trajectories data source and
     * "trajectory start time" setting.
     */
    void updateTrajectoryIntegrationTimeProperty();

    /**
     * Internal function containing common code for @c setStartDateTime() and
     * @c setInitDateTime().
     */
    bool internalSetDateTime(const QList<QDateTime>& availableTimes,
                             const QDateTime& datetime,
                             MEnumProperty &timeProperty);

    /**
     * Update the enable state of the properties. This is called whenever the
     * mode is changed between precomputed and real-time computed trajectories.
     */
    void updateEnabledProperties();

    /**
     * @c selectDataSource() calls a selection dialog to select the precomputed
     * trajectory data source for this trajectory actor.
     * @return False if the user cancelled the selection dialog, no data sources
     * are available to select, or the user selected the already selected
     * data source. True otherwise.
     */
    bool selectDataSource();

    /**
     * Loads the pipeline configuration from the @c settings. This is called on
     * loading an actor configuration and session.
     */
    void loadTrajectoryPipelineConfig(QSettings *settings, bool preV114Compatibility);

    /**
     * Saves the current state of the pipeline configuration into the provided
     * @c settings. This is called on saving an actor configuration and session.
     */
    void saveTrajectoryPipelineConfig(QSettings *settings) const;

    /**
     * Release the data items currently stored in the local data item.
     */
    void releaseData();

    /**
     * @return The available auxiliary variables to render. Includes actor
     * variables and currently active auxiliary variables from the precomputed
     * source.
     */
    QStringList getAvailableAuxiliaryVariables();

    /**
     * Update the auxiliary variable dropdown.
     * @param index The index to set in the dropdown. If -1, keep the current
     * index.
     */
    void updateAuxiliaryVariables(int index = -1);

    // The dispatcher is handling data requests to the trajectory pipelines.
    MTrajectoryRequestDispatcher *trajectoryDispatcher;

    // The data item returned from the dispatcher. Contains all required
    // information for rendering the trajectories.
    MTrajectoryDataCollectionGroup* trajectoryDataCollections;
    MSyncControl* synchronizationControl = nullptr;

    /** Render color mode (pressure, aux-data). */
    enum TrajectoryRenderColorMode {
        CONSTANT = 0,
        PRESSURE = 1,
        AUXDATA  = 2
    };

    /** Computation mode properties */
    MEnumProperty sourceTypeProp;
    MProperty precomputationGroupProp;
    MButtonProperty selectDataSourceProp;
    MStringProperty utilizedDataSourceProp;
    QString dataSourceID;

    MTrajectoryMode trajectoryMode;
    MTrajectoryRenderType renderMode;
    TrajectoryRenderColorMode renderColorMode;
    MEnumProperty renderModeProp;
    MEnumProperty renderColorModeProp;
    MColorProperty renderColourProp;

    /** Synchronization properties */
    MProperty synchronizationGroupProp;
    MEnumProperty synchronizationProp;
    MBoolProperty synchronizeInitTimeProp;
    MBoolProperty synchronizeStartTimeProp;
    MBoolProperty synchronizeParticlePosTimeProp;
    MBoolProperty synchronizeEnsembleProp;

    /** Trajectory Computation properties */
    MProperty computationGroupProp;
    MEnumProperty computationBackwardIntegrationTimeProp;
    MFloatProperty computationBackwardIntegrationTargetProp;
    MEnumProperty computationForwardIntegrationTimeProp;
    MFloatProperty computationForwardIntegrationTargetProp;
    MEnumProperty computationInterpolationMethodProp;
    MEnumProperty computationLineTypeProp;
    MEnumProperty computationIntegrationMethodProp;
    MIntProperty computationNumSubTimeStepsProp;
    MBoolProperty computationVertexAtIntegrationTimeStepsProp;
    MDoubleProperty computationStreamlineDeltaSProp;
    MBoolProperty computationIntegrateInitAndValidTimeProp;
    MIntProperty computationStreamlineLengthProp;
    MIntProperty computationRecomputeIntervalProp;

    /** Actor Variables for computations. */
    MNWPActorVarProperty uVarProp;
    MNWPActorVarProperty vVarProp;
    MNWPActorVarProperty wVarProp;
    MEnumProperty auxVarIndexProp;
    QStringList precomputedVarNameList;

    /** Time management. */
    QList<QDateTime> availableStartTimes;
    QList<QDateTime> availableInitTimes;
    QList<QDateTime> availableParticlePosTimes;
    QList<unsigned int> availableEnsembleMembersAsSortedList;
    MEnumProperty initTimeProp;
    MEnumProperty startTimeProp;
    MEnumProperty particlePosTimeProp;

    /** Ensemble management. */
    MEnumProperty ensembleModeProp;
    MEnumProperty ensembleSingleMemberProp;

    /** Trajectory pipeline. */
    MProperty computationSeedGroupProp;
    MButtonProperty computationAddSeedSourceProp;
    MProperty computationPointFilterGroupProp;
    MButtonProperty computationAddPointFilterProp;
    MProperty filtersGroupProp;
    MButtonProperty addTrajectoryFilterProp;

    QList<MPointGeneratorInterfaceSettings *> pointSourceUIs;
    QList<MPointFilterSettings*> pointFilterUIs;
    QList<MTrajectoryFilterSettings*> trajectoryFilterUIs;

    /** Rendering. */
    MProperty renderingGroupProp;

    /** Index to rendering bounding box. */
    int renderingBoundingBoxIndex;

    /** Analysis. */
    MProperty analysisGroupProp;
    MButtonProperty outputAsLagrantoASCIIProp;

    /** GLSL shader objects. */
    std::shared_ptr<GL::MShaderEffect> tubeShader;
    std::shared_ptr<GL::MShaderEffect> positionSphereShader;

    /** Pointer to transfer function object and corresponding texture unit. */
    MTransferFunction1DProperty transferFunctionProp;
    int textureUnitTransferFunction;

    /**
     * Texture unit for the scenes shadow map.
     */
    int textureUnitShadowMap;

    /** Render properties: tube/sphere radius in world space */
    MFloatProperty tubeRadiusProp;
    MFloatProperty sphereRadiusProp;

    // Time information to prevent flooding the dispatcher with requests.
    qint64 timeOfLastRequest = 0;
    bool timedRequestScheduled = false;

    /** Buttons to control manual / auto-computation. */
    MButtonProperty computeClickProp;
    MBoolProperty enableAutoComputationProp;
    bool forceComputationRequested = false;
};

} // namespace Met3D

#endif // TRAJECTORYACTOR_GS_H
