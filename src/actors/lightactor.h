/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022      Luka Elwart
**  Copyright 2023-2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef LIGHTACTOR_H
#define LIGHTACTOR_H

// standard library imports
#include <list>
#include <memory>
#include <set>
#include <vector>

// related third party imports
#include <GL/glew.h>
#include <QtCore>

// local application imports
#include "components/mtransformcomponent.h"
#include "components/mspritecomponent.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/vertexbuffer.h"
#include "movablepoleactor.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mcolorproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/marrayproperty.h"

namespace Met3D
{

class MGLResourcesManager;
class MSceneViewGLWidget;

/**
 * This struct represents packed light data that is used to
 * upload the lights to the GPU.
 * This struct is equivalent to the @c Light struct in the @c lighting.glsl file.
 * The struct memory layout should always align with multiples of 16 bytes.
 */
struct MLight
{
    /**
     * The position of the light in world space.
     */
    alignas(16) float positionWS[4] = {0, 0, 0, 0}; // 16 Bytes

    /**
     * The direction of the light in world space.
     */
    alignas(16) float directionWS[4] = {0, 0, -1, 0}; // 16 Bytes

    /**
     * The color of the light.
     */
    alignas(16) float color[4] = {1, 1, 1, 1}; // 16 Bytes

    /**
     * The right vector of the rectangle of a rect light.
     * The w component is the width of the rectangle.
     */
    alignas(16) float rectRight[4] = {1, 0, 0, 1}; // 16 Bytes

    /**
     * The up vector of the rectangle of a rect light.
     * The w component is the height of the rectangle.
     */
    alignas(16) float rectUp[4] = {0, 1, 0, 1}; // 16 Bytes

    /**
     * The intensity of the light.
     */
    alignas(4) float intensity = 1;

    /**
     * The cosine of the opening angle of the spotlight cone.
     */
    alignas(4) float spotlightAngle = 0;
    /**
     * The cosine of the half angle of the spotlight cone.
     */
    alignas(4) float spotlightHalfAngle = 0;

    /**
     * The type of the light.
     * 0 = Directional light,
     * 1 = Point light,
     * 2 = Spot light,
     * 3 = Rect light
     */
    alignas(4) int type = 0;
};

/**
 * @brief The MLightActor class implements an actor that represents a lightsource.
 * These light sources are currently only used in the direct volume ray caster to light the volume with.
 * It implements multiple different light types:\n\n
 *
 *   - A directional light, which is a light in infinite distance, such as the sun.\n
 *   - A point light, which can be seen as a light bulb casting light in all directions over a certain distance.\n
 *   - A spot light, which casts light in a cone shape.\n
 *   - A rectangular light, which casts light out of a rectangle in direction of its normal vector.\n
 */
class MLightActor : public MActor
{
    Q_OBJECT

public:
    MLightActor();
    ~MLightActor() override;

    // MActor interface
    void reloadShaderEffects() override;
    void addPositionLabel(MSceneViewGLWidget *sceneView, int handleID,
                                  float clipX, float clipY) override;
    void removePositionLabel() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "LightActor"; }

    static QString staticActorType() { return "Light Actor"; }

    static QString staticIconFileName() { return "light.png"; }

    QList<MLabel *> getPositionLabelToRender() override;

    /**
      @brief initProperties Initialize the actors properties.
     */
    void initProperties();

    /**
      @return The direction this light is facing.
     */
    QVector3D direction() const;

    /**
      @return The right vector of this lights transform.
     */
    QVector3D right() const;

    /**
      @return The up vector of this lights transform.
     */
    QVector3D up() const;

    /**
      @return This lights transform as a 4x4 Matrix.
     */
    QMatrix4x4 transform() const;

    /**
      @return The position of this light in world space.
     */
    QVector3D position() const;

    /**
      @return The color of this light.
     */
    QVector3D color() const;

    /**
      @return The light intensity.
     */
    float intensity() const;

    /**
      @return The half opening angle of the spotlight in radians.
     */
    float spotlightHalfAngle() const;

    /**
     * The view matrix of this light.
     * TODO (tv, 22Apr2024): Make this work with all light types.
     *      Especially the point light needs multiple matrices.
     * @param sceneView The scene view that requested this matrix
     * @return The view matrix of this light.
     */
    QMatrix4x4 getViewMatrix(MSceneViewGLWidget *sceneView) const;

    /**
     * The projection matrix of this light.
     * TODO (tv, 22Apr2024): Make this work with all light types.
     *      Especially the point light needs multiple matrices.
     * @param sceneView The scene view that requested this matrix
     * @return The projection matrix of this light.
     */
    QMatrix4x4 getProjectionMatrix(MSceneViewGLWidget *sceneView) const;

    /**
     * Creates an @c MLight with the data needed to upload this light to the GPU.
     * @return The @c MLight struct with data for this light.
     */
    MLight packLight() const;

    /**
      @brief The LightType enum defines different light types
     */
    enum LightType
    {
        Directional = 0,
        Point,
        Spot,
        Rect,
        LightTypeSize
    };

    /**
     * Get the type of this light.
     * @return The type of this light.
     */
    LightType getType() const { return lightType; }

    /**
     * @return the width of a rectangle light.
     */
    float getRectW() const { return rectWProp; }

    /**
     * @return the height of a rectangle light.
     */
    float getRectH() const { return rectHProp; }

    /**
     * @return The type of the light.
     */
    LightType getLightType() const { return lightType; }

    /**
     * @return Whether this light is a headlight.
     */
    bool isHeadlight() const { return headlightProp; }

    /**
     * Update the light if it is a headlight.
     * @param sceneView The scene view for which this light is used as a headlight.
     * @return If the light needed updating and therefore moved or rotated.
     */
    bool updateHeadlight(MSceneViewGLWidget *sceneView);

public slots:
    void onTransformComponentChanged();
    void emitLightChangedSignal();
    void onLightGeometryChanged();

signals:
    /**
      @brief lightChanged Called when this light is changed in any way that requires a lightmap update
      @param actor The light actor that emitted the signal.
     */
    void lightChanged();

protected:
    // MActor interface
    void initializeActorResources() override;
    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;
    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                    float clipX,
                                    float clipY) override;
    void dragEvent(MSceneViewGLWidget *sceneView, int handleID,
                   float clipX, float clipY) override;
    void releaseEvent(MSceneViewGLWidget *sceneView, int handleID) override;
    void onActorEnabledToggle() override;
    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

protected:
    /**
     * @brief generateRoundFrustumGeo Generate geometry for a cone frustum.
     * @param verts Vector where vertices get saved.
     * @param dir Direction of the cone.
     * @param up Up vector of the cone.
     * @param angle Opening angle.
     * @param far Far distance.
     */
    static void generateRoundFrustumGeo(QVector<QVector3D> &verts, QVector3D dir,
                                 QVector3D up, float angle, float far);

    /**
     * @brief generateArrowGeometry generate geometry to display an arrow.
     * @param magnitude Arrow magnitude and direction.
     * @param vertices Vector storing vertices.
     * @param indices Vector storing indices.
     * @param resolution Resolution of arrow cylinder.
     * @param radius Radius of arrow.
     * @param tipLengthRatio Ratio of tip length to arrow length.
     * @param tipRadiusRatio Ratio of tip radius to arrow radius.
     * @param interleaveNormal Whether to interleave normals or not.
     */
    void generateArrowGeometry(const QVector3D &magnitude,
                               QVector<QVector3D> &vertices,
                               QVector<uint32_t> &indices,
                               const int &resolution, const float &radius,
                               const float &tipLengthRatio,
                               const float &tipRadiusRatio,
                               const bool &interleaveNormal);

    /**
      @brief initGeometry Initialize geometry vertex and index buffers.
     */
    void initGeometry();

    /**
      @brief reloadSprite Loads the light icon sprite for a light of type @ref lightType.
     */
    void reloadSprite() const;

    /**
      @brief spawnPole Spawn vertical pole for this light.
     */
    void spawnPole();

    /**
      @brief updateProperties Update properties to only show the ones currently needed for this light.
     */
    void updateProperties();

    /**
      @brief lightType The current type of the light.
     */
    LightType lightType;

    /**
      @brief spotAngleProp Angle of spotlight cone in degrees. Only valid if @ref lightType is @ref LightType::Spot.
     */
    MFloatProperty spotAngleProp;

    /**
      @brief rectWProp Width of rect light. Only valid if @ref lightType is @ref LightType::Rect.
     */
    MFloatProperty rectWProp;

    /**
      @brief rectH Height of rect light. Only valid if @ref lightType is @ref LightType::Rect.
     */
    MFloatProperty rectHProp;

    /**
      @brief rectFilled Whether the rect light is displayed as a filled rectangle or just the bounding lines. Only valid if @ref lightType is @ref LightType::Rect.
     */
    bool rectFilled;

    /**
      @brief lightMaxDistanceProp Maximum distance from the light, which it can influence.
     */


    /**
      @brief lastModelMatrix Last frame model matrix of this light. Used to check transform changes which require light updates.
     */
    QMatrix4x4 lastModelMatrix;

    /**
      @brief gizmo Transform gizmo of this light. Also contains position and rotation information.
     */
    std::shared_ptr<MTransformComponent> transformComponent;

    /**
     * Sprite of this light. Displays an icon representing the light type.
     */
    std::shared_ptr<MSpriteComponent> sprite;

    /**
      @brief Whether the last intersection checked by 'checkIntersectionWithHandleAndAllComponents()' was with the movable pole actor.
      If false the last intersection was the transform gizmo, if 'checkIntersectionWithHandleAndAllComponents()' returned true, otherwise no component of this actor was intersected.
    */
    bool bIntersectionWasPole;

    /**
      @brief defaultForwardDir Default forward direction of this light when no rotation is applied. Should not be changed.
     */
    const QVector3D defaultForwardDir = QVector3D(0, 1, 0);

    /**
      @brief defaultUpDir Default up direction of this light when no rotation is applied. Should not be changed.
     */
    const QVector3D defaultUpDir = QVector3D(0, 0, 1);

    /**
      @brief defaultRightDir Default right direction of this light when no rotation is applied. Should not be changed.
     */
    const QVector3D defaultRightDir = QVector3D(1, 0, 0);

    // Properties
    MEnumProperty lightTypeProp;
    MColorProperty lightColorProp;
    MFloatProperty lightIntensityProp;
    MBoolProperty showGeometryProp;
    MArrayProperty rectSizeProp;
    MBoolProperty headlightProp;

    QVector3D previousLightPos;
    QVector3D previousLightRot;

    // Light rendering
    std::shared_ptr<GL::MShaderEffect> shader;
    GL::MVertexBuffer *triVertexBuffer;
    GLuint triIndexBuffer;
    GLuint triIndexBufferSize;
    GL::MVertexBuffer *lineVertexBuffer;
    GLuint lineVertexBufferSize;

    // Pole rendering
    std::shared_ptr<GL::MShaderEffect> poleShader;
    GL::MVertexBuffer *poleVertexBuffer;
    uint32_t numPoleIndices;
    GLuint poleIndexBuffer;

    // TODO (tv, 20Feb2023) Maybe rework movable pole actor to use actor components for each pole. This way, the light actor could simply
    //      add a single movable pole component instead of inheriting the movable pole actor and stripping it of its functionality.
    //      Will also add another use case for actor components.
    // Pole sub actor.
    std::shared_ptr<MMovablePoleActor> poleActor;
};

} // namespace Met3D

#endif // LIGHTACTOR_H
