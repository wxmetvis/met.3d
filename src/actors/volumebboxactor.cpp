/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017      Bianca Tost [+]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "volumebboxactor.h"

// standard library imports
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"

namespace Met3D
{


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MVolumeBoundingBoxActor::MVolumeBoundingBoxActor()
    : MActor(),
      MBoundingBoxInterface(this, MBoundingBoxConnectionType::VOLUME),
      coordinateVertexBuffer(nullptr),
      axisVertexBuffer(nullptr)
{
    // Create and initialise properties.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    // Bounding box of the actor.
    insertBoundingBoxProperty(&actorPropertiesSupGroup);

    tickLengthProp = MFloatProperty("Tick length", 0.8);
    tickLengthProp.setConfigKey("tick_length");
    tickLengthProp.setMinMax(0.001f, 20.0f);
    tickLengthProp.setDecimals(3);
    tickLengthProp.setStep(0.05f);
    tickLengthProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(tickLengthProp);

    lineColourProp = MColorProperty("Colour", QColor(0, 104, 139, 255));
    lineColourProp.setConfigKey("colour");
    lineColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(lineColourProp);

    auto updateGeometryCallback = [=]()
    {
        if (suppressActorUpdates()) return;
        generateGeometry();
        emitActorChangedSignal();
    };

    // Set value callbacks for parent class properties.
    labelProps.fontSizeProp.registerValueCallback(updateGeometryCallback);
    labelProps.fontColourProp.registerValueCallback(updateGeometryCallback);
    labelProps.enableBBoxProp.registerValueCallback(updateGeometryCallback);
    labelProps.bboxColourProp.registerValueCallback(updateGeometryCallback);
}


MVolumeBoundingBoxActor::~MVolumeBoundingBoxActor()
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE 0

void MVolumeBoundingBoxActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");
    geometryEffect->compileFromFile_Met3DHome("src/glsl/simple_coloured_geometry.fx.glsl");
}


void MVolumeBoundingBoxActor::setColour(const QColor& c)
{
    lineColourProp = c;
}


void MVolumeBoundingBoxActor::saveConfiguration(QSettings *settings)
{
    settings->beginGroup(MVolumeBoundingBoxActor::getSettingsID());

    MBoundingBoxInterface::saveConfiguration(settings);

    settings->endGroup();
}


void MVolumeBoundingBoxActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MActor::loadConfigurationPrior_V_1_14(settings);

    settings->beginGroup(MVolumeBoundingBoxActor::getSettingsID());

    MBoundingBoxInterface::loadConfiguration(settings);

    tickLengthProp = settings->value("tickLength", tickLengthProp.value()).toFloat();
    lineColourProp = settings->value("lineColour", lineColourProp.value()).value<QColor>();

    settings->endGroup();
}


void MVolumeBoundingBoxActor::onBoundingBoxChanged()
{
    labels.clear();
    if (suppressActorUpdates())
    {
        return;
    }
    // Switching to no bounding box only needs a redraw, but no recomputation
    // because it disables rendering of the actor.
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        emitActorChangedSignal();
        return;
    }
    generateGeometry();
    emitActorChangedSignal();
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MVolumeBoundingBoxActor::initializeActorResources()
{
    generateGeometry();

    bool loadShaders = false;

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    loadShaders |= glRM->generateEffectProgram("volumebox_shader", geometryEffect);

    if (loadShaders) reloadShaderEffects();
}


void MVolumeBoundingBoxActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }
    // A) Render volume box.
    // =====================

    // Bind shader program.
    geometryEffect->bindProgram("Pressure");

    // Set uniform and attribute values.
    geometryEffect->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    geometryEffect->setUniformValue(
                "pToWorldZParams", sceneView->pressureToWorldZParameters());

    coordinateVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    geometryEffect->setUniformValue("colour", lineColourProp);
    sceneView->setLineWidth(2.0f / sceneView->getViewportUpscaleFactor());
    glDrawArrays(GL_LINE_STRIP, 0, coordinateSystemVertices.size());

    // Unbind VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;


    // B) Render tick marks and adjust label positions.
    // ================================================

    // Bind shader program.
    geometryEffect->bindProgram("TickMarks");

    // Set uniform and attribute values.
    geometryEffect->setUniformValue(
                "pToWorldZParams", sceneView->pressureToWorldZParameters());
    geometryEffect->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    geometryEffect->setUniformValue(
            "colour", lineColourProp);

    // Offset for the "other end" of the tick line and anchor offset for
    // the labels.
    QVector3D anchorOffset = tickLengthProp * sceneView->getCamera()->getXAxis();

    geometryEffect->setUniformValue(
                "offsetDirection", anchorOffset);

    // Set label offset; the labels are rendered by the text manager.
    for (auto & label : labels)
        label->anchorOffset = anchorOffset;

    axisVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
    sceneView->setLineWidth(2.0f / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;
    glDrawArrays(GL_POINTS, 0, axisTicks.size()); CHECK_GL_ERROR;

    // Unbind VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MVolumeBoundingBoxActor::generateGeometry()
{
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }
    // A) Generate geometry.
    // =====================

    // Coordinates of a unit cube that becomes the volume box.
    const unsigned int unitcubeLength = 16;
    float unitcubeCoordinates[unitcubeLength][3] = {{0, 0, 0},
                                                    {1, 0, 0},
                                                    {1, 1, 0},
                                                    {0, 1, 0},
                                                    {0, 0, 0},
                                                    {0, 0, 1},
                                                    {1, 0, 1},
                                                    {1, 0, 0},
                                                    {1, 0, 1},
                                                    {1, 1, 1},
                                                    {1, 1, 0},
                                                    {1, 1, 1},
                                                    {0, 1, 1},
                                                    {0, 1, 0},
                                                    {0, 1, 1},
                                                    {0, 0, 1}};

    float llcrnrlat = getBBoxConnection()->southLat();
    float llcrnrlon = getBBoxConnection()->westLon();
    float urcrnrlat = getBBoxConnection()->northLat();
    float urcrnrlon = getBBoxConnection()->eastLon();

    float pbot_hPa = getBBoxConnection()->bottomPressure_hPa();
    float ptop_hPa = getBBoxConnection()->topPressure_hPa();

    // Create coordinate system geometry.
    coordinateSystemVertices.clear();
    for (unsigned int i = 0; i < unitcubeLength; i++) {
        coordinateSystemVertices.append(
                    QVector3D(
                        llcrnrlon + unitcubeCoordinates[i][0]
                        * (urcrnrlon-llcrnrlon),

                        urcrnrlat - unitcubeCoordinates[i][1]
                        * (urcrnrlat-llcrnrlat),

                        (unitcubeCoordinates[i][2] == 0) ? pbot_hPa :  ptop_hPa
                        )
                    );
    }

    // Create axis ticks geometry.
    axisTicks.clear();
    int interval = pbot_hPa > 100. ? 100 : 10;
    int p = int(pbot_hPa / interval) * interval;

    while (p >= ptop_hPa)
    {
        axisTicks.append(QVector3D(llcrnrlon, llcrnrlat, p));
        axisTicks.append(QVector3D(llcrnrlon, urcrnrlat, p));
        axisTicks.append(QVector3D(urcrnrlon, urcrnrlat, p));
        axisTicks.append(QVector3D(urcrnrlon, llcrnrlat, p));

        p -= (p > 100) ? 100 : 10;
    }


    // B) Upload geometry data to VBO.
    // ===============================

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    glRM->makeCurrent();

    const QString coordinateRequestKey = "coords_vertices_actor#"
                                    + QString::number(getID());
    uploadVec3ToVertexBuffer(coordinateSystemVertices, coordinateRequestKey,
                             &coordinateVertexBuffer);

    const QString axisRequestKey = "axis_vertices_actor#"
                                    + QString::number(getID());
    uploadVec3ToVertexBuffer(axisTicks, axisRequestKey, &axisVertexBuffer);


    // C) Generate labels.
    // ===================

    // Remove all text labels of the old geometry (MActor method).
    removeAllLabels();
    MTextManager* tm = glRM->getTextManager();

    int drawLabel = 0;
    for (auto tickPosition : axisTicks)
    {
        // Label only every 3rd tick mark (4 labelled axis of the volume box,
        // hence 4 ticks for each pressure.
        if (drawLabel++ < 0) continue;
        if (drawLabel == 4)  drawLabel = -8;

        labels.append(tm->addText(
                              QString("%1").arg(tickPosition.z()),
                              MTextManager::LONLATP,
                              tickPosition.x(), tickPosition.y(), tickPosition.z(),
                              labelProps.fontSizeProp,
                              labelProps.fontColourProp,
                              MTextManager::MIDDLELEFT,
                              labelProps.enableBBoxProp,
                              labelProps.bboxColourProp)
                      );
    }
}

} // namespace Met3D
