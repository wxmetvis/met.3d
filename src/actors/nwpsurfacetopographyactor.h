/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus [*, previously +]
**  Copyright 2024 Thorwin Vogt [*]
**  Copyright 2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef LONLATSURFACEACTOR_H
#define LONLATSURFACEACTOR_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>

// local application imports
#include "gxfw/nwpmultivaractor.h"
#include "gxfw/nwpactorvariable.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/properties/mnwpactorvarproperty.h"
#include "actors/transferfunction1d.h"
#include "actors/graticuleactor.h"

class MSceneViewGLWidget;

namespace Met3D
{

/**
  @brief MNWPSurfaceTopographyActor renders a 2D NWP surface field with
  topography.

  Rendering is implemented using "instanced rendering" (notes 09Feb2012).

  @todo Revise implementation of lighting. (mr, 13Feb2012)
  @todo Make this work with PVU and, in general, ML surfaces! (mr, 13Feb2012)
  */
class MNWPSurfaceTopographyActor : public MNWPMultiVarActor,
        public MBoundingBoxInterface
{
public:
    MNWPSurfaceTopographyActor();
    ~MNWPSurfaceTopographyActor() override;

    static QString staticActorType() { return "Surface topography"; }

    static QString staticIconFileName() { return "topography.png"; }

    void reloadShaderEffects() override;

    QList<MVerticalLevelType> supportedLevelTypes() override;

    MNWPActorVariable *createActorVariable(
            const MSelectableDataVariable& dataVariable) override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "NWPSurfaceTopographyActor"; }

    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    void onBoundingBoxChanged() override;

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    void dataFieldChangedEvent() override;

    /**
      Computes the array indices of the data field covered by the current
      bounding box. Computation is done per-variable.

      @see MNWP2DHorizontalActorVariable::computeRenderRegionParameters()
      */
    void computeRenderRegionParameters();

    void onDeleteActorVariable(MNWPActorVariable* var) override;

    void onAddActorVariable(MNWPActorVariable* var) override;

    void onChangeActorVariable(MNWPActorVariable *var) override;

    MNWPActorVarProperty topographyVariableProp;
    MNWPActorVarProperty shadingVariableProp;

    bool updateRenderRegion;

    QList<QString> varNames;

private:
    std::shared_ptr<GL::MShaderEffect> shaderProgram;

    int texUnitShadowMap;

};

} // namespace Met3D

#endif // LONLATSURFACEACTOR_H
