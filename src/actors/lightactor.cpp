/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022      Luka Elwart
**  Copyright 2023-2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "lightactor.h"
#include <QImage>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "util/mutil.h"

namespace Met3D
{

static QString LIGHT_ACTOR_SPRITES[MLightActor::LightType::LightTypeSize] = {
        "directional.png", "point.png", "spot.png", "rect.png"};

MLightActor::MLightActor()
    : MActor(),
      lightType(LightType::Directional),
      rectFilled(false),
      lastModelMatrix(),
      bIntersectionWasPole(false),
      triVertexBuffer(nullptr),
      triIndexBuffer(0),
      lineVertexBuffer(nullptr),
      poleVertexBuffer(nullptr),
      poleIndexBuffer(0)
{
    // Setup light
    lastModelMatrix = QMatrix4x4();

    setActorType(staticActorType());
    setName(getActorType());
    enablePicking(true);

    initProperties();

    // Add transform component
    transformComponent = this->addComponentOfType<MTransformComponent>(
            QString("Transform"), false, true, false, "Light direction");

    // Add sprite component
    sprite = this->addComponentOfType<MSpriteComponent>(
            "Sprite", MSpriteComponent::WORLD_SPACE, false);
    sprite->setEnabled(showGeometryProp);
    sprite->setOpacity(1.0);
    sprite->setScale(0.05, 0.05);
    sprite->setPosition(position());

    // Set default rotation to view down along the z axis.
    transformComponent->setPitch(-90.0);

    connect(transformComponent.get(), &MTransformComponent::componentChanged, this, &MLightActor::onTransformComponentChanged);

    poleActor = std::make_shared<MMovablePoleActor>();
    poleActor->setName("lightActorPole");
}


MLightActor::~MLightActor()
{
    disconnect(transformComponent.get(), &MTransformComponent::componentChanged, this, &MLightActor::onTransformComponentChanged);

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    if (triVertexBuffer) glRM->releaseGPUItem(triVertexBuffer);
    if (lineVertexBuffer) glRM->releaseGPUItem(lineVertexBuffer);
    if (poleVertexBuffer) glRM->releaseGPUItem(poleVertexBuffer);
}


void MLightActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");

    poleActor->reloadShaderEffects();

    beginCompileShaders(2);

    compileShadersFromFileWithProgressDialog(shader,
                                             "src/glsl/simple_coloured_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(poleShader,
                                             "src/glsl/light_pole.fx.glsl");
    endCompileShaders();
}


QList<MLabel *> MLightActor::getPositionLabelToRender()
{
    return MActor::getPositionLabelToRender();
}


void MLightActor::addPositionLabel(MSceneViewGLWidget *sceneView, int handleID,
                                   float clipX, float clipY)
{
    if (bIntersectionWasPole)
    {
        poleActor->addPositionLabel(sceneView, handleID, clipX, clipY);
    }
}


void MLightActor::removePositionLabel()
{
    if (bIntersectionWasPole)
    {
        poleActor->removePositionLabel();
    }
}


void MLightActor::initProperties()
{
    // Light properties
    QStringList typesList;
    typesList << "Directional"
              << "Point"
              << "Spot"
              << "Rectangle";
    lightTypeProp = MEnumProperty("Light type", typesList, 0);
    lightTypeProp.setConfigKey("light_type");
    lightTypeProp.setTooltip("The type of this light. Directional is a light "
                              "source in infinite distance, emulating a sun.");
    lightTypeProp = static_cast<int>(lightType);
    lightTypeProp.registerValueCallback([=]()
    {
        lightType = static_cast<LightType>(lightTypeProp.value());

        if (isInitialized())
        {
            initGeometry();
            reloadSprite();
        }

        updateProperties();

        if (suppressActorUpdates()) return;

        emitLightChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(lightTypeProp);

    headlightProp = MBoolProperty("Headlight mode", false);
    headlightProp.setConfigKey("headlight_mode_enabled");
    headlightProp.setTooltip("Attaches this light as headlight to the camera of all active scene views.");
    headlightProp.registerValueCallback([=]()
    {
        if (headlightProp)
        {
            previousLightPos = transformComponent->getPosition();
            previousLightRot = transformComponent->getRotation();
            enablePicking(false);
        }
        else
        {
            transformComponent->setPosition(previousLightPos);
            transformComponent->setRotation(previousLightRot);
            enablePicking(true);
        }
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(headlightProp);

    // Light Rendering
    lightColorProp = MColorProperty("Light color", QColor(255, 255, 255));
    lightColorProp.setConfigKey("light_color");
    lightColorProp.setTooltip("The color of this light.");
    lightColorProp.registerValueCallback(this, &MLightActor::emitLightChangedSignal);
    actorPropertiesSupGroup.addSubProperty(lightColorProp);

    lightIntensityProp = MFloatProperty("Light intensity", 1.0f);
    lightIntensityProp.setConfigKey("light_intensity");
    lightIntensityProp.setTooltip("Light intensity. 1 is default "
                              "and falls off using inverse square law.");
    lightIntensityProp.setMinimum(0);
    lightIntensityProp.registerValueCallback(this, &MLightActor::emitLightChangedSignal);
    actorPropertiesSupGroup.addSubProperty(lightIntensityProp);


    // Initialize light type specific properties
    spotAngleProp = MFloatProperty("Spot light angle", 90.0f);
    spotAngleProp.setConfigKey("spot_angle");
    spotAngleProp.setTooltip("The spotlight opening angle in degrees.");
    spotAngleProp.setMinimum(0);
    spotAngleProp.setSuffix(" °");
    spotAngleProp.registerValueCallback(this, &MLightActor::onLightGeometryChanged);
    actorPropertiesSupGroup.addSubProperty(spotAngleProp);

    rectSizeProp = MArrayProperty("Rectangle light size");
    actorPropertiesSupGroup.addSubProperty(rectSizeProp);

    rectWProp = MFloatProperty("Width", 10.0f);
    rectWProp.setConfigKey("rect_width");
    rectWProp.setTooltip("The width of the rectangular light source.");
    rectWProp.setMinimum(0);
    rectWProp.registerValueCallback(this, &MLightActor::onLightGeometryChanged);
    rectSizeProp.append(&rectWProp);

    rectHProp = MFloatProperty("Height", 8.0f);
    rectHProp.setConfigKey("rect_height");
    rectHProp.setTooltip("The height of the rectangular light source.");
    rectHProp.setMinimum(0);
    rectHProp.registerValueCallback(this, &MLightActor::onLightGeometryChanged);
    rectSizeProp.append(&rectHProp);

    updateProperties();

    showGeometryProp = MBoolProperty("Light geometry", true);
    showGeometryProp.setTooltip("Hides the light sprite and geometry but still "
                            "renders the transformation gizmo in interaction "
                            "mode and the vertical pole in any mode.");
    showGeometryProp.setConfigKey("light_geometry_enabled");
    showGeometryProp.registerValueCallback(this, [=]()
    {
        sprite->setEnabled(showGeometryProp);

        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(showGeometryProp);
}


QVector3D MLightActor::direction() const
{
    QMatrix4x4 model =
        transformComponent->getTransformMatrix(false, true, false);
    return model * defaultForwardDir;
}


QVector3D MLightActor::right() const
{
    QMatrix4x4 model =
        transformComponent->getTransformMatrix(false, true, false);
    return model * defaultRightDir;
}


QVector3D MLightActor::up() const
{
    QMatrix4x4 model =
        transformComponent->getTransformMatrix(false, true, false);
    return model * defaultUpDir;
}


QMatrix4x4 MLightActor::transform() const
{
    return transformComponent->getTransformMatrix(true, true, false);
}


QVector3D MLightActor::position() const
{
    return transformComponent->getPosition();
}


QVector3D MLightActor::color() const
{
    return QVector3D(lightColorProp.value().redF(),
                     lightColorProp.value().greenF(),
                     lightColorProp.value().blueF());
}


float MLightActor::intensity() const
{
    return lightIntensityProp;
}


float MLightActor::spotlightHalfAngle() const
{
    return (spotAngleProp * M_PI / 180.f) * 0.5f;
}


QMatrix4x4 MLightActor::getViewMatrix(MSceneViewGLWidget *sceneView) const
{
    if (lightType == Directional)
    {
        MCamera *cam = sceneView->getCamera();

        float near = cam->getNearPlane();
        float far = cam->getFarPlane();
        QVector3D camOrigin = cam->getOrigin();
        QVector3D camDir = cam->getZAxis();
        QVector3D center = camOrigin + camDir * ((far - near) / 2.0f);
        QVector3D lightDir = direction().normalized();

        if (headlightProp)
        {
            lightDir = cam->getZAxis();
        }

        // Get center on ground, so that shadow view, matrix always includes the scene.
        // With a high far-plane, looking down, the center will be far below the sea level,
        // resulting in higher geometry being behind the near plane of the light.
        // We don't want that, so we make it so that the light always points to a ground
        // location or higher, but never below.
        if (center.z() < 0)
        {
            center = center - lightDir * center.z();
        }

        float distance = static_cast<float>(sceneView->getSceneBounds().radius * 2);

        QMatrix4x4 view;
        view.lookAt(center - lightDir * distance,
                    center,
                    up());
        return view;
    }
    return {};
}


QMatrix4x4 MLightActor::getProjectionMatrix(MSceneViewGLWidget *sceneView) const
{
    if (lightType == Directional)
    {
        QVector<QVector4D> frustum = sceneView->getCamera()->getViewFrustum();
        QMatrix4x4 view = getViewMatrix(sceneView);

        float minX = std::numeric_limits<float>::max();
        float maxX = std::numeric_limits<float>::lowest();
        float minY = minX;
        float maxY = maxX;
        float minZ = minY;
        float maxZ = maxY;

        // Convert frustum to light space.
        for (const QVector4D &corner : frustum)
        {
            QVector4D trf = view * corner;
            minX = std::min(minX, trf.x());
            maxX = std::max(maxX, trf.x());
            minY = std::min(minY, trf.y());
            maxY = std::max(maxY, trf.y());
            minZ = std::min(minZ, trf.z());
            maxZ = std::max(maxZ, trf.z());
        }

        maxZ = static_cast<float>(sceneView->getSceneBounds().radius * 4);

        QMatrix4x4 proj;
        proj.setToIdentity();
        proj.ortho(minX, maxX,
                   minY, maxY, 0, maxZ);
        return proj;
    }
    return {};
}


MLight MLightActor::packLight() const
{
    MLight light{};
    QVector3D pos = position();
    QVector3D dir = direction();
    QColor color = lightColorProp;

    light.positionWS[0] = pos.x();
    light.positionWS[1] = pos.y();
    light.positionWS[2] = pos.z();

    light.directionWS[0] = dir.x();
    light.directionWS[1] = dir.y();
    light.directionWS[2] = dir.z();

    light.color[0] = static_cast<float>(color.redF());
    light.color[1] = static_cast<float>(color.greenF());
    light.color[2] = static_cast<float>(color.blueF());
    light.color[3] = static_cast<float>(color.alphaF());

    light.intensity = lightIntensityProp;

    float angle = spotlightHalfAngle();
    light.spotlightAngle = cos(angle);
    light.spotlightHalfAngle = cos(angle / 2.0f);

    QVector3D right = this->right();
    QVector3D up = this->up();

    light.rectRight[0] = right.x();
    light.rectRight[1] = right.y();
    light.rectRight[2] = right.z();
    light.rectRight[3] = rectWProp;

    light.rectUp[0] = up.x();
    light.rectUp[1] = up.y();
    light.rectUp[2] = up.z();
    light.rectUp[3] = rectHProp;

    light.type = static_cast<int>(lightType);

    return light;
}


bool MLightActor::updateHeadlight(MSceneViewGLWidget *sceneView)
{
    if (headlightProp)
    {
        enableActorUpdates(false);
        QVector3D oldPos = transformComponent->getPosition();
        QVector3D oldRot = transformComponent->getRotation();

        MCamera *cam = sceneView->getCamera();
        transformComponent->setPosition(cam->getOrigin());
        // TODO (tv, 16Oct2024): Rotation should be unified. Why is the pitch of
        //   the camera different from the pitch of the transform component?
        transformComponent->setRotation(cam->getPitch() - 90.0f, cam->getYaw(), cam->getRoll());
        enableActorUpdates(true);

        if (oldPos != transformComponent->getPosition() || oldRot != transformComponent->getRotation())
        {
            return true;
        }
    }

    return false;
}


void MLightActor::onTransformComponentChanged()
{
    sprite->setPosition(transformComponent->getPosition());
    QPointF lonLat = {transformComponent->getPosition().x(), transformComponent->getPosition().y()};
    poleActor->setPolePosition(0, lonLat);
    emit lightChanged();
}


void MLightActor::emitLightChangedSignal()
{
    if (suppressActorUpdates()) return;
    emit lightChanged();
    emitActorChangedSignal();
}


void MLightActor::onLightGeometryChanged()
{
    if (isInitialized())
        initGeometry();

    emit lightChanged();
    emitActorChangedSignal();
}


void MLightActor::initializeActorResources()
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    poleActor->initialize();

    spawnPole();
    reloadSprite();

    bool loadShaders = false;

    // Load shader program if the returned program is new.
    loadShaders |= glRM->generateEffectProgram("WorldSpace", shader);
    loadShaders |= glRM->generateEffectProgram("LightPole", poleShader);

    if (loadShaders) reloadShaderEffects();

    initGeometry();
}


void MLightActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    labels.clear();
    if (!headlightProp)
    {
        poleActor->render(sceneView);
        labels = poleActor->getLabelsToRender();
    }

    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING) return;

    if (!showGeometryProp.value() || headlightProp)
    {
        lastModelMatrix =
            transformComponent->getTransformMatrix(true, true, false);
        return;
    }

    // 1. Render light pole
    poleShader->bindProgram("LightPole");
    CHECK_GL_ERROR;
    QMatrix4x4 poleMatrix =
        transformComponent->getTransformMatrix(true, false, false);
    QMatrix4x4 realPoleMVP =
        *(sceneView->getModelViewProjectionMatrix()) * poleMatrix;
    QVector3D position = transformComponent->getPosition();

    poleShader->setUniformValue("mvpMatrix", realPoleMVP);
    CHECK_GL_ERROR;
    poleShader->setUniformValue("colour", QVector4D(0.f, 0.f, 0.f, 1.f));
    CHECK_GL_ERROR;
    poleShader->setUniformValue("poleHeight", position.z());
    CHECK_GL_ERROR;

    poleVertexBuffer->attachToVertexAttribute(0);
    CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poleIndexBuffer);
    CHECK_GL_ERROR;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    CHECK_GL_ERROR;
    glDrawElements(GL_TRIANGLES, numPoleIndices, GL_UNSIGNED_INT, (void *)0);
    CHECK_GL_ERROR;

    // 2. Render light geometry
    QMatrix4x4 model =
        transformComponent->getTransformMatrix(true, true, false);

    QMatrix4x4 mvp = *(sceneView->getModelViewProjectionMatrix()) * model;
    if (lightType == Directional)
    {
        shader->bindProgram("WorldSpace");
        CHECK_GL_ERROR;

        shader->setUniformValue("mvpMatrix", mvp);
        CHECK_GL_ERROR;
        shader->setUniformValue("colour", QVector4D(0.f, 0.f, 0.f, 1.f));
        CHECK_GL_ERROR;

        triVertexBuffer->attachToVertexAttribute(0);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triIndexBuffer);
        CHECK_GL_ERROR;

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        CHECK_GL_ERROR;
        glDrawElements(GL_TRIANGLES, triIndexBufferSize, GL_UNSIGNED_INT,
                       (void *)0);
        CHECK_GL_ERROR;
    }
    else if (lightType == Spot)
    {
        shader->bindProgram("WorldSpace");
        CHECK_GL_ERROR;

        shader->setUniformValue("mvpMatrix", mvp);
        CHECK_GL_ERROR;
        shader->setUniformValue("colour", QVector4D(0.f, 0.f, 0.f, 1.f));
        CHECK_GL_ERROR;

        lineVertexBuffer->attachToVertexAttribute(0);
        CHECK_GL_ERROR;

        // Draw
        glDrawArrays(GL_LINES, 0, lineVertexBufferSize);
        CHECK_GL_ERROR;
    }
    else if (lightType == Rect)
    {
        // Render rectangle
        shader->bindProgram("WorldSpace");
        CHECK_GL_ERROR;

        shader->setUniformValue("mvpMatrix", mvp);
        CHECK_GL_ERROR;
        shader->setUniformValue("colour",
                                rectFilled ? QVector4D(lightColorProp.value().red(),
                                                       lightColorProp.value().green(),
                                                       lightColorProp.value().blue(), 1.f)
                                           : QVector4D(0.f, 0.f, 0.f, 1.f));
        CHECK_GL_ERROR;

        lineVertexBuffer->attachToVertexAttribute(0);
        CHECK_GL_ERROR;

        if (rectFilled) glDrawArrays(GL_TRIANGLE_FAN, 0, lineVertexBufferSize);
        else
            glDrawArrays(GL_LINE_LOOP, 0, lineVertexBufferSize);
        CHECK_GL_ERROR;

        // Render direction arrow
        shader->bindProgram("WorldSpace");
        CHECK_GL_ERROR;

        shader->setUniformValue("mvpMatrix", mvp);
        CHECK_GL_ERROR;
        shader->setUniformValue("colour", QVector4D(0.f, 0.f, 0.f, 1.f));
        CHECK_GL_ERROR;

        triVertexBuffer->attachToVertexAttribute(0);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triIndexBuffer);
        CHECK_GL_ERROR;

        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        CHECK_GL_ERROR;
        glDrawElements(GL_TRIANGLES, triIndexBufferSize, GL_UNSIGNED_INT,
                       (void *)0);
        CHECK_GL_ERROR;
    }

    // Update last model matrix
    lastModelMatrix = model;
}


int MLightActor::checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                                 float clipX, float clipY)
{
    int handleID = poleActor->checkIntersectionWithHandleAndAllComponents(
        sceneView, clipX, clipY);

    if (handleID < 0)
    {
        bIntersectionWasPole = false;
    }
    else
    {
        bIntersectionWasPole = true;
    }

    return handleID;
}


void MLightActor::dragEvent(MSceneViewGLWidget *sceneView, int handleID,
                            float clipX, float clipY)
{
    if (bIntersectionWasPole)
    {
        poleActor->dragEventWithAllComponents(sceneView, handleID, clipX, clipY);
        QPointF pp = poleActor->getPolePosition(0);

        transformComponent->setPositionX(pp.x());
        transformComponent->setPositionY(pp.y());

        emitActorChangedSignal();
        emit lightChanged();
    }
    else
    {
        QVector3D pos = transformComponent->getPosition();

        poleActor->setPolePosition(0, QPointF(pos.x(), pos.y()));

        emitActorChangedSignal();
        emit lightChanged();
    }
}


void MLightActor::releaseEvent(Met3D::MSceneViewGLWidget *sceneView,
                               int handleID)
{
    poleActor->releaseEventWithAllComponents(sceneView, handleID);
}


void MLightActor::onActorEnabledToggle()
{
    emit lightChanged();
}


void MLightActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MActor::loadConfigurationPrior_V_1_14(settings);

    settings->beginGroup(MLightActor::getSettingsID());

    lightTypeProp.setEnumItem(settings->value("Light Type").toString());
    lightType = static_cast<LightType>(lightTypeProp.value());

    lightColorProp = settings->value("Light Color").value<QColor>();

    lightIntensityProp = settings->value("Light Intensity").toFloat();

    spotAngleProp = settings->value("SpotAngle").toFloat();

    rectWProp = settings->value("RectW").toFloat();

    rectHProp = settings->value("RectH").toFloat();

    showGeometryProp = !settings->value("HideGeo", false).toBool();

    settings->endGroup();
}


void MLightActor::generateRoundFrustumGeo(QVector<QVector3D> &verts,
                                          QVector3D dir, QVector3D up,
                                          float angle, float far)
{
    // clamp between [0, 180) degrees
    angle = fmin(M_PI * 0.99f, fmax(0.f, angle));

    QVector3D right = QVector3D::crossProduct(dir, up);
    right.normalize();

    float farRadius = tan(angle * 0.5f) * far;

    const int circleRes = 12;

    verts.clear();
    for (int i = 0; i < circleRes; i++)
    {
        float angle = float(i) / float(circleRes) * 2.f * M_PI;
        float nextAngle =
            float((i + 1) % circleRes) / float(circleRes) * 2.f * M_PI;

        QVector3D curVert = QVector3D(dir * far + sin(angle) * right * farRadius
                                      + cos(angle) * up * farRadius);
        verts.push_back(curVert);
        verts.push_back(QVector3D(dir * far + sin(nextAngle) * right * farRadius
                                  + cos(nextAngle) * up * farRadius));

        if (i % 3 == 0)
        {
            verts.push_back(QVector3D(0, 0, 0));
            verts.push_back(curVert);
        }
    }
}


void MLightActor::generateArrowGeometry(
    const QVector3D &magnitude, QVector<QVector3D> &vertices,
    QVector<uint32_t> &indices, const int &resolution, const float &radius,
    const float &tipLengthRatio, const float &tipRadiusRatio,
    const bool &interleaveNormal)
{
    // if normals are interleaved then 1 vertes is actually made up of 2 indices
    uint32_t indexOffset = vertices.size() / (interleaveNormal ? 2 : 1);

    QVector3D dir = magnitude;
    dir.normalize();

    float length = magnitude.length();
    float tipLength = length * tipLengthRatio;

    QVector3D bi1 = QVector3D(dir.z(), dir.x(), dir.y());
    QVector3D bi2 = QVector3D(dir.y(), dir.z(), dir.x());

    // generate vertices for the rings
    for (int i = 0; i < 3; i++)
    {
        // first ring is at 0 offset, second and third have same offset (but different widths)
        float offset = i == 0 ? 0.f : (length - tipLength);
        for (int j = 0; j < resolution; j++)
        {
            float rad = i == 2 ? radius * tipRadiusRatio : radius;
            float arrowResAngle = 2.f * M_PI / resolution;

            // bi-vector (relative to 'dir') strengths for current vertex
            float a = sin(arrowResAngle * (float)j) * rad;
            float b = cos(arrowResAngle * (float)j) * rad;
            QVector3D n = bi1 * a + bi2 * b;
            QVector3D pos = n + offset * dir;
            vertices.push_back(QVector3D(pos.x(), pos.y(), pos.z()));
            if (interleaveNormal)
            {
                QVector3D normal = QVector3D(n.normalized());
                vertices.push_back(normal);
            }
        }
    }

    // add tip vertex
    QVector3D tipPos = length * dir;
    vertices.push_back(QVector3D(tipPos.x(), tipPos.y(), tipPos.z()));
    if (interleaveNormal) vertices.push_back(dir.normalized());

    // generate indices
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < resolution; j++)
        {
            // TRI 1
            indices.push_back(indexOffset + i * resolution + j);
            // same position on next ring
            indices.push_back(indexOffset + (i + 1) * resolution + j);
            // next ring and one vertex to the right
            indices.push_back(indexOffset + (i + 1) * resolution
                              + ((j + 1) % resolution));

            // TRI 2
            indices.push_back(indexOffset + i * resolution + j);
            // next ring and one vertex to the right
            indices.push_back(indexOffset + (i + 1) * resolution
                              + ((j + 1) % resolution));
            // same ring, adjacent vertex to the right
            indices.push_back(indexOffset + i * resolution
                              + ((j + 1) % resolution));
        }
    }
    for (int j = 0; j < resolution; j++)
    {
        indices.push_back(indexOffset + 2 * resolution + j);
        // tip
        indices.push_back(indexOffset + 3 * resolution);
        // same ring, adjacent vertex to the right
        indices.push_back(indexOffset + 2 * resolution
                          + ((j + 1) % resolution));
    }
}


void MLightActor::initGeometry()
{
    QVector<QVector3D> triVertices;
    QVector<uint32_t> triIndices;

    QVector<QVector3D> lineVertices;

    if (lightType == Directional)
    {
        QVector3D direction = defaultForwardDir * 5.f;
        generateArrowGeometry(direction, triVertices, triIndices, 8, .125f,
                              0.25f, 1.5f, false);
    }
    else if (lightType == Spot)
    {
        generateRoundFrustumGeo(lineVertices, QVector3D(0, 1, 0),
                                QVector3D(0, 0, 1), spotAngleProp * M_PI / 180.f,
                                10);
    }
    else if (lightType == Rect)
    {
        lineVertices.push_back(QVector3D(-QVector3D(0, 0, 1) * rectHProp * 0.5f
                                         - QVector3D(1, 0, 0) * rectWProp * 0.5f));
        lineVertices.push_back(QVector3D(QVector3D(0, 0, 1) * rectHProp * 0.5f
                                         - QVector3D(1, 0, 0) * rectWProp * 0.5f));
        lineVertices.push_back(QVector3D(QVector3D(0, 0, 1) * rectHProp * 0.5f
                                         + QVector3D(1, 0, 0) * rectWProp * 0.5f));
        lineVertices.push_back(QVector3D(-QVector3D(0, 0, 1) * rectHProp * 0.5f
                                         + QVector3D(1, 0, 0) * rectWProp * 0.5f));

        QVector3D direction = defaultForwardDir * 5.f;
        generateArrowGeometry(direction, triVertices, triIndices, 8, .125f,
                              0.25f, 1.5f, false);
    }

    // Upload triangle geometry data to VBO
    const QString triRequestKey =
        "light_actor_triangle_geo_#%1" + QString::number(getID());

    uploadVec3ToVertexBuffer(triVertices, triRequestKey, &triVertexBuffer);

    if (! triIndexBuffer)
    {
        glGenBuffers(1, &triIndexBuffer);
        CHECK_GL_ERROR;
    }

    triIndexBufferSize = triIndices.size();

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, triIndexBuffer);
    CHECK_GL_ERROR;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, triIndexBufferSize * sizeof(uint32_t),
                 triIndices.data(), GL_STATIC_DRAW);
    CHECK_GL_ERROR;

    // Upload line geometry data to VBO
    const QString lineRequestKey =
        "light_actor_line_geo_#" + QString::number(getID());

    lineVertexBufferSize = lineVertices.size();
    uploadVec3ToVertexBuffer(lineVertices, lineRequestKey, &lineVertexBuffer);

    // Pole data
    const QString requestKeyPole =
        "light_actor_pole_geo" + QString::number(getID());

    const int poleRes = 8;
    const float poleRadius = 0.1f;

    QVector<QVector4D> poleVerts(
        poleRes * 2
        + 2); // +2 for bottom and top cap vertex (to close the holes)

    numPoleIndices = poleRes * 6 * 2;

    QVector<uint32_t> poleIndices(numPoleIndices);

    for (uint32_t i = 0; i < poleRes; i++)
    {
        float angle = (float)i / (float)poleRes * 2.f * M_PI;
        float x = cos(angle) * poleRadius;
        float y = sin(angle) * poleRadius;

        // bottom and top pole vertices, bottom vertex has z = -1 because the shader scales z by the actual pole height
        // and the model matrix for this object already translates by that height on z, so move the bottom vertex back done to z = 0
        // by translating with -poleHeight again
        poleVerts[i * 2] = QVector4D(x, y, -1, 1);
        poleVerts[i * 2 + 1] = QVector4D(x, y, 0, 1);

        // Tri 1
        poleIndices[i * 6] = i * 2;
        poleIndices[i * 6 + 2] = ((i + 1) % poleRes) * 2;
        poleIndices[i * 6 + 1] = ((i + 1) % poleRes) * 2 + 1;

        // Tri 2
        poleIndices[i * 6 + 3] = ((i + 1) % poleRes) * 2 + 1;
        poleIndices[i * 6 + 4] = i * 2 + 1;
        poleIndices[i * 6 + 5] = i * 2;
    }

    // close bottom and top hole
    poleVerts[2 * poleRes] = QVector4D(0, 0, -1, 1);
    poleVerts[2 * poleRes + 1] = QVector4D(0, 0, 0, 1);

    uint32_t indexOffset = poleRes * 6;

    for (uint32_t i = 0; i < poleRes; i++)
    {
        // Bottom Tri
        poleIndices[indexOffset + i * 6] = i * 2;
        poleIndices[indexOffset + i * 6 + 1] = 2 * poleRes;
        poleIndices[indexOffset + i * 6 + 2] = ((i + 1) % poleRes) * 2;
        // Top Tri
        poleIndices[indexOffset + i * 6 + 3] = i * 2 + 1;
        poleIndices[indexOffset + i * 6 + 5] = ((i + 1) % poleRes) * 2 + 1;
        poleIndices[indexOffset + i * 6 + 4] = 2 * poleRes + 1;
    }

    uploadVec4ToVertexBuffer(poleVerts, requestKeyPole, &poleVertexBuffer);

    if (! poleIndexBuffer)
    {
        glGenBuffers(1, &poleIndexBuffer);
        CHECK_GL_ERROR;
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, poleIndexBuffer);
    CHECK_GL_ERROR;
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, poleIndices.size() * sizeof(uint32_t),
                 poleIndices.data(), GL_STATIC_DRAW);
    CHECK_GL_ERROR;
}


void MLightActor::reloadSprite() const
{
    Met3D::MSystemManagerAndControl *sysMC =
            Met3D::MSystemManagerAndControl::getInstance();

    QString path = sysMC->getMet3DHomeDir().absoluteFilePath(
            "resources/icons/lights/" + LIGHT_ACTOR_SPRITES[lightType]);

    sprite->setImage(path);
}


void MLightActor::spawnPole()
{
    QVector3D position = transformComponent->getPosition();

    // add one default pole with each light
    poleActor->addPole(QPointF(position.x(), position.y()), false);
    poleActor->getPole(0)->positionProp.registerValueCallback([=]()
    {
        poleActor->getPole(0)->positionProp.suppressValueEvent(true);
        QPointF pos = poleActor->getPolePosition(0);
        transformComponent->setPositionX(pos.x());
        transformComponent->setPositionY(pos.y());
        poleActor->getPole(0)->positionProp.suppressValueEvent(false);
    });
}


void MLightActor::updateProperties()
{
    // Delete properties that are currently not needed.
    if (lightType != Spot)
    {
        spotAngleProp.setHidden(true);
    }
    if (lightType != Rect)
    {
        rectSizeProp.setHidden(true);
    }

    if (transformComponent)
    {
        transformComponent->toggleProperties(lightType != Point);
    }

    // Add properties for the current light type
    switch (lightType)
    {
    case Point:
        break;
    case Spot:
        spotAngleProp.setHidden(false);
        break;
    case Rect:
        rectSizeProp.setHidden(false);
        break;
    default:
        break;
    }
}

} // namespace Met3D
