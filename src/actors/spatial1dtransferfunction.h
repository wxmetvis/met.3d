/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSPATIAL1DTRANSFERFUNCTION_H
#define MSPATIAL1DTRANSFERFUNCTION_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "gxfw/transferfunction.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mstringproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mcolorproperty.h"

class MGLResourcesManager;
class MSceneViewGLWidget;

namespace Met3D
{

/**
  @brief MSpatial1DTransferFunction represents a texturebar, providing both a
  2D-texture that can be used as a lookup table by actors to map scalar values
  to colours, and the geometric representation of the texturebar to be
  drawn into the scene.

  The user can control the mapping scalar value to colour value as well as
  geometric properties of the rendered texturebar (position, size, labelling).

  To allow the user more flexibility in the use of the texture, it is possible
  to choose either to use the texture as it is or to use one or more of its
  channels as an alpha map and set the colour to a constant value. Since the
  user might use a black and white texture with black representing the structure
  and white the transparent part, it is possible to invert the alpha value used.

  To simplify loading textures and since GL_TEXTURE_2D_ARRAY only allows
  textures of the same size, it is only allow to load a set of textures with
  all having the same size. In a different approach one could implement to ask
  the user to scale the textures to the same size if loaded with different
  sizes, but this could lead to unexpected behaviour and confusion.

  To achieve the best result it is advised to use a set of textures in which the
  sparser textures are part of the denser textures.
  */
class MSpatial1DTransferFunction : public MTransferFunction
{
public:
    explicit MSpatial1DTransferFunction(QObject *parent = nullptr);
    ~MSpatial1DTransferFunction() override;

    static QString staticActorType()
    { return "Transfer function scalar to texture"; }

    static QString staticIconFileName() { return "texturemap.png"; }

    void loadConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    void setValueSignificantDigits(int significantDigits) override;

    void setValueStep(float step) override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "TransferFunction1DSpatialTexture"; }

    int getNumLevels() const { return numLevels; }

    bool getClampMaximum() const { return clampMaximumProp; }

    double getInterpolationRange() const { return interpolationRangeProp; }

    bool getInvertAlpha() const { return invertAlphaProp; }

    bool getUseConstantColour() const { return useConstantColourProp; }

    QColor getConstantColour() { return constantColourProp; }

    float getTextureScale() const { return (float)textureScaleProp; }

    float getTextureAspectRatio() const
    { return (float)currentTextureHeight / (float)currentTextureWidth; }

    enum AlphaBlendingMode {
        AlphaChannel = 0,
        RedChannel,
        GreenChannel,
        BlueChannel,
        RGBAverage,
        None
    };
    AlphaBlendingMode getAlphaBlendingMode() { return alphaBlendingMode; }

public slots:

    /**
     * Callback, for when the load levels property is clicked.
     */
    void onLoadLevelsProperty();

protected:
    void generateTransferTexture() override;

    /**
      Creates geometry for a box filled with the texture and for tick
      marks, and places labels at the tick marks.
      */
    void generateBarGeometry() override;

    void renderToCurrentContextUiLayer(MSceneViewGLWidget *sceneView) override;

private:
    /**
      @brief Method to create the 2D texture array with mip-mapping containing
      the texture images and uploads the 3D-texture to the GPU and/or store the
      @p level-th image at @p level in the texture.

      Creates a new 2D texture array if @ref tfTexture is nullptr or
      @p recreate is true. Takes the image stored at @p level in
      @ref loadedImages and stores its content in the 2D texture array.
      */
    void generateTransferTexture(int level, bool recreate);

    void loadImagesFromPaths(const QStringList& pathList);

    /**
     * Format the given list of image paths to a human readable string displayed
     * in the UI.
     * @param list The path list.
     * @return The single string listing all paths from the list.
     */
    static QString formatPathList(const QStringList &list);

    /**
     * Get the list of image paths from a string previously generated by
     * @c formatPathList.
     * @param formattedString The formatted path string.
     * @return The list of paths.
     */
    static QStringList pathsFromFormattedString(const QString &formattedString);

    QVector<QImage> loadedImages;

    // General properties.

    // Properties related to texture levels.
    MProperty            levelsPropertiesSubGroup;
    int                  numLevels;
    MButtonProperty      loadLevelsImagesProperty;
    MStringProperty      pathToLoadedImagesProperty;
    QVector<QString>     pathsToLoadedImages;
    MBoolProperty        useMirroredRepeat;


    // Properties related to value range.
    MBoolProperty clampMaximumProp;
    // Defines scalar range of interpolated texture.
    MSciDoubleProperty interpolationRangeProp;

    // Properties related to alpha blending.
    MProperty alphaBlendingGroupProp;
    MEnumProperty alphaBlendingModeProp;
    MBoolProperty invertAlphaProp;
    MBoolProperty useConstantColourProp;
    MColorProperty constantColourProp;
    MBoolProperty useWhiteBgForBarProp;
    AlphaBlendingMode alphaBlendingMode;

    // Properties related to texture scale.
    MProperty textureScaleGroupProp;
    MIntProperty textureScaleDecimalsProp;
    // Scale of texture width with respect in longitude. Texture height is
    // scaled keeping the aspect ratio.
    MDoubleProperty textureScaleProp;

    int         currentTextureWidth;
    int         currentTextureHeight;
};

} // namespace Met3D

#endif // MSPATIAL1DTRANSFERFUNCTION_H
