/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mtextactor.h"

// standard library imports

// related third party imports

// local application imports

namespace Met3D
{
MTextActor::MTextActor()
: MActor(nullptr, false)
{
    setActorType(staticActorType());
    setName(getActorType());
    enablePicking(true);

    labelProps.enabledProp.setHidden(true);

    addTextProp = MButtonProperty("Add text", "Add");
    addTextProp.registerValueCallback(this, &MTextActor::onAddText);
    actorPropertiesSupGroup.addSubProperty(addTextProp);
}


MTextActor::~MTextActor()
= default;


void MTextActor::saveConfigurationHeader(QSettings *settings)
{
    MActor::saveConfigurationHeader(settings);

    settings->beginGroup(getSettingsID());

    int numTextComps = textComponents.size();
    settings->setValue("num_texts", numTextComps);

    settings->endGroup();
}


void MTextActor::loadConfigurationHeader(QSettings *settings)
{
    MActor::loadConfigurationHeader(settings);

    settings->beginGroup(getSettingsID());

    int numTextComps = settings->value("num_texts", 0).toInt();

    for (int i = 0; i < numTextComps; i++)
    {
        addTextComponent();
    }

    settings->endGroup();
}


void MTextActor::onTextChanged()
{
    labels.clear();

    for (const std::shared_ptr<MTextComponent> &comp : textComponents)
    {
        if (!comp->isEnabled()) continue;

        MLabel *label = comp->getLabel();

        if (label == nullptr) continue;

        labels.append(comp->getLabel());
    }

    emitActorChangedSignal();
}


void MTextActor::onAddText()
{
    auto *cmd = new AddTextCommand(this);
    MUndoStack::run(cmd);
}


std::shared_ptr<MTextComponent> MTextActor::addTextComponent()
{
    QString name = "Text %1";
    name = name.arg(textComponents.length() + 1);

    auto comp = addComponentOfType<MTextComponent>(name);

    // Needed to avoid a memory leak with the remove action lambda.
    std::weak_ptr<MTextComponent> weakComp(comp);

    connect(comp.get(), &MTextComponent::componentChanged, this, &MTextActor::onTextChanged);

    QAction *removeAction = new QAction("Remove");
    connect(removeAction, &QAction::triggered, [=]()
    {
        auto removedComp = weakComp.lock();
        auto *cmd = new RemoveTextCommand(this, removedComp);
        MUndoStack::run(cmd);
    });

    comp->getGroupProperty()->addContextMenuAction(removeAction);
    comp->enableDragging(true);

    textComponents.append(comp);

    onTextChanged();

    return comp;
}

bool MTextActor::addTextComponent(const std::shared_ptr<MTextComponent> &comp)
{
    if (!addComponentOfType(comp))
    {
        return false;
    }

    // Needed to avoid a memory leak with the remove action lambda.
    std::weak_ptr<MTextComponent> weakComp(comp);

    connect(comp.get(), &MTextComponent::componentChanged, this, &MTextActor::onTextChanged);

    QAction *removeAction = new QAction("Remove");
    connect(removeAction, &QAction::triggered, [=]()
    {
        auto removedComp = weakComp.lock();
        removeTextComponent(removedComp);
    });

    comp->getGroupProperty()->addContextMenuAction(removeAction);
    comp->enableDragging(true);

    textComponents.append(comp);

    onTextChanged();

    return true;
}


void MTextActor::removeTextComponent(const std::shared_ptr<MTextComponent>& component)
{
    component->disconnect(this);
    textComponents.removeAll(component);
    component->getGroupProperty()->removeContextMenuAction("Remove");
    removeComponentByID(component->getID());

    const QString name = "Text %1";

    // Update index
    for (int i = 0; i < textComponents.length(); i++)
    {
        textComponents[i]->setName(name.arg(i + 1));
    }

    onTextChanged();
}


MTextActor::AddTextCommand::AddTextCommand(MTextActor *actor)
        : QUndoCommand("Add text"),
          actorName(actor->getName())
{

}


void MTextActor::AddTextCommand::undo()
{
    auto *glRM = MGLResourcesManager::getInstance();
    MTextActor *actor = dynamic_cast<MTextActor*>(glRM->getActorByName(actorName));

    if (actor)
    {
        if (textComponent)
        {
            actor->removeTextComponent(textComponent);
        }
        else
        {
            setObsolete(true);
        }
    }
    else
    {
        setObsolete(true);
    }
}


void MTextActor::AddTextCommand::redo()
{
    auto *glRM = MGLResourcesManager::getInstance();
    MTextActor *actor = dynamic_cast<MTextActor*>(glRM->getActorByName(actorName));

    if (actor)
    {
        if (textComponent)
        {
            if (!actor->addTextComponent(textComponent))
            {
                setObsolete(true);
            }
        }
        else
        {
            textComponent = actor->addTextComponent();
        }
    }
    else
    {
        setObsolete(true);
    }
}


MTextActor::RemoveTextCommand::RemoveTextCommand(MTextActor *actor,
                                                 std::shared_ptr<MTextComponent> component)
                                                 : QUndoCommand("Remove text"),
                                                   actorName(actor->getName()),
                                                   textComponent(component)
{
}


void MTextActor::RemoveTextCommand::undo()
{
    auto *glRM = MGLResourcesManager::getInstance();
    MTextActor *actor = dynamic_cast<MTextActor*>(glRM->getActorByName(actorName));

    if (actor)
    {
        if (textComponent)
        {
            actor->addTextComponent(textComponent);
        }
        else
        {
            setObsolete(true);
        }
    }
    else
    {
        setObsolete(true);
    }
}


void MTextActor::RemoveTextCommand::redo()
{
    auto *glRM = MGLResourcesManager::getInstance();
    MTextActor *actor = dynamic_cast<MTextActor*>(glRM->getActorByName(actorName));

    if (actor)
    {
        if (textComponent)
        {
            actor->removeTextComponent(textComponent);
        }
        else
        {
            setObsolete(true);
        }
    }
    else
    {
        setObsolete(true);
    }
}
} // Met3D