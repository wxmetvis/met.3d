/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017      Bianca Tost [+]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef VOLUMEBOXACTOR_H
#define VOLUMEBOXACTOR_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>
#include <QtCore>

// local application imports
#include "gxfw/mactor.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/vertexbuffer.h"
#include "gxfw/boundingbox/boundingbox.h"

class MGLResourcesManager;
class MSceneViewGLWidget;

namespace Met3D
{

/**
  @brief MVolumeBoundingBoxActor draws a bounding box into the scene that
  visualises the limits of a data volume.

  @todo button "get bounding box from actor XY" (checkbox that lists the other
  actors, if one is selected re-set to "choose" or something -- button is not
  possible in properties browser -- that is.. something similar to QtDesigner
  with the toolbuttons?).
  */
class MVolumeBoundingBoxActor : public MActor, public MBoundingBoxInterface
{
public:
    MVolumeBoundingBoxActor();

    ~MVolumeBoundingBoxActor() override;

    static QString staticActorType() { return "Volume bounding box"; }

    static QString staticIconFileName() { return "boundingbox.png"; }

    void reloadShaderEffects() override;

    /**
      Set the colour of the lines.
     */
    void setColour(const QColor& c);

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "VolumeBoundingBoxActor"; }

    void saveConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    QRectF getHorizontalBBox() { return getBBoxConnection()->horizontal2DCoords(); }

    double getBottomPressure_hPa() { return getBBoxConnection()->bottomPressure_hPa(); }

    double getTopPressure_hPa() { return getBBoxConnection()->topPressure_hPa(); }

    void onBoundingBoxChanged() override;

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

private:
    void generateGeometry();

    std::shared_ptr<GL::MShaderEffect> geometryEffect;

    QVector<QVector3D> coordinateSystemVertices;
    GL::MVertexBuffer* coordinateVertexBuffer;
    QVector<QVector3D> axisTicks;
    GL::MVertexBuffer* axisVertexBuffer;

    MFloatProperty tickLengthProp;

    MColorProperty lineColourProp;
};


} // namespace Met3D
#endif // VOLUMEBOXACTOR_H
