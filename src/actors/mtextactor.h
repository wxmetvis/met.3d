/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MTEXTACTOR_H
#define MET_3D_MTEXTACTOR_H

// standard library imports

// related third party imports
#include <QVector>

// local application imports
#include "gxfw/mactor.h"
#include "actors/components/mtextcomponent.h"
#include "gxfw/properties/mbuttonproperty.h"

// forward declarations

namespace Met3D
{

/**
 * An actor that lets the user to add text to the scene.
 */
class MTextActor : public MActor
{
public:
    MTextActor();
    ~MTextActor() override;

    static QString staticActorType() { return "Text"; }
    static QString staticIconFileName() { return "text.png"; }
    static QString staticSettingsID() { return "TextActor"; }

    QString getSettingsID() override { return staticSettingsID(); };
    void reloadShaderEffects() override {};
    void initializeActorResources() override {};
    void saveConfigurationHeader(QSettings *settings) override;
    void loadConfigurationHeader(QSettings *settings) override;

private slots:
    /**
     * Called when any of the displayed text changes.
     */
    void onTextChanged();

    /**
     * Called when a new text component should be added.
     */
    void onAddText();

protected:
    /**
     * Add a new text component to the actor.
     * @return The new text component.
     */
    std::shared_ptr<MTextComponent> addTextComponent();

    /**
     * Add an existing text component to the actor.
     * @param comp The component to add.
     * @return Whether adding the component was successful.
     */
    bool addTextComponent(const std::shared_ptr<MTextComponent> &comp);

    /**
     * Removes the given text component from this actor.
     */
    void removeTextComponent(const std::shared_ptr<MTextComponent>& component);

    /**
     * The text components currently being used by this actor to render text.
     */
    QVector<std::shared_ptr<MTextComponent>> textComponents;

    MButtonProperty addTextProp;

public:
    /**
     * Undo command to add a new text to the text actor.
     */
    class AddTextCommand : public QUndoCommand
    {
    public:
        AddTextCommand(MTextActor *actor);
        ~AddTextCommand() = default;

        void undo() override;
        void redo() override;

    private:
        /**
         * The name of the actor the component was added to.
         * TODO (tv, 20Jan2025): We cannot save a reference to the actor,
         * as the actor might also be deleted through the user.
         */
        QString actorName;

        /**
         * The added text component.
         */
        std::shared_ptr<MTextComponent> textComponent;
    };

    /**
     * Undo comand to remove a text from the text actor.
     */
    class RemoveTextCommand : public QUndoCommand
    {
    public:
        RemoveTextCommand(MTextActor *actor, std::shared_ptr<MTextComponent> component);
        ~RemoveTextCommand() = default;

        void undo() override;
        void redo() override;

    private:
        /**
         * The name of the actor the component was added to.
         * TODO (tv, 20Jan2025): We cannot save a reference to the actor,
         * as the actor might also be deleted through the user.
         */
        QString actorName;

        /**
         * The removed text component. Needed for undo.
         */
        std::shared_ptr<MTextComponent> textComponent;
    };
};

} // Met3D

#endif //MET_3D_MTEXTACTOR_H
