/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020      Kameswarro Modali [*]
**  Copyright 2017      Bianca Tost [+]
**  Copyright 2021      Christoph Neuhauser [+]
**  Copyright 2023-2024 Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "graticuleactor.h"

// standard library imports
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/textmanager.h"
#include "actors/nwphorizontalsectionactor.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MGraticuleActor::MGraticuleActor(MBoundingBoxConnection *boundingBoxConnection,
                                 MNWPHorizontalSectionActor *connectedHSecActor)
    : MMapProjectionSupportingActor(QList<MapProjectionType>()
                                    << MAPPROJECTION_CYLINDRICAL
                                    << MAPPROJECTION_ROTATEDLATLON
                                    << MAPPROJECTION_PROJ_LIBRARY),
      MBoundingBoxInterface(this, MBoundingBoxConnectionType::HORIZONTAL,
                            boundingBoxConnection),
      graticuleVertexBuffer(nullptr),
      coastlineVertexBuffer(nullptr),
      borderlineVertexBuffer(nullptr),
      verticalPosition_hPa(1049.),
      connectedHSecActor(connectedHSecActor)
{
    // Create and initialise properties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    // Only add property group if graticule is not part of a horizontal cross
    // section.
    if (boundingBoxConnection == nullptr)
    {
        // Created graticule actor as standalone actor and thus it needs its
        // own bounding box actor. (As part of e.g. 2D Horizontal Cross-Section
        // it uses the bounding box connected to the Horizontal Cross-Section.)
        insertBoundingBoxProperty(&actorPropertiesSupGroup);
    }

    drawGraticuleProp = MBoolProperty("Graticule", true);
    drawGraticuleProp.setConfigKey("graticule_enabled");
    drawGraticuleProp.setConfigGroup("graticule");
    drawGraticuleProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(drawGraticuleProp);

    graticuleColourProp = MColorProperty("Colour", QColor(Qt::black));
    graticuleColourProp.setConfigKey("graticule_colour");
    graticuleColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    drawGraticuleProp.addSubProperty(graticuleColourProp);

    graticuleThicknessProp = MFloatProperty("Thickness", 1.0f);
    graticuleThicknessProp.setConfigKey("graticule_thickness");
    graticuleThicknessProp.setMinMax(0.0f, 10.0f);
    graticuleThicknessProp.setDecimals(1.0f);
    graticuleThicknessProp.setStep(1.0f);
    graticuleThicknessProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    drawGraticuleProp.addSubProperty(graticuleThicknessProp);

    graticuleLinesProp = MProperty("Graticule lines");
    drawGraticuleProp.addSubProperty(graticuleLinesProp);

    graticuleLongitudesProp = MStringProperty("Longitudes", "[-180.,180.,10.]");
    graticuleLongitudesProp.setConfigKey("graticule_lons");
    graticuleLongitudesProp.setTooltip(
            "Format can be '[from,to,step]' or 'v1,v2,v3,...'.");
    graticuleLinesProp.addSubProperty(graticuleLongitudesProp);

    graticuleLatitudesProp = MStringProperty("Latitudes", "[-90.,90.,5.]");
    graticuleLatitudesProp.setConfigKey("graticule_lats");
    graticuleLatitudesProp.setTooltip(
            "Format can be '[from,to,step]' or 'v1,v2,v3,...'.");
    graticuleLinesProp.addSubProperty(graticuleLatitudesProp);

    graticuleLinesLabelsProp = MProperty("Labels at graticule lines");
    drawGraticuleProp.addSubProperty(graticuleLinesLabelsProp);

    longitudeLabelsProp = MStringProperty("Longitudes", "[-180.,180.,20.]");
    longitudeLabelsProp.setConfigKey("lon_labels");
    longitudeLabelsProp.setTooltip(
            "Format can be '[from,to,step]' or 'v1,v2,v3,...'.");
    graticuleLinesLabelsProp.addSubProperty(longitudeLabelsProp);

    latitudeLabelsProp = MStringProperty("Latitudes", "[-90.,90.,10.]");
    latitudeLabelsProp.setConfigKey("lat_labels");
    latitudeLabelsProp.setTooltip(
            "Format can be '[from,to,step]' or 'v1,v2,v3,...'.");
    graticuleLinesLabelsProp.addSubProperty(latitudeLabelsProp);

    vertexSpacingProp = MPropertyTemplates::GeographicPosition2D("Vertex spacing", QPointF(1., 1.));
    vertexSpacingProp.setConfigKey("vertex_spacing");
    vertexSpacingProp.setDecimals(2);
    drawGraticuleProp.addSubProperty(vertexSpacingProp);

    computeGraticuleProp = MButtonProperty("Re-compute graticule", "Compute");
    computeGraticuleProp.registerValueCallback(this, &MGraticuleActor::onRecomputeGeometry);
    drawGraticuleProp.addSubProperty(computeGraticuleProp);

    drawCoastLinesProp = MBoolProperty("Coast lines", true);
    drawCoastLinesProp.setConfigKey("coast_lines_enabled");
    drawCoastLinesProp.setConfigGroup("coast_lines");
    drawCoastLinesProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(drawCoastLinesProp);

    coastLinesColourProp = MColorProperty("Colour", QColor(Qt::black));
    coastLinesColourProp.setConfigKey("colour");
    coastLinesColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    drawCoastLinesProp.addSubProperty(coastLinesColourProp);

    coastLinesThicknessProp = MFloatProperty("Thickness", 2.0f);
    coastLinesThicknessProp.setMinMax(0.0f, 10.0f);
    coastLinesThicknessProp.setDecimals(1.0f);
    coastLinesThicknessProp.setStep(1.0f);
    coastLinesThicknessProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    drawCoastLinesProp.addSubProperty(coastLinesThicknessProp);

    drawBorderLinesProp = MBoolProperty("Border lines", true);
    drawBorderLinesProp.setConfigKey("border_lines_enabled");
    drawBorderLinesProp.setConfigGroup("border_lines");
    drawBorderLinesProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(drawBorderLinesProp);

    borderLinesColourProp = MColorProperty("Colour", QColor(Qt::black));
    borderLinesColourProp.setConfigKey("colour");
    borderLinesColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    drawBorderLinesProp.addSubProperty(borderLinesColourProp);

    borderLinesThicknessProp = MFloatProperty("Thickness", 1.0f);
    borderLinesThicknessProp.setConfigKey("thickness");
    borderLinesThicknessProp.setMinMax(0.0f, 10.0f);
    borderLinesThicknessProp.setDecimals(1.0f);
    borderLinesThicknessProp.setStep(1.0f);
    borderLinesThicknessProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    drawBorderLinesProp.addSubProperty(borderLinesThicknessProp);

    actorPropertiesSupGroup.addSubProperty(mapProjectionPropertiesSubGroup);

    mapProjectionTypesProp.registerValueCallback([=]()
    {
        updateMapProjectionProperties();
        if (suppressActorUpdates()) return;

        emit projectionChangedSignal();
        generateGeometry();
        emitActorChangedSignal();
    });

    rotatedNorthPoleProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;

        emit projectionChangedSignal();
        if (mapProjection == MAPPROJECTION_ROTATEDLATLON)
        {
            generateGeometry();
            emitActorChangedSignal();
        }
    });

    projLibraryApplyProp.registerValueCallback([=]()
    {
        if (suppressActorUpdates()) return;

        emit projectionChangedSignal();
        if (mapProjection == MAPPROJECTION_PROJ_LIBRARY)
        {
            generateGeometry();
            emitActorChangedSignal();
        }
    });

    // Default vertical position is at 1049 hPa.
    setVerticalPosition(1049.);

    auto labelCallbacks = [=]()
    {
        if (suppressActorUpdates()) return;
        generateGeometry();
        emitActorChangedSignal();
    };

    labelProps.enabledProp.registerValueCallback(labelCallbacks);
    labelProps.fontSizeProp.registerValueCallback(labelCallbacks);
    labelProps.fontColourProp.registerValueCallback(labelCallbacks);
    labelProps.enableBBoxProp.registerValueCallback(labelCallbacks);
    labelProps.bboxColourProp.registerValueCallback(labelCallbacks);
}


MGraticuleActor::~MGraticuleActor()
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MGraticuleActor::saveConfiguration(QSettings *settings)
{
    MMapProjectionSupportingActor::saveConfiguration(settings);

    settings->beginGroup(MGraticuleActor::getSettingsID());

    // Only save bounding box if the graticule is directly connected to it.
    // Otherwise graticule is part of a horizontal cross-section actor and thus
    // its bounding box is handled via the horizontal cross-section actor.
    if (getBBoxConnection()->getActor() == this)
    {
        MBoundingBoxInterface::saveConfiguration(settings);
    }

    settings->endGroup();
}


void MGraticuleActor::loadConfiguration(QSettings *settings)
{
    MMapProjectionSupportingActor::loadConfiguration(settings);

    settings->beginGroup(MGraticuleActor::getSettingsID());

    // Only load bounding box if the graticule is directly connected to it.
    // Otherwise graticule is part of a horizontal cross-section actor and thus
    // its bounding box is handled via the horizontal cross-section actor.
    if (getBBoxConnection()->getActor() == this)
    {
        MBoundingBoxInterface::loadConfiguration(settings);
    }

    settings->endGroup();

    // Update geometry with loaded configuration.
    if (isInitialized())
    {
        generateGeometry();
        if (connectedHSecActor) connectedHSecActor->updateMapProjectionVectorCorrectionField();
    }
}


void MGraticuleActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MMapProjectionSupportingActor::loadConfiguration(settings);

    settings->beginGroup(MGraticuleActor::getSettingsID());

    // Only load bounding box if the graticule is directly connected to it.
    // Otherwise graticule is part of a horizontal cross-section actor and thus
    // its bounding box is handled via the horizontal cross-section actor.
    if (getBBoxConnection()->getActor() == this)
    {
        MBoundingBoxInterface::loadConfiguration(settings);
    }

    graticuleLongitudesProp = settings->value("graticuleLongitudes")
                                      .toString();
    graticuleLatitudesProp = settings->value("graticuleLatitudes")
                                     .toString();
    longitudeLabelsProp = settings->value("graticuleLongitudeLabels")
                                  .toString();
    latitudeLabelsProp = settings->value("graticuleLatitudeLabels")
                                 .toString();
    vertexSpacingProp = settings
            ->value("vertexSpacing", QPointF(1., 1.)).toPointF();

    QColor defaultColour(0.0, 0.0, 0.0);

    // Compatibility for old session files containing a single colour option
    if (settings->contains("colour"))
    {
        defaultColour = settings->value("colour").value<QColor>();
    }

    drawGraticuleProp = settings->value("drawGraticule", true).toBool();
    graticuleColourProp = settings->value("graticuleColour", defaultColour)
                                  .value<QColor>();
    graticuleThicknessProp = (float) settings->value("graticuleThickness", 1.0)
                                             .toDouble();
    drawCoastLinesProp = settings->value("drawCoastLines", true).toBool();
    coastLinesColourProp = settings->value("coastLinesColour", defaultColour)
                                   .value<QColor>();
    coastLinesThicknessProp = (float) settings
            ->value("coastLinesThickness", 2.0).toDouble();
    drawBorderLinesProp = settings->value("drawBorderLines", true).toBool();
    borderLinesColourProp = settings->value("borderLinesColour", defaultColour)
                                    .value<QColor>();
    borderLinesThicknessProp = (float) settings
            ->value("borderLinesThickness", 1.0).toDouble();

    settings->endGroup();

    // Update geometry with loaded configuration.
    if (isInitialized())
    {
        generateGeometry();
        if (connectedHSecActor) connectedHSecActor->updateMapProjectionVectorCorrectionField();
    }
}


#define SHADER_VERTEX_ATTRIBUTE 0

void MGraticuleActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");
    shaderProgram->compileFromFile_Met3DHome("src/glsl/simple_coloured_geometry.fx.glsl");
}


void MGraticuleActor::setVerticalPosition(double pressure_hPa)
{
    // NOTE that the vertical position cannot be set by the user. Hence no
    // property is set here. No redraw is triggered.
    verticalPosition_hPa = pressure_hPa;

    for (MLabel* label : labels) label->anchor.setZ(pressure_hPa);
}


void MGraticuleActor::setColour(const QColor& c)
{
    graticuleColourProp = c;
    coastLinesColourProp = c;
    borderLinesColourProp = c;
}


void MGraticuleActor::onBoundingBoxChanged()
{
    labels.clear();

    if (suppressActorUpdates())
    {
        return;
    }
    // Switching to no bounding box only needs a redraw, but no recomputation
    // because it disables rendering of the actor.
    if (getBBoxConnection()->getBoundingBox() != nullptr)
    {
        generateGeometry();
    }
    emitActorChangedSignal();
}


void MGraticuleActor::onRecomputeGeometry()
{
    if (suppressActorUpdates()) return;
    generateGeometry();
    emitActorChangedSignal();
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MGraticuleActor::initializeActorResources()
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    bool loadShaders = false;

    // Load shader program if the returned program is new.
    loadShaders |= glRM->generateEffectProgram("graticule_shader", shaderProgram);

    if (loadShaders) reloadShaderEffects();

    generateGeometry();
}


void MGraticuleActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    // Draw nothing if no bounding box is available.
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }
    shaderProgram->bindProgram("IsoPressure");

    // Set uniform and attribute values.
    shaderProgram->setUniformValue("mvpMatrix",
                                   *(sceneView->getModelViewProjectionMatrix()));
    float worldZ = sceneView->worldZfromPressure(verticalPosition_hPa);
    shaderProgram->setUniformValue("worldZ", worldZ);

    if (drawGraticuleProp)
    {
        shaderProgram->setUniformValue("colour", graticuleColourProp);

        // Draw graticule.
        graticuleVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
        sceneView->setLineWidth(graticuleThicknessProp / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

        glMultiDrawArrays(GL_LINE_STRIP,
                          graticuleStartIndices.constData(),
                          graticuleVertexCount.constData(),
                          graticuleStartIndices.size()); CHECK_GL_ERROR;
    }

    if (drawCoastLinesProp)
    {
        shaderProgram->setUniformValue("colour", coastLinesColourProp);

        // Draw coastlines.
        coastlineVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);
        CHECK_GL_ERROR;

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
        sceneView->setLineWidth(coastLinesThicknessProp / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

        glMultiDrawArrays(GL_LINE_STRIP,
                          coastlineStartIndices.constData(),
                          coastlineVertexCount.constData(),
                          coastlineStartIndices.size()); CHECK_GL_ERROR;
    }

    if (drawBorderLinesProp)
    {
        shaderProgram->setUniformValue("colour", borderLinesColourProp);

        // Draw borderlines.
        borderlineVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);
        CHECK_GL_ERROR;

        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
        sceneView->setLineWidth(borderLinesThicknessProp / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

        glMultiDrawArrays(GL_LINE_STRIP,
                          borderlineStartIndices.constData(),
                          borderlineVertexCount.constData(),
                          borderlineStartIndices.size()); CHECK_GL_ERROR;
    }

    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MGraticuleActor::generateGeometry()
{
    // The method requires a bounding box to correctly generate geometry.
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }

    LOG4CPLUS_INFO(mlog, "Generating graticule and coast-/borderline "
                          "geometry...");

    // Generate graticule geometry.
    // ============================
    QVector<float> graticuleLongitudes = parseFloatRangeString(graticuleLongitudesProp);
    QVector<float> graticuleLatitudes = parseFloatRangeString(graticuleLatitudesProp);
    QVector2D graticuleSpacing(vertexSpacingProp);

    // Instantiate utility class for geometry handling.
    MGeometryHandling geo;
    geo.initProjProjection(projLibraryString);
    geo.initRotatedLonLatProjection(rotatedNorthPoleProp);

    // Generate graticule geometry.
    QVector<QPolygonF> graticule = geo.generate2DGraticuleGeometry(
                graticuleLongitudes, graticuleLatitudes, graticuleSpacing);

    // Heuristic value to eliminate line segments that after projection cross
    // the map domain due to a connection that after projection is invalid.
    // This happens when a line segment that connects two closeby vertices after
    // projection leaves e.g. the eastern side of the map and re-enters on the
    // western side (or vice versa).
    // A value of "20deg" seems to work well with global data from NaturalEarth.
    // NOTE (mr, 28Oct2020): This is "quick&dirty" workaround. The correct
    // approach to this would be to perform some sort of test that checks if
    // the correct connection of the two vertices after projection should cross
    // map boundaries, in such as case the segment needs to be broken up.
    // Another approach to this was previously implemented in BT's code, see
    // Met.3D version 1.6 or earlier.
    double rotatedGridMaxSegmentLength_deg = 20.;

    // Get bounding box in which the graticule will be displayed.
    QRectF bbox = getBBoxConnection()->horizontal2DCoords();

    // Project and clip the generated graticule geometry.
    graticule = projectAndClipGeometry(
                &geo, graticule, bbox, rotatedGridMaxSegmentLength_deg);

    // Convert list of polygons to vertex list for OpenGL rendering.
    QVector<QVector2D> verticesGraticule;
    graticuleStartIndices.clear();
    graticuleVertexCount.clear();
    geo.flattenPolygonsToVertexList(graticule,
                                    &verticesGraticule,
                                    &graticuleStartIndices,
                                    &graticuleVertexCount);

    // Make sure that "glResourcesManager" is the currently active context,
    // otherwise glDrawArrays on the VBO generated here will fail in any other
    // context than the currently active. The "glResourcesManager" context is
    // shared with all visible contexts, hence modifying the VBO there works
    // fine.
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    glRM->makeCurrent();

    // Upload vertex list to vertex buffer.
    const QString graticuleRequestKey = QString("graticule_vertices_actor#")
                                        + QString::number(getID());
    uploadVec2ToVertexBuffer(verticesGraticule, graticuleRequestKey,
                             &graticuleVertexBuffer);


    // Generate graticule labels.
    // ==========================
    MTextManager* tm = glRM->getTextManager();

    // Remove all text labels of the old geometry (MActor method).
    removeAllLabels();

    QVector<float> longitudeLabels = parseFloatRangeString(longitudeLabelsProp);
    QVector<float> latitudeLabels = parseFloatRangeString(latitudeLabelsProp);

    // Label positions are found by creating a "proxy graticule" for each
    // meridian, then after projection and clipping placing the label
    // at the first polygon vertex (i.e. boundary of the bbox).
    for (float lonLabel : longitudeLabels)
    {
        QVector<float> singleLongitudeLabel;
        singleLongitudeLabel << lonLabel;

        QVector<QPolygonF> proxyGraticule =
                geo.generate2DGraticuleGeometry(
                    singleLongitudeLabel, latitudeLabels, graticuleSpacing);

        proxyGraticule = projectAndClipGeometry(
                    &geo, proxyGraticule, bbox, rotatedGridMaxSegmentLength_deg);

        for (QPolygonF proxyPolygon : proxyGraticule)
        {
            QPointF labelPosition = proxyPolygon.first();
            labels.append(tm->addText(
                                  QString("%1%2").arg(lonLabel).arg(
                                  lonLabel >= 0 ? "E" : "W"),
                                  MTextManager::LONLATP,
                                  static_cast<float>(labelPosition.x()),
                                  static_cast<float>(labelPosition.y()),
                                  verticalPosition_hPa,
                                  labelProps.fontSizeProp,
                                  labelProps.fontColourProp,
                                  MTextManager::BASELINECENTRE,
                                  labelProps.enableBBoxProp,
                                  labelProps.bboxColourProp)
                          );
        }
    }

    // Parallels' labels are positioned at the last vertex of each projected
    // clipped polygon.
    for (float latLabel : latitudeLabels)
    {
        QVector<float> singleLatitudeLabel;
        singleLatitudeLabel << latLabel;

        QVector<QPolygonF> proxyGraticule =
                geo.generate2DGraticuleGeometry(
                    longitudeLabels, singleLatitudeLabel, graticuleSpacing);

        proxyGraticule = projectAndClipGeometry(
                    &geo, proxyGraticule, bbox, rotatedGridMaxSegmentLength_deg);

        for (QPolygonF proxyPolygon : proxyGraticule)
        {
            QPointF labelPosition = proxyPolygon.last();
            labels.append(tm->addText(
                                  QString("%1%2").arg(latLabel).arg(
                                  latLabel >= 0 ? "N" : "S"),
                                  MTextManager::LONLATP,
                                  static_cast<float>(labelPosition.x()),
                                  static_cast<float>(labelPosition.y()),
                                  verticalPosition_hPa,
                                  labelProps.fontSizeProp,
                                  labelProps.fontColourProp,
                                  MTextManager::BASELINECENTRE,
                                  labelProps.enableBBoxProp,
                                  labelProps.bboxColourProp)
                          );
        }
    }


    // Read coastline geometry from shapefile.
    // =======================================

    // For projection and clippling to work correctly, we load coastline and
    // borderline geometry on the entire globe, then clip to the bounding
    // box after projection. Performance seems to be accaptable (mr, 28Oct2020).
    QRectF geometryLimits = QRectF(-180., -90., 360., 180.);

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    QString coastfile = sysMC->getApplicationConfigurationValue(
                "geometry_shapefile_coastlines").toString();
    QVector<QPolygonF> coastlines = geo.read2DGeometryFromShapefile(
                expandEnvironmentVariables(coastfile), geometryLimits);

    coastlines = projectAndClipGeometry(
                &geo, coastlines, bbox, rotatedGridMaxSegmentLength_deg);

    QVector<QVector2D> verticesCoastlines;
    coastlineStartIndices.clear();
    coastlineVertexCount.clear();
    geo.flattenPolygonsToVertexList(coastlines,
                                    &verticesCoastlines,
                                    &coastlineStartIndices,
                                    &coastlineVertexCount);

    const QString coastRequestKey = "graticule_coastlines_actor#"
                                    + QString::number(getID());
    uploadVec2ToVertexBuffer(verticesCoastlines, coastRequestKey,
                             &coastlineVertexBuffer);


    // Read borderline geometry from shapefile.
    // ========================================
    QString borderfile = sysMC->getApplicationConfigurationValue(
                "geometry_shapefile_borderlines").toString();
    QVector<QPolygonF> borderlines = geo.read2DGeometryFromShapefile(
                expandEnvironmentVariables(borderfile), geometryLimits);

    borderlines = projectAndClipGeometry(
                &geo, borderlines, bbox, rotatedGridMaxSegmentLength_deg);

    QVector<QVector2D> verticesBorderlines;
    borderlineStartIndices.clear();
    borderlineVertexCount.clear();
    geo.flattenPolygonsToVertexList(borderlines,
                                    &verticesBorderlines,
                                    &borderlineStartIndices,
                                    &borderlineVertexCount);

    const QString borderRequestKey = "graticule_borderlines_actor#"
                                     + QString::number(getID());
    uploadVec2ToVertexBuffer(verticesBorderlines, borderRequestKey,
                             &borderlineVertexBuffer);

    LOG4CPLUS_INFO(mlog, "Graticule and coast-/borderline geometry was generated.");
}


QVector<QPolygonF> MGraticuleActor::projectAndClipGeometry(
        MGeometryHandling *geo,
        QVector<QPolygonF> geometry, QRectF bbox,
        double rotatedGridMaxSegmentLength_deg)
{
    // Projection-dependent operations.
    if (mapProjection == MAPPROJECTION_CYLINDRICAL)
    {
        // Cylindrical projections may display bounding boxed outside the
        // -180..180 degrees range, hence enlarge the geometry if required.
        geometry = geo->enlargeGeometryToBBoxIfNecessary(geometry, bbox);
    }
    else if (mapProjection == MAPPROJECTION_PROJ_LIBRARY)
    {
        geometry = geo->geographicalToProjectedCoordinates(geometry);
    }
    else if (mapProjection == MAPPROJECTION_ROTATEDLATLON)
    {
        geometry = geo->geographicalToRotatedCoordinates(geometry);
        geometry = geo->splitLineSegmentsLongerThanThreshold(
                    geometry, rotatedGridMaxSegmentLength_deg);
    }

    // Clip line geometry to the bounding box that is rendered.
    geometry = geo->clipPolygons(geometry, bbox);

    return geometry;
}

} // namespace Met3D
