/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2023 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2017 Bianca Tost [+]
**  Copyright 2017      Michael Kern [+]
**  Copyright 2023-2024 Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "movablepoleactor.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "data/structuredgrid.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "trajectories/pointsource/pointgeneratorinterfacesettings.h"
#include "trajectories/pointsource/pointsourcefactory.h"
#include "gxfw/properties/mpropertytemplates.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MMovablePoleActor::MMovablePoleActor(bool addAddPoleProperty)
    : MActor(),
      MPointGeneratorInterface(MPointGeneratorInterfaceType::ACTOR),
      axisVertexBuffer(nullptr),
      renderMode(RenderModes::TUBES),
      movementEnabled(true),
      useBlinnPhongShading(true),
      poleVertexBuffer(nullptr),
      highlightPole(-1),
      offsetPickPositionToHandleCentre(QVector2D(0., 0.))
{
    enablePicking(true);

    setActorType(staticActorType());
    setName(getActorType());

    // Create and initialise properties for the GUI.
    // ===============================================
    lineColourProp = MColorProperty("Colour", QColor(0, 104, 139, 255));
    lineColourProp.setConfigKey("colour");
    lineColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(lineColourProp);

    renderModeProp = MEnumProperty("Render mode");
    renderModeProp.setConfigKey("render_mode");
    renderModeProp.setEnumNames({"Tubes", "Lines"});
    renderModeProp.setValue(static_cast<int>(renderMode));
    renderModeProp.registerValueCallback([=]()
    {
        renderMode = static_cast<RenderModes>(renderModeProp.value());
        tubeRadiusProp.setEnabled(renderMode == RenderModes::TUBES);

        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(renderModeProp);

    tubeRadiusProp = MFloatProperty("Tube radius", 0.06f);
    tubeRadiusProp.setConfigKey("tube_radius");
    tubeRadiusProp.setMinMax(0.01f, 5.0f);
    tubeRadiusProp.setDecimals(2);
    tubeRadiusProp.setStep(0.01f);
    tubeRadiusProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(tubeRadiusProp);

    enableTicksProp = MBoolProperty("Tick marks", true);
    enableTicksProp.setConfigKey("tick_marks_enabled");
    enableTicksProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(enableTicksProp);

    tickLengthProp = MFloatProperty("Tick length", 0.8);
    tickLengthProp.setConfigKey("tick_length_enabled");
    tickLengthProp.setMinMax(0.05f, 20.0f);
    tickLengthProp.setDecimals(2);
    tickLengthProp.setStep(0.05);
    tickLengthProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    enableTicksProp.addSubProperty(tickLengthProp);

    ticksOnRightSideProp = MBoolProperty("Flip ticks", true);
    ticksOnRightSideProp.setConfigKey("flip_ticks");
    ticksOnRightSideProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    enableTicksProp.addSubProperty(ticksOnRightSideProp);

    tickPressureThresholdProp = MFloatProperty("Tick interval threshold", 100.f);
    tickPressureThresholdProp.setConfigKey("tick_interval_threshold");
    tickPressureThresholdProp.setMinMax(20.0f, 1050.0f);
    tickPressureThresholdProp.setDecimals(1);
    tickPressureThresholdProp.setStep(10.0f);
    tickPressureThresholdProp.setSuffix(" hPa");
    tickPressureThresholdProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    enableTicksProp.addSubProperty(tickPressureThresholdProp);

    tickIntervalAboveThresholdProp = MFloatProperty("Tick interval above threshold", 100.0f);
    tickIntervalAboveThresholdProp.setConfigKey("tick_interval_above_threshold");
    tickIntervalAboveThresholdProp.setSuffix(" hPa");
    tickIntervalAboveThresholdProp.setMinMax(10.0f, 300.0f);
    tickIntervalAboveThresholdProp.setDecimals(1);
    tickIntervalAboveThresholdProp.setStep(10);
    tickIntervalAboveThresholdProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    enableTicksProp.addSubProperty(tickIntervalAboveThresholdProp);

    tickIntervalBelowThresholdProp = MFloatProperty("Tick interval below threshold", 10.0f);
    tickIntervalBelowThresholdProp.setConfigKey("tick_interval_below_threshold");
    tickIntervalBelowThresholdProp.setSuffix(" hPa");
    tickIntervalBelowThresholdProp.setMinMax(10.0f, 300.0f);
    tickIntervalBelowThresholdProp.setDecimals(1);
    tickIntervalBelowThresholdProp.setStep(10);
    tickIntervalBelowThresholdProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    enableTicksProp.addSubProperty(tickIntervalBelowThresholdProp);

    labelSpacingProp = MIntProperty("Label every n-th tick", 3);
    labelSpacingProp.setConfigKey("label_every_nth_tick");
    labelSpacingProp.setMinMax(1, 100);
    labelSpacingProp.setStep(1);
    labelSpacingProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    enableTicksProp.addSubProperty(labelSpacingProp);

    individualPoleHeightsEnabledProp = MBoolProperty("Specify vertical extent per pole", false);
    individualPoleHeightsEnabledProp.setConfigKey("specify_vertical_extent_per_pole");
    individualPoleHeightsEnabledProp.registerValueCallback([=]()
    {
        for (const auto& pole : poles)
        {
            pole->verticalExtentProp.groupProp.setHidden(!individualPoleHeightsEnabledProp);
        }
        onGeometryChanged();
    });
    actorPropertiesSupGroup.addSubProperty(individualPoleHeightsEnabledProp);

    verticalExtentProp.bottomProp.setConfigKey("vertical_extent_bottom_pressure");
    verticalExtentProp.bottomProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);

    verticalExtentProp.topProp.setConfigKey("vertical_extent_top_pressure");
    verticalExtentProp.topProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);

    actorPropertiesSupGroup.addSubProperty(verticalExtentProp.groupProp);

    if (addAddPoleProperty)
    {
        addPoleProp = MButtonProperty("Add pole", "Add");
        addPoleProp.registerValueCallback([=]()
        {
            if (addPoleProp.isEnabled())
            {
                generatePole();
                onGeometryChanged();
            }
        });
        actorPropertiesSupGroup.addSubProperty(addPoleProp);
    }

    labelProps.fontSizeProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    labelProps.fontColourProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    labelProps.enableBBoxProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    labelProps.bboxColourProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);

    MPointSourceFactory::getInstance()
            ->registerPointGeneratingInterface<MPoleActorPointGeneratorSettings>(
                    this);
}


MMovablePoleActor::~MMovablePoleActor()
{
    // Release all vertex buffers.
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    const QString axisRequestKey = "axis_vertices_actor#"
                                   + QString::number(getID());
    glRM->releaseAllGPUItemReferences(axisRequestKey);
    const QString poleRequestKey = "pole_vertices_actor#"
                                   + QString::number(getID());
    glRM->releaseAllGPUItemReferences(poleRequestKey);

    MPointSourceFactory::getInstance()
        ->deregisterPointGeneratingInterface(this);
}


MovablePole::MovablePole(MActor *actor)
{
    if (actor == nullptr) return;

    groupProperty = MProperty("Pole");

    positionProp = MPropertyTemplates::GeographicPosition2D("Position", {});
    groupProperty.addSubProperty(positionProp);

    verticalExtentProp = MPropertyTemplates::VerticalExtentHPa();
    verticalExtentProp.topProp.setConfigKey("vertical_extent_top_pressure");
    verticalExtentProp.bottomProp.setConfigKey("vertical_extent_bottom_pressure");
    groupProperty.addSubProperty(verticalExtentProp.groupProp);

    removePoleProp = MButtonProperty("Remove pole", "Remove");
    groupProperty.addSubProperty(removePoleProp);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE 0

void MMovablePoleActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");

    beginCompileShaders(2);

    compileShadersFromFileWithProgressDialog(
                simpleGeometryEffect,
                "src/glsl/simple_geometry_generation.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                positionSpheresShader,
                "src/glsl/trajectory_positions.fx.glsl");

    endCompileShaders();
}


void MMovablePoleActor::removeAllPoles()
{
    for (const std::shared_ptr<MovablePole>& pole : poles)
    {
        actorPropertiesSupGroup.removeSubProperty(pole->groupProperty);
    }

    poles.clear();

    if (isInitialized())
    {
        generateGeometry();
        emitActorChangedSignal();
    }
}


void MMovablePoleActor::addPole(QPointF pos, bool addToProperties)
{
    std::shared_ptr<MovablePole> pole = std::make_shared<MovablePole>(this);
    pole->positionProp = pos;
    poles.append(pole);

    actorPropertiesSupGroup.addSubProperty(pole->groupProperty);
    pole->groupProperty.setHidden(!addToProperties);
    registerPoleCallbacks(pole);

    if (isInitialized())
    {
        generateGeometry();
        emitActorChangedSignal();
    }
}


void MMovablePoleActor::addPole(const QVector3D& lonlatP, bool addToProperties)
{
    std::shared_ptr<MovablePole> pole = std::make_shared<MovablePole>(this);
    pole->positionProp = lonlatP.toPointF();
    pole->verticalExtentProp.topProp = lonlatP.z();
    pole->verticalExtentProp.bottomProp = 1050.0f;
    poles.append(pole);

    actorPropertiesSupGroup.addSubProperty(pole->groupProperty);
    pole->groupProperty.setHidden(!addToProperties);
    registerPoleCallbacks(pole);

    if (isInitialized())
    {
        generateGeometry();
        emitActorChangedSignal();
    }
}


std::shared_ptr<MovablePole> MMovablePoleActor::getPole(int index) const
{
    if (index < 0 || index >= poles.size()) return {};

    return poles[index];
}


void MMovablePoleActor::saveConfigurationHeader(QSettings *settings)
{
    settings->beginGroup(MMovablePoleActor::getSettingsID());

    settings->setValue("numPoles", poles.size());

    // Even though the new property system can automatically save the property tree,
    // the poles need to be saved and loaded manually due to them being runtime user created.
    for (int i = 0; i < poles.size(); i++)
    {
        std::shared_ptr<MovablePole> pole = poles.at(i);

        QString poleID = QString("polePosition_%1").arg(i);

        settings->setValue(poleID, pole->positionProp.value());
        settings->setValue(QString("poleBottomPressure_%1").arg(i),
                           pole->verticalExtentProp.bottomProp.value());
        settings->setValue(QString("poleTopPressure_%1").arg(i),
                           pole->verticalExtentProp.topProp.value());
    }

    settings->endGroup();
}


void MMovablePoleActor::loadConfigurationHeader(QSettings *settings)
{
    settings->beginGroup(MMovablePoleActor::getSettingsID());

    int numPoles = settings->value("numPoles", 0).toInt();

    // Clear current poles.
    for (const std::shared_ptr<MovablePole> &ps : poles)
    {
        actorPropertiesSupGroup.removeSubProperty(ps->groupProperty);
    }
    poles.clear();

    // Read saved poles.
    for (int i = 0; i < numPoles; i++)
    {
        QString poleID = QString("polePosition_%1").arg(i);
        QPointF pos = settings->value(poleID).toPointF();
        float pBot = settings->value(QString("poleBottomPressure_%1").arg(i), 1050.).toFloat();
        float pTop = settings->value(QString("poleTopPressure_%1").arg(i), 100.).toFloat();

        addPole(pos);
        std::shared_ptr<MovablePole> pole = poles.last();

        pole->verticalExtentProp.groupProp.setHidden(!individualPoleHeightsEnabledProp);

        pole->verticalExtentProp.bottomProp = pBot;
        pole->verticalExtentProp.topProp = pTop;
    }

    settings->endGroup();
}


void MMovablePoleActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MActor::loadConfigurationPrior_V_1_14(settings);

    settings->beginGroup(MMovablePoleActor::getSettingsID());

    enableTicksProp = settings->value("tickEnable", true).toBool();

    tickLengthProp = static_cast<float>(settings->value("tickLength", 0.8)
                                                .toDouble());

    ticksOnRightSideProp = settings->value("ticksOnRightSide", true).toBool();

    lineColourProp = settings->value("lineColour", QColor(Qt::black))
                             .value<QColor>();

    renderModeProp = settings
            ->value("renderMode", int(RenderModes::TUBES)).toInt();

    tubeRadiusProp = static_cast<float>(settings->value("tubeRadius", 0.1)
                                                .toDouble());

    individualPoleHeightsEnabledProp = settings
            ->value("individualPoleHeightsEnabled").toBool();

    verticalExtentProp.bottomProp = static_cast<float>(settings
            ->value("bottomPressure", 1050.).toDouble());

    verticalExtentProp.topProp = static_cast<float>(settings
            ->value("topPressure", 100.).toDouble());

    tickIntervalAboveThresholdProp = static_cast<float>(settings
            ->value("tickIntervalAboveThreshold", 100.).toDouble());

    tickIntervalBelowThresholdProp = static_cast<float>(settings
            ->value("tickIntervalBelowThreshold", 10.).toDouble());

    tickPressureThresholdProp = static_cast<float>(settings
            ->value("tickIntervalThreshold", 100.).toDouble());

    labelSpacingProp = settings->value("labelSpacing", 3).toInt();

    int numPoles = settings->value("numPoles", 0).toInt();

    // Clear current poles.
    for (const std::shared_ptr<MovablePole> &ps : poles)
    {
        actorPropertiesSupGroup.removeSubProperty(ps->groupProperty);
    }
    poles.clear();

    // Read saved poles.
    for (int i = 0; i < numPoles; i++)
    {
        QString poleID = QString("polePosition_%1").arg(i);
        QPointF pos = settings->value(poleID).toPointF();
        float pBot = settings
                ->value(QString("poleBottomPressure_%1").arg(i), 1050.)
                .toFloat();
        float pTop = settings->value(QString("poleTopPressure_%1").arg(i), 100.)
                             .toFloat();

        addPole(pos);
        std::shared_ptr<MovablePole> pole = poles.last();

        pole->verticalExtentProp.bottomProp.setEnabled(individualPoleHeightsEnabledProp);
        pole->verticalExtentProp.topProp.setEnabled(individualPoleHeightsEnabledProp);

        pole->verticalExtentProp.bottomProp = pBot;
        pole->verticalExtentProp.topProp = pTop;
    }

    settings->endGroup();

    if (isInitialized()) generateGeometry();
}


const QVector<QVector3D>& MMovablePoleActor::getPoleVertices() const
{
    return poleVertices;
}


void MMovablePoleActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    // A) Render vertical axes.
    // ========================

    // Bind shader program.
    switch (renderMode)
    {
        case RenderModes::LINES:
            simpleGeometryEffect->bindProgram("LonLatPLines");
            break;
        case RenderModes::TUBES:
            simpleGeometryEffect->bindProgram("LonLatPTubes");
            break;
    }

    const float tubeRadiusTick = tubeRadiusProp * 0.5f;

    // Set uniform and attribute values.
    simpleGeometryEffect->setUniformValue(
            "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    simpleGeometryEffect->setUniformValue(
            "pToWorldZParams", sceneView->pressureToWorldZParameters());
    simpleGeometryEffect->setUniformValue("tubeRadius", tubeRadiusProp);
    simpleGeometryEffect->setUniformValue("endSegmentOffset", tubeRadiusProp);
    simpleGeometryEffect->setUniformValue("geometryColor", lineColourProp);
    simpleGeometryEffect->setUniformValue("useBlinnPhongShading", useBlinnPhongShading);

    sceneView->bindShadowMap(simpleGeometryEffect, 0);

    poleVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    sceneView->setLineWidth(2.0f / sceneView->getViewportUpscaleFactor());
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
    glDrawArrays(GL_LINES, 0, poleVertices.size()); CHECK_GL_ERROR;

    // Unbind VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;

    // B) Render tick marks and adjust label positions.
    // ================================================

    // Bind shader program.
    switch (renderMode)
    {
        case RenderModes::LINES:
            simpleGeometryEffect->bindProgram("TickLines");
            break;
        case RenderModes::TUBES:
            simpleGeometryEffect->bindProgram("TickTubes");
            break;
    }

    // Set uniform and attribute values.
    simpleGeometryEffect->setUniformValue(
            "pToWorldZParams", sceneView->pressureToWorldZParameters());
    simpleGeometryEffect->setUniformValue(
            "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    simpleGeometryEffect->setUniformValue("geometryColor", lineColourProp);
    simpleGeometryEffect->setUniformValue(
            "pToWorldZParams", sceneView->pressureToWorldZParameters());
    simpleGeometryEffect->setUniformValue("tubeRadius", tubeRadiusTick);
    simpleGeometryEffect->setUniformValue("endSegmentOffset", 0.1f);
    simpleGeometryEffect->setUniformValue("useBlinnPhongShading", useBlinnPhongShading);


    // Offset for the "other end" of the tick line and anchor offset for
    // the labels.
    QVector3D anchorOffset = (ticksOnRightSideProp ? 1 : -1) *
            tickLengthProp * sceneView->getCamera()->getXAxis();

    simpleGeometryEffect->setUniformValue(
            "offsetDirection", anchorOffset);

    // Set label offset; the labels are rendered by the text manager.
    for (int i = 0; i < labels.size(); i++)
    {
        labels[i]->anchorOffset = anchorOffset
                + ((ticksOnRightSideProp ? 1 : -1) * tubeRadiusProp)
                * sceneView->getCamera()->getXAxis();
    }

    // Render tick marks.

    if (enableTicksProp)
    {
        axisVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

    sceneView->setLineWidth(2.0f / sceneView->getViewportUpscaleFactor());
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
    glDrawArrays(GL_POINTS, 0, axisTicks.size()); CHECK_GL_ERROR;

        // Unbind VBO.
        glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    }

    // C) Highlight pole if one is currently dragged.
    // ================================================
    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING)
    {
        return;
    }

    // If the index "highlightPole" is < 0, no waypoint should be highlighted.
    if (sceneView->interactionModeEnabled() && movementEnabled)
    {
        // Bind shader program.
        positionSpheresShader->bindProgram("Normal");

        // Set MVP-matrix and parameters to map pressure to world space in the
        // vertex shader.
        positionSpheresShader->setUniformValue(
                "mvpMatrix",
                *(sceneView->getModelViewProjectionMatrix()));
        positionSpheresShader->setUniformValue(
                "pToWorldZParams",
                sceneView->pressureToWorldZParameters());
        positionSpheresShader->setUniformValue(
                "cameraPosition",
                sceneView->getCamera()->getOrigin());
        positionSpheresShader->setUniformValue(
                "cameraUpDir",
                sceneView->getCamera()->getYAxis());
        positionSpheresShader->setUniformValue(
                "radius", GLfloat(sceneView->getHandleSize()));
        positionSpheresShader->setUniformValue(
                "scaleRadius",
                GLboolean(true));


        // Texture bindings for transfer function for data scalar (1D texture from
        // transfer function class). The data scalar is stored in the vertex.w
        // component passed to the vertex shader.
        positionSpheresShader->setUniformValue(
                "useTransferFunction", GLboolean(false));

        // Bind shadow map to texture unit 1.
        sceneView->bindShadowMap(positionSpheresShader, 1);

        // Bind vertex buffer object.

        poleVertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

        glPolygonMode(GL_FRONT_AND_BACK,
                      renderAsWireFrameProp ? GL_LINE : GL_FILL); CHECK_GL_ERROR;
        sceneView->setLineWidth(1.0f / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

        if (highlightPole >= 0)
        {
            positionSpheresShader->setUniformValue(
                        "constColour", QColor(Qt::red));
            glDrawArrays(GL_POINTS, highlightPole, 1); CHECK_GL_ERROR;
        }

        positionSpheresShader->setUniformValue(
                "constColour", QColor(Qt::white));
        glDrawArrays(GL_POINTS, 0, poleVertices.size()); CHECK_GL_ERROR;


        // Unbind VBO.
        glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    }
}


int MMovablePoleActor::checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                                   float clipX, float clipY)
{
    if (!movementEnabled)
    {
        return -1;
    }

    // Default: No pole has been touched by the mouse. Note: This instance
    // variable is used in renderToCurrentContext; if it is >= 0 the pole
    // with the corresponding index is highlighted.
    highlightPole = -1;

    //! TODO: HACK for pole actor. Should be changed for all movable actors!
    float clipRadius = sceneView->getHandleSize();

    // Loop over all poles and check whether the mouse cursor is inside a
    // circle with radius "clipRadius" around the bottom pole point (in clip
    // space).
    for (int i = 0; i < poleVertices.size(); i++)
    {
        // Compute the world position of the current pole
        QVector3D posPole = poleVertices.at(i);
        posPole.setZ(sceneView->worldZfromPressure(posPole.z()));

        // Obtain the camera position and the view direction
        const QVector3D& cameraPos = sceneView->getCamera()->getOrigin();
        QVector3D viewDir = posPole - cameraPos;

        // Scale the radius (in world space) with respect to the viewer distance
        float radius = static_cast<float>(clipRadius * viewDir.length() / 100.0);

        QMatrix4x4 *mvpMatrixInverted =
                sceneView->getModelViewProjectionMatrixInverted();
        // Compute the world position of the current mouse position
        QVector3D mouseWorldPos = *mvpMatrixInverted * QVector3D(clipX, clipY, 1);

        // Get the ray direction from the camera to the mouse position
        QVector3D l = mouseWorldPos - cameraPos;
        l.normalize();

        // Compute (o - c) // ray origin (o) - sphere center (c)
        QVector3D oc = cameraPos - posPole;
        // Length of (o - c) = || o - c ||
        float lenOC = static_cast<float>(oc.length());
        // Compute l * (o - c)
        float loc = static_cast<float>(QVector3D::dotProduct(l, oc));

        // Solve equation:
        // d = - (l * (o - c) +- sqrt( (l * (o - c))² - || o - c ||² + r² )
        // Since the equation can be solved only if root discriminant is >= 0
        // just compute the discriminant
        float root = loc * loc - lenOC * lenOC + radius * radius;

        // If root discriminant is positive or zero, there's an intersection
        if (root >= 0)
        {
            highlightPole = i;
            QMatrix4x4 *mvpMatrix = sceneView->getModelViewProjectionMatrix();
            QVector3D posPoleClip = *mvpMatrix * posPole;
            offsetPickPositionToHandleCentre = QVector2D(posPoleClip.x() - clipX,
                                     posPoleClip.y() - clipY);
            break;
        }
    } // for (poles)

    return highlightPole;
}


void MMovablePoleActor::addPositionLabel(MSceneViewGLWidget *sceneView,
                                         int handleID, float clipX, float clipY)
{
    double lon = poles[handleID / 2]->positionProp.value().x();
    double lat = poles[handleID / 2]->positionProp.value().y();

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    MTextManager* tm = glRM->getTextManager();
    positionLabel = tm->addText(
            QString("lon:%1, lat:%2").arg(lon, 0, 'f', 2)
                .arg(lat, 0, 'f', 2),
            MTextManager::LONLATP, poleVertices[handleID].x(),
            poleVertices[handleID].y(), poleVertices[handleID].z(),
            labelProps.fontSizeProp,
            labelProps.fontColourProp, MTextManager::MIDDLERIGHT,
            labelProps.enableBBoxProp, labelProps.bboxColourProp);

    // Select an arbitrary z-value to construct a point in clip space that,
    // transformed to world space, lies on the ray passing through the camera
    // and the location on the worldZ==0 plane "picked" by the mouse.
    // (See notes 22-23Feb2012).
    QVector3D mousePosClipSpace = QVector3D(clipX, clipY, 0.);

    // The point p at which the ray intersects the worldZ==0 plane is found by
    // computing the value d in p=d*l+l0, where l0 is a point on the ray and l
    // is a vector in the direction of the ray. d can be found with
    //        (p0 - l0) * n
    //   d = ----------------
    //            l * n
    // where p0 is a point on the worldZ==p2worldZ(pbot) plane and n is the
    // normal vector of the plane.
    //       http://en.wikipedia.org/wiki/Line-plane_intersection

    // To compute l0, the MVP matrix has to be inverted.
    QMatrix4x4 *mvpMatrixInverted =
            sceneView->getModelViewProjectionMatrixInverted();
    QVector3D l0 = *mvpMatrixInverted * mousePosClipSpace;

    // Compute l as the vector from l0 to the camera origin.
    QVector3D cameraPosWorldSpace = sceneView->getCamera()->getOrigin();
    QVector3D l = (l0 - cameraPosWorldSpace);

    // The plane's normal vector simply points upward, the origin in world
    // space is lcoated on the plane.
    QVector3D n = QVector3D(0, 0, 1);
    QVector3D p0 = QVector3D(0, 0, sceneView->worldZfromPressure(
            poleVertices[handleID].z()));

    // Compute the mouse position in world space.
    float d = static_cast<float>(QVector3D::dotProduct(p0 - l0, n)
                                 / QVector3D::dotProduct(l, n));
    QVector3D mousePosWorldSpace = l0 + d * l;

    double weight = computePositionLabelDistanceWeight(sceneView->getCamera(),
                                                       mousePosWorldSpace);
    positionLabel->anchorOffset = -((weight + tubeRadiusProp)
            * sceneView->getCamera()->getXAxis());

    emitActorChangedSignal();
}


void MMovablePoleActor::dragEvent(MSceneViewGLWidget *sceneView,
                                  int handleID, float clipX, float clipY)
{
    if (!movementEnabled)
    {
        return;
    }

    QPointF clipPos{clipX + offsetPickPositionToHandleCentre.x(),
                    clipY + offsetPickPositionToHandleCentre.y()};

    QVector3D p0 = QVector3D(0, 0, sceneView->worldZfromPressure(
            poleVertices[handleID].z()));

    QVector3D mousePosWorldSpace = sceneView->projectClipSpaceToWorld(clipPos, p0, QVector3D(0, 0, 1));

    // Update the coordinates of pole, axis tick marks and labels. Upload new
    // positions to vertex buffers and redraw the scene.

    // One pole has two handles so to get the pole index, divide the handleID by
    // 2 and round down.
    int pole = handleID / 2;
    // Get the smaller index belonging to the pole in a vector containing two
    // elements per pole (e.g. poleVertices, startEndPositions vectors).
    int poleVectorPos = pole * 2;

    poleVertices[poleVectorPos].setX(mousePosWorldSpace.x());
    poleVertices[poleVectorPos].setY(mousePosWorldSpace.y());
    poleVertices[poleVectorPos + 1].setX(mousePosWorldSpace.x());
    poleVertices[poleVectorPos + 1].setY(mousePosWorldSpace.y());

    // Update tick mark positions.
    for (int i = axisTicksFirstLastIndices[poleVectorPos];
         i < axisTicksFirstLastIndices[poleVectorPos + 1]; ++i)
    {
        axisTicks[i].setX(mousePosWorldSpace.x());
        axisTicks[i].setY(mousePosWorldSpace.y());
    }

    const QString poleRequestKey = "pole_vertices_actor#"
            + QString::number(getID());
    // NOTE: needs to be released in destructor.
    uploadVec3ToVertexBuffer(poleVertices, poleRequestKey, &poleVertexBuffer,
                             sceneView);

    const QString axisRequestKey = "axis_vertices_actor#"
            + QString::number(getID());
    // NOTE: needs to be released in destructor.
    uploadVec3ToVertexBuffer(axisTicks, axisRequestKey, &axisVertexBuffer,
                             sceneView);

    // Update label positions.
    for (int i = labelsFirstLastIndices[poleVectorPos];
         i < labelsFirstLastIndices[poleVectorPos + 1]; ++i)
    {
        labels[i]->anchor.setX(mousePosWorldSpace.x());
        labels[i]->anchor.setY(mousePosWorldSpace.y());
    }

    // Only change label if there is one present.
    if (positionLabel != nullptr)
    {
        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        MTextManager *tm = glRM->getTextManager();
        tm->removeText(positionLabel);

        positionLabel = tm->addText(
                QString("lon:%1, lat:%2")
                        .arg(poleVertices[handleID].x(), 0, 'f', 2)
                        .arg(poleVertices[handleID].y(), 0, 'f', 2),
                MTextManager::LONLATP, poleVertices[handleID].x(),
                poleVertices[handleID].y(), poleVertices[handleID].z(),
                labelProps.fontSizeProp,
                labelProps.fontColourProp,
                MTextManager::LOWERRIGHT,
                labelProps.enableBBoxProp,
                labelProps.bboxColourProp);

        float weight = static_cast<float>(computePositionLabelDistanceWeight(
                sceneView->getCamera(),
                mousePosWorldSpace));
        positionLabel->anchorOffset = -((weight + tubeRadiusProp)
                * sceneView->getCamera()->getXAxis());
    }

    enableActorUpdates(false);
    // Update GUI properties.
    poles[pole]->positionProp
               .setUndoableValue(QPointF(mousePosWorldSpace.x(),
                                         mousePosWorldSpace.y()),
                                 dragEventID);
    enableActorUpdates(true);

    // Notify the point generating interface that the points generated by this
    // actor changed by this action.
    pointInterfaceChanged();
    emitActorChangedSignal();
}


void MMovablePoleActor::setMovement(bool enabled)
{
    movementEnabled = enabled;
}


void MMovablePoleActor::setIndividualPoleHeightsEnabled(bool enabled)
{
    individualPoleHeightsEnabledProp = enabled;
}


void MMovablePoleActor::enablePoleProperties(bool enabled)
{
    addPoleProp.setEnabled(enabled);
    individualPoleHeightsEnabledProp.setEnabled(enabled);
    verticalExtentProp.groupProp.setEnabled(enabled);

    for (const auto & pole : poles)
    {
        pole->groupProperty.setEnabled(enabled);
    }
}


void MMovablePoleActor::setTubeRadius(float radius)
{
    tubeRadiusProp = radius;
}


void MMovablePoleActor::setVerticalExtent(float pbot_hPa, float ptop_hPa)
{
    enableActorUpdates(false);
    verticalExtentProp.bottomProp = pbot_hPa;
    enableActorUpdates(true);
    verticalExtentProp.topProp = ptop_hPa;
}


void MMovablePoleActor::setPolePosition(int index, QPointF lonlatPos)
{
    if (index < poles.size())
    {
        poles[index]->positionProp = lonlatPos;
    }
    // Notify the point generating interface that the points generated by this
    // actor changed by this action.
    pointInterfaceChanged();
    emitActorChangedSignal();
}

QPointF MMovablePoleActor::getPolePosition(int index) const
{
    if(index < poles.size() && index < poleVertices.size() * 2) {
        return QPointF(poleVertices[index * 2].x(), poleVertices[index * 2].y());
    }
    return QPointF(0,0);
}

void MMovablePoleActor::setColorProperty(const QColor& color)
{
    lineColourProp = color;
}


void MMovablePoleActor::setTicksOnRightSide(bool rightSide)
{
    ticksOnRightSideProp = rightSide;
}

void MMovablePoleActor::setBlinnPhongShading(bool enable)
{
    useBlinnPhongShading = enable;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MMovablePoleActor::onGeometryChanged()
{
    if (suppressActorUpdates()) return;
    generateGeometry();
    emitActorChangedSignal();
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MMovablePoleActor::initializeActorResources()
{
    generateGeometry();

    bool loadShaders = false;
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    loadShaders |= glRM->generateEffectProgram("ppole_geometry",
                                               simpleGeometryEffect);
    loadShaders |= glRM->generateEffectProgram("ppole_spheres",
                                               positionSpheresShader);

    if (loadShaders) reloadShaderEffects();
}


void MMovablePoleActor::generatePole()
{
    poleCounter++;
    std::shared_ptr<MovablePole> pole = std::make_shared<MovablePole>(this);
    pole->verticalExtentProp.groupProp.setHidden(!individualPoleHeightsEnabledProp);
    poles.append(pole);
    pole->groupProperty.setName(QString("Pole #%1").arg(poleCounter));
    actorPropertiesSupGroup.addSubProperty(pole->groupProperty);

    registerPoleCallbacks(pole);
}


void MMovablePoleActor::generateGeometry()
{
    // A) Update/generate geometry.
    // =====================

    // Clear all poles
    poleVertices.clear();
    // Clear all ticks
    axisTicks.clear();
    axisTicksFirstLastIndices.clear();

    // C) Generate labels.
    // ===================

    // Remove all text labels of the old geometry (MActor method).
    removeAllLabels();
    labelsFirstLastIndices.clear();

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    MTextManager* tm = glRM->getTextManager();

    for (int i = 0; i < poles.size(); ++i)
    {
        std::shared_ptr<MovablePole> pole = poles.at(i);

        QPointF pos = pole->positionProp;
        QVector3D polePos(pos);

        double pBot = 0; double pTop = 0;

        if (individualPoleHeightsEnabledProp)
        {
            pBot = pole->verticalExtentProp.bottomProp;
            pTop = pole->verticalExtentProp.topProp;
        }
        else
        {
            pBot = verticalExtentProp.bottomProp;
            pTop = verticalExtentProp.topProp;
        }

        polePos.setZ(pBot);
        poleVertices.append(polePos); // for pbot
        polePos.setZ(pTop);
        poleVertices.append(polePos); // for ptop

        // Create new axis ticks geometry, according to the pressure limits.
        const double upperTickStep = tickIntervalAboveThresholdProp;

        const double lowerTickStep = tickIntervalBelowThresholdProp;

        const double pressureThreshold = tickPressureThresholdProp;

        const int labelIteration = labelSpacingProp;

        int interval = static_cast<int>((pBot > pressureThreshold) ? upperTickStep : lowerTickStep);
        int p = int(pBot / interval) * interval;
        int counter = 0;

        axisTicksFirstLastIndices.append(axisTicks.size());
        labelsFirstLastIndices.append(labels.size());
        while (p >= pTop)
        {
            axisTicks.append(QVector3D(polePos.x(), polePos.y(), p));

            // Generate labels for every ith tick mark
            if (counter % labelIteration == 0)
            {
                labels.append(tm->addText(
                        QString("%1").arg(p),
                        MTextManager::LONLATP,
                        polePos.x(), polePos.y(), p,
                        labelProps.fontSizeProp,
                        labelProps.fontColourProp,
                        (ticksOnRightSideProp ? MTextManager::MIDDLELEFT : MTextManager::MIDDLERIGHT),
                        labelProps.enableBBoxProp, labelProps.bboxColourProp)
                );
            }

            counter++;
            p -= (p > pressureThreshold) ? upperTickStep : lowerTickStep;
        }
        axisTicksFirstLastIndices.append(axisTicks.size());
        labelsFirstLastIndices.append(labels.size());
    }

    // B) Upload geometry data to VBO.
    // ===============================

    const QString poleRequestKey = "pole_vertices_actor#"
                                   + QString::number(getID());
    // NOTE: needs to be released in destructor.
    uploadVec3ToVertexBuffer(poleVertices, poleRequestKey, &poleVertexBuffer);

    const QString axisRequestKey = "axis_vertices_actor#"
                                   + QString::number(getID());
    // NOTE: needs to be released in destructor.
    uploadVec3ToVertexBuffer(axisTicks, axisRequestKey, &axisVertexBuffer);
    pointInterfaceChanged();
}


MPoints *
MMovablePoleActor::generatePointsFromString(const QString& requestString) const
{
    auto* seedPts = new MPoints();

    // Extract parameters for generating points.
    QStringList reqParameters = requestString.split('/');
    float minP = reqParameters[0].toFloat();
    float pSpacing = reqParameters[1].toFloat();
    float maxP = reqParameters[2].toFloat();
    int numPoles = reqParameters[3].toInt();

    // Every pole is defined by its bottom and top pressure. Generate seed
    // points at every pole, from the bottom to the top, using the spacing.
    for (int i = 0; i < numPoles; i++)
    {
        float lon = reqParameters[4 + 2*i].toFloat();
        float lat = reqParameters[5 + 2*i].toFloat();

        double p = minP;
        while (p <= maxP)
        {
            seedPts->append(QVector3D(lon, lat, p));
            p += pSpacing;
        }
    }

    // Set up the start grid.
    auto startGrid = new MRegularLonLatStructuredPressureGrid(
        seedPts->getPoints().size(), 1, 1);

    for (int p = 0; p < seedPts->getPoints().size(); p++)
    {
        startGrid->setLevel(p, seedPts->getPoints()[p].z());
    }
    seedPts->setSourceGrid(startGrid);

    return seedPts;
}


QString MMovablePoleActor::generateStringForPointGeneration(
    MPointGeneratorInterfaceSettings* uiSrc) const
{
    auto ui = dynamic_cast<MPoleActorPointGeneratorSettings *>(uiSrc);

    double pSpacing = ui->pSpacingProp;
    double pMin = ui->pMinProp;
    double pMax = ui->pMaxProp;

    // Build the request string: minP/pSpacing/maxP/numPoles/...
    QString reqString;
    reqString.append(QString::number(pMin));
    reqString.append("/" + QString::number(pSpacing));
    reqString.append("/" + QString::number(pMax));
    reqString.append("/" + QString::number(poles.size()));

    for (int i = 0; i < poles.size(); i++)
    {
        // ... /lon/lat for each pole.
        QPointF pos = getPolePosition(i);
        reqString.append(QString("/%1/%2").arg(pos.x()).arg(pos.y()));
    }

    return reqString;
}

void MMovablePoleActor::registerPoleCallbacks(const std::shared_ptr<MovablePole>& pole)
{
    pole->positionProp.registerValueCallback(this, &MMovablePoleActor::onGeometryChanged);
    pole->verticalExtentProp.bottomProp.registerValueCallback([=]()
    {
        if (individualPoleHeightsEnabledProp)
        {
            onGeometryChanged();
        }
    });
    pole->verticalExtentProp.topProp.registerValueCallback([=]()
    {
        if (individualPoleHeightsEnabledProp)
        {
            onGeometryChanged();
        }
    });

    auto *removePoleAction = new QAction("Remove");
    auto removePoleCallback = [=]()
    {
        poles.removeOne(pole);
        generateGeometry();

        // Disconnect signals, so that the lambdas are deleted and the shared_ptr
        // can be freed, as it is also stored in the lambdas.
        pole->positionProp.disconnect(this);
        pole->verticalExtentProp.bottomProp.disconnect(this);
        pole->verticalExtentProp.topProp.disconnect(this);
        pole->removePoleProp.disconnect(this);
        removePoleAction->deleteLater();
    };

    pole->removePoleProp.registerValueCallback(this, removePoleCallback);

    connect(removePoleAction, &QAction::triggered, [=](bool checked)
    {
        removePoleCallback();
    });

    pole->groupProperty.addContextMenuAction(removePoleAction);
}


} // namespace Met3D