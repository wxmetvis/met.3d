/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mvramtestactor.h"

// standard library imports
#include <algorithm>

// related third party imports
#include "log4cplus/loggingmacros.h"

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "util/mutil.h"

namespace Met3D
{

MVramTestActor::MVramTestActor()
    : MActor(nullptr, false),
      debugShader(nullptr),
      counter(0)
{
    setActorType(staticActorType());
    setName(getActorType());

    addBrickProp = MButtonProperty("Add data bricks to GPU", "Add");
    addBrickProp.registerValueCallback(this, &MVramTestActor::addBrick);
    actorPropertiesSupGroup.addSubProperty(addBrickProp);

    areBricksResidentProp = MButtonProperty("Check if bricks are resident", "Check");
    areBricksResidentProp.setTooltip("Checks which textures are currently resident in VRAM");
    areBricksResidentProp.registerValueCallback(this, &MVramTestActor::checkResidency);
    actorPropertiesSupGroup.addSubProperty(areBricksResidentProp);

    brickSizeBytesProp = MIntProperty("Brick size", 1024 * 1024 * 1024);
    brickSizeBytesProp.setSuffix(" byte");
    brickSizeBytesProp.setMinMax(0, 1024 * 1024 * 1024);
    actorPropertiesSupGroup.addSubProperty(brickSizeBytesProp);

    numberToAllocateProp = MIntProperty("Number of bricks to allocate", 1);
    numberToAllocateProp.setMinMax(1, 40);
    actorPropertiesSupGroup.addSubProperty(numberToAllocateProp);

    freeBricksProp = MButtonProperty("Free bricks", "free");
    freeBricksProp.registerValueCallback(this, &MVramTestActor::freeBricks);
    actorPropertiesSupGroup.addSubProperty(freeBricksProp);

    numberToFreeProp = MIntProperty("Number of bricks to free", 1);
    numberToFreeProp.setMinMax(1, 40);
    actorPropertiesSupGroup.addSubProperty(numberToFreeProp);

    bindTextureProp = MButtonProperty("Re-bind texture at index", "Bind");
    bindTextureProp.registerValueCallback(this, &MVramTestActor::bindBrick);
    actorPropertiesSupGroup.addSubProperty(bindTextureProp);

    bindIndexProp = MIntProperty("Index of texture to bind", 0);
    bindIndexProp.setMinimum(0);
    bindIndexProp.setMaximum(textures.size());
    actorPropertiesSupGroup.addSubProperty(bindIndexProp);
}


MVramTestActor::~MVramTestActor()
{
    auto glRm = MGLResourcesManager::getInstance();
    for (GL::MTexture* texture : textures)
    {
        glRm->releaseGPUItem(texture);
    }

    for (float* data : dataBricks)
    {
        delete[] data;
    }

    dataBricks.clear();
    brickSizesBytes.clear();
    textures.clear();

    delete debugShader;
}


void MVramTestActor::reloadShaderEffects()
{
    auto vShader = new QGLShader(QGLShader::Vertex, this);
    vShader->compileSourceCode(getVertexShaderCode());

    auto fShader = new QGLShader(QGLShader::Fragment, this);
    fShader->compileSourceCode(getFragmentShaderCode());

    delete debugShader;

    debugShader = new QGLShaderProgram(this);
    debugShader->addShader(vShader);
    debugShader->addShader(fShader);
    debugShader->bindAttributeLocation("vertex", 0);
    debugShader->link();
}


void MVramTestActor::addBrick()
{
    for (int j = 0; j < numberToAllocateProp; j++)
    {
        size_t size_float = sizeof(float);
        size_t num = brickSizeBytesProp / size_float;

        int maxSize;
        glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxSize);

        LOG4CPLUS_DEBUG(mlog, "Max texture dimension size: " << maxSize);
        LOG4CPLUS_DEBUG(mlog, "Current texture dimension x size: " << num);

        size_t x = num;
        size_t y = 1;

        if (num >= (size_t)maxSize)
        {
            y = num / maxSize;
            x = maxSize;

            LOG4CPLUS_DEBUG(mlog, "New texture dimensions: x = " << x << ", y = " << y);
        }

        auto* dataBrick = new float[num];

        for (size_t i = 0; i < num; i++)
        {
            // Fill data with index of brick. That way
            // we can identify the storage bricks in VRAM later.
            dataBrick[i] = (float) counter;
        }

        auto glRM = MGLResourcesManager::getInstance();

        QString textureID = QString("brick_#%1").arg(counter);

        auto texture = new GL::MTexture(textureID, GL_TEXTURE_2D, GL_R32F, (GLsizei)x, (GLsizei) y);

        if (!glRM->tryStoreGPUItem(texture))
        {
            delete texture;
            texture = nullptr;
            delete[] dataBrick;
        }

        if (texture)
        {
            // Increase ref counter
            texture = dynamic_cast<GL::MTexture *>(glRM->getGPUItem(textureID));

            // Bind texture to "random" texture unit to upload the data.
            texture->bindToTextureUnit(0);

                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_GL_ERROR;

            glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, (GLsizei) x, (GLsizei) y, 0, GL_RED, GL_FLOAT, dataBrick); CHECK_GL_ERROR;

            dataBricks.append(dataBrick);
            textures.append(texture);
            bindIndexProp.setMaximum(textures.size());
            brickSizesBytes.append(brickSizeBytesProp);

            LOG4CPLUS_DEBUG(mlog, "Created object " << texture->getTextureObject() << ".");

            // Bind debugging shader that forces the created texture to load into VRAM.
            // This is needed, as unused textures are not always uploaded when creating their storage.
            // We don't actually draw anything, it just binds the created storage to a shader and executes it
            // so that it loads to VRAM.
            debugShader->bind();
            debugShader->setUniformValue("colourValue", QColor(Qt::red));

            glDrawArrays(GL_TRIANGLES, 0, 1); CHECK_GL_ERROR;

            glActiveTexture(GL_TEXTURE0); CHECK_GL_ERROR;
            glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;

            LOG4CPLUS_DEBUG(mlog, "Uploaded " << brickSizeBytesProp << " bytes to the GPU.");
        }
        counter++;
    }
    emitActorChangedSignal();
}


void MVramTestActor::checkResidency()
{
    QVector<GLuint> textureObjects;
    QVector<GLboolean> areResident;

    textureObjects.resize(textures.size());
    areResident.resize(textures.size());

    for (int i = 0; i < textures.size(); i++)
    {
        textureObjects[i] = textures[i]->getTextureObject();
    }

    GLboolean allTexturesResident = glAreTexturesResident(textures.size(), textureObjects.data(), areResident.data());

    for (int i = 0; i < textures.count(); i++)
    {
        if (areResident[i] || allTexturesResident)
        {
            LOG4CPLUS_DEBUG(mlog, "Brick " << i << " (object " <<
                                           textureObjects[i] << ") is resident (true).");
        }
        else
        {
            LOG4CPLUS_DEBUG(mlog, "Brick " << i << " (object " <<
                                           textureObjects[i] << ") is not resident (false).");
        }
    }
}


void MVramTestActor::freeBricks()
{
    int count = std::min(numberToFreeProp.value(), dataBricks.length());

    auto glRM = MGLResourcesManager::getInstance();

    size_t totalFreedMemory = 0;

    for (int i = 0; i < count; i++)
    {
        glRM->releaseGPUItem(textures[i]);
        delete[] dataBricks[i];
        totalFreedMemory += brickSizesBytes[i];
    }
    dataBricks.remove(0, count);
    textures.remove(0, count);
    brickSizesBytes.remove(0, count);
    bindIndexProp.setMaximum(textures.size());

    LOG4CPLUS_DEBUG(mlog, "Removed " << count << " bricks from memory.");
    LOG4CPLUS_DEBUG(mlog, "Freed memory: " << totalFreedMemory << " bytes.");
}


void MVramTestActor::bindBrick()
{
    if (bindIndexProp >= textures.size())
    {
        LOG4CPLUS_ERROR(mlog, "Bind index out of bounds. Index is " <<
                                                                    bindIndexProp
                                                                    << " while max index is "
                                                                    << (textures.size() - 1)
                                                                    << ".");
        return;
    }

    LOG4CPLUS_DEBUG(mlog, "Rebinding texture " << bindIndexProp << ".");

    textures[bindIndexProp]->bindToTextureUnit(0);

    // Bind debugging shader that forces the created texture to load into VRAM.
    // This is needed, as unused textures are not always uploaded when creating their storage.
    // We don't actually draw anything, it just binds the created storage to a shader and executes it
    // so that it loads to VRAM.
    debugShader->bind();
    debugShader->setUniformValue("colourValue", QColor(Qt::red));

    glDrawArrays(GL_TRIANGLES, 0, 1); CHECK_GL_ERROR;

    glActiveTexture(GL_TEXTURE0); CHECK_GL_ERROR;
    glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
}


void MVramTestActor::initializeActorResources()
{
    auto vShader = new QGLShader(QGLShader::Vertex, this);
    vShader->compileSourceCode(getVertexShaderCode());

    auto fShader = new QGLShader(QGLShader::Fragment, this);
    fShader->compileSourceCode(getFragmentShaderCode());

    debugShader = new QGLShaderProgram(this);
    debugShader->addShader(vShader);
    debugShader->addShader(fShader);
    debugShader->bindAttributeLocation("vertex", 0);
    debugShader->link();
}


void MVramTestActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    Q_UNUSED(sceneView);
}


QString MVramTestActor::getVertexShaderCode()
{
    const QString vs = "#version 130\n"
                       "void main(void)\n"
                       "{\n"
                       "    gl_Position = vec4(0, 0, -1, 1);\n"
                       "}\n";

    return vs;
}


QString MVramTestActor::getFragmentShaderCode()
{
    const QString vs = "#version 130\n"
                       "uniform sampler2D tex;\n"
                       "out vec4 fragColour;\n"
                       "void main(void)\n"
                       "{\n"
                       "    fragColour = vec4(texelFetch(tex, ivec2(0), 0).r);\n"
                       "    discard;\n"
                       "}\n";

    return vs;
}

} // Met3D