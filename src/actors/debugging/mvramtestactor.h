/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MVRAMTESTACTOR_H
#define MET_3D_MVRAMTESTACTOR_H

// standard library imports

// related third party imports
#include "GL/glew.h"
#include "QVector"
#include "QGLShaderProgram"

// local application imports
#include "gxfw/mactor.h"
#include "gxfw/gl/texture.h"

namespace Met3D
{

/**
 * A debug only actor that can quickly and precisely allocate GPU resources.
 * It can be used to debug or test VRAM / RAM related issues.
 */
class MVramTestActor : public MActor
{
public:
    MVramTestActor();
    ~MVramTestActor() override;

    void reloadShaderEffects() override;

    static QString staticActorType() { return "VRAM testing actor"; }

    static QString staticIconFileName() { return "vram.png"; }

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "MVramTestActor"; }

public slots:
    /**
     * Allocate a new memory brick in VRAM.
     */
    void addBrick();

    /**
     * Checks the residency of allocated bricks in VRAM.
     */
    void checkResidency();

    /**
     * Frees @c numberToFree amount of bricks from memory.
     */
    void freeBricks();

    /**
     * Rebinds a specific brick at the @c bindIndex.
     */
    void bindBrick();

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

private:
    static QString getVertexShaderCode();
    static QString getFragmentShaderCode();

    // Shader that forces a texture to load into VRAM.
    QGLShaderProgram *debugShader;

    QVector<float*> dataBricks;
    QVector<size_t> brickSizesBytes;
    QVector<GL::MTexture*> textures;

    MButtonProperty addBrickProp;
    MButtonProperty areBricksResidentProp;
    MIntProperty brickSizeBytesProp;
    MIntProperty numberToAllocateProp;
    MButtonProperty freeBricksProp;
    MIntProperty numberToFreeProp;
    MButtonProperty bindTextureProp;
    MIntProperty bindIndexProp;
    int counter;
};

} // Met3D

#endif //MET_3D_MVRAMTESTACTOR_H
