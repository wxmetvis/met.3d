/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MIMAGEACTOR_H
#define MET_3D_MIMAGEACTOR_H

// standard library imports

// related third party imports
#include <QImage>
#include <QVector2D>

// local application imports
#include "gxfw/mactor.h"
#include "actors/components/mspritecomponent.h"

namespace Met3D
{

/**
 * An actor that simply shows an image.
 */
class MImageActor : public MActor
{
public:

    MImageActor();

    ~MImageActor() override;

    static QString staticActorType()
    { return "Image"; }

    void reloadShaderEffects() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "imageActor"; }

    static QString staticIconFileName() { return "image.png"; }

    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView, float clipX,
                                    float clipY) override;

    void dragEvent(MSceneViewGLWidget *sceneView, int handleID, float clipX,
                   float clipY) override;

    void releaseEvent(MSceneViewGLWidget *sceneView, int handleID) override;

protected:
    void initializeActorResources() override;

    std::shared_ptr<MSpriteComponent> image;

    // Moving the image interactively.
    bool isDragging = false;
    int currentDragHandleID;
    float mouseDragStartX = 0;
    float mouseDragStartY = 0;
    float posDragStartX = 0;
    float posDragStartY = 0;
};

} // Met3D

#endif //MET_3D_MIMAGEACTOR_H
