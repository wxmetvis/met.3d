/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2023 Marc Rautenhaus [*, previously +]
**  Copyright 2017      Michael Kern [+]
**  Copyright 2023-2024 Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef PRESSUREPOLEACTOR_H
#define PRESSUREPOLEACTOR_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>
#include <QtCore>
#include <QVector2D>

// local application imports
#include "gxfw/mactor.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/vertexbuffer.h"
#include "gxfw/properties/mproperty.h"
#include "gxfw/properties/mpointfproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "trajectories/pointsource/pointgeneratorinterface.h"
#include "gxfw/properties/mpropertytemplates.h"

class MGLResourcesManager;

namespace Met3D
{
class MSceneViewGLWidget;

class MovablePole
{
public:
    explicit MovablePole(MActor *actor = nullptr);
    MProperty groupProperty;
    MPointFProperty positionProp;
    MPropertyTemplates::VerticalExtentHPa verticalExtentProp;
    MButtonProperty removePoleProp;
};

/**
  @brief MMovablePoleActor implements vertical axes ("poles") that are labelled
  and can be interactively moved by the user in interaction mode.
  */
class MMovablePoleActor : public MActor,
                          public MPointGeneratorInterface
{
public:
    /**
     * @param addAddPoleProperty Whether to add properties to the actor properties window.
     */
    explicit MMovablePoleActor(bool addAddPoleProperty = true);
    ~MMovablePoleActor() override;

    static QString staticActorType() { return "Movable poles"; }

    static QString staticIconFileName() { return "pole.png"; }

    void reloadShaderEffects() override;

    void addPole(QPointF pos, bool addToProperties = true);
    void addPole(const QVector3D& lonlatP, bool addToProperties = true);

    /**
     * Get the pole at the given index.
     * @param index The index of the pole.
     * @return The pole, if the index is valid.
     */
    std::shared_ptr<MovablePole> getPole(int index) const;

    void removeAllPoles();

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "PressurePoleActor"; }

    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                    float clipX, float clipY) override;

    void addPositionLabel(MSceneViewGLWidget *sceneView, int handleID,
                          float clipX, float clipY) override;

    /**
      @todo Currently uses the worldZ==0 plane, make this work with the
            worldZ==arbitrary.
     */
    void dragEvent(MSceneViewGLWidget *sceneView,
                   int handleID, float clipX, float clipY) override;

    void saveConfigurationHeader(QSettings *settings) override;

    void loadConfigurationHeader(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    const QVector<QVector3D>& getPoleVertices() const;

    void setMovement(bool enabled);

    void setIndividualPoleHeightsEnabled(bool enabled);

    /**
      Programatically enable/disable the "add pole" property and the properties
      that control position and vertical extent of a pole. Used e.g. by the
      SkewT-Actor that keep a pole as a subactor that should only have exactly
      one pole.
     */
    void enablePoleProperties(bool enabled);

    void setTubeRadius(float radius);

    void setVerticalExtent(float pbot_hPa, float ptop_hPa);

    void setPolePosition(int index, QPointF lonlatPos);

    QPointF getPolePosition(int index) const;

    void setColorProperty(const QColor& color);

    void setTicksOnRightSide(bool rightSide);

    void setBlinnPhongShading(bool enable);

    const QVector<QVector3D>& getAxisTicks() { return axisTicks; }

    /** Implement MPointGeneratorInterface interface. */

    QString getPointInterfaceName() const override { return getName(); }

    QStringList getPointGeneratingSourceBaseRequestKeys() const override
    { return { "POLEACTOR_POINT_SOURCE" }; }

    MPoints *
    generatePointsFromString(const QString& requestString) const override;

    QString generateStringForPointGeneration(
            MPointGeneratorInterfaceSettings* uiSrc) const override;

public slots:
    /**
     * Slot called by a property, that changes the geometry.
     * Will update the geometry and emit the actor changed signal.
     */
    void onGeometryChanged();

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    void generatePole();

    void generateGeometry();

    void registerPoleCallbacks(const std::shared_ptr<MovablePole>& pole);

    std::shared_ptr<GL::MShaderEffect> simpleGeometryEffect;
    std::shared_ptr<GL::MShaderEffect> positionSpheresShader;

    QVector<QVector3D> axisTicks;
    GL::MVertexBuffer* axisVertexBuffer;

    // Contain the first and last indices of the values stored in axisTicks
    // respectively labels vector per pole (pole0First, pole0Last, pole1First,
    // pole1Last, ...).
    QVector<int> axisTicksFirstLastIndices;
    QVector<int> labelsFirstLastIndices;

    MBoolProperty enableTicksProp;
    MFloatProperty tickLengthProp;
    MBoolProperty ticksOnRightSideProp;

    MColorProperty lineColourProp;

    MButtonProperty addPoleProp;

    MFloatProperty tickIntervalAboveThresholdProp;
    MFloatProperty tickIntervalBelowThresholdProp;
    MFloatProperty tickPressureThresholdProp;
    MIntProperty labelSpacingProp;

    MPropertyTemplates::VerticalExtentHPa verticalExtentProp;

    MEnumProperty renderModeProp;
    enum class RenderModes : uint8_t { TUBES = 0, LINES = 1 };
    RenderModes renderMode;

    MFloatProperty tubeRadiusProp;

    MBoolProperty individualPoleHeightsEnabledProp;

    bool movementEnabled;
    bool useBlinnPhongShading;

    QVector<std::shared_ptr<MovablePole>> poles;

    QVector<QVector3D> poleVertices;
    GL::MVertexBuffer* poleVertexBuffer;

    /** This variable stores the ID of a pole to be highlighted. If the value
        is -1, no waypoint will be highlighted. */
    int highlightPole;

    // If the user picks the handle not in its centre, we cannot move the handle
    // by setting the centre point to the mouse position so we need this offset
    // to place the handle relative to the mouse position.
    QVector2D offsetPickPositionToHandleCentre;

    /**
     * The amount of poles generated by this actor.
     */
    int poleCounter = 0;
};


} // namespace Met3D

#endif // PRESSUREPOLEACTOR_H
