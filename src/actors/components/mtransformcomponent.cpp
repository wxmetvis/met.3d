/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022      Luka Elwart
**  Copyright 2023-2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mtransformcomponent.h"

// standard library imports
#include <utility>

// related third party imports

// local application imports
#include "gxfw/mactor.h"
#include "gxfw/msceneviewglwidget.h"
#include "util/intersectionutils.h"

namespace Met3D
{

MTransformComponent::MTransformComponent(MActor *parent, QString name,
                                         bool addLocationProperties,
                                         bool addRotationProperties,
                                         bool addScaleProperties,
                                         QString propertyGroupName)
        : MActorComponent(parent, name),
          scale(1.0, 1.0, 1.0),
          rotation(0.0, 0.0, 0.0),
          isDragging(false),
          activeAxis(0),
          activeMode(TransformationMode::None),
          propertyGroupName(std::move(propertyGroupName)),
          addPositionalProperties(addLocationProperties),
          addRotationalProperties(addRotationProperties),
          addScaleProperties(addScaleProperties),
          gizmoShader(nullptr),
          vertexBuffer(nullptr)
{
    transform = QMatrix4x4();
}


MTransformComponent::~MTransformComponent()
{
    if (!vertexBuffer) return;
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    glRM->releaseGPUItem(vertexBuffer);
}


void MTransformComponent::onEnabled()
{
    transformGroupProp.setEnabled(true);
}


void MTransformComponent::onDisabled()
{
    transformGroupProp.setEnabled(false);
}


void MTransformComponent::initializeResources()
{
    // Check if geometry is already initialized
    if (vertexBuffer) return;

    QVector<QVector4D> gizmoVertices;
    QVector<uint32_t> gizmoIndices;

    // Arrows
    generateArrowGeometry(QVector3D(1, 0, 0) * arrowLength, gizmoVertices,
                          gizmoIndices, 0);
    generateArrowGeometry(QVector3D(0, 1, 0) * arrowLength, gizmoVertices,
                          gizmoIndices, 1);
    generateArrowGeometry(QVector3D(0, 0, 1) * arrowLength, gizmoVertices,
                          gizmoIndices, 2);

    // 2D movement planes
    generatePlaneGeometry(gizmoVertices, gizmoIndices, QVector3D(),
                          QVector3D(1, 0, 0), QVector3D(0, 1, 0),
                          gizmoPlaneSize, gizmoPlaneSize, 3);
    generatePlaneGeometry(gizmoVertices, gizmoIndices, QVector3D(),
                          QVector3D(1, 0, 0), QVector3D(0, 0, 1),
                          gizmoPlaneSize, gizmoPlaneSize, 4);
    generatePlaneGeometry(gizmoVertices, gizmoIndices, QVector3D(),
                          QVector3D(0, 1, 0), QVector3D(0, 0, 1),
                          gizmoPlaneSize, gizmoPlaneSize, 5);

    // 2D circle segments for rotation
    generateQuarterCircleGeometry(
            gizmoVertices, gizmoIndices, QVector3D(), QVector3D(1, 0, 0),
            QVector3D(0, 1, 0), gizmoRotatorInnerRadius,
            gizmoRotatorInnerRadius + gizmoRotatorThickness, gizmoRotatorRes, 3);
    generateQuarterCircleGeometry(
            gizmoVertices, gizmoIndices, QVector3D(), QVector3D(1, 0, 0),
            QVector3D(0, 0, 1), gizmoRotatorInnerRadius,
            gizmoRotatorInnerRadius + gizmoRotatorThickness, gizmoRotatorRes, 4);
    generateQuarterCircleGeometry(
            gizmoVertices, gizmoIndices, QVector3D(), QVector3D(0, 1, 0),
            QVector3D(0, 0, 1), gizmoRotatorInnerRadius,
            gizmoRotatorInnerRadius + gizmoRotatorThickness, gizmoRotatorRes, 5);

    // Upload geometry data to VBO.
    const QString gizmoRequestKey =
            "transform_gizmo_vertices_#%1" + QString::number(getID());

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (!vertexBuffer)
    {
        vertexBuffer = new GL::MVector4DVertexBuffer(gizmoRequestKey,
                                                     gizmoVertices.size());

        if (glRM->tryStoreGPUItem(vertexBuffer))
        {
            vertexBuffer->upload(gizmoVertices);
            vertexBuffer = dynamic_cast<GL::MVector4DVertexBuffer *>(
                    glRM->getGPUItem(gizmoRequestKey));
        }
        else
        {
            delete vertexBuffer;
        }

        glGenBuffers(1, &gizmoIndexBuffer);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gizmoIndexBuffer);
        CHECK_GL_ERROR;
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     (GLsizeiptr) (gizmoIndices.size() * sizeof(uint32_t)),
                     gizmoIndices.data(), GL_STATIC_DRAW);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        indexBufferSize = gizmoIndices.size();
    }
}


void MTransformComponent::reloadShaderEffects(bool recompile)
{
    LOG4CPLUS_INFO(mlog, "Reloading shader programs for transform component...");

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    bool compile = false;

    if (!gizmoShader)
        compile = glRM->generateEffectProgram("gizmoShader", gizmoShader);

    if (compile || recompile)
        gizmoShader->compileFromFile_Met3DHome("src/glsl/transform_gizmo.fx.glsl");
}


void MTransformComponent::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (!sceneView->interactionModeEnabled()
            || sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING) return;

    gizmoShader->bindProgram("gizmoShader");

    QMatrix4x4 realM;
    realM.setToIdentity();
    realM.translate(getPosition());

    // Scale the gizmo in relation to its distance from the camera, so that it always keeps the same screen size.
    realM.scale(sceneView->getCamera()->getOrigin().distanceToPoint(getPosition()) * gizmoScreenScaling);

    QMatrix4x4 realMVP = *(sceneView->getModelViewProjectionMatrix()) * realM;

    gizmoShader->setUniformValue("mvpMatrix", realMVP);
    gizmoShader->setUniformValue("modelMatrix", realM);

    vertexBuffer->attachToVertexAttribute(0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gizmoIndexBuffer);
    CHECK_GL_ERROR;

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    CHECK_GL_ERROR;
    glDrawElements(GL_TRIANGLES, indexBufferSize, GL_UNSIGNED_INT, (void *) 0);
    CHECK_GL_ERROR;
}


int MTransformComponent::checkIntersectionWithHandle(
        MSceneViewGLWidget *sceneView, float clipX, float clipY)
{
    // We are currently dragging a handle
    if (isDragging) return -1;

    activeMode = MTransformComponent::None;

    const QVector3D &camPos = sceneView->getCamera()->getOrigin();

    // Scale the gizmo in relation to its distance from the camera, so that it always keeps the same screen size.
    float viewportScale = camPos.distanceToPoint(getPosition()) * gizmoScreenScaling;
    float arrowNoTipLength = arrowLength - arrowTipLength;

    // Get inverted mvp-matrix for scene
    QMatrix4x4 *mvpMatrixInverted =
            sceneView->getModelViewProjectionMatrixInverted();

    // Calculate mouse world position
    QVector3D mouseWorldPos = *mvpMatrixInverted * QVector3D(clipX, clipY, 1);

    // Get the ray direction from the camera to the mouse position
    QVector3D rayDir = mouseWorldPos - camPos;
    rayDir.normalize();

    QVector3D position = getPosition();

    // trace intersections with gizmo
    float minDist = FLT_MAX;
    uint8_t minAxis = Axis::Undef;

    // 1. trace intersections against arrows
    // loop over all 3 axis
    for (int i = 0; i < 3; i++)
    {
        QVector3D axisDir = QVector3D(i == 0, i == 1, i == 2);
        float cyd = rayCylinderIntersection(
                camPos, rayDir, position, position + arrowNoTipLength * axisDir * viewportScale,
                arrowRadius * viewportScale);
        float cod = rayConeIntersection(
                camPos, rayDir, position + arrowNoTipLength * axisDir * viewportScale,
                position + arrowLength * axisDir * viewportScale, arrowTipRadius * viewportScale, 0.f);

        if (cyd >= 0 && cyd < minDist)
        {
            minDist = cyd;
            minAxis = Axis::X << i; // X is defined is 1u, shifting it by i results in the current iterated axis.
            activeMode = MTransformComponent::Move;
        }

        if (cod >= 0 && cod < minDist)
        {
            minDist = cod;
            minAxis = Axis::X << i;
            activeMode = MTransformComponent::Move;
        }
    }

    // 2. Trace against planes
    float dXY =
            plaIntersectBounded(camPos, rayDir, QVector4D(0, 0, 1, position.z()),
                                position, gizmoPlaneSize * viewportScale); // XY plane
    float dXZ =
            plaIntersectBounded(camPos, rayDir, QVector4D(0, 1, 0, position.y()),
                                position, gizmoPlaneSize * viewportScale); // XZ plane
    float dYZ =
            plaIntersectBounded(camPos, rayDir, QVector4D(1, 0, 0, position.x()),
                                position, gizmoPlaneSize * viewportScale); // YZ plane

    float planeMinDist = FLT_MAX;
    uint8_t planeAxis = 0u;

    if (dXY >= 0 && dXY < FLT_MAX)
    {
        planeMinDist = dXY;
        planeAxis = Axis::X | Axis::Y; // X and Y active
    }

    if (dXZ >= 0 && dXZ < FLT_MAX)
    {
        planeMinDist = dXZ;
        planeAxis = Axis::X | Axis::Z; // X and Z active
    }

    if (dYZ >= 0 && dYZ < FLT_MAX)
    {
        planeMinDist = dYZ;
        planeAxis = Axis::Y | Axis::Z; // Y and Z active
    }

    if (planeMinDist < minDist)
    {
        minDist = planeMinDist;
        minAxis = planeAxis;
        activeMode = TransformationMode::Move;
    }

    // 3. trace against 2D rotation circles
    float rdXY = partCirclePlaneIntersection(
            camPos, rayDir, QVector4D(0, 0, 1, position.z()), QVector3D(0.0, 1.0, 0.0), position,
            gizmoRotatorInnerRadius * viewportScale,
            (gizmoRotatorInnerRadius + gizmoRotatorThickness) * viewportScale, 0.0f, 90.0f); // XY plane
    float rdXZ = partCirclePlaneIntersection(
            camPos, rayDir, QVector4D(0, 1, 0, position.y()), QVector3D(1.0, 0.0, 0.0), position,
            gizmoRotatorInnerRadius * viewportScale,
            (gizmoRotatorInnerRadius + gizmoRotatorThickness) * viewportScale, 0.0f, 90.0f); // XZ plane
    float rdYZ = partCirclePlaneIntersection(
            camPos, rayDir, QVector4D(1, 0, 0, position.x()), QVector3D(0.0, 0.0, 1.0),position,
            gizmoRotatorInnerRadius * viewportScale,
            (gizmoRotatorInnerRadius + gizmoRotatorThickness) * viewportScale, 0.0f, 90.0f); // YZ plane

    float circleMinDist = FLT_MAX;
    uint8_t circleAxis = 0u;

    if (rdXY >= 0 && rdXY < circleMinDist)
    {
        circleMinDist = rdXY;
        circleAxis = Axis::X | Axis::Y;
    }

    if (rdXZ >= 0 && rdXZ < circleMinDist)
    {
        circleMinDist = rdXZ;
        circleAxis = Axis::X | Axis::Z;
    }

    if (rdYZ >= 0 && rdYZ < circleMinDist)
    {
        circleMinDist = rdYZ;
        circleAxis = Axis::Y | Axis::Z;
    }

    if (circleMinDist < minDist)
    {
        minAxis = circleAxis;
        activeMode = TransformationMode::Rotate;
    }

    activeAxis = minAxis;
    return activeAxis != Axis::Undef ? activeAxis : -1;
}


void MTransformComponent::dragEvent(MSceneViewGLWidget *sceneView, int handleID,
                                    float clipX, float clipY)
{
    const QVector3D &camPos = sceneView->getCamera()->getOrigin();

    QMatrix4x4 *mvpMatrixInverted =
            sceneView->getModelViewProjectionMatrixInverted();

    // Compute the world position of the current mouse position
    QVector3D mouseWorldPos = *mvpMatrixInverted * QVector3D(clipX, clipY, 1);

    // Get the ray direction from the camera to the mouse position
    QVector3D rayDir = mouseWorldPos - camPos;
    rayDir.normalize();

    QVector3D position = getPosition();

    if (!isDragging && activeAxis != Axis::Undef)
    {
        isDragging = true;

        QVector3D planeNormal;
        if ((activeAxis == Axis::X || activeAxis == Axis::Y
                || activeAxis == Axis::Z)
                && activeMode == TransformationMode::Move)
        {
            // Find closest intersection with the 2 planes being spanned by the active axis and one of it's bi-axis
            QVector3D aa = getActiveAxis();
            QVector3D bi1 = QVector3D(aa.z(), aa.x(), aa.y());
            QVector3D bi2 = QVector3D(aa.y(), aa.z(), aa.x());

            // select plane with most view direction aligned normal (abs because we do not care for alignment direction)
            planeNormal = abs(QVector3D::dotProduct(bi1, rayDir))
                                  >= abs(QVector3D::dotProduct(bi2, rayDir))
                          ? bi1
                          : bi2;

            initialDragTransform = transform;
        }
        else if (activeAxis == (Axis::X | Axis::Y)
                || activeAxis == (Axis::X | Axis::Z)
                || activeAxis == (Axis::Y | Axis::Z))
        {
            // Movement in 2D plane spanned by both active axis
            // 2 axis are active, select the single one that is not -> that is the normal
            planeNormal = QVector3D((activeAxis & Axis::X) == 0,
                                    (activeAxis & Axis::Y) == 0,
                                    (activeAxis & Axis::Z) == 0);
            initialDragTransform = transform;
        }

        float offset = QVector3D::dotProduct(planeNormal, position);
        activePlane = QVector4D(planeNormal, offset);
        float d = rayPlaneIntersection(camPos, rayDir, activePlane);

        initialMousePosition = camPos + rayDir * d;

        return;
    }
    else if (isDragging)
    {
        if (activeAxis == Axis::Undef)
        {
            isDragging = false;
            return;
        }

        QVector3D delta(0.f, 0.f, 0.f);
        float d = rayPlaneIntersection(camPos, rayDir, activePlane);
        QVector3D intersectPos = camPos + rayDir * d;

        if ((activeAxis == Axis::X || activeAxis == Axis::Y
                || activeAxis == Axis::Z))
        {
            delta = intersectPos - initialMousePosition;
            QVector3D aa = getActiveAxis();
            delta *= aa; // 0 out all components but active axis
        }
        else if (activeAxis == (Axis::X | Axis::Y)
                || activeAxis == (Axis::X | Axis::Z)
                || activeAxis == (Axis::Y | Axis::Z))
        {
            // Movement in 2D plane spanned by both active axis
            delta = intersectPos - initialMousePosition;
        }

        if (activeMode == TransformationMode::Move)
        {
            QVector3D initialPos = initialDragTransform.column(3).toVector3D();
            QVector3D newPos = initialPos + delta;
            transform.setColumn(3, QVector4D(newPos, 1));

            positionProp.setUndoableValue(newPos, dragEventID);
        }
        else if (activeMode == TransformationMode::Rotate)
        {
            QVector3D initialRelPos = initialMousePosition - position;
            QVector3D relPos = intersectPos - position;
            initialRelPos.normalize();
            relPos.normalize();
            float angle = acos(QVector3D::dotProduct(initialRelPos, relPos));

            // if we are in rotate mode then 2 planes are active, normal is the vector with 1 in the unselected axis component
            QVector3D normal =
                    activeAxis == (Axis::X | Axis::Y)
                    ? QVector3D(0, 0, 1)
                    : (activeAxis == (Axis::X | Axis::Z) ? QVector3D(0, 1, 0)
                                                         : QVector3D(1, 0, 0));
            QVector3D cross = QVector3D::crossProduct(initialRelPos, relPos);
            // make rotation counter-/clockwise depending on how the mouse was moved (relPos) relative to 'initialRelPos'
            angle *= QVector3D::dotProduct(cross, normal) >= 0.f ? 1.f : -1.f;
            // to degrees
            angle *= 180.f / M_PI;

            QMatrix4x4 deltaRot = QMatrix4x4();
            deltaRot.rotate(angle, normal);

            QVector4D pos = initialDragTransform.column(3);

            transform = deltaRot * initialDragTransform;

            // We dont want to rotate the position around world origin.
            transform.setColumn(3, pos);

            // Update cached rotation
            rotation.setX(getPitch());
            rotation.setY(getYaw());
            rotation.setZ(getRoll());

            rotationProp.setUndoableValue(rotation, dragEventID);
        }
    }

    emitComponentChangedSignal();
}


void MTransformComponent::releaseEvent(MSceneViewGLWidget *sceneView,
                                       int handleID)
{
    MActorComponent::releaseEvent(sceneView, handleID);
    activeAxis = Axis::Undef;
    activeMode = TransformationMode::None;
    isDragging = false;
}


void MTransformComponent::addProperties(MProperty *groupProp)
{
    transformGroupProp = MProperty(propertyGroupName);
    // We cannot save via the properties here,
    // as that would make the save file incompatible.

    positionProp = MVector3DProperty("Graphics space position", {});
    positionProp.getXEditor()->setName("X");
    positionProp.getYEditor()->setName("Y");
    positionProp.getZEditor()->setName("Z");
    positionProp = getPosition();
    positionProp.registerValueCallback([=]()
    {
        setPosition(positionProp);

        emitComponentChangedSignal();
    });

    transformGroupProp.addSubProperty(positionProp);

    rotationProp = MVector3DProperty("Rotation", {});
    rotationProp.getXEditor()->setName("Pitch");
    rotationProp.getXEditor()->setSuffix(" °");
    rotationProp.getYEditor()->setName("Yaw");
    rotationProp.getYEditor()->setSuffix(" °");
    rotationProp.getZEditor()->setName("Roll");
    rotationProp.getZEditor()->setSuffix(" °");
    rotationProp.setShowSuffixInPreview(true);
    rotationProp = getRotation();
    rotationProp.registerValueCallback([=]()
    {
        setRotation(rotationProp);

        emitComponentChangedSignal();
    });

    transformGroupProp.addSubProperty(rotationProp);

    scaleProp = MVector3DProperty("Scale", {1.0f, 1.0f, 1.0f});
    scaleProp.getXEditor()->setName("Scale x");
    scaleProp.getYEditor()->setName("Scale y");
    scaleProp.getZEditor()->setName("Scale z");
    scaleProp = getScale();
    scaleProp.registerValueCallback([=]()
    {
        setScale(scaleProp);

        emitComponentChangedSignal();
    });

    transformGroupProp.addSubProperty(scaleProp);

    positionProp.setHidden(!addPositionalProperties);
    rotationProp.setHidden(!addRotationalProperties);
    scaleProp.setHidden(!addScaleProperties);

    transformGroupProp.setHidden(!addPositionalProperties && !addRotationalProperties
                                    && !addScaleProperties);

    groupProp->addSubProperty(transformGroupProp);
}


void MTransformComponent::toggleProperties(bool enabled)
{
    transformGroupProp.setEnabled(enabled);
}


void MTransformComponent::removeProperties(MProperty *groupProp)
{
    groupProp->removeSubProperty(transformGroupProp);
}


void MTransformComponent::saveConfiguration(QSettings *settings)
{
    QVector3D pos = getPosition();
    QVector3D sca = getScale();
    QVector3D rot = getRotation();

    settings->beginGroup(getName());

    settings->setValue("positionX", pos.x());
    settings->setValue("positionY", pos.y());
    settings->setValue("positionZ", pos.z());

    settings->setValue("scaleX", sca.x());
    settings->setValue("scaleY", sca.y());
    settings->setValue("scaleZ", sca.z());

    settings->setValue("pitch", rot.x());
    settings->setValue("yaw", rot.y());
    settings->setValue("roll", rot.z());

    settings->endGroup();
}


void MTransformComponent::loadConfiguration(QSettings *settings)
{
    QVector3D pos;
    QVector3D sca;
    QVector3D rot;

    settings->beginGroup(getName());

    pos.setX(settings->value("positionX", 0).toFloat());
    pos.setY(settings->value("positionY", 0).toFloat());
    pos.setZ(settings->value("positionZ", 0).toFloat());

    sca.setX(settings->value("scaleX", 1).toFloat());
    sca.setY(settings->value("scaleY", 1).toFloat());
    sca.setZ(settings->value("scaleZ", 1).toFloat());

    rot.setX(settings->value("pitch", 0).toFloat());
    rot.setY(settings->value("yaw", 0).toFloat());
    rot.setZ(settings->value("roll", 0).toFloat());

    settings->endGroup();

    setPosition(pos);
    setScale(sca);
    setRotation(rot);
}


void MTransformComponent::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    loadConfiguration(settings);
}


QVector3D MTransformComponent::getPosition() const
{
    return transform.column(3).toVector3D();
}


void MTransformComponent::setPosition(const QVector3D &position)
{
    transform.setColumn(3, QVector4D(position, 1));

    positionProp = position;

    emitComponentChangedSignal();
}


void MTransformComponent::setPositionX(const float &x)
{
    transform(0, 3) = x;

    positionProp.setX(x);

    emitComponentChangedSignal();
}


void MTransformComponent::setPositionY(const float &y)
{
    transform(1, 3) = y;

    positionProp.setY(y);

    emitComponentChangedSignal();
}


void MTransformComponent::setPositionZ(const float &z)
{
    transform(2, 3) = z;

    positionProp.setZ(z);

    emitComponentChangedSignal();
}


QVector3D MTransformComponent::getRotation() const
{
    return {getPitch(), getYaw(), getRoll()};
}


float MTransformComponent::getPitch() const
{
    float r32 = transform(2, 1);
    float r22 = transform(1, 1);
    float r12 = transform(0, 1);

    auto pitch = (float) (atan2(r32, sqrt(r12 * r12 + r22 * r22)) / M_PI * 180.0f);
    return pitch;
}


float MTransformComponent::getYaw() const
{
    float r12 = transform(0, 1);
    float r22 = transform(1, 1);

    if (r12 == 0 || r22 == 0)
    {
        // Use cached value to retain old yaw when pitch is 90 or -90 degrees.
        return rotation.y();
    }

    auto yaw = (float) (atan2(-r12, r22) / M_PI * 180.0);

    return yaw;
}


float MTransformComponent::getRoll() const
{
    float r31 = transform(2, 0);
    float r33 = transform(2, 2);

    if (r31 == 0 || r33 == 0)
    {
        // Use cached value to retain old yaw when pitch is 90 or -90 degrees.
        return rotation.z();
    }

    auto roll = (float) (atan2(-r31, r33) / M_PI * 180.0);

    return roll;
}


void MTransformComponent::setRotation(const QVector3D &newRotation)
{
    setRotation(newRotation.x(), newRotation.y(), newRotation.z());
}


void MTransformComponent::setRotation(const float &pitch, const float &yaw,
                                      const float &roll)
{
    QVector4D pos = transform.column(3);

    transform.setToIdentity();

    transform.rotate(yaw, QVector3D(0.0, 0.0, 1.0));
    transform.rotate(pitch, QVector3D(1.0, 0.0, 0.0));
    transform.rotate(roll, QVector3D(0.0, 1.0, 0.0));

    transform.setColumn(3, pos);

    rotation.setX(pitch);
    rotation.setY(yaw);
    rotation.setZ(roll);

    rotationProp = rotation;

    emitComponentChangedSignal();
}


void MTransformComponent::setPitch(const float &pitch)
{
    setRotation(pitch, getYaw(), getRoll());
}


void MTransformComponent::setYaw(const float &yaw)
{
    setRotation(getPitch(), yaw, getRoll());
}


void MTransformComponent::setRoll(const float &roll)
{
    setRotation(getPitch(), getYaw(), roll);
}


QVector3D MTransformComponent::getScale() const
{
    return scale;
}


void MTransformComponent::setScale(const QVector3D &newScale)
{
    this->scale = QVector3D(newScale);

    scaleProp = scale;

    emitComponentChangedSignal();
}


void MTransformComponent::setScale(const float &newScale)
{
    this->scale = QVector3D(newScale, newScale, newScale);

    scaleProp = scale;

    emitComponentChangedSignal();
}


QMatrix4x4 MTransformComponent::getTransformMatrix(const bool getPosition,
                                                   const bool getRotation,
                                                   const bool getScale) const
{
    QMatrix4x4 t;

    QMatrix4x4 s;

    if (getScale)
    {
        s(0, 0) *= this->scale.x();
        s(1, 1) *= this->scale.y();
        s(2, 2) *= this->scale.z();
    }

    if (getRotation)
    {
        t = QMatrix4x4(transform);
    }

    t = s * t;

    if (getPosition)
    {
        t.setColumn(3, transform.column(3));
    }
    else
    {
        t.setColumn(3, QVector4D(0, 0, 0, 1));
    }

    return t;
}


MTransformComponent::TransformationMode
MTransformComponent::getLastManipulationMode() const
{
    return activeMode;
}


void MTransformComponent::generateArrowGeometry(
        const QVector3D &magnitude, QVector<QVector4D> &vertices,
        QVector<uint32_t> &indices, const float &w, const int &resolution,
        const float &radius, const float &tipLengthRatio,
        const float &tipRadiusRatio, const bool &interleaveNormal)
{
    // if normals are interleaved then 1 vertes is actually made up of 2 indices
    uint32_t indexOffset = vertices.size() / (interleaveNormal ? 2 : 1);

    QVector3D dir = magnitude;
    dir.normalize();

    float length = magnitude.length();
    float tipLength = length * tipLengthRatio;

    QVector3D bi1 = QVector3D(dir.z(), dir.x(), dir.y());
    QVector3D bi2 = QVector3D(dir.y(), dir.z(), dir.x());

    // generate vertices for the rings
    for (int i = 0; i < 3; i++)
    {
        // first ring is at 0 offset, second and third have same offset (but different widths)
        float offset = i == 0 ? 0.f : (length - tipLength);
        for (int j = 0; j < resolution; j++)
        {
            float rad = i == 2 ? radius * tipRadiusRatio : radius;
            float arrowResAngle = (float) (2.0f * M_PI / resolution);

            // bi-vector (relative to 'dir') strengths for current vertex
            float a = sin(arrowResAngle * (float) j) * rad;
            float b = cos(arrowResAngle * (float) j) * rad;
            QVector3D n = bi1 * a + bi2 * b;
            QVector3D pos = n + offset * dir;
            vertices.push_back(QVector4D(pos.x(), pos.y(), pos.z(), w));
            if (interleaveNormal)
            {
                QVector4D normal = QVector4D(n.normalized(), 0.f);
                vertices.push_back(normal);
            }
        }
    }

    // add tip vertex
    QVector3D tipPos = length * dir;
    vertices.push_back(QVector4D(tipPos.x(), tipPos.y(), tipPos.z(), w));
    if (interleaveNormal) vertices.push_back(dir.normalized());

    // generate indices
    for (int i = 0; i < 2; i++)
    {
        for (int j = 0; j < resolution; j++)
        {
            // TRI 1
            indices.push_back(indexOffset + i * resolution + j);
            // same position on next ring
            indices.push_back(indexOffset + (i + 1) * resolution + j);
            // next ring and one vertex to the right
            indices.push_back(indexOffset + (i + 1) * resolution
                                      + ((j + 1) % resolution));

            // TRI 2
            indices.push_back(indexOffset + i * resolution + j);
            // next ring and one vertex to the right
            indices.push_back(indexOffset + (i + 1) * resolution
                                      + ((j + 1) % resolution));
            // same ring, adjacent vertex to the right
            indices.push_back(indexOffset + i * resolution
                                      + ((j + 1) % resolution));
        }
    }

    for (int j = 0; j < resolution; j++)
    {
        indices.push_back(indexOffset + 2 * resolution + j);
        // tip
        indices.push_back(indexOffset + 3 * resolution);
        // same ring, adjacent vertex to the right
        indices.push_back(indexOffset + 2 * resolution
                                  + ((j + 1) % resolution));
    }
}


void MTransformComponent::generatePlaneGeometry(
        QVector<QVector4D> &vertices, QVector<uint32_t> &indices,
        const QVector3D &origin, const QVector3D &right, const QVector3D &up,
        const float &sizeRight, const float &sizeUp, const float &w)
{
    QVector3D p00 = origin;
    QVector3D p01 = origin + right * sizeRight;
    QVector3D p10 = origin + up * sizeUp;
    QVector3D p11 = origin + right * sizeRight + up * sizeUp;

    uint32_t curVert = vertices.size();

    vertices.push_back(QVector4D(p00, w));
    vertices.push_back(QVector4D(p01, w));
    vertices.push_back(QVector4D(p10, w));
    vertices.push_back(QVector4D(p11, w));

    // SIDE 1
    // tri 1
    indices.push_back(curVert);
    indices.push_back(curVert + 1);
    indices.push_back(curVert + 3);
    // tri 2
    indices.push_back(curVert);
    indices.push_back(curVert + 3);
    indices.push_back(curVert + 2);

    // SIDE 2
    // tri 1
    indices.push_back(curVert);
    indices.push_back(curVert + 3);
    indices.push_back(curVert + 1);
    // tri 2
    indices.push_back(curVert);
    indices.push_back(curVert + 2);
    indices.push_back(curVert + 3);
}


void MTransformComponent::generateQuarterCircleGeometry(
        QVector<QVector4D> &vertices, QVector<uint32_t> &indices,
        const QVector3D &origin, const QVector3D &right, const QVector3D &up,
        const float &innerRadius, const float &outerRadius, const uint &resolution,
        const float &w)
{
    static const float quarterCircle = 0.5f * M_PI;
    // -1 so we span the whole quarter circle starting at 0° and ending at angle 90°
    float angleStep = quarterCircle / float(resolution - 1u);

    for (uint32_t i = 0; i < resolution; i++)
    {
        uint32_t curVert = vertices.size();
        float angle = angleStep * (float) i;
        QVector3D p = cos(angle) * right + sin(angle) * up;
        vertices.push_back(QVector4D(p * innerRadius, w));
        vertices.push_back(QVector4D(
                p * outerRadius, w));

        // on all but last iteration spawn 2 tris connecting this vertex pair with the next one
        if (i < resolution - 1)
        {
            // SIDE 1
            // tri 1
            indices.push_back(curVert);
            indices.push_back(curVert + 3);
            indices.push_back(curVert + 2);
            // tri 2
            indices.push_back(curVert);
            indices.push_back(curVert + 1);
            indices.push_back(curVert + 3);

            // SIDE 2
            // tri 1
            indices.push_back(curVert);
            indices.push_back(curVert + 2);
            indices.push_back(curVert + 3);
            // tri 2
            indices.push_back(curVert);
            indices.push_back(curVert + 3);
            indices.push_back(curVert + 1);
        }
    }
}


QVector3D MTransformComponent::getActiveAxis() const
{
    return QVector3D((activeAxis & Axis::X) > 0, (activeAxis & Axis::Y) > 0,
                     (activeAxis & Axis::Z) > 0);
}


} // namespace Met3D
