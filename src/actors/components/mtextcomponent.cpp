/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mtextcomponent.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"

namespace Met3D
{
MTextComponent::MTextComponent(MActor *parent, QString &componentName)
        : MActorComponent(parent, componentName),
          label(nullptr),
          coordSystem(MTextManager::CLIPSPACE),
          lineVBO(nullptr)
{
    labelProperties = MPropertyTemplates::Labels(true);
    labelProperties.enabledProp.setName(getName());

    labelTextProp = MStringProperty("Text", "");
    labelTextProp.setConfigKey("text");
    labelTextProp.registerValueCallback(this, &MTextComponent::onLabelChanged);
    labelProperties.enabledProp.insertSubProperty(0, labelTextProp);

    QFont appFont = MGLResourcesManager::getInstance()->getTextManager()->getDefaultFont();
    fontProp = MFontProperty("Font", appFont);
    fontProp.setConfigKey("Font");
    fontProp.registerValueCallback([=]()
    {
        MTextManager *tm = MGLResourcesManager::getInstance()->getTextManager();
        if (!tm->hasLoadedFont(fontProp))
        {
            tm->addFont(fontProp);
        }

        onLabelChanged();
    });
    labelProperties.enabledProp.insertSubProperty(1, fontProp);

    coordinateSystemProp = MEnumProperty("Coordinate space", {"Clip Space", "Graphics Space", "Lon-Lat-P"});
    coordinateSystemProp.setValue(coordSystem);
    coordinateSystemProp.setConfigKey("coordinate_space");
    coordinateSystemProp.registerValueCallback([=]()
    {
        // Prevent position properties of wrong coordinate system to be saved.
        switch (coordSystem)
        {
        case MTextManager::CLIPSPACE:
            labelProperties.enabledProp.removeSubProperty(clipSpacePosProp);
            break;
        case MTextManager::WORLDSPACE:
            labelProperties.enabledProp.removeSubProperty(worldSpacePosProp);
            break;
        case MTextManager::LONLATP:
            labelProperties.enabledProp.removeSubProperty(lonLatPPosProp);
            break;
        }

        coordSystem = static_cast<MTextManager::CoordinateSystem>(coordinateSystemProp.value());

        switch (coordSystem)
        {
        case MTextManager::CLIPSPACE:
            labelProperties.enabledProp.addSubProperty(clipSpacePosProp);
            break;
        case MTextManager::WORLDSPACE:
            labelProperties.enabledProp.addSubProperty(worldSpacePosProp);
            break;
        case MTextManager::LONLATP:
            labelProperties.enabledProp.addSubProperty(lonLatPPosProp);
            break;
        }

        // Used to show a small pole below the text to get a geo reference.
        updateGeometry();

        onLabelChanged();
    });
    labelProperties.enabledProp.addSubProperty(coordinateSystemProp);

    static QStringList anchors = {"Baseline left", "Baseline right",
                                  "Baseline center", "Upper left",
                                  "Upper right", "Upper center", "Lower left",
                                  "Lower right", "Lower center", "Middle left",
                                  "Middle right", "Middle center"};
    anchorProp = MEnumProperty("Anchor", anchors, MTextManager::TextAnchor::MIDDLECENTRE);
    anchorProp.setConfigKey("anchor");
    anchorProp.registerValueCallback(this, &MTextComponent::onLabelChanged);
    labelProperties.enabledProp.addSubProperty(anchorProp);

    // By default, the clip space coordinate system is used.
    clipSpacePosProp = MPropertyTemplates::ViewportPosition2D("Viewport position", QPointF(0, 0));
    clipSpacePosProp.setConfigKey("position");
    clipSpacePosProp.registerValueCallback(this, &MTextComponent::onLabelChanged);
    labelProperties.enabledProp.addSubProperty(clipSpacePosProp);

    lonLatPPosProp = MPropertyTemplates::GeographicPosition3D("Position", {0, 0, 1050.f});
    lonLatPPosProp.setConfigKey("position");
    lonLatPPosProp.registerValueCallback(this, &MTextComponent::onLabelChanged);

    worldSpacePosProp = MVector3DProperty("Position", {0, 0, 0});
    worldSpacePosProp.setConfigKey("position");
    worldSpacePosProp.registerValueCallback(this, &MTextComponent::onLabelChanged);

    labelProperties.enabledProp.setConfigKey("enabled");
    labelProperties.enabledProp.setConfigGroup(getName());
    labelProperties.enabledProp.registerValueCallback(this, &MTextComponent::onLabelChanged);

    labelProperties.fontSizeProp.setConfigKey("font_size");
    labelProperties.fontSizeProp.registerValueCallback(this, &MTextComponent::onLabelChanged);

    labelProperties.fontColourProp.setConfigKey("font_colour");
    labelProperties.fontColourProp.registerValueCallback(this, &MTextComponent::onLabelChanged);

    labelProperties.bboxColourProp.setConfigKey("bbox_colour");
    labelProperties.bboxColourProp.registerValueCallback(this, &MTextComponent::onLabelChanged);

    labelProperties.enableBBoxProp.setConfigKey("bbox_enabled");
    labelProperties.enableBBoxProp.registerValueCallback(this, &MTextComponent::onLabelChanged);
}

MTextComponent::~MTextComponent()
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (glRM == nullptr) return;

    MTextManager* tm = glRM->getTextManager();

    if (tm == nullptr) return;

    if (label != nullptr)
    {
        tm->removeText(label);
    }
}


void MTextComponent::onEnabled()
{
    MActorComponent::onEnabled();
}


void MTextComponent::onDisabled()
{
    MActorComponent::onDisabled();
}


void MTextComponent::onNameChanged()
{
    labelProperties.enabledProp.setName(getName());
    labelProperties.enabledProp.setConfigGroup(getName());
}



void MTextComponent::initializeResources()
{
    updateGeometry();
}


void MTextComponent::reloadShaderEffects(bool recompile)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    bool compile = false;

    if (!lineShader)
        compile = glRM->generateEffectProgram("lineShader", lineShader);

    if (compile || recompile)
    {
        lineShader->compileFromFile_Met3DHome("src/glsl/simple_coloured_geometry.fx.glsl");
    }
}


void MTextComponent::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (coordSystem == MTextManager::CLIPSPACE) return;
    if (!sceneView->interactionModeEnabled()) return;
    if (!labelProperties.enabledProp) return;

    if (coordSystem == MTextManager::WORLDSPACE)
    {
        lineShader->bindProgram("WorldSpace");
        lineShader->setUniformValue(
                "mvpMatrix",
                *(sceneView->getModelViewProjectionMatrix()));
        CHECK_GL_ERROR;
        lineShader->setUniformValue("colour", QColor(0, 50, 200));
        CHECK_GL_ERROR;
    }
    else
    {
        lineShader->bindProgram("Pressure");
        lineShader->setUniformValue(
                "mvpMatrix",
                *(sceneView->getModelViewProjectionMatrix()));
        CHECK_GL_ERROR;
        lineShader->setUniformValue(
                "pToWorldZParams",
                sceneView->pressureToWorldZParameters());
        CHECK_GL_ERROR;
        lineShader->setUniformValue("colour", QColor(0, 50, 200));
        CHECK_GL_ERROR;
    }

    lineVBO->attachToVertexAttribute(0);
    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
    sceneView->setLineWidth(3 / sceneView
            ->getViewportUpscaleFactor());
    CHECK_GL_ERROR;

    glDrawArrays(GL_LINE_STRIP, 0, 2);
    CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    CHECK_GL_ERROR;
}


int MTextComponent::checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                                float clipX, float clipY)
{
    if (draggingEnabled)
    {
        if (label != nullptr)
        {
            MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
            MTextManager* tm = glRM->getTextManager();

            QPointF currentClipSpacePos;
            QVector3D tempClipSpace;

            switch(coordSystem)
            {
            case MTextManager::CLIPSPACE:
                currentClipSpacePos = clipSpacePosProp.value();
                break;
            case MTextManager::WORLDSPACE:
                tempClipSpace = sceneView->worldToClipSpace(worldSpacePosProp);
                currentClipSpacePos = {tempClipSpace.x(), tempClipSpace.y()};
                break;
            case MTextManager::LONLATP:
                tempClipSpace = sceneView->lonLatPToClipSpace(lonLatPPosProp);
                currentClipSpacePos = {tempClipSpace.x(), tempClipSpace.y()};
                break;
            }

            QRectF clipSpaceBounds = tm->getLabelClipSpaceBounds(sceneView, label);
            if (clipSpaceBounds.contains(clipX, clipY))
            {
                draggingOffset = QPointF(clipX, clipY) - currentClipSpacePos;
                return getID();
            }
        }
    }

    return MActorComponent::checkIntersectionWithHandle(sceneView, clipX,
                                                        clipY);
}


void MTextComponent::dragEvent(MSceneViewGLWidget *sceneView, int handleID,
                               float clipX, float clipY)
{
    MActorComponent::dragEvent(sceneView, handleID, clipX, clipY);

    if (handleID != static_cast<int>(getID())) return;

    if (draggingEnabled && label != nullptr)
    {
        QPointF clipSpacePos = QPointF(clipX, clipY) - draggingOffset;
        QVector3D worldSpacePos;

        switch(coordSystem)
        {
        case MTextManager::CLIPSPACE:
            clipSpacePosProp.setUndoableValue(clipSpacePos, dragEventID);
            break;
        case MTextManager::WORLDSPACE:
            worldSpacePos = sceneView->projectClipSpaceToWorld(clipSpacePos, worldSpacePosProp, QVector3D(0, 0, 1));
            worldSpacePosProp.setUndoableValue(worldSpacePos, dragEventID);
            break;
        case MTextManager::LONLATP:
            worldSpacePos = lonLatPPosProp.value();
            worldSpacePos.setZ(sceneView->worldZfromPressure(lonLatPPosProp.z()));
            worldSpacePos = sceneView->projectClipSpaceToWorld(clipSpacePos, worldSpacePos, QVector3D(0, 0, 1));
            worldSpacePos.setZ(sceneView->pressureFromWorldZ(worldSpacePos.z()));
            lonLatPPosProp.setUndoableValue(worldSpacePos, dragEventID);
            break;
        }

    }
}


void MTextComponent::addProperties(MProperty *groupProp)
{
    groupProp->addSubProperty(labelProperties.enabledProp);
}


void MTextComponent::toggleProperties(bool enable)
{
    MActorComponent::toggleProperties(enable);

    labelProperties.enabledProp.setEnabled(enable);
}


void MTextComponent::removeProperties(MProperty *groupProp)
{
    MActorComponent::removeProperties(groupProp);

    groupProp->removeSubProperty(labelProperties.enabledProp);
}


void MTextComponent::setText(const QString &text)
{
    labelTextProp = text;
}


void MTextComponent::setFontSize(int size)
{
    labelProperties.fontSizeProp = size;
}


void MTextComponent::setFontColour(const QColor &colour)
{
    labelProperties.fontColourProp = colour;
}


void MTextComponent::enableBBox(bool enable)
{
    labelProperties.enableBBoxProp = enable;
}


void MTextComponent::setBBoxColour(const QColor &colour)
{
    labelProperties.bboxColourProp = colour;
}


void MTextComponent::setClipSpacePosition(const QPointF &pos)
{
    if (coordSystem != MTextManager::CLIPSPACE) return;

    clipSpacePosProp = pos;
}


void MTextComponent::setLonLatPPosition(const QVector3D &pos)
{
    if (coordSystem != MTextManager::LONLATP) return;

    lonLatPPosProp = pos;
}


void MTextComponent::setWorldSpacePosition(const QVector3D &pos)
{
    if (coordSystem != MTextManager::WORLDSPACE) return;

    worldSpacePosProp = pos;
}


void MTextComponent::setCoordinateSystem(
        const MTextManager::CoordinateSystem &coordinateSystem)
{
    coordinateSystemProp = coordinateSystem;
}


void MTextComponent::setAnchor(const MTextManager::TextAnchor &anchor)
{
    anchorProp = anchor;
}


void MTextComponent::enableDragging(bool enable)
{
    draggingEnabled = enable;
}


MLabel *MTextComponent::getLabel() const
{
    return label;
}


void MTextComponent::onLabelChanged()
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (glRM == nullptr) return;

    MTextManager* tm = glRM->getTextManager();

    if (tm == nullptr) return;

    if (label != nullptr)
    {
        tm->removeText(label);
    }

    float x = 0;
    float y = 0;
    float z = 0;

    switch (coordSystem)
    {

    case MTextManager::CLIPSPACE:
        x = static_cast<float>(clipSpacePosProp.x());
        y = static_cast<float>(clipSpacePosProp.y());
        break;
    case MTextManager::WORLDSPACE:
        x = static_cast<float>(worldSpacePosProp.x());
        y = static_cast<float>(worldSpacePosProp.y());
        z = static_cast<float>(worldSpacePosProp.z());
        break;
    case MTextManager::LONLATP:
        x = static_cast<float>(lonLatPPosProp.x());
        y = static_cast<float>(lonLatPPosProp.y());
        z = static_cast<float>(lonLatPPosProp.z());
        break;
    }

    auto anchor = static_cast<MTextManager::TextAnchor>(anchorProp.value());

    if (labelProperties.enabledProp)
    {
        label = tm->addText(labelTextProp,
                            fontProp.value().family(),
                            coordSystem,
                            x, y, z,
                            labelProperties.fontSizeProp,
                            labelProperties.fontColourProp,
                            anchor,
                            labelProperties.enableBBoxProp,
                            labelProperties.bboxColourProp);
    }
    else
    {
        label = nullptr;
    }

    updateGeometry();

    emitComponentChangedSignal();
}


void MTextComponent::updateGeometry()
{
    if (coordSystem == MTextManager::CLIPSPACE) return;

    if (!lineVBO)
    {
        QString id = QString("text_line_vbo_%1").arg(getID());
        lineVBO = new GL::MVector3DVertexBuffer(id, 2);
        if (MGLResourcesManager::getInstance()->tryStoreGPUItem(lineVBO))
        {
            lineVBO = dynamic_cast<GL::MVector3DVertexBuffer *>(MGLResourcesManager::getInstance()->getGPUItem(id));
        }
        else
        {
            delete lineVBO;
            return;
        }
    }

    QVector3D bottom{};
    QVector3D top{};

    if (coordSystem == MTextManager::WORLDSPACE)
    {
        bottom = worldSpacePosProp.value();
        bottom.setZ(0);

        top = worldSpacePosProp.value();
    }
    else if (coordSystem == MTextManager::LONLATP)
    {
        bottom = lonLatPPosProp.value();
        bottom.setZ(1050);
        top = lonLatPPosProp.value();
    }

    lineVBO->upload({bottom, top});
}

} // Met3D