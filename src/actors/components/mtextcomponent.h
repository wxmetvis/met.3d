/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MTEXTCOMPONENT_H
#define MET_3D_MTEXTCOMPONENT_H

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mactorcomponent.h"
#include "gxfw/properties/mpropertytemplates.h"
#include "gxfw/properties/mstringproperty.h"
#include "gxfw/properties/mfontproperty.h"
#include "gxfw/textmanager.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "gxfw/gl/shadereffect.h"

namespace Met3D
{

/**
 * An actor component to add an editable text label.
 */
class MTextComponent : public MActorComponent
{
public:
    MTextComponent(MActor *parent, QString &componentName);
    ~MTextComponent() override;

    void onEnabled() override;
    void onDisabled() override;
    void onNameChanged() override;

    void initializeResources() override;
    void reloadShaderEffects(bool recompile) override;
    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                    float clipX, float clipY) override;
    void dragEvent(MSceneViewGLWidget *sceneView,
                   int handleID, float clipX, float clipY) override;
    void addProperties(MProperty *groupProp) override;
    void toggleProperties(bool enable) override;
    void removeProperties(MProperty *groupProp) override;

    /**
     * Set the text of this component that is displayed to the user.
     */
    void setText(const QString &text);

    /**
     * Set the font size.
     */
    void setFontSize(int size);

    /**
     * Set the text colour.
     */
    void setFontColour(const QColor &colour);

    /**
     * Enable or disable the text bounding box.
     */
    void enableBBox(bool enable);

    /**
     * Set the colour of the text bounding box.
     */
    void setBBoxColour(const QColor &colour);

    /**
     * Set the clip space position of the text, if it is being displayed
     * in clip space.
     */
    void setClipSpacePosition(const QPointF &pos);

    /**
     * Set the lon-lat-p position of the text, if it is being displayed
     * in geographical space.
     */
    void setLonLatPPosition(const QVector3D &pos);

    /**
     * Set the graphics space position of the text, if it is being displayed
     * in graphics space.
     */
    void setWorldSpacePosition(const QVector3D &pos);

    /**
     * Change the coordinate system of the text.
     * Changes if it is being anchored in the world or screen space.
     */
    void setCoordinateSystem(const MTextManager::CoordinateSystem &coordinateSystem);

    /**
     * Change the anchor of the text.
     * @param anchor The new anchor.
     */
    void setAnchor(const MTextManager::TextAnchor &anchor);

    /**
     * Enable or disable the ability for the user to move the text interactively.
     */
    void enableDragging(bool enable);

    /**
     * @return The label for this text component or @c nullptr, if no text is currently rendered.
     */
    MLabel* getLabel() const;

    /**
     * @return The group property for this component.
     */
    MProperty *getGroupProperty() { return &labelProperties.enabledProp; };

public slots:

    /**
     * Called when the label should update based on changes to the properties.
     */
    void onLabelChanged();

protected:

    /**
     * Update the geometry associated with this component.
     */
    void updateGeometry();

    MPropertyTemplates::Labels labelProperties;
    MStringProperty labelTextProp;
    MFontProperty fontProp;
    MEnumProperty coordinateSystemProp;
    MEnumProperty anchorProp;
    MPointFProperty clipSpacePosProp;
    MVector3DProperty lonLatPPosProp;
    MVector3DProperty worldSpacePosProp;

    MLabel *label;

    MTextManager::CoordinateSystem coordSystem;
    bool draggingEnabled;
    QPointF draggingOffset;

    // OpenGL resources
    std::shared_ptr<GL::MShaderEffect> lineShader;
    GL::MVector3DVertexBuffer *lineVBO;

};

} // Met3D

#endif //MET_3D_MTEXTCOMPONENT_H
