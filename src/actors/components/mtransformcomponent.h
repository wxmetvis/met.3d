/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022      Luka Elwart
**  Copyright 2023-2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MTRANSFORMCOMPONENT_H
#define MTRANSFORMCOMPONENT_H

// standard library imports

// related third party import
#include <QMatrix4x4>
#include <QVector3D>

// local application imports
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "gxfw/mactorcomponent.h"
#include "gxfw/properties/mvector3dproperty.h"
#include "gxfw/properties/marrayproperty.h"

namespace Met3D
{

/**
 * @brief The MTransformComponent class Is an actor component representing a transform in 3D space with a special transform gizmo.
 * It can be added to an actor to give it properties for positional, rotational and scaling information.
 * It also provides a gizmo, which is activated when entering interaction mode.
 * In this mode, the gizmo can be used to interactively modify the transform.
 * TODO (tv, 20Feb2023) Support scaling modification in transform gizmo.
 */
class MTransformComponent : public MActorComponent
{
public:
    /**
     * @brief The TransformationMode enum describes the current manipulation mode of this gizmo. Scale is currently not supported.
     */
    enum TransformationMode
    {
        Move,
        Rotate,
        Scale,
        None
    };

    /**
     * @brief The Axis enum assigns each transformation axis a bit for bitwise operations
     */
    enum Axis : uint8_t
    {
        Undef = 0u,
        X = 1u,
        Y = 2u,
        Z = 4u
    };

    /**
     * @brief MTransformComponent Initialize a new transform component.
     * @param parent Parent actor
     * @param name Unique name for this component within the actors component list.
     * @param addLocationProperties Whether to add locational properties to the property window.
     * @param addRotationProperties Whether to add rotational properties to the property window.
     * @param addScaleProperties Whether to add scaling properties to the property window.
     */
    MTransformComponent(MActor *parent, QString name,
                        bool addLocationProperties, bool addRotationProperties,
                        bool addScaleProperties, QString propertyGroupName);
    ~MTransformComponent() override;

    // MActorComponent interface
    void onEnabled() override;
    void onDisabled() override;
    void initializeResources() override;
    void reloadShaderEffects(bool recompile) override;
    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;
    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                    float clipX, float clipY) override;
    void dragEvent(MSceneViewGLWidget *sceneView, int handleID,
                           float clipX, float clipY) override;
    void releaseEvent(MSceneViewGLWidget *sceneView,
                              int handleID) override;
    void addProperties(MProperty *groupProp) override;
    void toggleProperties(bool enabled) override;
    void removeProperties(MProperty *groupProp) override;
    void saveConfiguration(QSettings *settings) override;
    void loadConfiguration(QSettings *settings) override;
    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    // Transformation methods
    /**
     * @return The current world position of the transform.
     */
    QVector3D getPosition() const;

    /**
     * @brief setPosition Sets the world position of this transform.
     * @param position World position in lat/lon/z coordinates
     */
    void setPosition(const QVector3D &position);

    /**
     * @brief setPositionX Sets the x component of the position of this transform.
     * @param x The x component of this transform, also known as latitude.
     */
    void setPositionX(const float &x);

    /**
     * @brief setPositionY Sets the y component of the position of this transform.
     * @param y The y component of this transform, also known as longitude.
     */
    void setPositionY(const float &y);

    /**
     * @brief setPositionZ Sets the z component of the position of this transform.
     * @param z The z component of this transform.
     */
    void setPositionZ(const float &z);

    /**
     * @return The rotation of this transform in euler angles (pitch, yaw, roll)
     */
    QVector3D getRotation() const;

    /**
     * @brief getPitch Returns the pitch of this transform (up-down rotation)
     */
    float getPitch() const;

    /**
     * @brief getYaw Returns the yaw of this transform (left-right rotation)
     */
    float getYaw() const;

    /**
     * @brief getRoll Return s the roll of this transform.
     */
    float getRoll() const;

    /**
     * @brief setRotation Sets the rotation of this transform in euler angles.
     * @param newRotation A vector containing rotation angles (pitch, yaw, roll).
     */
    void setRotation(const QVector3D &newRotation);

    /**
     * @brief setRotation Sets the rotation of this transform in euler angles. Yaw is applied first, then pitch and last roll.
     * @param pitch Pitch angle around local y-axis.
     * @param yaw Yaw angle around global z-axis.
     * @param roll Roll angle around local x-axis.
     */
    void setRotation(const float &pitch, const float &yaw, const float &roll);

    /**
     * @brief setPitch Sets the pitch of this transform.
     * @param pitch Pitch in euler angles.
     */
    void setPitch(const float &pitch);

    /**
     * @brief setYaw Sets the yaw of this transform.
     * @param yaw Yaw in euler angles.
     */
    void setYaw(const float &yaw);

    /**
     * @brief setRoll Sets the roll of this transform.
     * @param roll Roll in euler angles.
     */
    void setRoll(const float &roll);

    /**
     * @return The world scale of this transform.
     */
    QVector3D getScale() const;

    /**
     * @brief setScale Sets the scale of this transform.
     * @param newScale Scale of this transform.
     */
    void setScale(const QVector3D &newScale);

    /**
     * @brief setScale Sets the scale of this transform.
     * @param newScale Scale of this transform.
     */
    void setScale(const float &newScale);

    /**
     * @brief getTransformMatrix Retrieve the transforms transformation matrix.
     * @param getPosition Whether to include position of transform, otherwise use default.
     * @param getRotation Whether to include rotation of transform, otherwise use default.
     * @param getScale Whether to include scale of transform, otherwise use default.
     * @return A 4x4 Matrix containing position, rotation and scale information.
     */
    QMatrix4x4 getTransformMatrix(bool getPosition = true,
                                  bool getRotation = true,
                                  bool getScale = true) const;

    /**
     * @return The last manipulation mode of this transform.
     */
    TransformationMode getLastManipulationMode() const;

    /**
     * @brief generateArrowGeometry Generates geometry data (vertices & indices) for an arrow in direction: normalize(magnitude) with length: |magnitude|
     * @param magnitude QVector3D in direction of arrow with magnitude as length of arrow.
     * @param vertices QVector where vertices will be stored in.
     * @param indices QVector where indices will be stored in.
     * @param w The value the 4th component of the vertex array should contain, can be used as an identifier for different mesh sections.
     * @param resolution Resolution of arrow in vertices.
     * @param radius Radius of the arrow.
     * @param tipLengthRatio Ratio between arrow length and arrow tip length.
     * @param tipRadiusRatio Ratio between arrow radius and arrow tip radius.
     * @param interleaveNormal Whether or not to interleave normals. Will double the amount of vertices needed if true.
     */
    static void generateArrowGeometry(const QVector3D &magnitude,
                               QVector<QVector4D> &vertices,
                               QVector<uint32_t> &indices, const float &w,
                               const int &resolution = 8,
                               const float &radius = 0.125f,
                               const float &tipLengthRatio = 0.75f / 2.5f,
                               const float &tipRadiusRatio = 1.5f,
                               const bool &interleaveNormal = false);

    /**
     * @brief generatePlaneGeometry Generates geometry data (vertices & indices) for a plane
     * @param vertices QVector where vertices will be stored in.
     * @param indices QVector where indices will be stored in.
     * @param origin Plane origin.
     * @param right Right vector of the plane from the origin.
     * @param up Up vector of the plane from the origin.
     * @param sizeRight Size of the plane in right direction.
     * @param sizeUp Size of the plane in up direction.
     * @param w The value the 4th component of the vertex array should contain, can be used as an identifier for different mesh sections.
     */
    static void generatePlaneGeometry(QVector<QVector4D> &vertices,
                               QVector<uint32_t> &indices,
                               const QVector3D &origin, const QVector3D &right,
                               const QVector3D &up, const float &sizeRight,
                               const float &sizeUp, const float &w);

    /**
     * @brief generateQuarterCircleGeometry Generates geometry data (vertices & indices) for a quarter circle
     * @param vertices QVector where vertices will be stored in.
     * @param indices QVector where indices will be stored in.
     * @param origin Circle origin.
     * @param right Right vector of the plane from the origin.
     * @param up Up vector of the plane from the origin.
     * @param innerRadius Inner radius of circle.
     * @param outerRadius Outer radius of circle.
     * @param w The value the 4th component of the vertex array should contain, can be used as an identifier for different mesh sections.
     */
    static void generateQuarterCircleGeometry(
        QVector<QVector4D> &vertices, QVector<uint32_t> &indices,
        const QVector3D &origin, const QVector3D &right, const QVector3D &up,
        const float &innerRadius, const float &outerRadius,
        const uint &resolution, const float &w) ;

private:
    /**
     * @return A vector containing whether each axis is active as 0 for false and otherwise true.
     */
    QVector3D getActiveAxis() const;

    // Member variables
protected:
    /**
     * @brief transform Transformation matrix of this transform.
     */
    QMatrix4x4 transform;

    /**
     * @brief scale The scale of this transform. Will be applied on the diagonal of the transformation matrix.
     */
    QVector3D scale;

    /**
     * @brief rotation The current rotation of the transform stored in a vector for edge cases where pitch reaches 90 or -90 degrees, so that we still know the roll and yaw.
     * Do not set this variable manually, please use @c setRotation(...) to set this variable, as it needs to also update the member @c transform
     */
    QVector3D rotation;

    // Temp drag variables
    /**
     * @brief initialDragTransform Transform before drag event executed.
     */
    QMatrix4x4 initialDragTransform;
    /**
     * @brief initialMousePosition Mouse position before drag event executed.
     */
    QVector3D initialMousePosition;
    /**
     * @brief isDragging Whether the gizmo is currently being dragged.
     */
    bool isDragging;
    /**
     * @brief activeAxis Axis being dragged or hovered over. 1st bit is X, 2nd is Y, 3rd is Z -> e.g. 3 means X and Y are active; if 0 none is active.
     */
    uint8_t activeAxis;
    /**
     * @brief activeMode Active transformation mode of this transform.
     */
    TransformationMode activeMode;
    /**
     * @brief activePlane The plane this object's movement is being projected on.
     */
    QVector4D activePlane;

    /**
     * @brief propertyGroupName The name of the property group in the parent actor, where this gizmos properties are placed.
     */
    QString propertyGroupName;

    // Properties
    const bool addPositionalProperties;
    const bool addRotationalProperties;
    const bool addScaleProperties;
    MVector3DProperty positionProp;

    MVector3DProperty scaleProp;

    MVector3DProperty rotationProp;

    MProperty transformGroupProp;

    // Render specific variables
    std::shared_ptr<GL::MShaderEffect> gizmoShader;
    GL::MVector4DVertexBuffer *vertexBuffer;
    GLuint gizmoIndexBuffer;
    GLsizei indexBufferSize;

    // Constants for gizmo geometry
    const float arrowLength = 2.5f;
    const float arrowTipLength = 0.75f;
    const float arrowRadius = 0.125f;
    const float arrowTipRadius = arrowRadius * 1.5f;
    const float gizmoPlaneSize = 0.75f;
    const float gizmoRotatorInnerRadius = 1.625f;
    const float gizmoRotatorThickness = 0.15f;
    const int gizmoRotatorRes = 16u;
    const float gizmoScreenScaling = 0.05f;
};

} // namespace Met3D

#endif // MTRANSFORMCOMPONENT_H
