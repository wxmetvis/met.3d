/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MSPRITECOMPONENT_H
#define MET_3D_MSPRITECOMPONENT_H

// standard library imports

// related third party imports
#include <QVector3D>
#include <QVector2D>

// local application imports
#include "gxfw/mactorcomponent.h"
#include "gxfw/gl/texture.h"
#include "gxfw/gl/vertexbuffer.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/mstringproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mpointfproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mfileproperty.h"

namespace Met3D
{

/**
 * An actor component displaying a 2D sprite, either in world space or clip space.
 * In clip space, the image can be anchored to any of the four screen corners or its center.
 * In world space, the image is always anchored at its center at the position it is currently at.
 * It will also always be the same size on screen, regardless of the camera distance.
 * This component also caches loaded images in the @c MGLResourceManager,
 * so that when two different image components display the same image,
 * it is only uploaded and read once.
 */
class MSpriteComponent : public MActorComponent
{
public:
    /**
     * The coordinate system used for the image anchor.
     * @c WORLD_SPACE is for world space images.
     * @c PIXEL_SPACE is for clip space images, though the position is given
     * in pixels to ensure that the image always stays at the same position relative
     * to the anchor, regardless of screen size etc.
     */
    enum CoordinateSystem
    {
        WORLD_SPACE,
        PIXEL_SPACE
    };

    /**
     * The anchors for clip space / pixel space images.
     */
    enum ClipSpaceAnchor
    {
        CENTER = 0,
        BOTTOM_LEFT,
        BOTTOM_RIGHT,
        TOP_LEFT,
        TOP_RIGHT
    };

    MSpriteComponent(MActor *parent, QString name,
                     CoordinateSystem coordinates,
                     bool enableProperties = false);

    ~MSpriteComponent() override;

    void initializeResources() override;

    void reloadShaderEffects(bool recompile) override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    void renderToUiLayer(MSceneViewGLWidget *sceneView) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    void addProperties(MProperty *groupProp) override;

    /**
     * Sets the position of this image.
     * The position is always specified in the coordinate system that
     * is currently used by the image.
     * @param pos The position.
     * @param undoable Whether the user can undo this action.
     * @ref getCoordinateSystem()
     */
    void setPosition(const QVector3D &pos, bool undoable = false);

    /**
     * Get the current position of the image in the coordinate system
     * that is currently used by it.
     * @return A vector containing the position of the image.
     * @ref getCoordinateSystem()
     */
    QVector3D getPosition() const;

    /**
     * Set the scale of this image in clip space.
     * The x component of the vector @p s scales the width,
     * the y component the height.
     * @param s The scale vector.
     */
    void setScale(const QVector2D &s);

    /**
     * Set the scale of this image in clip space.
     * @param scaleX The width scale.
     * @param scaleY The height scale.
     */
    void setScale(float xScale, float yScale);

    /**
     * Get the scale of the image in clip space. The x component scales width,
     * the y component the height.
     * @return A vector containing width and height scale.
     */
    QVector2D getScale() const;

    /**
     * Get the x scale of the image. This scales the width.
     * @return The x scale.
     */
    float getScaleX() const;

    /**
     * Get the y scale of the image. This scales the height.
     * @return The y scale.
     */
    float getScaleY() const;

    /**
     * Get the width of the image.
     * @return The width in pixels.
     */
    int width() const;

    /**
     * Get the height of the image.
     * @return The height in pixels.
     */
    int height() const;

    /**
     * This will set the image that the sprite currently displays.
     * @param path The path to the new image.
     */
    void setImage(const QString &path);

    /**
     * Get the path to the current image.
     * If the image was not set through a path,
     * this will return an empty string.
     * @return The current image path, if set.
     */
    const QString &getImagePath() const;

    /**
     * Get the coordinate system, in which the image is displayed.
     * @return The coordinate system.
     */
    CoordinateSystem getCoordinateSystem() const;

    /**
     * Sets the anchor for clip space images.
     * Does nothing for world space images.
     * @param anchor The new anchor.
     */
    void setClipSpaceAnchor(ClipSpaceAnchor anchor);

    /**
     * Get the clip space anchor for clip space images.
     * Will return @c CENTER for world space images but is irrelevant for them.
     * @return The clip space anchor for clip space images.
     */
    ClipSpaceAnchor getClipSpaceAnchor() const;

    /**
     * @return Position of the clip space anchor in clip space coordinates.
     */
    QVector2D getClipSpaceAnchorPosition() const;

    /**
     * Set the opacity of the image.
     * @param alpha The new opacity in range 0...1.
     */
    void setOpacity(float alpha);

    /**
     * @return The opacity of the image.
     */
    float getOpacity() const;

    /**
     * If this component uses @c PIXEL_SPACE, convert the given pixel coordinates
     * to clip space coordinates, based on the anchor of the displayed image.
     * @param sceneView The scene view for which the clip space coordinates are calculated.
     * @param pixelPos The pixel position that should be converted.
     * @return The corresponding clip space position or [0,0], if the coordinate system of this image
     * component is not @c PIXEL_SPACE.
     */
    QVector2D
    pixelToClipSpace(MSceneViewGLWidget *sceneView, QVector2D pixelPos) const;

    /**
     * If this component uses @c PIXEL_SPACE, convert the given clip space
     * coordinates to pixel coordinates, based on the anchor of the displayed image.
     * @param sceneView The scene view for which the pixel space coordinates are calculated.
     * @param clipPos The clip space position that should be converted.
     * @return The corresponding pixel space position or [0,0], if the coordinate system of this image
     * component is not @c PIXEL_SPACE.
     */
    QVector2D
    clipToPixelSpace(MSceneViewGLWidget *sceneView, QVector2D clipPos) const;

protected:
    /**
     * Generate the geometry for the image.
     */
    void generateGeometry();

    /**
     * The coordinate system the image is displayed in.
     */
    CoordinateSystem coordinateSystem;

    /**
     * Center position of the sprite.
     * In @c PIXEL_SPACE coordinate system,
     * only the x and y component are used
     * and represent pixels relative to the @c csAnchor.
     */
    QVector3D position;

    /**
     * The width and height of the image as a multiple of its original size.
     */
    MPointFProperty scaleProp;

    /**
     * The path to the displayed image.
     */
    QString imagePath;

    /**
     * The image opacity.
     */
    MFloatProperty opacityProp;

    /**
     * Adds properties for image manipulation to the parent actor and enables
     * saving of this components settings.
     */
    bool enableProperties;

    /**
     * The anchor for clip-space images.
     */
    ClipSpaceAnchor csAnchor;

    // Sprite rendering
    GL::MVertexBuffer *spriteVertexBuffer;
    GL::MTexture *imageTexture;
    std::shared_ptr<GL::MShaderEffect> spriteShader;
    std::shared_ptr<GL::MShaderEffect> anchorShader;

    // Properties
    MFileProperty imagePathProp;
    MButtonProperty reloadProp;
    MPointFProperty posCsProp;
    MEnumProperty anchorProp;

    // Counter for image IDs used.
    static int nextAvailableImageID;
};

} // Met3D

#endif //MET_3D_MSPRITECOMPONENT_H
