/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mspritecomponent.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "util/mfiletypes.h"


namespace Met3D
{
int MSpriteComponent::nextAvailableImageID = 0;

MSpriteComponent::MSpriteComponent(MActor *parent, QString name,
                                   CoordinateSystem coordinates,
                                   bool enableProperties)
        : MActorComponent(parent, name),
          coordinateSystem(coordinates),
          position(0, 0, 0),
          imagePath(),
          enableProperties(enableProperties),
          csAnchor(CENTER),
          spriteVertexBuffer(nullptr),
          imageTexture(nullptr)
{

}


MSpriteComponent::~MSpriteComponent()
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (spriteVertexBuffer)
    {
        glRM->releaseGPUItem(spriteVertexBuffer);
    }

    if (imageTexture != nullptr)
    {
        glRM->releaseGPUItem(imageTexture);
    }
}


void MSpriteComponent::initializeResources()
{
    reloadShaderEffects(false);

    if (! imagePath.isEmpty())
    {
        setImage(imagePath);
    }
    generateGeometry();
}


void MSpriteComponent::reloadShaderEffects(bool recompile)
{
    if (recompile)
    {
        spriteShader->compileFromFile_Met3DHome("src/glsl/sprite.fx.glsl");
        anchorShader->compileFromFile_Met3DHome(
                "src/glsl/simple_coloured_geometry.fx.glsl");
    }
    else
    {
        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        if (glRM->generateEffectProgram("Sprite", spriteShader))
        {
            spriteShader->compileFromFile_Met3DHome("src/glsl/sprite.fx.glsl");
        }
        if (glRM->generateEffectProgram("Anchor", anchorShader))
        {
            anchorShader->compileFromFile_Met3DHome(
                    "src/glsl/simple_coloured_geometry.fx.glsl");
        }
    }
}


void
MSpriteComponent::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (coordinateSystem != CoordinateSystem::WORLD_SPACE) return;

    if (imagePathProp.value() != imagePath)
    {
        setImage(imagePathProp);
    }

    if (imageTexture == nullptr) return;
    if (sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING) return;

    spriteShader->bindProgram("Sprite");
    CHECK_GL_ERROR;

    QMatrix4x4 mv = *(sceneView->getViewMatrix());
    QMatrix4x4 projMat = *(sceneView->getProjectionMatrix());

    spriteShader->setUniformValue("anchorPos", position);
    spriteShader->setUniformValue("size", QVector2D(scaleProp));
    spriteShader->setUniformValue("mvMatrix", mv);
    spriteShader->setUniformValue("pMatrix", projMat);
    spriteShader->setUniformValue("opacity", opacityProp);

    imageTexture->bindToTextureUnit(0);
    CHECK_GL_ERROR;

    spriteVertexBuffer->attachToVertexAttribute(0);
    CHECK_GL_ERROR;

    bool wire = getParentActor()->renderingAsWireframe();

    glPolygonMode(GL_FRONT_AND_BACK, wire ? GL_LINES : GL_FILL);
    CHECK_GL_ERROR;
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    CHECK_GL_ERROR;

    imageTexture->unbindFromTextureUnit(0);
}


void MSpriteComponent::renderToUiLayer(MSceneViewGLWidget *sceneView)
{
    if (coordinateSystem != CoordinateSystem::PIXEL_SPACE) return;

    if (imagePathProp.value() != imagePath)
    {
        setImage(imagePathProp);
    }

    if (imageTexture == nullptr) return;

    int sceneW = sceneView->getRenderResolutionWidth();
    int sceneH = sceneView->getRenderResolutionHeight();
    float clipW =
            static_cast<float>(width()) / static_cast<float>(sceneW);
    float clipH =
            static_cast<float>(height()) / static_cast<float>(sceneH);

    QVector2D clipPos = pixelToClipSpace(sceneView,
                                         {position.x(), position.y()});

    clipW *= static_cast<float>(scaleProp.x());
    clipH *= static_cast<float>(scaleProp.y());

    spriteShader->bindProgram("UISprite");
    CHECK_GL_ERROR;

    spriteShader->setUniformValue("anchorPos", QVector3D(
            static_cast<float>(clipPos.x()),
            static_cast<float>(clipPos.y()),
            -1));
    spriteShader->setUniformValue("size", QVector2D(clipW, clipH));
    spriteShader->setUniformValue("opacity", opacityProp);

    imageTexture->bindToTextureUnit(0);
    CHECK_GL_ERROR;

    spriteVertexBuffer->attachToVertexAttribute(0);
    CHECK_GL_ERROR;

    bool wire = getParentActor()->renderingAsWireframe();

    glPolygonMode(GL_FRONT_AND_BACK, wire ? GL_LINE : GL_FILL);
    CHECK_GL_ERROR;

    glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
    CHECK_GL_ERROR;

    imageTexture->unbindFromTextureUnit(0);

    if (!sceneView->interactionModeEnabled()) return;

    // Draw anchor
    float ratio = static_cast<float>(sceneH) / static_cast<float>(sceneW);
    QVector2D anchorPos = getClipSpaceAnchorPosition();

    anchorShader->bindProgram("ImageAnchor");
    anchorShader->setUniformValue("anchor",
                                  QVector3D(anchorPos.x(), anchorPos.y(), -1));
    anchorShader->setUniformValue("scale", QVector2D(0.05f * ratio, 0.05f));
    anchorShader->setUniformValue("colour", QColor(255, 0, 0, 255));

    sceneView->setLineWidth(2 / sceneView->getViewportUpscaleFactor());
    glPolygonMode(GL_FRONT_AND_BACK, GL_POINT);
    CHECK_GL_ERROR;
    glDrawArrays(GL_POINTS, 0, 1);
    CHECK_GL_ERROR;
}


void MSpriteComponent::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    // Nothing to load if properties are not enabled.
    if (!enableProperties) return;

    settings->beginGroup(getName());
    QVector2D scale = settings->value("scale", QVector2D(1, 1)).value<QVector2D>();
    QVector3D pos = settings->value("position", QVector2D(0, 0)).value<QVector3D>();
    csAnchor = static_cast<ClipSpaceAnchor>(settings->value("anchor", 0)
                                                    .toInt());
    QString path = settings->value("imagePath", "").toString();
    opacityProp = settings->value("opacity").toFloat();
    settings->endGroup();

    scaleProp = {scale.x(), scale.y()};

    if (coordinateSystem == PIXEL_SPACE)
    {
        anchorProp.setValue(csAnchor);
    }

    setPosition(pos);
    if (getParentActor()->isInitialized())
    {
        setImage(path);
    }
    else
    {
        // Already set the path, we can't upload the image yet, though.
        imagePath = path;
    }
}


void MSpriteComponent::addProperties(MProperty *groupProp)
{
    imagePathProp = MFileProperty("Image path", imagePath);
    imagePathProp.setConfigKey("image_path");
    imagePathProp.setHidden(!enableProperties);
    imagePathProp.setFileFilter(FileTypes::M_IMAGES);
    imagePathProp.setDialogTitle("Load image");
    imagePathProp.registerValueCallback(this, &MActorComponent::emitComponentChangedSignal);
    groupProp->addSubProperty(imagePathProp);

    reloadProp = MButtonProperty("Reload image", "Reload");
    reloadProp.registerValueCallback([=]() {
        setImage(imagePath);
    });
    groupProp->addSubProperty(reloadProp);

    scaleProp = MPointFProperty("Scale", {1.0f, 1.0f});
    scaleProp.setTooltip("The scale of the image as a multiple of its original size.");
    scaleProp.setConfigKey("scale");
    scaleProp.setMinMax(0, 100);
    scaleProp.setDecimals(2);
    scaleProp.setStep(0.1);
    scaleProp.registerValueCallback(this, &MActorComponent::emitComponentChangedSignal);
    scaleProp.setHidden(!enableProperties);
    groupProp->addSubProperty(scaleProp);

    if (coordinateSystem == PIXEL_SPACE)
    {
        anchorProp = MEnumProperty("Anchor");
        anchorProp.setConfigKey("anchor");
        anchorProp.setTooltip(
                "The anchor is a fix point used to position the image. "
                "If the scene resizes, the image will stay in the "
                "same position relative to this anchor point."
                "Changing the anchor will try to keep the image in the same location for the first scene view."
                "In all others, the image might move.");

        QStringList anchors = {"Center", "Bottom left", "Bottom right",
                               "Top left", "Top right"};
        anchorProp.setEnumNames(anchors);
        anchorProp.setValue(csAnchor);
        anchorProp.setHidden(!enableProperties);
        anchorProp.registerValueCallback([=]()
        {
            auto value = static_cast<ClipSpaceAnchor>(anchorProp.value());

            auto sceneViews = getParentActor()->getViews();

            // Try to retain position relative to the anchor for the first scene view.
            if (!sceneViews.empty())
            {
                QVector2D clipPos = pixelToClipSpace(sceneViews[0],
                                                     {position.x(), position.y()});

                csAnchor = value;

                setPosition(clipToPixelSpace(sceneViews[0], clipPos));
            }
            else
            {
                // Set position to the anchor if actor is part of no scene.
                setPosition({0, 0, 0});
                csAnchor = value;
            }

            emitComponentChangedSignal();
        });

        groupProp->addSubProperty(anchorProp);
        posCsProp = MPointFProperty("Viewport position", {});
        posCsProp.setConfigKey("viewport_position");
        posCsProp.setSuffix(" px");
        posCsProp.setShowSuffixInPreview(true);
        posCsProp.setTooltip(
                "The position in pixels relative to the image anchor.");
        posCsProp.setHidden(!enableProperties);
        posCsProp.registerValueCallback([=]()
        {
            position.setX(static_cast<float>(posCsProp.value().x()));
            position.setY(static_cast<float>(posCsProp.value().y()));

            emitComponentChangedSignal();
        });
        groupProp->addSubProperty(posCsProp);
    }
    else
    {
        //TODO (tv, 12Apr2024) Add position properties for world space.
        // However, do this when the new property system is online,
        // as that can easily add a QVector3D property, instead of 3 double properties.
    }

    opacityProp = MFloatProperty("Opacity", 1.0);
    opacityProp.setConfigKey("opacity");
    opacityProp.setMinMax(0.0, 1.0);
    opacityProp.setDecimals(2);
    opacityProp.setStep(0.1);
    opacityProp.setHidden(!enableProperties);
    opacityProp.registerValueCallback(this, &MActorComponent::emitComponentChangedSignal);
    groupProp->addSubProperty(opacityProp);

    if (!enableProperties)
    {
        // Disable saving of properties, if properties should not be visible.
        scaleProp.setConfigKey("");
        imagePathProp.setConfigKey("");
        opacityProp.setConfigKey("");
        posCsProp.setConfigKey("");
        anchorProp.setConfigKey("");
    }
}


void MSpriteComponent::setPosition(const QVector3D &pos, bool undoable)
{
    position = pos;

    if (coordinateSystem == PIXEL_SPACE)
    {
        if (undoable)
        {
            posCsProp.setUndoableValue({pos.x(), pos.y()}, dragEventID);
        }
        else
        {
            posCsProp.setValue({pos.x(), pos.y()});
        }
    }

    emitComponentChangedSignal();
}


QVector3D MSpriteComponent::getPosition() const
{
    return position;
}


void MSpriteComponent::setScale(const QVector2D &s)
{
    scaleProp = {s.x(), s.y()};

    emitComponentChangedSignal();
}


void MSpriteComponent::setScale(float xScale, float yScale)
{
    scaleProp = {xScale, yScale};

    emitComponentChangedSignal();
}


QVector2D MSpriteComponent::getScale() const
{
    return QVector2D(scaleProp.value());
}


float MSpriteComponent::getScaleX() const
{
    return static_cast<float>(scaleProp.x());
}


float MSpriteComponent::getScaleY() const
{
    return static_cast<float>(scaleProp.y());
}


int MSpriteComponent::width() const
{
    if (imageTexture == nullptr) return -1;
    return imageTexture->getWidth();
}


int MSpriteComponent::height() const
{
    if (imageTexture == nullptr) return -1;
    return imageTexture->getHeight();
}


void MSpriteComponent::setImage(const QString &path)
{
    imagePath = path;

    imagePathProp = imagePath;

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (path.isEmpty())
    {
        if (imageTexture != nullptr)
        {
            glRM->releaseGPUItem(imageTexture);
            imageTexture = nullptr;
        }

        return;
    }

    // Create request key unique to the image loaded. Use a global counter
    // as an image at a specific path might be changed by the user, or the user
    // changes an image and wants to update it.
    const QString requestKey = "image_" + QString::number(nextAvailableImageID);
    nextAvailableImageID += 1;

    // Release current texture.
    if (imageTexture != nullptr)
    {
        glRM->releaseGPUItem(imageTexture);
    }

    if (!glRM->contains(requestKey))
    {
        // Read image if it already exists.
        auto img = QImage(imagePath);

        std::vector<GLubyte> colData(img.width() * img.height() * 4);
        for (int y = 0; y < img.height(); y++)
        {
            for (int x = 0; x < img.width(); x++)
            {
                QRgb val = img.pixel(x, y);
                uint32_t index = (y * img.width() + x) * 4;
                colData[index] = qRed(val);
                colData[index + 1] = qGreen(val);
                colData[index + 2] = qBlue(val);
                colData[index + 3] = qAlpha(val);
            }
        }

        bool existed = false;

        imageTexture = glRM
                ->createTexture(requestKey, &existed, GL_TEXTURE_2D,
                                GL_RGBA8, img.width(), img.height());

        if (imageTexture)
        {
            imageTexture->bindToTextureUnit(0);
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, img.width(),
                         img.height(), 0, GL_RGBA, GL_UNSIGNED_BYTE,
                         colData.data());
            CHECK_GL_ERROR;
            glGenerateMipmap(GL_TEXTURE_2D);
            CHECK_GL_ERROR;
            imageTexture->unbindFromTextureUnit(0);
        }
    }
    else
    {
        imageTexture = dynamic_cast<GL::MTexture *>(glRM
                ->getGPUItem(requestKey));

        // Should we check if the cached image and requested image share the same
        // dimensions etc.? Or add a force reload option?
    }

    emitComponentChangedSignal();
}


const QString &MSpriteComponent::getImagePath() const
{
    return imagePath;
}


MSpriteComponent::CoordinateSystem
MSpriteComponent::getCoordinateSystem() const
{
    return coordinateSystem;
}


void MSpriteComponent::setClipSpaceAnchor(
        MSpriteComponent::ClipSpaceAnchor anchor)
{
    if (coordinateSystem == WORLD_SPACE) return;

    csAnchor = anchor;

    if (enableProperties && coordinateSystem == PIXEL_SPACE)
    {
        anchorProp.setValue(csAnchor);
    }

    emitComponentChangedSignal();
}


MSpriteComponent::ClipSpaceAnchor
MSpriteComponent::getClipSpaceAnchor() const
{
    return csAnchor;
}


QVector2D MSpriteComponent::getClipSpaceAnchorPosition() const
{
    if (coordinateSystem != PIXEL_SPACE)
    {
        return {};
    }

    switch (csAnchor)
    {
    case BOTTOM_LEFT:
        return {-1, -1};
    case BOTTOM_RIGHT:
        return {1, -1};
    case TOP_LEFT:
        return {-1, 1};
    case TOP_RIGHT:
        return {1, 1};
    case CENTER:
        return {0, 0};
    }

    return {};
}


void MSpriteComponent::setOpacity(float alpha)
{
    opacityProp = alpha;

    emitComponentChangedSignal();
}


float MSpriteComponent::getOpacity() const
{
    return opacityProp;
}


void MSpriteComponent::generateGeometry()
{
    // Sprite data request key. We only need it stored once,
    // as all sprites can share the geometry.
    const QString requestKeySprite =
            "sprite_component_geometry";

    QVector<QVector4D> vertices(4);

    // x/y are clip space coordinates; z/w are UV coordinates.
    vertices[0] =
            QVector4D(-0.5, -0.5, 0, 1); // bottom left
    vertices[1] =
            QVector4D(0.5, -0.5, 1, 1); // bottom right
    vertices[2] =
            QVector4D(0.5, 0.5, 1, 0); // top right
    vertices[3] =
            QVector4D(-0.5, 0.5, 0, 0); // top left


    // TODO (tv, 15Apr2024): Refactor vertex buffer creation.
    //  Currently, the MActor class has utility methods to upload them easily to GPU.
    //  These could be moved elsewhere, so that components can use them too.
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    GL::MVertexBuffer *vb = dynamic_cast<GL::MVertexBuffer *>(
            glRM->getGPUItem(requestKeySprite));

    if (vb)
    {
        spriteVertexBuffer = vb;
        auto *buf = dynamic_cast<GL::MVector4DVertexBuffer *>(vb);
        // reallocate buffer if size has changed
        buf->reallocate(nullptr, vertices.size(), 0, false, nullptr);
        buf->update(vertices, 0, 0, nullptr);

    }
    else
    {

        GL::MVector4DVertexBuffer *newVB;
        newVB = new GL::MVector4DVertexBuffer(requestKeySprite,
                                              vertices.size());
        if (glRM->tryStoreGPUItem(newVB))
        { newVB->upload(vertices, nullptr); }
        else
        { delete newVB; }
        spriteVertexBuffer = dynamic_cast<GL::MVertexBuffer *>(glRM
                ->getGPUItem(requestKeySprite));
    }
}


QVector2D MSpriteComponent::pixelToClipSpace(MSceneViewGLWidget *sceneView,
                                             QVector2D pixelPos) const
{
    if (coordinateSystem != PIXEL_SPACE) return {};

    int sceneW = sceneView->getRenderResolutionWidth();
    int sceneH = sceneView->getRenderResolutionHeight();

    float clipPosX = 0;
    float clipPosY = 0;

    switch (csAnchor)
    {
    case BOTTOM_LEFT:
        clipPosX = pixelPos.x() / static_cast<float>(sceneW) * 2.0f - 1.0f;
        clipPosY = pixelPos.y() / static_cast<float>(sceneH) * 2.0f - 1.0f;
        break;
    case BOTTOM_RIGHT:
        clipPosX = (1.0f + pixelPos.x() / static_cast<float>(sceneW)) * 2.0f
                - 1.0f;
        clipPosY = pixelPos.y() / static_cast<float>(sceneH) * 2.0f - 1.0f;
        break;
    case TOP_LEFT:
        clipPosX = pixelPos.x() / static_cast<float>(sceneW) * 2.0f - 1.0f;
        clipPosY = (1.0f + pixelPos.y() / static_cast<float>(sceneH)) * 2.0f
                - 1.0f;
        break;
    case TOP_RIGHT:
        clipPosX = (1.0f + pixelPos.x() / static_cast<float>(sceneW)) * 2.0f
                - 1.0f;
        clipPosY = (1.0f + pixelPos.y() / static_cast<float>(sceneH)) * 2.0f
                - 1.0f;
        break;
    case CENTER:
        clipPosX = pixelPos.x() * 2 / (static_cast<float>(sceneW));
        clipPosY = pixelPos.y() * 2 / (static_cast<float>(sceneH));
        break;
    }

    return {clipPosX, clipPosY};
}


QVector2D MSpriteComponent::clipToPixelSpace(MSceneViewGLWidget *sceneView,
                                             QVector2D clipPos) const
{
    if (coordinateSystem != PIXEL_SPACE) return {};

    int sceneW = sceneView->getRenderResolutionWidth();
    int sceneH = sceneView->getRenderResolutionHeight();

    float pixelPosX = 0;
    float pixelPosY = 0;

    switch (csAnchor)
    {
    case BOTTOM_LEFT:
        pixelPosX = (clipPos.x() + 1.0f) / 2.0f * static_cast<float>(sceneW);
        pixelPosY = (clipPos.y() + 1.0f) / 2.0f * static_cast<float>(sceneH);
        break;
    case BOTTOM_RIGHT:
        pixelPosX =
                -(1 - (clipPos.x() + 1.0f) / 2.0f) * static_cast<float>(sceneW);
        pixelPosY = (clipPos.y() + 1.0f) / 2.0f * static_cast<float>(sceneH);
        break;
    case TOP_LEFT:
        pixelPosX = (clipPos.x() + 1.0f) / 2.0f * static_cast<float>(sceneW);
        pixelPosY =
                -(1 - (clipPos.y() + 1.0f) / 2.0f) * static_cast<float>(sceneH);
        break;
    case TOP_RIGHT:
        pixelPosX =
                -(1 - (clipPos.x() + 1.0f) / 2.0f) * static_cast<float>(sceneW);
        pixelPosY =
                -(1 - (clipPos.y() + 1.0f) / 2.0f) * static_cast<float>(sceneH);
        break;
    case CENTER:
        pixelPosX = clipPos.x() * static_cast<float>(sceneW) / 2.0f;
        pixelPosY = clipPos.y() * static_cast<float>(sceneH) / 2.0f;
        break;
    }

    return {pixelPosX, pixelPosY};
}

} // Met3D