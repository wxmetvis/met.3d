/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020      Kameswarro Modali [*]
**  Copyright 2017      Bianca Tost [+]
**  Copyright 2023-2024 Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef GRATICULEACTOR_H
#define GRATICULEACTOR_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>
#include <QVector>
#include <QVector3D>

// local application imports
#include "gxfw/mapprojectionsupportingactor.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/vertexbuffer.h"
#include "gxfw/boundingbox/boundingbox.h"
#include "util/geometryhandling.h"
#include "gxfw/properties/mproperty.h"
#include "gxfw/properties/mstringproperty.h"
#include "gxfw/properties/mpointfproperty.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mcolorproperty.h"
#include "gxfw/properties/mnumberproperty.h"

class MGLResourcesManager;
class MSceneViewGLWidget;

namespace Met3D
{

class MNWPHorizontalSectionActor;

/**
  @brief GraticuleActor draws a graticule into a scene. Horizontal extent,
  spacing of latitude / longitude lines and colour parameters are customisable
  through the scene's property browser. The vertical position of the graticule
  can be set programmatically through the @ref setVerticalPosition() method
  (cannot be changed by the user; the method is to sync a graticule with
  an hsec actor).
  */
class MGraticuleActor : public MMapProjectionSupportingActor,
        public MBoundingBoxInterface
{
    Q_OBJECT
public:
    MGraticuleActor(MBoundingBoxConnection *boundingBoxConnection = nullptr,
                    MNWPHorizontalSectionActor *connectedHSecActor = nullptr);

    ~MGraticuleActor() override;

    static QString staticActorType() { return "Graticule"; }

    static QString staticIconFileName() { return "graticule.png"; }

    void reloadShaderEffects() override;

    /**
      Set the vertical position of the graticule in pressure coordinates.
      */
    void setVerticalPosition(double pressure_hPa);

    /**
      Set the colour of the graticule, coast lines and border lines.
     */
    void setColour(const QColor& c);

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "GraticuleActor"; }

    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    void onBoundingBoxChanged() override;

public slots:
    void onRecomputeGeometry();

signals:
    /**
     * Signal emitted everytime the projection of the graticule changes. Used
     * by a corresponding horizontal section to perform updates.
     */
    void projectionChangedSignal();

protected:
    /**
      Loads the shader programs and generates the graticule geometry. The
      geometry is uploaded to the GPU into a vertex buffer object.
      */
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

private:
    /**
      Generates the graticule geometry and uploads it to a vertex buffer.
      ToDo: add a short summary / documentation
      */
    void generateGeometry();

    QVector<QPolygonF> projectAndClipGeometry(
            MGeometryHandling *geo,
            QVector<QPolygonF> geometry, QRectF bbox,
            double rotatedGridMaxSegmentLength_deg);

    std::shared_ptr<GL::MShaderEffect> shaderProgram;

    GL::MVertexBuffer* graticuleVertexBuffer;
    QVector<int> graticuleStartIndices;
    QVector<int> graticuleVertexCount;

    GL::MVertexBuffer* coastlineVertexBuffer;
    QVector<int> coastlineStartIndices;
    QVector<int> coastlineVertexCount;

    GL::MVertexBuffer* borderlineVertexBuffer;
    QVector<int> borderlineStartIndices;
    QVector<int> borderlineVertexCount;

    MProperty graticuleLinesProp;
    MStringProperty graticuleLongitudesProp;
    MStringProperty graticuleLatitudesProp;
    MProperty graticuleLinesLabelsProp;
    MStringProperty longitudeLabelsProp;
    MStringProperty latitudeLabelsProp;
    MPointFProperty vertexSpacingProp;
    MButtonProperty computeGraticuleProp;

    MBoolProperty drawGraticuleProp;
    MColorProperty graticuleColourProp;
    MFloatProperty graticuleThicknessProp;

    MBoolProperty drawCoastLinesProp;
    MColorProperty coastLinesColourProp;
    MFloatProperty coastLinesThicknessProp;

    MBoolProperty drawBorderLinesProp;
    MColorProperty borderLinesColourProp;
    MFloatProperty borderLinesThicknessProp;

    float verticalPosition_hPa;

    MNWPHorizontalSectionActor *connectedHSecActor;
};

} // namespace Met3D

#endif // GRATICULEACTOR_H
