/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "spatial1dtransferfunction.h"

// standard library imports
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QListWidget>
#include <QListWidgetItem>

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "util/mutil.h"
#include "gxfw/msceneviewglwidget.h"
#include "util/mfiletypes.h"


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSpatial1DTransferFunction::MSpatial1DTransferFunction(QObject *parent)
    : MTransferFunction(parent),
      numLevels(0),
      alphaBlendingMode(AlphaBlendingMode::AlphaChannel),
      currentTextureWidth(0),
      currentTextureHeight(0)
{
    loadedImages.clear();
    pathsToLoadedImages.clear();

    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    // Properties related to texture levels.
    // =====================================

    auto generateBarCallback = [=]()
    {
        if (suppressActorUpdates()) return;

        generateBarGeometry();
        emitActorChangedSignal();
    };

    levelsPropertiesSubGroup = MProperty("Texture levels");
    actorPropertiesSupGroup.addSubProperty(levelsPropertiesSubGroup);

    loadLevelsImagesProperty = MButtonProperty("Load levels", "Load");
    loadLevelsImagesProperty.registerValueCallback(this, &MSpatial1DTransferFunction::onLoadLevelsProperty);
    levelsPropertiesSubGroup.addSubProperty(loadLevelsImagesProperty);

    pathToLoadedImagesProperty = MStringProperty("Level paths", "");
    pathToLoadedImagesProperty.setConfigKey("level_paths");
    pathToLoadedImagesProperty.setEditable(false);
    pathToLoadedImagesProperty.registerValueCallback([=]()
    {
        loadImagesFromPaths(pathsFromFormattedString(pathToLoadedImagesProperty));
    });
    levelsPropertiesSubGroup.addSubProperty(pathToLoadedImagesProperty);

    useMirroredRepeat = MBoolProperty("Mirrored repeat", false);
    useMirroredRepeat.setConfigKey("mirrored_repeat");
    useMirroredRepeat.registerValueCallback([=]()
    {
        if (tfTexture != nullptr)
        {
            MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
            glRM->makeCurrent();
            tfTexture->bindToTextureUnit(0);

            if (useMirroredRepeat)
            {
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
            }
            else
            {
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S,
                                GL_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T,
                                GL_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R,
                                GL_REPEAT); CHECK_GL_ERROR;
            }
            // Need to set these parameters as well, otherwise they are set back
            // to their default values.
            glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER,
                            GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER,
                            GL_LINEAR_MIPMAP_LINEAR); CHECK_GL_ERROR;

        }

        if (suppressActorUpdates()) return;

        emitActorChangedSignal();
    });
    levelsPropertiesSubGroup.addSubProperty(useMirroredRepeat);

    // Properties related to data range.
    // =================================

    clampMaximumProp = MBoolProperty("Clamp maximum", true);
    clampMaximumProp.setConfigKey("clamp_max");
    clampMaximumProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    rangeGroupProp.insertSubProperty(0, clampMaximumProp);

    int significantDigits = valueSignificantDigitsProp;
    interpolationRangeProp = MSciDoubleProperty("Interpolation range", 1.0);
    interpolationRangeProp.setConfigKey("interpolation_range");
    interpolationRangeProp.setMinimum(0.0);
    interpolationRangeProp.setSignificantDigits(significantDigits);
    interpolationRangeProp.setStep(pow(10., -significantDigits));
    interpolationRangeProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    rangeGroupProp.insertSubProperty(1, interpolationRangeProp);

    // Properties related to alpha blending.
    // =====================================

    alphaBlendingGroupProp = MProperty("Alpha blending");
    actorPropertiesSupGroup.addSubProperty(alphaBlendingGroupProp);

    QStringList alphaModeNames = {"Use alpha channel", "Use red channel",
                                  "Use green channel", "Use blue channel",
                                  "Use rgb average", "Use none"};

    alphaBlendingModeProp = MEnumProperty("Mode", alphaModeNames, 0);
    alphaBlendingModeProp.setConfigKey("mode");
    alphaBlendingModeProp.setValue(alphaBlendingMode);
    alphaBlendingModeProp.registerValueCallback([=]()
    {
        alphaBlendingMode =
                AlphaBlendingMode(alphaBlendingModeProp.value());

        // Only invert alpha if alphaBlendingMode is not "none".
        // Therefore disable invertAlpha-Property and set invertAlpha to false.
        if (alphaBlendingMode == AlphaBlendingMode::None)
        {
            enableActorUpdates(false);
            invertAlphaProp.setEnabled(false);
            invertAlphaProp = false;
            enableActorUpdates(true);
        }
        else
        {
            enableActorUpdates(false);
            invertAlphaProp.setEnabled(true);
            enableActorUpdates(true);
        }

        emitActorChangedSignal();
    });
    alphaBlendingGroupProp.addSubProperty(alphaBlendingModeProp);

    invertAlphaProp = MBoolProperty("Invert alpha", false);
    invertAlphaProp.setConfigKey("invert_alpha");
    invertAlphaProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    alphaBlendingGroupProp.addSubProperty(invertAlphaProp);

    useConstantColourProp = MBoolProperty("Constant colour", false);
    useConstantColourProp.setConfigKey("constant_colour");
    useConstantColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    alphaBlendingGroupProp.addSubProperty(useConstantColourProp);

    constantColourProp = MColorProperty("Colour", QColor(0, 0, 0, 255));
    constantColourProp.setConfigKey("colour");
    constantColourProp.registerValueCallback([=]()
    {
        if (!useConstantColourProp) return;

        emitActorChangedSignal();
    });
    alphaBlendingGroupProp.addSubProperty(constantColourProp);

    useWhiteBgForBarProp = MBoolProperty("White background for bar", false);
    useWhiteBgForBarProp.setConfigKey("white_background_for_bar");
    useWhiteBgForBarProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    alphaBlendingGroupProp.addSubProperty(useWhiteBgForBarProp);


    // Properties related to scale of texture.
    // =======================================

    textureScaleGroupProp = MProperty("Texture scale");
    actorPropertiesSupGroup.addSubProperty(textureScaleGroupProp);

    int decimals = 1;
    textureScaleDecimalsProp = MIntProperty("Decimals", decimals);
    textureScaleDecimalsProp.setConfigKey("decimals");
    textureScaleDecimalsProp.setMinMax(0, 9);
    textureScaleDecimalsProp.registerValueCallback([=]()
    {
        int decimals = textureScaleDecimalsProp;
        double minStep = pow(10.,-decimals);

        textureScaleProp.setDecimals(decimals);
        textureScaleProp.setStep(minStep);
        textureScaleProp.setMinimum(minStep);
    });
    textureScaleGroupProp.addSubProperty(textureScaleDecimalsProp);

    textureScaleProp = MDoubleProperty("Scale", 1.0);
    textureScaleProp.setConfigKey("scale");
    textureScaleProp.setDecimals(decimals);
    textureScaleProp.setStep(pow(10., -decimals));
    textureScaleProp.setMinimum(pow(10., -decimals));
    textureScaleProp.setTooltip("Scale of texture width in data resolution. \n"
                                     "Height is scaled according to aspect ratio.");
    textureScaleProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    textureScaleGroupProp.addSubProperty(textureScaleProp);

    minimumValueProp.registerValueCallback(generateBarCallback);
    maximumValueProp.registerValueCallback(generateBarCallback);
    maxNumTicksProp.registerValueCallback(generateBarCallback);
    maxNumLabelsProp.registerValueCallback(generateBarCallback);
    positionProp.registerValueCallback(generateBarCallback);
    tickWidthProp.registerValueCallback(generateBarCallback);
    labelSpacingProp.registerValueCallback(generateBarCallback);
    labelProps.enabledProp.registerValueCallback(generateBarCallback);
    labelProps.fontColourProp.registerValueCallback(generateBarCallback);
    labelProps.enableBBoxProp.registerValueCallback(generateBarCallback);
    labelProps.bboxColourProp.registerValueCallback(generateBarCallback);
    descriptionLabelProp.registerValueCallback(generateBarCallback);
    flipDescriptionProp.registerValueCallback(generateBarCallback);

    valueSignificantDigitsProp.registerValueCallback([=]()
    {
        int sigDigits = valueSignificantDigitsProp;
        minimumValueProp.setSignificantDigits(sigDigits);
        maximumValueProp.setSignificantDigits(sigDigits);
        interpolationRangeProp.setSignificantDigits(sigDigits);
        valueStepProp.setSignificantDigits(sigDigits);

        if (suppressActorUpdates()) return;

        // Texture remains unchanged; only geometry needs to be updated.
        generateBarGeometry();
        emitActorChangedSignal();
    });
    valueStepProp.registerValueCallback([=]()
    {
        float step = valueStepProp;
        minimumValueProp.setStep(step);
        maximumValueProp.setStep(step);
        interpolationRangeProp.setStep(step);
    });
}


MSpatial1DTransferFunction::~MSpatial1DTransferFunction()
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE  0
#define SHADER_TEXTURE_ATTRIBUTE 1

void MSpatial1DTransferFunction::loadConfiguration(QSettings *settings)
{
    MTransferFunction::loadConfiguration(settings);

    if (isInitialized())
    {
        generateTransferTexture();
        generateBarGeometry();
    }
}


void MSpatial1DTransferFunction::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MTransferFunction::loadConfigurationPrior_V_1_14(settings);

    settings->beginGroup(MSpatial1DTransferFunction::getSettingsID());

    // Properties related to data range.
    // =================================
    clampMaximumProp = settings->value("clampMaximum", true).toBool();
    interpolationRangeProp = settings->value("interpolationRange", 1.0).toDouble();

    // Properties related to alpha blending.
    // =====================================
    alphaBlendingModeProp = settings->value("alphaBlendingMode", 0).toInt();
    invertAlphaProp = settings->value("invertAlpha", false).toBool();
    useConstantColourProp = settings->value("useConstantColour", false).toBool();
    constantColourProp = settings->value("constantColour",
                                         QColor(0, 0, 0, 255))
                                 .value<QColor>();

    // Properties related to type of texture.
    // ======================================
    QStringList paths = ((settings->value("pathsToLoadedImages", "").toString())
            .split("; ", Qt::SkipEmptyParts));
    pathToLoadedImagesProperty = formatPathList(paths);

    useMirroredRepeat = settings->value("useMirroredRepeat", false).toBool();

    // Properties related to texture scale.
    // ====================================
    textureScaleDecimalsProp = settings->value("textureScaleDecimals", 1).toInt();
    textureScaleProp = settings->value("textureScale", 1.0).toDouble();

    settings->endGroup();

    if (isInitialized())
    {
        generateTransferTexture();
        generateBarGeometry();
    }
}


void MSpatial1DTransferFunction::setValueSignificantDigits(int significantDigits)
{
    MTransferFunction::setValueSignificantDigits(significantDigits);
    interpolationRangeProp.setSignificantDigits(significantDigits);
}


void MSpatial1DTransferFunction::setValueStep(float step)
{
    MTransferFunction::setValueStep(step);
    interpolationRangeProp.setStep(step);
}


void MSpatial1DTransferFunction::onLoadLevelsProperty()
{
    QStringList fileNames = MFileUtils::getOpenFileNames(nullptr,
                                                         "Open Images",
                                                         FileTypes::M_IMAGES,
                                                         (pathsToLoadedImages.empty() ?
                                                          "/home/" :
                                                          pathsToLoadedImages.at(0)));

    if (fileNames.length() >= 2)
    {
        numLevels = fileNames.length();

        QDialog dialog(nullptr);
        dialog.setWindowTitle("Change order of textures:");
        auto *layout = new QVBoxLayout();
        auto *listWidget = new QListWidget(nullptr);
        dialog.setLayout(layout);
        layout->addWidget(listWidget);
        listWidget->addItems(fileNames);
        listWidget->setDragEnabled(true);
        listWidget->setDropIndicatorShown(true);
        listWidget->setDragDropMode(QAbstractItemView::InternalMove);
        auto *okButton = new QPushButton(nullptr);
        okButton->setText("OK");
        connect(okButton, SIGNAL(clicked()), &dialog, SLOT(accept()));
        layout->addWidget(okButton);

        dialog.exec();
        // Clear fileNames to store file names in user set order.
        fileNames.clear();

        for (int index = 0; index < listWidget->count(); index++)
        {
            QListWidgetItem *item = listWidget->item(index);
            fileNames.push_back(item->text());
        }

        listWidget->close();
        delete layout;
        delete listWidget;

        pathsToLoadedImages.resize(numLevels);
        // Adapt ticks and labels to the new amount of textures.
        generateBarGeometry();
        loadImagesFromPaths(fileNames);
        loadedImages.clear();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(QString("Amount of selceted files is not enough.\n"
                               "You have to select at least 2 image "
                               "files."));
        msgBox.exec();
        return;
    }
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MSpatial1DTransferFunction::generateTransferTexture()
{
    if (!loadedImages.empty())
    {
        generateTransferTexture(0, true);
        for (int level = 1; level < loadedImages.size(); level++)
        {
            generateTransferTexture(level, false);
        }
        loadedImages.clear();
    }
}



void MSpatial1DTransferFunction::renderToCurrentContextUiLayer(
        MSceneViewGLWidget *sceneView)
{
    Q_UNUSED(sceneView);

    if (tfTexture != nullptr)
    {
        tfTexture->bindToTextureUnit(textureUnit);
        int viewPortWidth = sceneView->getSceneResolutionWidth();
        int viewPortHeight = sceneView->getSceneResolutionHeight();

        QRectF positionRect = positionProp;
        float barWidth     = positionRect.width();
        float barHeight    = positionRect.height();

        // First draw the colourbar itself. glPolygonOffset is used to displace
        // the colourbar's z-value slightly to the back, to that the frame drawn
        // afterwards is rendered correctly.
        colourbarShader->bindProgram("spatialTF");

        colourbarShader->setUniformValue("distInterp",
                                         GLfloat(interpolationRangeProp));

        colourbarShader->setUniformValue("minimumValue",
                                         GLfloat(minimumValue));
        colourbarShader->setUniformValue("maximumValue",
                                         GLfloat(maximumValue));

        colourbarShader->setUniformValue("viewPortWidth",
                                         GLint(viewPortWidth));
        colourbarShader->setUniformValue("viewPortHeight",
                                         GLint(viewPortHeight));
        colourbarShader->setUniformValue("textureWidth",
                                         GLint(currentTextureWidth));
        colourbarShader->setUniformValue("textureHeight",
                                         GLint(currentTextureHeight));
        colourbarShader->setUniformValue("barWidthF",
                                         GLfloat(barWidth));
        colourbarShader->setUniformValue("barHeightF",
                                         GLfloat(barHeight));

        colourbarShader->setUniformValue("numLevels",
                                         GLint(numLevels));

        colourbarShader->setUniformValue("alphaBlendingMode",
                                         GLenum(alphaBlendingMode));
        colourbarShader->setUniformValue("invertAlpha",
                                         GLboolean(invertAlphaProp));
        colourbarShader->setUniformValue("useConstantColour",
                                         GLboolean(useConstantColourProp));
        colourbarShader->setUniformValue("constantColour", constantColourProp);
        colourbarShader->setUniformValue("useWhiteBgForBar",
                                         GLboolean(useWhiteBgForBarProp));

        colourbarShader->setUniformValue("spatialTransferTexture", textureUnit);CHECK_GL_ERROR;

        vertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE, 3, false,
                                              5 * sizeof(float),  
                                              (const GLvoid*)(0 * sizeof(float)));CHECK_GL_ERROR;
        vertexBuffer->attachToVertexAttribute(SHADER_TEXTURE_ATTRIBUTE, 2, false,
                                              5 * sizeof(float),
                                              (const GLvoid*)(3 * sizeof(float)));CHECK_GL_ERROR;

        glPolygonOffset(.01f, 1.0f);
        glEnable(GL_POLYGON_OFFSET_FILL);
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, numVertices); CHECK_GL_ERROR;
        glDisable(GL_POLYGON_OFFSET_FILL);
    }

    // Next draw a black frame around the colourbar.
    simpleGeometryShader->bindProgram("Simple"); CHECK_GL_ERROR;
    simpleGeometryShader->setUniformValue("colour", QColor(0, 0, 0)); CHECK_GL_ERROR;
    vertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE, 3, false,
                                          5 * sizeof(float),
                                          (const GLvoid*)(10 * sizeof(float)));

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
    sceneView->setLineWidth(1);
    glDrawArrays(GL_LINE_LOOP, 0, numVertices); CHECK_GL_ERROR;

    // Finally draw the tick marks.
    vertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE, 3, false,
                                          0,
                                          (const GLvoid*)(30 * sizeof(float)));


    glDrawArrays(GL_LINES, 0, 2 * numTicks); CHECK_GL_ERROR;

    // Unbind VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
}


void MSpatial1DTransferFunction::generateTransferTexture(int level, bool recreate)
{
    if (!loadedImages.at(level).isNull())
    {
        MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

        // Upload the texture to GPU memory:
        if (tfTexture == nullptr)
        {
            // No texture exists. Create a new one and register with memory manager.
            QString textureID = QString("spatialTransferFunction_#%1").arg(getID());
            tfTexture = new GL::MTexture(textureID, GL_TEXTURE_2D_ARRAY,
                                         GL_RGBA32F,
                                         currentTextureWidth,
                                         currentTextureHeight,
                                         numLevels);

            if ( !glRM->tryStoreGPUItem(tfTexture) )
            {
                delete tfTexture;
                tfTexture = nullptr;
            }


            if (useMirroredRepeat)
            {
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
            }
            else
            {
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S,
                                GL_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T,
                                GL_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R,
                                GL_REPEAT); CHECK_GL_ERROR;
            }
            glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER,
                            GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER,
                            GL_LINEAR_MIPMAP_LINEAR); CHECK_GL_ERROR;

            glRM->makeCurrent();
            tfTexture->bindToTextureUnit(0);

            glTexImage3D(GL_TEXTURE_2D_ARRAY,            // target
                         0,                         // level of detail
                         GL_RGBA32F,                // internal format
                         currentTextureWidth,       // width
                         currentTextureHeight,      // height
                         numLevels,                 // depth
                         0,                         // border
                         GL_RGBA,                   // format
                         GL_UNSIGNED_BYTE,          // data type of the pixel data
                         NULL); CHECK_GL_ERROR;
        }

        if (recreate)
        {
            tfTexture->updateSize(currentTextureWidth, currentTextureHeight,
                                  numLevels);
            glRM->makeCurrent();
            tfTexture->bindToTextureUnit(0);

            if (useMirroredRepeat)
            {
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R,
                                GL_MIRRORED_REPEAT); CHECK_GL_ERROR;
            }
            else
            {
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_S,
                                GL_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_T,
                                GL_REPEAT); CHECK_GL_ERROR;
                glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_WRAP_R,
                                GL_REPEAT); CHECK_GL_ERROR;
            }
            glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MAG_FILTER,
                            GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D_ARRAY, GL_TEXTURE_MIN_FILTER,
                            GL_LINEAR_MIPMAP_LINEAR); CHECK_GL_ERROR;


            glTexImage3D(GL_TEXTURE_2D_ARRAY,            // target
                         0,                         // level of detail
                         GL_RGBA32F,                // internal format
                         currentTextureWidth,       // width
                         currentTextureHeight,      // height
                         numLevels,                 // depth
                         0,                         // border
                         GL_RGBA,                   // format
                         GL_UNSIGNED_BYTE,          // data type of the pixel data
                         NULL); CHECK_GL_ERROR;
        }

        if ( tfTexture )
        {
            tfTexture->updateSize(currentTextureWidth, currentTextureHeight,
                                  numLevels);

            glRM->makeCurrent();
            tfTexture->bindToTextureUnit(0);

            // Upload data array to GPU.
            glTexSubImage3D(GL_TEXTURE_2D_ARRAY,             // target
                            0,                         // level of detail
                            0,                         // xoffset
                            0,                         // yoffset
                            level,                     // zoffset
                            currentTextureWidth,       // width
                            currentTextureHeight,      // height
                            1,                         // depth
                            GL_RGBA,                   // format
                            GL_UNSIGNED_BYTE,          // data type of the pixel data
                            loadedImages.at(level).bits()); CHECK_GL_ERROR;

//             glGenerateTextureMipmap(tfTexture->getTextureObject());
            glGenerateMipmap(GL_TEXTURE_2D_ARRAY);
        }
    }
}


void MSpatial1DTransferFunction::generateBarGeometry()
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    bool horizontal = orientation == HORIZONTAL;

    // Create geometry for a box filled with the colourbar texture.

    // Get user-defined upper left corner x, y, z and width, height in clip
    // space.
    QRectF positionRect = positionProp;
    float ulcrnr[3] = { float(positionRect.x()),
                        float(positionRect.y()),
                        -1. };
    float lrcrnr[3] = { float(positionRect.x() + positionRect.width()),
                        float(positionRect.y() - positionRect.height()),
                        -1. };
    float width     = positionRect.width();
    float height    = positionRect.height();

    // This array accomodates the geometry for two filled triangles showing the
    // actual colourbar (GL_TRIANGLE_STRIP). The 3rd, 4th, and the additional
    // 5th and 6th vertices are used to draw a box around the colourbar
    // (GL_LINE_LOOP).
    float coordinates[30] =
    {
        ulcrnr[0]        , ulcrnr[1]         , ulcrnr[2], 1, 0, // ul
        ulcrnr[0]        , ulcrnr[1] - height, ulcrnr[2], 0, 0, // ll
        ulcrnr[0] + width, ulcrnr[1]         , ulcrnr[2], 1, 1, // ur
        ulcrnr[0] + width, ulcrnr[1] - height, ulcrnr[2], 0, 1, // lr
        ulcrnr[0]        , ulcrnr[1] - height, ulcrnr[2], 0, 0, // ll for frame
        ulcrnr[0]        , ulcrnr[1]         , ulcrnr[2], 1, 0, // ul for frame
    };

    // Update UV coordinates for horizontal rendering.
    if (horizontal)
    {
        coordinates[3] = 0;
        coordinates[8] = 0;
        coordinates[13] = 1;
        coordinates[18] = 1;
    }

    // ========================================================================
    // Next, generate the tickmarks. maxNumTicks tickmarks are drawn, but not
    // more than texture levels.
    int maxNumTicks = maxNumTicksProp;
    numTicks = std::min(numLevels + 1, maxNumTicks);

    // This array accomodates the tickmark geometry.
	// float tickmarks[6 * numTicks]; VLAs not supported in msvc
	float* tickmarks = new float[6 * numTicks];

    // Width of the tickmarks in clip space.
    float tickwidth = tickWidthProp;

    int n = 0;

    if (horizontal)
    {
        // Treat numTicks equals 1 as a special case to avoid divison by zero.
        if (numTicks != 1)
        {
            for (uint i = 0; i < numTicks; i++)
            {
                tickmarks[n++] = lrcrnr[0] - i * (width / (numTicks - 1));
                tickmarks[n++] = lrcrnr[1];
                tickmarks[n++] = lrcrnr[2];
                tickmarks[n++] = lrcrnr[0] - i * (width / (numTicks - 1));
                tickmarks[n++] = lrcrnr[1] - tickwidth;
                tickmarks[n++] = lrcrnr[2];
            }
        }
        else
        {
            tickmarks[n++] = lrcrnr[0];
            tickmarks[n++] = lrcrnr[1];
            tickmarks[n++] = lrcrnr[2];
            tickmarks[n++] = lrcrnr[0];
            tickmarks[n++] = lrcrnr[1] - tickwidth;
            tickmarks[n++] = lrcrnr[2];
        }
    }
    else
    {
        // Treat numTicks equals 1 as a special case to avoid divison by zero.
        if (numTicks != 1)
        {
            for (uint i = 0; i < numTicks; i++)
            {
                tickmarks[n++] = ulcrnr[0];
                tickmarks[n++] = ulcrnr[1] - i * (height / (numTicks - 1));
                tickmarks[n++] = ulcrnr[2];
                tickmarks[n++] = ulcrnr[0] - tickwidth;
                tickmarks[n++] = ulcrnr[1] - i * (height / (numTicks - 1));
                tickmarks[n++] = ulcrnr[2];
            }
        }
        else
        {
            tickmarks[n++] = ulcrnr[0];
            tickmarks[n++] = ulcrnr[1];
            tickmarks[n++] = ulcrnr[2];
            tickmarks[n++] = ulcrnr[0] - tickwidth;
            tickmarks[n++] = ulcrnr[1];
            tickmarks[n++] = ulcrnr[2];
        }
    }

    // ========================================================================
    // Now we can upload the two geometry arrays to the GPU. Switch to the
    // shared background context so all views can access the VBO generated
    // here.
    glRM->makeCurrent();

    QString requestKey = QString("vbo_transfer_function_actor_%1").arg(getID());

    GL::MVertexBuffer* vb =
            static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));

    if (vb)
    {
        vertexBuffer = vb;
        GL::MFloatVertexBuffer* buf = dynamic_cast<GL::MFloatVertexBuffer*>(vb);
        // reallocate buffer if size has changed
        buf->reallocate(nullptr, 30 + numTicks * 6);
        buf->update(coordinates, 0, 0, sizeof(coordinates));
        buf->update(tickmarks, 0, sizeof(coordinates),
                    sizeof(float) * 6 * numTicks);
    }
    else
    {
        GL::MFloatVertexBuffer* newVB = nullptr;
        newVB = new GL::MFloatVertexBuffer(requestKey, 30 + numTicks * 6);
        if (glRM->tryStoreGPUItem(newVB))
        {
            newVB->reallocate(nullptr, 30 + numTicks * 6, 0, true);
            newVB->update(coordinates, 0, 0, sizeof(coordinates));
            newVB->update(tickmarks, 0, sizeof(coordinates),
                          sizeof(float) * 6 * numTicks);
        }
        else
        {
            delete newVB;
        }
        vertexBuffer = static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));
    }

    // Required for the glDrawArrays() call in renderToCurrentContext().
    numVertices = 4;

    // ========================================================================
    // Finally, place labels at the tickmarks:

    minimumValue = minimumValueProp;
    maximumValue = maximumValueProp;
    int maxNumLabels = maxNumLabelsProp;

    // Obtain a shortcut to the application's text manager to register the
    // labels generated in the loops below.
    MTextManager* tm = glRM->getTextManager();

    // Remove all text labels of the old geometry.
    while (!labels.isEmpty())
    {
        tm->removeText(labels.takeLast());
    }

    // Draw no labels if either numTicks or maxNumLabels equal 0.
    if (numTicks == 0 || maxNumLabels == 0)
    {
        return;
    }

    // A maximum of maxNumLabels are placed. The approach taken here is to
    // compute a "tick step size" from the number of ticks drawn and the
    // maximum number of labels to be drawn. A label will then be placed
    // every tickStep-th tick. The formula tries to place a label at the
    // lower and upper end of the colourbar, if possible.
    int tickStep = ceil(double(numTicks - 1) / double(maxNumLabels - 1));

    float x = tickmarks[3];
    float y = tickmarks[4];
    float z = tickmarks[5];

    MTextManager::TextAnchor anchor = horizontal ? MTextManager::UPPERCENTRE : MTextManager::MIDDLERIGHT;

    // Register the labels with the text manager.
    // Treat numTicks equals 1 as a special case to avoid divison by zero.
    if (numTicks != 1)
    {
        for (uint i = 0; i < numTicks; i += tickStep)
        {
            x = tickmarks[6*i + 3];
            y = tickmarks[6*i + 4];
            z = tickmarks[6*i + 5];

            if (horizontal)
            {
                y -= labelSpacingProp;
            }
            else
            {
                x -= labelSpacingProp;
            }

            float value = maximumValue - float(i) / float(numTicks - 1)
                    * (maximumValue - minimumValue);
            QString labelText =
                    minimumValueProp.getValueAsText(value);
            labels.append(tm->addText(
                                  labelText,
                                  MTextManager::CLIPSPACE,
                                  x,
                                  y,
                                  z,
                                  labelProps.fontSizeProp,
                                  labelProps.fontColourProp, anchor,
                                  labelProps.enableBBoxProp, labelProps.bboxColourProp)
                    );
        }
    }
    else
    {
        if (horizontal)
        {
            y -= labelSpacingProp;
        }
        else
        {
            x -= labelSpacingProp;
        }

        QString labelText =
                maximumValueProp.getValueAsText();
        labels.append(tm->addText(
                              labelText,
                              MTextManager::CLIPSPACE,
                              x,
                              y,
                              z,
                              labelProps.fontSizeProp,
                              labelProps.fontColourProp, anchor,
                              labelProps.enableBBoxProp, labelProps.bboxColourProp)
                      );
    }

    if (!descriptionLabelProp.value().isEmpty())
    {
        QString labelText = descriptionLabelProp;
        anchor = MTextManager::UPPERCENTRE;
        float x = positionRect.x() + width + labelSpacingProp;
        float y = positionRect.y() - height / 2.0;
        float z = -1;
        float rotation = 90.0f;

        if (horizontal)
        {
            anchor = MTextManager::LOWERCENTRE;

            x = positionRect.x() + width / 2.0;
            y = positionRect.y() + labelSpacingProp;

            rotation = 0;
        }
        else
        {
            if (flipDescriptionProp)
            {
                anchor = MTextManager::LOWERCENTRE;
                rotation += 180.0f;
            }
        }

        labels.append(tm->addText(labelText,
                                  MTextManager::CLIPSPACE,
                                  x, y, z,
                                  labelProps.fontSizeProp,
                                  labelProps.fontColourProp,
                                  anchor,
                                  labelProps.enableBBoxProp,
                                  labelProps.bboxColourProp,
                                  0.1,
                                  rotation));
    }

	delete[] tickmarks;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MSpatial1DTransferFunction::loadImagesFromPaths(const QStringList& pathList)
{
    if (!pathList.empty())
    {
        int newWidth  = 0;
        int newHeight = 0;
        int level = 0;
        bool setSize = true;

        pathsToLoadedImages.clear();
        loadedImages.clear();

        for (const QString &path : pathList)
        {
            if (path != "")
            {
                QImage image(path);
                if (!image.isNull())
                {
                    if (setSize)
                    {
                        newWidth  = image.width();
                        newHeight = image.height();
                        setSize = false;
                    }

                    if (image.width() != newWidth || image.height() != newHeight)
                    {
                        QMessageBox msgBox;
                        msgBox.setIcon(QMessageBox::Warning);
                        msgBox.setText(
                                    QString("Selected images don't have the "
                                            "same size.\n"
                                            "Aborting texture generation."));
                        msgBox.exec();
                        loadedImages.clear();
                        return;
                    }

                    loadedImages.append(QGLWidget::convertToGLFormat(image));

                    pathsToLoadedImages.insert(level, path);
                    level++;
                }
                else
                {
                    QMessageBox msgBox;
                    msgBox.setIcon(QMessageBox::Warning);
                    msgBox.setText(QString("Image '%1':\n does not exist.\n"
                                           "No image was loaded.").arg(path));
                    msgBox.exec();
                }
            }

        }

        if (loadedImages.size() >= 2)
        {
            numLevels = loadedImages.size();
            currentTextureWidth  = newWidth;
            currentTextureHeight = newHeight;

            QString text("");

            // Construct String to store all paths in one property.
            for (int i = 0; i < (numLevels - 1); i++)
            {
                text.append(QString("%1: ").arg(i) + pathsToLoadedImages.at(i)
                            + "; \n");
            }
            text.append(QString("%1: ").arg(numLevels - 1)
                        + pathsToLoadedImages.at(numLevels - 1));

            pathToLoadedImagesProperty.suppressValueEvent(true);
            pathToLoadedImagesProperty = text;
            pathToLoadedImagesProperty.suppressValueEvent(false);
            pathToLoadedImagesProperty.setTooltip(text);

            if (suppressActorUpdates()) return;

            // Initialize texture with width and height of images.
            generateTransferTexture(0, true);
            // Fill texture with all given images.
            for (int l = 1; l < numLevels; l++)
            {
                generateTransferTexture(l, false);
            }
            emitActorChangedSignal();
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText(QString("Could not create enough images.\n"
                                   "Therefore no texture was created."));
            msgBox.exec();
        }
    }
}
QString MSpatial1DTransferFunction::formatPathList(const QStringList &list)
{
    int numLevels = list.size();

    QString text("");

    // Construct String to store all paths in one property.
    for (int i = 0; i < (numLevels - 1); i++)
    {
        text.append(QString("%1: ").arg(i) + list.at(i)
                            + "; \n");
    }
    text.append(QString("%1: ").arg(numLevels - 1)
                        + list.at(numLevels - 1));

    return text;
}
QStringList MSpatial1DTransferFunction::pathsFromFormattedString(
        const QString &formattedString)
{
    QStringList paths = formattedString.split("; ");
    QStringList newPaths;

    for (QString &path : paths)
    {
        int split = path.indexOf(": ");
        path = path.remove(0, split + 2);
        newPaths.append(path);
    }
    return newPaths;
}

} // namespace Met3D
