/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Alexander Klassen
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MET_3D_GEOMETRYACTOR_H
#define MET_3D_GEOMETRYACTOR_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>
#include <QVector2D>
#include <QVector3D>
#include <QtCore>

// local application imports
#include "data/objreader.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/vertexbuffer.h"
#include "gxfw/mactor.h"
#include "gxfw/nwpmultivaractor.h"
#include "actors/components/mtransformcomponent.h"
#include "actors/transferfunction1d.h"
#include "gxfw/properties/mproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mfileproperty.h"
#include "gxfw/properties/mtransferfunctionproperty.h"
#include "gxfw/properties/mnwpactorvarproperty.h"

namespace Met3D
{
class MSceneViewGLWidget;

/**
  @brief MGeometryActor draws a 3D mesh from an OBJ file onto the scene.
  By default, the z axis of the mesh is treated as world coordinates,
  but can be interpreted as pressure or converted from meters to pressure too.
  */
class MGeometryActor : public MNWPMultiVarActor
{
Q_OBJECT
public:
    enum VerticalCoordSpace
    {
        HPA = 0,
        M = 1,
        KM = 2,
        Z = 3
    };

    MGeometryActor();

    ~MGeometryActor() override;


    static QString staticActorType()
    { return "Geometry"; }


    void reloadShaderEffects() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "geometry"; }


    static QString staticIconFileName() { return "geometry.png"; }


    void dragEvent(MSceneViewGLWidget *sceneView, int handleID, float clipX,
                   float clipY) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    QList<MVerticalLevelType> supportedLevelTypes() override;

    MNWPActorVariable *
    createActorVariable(const MSelectableDataVariable &dataVariable) override;

public slots:

    /**
      Called by the mesh reader class when OBJ has been parsed.
     */
    void asynchronousDataAvailable(Met3D::MDataRequest request);

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    void uploadGeometryToGPU(float id);

    void requestMesh(const QString &filename);

    void onDeleteActorVariable(MNWPActorVariable *variable) override;

    void onAddActorVariable(MNWPActorVariable *variable) override;

    void onChangeActorVariable(MNWPActorVariable *variable) override;

    void setVarSpecificShaderVars(
            std::shared_ptr<GL::MShaderEffect>& shader,
            MSceneViewGLWidget* sceneView,
            const QString& structName,
            const QString& volumeName,
            const QString& transferFuncName,
            const QString& pressureTableName,
            const QString& surfacePressureName,
            const QString& hybridCoeffName,
            const QString& lonLatLevAxesName,
            const QString& pressureTexCoordTable2DName,
            const QString& auxPressureField3DName);

    std::shared_ptr<GL::MShaderEffect> simpleGeometryEffect;

    MEnumProperty verticalCoordSpaceProp;
    MFileProperty fileNameProp;
    MFileProperty attFileNameProp;
    MTransferFunction1DProperty transferFunctionProp;
    MNWPActorVarProperty varProp;

    struct MeshRenderData
    {
        GL::MVertexBuffer *vertexBuffer = nullptr;
        GL::MVertexBuffer *normalBuffer = nullptr;
        GL::MVertexBuffer *uvBuffer = nullptr;
        GL::MVertexBuffer *attribBuffer = nullptr;
        GLuint indexBuffer = -1;
    };

    /**
     * Map of scene vertical scales to mesh render data structs.
     * Each scene, this actor is part of, can have a different vertical scaling.
     * When rendering meshes on pressure or m z coordinates, we need to convert
     * them, per scene scaling, to the scene z coordinates.
     */
    QMap<float, MeshRenderData> buffers;

    MTransferFunction1D *spatialTransferFunction;
    int textureUnitSpatialTransferFunction;

    int texUnitShadowMap;

    MObjReader *meshReader;

    /**
     * Map of scene vertical scales to meshes.
     * Each scene, this actor is part of, can have a different vertical scaling.
     * When rendering meshes on pressure or m z coordinates, we need to convert
     * them, per scene scaling, to the scene z coordinates.
     */
    QMap<float, MMesh*> meshes;

    /**
     * Map of scene vertical scales to the mesh data requests.
     * Each scene, this actor is part of, can have a different vertical scaling.
     * When rendering meshes on pressure or m z coordinates, we need to convert
     * them, per scene scaling, to the scene z coordinates.
     */
    QMap<float, MDataRequest> currentRequests;

    std::shared_ptr<MTransformComponent> transformComponent;
};

} // namespace Met3D

#endif //MET_3D_GEOMETRYACTOR_H
