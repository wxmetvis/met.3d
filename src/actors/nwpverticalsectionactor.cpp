/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2024      Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "nwpverticalsectionactor.h"

// standard library imports
#include <iostream>
#include <cmath>

// related third party imports
#include <QObject>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "data/structuredgrid.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "trajectories/pointsource/pointgeneratorinterfacesettings.h"
#include "trajectories/pointsource/pointsourcefactory.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWPVerticalSectionActor::MNWPVerticalSectionActor()
    : MNWPMultiVarActor(),
      MBoundingBoxInterface(this, MBoundingBoxConnectionType::VERTICAL),
      MPointGeneratorInterface(MPointGeneratorInterfaceType::ACTOR),
      waypointsModel(nullptr),
      modifyWaypoint(-1),
      modifyWaypoint_worldZ(0.),
      textureVerticalSectionPath(nullptr),
      textureUnitVerticalSectionPath(-1),
      texturePressureLevels(nullptr),
      textureUnitPressureLevels(-1),
      textureUnitShadowMap(-1),
      vbVerticalWaypointLines(nullptr),
      numVerticesVerticalWaypointLines(0),
      vbInteractionHandlePositions(nullptr),
      numInteractionHandlePositions(0),
      vbFlightTrackWaypointPositions(nullptr),
      numFlightTrackWaypointPositions(0),
      numFlightTrackWaypointVertices(0),
      p_top_hPa(100.),
      p_bot_hPa(1050.),
      updatePath(false),
      offsetPickPositionToHandleCentre(QVector2D(0., 0.))
{
    enablePicking(true);

    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    auto labelCallback = [=]()
    {
        if (suppressActorUpdates()) return;

        generateLabels();
        emitActorChangedSignal();
    };

    labelDistanceProp = MIntProperty("Distance (in tick marks)", 1);
    labelDistanceProp.setConfigKey("label_distance_in_tick_marks");
    labelDistanceProp.setMinimum(0);
    labelDistanceProp.setTooltip("Depends on order in pressure levels list.");
    labelDistanceProp.registerValueCallback(labelCallback);
    labelProps.enabledProp.addSubProperty(labelDistanceProp);

    QStringList waypointsList =  MSystemManagerAndControl::getInstance()
            ->getWaypointsModelsIdentifiers();
    waypointsModelProp = MEnumProperty("Waypoints", waypointsList);
    waypointsModelProp.setConfigKey("waypoints");

    // Set default waypoints to second entry if there exists a second entry.
    if (waypointsList.size() > 1)
    {
        enableActorUpdates(false);
        waypointsModelProp.setValue(1);
        setWaypointsModel(MSystemManagerAndControl::getInstance()
                          ->getWaypointsModel(waypointsModelProp.getSelectedEnumName()));
        enableActorUpdates(true);
    }

    waypointsModelProp.registerValueCallback([=]()
    {
        QString wpID = waypointsModelProp.getSelectedEnumName();
        setWaypointsModel(MSystemManagerAndControl::getInstance()
                                  ->getWaypointsModel(wpID));

        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(waypointsModelProp);

    // Bounding box of the actor.
    insertBoundingBoxProperty(&actorPropertiesSupGroup);

    QString defaultPressureLineLevel = QString("1000.,900.,800.,700.,600.,500.")
                                       + QString(",400.,300.,200.,100.,90.,80.")
                                       + QString(",70.,60.,50.,40.,30.,20.");

    pressureLineLevelsProp = MStringProperty("Lines at pressure levels", defaultPressureLineLevel);
    pressureLineLevelsProp.registerValueCallback([=]()
    {
        selectedPressureLineLevels = parseFloatRangeString(pressureLineLevelsProp);

        if (suppressActorUpdates()) return;

        generateIsoPressureLines();
        generateLabels();
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(pressureLineLevelsProp);

    selectedPressureLineLevels = parseFloatRangeString(defaultPressureLineLevel);

    opacityProp = MFloatProperty("Opacity", 1.0f);
    opacityProp.setConfigKey("opacity");
    opacityProp.setMinMax(0, 1);
    opacityProp.setDecimals(2);
    opacityProp.setStep(0.05);
    opacityProp.setSuffix(" (0-1)");
    opacityProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(opacityProp);

    interpolationNodeSpacingProp = MFloatProperty("Interpolation node spacing", 0.15f);
    interpolationNodeSpacingProp.setConfigKey("interpolation_node_spacing");
    interpolationNodeSpacingProp.setMinMax(0.000001, 180);
    interpolationNodeSpacingProp.setDecimals(6);
    interpolationNodeSpacingProp.setStep(0.05);
    interpolationNodeSpacingProp.setSuffix(" °");
    interpolationNodeSpacingProp.registerValueCallback([=]()
    {
        targetGridToBeUpdated = true;

        if (suppressActorUpdates()) return;

        updatePath = true;
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(interpolationNodeSpacingProp);

    enableFlightTrackProp = MBoolProperty("Flight track", false);
    enableFlightTrackProp.setConfigKey("flight_track_enabled");
    enableFlightTrackProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(enableFlightTrackProp);

    flightTrackLineColourProp = MColorProperty("Line colour", QColor(0, 0, 255));
    flightTrackLineColourProp.setConfigKey("flight_track_line_colour");
    flightTrackLineColourProp.toggleAlpha(false);
    flightTrackLineColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    enableFlightTrackProp.addSubProperty(flightTrackLineColourProp);

    flightTrackLineThicknessProp = MFloatProperty("Line thickness", 2);
    flightTrackLineThicknessProp.setConfigKey("flight_track_line_thickness");
    flightTrackLineThicknessProp.setMinMax(1, 10);
    flightTrackLineThicknessProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    enableFlightTrackProp.addSubProperty(flightTrackLineThicknessProp);

    flightTrackAreaColorProp = MColorProperty("Area colour", QColor(0, 0, 255, 100));
    flightTrackAreaColorProp.setConfigKey("flight_track_area_colour");
    flightTrackAreaColorProp.toggleAlpha(true);
    flightTrackAreaColorProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    enableFlightTrackProp.addSubProperty(flightTrackAreaColorProp);

    labelProps.fontSizeProp.registerValueCallback(labelCallback);
    labelProps.fontColourProp.registerValueCallback(labelCallback);
    labelProps.enableBBoxProp.registerValueCallback(labelCallback);
    labelProps.bboxColourProp.registerValueCallback(labelCallback);

    MPointSourceFactory::getInstance()
            ->registerPointGeneratingInterface<MVSecPointGeneratorSettings>(this);
}


MNWPVerticalSectionActor::~MNWPVerticalSectionActor()
{
    if (textureUnitVerticalSectionPath >=0)
        releaseTextureUnit(textureUnitVerticalSectionPath);
    if (textureUnitPressureLevels >=0)
        releaseTextureUnit(textureUnitPressureLevels);
    if (textureUnitShadowMap >= 0)
        releaseTextureUnit(textureUnitShadowMap);

    if (vbVerticalWaypointLines) delete vbVerticalWaypointLines;
    if (vbInteractionHandlePositions) delete vbInteractionHandlePositions;

    MPointSourceFactory::getInstance()->deregisterPointGeneratingInterface(this);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE 0

void MNWPVerticalSectionActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");

    beginCompileShaders(5);

    compileShadersFromFileWithProgressDialog(
                sectionGridShader,
                "src/glsl/vsec_interpolation_filledcontours.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                marchingSquaresShader,
                "src/glsl/vsec_marching_squares.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                pressureLinesShader,
                "src/glsl/vsec_pressureisolines.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                simpleGeometryShader,
                "src/glsl/simple_coloured_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                positionSpheresShader,
                "src/glsl/trajectory_positions.fx.glsl");

    endCompileShaders();
}


void MNWPVerticalSectionActor::saveConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::saveConfiguration(settings);
    MBoundingBoxInterface::saveConfiguration(settings);
}


void MNWPVerticalSectionActor::loadConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::loadConfiguration(settings);
    MBoundingBoxInterface::loadConfiguration(settings);
}


void MNWPVerticalSectionActor::loadConfigurationPrior_V_1_14(
        QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationPrior_V_1_14(settings);
    MBoundingBoxInterface::loadConfiguration(settings);

    settings->beginGroup(MNWPVerticalSectionActor::getSettingsID());

    labelDistanceProp = settings->value("labelDistance", 1).toInt();

    QString wpID = settings->value("waypointsModelID").toString();
    setWaypointsModel(MSystemManagerAndControl::getInstance()
                              ->getWaypointsModel(wpID));

    MBoundingBoxInterface::loadConfiguration(settings);

    QString defaultPressureLineLevel = QString("1000.,900.,800.,700.,600.,500.")
            + QString(",400.,300.,200.,100.,90.,80.")
            + QString(",70.,60.,50.,40.,30.,20.");
    const QString pressureLevels =
            settings->value("pressureLevels",
                            defaultPressureLineLevel).toString();
    pressureLineLevelsProp.setValue(pressureLevels);
    opacityProp = settings->value("opacity").toFloat();
    interpolationNodeSpacingProp =
            settings->value("interpolationNodeSpacing", 0.15).toFloat();

    settings->endGroup();

    if (this->isInitialized())
    {
        generateIsoPressureLines();
    }
}


int MNWPVerticalSectionActor::checkIntersectionWithHandle(
        MSceneViewGLWidget *sceneView,
        float clipX, float clipY)
{
    // See notes 22-23Feb2012 and 21Nov2012.

    if (waypointsModel == nullptr) return -1;

    float clipRadius = sceneView->getHandleSize();

    // cout << "checkIntersection(" << clipX << ", " << clipY << " / "
    //      << clipRadius << ")\n" << flush;

    // NOTE: This function considers both waypoints and midpoints between the
    // waypoints. If the user drags a midpoint, both adjacent waypoints are
    // moved. i.e. the entire segment.

    // Default: No waypoint has been touched by the mouse. Note: This instance
    // variable is used in renderToCurrentContext; if it is >= 0 the waypoint
    // with the corresponding index is highlighted.
    modifyWaypoint = -1;

    // Loop over all way-/midpoints and check whether the mouse cursor is inside
    // a circle with radius "clipRadius" around the waypoint (in clip space).
    for (int i = 0; i < waypointsModel->sizeIncludingMidpoints(); i++)
    {
        // Transform the waypoint coordinates to clip space. As only lat/lon
        // of the waypoint is stored, assume a worldZ = 0.
        QVector3D wpPositionBottom = QVector3D(
                waypointsModel->positionLonLatIncludingMidpoints(i),
                sceneView->worldZfromPressure(p_bot_hPa));
        QVector3D wpPositionTop = QVector3D(
                waypointsModel->positionLonLatIncludingMidpoints(i),
                sceneView->worldZfromPressure(p_top_hPa));

        // Obtain the camera position and the view direction
        const QVector3D& cameraPos = sceneView->getCamera()->getOrigin();
        QVector3D viewDirBot = wpPositionBottom - cameraPos;
        QVector3D viewDirTop = wpPositionTop - cameraPos;

        // Scale the radius (in world space) with respect to the viewer distance
        float radiusBot = static_cast<float>(clipRadius * viewDirBot.length() / 100.0);
        float radiusTop = static_cast<float>(clipRadius * viewDirTop.length() / 100.0);

        QMatrix4x4 *mvpMatrixInverted =
                sceneView->getModelViewProjectionMatrixInverted();
        // Compute the world position of the current mouse position
        QVector3D mouseWorldPos = *mvpMatrixInverted * QVector3D(clipX, clipY, 1);

        // Get the ray direction from the camera to the mouse position
        QVector3D l = mouseWorldPos - cameraPos;
        l.normalize();

        // Compute (o - c) // ray origin (o) - sphere center (c)
        QVector3D ocBot = cameraPos - wpPositionBottom;
        QVector3D ocTop = cameraPos - wpPositionTop;
        // Length of (o - c) = || o - c ||
        float lenOCBot = static_cast<float>(ocBot.length());
        float lenOCTop = static_cast<float>(ocTop.length());
        // Compute l * (o - c)
        float locBot = static_cast<float>(QVector3D::dotProduct(l, ocBot));
        float locTop = static_cast<float>(QVector3D::dotProduct(l, ocTop));

        // Solve equation:
        // d = - (l * (o - c) +- sqrt( (l * (o - c))² - || o - c ||² + r² )
        // Since the equation can be solved only if root discriminant is >= 0
        // just compute the discriminant
        float rootBot = locBot * locBot - lenOCBot * lenOCBot + radiusBot * radiusBot;
        float rootTop = locTop * locTop - lenOCTop * lenOCTop + radiusTop * radiusTop;


        // If root discriminant is positive or zero, there's an intersection
        if (rootBot >= 0)
        {
            modifyWaypoint = i;
            modifyWaypoint_worldZ = wpPositionBottom.z();
            QMatrix4x4 *mvpMatrix = sceneView->getModelViewProjectionMatrix();
            QVector3D posPoleClip = *mvpMatrix * wpPositionBottom;
            offsetPickPositionToHandleCentre = QVector2D(posPoleClip.x() - clipX,
                                     posPoleClip.y() - clipY);
            break;
        }
        else if (rootTop >= 0)
        {
            modifyWaypoint = i;
            modifyWaypoint_worldZ = wpPositionTop.z();
            QMatrix4x4 *mvpMatrix = sceneView->getModelViewProjectionMatrix();
            QVector3D posCentreClip = *mvpMatrix * wpPositionTop;
            offsetPickPositionToHandleCentre = QVector2D(posCentreClip.x() - clipX,
                                     posCentreClip.y() - clipY);
            break;
        }

    } // for (waypoints)

    return modifyWaypoint;
}


void MNWPVerticalSectionActor::addPositionLabel(MSceneViewGLWidget *sceneView,
                                                int handleID,
                                                float clipX, float clipY)
{
    if (waypointsModel == nullptr) return;

    // Select an arbitrary z-value to construct a point in clip space that,
    // transformed to world space, lies on the ray passing through the camera
    // and the location on the worldZ==0 plane "picked" by the mouse.
    // (See notes 22-23Feb2012).
    QVector3D mousePosClipSpace = QVector3D(clipX, clipY, modifyWaypoint_worldZ);

    // The point p at which the ray intersects the worldZ==0 plane is found by
    // computing the value d in p=d*l+l0, where l0 is a point on the ray and l
    // is a vector in the direction of the ray. d can be found with
    //        (p0 - l0) * n
    //   d = ----------------
    //            l * n
    // where p0 is a point on the worldZ==0 plane and n is the normal vector
    // of the plane.
    //       http://en.wikipedia.org/wiki/Line-plane_intersection

    // To compute l0, the MVP matrix has to be inverted.
    QMatrix4x4 *mvpMatrixInverted =
            sceneView->getModelViewProjectionMatrixInverted();
    QVector3D l0 = *mvpMatrixInverted * mousePosClipSpace;

    // Compute l as the vector from l0 to the camera origin.
    QVector3D cameraPosWorldSpace = sceneView->getCamera()->getOrigin();
    QVector3D l = (l0 - cameraPosWorldSpace);

    // The plane's normal vector simply points upward, the origin in world
    // space is lcoated on the plane.
    QVector3D n = QVector3D(0, 0, 1);
    QVector3D p0 = waypointsModel->positionLonLatIncludingMidpoints(handleID)
            + QVector3D(0, 0, modifyWaypoint_worldZ);

    // Compute the mouse position in world space.
    float d = QVector3D::dotProduct(p0 - l0, n) / QVector3D::dotProduct(l, n);
    QVector3D mousePosWorldSpace = l0 + d * l;

    // Get properties for label font size and colour and bounding box.
    QVector3D pos = waypointsModel->positionLonLatIncludingMidpoints(handleID);
    double lon = pos.x();
    double lat = pos.y();

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    MTextManager* tm = glRM->getTextManager();
    positionLabel = tm->addText(
            QString("lon:%1, lat:%2").arg(lon, 0, 'f', 2).arg(lat, 0, 'f', 2),
            MTextManager::LONLATP, lon, lat,
            sceneView->pressureFromWorldZ(modifyWaypoint_worldZ),
            labelProps.fontSizeProp,
            labelProps.fontColourProp,
            MTextManager::LOWERRIGHT,
            labelProps.enableBBoxProp,
            labelProps.bboxColourProp);

    double dist = computePositionLabelDistanceWeight(sceneView->getCamera(),
                                                     mousePosWorldSpace);
    QVector3D anchorOffset = dist * sceneView->getCamera()->getXAxis();
    positionLabel->anchorOffset = -anchorOffset;

    emitActorChangedSignal();
}


void MNWPVerticalSectionActor::dragEvent(MSceneViewGLWidget *sceneView,
                                         int handleID, float clipX, float clipY)
{
    // http://stackoverflow.com/questions/2093096/implementing-ray-picking

    if (waypointsModel == nullptr) return;

    // Select an arbitrary z-value to construct a point in clip space that,
    // transformed to world space, lies on the ray passing through the camera
    // and the location on the worldZ==0 plane "picked" by the mouse.
    // (See notes 22-23Feb2012).
    QVector3D mousePosClipSpace = QVector3D(clipX + offsetPickPositionToHandleCentre.x(),
                                            clipY + offsetPickPositionToHandleCentre.y(),
                                            modifyWaypoint_worldZ);

    // The point p at which the ray intersects the worldZ==0 plane is found by
    // computing the value d in p=d*l+l0, where l0 is a point on the ray and l
    // is a vector in the direction of the ray. d can be found with
    //        (p0 - l0) * n
    //   d = ----------------
    //            l * n
    // where p0 is a point on the worldZ==0 plane and n is the normal vector
    // of the plane.
    //       http://en.wikipedia.org/wiki/Line-plane_intersection

    // To compute l0, the MVP matrix has to be inverted.
    QMatrix4x4 *mvpMatrixInverted =
            sceneView->getModelViewProjectionMatrixInverted();
    QVector3D l0 = *mvpMatrixInverted * mousePosClipSpace;

    // Compute l as the vector from l0 to the camera origin.
    QVector3D cameraPosWorldSpace = sceneView->getCamera()->getOrigin();
    QVector3D l = (l0 - cameraPosWorldSpace);

    // The plane's normal vector simply points upward, the origin in world
    // space is lcoated on the plane.
    QVector3D n = QVector3D(0, 0, 1);
    QVector3D p0 = waypointsModel->positionLonLatIncludingMidpoints(handleID)
            + QVector3D(0, 0, modifyWaypoint_worldZ);

    // Compute the mouse position in world space.
    float d = QVector3D::dotProduct(p0 - l0, n) / QVector3D::dotProduct(l, n);
    QVector3D mousePosWorldSpace = l0 + d * l;

    // DEBUG output.
//    cout << "dragging handle " << handleID << endl << flush;
//    cout << "\tmouse position clip space = (" << mousePosClipSpace.x() << "," << mousePosClipSpace.y() << "," << mousePosClipSpace.z() << ")\n" << flush;
//    cout << "\tl0 = (" << l0.x() << "," << l0.y() << "," << l0.z() << ")\n" << flush;
//    QVector3D l0Clip = *(mvpMatrix) * l0;
//    cout << "\tcheck: l0, transformed back to clip space = (" << l0Clip.x() << "," << l0Clip.y() << "," << l0Clip.z() << ")\n" << flush;
//    cout << "\tl = (" << l.x() << "," << l.y() << "," << l.z() << ")\n" << flush;
//    cout << "\tmouse position, world space, on worldZ==0 plane = (" << mousePosWorldSpace.x() << "," << mousePosWorldSpace.y()
//         << "," << mousePosWorldSpace.z()
//         << "); d = " << d << "\n" << flush;

    if (positionLabel != nullptr)
    {


        // Get properties for label font size and colour and bounding box.
        MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
        MTextManager* tm = glRM->getTextManager();
        positionLabel = tm->addText(
                QString("lon:%1, lat:%2")
                    .arg(mousePosWorldSpace.x(), 0, 'f', 2)
                    .arg(mousePosWorldSpace.y(), 0, 'f', 2),
                MTextManager::LONLATP, mousePosWorldSpace.x(),
                mousePosWorldSpace.y(),
                sceneView->pressureFromWorldZ(modifyWaypoint_worldZ),
                labelProps.fontSizeProp,
                labelProps.fontColourProp,
                MTextManager::LOWERRIGHT,
                labelProps.enableBBoxProp,
                labelProps.bboxColourProp);

        double dist = computePositionLabelDistanceWeight(sceneView->getCamera(),
                                                         mousePosWorldSpace);
        QVector3D anchorOffset = dist * sceneView->getCamera()->getXAxis();
        positionLabel->anchorOffset = -anchorOffset;
    }

    QVector2D lonLat = {mousePosWorldSpace.x(), mousePosWorldSpace.y()};

    // Set the waypoint's coordinates. This will trigger a dataChanged signal
    // of the waypoints model, which in turn will call
    // generatePathFromWaypoints() and redraw the scene.
    auto *cmd = new MWaypointsTableModel::SetPositionLonLatCommand(waypointsModel,
                                                                   handleID / 2,
                                                                   lonLat,
                                                                   handleID & 1,
                                                                   dragEventID);
    MUndoStack::run(cmd);

}


void MNWPVerticalSectionActor::setWaypointsModel(MWaypointsTableModel *model)
{
    // If the actor is currently connected to a different model, disconnect.
    if ( waypointsModel != nullptr )
    {
        disconnect(waypointsModel,
                   SIGNAL(dataChanged(QModelIndex, QModelIndex)),
                   this,
                   SLOT(generatePathFromWaypoints(QModelIndex, QModelIndex)));
        disconnect(waypointsModel,
                   SIGNAL(rowsRemoved(QModelIndex, int, int)),
                   this,
                   SLOT(actOnWaypointInsertDelete(QModelIndex, int, int)));
        disconnect(waypointsModel,
                   SIGNAL(rowsInserted(QModelIndex, int, int)),
                   this,
                   SLOT(actOnWaypointInsertDelete(QModelIndex, int, int)));
    }

    // Store the pointer to the new model and connect to its signals.
    waypointsModel = model;

    enableActorUpdates(false);

    if ( waypointsModel != nullptr )
    {
        connect(waypointsModel,
                SIGNAL(dataChanged(QModelIndex, QModelIndex)),
                SLOT(generatePathFromWaypoints(QModelIndex, QModelIndex)));
        connect(waypointsModel,
                SIGNAL(rowsRemoved(QModelIndex, int, int)),
                SLOT(actOnWaypointInsertDelete(QModelIndex, int, int)));
        connect(waypointsModel,
                SIGNAL(rowsInserted(QModelIndex, int, int)),
                SLOT(actOnWaypointInsertDelete(QModelIndex, int, int)));

        // Update GUI property.
        waypointsModelProp.setEnumItem(waypointsModel->getID());
    }
    else
    {
        // Remove labels -- otherwise labels of the previous waypoints model
        // will remain visible.
        removeAllLabels();

        // Set GUI property to "None".
        waypointsModelProp.setEnumItem("None");
    }

    enableActorUpdates(true);

    // Trigger re-computation of vsec-grid on next render cycle.
    updatePath = true;
}


MWaypointsTableModel* MNWPVerticalSectionActor::getWaypointsModel() const
{
    return waypointsModel;
}


double MNWPVerticalSectionActor::getBottomPressure_hPa() const
{
    return p_bot_hPa;
}


double MNWPVerticalSectionActor::getTopPressure_hPa() const
{
    return p_top_hPa;
}


QList<MVerticalLevelType> MNWPVerticalSectionActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>()
            << HYBRID_SIGMA_PRESSURE_3D << PRESSURE_LEVELS_3D
            << AUXILIARY_PRESSURE_3D);
}


MNWPActorVariable* MNWPVerticalSectionActor::createActorVariable(
        const MSelectableDataVariable& dataVariable)
{
    MNWP2DVerticalActorVariable* newVar = new MNWP2DVerticalActorVariable(this);

    newVar->dataSourceID = dataVariable.dataSourceID;
    newVar->levelType = dataVariable.levelType;
    newVar->variableName = dataVariable.variableName;
    newVar->setRenderMode(MNWP2DSectionActorVariable::RenderMode::Disabled);

    return newVar;
}


void MNWPVerticalSectionActor::onBoundingBoxChanged()
{
    labels.clear();
    // Switching to no bounding box only needs a redraw, but no recomputation
    // because it disables rendering of the actor.
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        emitActorChangedSignal();
        return;
    }
    // The vertical extent of the section has been changed.
    p_top_hPa = getBBoxConnection()->topPressure_hPa();
    p_bot_hPa = getBBoxConnection()->bottomPressure_hPa();
    targetGridToBeUpdated = true;

    if (suppressActorUpdates()) return;

    // Adapt iso pressure lines set to new boundaries.
    generateIsoPressureLines();
    updatePath = true;
    emitActorChangedSignal();
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MNWPVerticalSectionActor::generatePathFromWaypoints(
        QModelIndex mindex1, QModelIndex mindex2, QGLWidget *currentGLContext)
{
//TODO: implement great circles.
    // Great circles (ECMWF seems to use a perfect sphere for the IFS):
    // * http://www.movable-type.co.uk/scripts/gis-faq-5.1.html
    // * http://williams.best.vwh.net/avform.htm
    // * http://trac.osgeo.org/openlayers/wiki/GreatCircleAlgorithms
    // * Jeff W's Python implementation with the Vincenty distance.
    // * 3D Engine Design Virtual Globes, Sect 2.4?

    if (mindex1.isValid() && mindex2.isValid())
    {
        // The index variable provide row and column of the changed item in the
        // table. row = number of waypoint, column = table column (i.e. name,
        // lat, lon, fl, ...).

        // Only react to the signal if the position of a waypoint has changed.
        if ( !((mindex1.column() == MWaypointsTableModel::LAT) ||
               (mindex1.column() == MWaypointsTableModel::LON) ||
               (mindex1.column() == MWaypointsTableModel::FLIGHT_LEVEL))) return;
    }

    // Implementation for linear connection.

    // A valid track must have at least two waypoints.
    if (waypointsModel->size() < 2) return;

    // The vector "path" accomodates a list of points that resemble the
    // vertical section path. Entries are QVector4D, storing (lon, lat, i, j),
    // with i,j being the indices of the closes model grid point.
    path.clear();

    // Approximate spacing between points along the cross section path.
    float deltaS = interpolationNodeSpacingProp;

    // Vector that accomodates vertices for the vertical lines drawn at the
    // waypoints.
    QVector<QVector3D> verticesVerticalWaypointLines;
    QVector<QVector3D> verticesInteractionHandlePositions;
    QVector<QVector3D> verticesFlightTrack;

    // Copy first waypoint (p1 of the first segment).
    QVector2D p = waypointsModel->positionLonLat(0);
    path.append(p);

    // Create vertices for a vertical line and interaction handle at this
    // waypoint.
    verticesVerticalWaypointLines.append(QVector3D(p, p_bot_hPa));
    verticesVerticalWaypointLines.append(QVector3D(p, p_top_hPa));

    verticesInteractionHandlePositions.append(QVector3D(p, p_bot_hPa));
    verticesInteractionHandlePositions.append(QVector3D(p, p_top_hPa));

    for (int i = 0; i < waypointsModel->size()-1; i++)
    {
        // Add intermediate points between p1=wp[i] and p2=wp[i+1].

        QVector2D p1 = waypointsModel->positionLonLat(i  );
        QVector2D p2 = waypointsModel->positionLonLat(i+1);

        float lengthOfSegment = (p2-p1).length();
        int   numPoints = int(round(lengthOfSegment / deltaS));
        float deltaLon = (p2.x()-p1.x()) / numPoints;
        float deltaLat = (p2.y()-p1.y()) / numPoints;

        // Generate points between p1 and p2 (excluding p1 and p2).
        for (int n = 1; n < numPoints; n++)
        {
            // !! See above comments for the intial point!
            QVector2D p = QVector2D(p1.x()+n*deltaLon, p1.y()+n*deltaLat);
            path.append(p);
        }

        // Copy segment endpoint p2 (which is also p1 of the next segment).
        QVector2D p = p2;
        path.append(p);

        // Compute segment midpoint for interaction handle.
        QVector2D p_mid = p1 + (p2 - p1) / 2.;
        verticesInteractionHandlePositions.append(QVector3D(p_mid, p_bot_hPa));
        verticesInteractionHandlePositions.append(QVector3D(p_mid, p_top_hPa));

        // Create vertices for a vertical line and interaction handle at this
        // waypoint.
        verticesVerticalWaypointLines.append(QVector3D(p, p_bot_hPa));
        verticesVerticalWaypointLines.append(QVector3D(p, p_top_hPa));
        verticesInteractionHandlePositions.append(QVector3D(p, p_bot_hPa));
        verticesInteractionHandlePositions.append(QVector3D(p, p_top_hPa));
    }

    // Generate geometry for flight path.
    {
        for (int i = 0; i < waypointsModel->size(); i++)
        {
            verticesFlightTrack.append(waypointsModel->positionLonLatP(i));
        }

        QVector<uint32_t> flightTrackPlaneIndices;
        int n = verticesFlightTrack.length();

        for (int i = 0; i < n - 1; i++)
        {
            QVector3D fPos = verticesFlightTrack[i];
            QVector3D fPos2 = verticesFlightTrack[i + 1];

            QVector3D fBot2 = QVector3D(fPos2.x(), fPos2.y(), p_bot_hPa);

            // Insert first bottom vertex.
            if (i == 0)
            {
                QVector3D fBot = QVector3D(fPos.x(), fPos.y(), p_bot_hPa);
                verticesFlightTrack.append(fBot);
            }

            verticesFlightTrack.append(fBot2);

            flightTrackPlaneIndices.append(i);
            flightTrackPlaneIndices.append(n + i);
            flightTrackPlaneIndices.append(i + 1);
            flightTrackPlaneIndices.append(n + 1 + i);
        }

        // Send vertices of flight path to GPU.
        if (!vbFlightTrackWaypointPositions)
        {
            glGenBuffers(1, &flightTrackIndexBuffer);
            CHECK_GL_ERROR;
        }
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, flightTrackIndexBuffer);
        CHECK_GL_ERROR;
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     (GLsizeiptr) (flightTrackPlaneIndices.size()
                             * sizeof(uint32_t)), flightTrackPlaneIndices.data(),
                     GL_STATIC_DRAW);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

        if (vbFlightTrackWaypointPositions) delete vbFlightTrackWaypointPositions;
        vbFlightTrackWaypointPositions = new GL::MVector3DVertexBuffer(
                QString("vbfp_%1").arg(myID),
                verticesFlightTrack.size());
        vbFlightTrackWaypointPositions->upload(verticesFlightTrack,
                                               currentGLContext);

        // Required for the glDrawArrays() call in renderToCurrentContext().
        numFlightTrackWaypointPositions = n;
        numFlightTrackWaypointVertices = flightTrackPlaneIndices.length();

    }

//TODO: Register this texture with the glRM memory management?
    if (textureVerticalSectionPath)
    {
        delete textureVerticalSectionPath;
    }
    textureVerticalSectionPath = new GL::MTexture(QString("vpath_%1").arg(myID),
                                                 GL_TEXTURE_1D, GL_R32F,
                                                 2 * path.size());
    textureVerticalSectionPath->bindToTextureUnit(textureUnitVerticalSectionPath);

    // Set texture parameters: wrap mode and filtering.
    // NOTE: GL_NEAREST is required here to avoid interpolation.
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexImage1D(GL_TEXTURE_1D,             // target
                 0,                         // level of detail
                 GL_R32F,           // internal format
                 2 * path.size(),           // width, size * (lon/lat/i/j)
                 0,                         // border
                 GL_RED,                  // format
                 GL_FLOAT,                  // data type of the pixel data
                 path.constData()); CHECK_GL_ERROR;

    // Update target grid texture for each variable. (no data is uploaded, only
    // the size is set).
    targetGridToBeUpdated = true;

    for (int vi = 0; vi < variables.size(); vi++)
    {
        MNWP2DVerticalActorVariable* var =
                static_cast<MNWP2DVerticalActorVariable*> (variables.at(vi));

        var->textureTargetGrid->bindToTextureUnit(var->textureUnitTargetGrid);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glTexImage2D(GL_TEXTURE_2D,             // target
                     0,                         // level of detail
                     GL_RG32F,                  // internal format
                     path.size(),               // width
                     var->grid->nlevs,          // height
                     0,                         // border
                     GL_RED,                    // format
                     GL_FLOAT,                  // data type of the pixel data
                     NULL); CHECK_GL_ERROR;
    }

    // Send vertices of vertical waypoint lines to video memory.
    if (vbVerticalWaypointLines) delete vbVerticalWaypointLines;
    vbVerticalWaypointLines = new GL::MVector3DVertexBuffer(
                QString("vbwp_%1").arg(myID),
                verticesVerticalWaypointLines.size());
    vbVerticalWaypointLines->upload(verticesVerticalWaypointLines,
                                    currentGLContext);
    // Required for the glDrawArrays() call in renderToCurrentContext().
    numVerticesVerticalWaypointLines = verticesVerticalWaypointLines.size();

    // Send vertices of drag handle positions to video memory.
    if (vbInteractionHandlePositions) delete vbInteractionHandlePositions;
    vbInteractionHandlePositions = new GL::MVector3DVertexBuffer(
                QString("vbdhpos_%1").arg(myID),
                verticesInteractionHandlePositions.size());
    vbInteractionHandlePositions->upload(verticesInteractionHandlePositions,
                                         currentGLContext);
    numInteractionHandlePositions = verticesInteractionHandlePositions.size();

    //NOTE: generateLabels() switches to the MGLResourcesManager OpenGL context,
    // hence we need to switch back to the currentGLContext afterwards.
    generateLabels();
    if (currentGLContext) currentGLContext->makeCurrent();

    pointInterfaceChanged();
    emitActorChangedSignal();
}


void MNWPVerticalSectionActor::actOnWaypointInsertDelete(
        const QModelIndex &parent, int start, int end)
{
    // Parameters are required so this function can act as a slot for
    // rowsInserted / rowsRemoved signals.
    Q_UNUSED(parent);
    Q_UNUSED(start);
    Q_UNUSED(end);

    generatePathFromWaypoints();
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MNWPVerticalSectionActor::initializeActorResources()
{
    // Parent initialisation.
    MNWPMultiVarActor::initializeActorResources();

    if (textureUnitPressureLevels >=0)
        releaseTextureUnit(textureUnitPressureLevels);
    textureUnitPressureLevels = assignTextureUnit();
    generateIsoPressureLines();

    if (textureUnitVerticalSectionPath >=0)
        releaseTextureUnit(textureUnitVerticalSectionPath);
    textureUnitVerticalSectionPath = assignTextureUnit();

    updatePath = true;

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    bool loadShaders = false;

    loadShaders |= glRM->generateEffectProgram("vsec_sectiongrid",
                                                sectionGridShader);
    loadShaders |= glRM->generateEffectProgram("vsec_marchingsquares",
                                                marchingSquaresShader);
    loadShaders |= glRM->generateEffectProgram("vsec_pressurelines",
                                                pressureLinesShader);
    loadShaders |= glRM->generateEffectProgram("vsec_simplegeometry",
                                                simpleGeometryShader);
    loadShaders |= glRM->generateEffectProgram("vsec_positionsphere",
                                                positionSpheresShader);

    if (loadShaders) reloadShaderEffects();
}


void MNWPVerticalSectionActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    // If there is no connected waypoints model, no connected bounding box or
    // no actor variable, nothing can be rendered.
    if (waypointsModel == nullptr || getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }

    if (updatePath)
    {
        // This method might already be called between initial data request and
        // all data fields being available. Return if not all variables
        // contain valid data yet.
        for (MNWPActorVariable *var : variables)
            if ( !var->hasData() ) return;

        // Prevent generatePathFromWaypoints() from emitting a signal.
        enableEmissionOfActorChangedSignal(false);
        updateVerticalLevelRange();
        generatePathFromWaypoints(QModelIndex(), QModelIndex(), sceneView);
        updatePath = false;
        enableEmissionOfActorChangedSignal(true);
    }

    bool shadowPass = sceneView->renderStep() == MSceneViewGLWidget::SHADOW_MAPPING;

    // If major visualisation parameters of the view have changed (e.g.
    // vertical scaling), a recompuation of the target grid is necessary, as it
    // stores worldZ coordinates.
    targetGridToBeUpdated = targetGridToBeUpdated
            || sceneView->visualisationParametersHaveChanged();

    // 1D texture that stores the horizontal coordinates of the section points.
    textureVerticalSectionPath->bindToTextureUnit(
                textureUnitVerticalSectionPath);

    // Rendering for all data variables:
    for (int vi = 0; vi < variables.size(); vi++)
    {
        // Shortcuts to the variable's properties.
        MNWP2DVerticalActorVariable* var =
                static_cast<MNWP2DVerticalActorVariable*> (variables.at(vi));

        if ( !var->hasData() ) continue;

        // A) Compute the vertical section grid, store it to the "targetGrid"
        //    texture, render filled contours if requested.
        // ==================================================================

        bool renderFilledContours = (
                    (var->renderSettings.renderMode
                     == MNWP2DSectionActorVariable::RenderMode::FilledContours)
                    ||
                    (var->renderSettings.renderMode
                     == MNWP2DSectionActorVariable::RenderMode::FilledAndLineContours)
                    ) && ( var->transferFunction != nullptr );

        if (renderFilledContours || targetGridToBeUpdated)
        {
            if (renderFilledContours)
            {
                sectionGridShader->bindProgram("Standard");

                // Change the depth function to less and equal. This allows
                // OpenGL to overwrite fragments with the same depths and thus
                // allows the vsec actor to draw filled contours of more than
                // one variable.
                glDepthFunc(GL_LEQUAL);
            }
            else
            {
                sectionGridShader->bindProgram("OnlyUpdateTargetGrid");
            }

            // Reset optional textures (to avoid draw errors).
            // ===============================================
            var->textureDummy1D->bindToTextureUnit(var->textureUnitDummy1D);
            sectionGridShader->setUniformValue(
                        "hybridCoefficients", var->textureUnitDummy1D); CHECK_GL_ERROR;

            var->textureDummy2D->bindToTextureUnit(var->textureUnitDummy2D);
            sectionGridShader->setUniformValue(
                        "surfacePressure", var->textureUnitDummy2D); CHECK_GL_ERROR;

            // Model-view-projection matrix from the current scene view.
            sectionGridShader->setUniformValue(
                        "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));

            sectionGridShader->setUniformValue(
                        "levelType", int(var->grid->getLevelType()));

            // Texture bindings for Lat/Lon axes (1D textures).
            var->textureLonLatLevAxes->bindToTextureUnit(var->textureUnitLonLatLevAxes);
            sectionGridShader->setUniformValue(
                        "lonLatLevAxes", var->textureUnitLonLatLevAxes);
            sectionGridShader->setUniformValue(
                        "latOffset", var->grid->nlons);

            // Texture bindings for data field (3D texture).
            var->textureDataField->bindToTextureUnit(var->textureUnitDataField);
            sectionGridShader->setUniformValue(
                        "dataField", var->textureUnitDataField);
            sectionGridShader->setUniformValue(
                        "auxPressureField_hPa", var->textureUnitDataField);

            // Texture bindings for transfer function for data field (1D texture from
            // transfer function class). Variables that are only rendered as
            // contour lines might not provide a valid transfer function.
            if (var->transferFunction != 0)
            {
                var->transferFunction->getTexture()->bindToTextureUnit(
                            var->textureUnitTransferFunction);
                sectionGridShader->setUniformValue(
                            "transferFunction", var->textureUnitTransferFunction);
                sectionGridShader->setUniformValue(
                            "scalarMinimum", var->transferFunction->getMinimumValue());
                sectionGridShader->setUniformValue(
                            "scalarMaximum", var->transferFunction->getMaximumValue());
            }

            if (var->grid->getLevelType() == HYBRID_SIGMA_PRESSURE_3D)
            {
                // Texture bindings for surface pressure (2D texture) and model level
                // coefficients (1D texture).
                var->textureSurfacePressure->bindToTextureUnit(var->textureUnitSurfacePressure);
                sectionGridShader->setUniformValue(
                            "surfacePressure", var->textureUnitSurfacePressure);
                var->textureHybridCoefficients->bindToTextureUnit(var->textureUnitHybridCoefficients);
                sectionGridShader->setUniformValue(
                            "hybridCoefficients", var->textureUnitHybridCoefficients);
            }

            if (var->grid->getLevelType() == AUXILIARY_PRESSURE_3D)
            {
                // Texture binding for pressure field (3D texture).
                var->textureAuxiliaryPressure->bindToTextureUnit(
                            var->textureUnitAuxiliaryPressure);
                sectionGridShader->setUniformValue(
                            "auxPressureField_hPa", var->textureUnitAuxiliaryPressure);
            }

            // Data volume info required to locate grid index for given lon/lat.
            sectionGridShader->setUniformValue(
                        "deltaLat", var->grid->getDeltaLat()); CHECK_GL_ERROR;
            sectionGridShader->setUniformValue(
                        "deltaLon", var->grid->getDeltaLon()); CHECK_GL_ERROR;
            QVector3D dataNWCrnr = var->grid->getNorthWestTopDataVolumeCorner_lonlatp();
            sectionGridShader->setUniformValue(
                        "dataNWCrnr", dataNWCrnr); CHECK_GL_ERROR;
            sectionGridShader->setUniformValue(
                        "gridIsCyclicInLongitude",
                        var->grid->gridIsCyclicInLongitude()); CHECK_GL_ERROR;

            // Scene view specific parameters to compute worldZ from pressure in
            // the vertex shader.
            sectionGridShader->setUniformValue(
                        "pToWorldZParams", sceneView->pressureToWorldZParameters());

            // (Texture object is already bound to this unit, see above).
            sectionGridShader->setUniformValue(
                        "path", textureUnitVerticalSectionPath);

            sectionGridShader->setUniformValue(
                        "targetGrid", var->imageUnitTargetGrid);
            glBindImageTexture(var->imageUnitTargetGrid,     // image unit
                               var->textureTargetGrid->getTextureObject(), // texture object
                               0,                             // level
                               GL_FALSE,                      // layered
                               0,                             // layer
                               GL_READ_WRITE,                 // shader access
                               GL_RG32F); CHECK_GL_ERROR;     // format
            sectionGridShader->setUniformValue(
                        "fetchFromTarget", !targetGridToBeUpdated);

            // Set the sections vertical limits (the fragment shader discards
            // elements outside this range).
            sectionGridShader->setUniformValue(
                        "verticalBounds",
                        QVector2D(sceneView->worldZfromPressure(p_bot_hPa),
                                  sceneView->worldZfromPressure(p_top_hPa))
                        ); CHECK_GL_ERROR;

            sectionGridShader->setUniformValue(
                    "opacity",
                    opacityProp); CHECK_GL_ERROR;


            if (renderFilledContours)
            {
                if (textureUnitShadowMap < 0)
                {
                    textureUnitShadowMap = assignTextureUnit();
                }
                sceneView->bindShadowMap(sectionGridShader, textureUnitShadowMap);
            }

            // Use instanced rendering to avoid geometry upload (see notes 09Feb2012).
            // Offset depth buffer slightly to ensure correct rendering of isopressure
            // lines.
            glPolygonOffset(.8f, 1.0f);
            glEnable(GL_POLYGON_OFFSET_FILL);
            glPolygonMode(GL_FRONT_AND_BACK,
                          renderAsWireFrameProp ? GL_LINE : GL_FILL); CHECK_GL_ERROR;
            glDrawArraysInstanced(GL_TRIANGLE_STRIP,
                                  2 * var->gridVerticalLevelStart,
                                  2 * var->gridVerticalLevelCount,
                                  path.size() - 1); CHECK_GL_ERROR;
            glDisable(GL_POLYGON_OFFSET_FILL);
            // Change the depth function back to its default value.
            glDepthFunc(GL_LESS);
        } // sectionGridShader (interpolation, target grid, filled contours)

        // B) Contouring with the GPU Marching Squares implementation, if
        // enabled (the marching squares algorithm uses the "targetGrid" that
        // was written by the previous shader run as input).
        // ==================================================================

        if (((var->renderSettings.renderMode
                == MNWP2DSectionActorVariable::RenderMode::LineContours) ||
                (var->renderSettings.renderMode
                        == MNWP2DSectionActorVariable::RenderMode::FilledAndLineContours))
                &&
                        !shadowPass)
        {
            marchingSquaresShader->bind();

            marchingSquaresShader->setUniformValue(
                        "mvpMatrix", *(sceneView->getModelViewProjectionMatrix())); CHECK_GL_ERROR;
            marchingSquaresShader->setUniformValue(
                        "pToWorldZParams", sceneView->pressureToWorldZParameters()); CHECK_GL_ERROR;

            // (Texture object is already bound to this unit, see above).
            marchingSquaresShader->setUniformValue(
                        "path", textureUnitVerticalSectionPath); CHECK_GL_ERROR;

            // The 2D data grid that the contouring algorithm processes.
            glBindImageTexture(var->imageUnitTargetGrid,     // image unit
                               var->textureTargetGrid->getTextureObject(), // texture object
                               0,                             // level
                               GL_FALSE,                      // layered
                               0,                             // layer
                               GL_READ_WRITE,                 // shader access
                               GL_RG32F); CHECK_GL_ERROR;     // format
            marchingSquaresShader->setUniformValue(
                        "sectionGrid", var->imageUnitTargetGrid); CHECK_GL_ERROR;

            // Set the sections vertical limits (the fragment shader discards
            // elements outside this range).
            marchingSquaresShader->setUniformValue(
                        "verticalBounds",
                        QVector2D(sceneView->worldZfromPressure(p_bot_hPa),
                                  sceneView->worldZfromPressure(p_top_hPa))
                        ); CHECK_GL_ERROR;

            // Draw individual line segments as output by the geometry shader (no
            // connected polygon is created).
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;

            // Loop over all contour sets.
            for (MNWP2DSectionActorVariable::ContourSettings
                     *contourSet : var->contourSetList)
            {
                if (contourSet->enabledProp)
                {
                    sceneView->setLineWidth(contourSet->thicknessProp / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;
                    marchingSquaresShader->setUniformValue(
                                "useTransferFunction",
                                GLboolean(var->renderSettings.contoursUseTFProp
                                          || contourSet->useTFProp)); CHECK_GL_ERROR;
                    if (!(var->renderSettings.contoursUseTFProp || contourSet->useTFProp))
                    {
                        marchingSquaresShader->setUniformValue(
                                    "colour", contourSet->colourProp); CHECK_GL_ERROR;
                    }
                    else
                    {
                        // Texture bindings for transfer function for data field
                        // (1D texture from transfer function class).
                        if (var->transferFunction != nullptr)
                        {
                            var->transferFunction->getTexture()->bindToTextureUnit(
                                        var->textureUnitTransferFunction);
                            marchingSquaresShader->setUniformValue(
                                        "transferFunction",
                                        var->textureUnitTransferFunction);
                            CHECK_GL_ERROR;
                            marchingSquaresShader->setUniformValue(
                                        "scalarMinimum",
                                        GLfloat(var->transferFunction
                                                ->getMinimumValue()));
                            CHECK_GL_ERROR;
                            marchingSquaresShader->setUniformValue(
                                        "scalarMaximum",
                                        GLfloat(var->transferFunction
                                                ->getMaximumValue()));
                            CHECK_GL_ERROR;
                        }
                        // Don't draw contour set if transfer function is not
                        // present.
                        else
                        {
                            continue;
                        }
                    }
                    // Loop over all iso values for which contour lines should
                    // be rendered -- one render pass per isovalue.
                    for (int i = contourSet->startIndex;
                         i < contourSet->stopIndex; i++)
                    {
                        marchingSquaresShader->setUniformValue(
                                    "isoValue",
                                    GLfloat(contourSet->levels.at(i)));
                        CHECK_GL_ERROR;
                        glDrawArraysInstanced(GL_POINTS,
                                              var->gridVerticalLevelStart,
                                              var->gridVerticalLevelCount - 1,
                                              path.size() - 1); CHECK_GL_ERROR;
                    }
                }
            }
        } // marching squares shader
    } // for (variables)

    if (!shadowPass)
    {
        // C) Render isopressure lines (vertical coordinate system).
        // =========================================================

        pressureLinesShader->bind();

        pressureLinesShader->setUniformValue(
                "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));

        pressureLinesShader->setUniformValue(
                "pToWorldZParams", sceneView->pressureToWorldZParameters());

        // (Texture object is already bound to this unit, see above).
        pressureLinesShader->setUniformValue(
                "path", textureUnitVerticalSectionPath);

        // 1D texture storing the pressure values at which lines should be drawn.
        texturePressureLevels->bindToTextureUnit(textureUnitPressureLevels);
        pressureLinesShader->setUniformValue(
                "pressureLevels", textureUnitPressureLevels);
        CHECK_GL_ERROR;

        sceneView->setLineWidth(1.0f / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;
        glDrawArraysInstanced(GL_LINE_STRIP,
                              0,
                              path.size(),
                              pressureLineLevels.size()); CHECK_GL_ERROR;

        // D) Render vertical lines at waypoints.
        // ======================================

        if (vbVerticalWaypointLines != nullptr)
        {

            simpleGeometryShader->bindProgram("Pressure"); CHECK_GL_ERROR;

            simpleGeometryShader->setUniformValue(
                    "mvpMatrix",
                    *(sceneView->getModelViewProjectionMatrix())); CHECK_GL_ERROR;
            simpleGeometryShader->setUniformValue(
                    "pToWorldZParams",
                    sceneView->pressureToWorldZParameters()); CHECK_GL_ERROR;
            simpleGeometryShader->setUniformValue(
                    "colour",
                    QColor(0, 0, 0)); CHECK_GL_ERROR;

            vbVerticalWaypointLines->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
            sceneView->setLineWidth(2.0f / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

            glDrawArrays(GL_LINES, 0, numVerticesVerticalWaypointLines); CHECK_GL_ERROR;
            glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
        }

        // E) "Hover" effect for a waypoint in interaction mode: Highlight
        //    a specific waypoint (that at index "modifyWaypoint" in the vector
        //    "waypoints".
        // ====================================================================

        if (sceneView->interactionModeEnabled() &&
                (vbInteractionHandlePositions != nullptr))
        {
            positionSpheresShader->bindProgram("Normal");

            positionSpheresShader->setUniformValue(
                    "mvpMatrix",
                    *(sceneView->getModelViewProjectionMatrix()));
            positionSpheresShader->setUniformValue(
                    "pToWorldZParams",
                    sceneView->pressureToWorldZParameters());
            positionSpheresShader->setUniformValue(
                    "cameraPosition",
                    sceneView->getCamera()->getOrigin());
            positionSpheresShader->setUniformValue(
                    "cameraUpDir",
                    sceneView->getCamera()->getYAxis());
            positionSpheresShader->setUniformValue(
                    "radius", GLfloat(sceneView->getHandleSize()));
            positionSpheresShader->setUniformValue(
                    "scaleRadius",
                    GLboolean(true));

            positionSpheresShader->setUniformValue(
                    "useTransferFunction", GLboolean(false));

            // Bind shadow map to texture unit 1.
            if (textureUnitShadowMap < 0)
            {
                textureUnitShadowMap = assignTextureUnit();
            }
            sceneView->bindShadowMap(positionSpheresShader, textureUnitShadowMap);

            vbInteractionHandlePositions->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

            glPolygonMode(GL_FRONT_AND_BACK,
                          renderAsWireFrameProp ? GL_LINE : GL_FILL); CHECK_GL_ERROR;
            sceneView->setLineWidth(1.0f / sceneView->getViewportUpscaleFactor()); CHECK_GL_ERROR;

            if (modifyWaypoint >= 0)
            {
                positionSpheresShader->setUniformValue(
                        "constColour", QColor(Qt::red));
                glDrawArrays(GL_POINTS, 2*modifyWaypoint, 2); CHECK_GL_ERROR;
            }

            positionSpheresShader->setUniformValue(
                    "constColour", QColor(Qt::white));
            glDrawArrays(GL_POINTS, 0, numInteractionHandlePositions); CHECK_GL_ERROR;


            // Unbind VBO.
            glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
        }

        // F) Render flight track
        // ======================================
        if (vbFlightTrackWaypointPositions && enableFlightTrackProp.value())
        {
            simpleGeometryShader->bindProgram("Pressure");
            simpleGeometryShader->setUniformValue(
                    "mvpMatrix",
                    *(sceneView->getModelViewProjectionMatrix()));
            CHECK_GL_ERROR;
            simpleGeometryShader->setUniformValue(
                    "pToWorldZParams",
                    sceneView->pressureToWorldZParameters());
            CHECK_GL_ERROR;
            simpleGeometryShader->setUniformValue(
                    "colour",
                    flightTrackLineColourProp.value());
            CHECK_GL_ERROR;

            vbFlightTrackWaypointPositions
                    ->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);

            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
            CHECK_GL_ERROR;
            sceneView->setLineWidth(flightTrackLineThicknessProp / sceneView
                    ->getViewportUpscaleFactor());
            CHECK_GL_ERROR;

            // Draw flight track
            glDrawArrays(GL_LINE_STRIP, 0, numFlightTrackWaypointPositions);
            CHECK_GL_ERROR;
            glBindBuffer(GL_ARRAY_BUFFER, 0);
            CHECK_GL_ERROR;

            simpleGeometryShader->setUniformValue(
                    "colour",
                    flightTrackAreaColorProp);
            CHECK_GL_ERROR;

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, flightTrackIndexBuffer);
            CHECK_GL_ERROR;

            // Draw path line filler plane below the flight track.
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
            CHECK_GL_ERROR;

            glDrawElements(GL_TRIANGLE_STRIP, numFlightTrackWaypointVertices,
                           GL_UNSIGNED_INT, nullptr);
            CHECK_GL_ERROR;

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
            CHECK_GL_ERROR;
        }
    }

    // Don't update the grid until the next update event occurs (see
    // actOnPropertyChange() and dataFieldChangedEvent().
    targetGridToBeUpdated = false;
}


void MNWPVerticalSectionActor::dataFieldChangedEvent()
{
    targetGridToBeUpdated = true;
    emitActorChangedSignal();
}


void MNWPVerticalSectionActor::generateIsoPressureLines()
{
    pressureLineLevels.clear();
    for (int i = 0; i < selectedPressureLineLevels.size(); i++)
    {
        if ((selectedPressureLineLevels[i] <= p_bot_hPa)
                && (selectedPressureLineLevels[i] >= p_top_hPa))
            pressureLineLevels.append(selectedPressureLineLevels[i]);
    }

    if (texturePressureLevels) delete texturePressureLevels;

//TODO: Register this texture with the glRM memory management?
    texturePressureLevels = new GL::MTexture(QString("prlevs_%1").arg(myID),
                                            GL_TEXTURE_1D, GL_R32F,
                                            pressureLineLevels.size());
    texturePressureLevels->bindToLastTextureUnit();

    // Set texture parameters: wrap mode and filtering.
    // NOTE: GL_NEAREST is required here to avoid interpolation.
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glTexImage1D(GL_TEXTURE_1D,             // target
                 0,                         // level of detail
                 GL_R32F,           // internal format
                 pressureLineLevels.size(), // width
                 0,                         // border
                 GL_RED,                  // format
                 GL_FLOAT,                  // data type of the pixel data
                 pressureLineLevels.constData()); CHECK_GL_ERROR;
}


void MNWPVerticalSectionActor::updateVerticalLevelRange()
{
    LOG4CPLUS_INFO(mlog, "Updating vertical level range...");

    // For each variables in the vertical section, determine the upper/lower
    // model levels that enclose the range pbot..ptop.
    for (int vi = 0; vi < variables.size(); vi++)
    {
        // Shortcuts to variable info structs.
        MNWP2DVerticalActorVariable* var =
                static_cast<MNWP2DVerticalActorVariable*> (variables.at(vi));

        var->updateVerticalLevelRange(p_bot_hPa, p_top_hPa);
    }
}


void MNWPVerticalSectionActor::generateLabels()
{
    // Remove all text labels of the old geometry (MActor method).
    removeAllLabels();

    if (path.size() == 0) return;

    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    MTextManager* tm = glRM->getTextManager();

    // Draw labels at these horizontal positions.
    QList<QVector2D> labelPoints;
    // First and last point of the path.
    labelPoints.append(path[0]);
    labelPoints.append(path[path.size()-1]);

    int drawLabel = 0;
    for (int i = 0; i < pressureLineLevels.size(); i++)
    {
        // Label only every (labelDistance + 1)-th tick mark.
        if (drawLabel++ < 0) continue;
        if (drawLabel == 1) drawLabel = -labelDistanceProp;

        for (int j = 0; j < labelPoints.size(); j++)
        {
            QVector3D position(labelPoints[j].x(), labelPoints[j].y(),
                               pressureLineLevels[i]);
            labels.append(tm->addText(
                                  QString("%1").arg(pressureLineLevels[i]),
                                  MTextManager::LONLATP,
                                  position.x(), position.y(), position.z(),
                                  labelProps.fontSizeProp,
                                  labelProps.fontColourProp,
                                  MTextManager::MIDDLELEFT,
                                  labelProps.enableBBoxProp,
                                  labelProps.bboxColourProp)
                          );
        }
    }
}


void MNWPVerticalSectionActor::onDeleteActorVariable(MNWPActorVariable *var)
{
    Q_UNUSED(var);

    // Remove labels if no variable is left. (Since variable is deleted
    // afterwards, current size() must be 1).
    if (variables.size() == 1)
    {
        removeAllLabels();
    }
}


void MNWPVerticalSectionActor::onAddActorVariable(MNWPActorVariable *var)
{
    Q_UNUSED(var);
    targetGridToBeUpdated = true;
    updatePath = true;
}


void MNWPVerticalSectionActor::onChangeActorVariable(MNWPActorVariable *var)
{
    Q_UNUSED(var);
    targetGridToBeUpdated = true;
    updatePath = true;
}


QString MNWPVerticalSectionActor::generateStringForPointGeneration(
    MPointGeneratorInterfaceSettings* uiSrc) const
{
    auto ui = dynamic_cast<MVSecPointGeneratorSettings *>(uiSrc);

    int horDensity = ui->horDensityProp;
    float pSpacing = ui->pSpacingProp;

    // Build the request string: horDensity/pTop/pSpacing/pBottom/
    //                              numWaypoints/x1/y1/x2/y2...
    QString reqString;

    reqString.append(QString::number(horDensity));
    // Also put pTop and pBottom in data request so changes to the bounding
    // box generates new points properly.
    reqString.append("/" + QString::number(p_top_hPa));
    reqString.append("/" + QString::number(pSpacing));
    reqString.append("/" + QString::number(p_bot_hPa));

    int numWayPoints = getWaypointsModel()->size();
    reqString.append("/" + QString::number(numWayPoints));

    for (int i = 0; i < getWaypointsModel()->size(); i++)
    {
        QVector2D pt = getWaypointsModel()->positionLonLat(i);
        reqString.append(QString("/%1/%2").arg(pt.x()).arg(pt.y()));
    }

    return reqString;
}


MPoints *MNWPVerticalSectionActor::generatePointsFromString
    (const QString& requestString) const
{
    auto *seedPts = new MPoints();

    QStringList reqParameters = requestString.split('/');
    int horDensity = reqParameters[0].toInt();
    float pTop = reqParameters[1].toFloat();
    float pSpacing = reqParameters[2].toFloat();
    float pBottom = reqParameters[3].toFloat();
    int numWaypoints = reqParameters[4].toInt();

    // Iterate over the waypoints in the request, and sample in between the
    // waypoints based on the given density and the vertical spacing.
    for (int i = 0; i < numWaypoints - 1; i++)
    {
        QVector2D p1(reqParameters[2 * i + 5].toFloat(),
                     reqParameters[2 * i + 6].toFloat());
        QVector2D p2(reqParameters[2 * i + 7].toFloat(),
                     reqParameters[2 * i + 8].toFloat());

        // Generate seed points between p1 and p2.
        for (int h = 1; h <= horDensity; h++)
        {
            float horWeight = float(h) / float(horDensity + 1);
            QVector2D ph = horWeight * p1 + (1.f - horWeight) * p2;

            double p = pTop;
            while (p <= pBottom)
            {
                seedPts->append(QVector3D(ph.x(), ph.y(), p));
                p += pSpacing;
            }
        }
    }

    // Points generated by vertical section do not align with a structured
    // grid, leaving that field in MPoints empty.
    return seedPts;
}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/


} // namespace Met3D
