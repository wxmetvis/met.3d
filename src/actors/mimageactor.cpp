/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mimageactor.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "util/mfileutils.h"
#include "util/mfiletypes.h"
#include "gxfw/msceneviewglwidget.h"

namespace Met3D
{

MImageActor::MImageActor()
        : MActor(nullptr, false),
          isDragging(false),
          currentDragHandleID(-1)
{
    setActorType(staticActorType());
    setName(getActorType());
    enablePicking(true);

    image = addComponentOfType<MSpriteComponent>(QString("image"), MSpriteComponent::PIXEL_SPACE, true);

    // Hide label properties.
    labelProps.enabledProp.setHidden(true);
}


MImageActor::~MImageActor()
= default;


void MImageActor::reloadShaderEffects()
{
}


int MImageActor::checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                             float clipX, float clipY)
{
    if (image == nullptr)
    {
        return MActor::checkIntersectionWithHandle(sceneView, clipX, clipY);
    }

    if (isDragging)
    {
        return currentDragHandleID;
    }

    int sceneW = sceneView->getSceneResolutionWidth();
    int sceneH = sceneView->getSceneResolutionHeight();
    float clipW =
            static_cast<float>(image->width()) / static_cast<float>(sceneW);
    float clipH =
            static_cast<float>(image->height()) / static_cast<float>(sceneH);

    clipW *= image->getScaleX();
    clipH *= image->getScaleX();

    QVector2D clipPos = image->pixelToClipSpace(sceneView,
                                                {image->getPosition().x(), image->getPosition().y()});

    QRectF clipRect = QRectF(clipPos.x() - clipW / 2.0f,
                             clipPos.y() + clipH / 2.0f,
                             clipW, -clipH);

    if (clipRect.contains(clipX, clipY))
    {
        return 1;
    }

    return MActor::checkIntersectionWithHandle(sceneView, clipX, clipY);
}


void
MImageActor::dragEvent(MSceneViewGLWidget *sceneView, int handleID, float clipX,
                       float clipY)
{
    // Start dragging
    if (!isDragging && handleID > 0)
    {
        isDragging = true;
        currentDragHandleID = handleID;

        QVector2D clipPos = image->pixelToClipSpace(sceneView, {image->getPosition().x(), image->getPosition().y()});

        mouseDragStartX = clipX;
        mouseDragStartY = clipY;
        posDragStartX = clipPos.x();
        posDragStartY = clipPos.y();
    }
    else if (isDragging && handleID == 1)
    {
        float deltaX = clipX - mouseDragStartX;
        float deltaY = clipY - mouseDragStartY;

        QVector2D newClipPos = {posDragStartX + deltaX, posDragStartY + deltaY};

        image->setPosition(image->clipToPixelSpace(sceneView, newClipPos), true);

        emitActorChangedSignal();
    }
}


void MImageActor::releaseEvent(MSceneViewGLWidget *sceneView, int handleID)
{
    isDragging = false;
    currentDragHandleID = -1;
}


void MImageActor::initializeActorResources()
{
}

} // Met3D