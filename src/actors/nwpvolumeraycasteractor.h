/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2015      Michael Kern [+]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2021-2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef LONLATMULTIPURPOSEACTOR_H
#define LONLATMULTIPURPOSEACTOR_H

// standard library imports
#include <unordered_map>
#include <bitset>
#include <cstdint>
#include <memory>
#include <vector>

// related third party imports
#include <GL/glew.h>
#include <QVector>
#include <QVector3D>
#include <QMatrix4x4>
#include <QHash>

// local application imports
#include "gxfw/nwpmultivaractor.h"
#include "gxfw/nwpactorvariable.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/shaderstoragebufferobject.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mnwpactorvarproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mcolorproperty.h"
#include "gxfw/properties/mbuttonproperty.h"

class MSceneViewGLWidget;

namespace Met3D
{

/**
  @brief 3D volume isosurface raycaster. This actor renders (multiple)
  isosurfaces of hybrid sigma-pressure and pressure-level grids via GPU-based
  raycasting. Normal curves are implemented in this actor as well. Analysis
  plug-ins (@ref MAnalysisControl) are supported; for example, for the region
  contribution module (@ref MRegionContributionAnalysisControl).
 */
class MNWPVolumeRaycasterActor : public MNWPMultiVarActor,
        public MBoundingBoxInterface
{
    Q_OBJECT

public:
    MNWPVolumeRaycasterActor();

    ~MNWPVolumeRaycasterActor() override;

    static QString staticActorType() { return "Volume raycaster"; }

    static QString staticIconFileName() { return "raycaster.png"; }

    void reloadShaderEffects() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "NWPVolumeRaycasterActor"; }

    void saveConfigurationHeader(QSettings *settings) override;

    void loadConfigurationHeader(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    /**
      Isosurface "picking". Identifies the isosurface "object" that intersects
      a ray from camera to mouse position (where the user has clicked). If
      an @ref MAnalysisControl is connected to this class, triggers the
      analysis of the selected object.
     */
    bool triggerAnalysisOfObjectAtPos(
            MSceneViewGLWidget *sceneView, float clipX, float clipY,
            float clipRadius) override;

    QList<MVerticalLevelType> supportedLevelTypes() override;

    MNWPActorVariable *createActorVariable(
            const MSelectableDataVariable& dataVariable) override;

    /**
      Returns the @ref MNWPActorVariable currently raycasted.
     */
    const MNWPActorVariable* getCurrentRenderVariable() const
    { return varProp; }

    /**
      Returns the @ref MNWPActorVariable that is used to shade the isosurfaces
      extracted from @ref getCurrentRenderVariable().
     */
    const MNWPActorVariable* getCurrentShadingVariable() const
    { return shadingVarProp; }

    void onBoundingBoxChanged() override;

public slots:
    /**
      Sets flag for shadow image to be updated in the next frame.

      Connected to @ref MTransferFunction1D::actorChanged() to update shadow if
      transfer function is changed.
     */
    void updateShadow();

    /**
     * Slot to set required flags and emit actor changes signal
     * when volume light should be updated.
     */
    void updateVolumeLight();

protected:
    void initializeActorResources() override;

    /**
      Specifies GLSL subroutine names used in the shaders.
     */
    void initializeRenderInformation();

    void renderToCurrentContext(MSceneViewGLWidget* sceneView) override;

    void dataFieldChangedEvent() override;

    /**
      Computes the intersection points of a ray with a box. Returns tnear and
      tfar (packed in a vec2), which are the entry and exit parameters of the
      ray into the box when a position pos on the ray is described by
            pos = ray.origin + t * ray.direction

      boxCrnr1 and boxCrnr2 are two opposite corners of the box.

      Literature: Williams et al. (2005), "An efficient and robust ray-box
      intersection algorithm." (notes 29/03/2012).
      */
    bool rayBoxIntersection(QVector3D rayOrigin, QVector3D rayDirection,
                            QVector3D boxCrnr1, QVector3D boxCrnr2,
                            QVector2D *tNearFar);

    int computeCrossingLevel(float scalar);

    void bisectionCorrection(MSceneViewGLWidget *sceneView,
                             QVector3D *rayPosition, float *lambda,
                             QVector3D prevRayPosition, float prevLambda,
                             int *crossingLevelFront, int *crossingLevelBack);

    void onDeleteActorVariable(MNWPActorVariable* var) override;

    void onAddActorVariable(MNWPActorVariable* var) override;

    void onChangeActorVariable(MNWPActorVariable *var) override;

    void onChangeActorVariableTransferFunction(MNWPActorVariable *variable) override;

private:
    // Methods that create/compute graphics resources (volume bounding box,
    // shadow image, normal curves).
    // ====================================================================

    /**
      Generates geometry for a volume bounding box.
     */
    void generateVolumeBoxGeometry();

    /**
      Updates the geometry of the cross that is drawn when the user selects
      an isosurface in interaction mode.

      @see triggerAnalysisOfObjectAtPos()
     */
    void updatePositionCrossGeometry(QVector3D worldSpacePosition);

    /**
      Create a shadow image (renders the scene from the top into a b/w
      texture).
     */
    void createIsoPositionBuffer(MSceneViewGLWidget* sceneView);

    /**
      Create a shadow image (renders the DVR result from the top into a b/w
      texture).
     */
    void createShadowImage(MSceneViewGLWidget* sceneView);

    /**
     * Render shadows of the DVR result into a 2D plane on the ground.
     * @param sceneView The rendering scene view.
     */
    void renderShadows(MSceneViewGLWidget* sceneView);

    /**
      Compute normal curve seed points (=initial points).
     */
    void computeNormalCurveInitialPoints(MSceneViewGLWidget* sceneView);

    /**
      Compute normal curve line segments.
     */
    void computeNormalCurves(MSceneViewGLWidget* sceneView);

    // Methods that set shader variables (e.g. uniforms) prior to rendering.
    // =====================================================================

    /**
      Sets shader variables that are common to all shaders used in this
      actor.
     */
    void setCommonShaderVars(
            std::shared_ptr<GL::MShaderEffect>& shader,
            MSceneViewGLWidget* sceneView);

    void setVarSpecificShaderVars(
            std::shared_ptr<GL::MShaderEffect>& shader,
            MSceneViewGLWidget* sceneView,
            MNWP3DVolumeActorVariable* var,
            const QString& structName,
            const QString& volumeName,
            const QString& transferFuncName,
            const QString& pressureTableName,
            const QString& surfacePressureName,
            const QString& hybridCoeffName,
            const QString& lonLatLevAxesName,
            const QString& pressureTexCoordTable2DName,
            const QString& minMaxAccelStructure3DName,
            const QString& dataFlagsVolumeName,
            const QString& auxPressureField3DName);

    void setRayCasterShaderVars(
            std::shared_ptr<GL::MShaderEffect>& shader,
            MSceneViewGLWidget* sceneView);

    void setBoundingBoxShaderVars(
            MSceneViewGLWidget* sceneView);

    void setNormalCurveShaderVars(
            std::shared_ptr<GL::MShaderEffect>& shader,
            MSceneViewGLWidget* sceneView);

    void setNormalCurveComputeShaderVars(
            std::shared_ptr<GL::MShaderEffect>& shader,
            MSceneViewGLWidget* sceneView);

    // Methods that do the OpenGL rendering work.
    // ==========================================

    void renderBoundingBox(MSceneViewGLWidget* sceneView);

    void renderPositionCross(MSceneViewGLWidget* sceneView);

    void renderRayCaster(std::shared_ptr<GL::MShaderEffect>& effect,
                         MSceneViewGLWidget* sceneView);

    void renderVolumeLighting(MSceneViewGLWidget *sceneView, bool interactiveChange = false);

    void renderIsoPositionsToShadow(MSceneViewGLWidget* sceneView);

    void renderNormalCurves(MSceneViewGLWidget* sceneView);

    void mirrorDepthBufferToTexture(MSceneViewGLWidget* sceneView);

    /**
     * Checks, whether all enabled variables have aligned data boundaries.
     * This is important for the empty space skipping that is used for DVR,
     * as it only supports aligned variable boundaries.
     * @param sceneView The currently rendering scene view
     * @return True, iff all variables, that are currently enabled and used, share the same data boundaries.
     */
    bool areVariableDataExtentsAligned(MSceneViewGLWidget *sceneView) const;

    /**
     * Calculates the current render region of the ray caster based on the ray caster bounds
     * and the variable data extents.
     * @param renderRegionBottomSWCrnr The output render region bottom SW corner.
     * @param renderRegionTopNECrnr The output render region top NE corner.
     * @param sceneView The rendering scene view.
     */
    void getRenderRegion(QVector3D &renderRegionBottomSWCrnr, QVector3D &renderRegionTopNECrnr,
                         MSceneViewGLWidget *sceneView) const;

    /**
     * Check all variables and return which are active and can be rendered.
     * For iso surface and bitfield rendering, only the first entry should return
     * true.
     * The returned vector will have length 3, for all possible variables.
     * @return A vector of booleans indicating, whether a variable can be rendered.
     */
    QVector<GLboolean> getActiveVariables() const;

    // Class members.
    // ==============

    /**
      Enums for render settings.
     */
    struct RenderMode
    {
        RenderMode() = default;
        enum Type { Original = 0, Bitfield, DVR, DVR_VOL_LIGHT };
        enum ColorType { IsoColor = 0, TransferFunction };
        enum ScaleFixMode { None = 0, Normal = 1, Physical = 2 };
    };

    /**
      Struct that accomodates OpenGL rendering resources.
     */
    struct OpenGL
    {
        OpenGL();

        // Shader effects.
        // ===============

        // shader for standard isosurface raycasting
        std::shared_ptr<GL::MShaderEffect> rayCasterEffect;

        // shader for ensemble bitfield isosurface raycasting
        std::shared_ptr<GL::MShaderEffect> bitfieldRayCasterEffect;
        // shader for simple bounding box rendering
        std::shared_ptr<GL::MShaderEffect> boundingBoxShader;
        // shaders to create and render shadow images
        std::shared_ptr<GL::MShaderEffect> shadowImageRenderShader;

        // shader to obtain init points for the normal curves
        std::shared_ptr<GL::MShaderEffect> normalCurveInitPointsShader;
        // shader to compute line segments of each normal curve
        std::shared_ptr<GL::MShaderEffect> normalCurveLineComputeShader;
        // shaders to render normal curves
        std::shared_ptr<GL::MShaderEffect> normalCurveGeometryEffect;

        // Vertex/index buffers.
        // =====================

        // bounding box vertex and index buffer
        GL::MVertexBuffer* vboBoundingBox;
        GLuint             iboBoundingBox;
        // position cross vertex buffer
        GL::MVertexBuffer* vboPositionCross;

        // Vertex buffer of the iso surface position buffer in world space.
        // Used to render it into shadow maps.
        GL::MVertexBuffer* vboIsoPositionBuffer;

        // shadow image render vertex buffer
        GL::MVertexBuffer* vboShadowImageRender;

        // ground image vertex buffer
        GL::MVertexBuffer* vboShadowImage;

        // normal curve vertices stored in a ssbo buffer to allow OpenGL to
        // read and write from/to that buffer - like UAVs in Direct3D 11
        GL::MShaderStorageBufferObject* ssboInitPoints;
        GL::MShaderStorageBufferObject* ssboNormalCurves;

        // Textures.
        // =========

        // Iso surface position buffer used for shadow mapping.
        // Contains fragment positions in world space from the scene lights
        // perspective.
        QMap<MSceneViewGLWidget*, GL::MTexture*> tex2DIsoPositionBuffer;

        // shadow image texture2D
        GL::MTexture* tex2DShadowImage;
        GLint         texUnitShadowImage;

        GLint         texUnitSceneShadow;

        GL::MTexture* volumeLightMap3DBuffer;
        GLint texUnitVolumeLightMap3D;

        // Texture that mirrors the OpenGL depth buffer. Used for DVR to
        // store the depths before the raycaster is started, so that the ray
        // can be terminated at the corresponding fragment depth. Each scene
        // view gets its own framebuffer object.
        GL::MTexture* textureDepthBufferMirror;
        GLint         textureUnitDepthBufferMirror;
        QHash<MSceneViewGLWidget*, GLuint> fboDepthBuffer;

        // Subroutines.
        // ============

        std::vector<QList<QString>> rayCasterSubroutines;
        std::vector<QList<QString>> bitfieldRayCasterSubroutines;
        std::vector<QList<QString>> normalCompSubroutines;
        std::vector<QList<QString>> normalInitSubroutines;

        // Iso surface position buffer generation parameters from the light source.
        QMap<MLightActor*, QMatrix4x4> isoDirLightMvp;
        QMap<MLightActor*, QVector3D> isoDirLightPos;
        QMap<MLightActor*, QVector3D> isoDirLightDir;

        // Guard for the position buffer and volume light map
        // to prevent them being updated for each
        // shadow cascade and only update it once in any frame it needs updating.
        bool cascadedShadowUpdateGuard;
    };

    /**
      Properties for volumetric lighting.
     */
    struct DVRLightSettings
    {
        DVRLightSettings(MNWPVolumeRaycasterActor *hostActor);

        bool enabled;

        MProperty groupProp;

        MProperty lightmapResolutionGroupProp;
        MIntProperty lightmapLonResProp;
        MIntProperty lightmapLatResProp;
        MIntProperty lightmapZResProp;

        MBoolProperty usePressureScaleProp;
        MIntProperty interactivityScaleFactorProp;

        MFloatProperty powderStrengthProp;
        MFloatProperty powderDepthProp;

        MFloatProperty densityScaleProp;

        MProperty lightSamplingGroup;
        MFloatProperty stepSizeProp;
        MFloatProperty interactiveStepSizeProp;
        MFloatProperty lightMaxDistanceProp;
    };

    /**
      Properties for direct volume rendered clouds.
     */
    struct CloudSettings
    {
        explicit CloudSettings(MNWPVolumeRaycasterActor *hostActor);

        GLint ciwcSlot;
        GLint clwcSlot;

        MProperty groupProp;
        MEnumProperty ciwcSlotProp;
        MEnumProperty clwcSlotProp;
        MColorProperty ciwcColorProp;
        MColorProperty clwcColorProp;
        MBoolProperty useShadingVarColorProp;
        MBoolProperty earthCosineEnabledProp;
        MBoolProperty realVerticalScaleProp;
        MBoolProperty uniformScaleProp;
        MFloatProperty spaceScaleProp;
    };

    /**
      Properties for a single isosurface.
     */
    struct IsoValueSettings
    {
        enum ColorType { ConstColor = 0,
                         TransferFuncColor,
                         TransferFuncShadingVar,
                         TransferFuncShadingVarMaxNeighbour };

        explicit IsoValueSettings(MNWPVolumeRaycasterActor *hostActor = nullptr,
                         int index = 0,
                         bool _enabled = true,
                         float _isoValue = 0.5,
                         int significantDigits = 1,
                         float singleStep = 0.1,
                         const QColor& _color = QColor(255,0,0,255),
                         ColorType _colorType = ColorType::ConstColor);

        MProperty *groupProperty() { return &enabledProp; }

        ColorType   isoColourType;

        MBoolProperty enabledProp;
        MSciFloatProperty isoValueProp;
        MIntProperty isoValueSignificantDigitsProp;
        MSciFloatProperty isoValueSingleStepProp;
        MColorProperty isoColourProp;
        MEnumProperty isoColourTypeProp;
        MButtonProperty isoValueRemoveProp;

        int index;
    };

    /**
      Data sampling settings
     */
    struct DataSamplingSettings
    {
        explicit DataSamplingSettings(MNWPVolumeRaycasterActor *hostActor);

        MNWPVolumeRaycasterActor      *hostActor;
        // properties
        MProperty                     groupProp;
        MSciFloatProperty             stepSizeProp;
        MSciFloatProperty             interactionStepSizeProp;
        MIntProperty                  bisectionStepsProp;
        MIntProperty                  interactionBisectionStepsProp;
    };

    /**
      Raycaster properties.
     */
    struct RayCasterSettings
    {
        explicit RayCasterSettings(MNWPVolumeRaycasterActor *hostActor);

        void sortIsoValues();

        void addIsoValue(
                bool enabled = true,
                bool hidden = false,
                float isoValue = 0.f,
                int decimals = 3,
                float singleStep = 0.1,
                QColor color = QColor(255, 255, 255, 255),
                IsoValueSettings::ColorType colorType
                = IsoValueSettings::ColorType::ConstColor);

        MNWPVolumeRaycasterActor      *hostActor;
        // iso value settings from GUI
        QVector<IsoValueSettings*>  isoValueSetList;
        // list need to be sorted so that crossing levels work correct
        QVector<GLint>                 isoEnabled;
        QVector<GLfloat>               isoValues;
        QVector<QVector4D>             isoColors;
        QVector<GLint>                 isoColorTypes;

        // properties
        MProperty                      groupProp;

        // Group for the iso values with an add button.
        MButtonProperty                isoValuesProp;
    };

    /**
      Properties for normal curves.
     */
    struct NormalCurveSettings
    {
        explicit NormalCurveSettings(MNWPVolumeRaycasterActor *hostActor);

        enum GlyphType { Line = 0, Box, Tube };
        enum Threshold { Steps = 0, StopIsoValue = 1 };
        enum CurveColor { ColorSteps = 0, ColorCurve, ColorIsoValue };
        enum Surface { Inner = 0, Outer };
        enum IntegrationDir { Backwards = 0, Forwards = 1, Both = 2};

        GlyphType           glyph;
        Threshold           threshold;
        CurveColor          colour;

        IntegrationDir      integrationDir;

        GLfloat             startIsoValue;
        GLfloat             stopIsoValue;

        MBoolProperty       normalCurvesEnabledProp;

        MProperty           groupRenderingSettingsProp;
        MEnumProperty       glyphProp;
        MFloatProperty      tubeRadiusProp;
        MEnumProperty       colourProp;

        MEnumProperty       thresholdProp;
        MEnumProperty       startIsoSurfaceProp;
        MEnumProperty       stopIsoSurfaceProp;

        MFloatProperty      stepSizeProp;
        MEnumProperty       integrationDirProp;
        MIntProperty        numLineSegmentsProp;

        MProperty           groupSeedSettingsProp;

        MProperty           groupSeedSpacingProp;
        MFloatProperty      initPointResXProp;
        MFloatProperty      initPointResYProp;
        MFloatProperty      initPointResZProp;

        MFloatProperty      initPointVarianceProp;

    };

    /**
      Properties specifically for direct volume rendering.
     */
    struct DVRSettings
    {
        explicit DVRSettings(MNWPVolumeRaycasterActor *hostActor);

        MProperty            groupProp;

        int refScaleUnit;
        RenderMode::ScaleFixMode scaleFix;

        MEnumProperty        scaleFixModeProp;
        MBoolProperty        useDitheringProp;
        MFloatProperty       ditherStrengthProp;
        MBoolProperty        secondVarEnabledProp;
        MNWPActorVarProperty secVariableProp;
        MBoolProperty        thirdVarEnabledProp;
        MNWPActorVarProperty thirdVariableProp;
        MEnumProperty        referenceScaleUnitProp;
        MFloatProperty       refScaleProp;

    };

    enum NextRenderFrameUpdateRequests
    {
        UpdateShadowImage = 0,
        ComputeNCInitPoints,
        RecomputeNCLines,
        RenderVolumeLighting,
        Size
    };

    std::bitset<NextRenderFrameUpdateRequests::Size> updateNextRenderFrame;

    // "Top-level" properties.
    RenderMode::Type        renderMode;
    MEnumProperty           renderModeProp;

    // List that stores the names of all registered actor variables (for the
    // enum properties that allow the user to select observed and shading
    // variables.
    QList<QString>          varNameList;

    // Variable that is raycasted.
    MNWPActorVarProperty    varProp;


    // Variable that is used to shade an isosurface set to "transfer function
    // shading".
    MNWPActorVarProperty    shadingVarProp;

    OpenGL                  gl;
    MBoolProperty           bBoxEnabledProp;
    DataSamplingSettings    *dataSamplingSettings;
    RayCasterSettings       *rayCasterSettings;
    DVRLightSettings        *volumeLightingSettings;
    CloudSettings           *cloudSettings;
    DVRSettings             *dvrSettings;
    NormalCurveSettings     *normalCurveSettings;
    // number of normal curve vertices to draw
    uint32_t                normalCurveNumVertices;

    /**
      Struct that describes a single normal curve vertex.
     */
    struct NormalCurveLineSegment
    {
        // world position
        float x,y,z;
        // value responsible for normal curve color mapping
        float value;
    };

    // initial seed points for the normal curves
    GLuint                                      numNormalCurveInitPoints;
    // normal curve lines
    std::vector<NormalCurveLineSegment>         normalCurves;
};

} // namespace Met3D

#endif // LONLATMULTIPURPOSEACTOR_H
