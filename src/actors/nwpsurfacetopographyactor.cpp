/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017      Bianca Tost [+]
**  Copyright 2024      Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "nwpsurfacetopographyactor.h"

// standard library imports
#include <iostream>

// related third party imports
#include <QObject>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/datasource/selectdatasourcedialog.h"
#include "data/structuredgrid.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWPSurfaceTopographyActor::MNWPSurfaceTopographyActor()
    : MNWPMultiVarActor(),
      MBoundingBoxInterface(this, MBoundingBoxConnectionType::HORIZONTAL),
      updateRenderRegion(false),
      texUnitShadowMap(-1)
{
    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    // Hide label settings
    labelProps.enabledProp.setHidden(true);

    topographyVariableProp = MNWPActorVarProperty("Topography variable");
    topographyVariableProp.setConfigKey("topography_var");
    topographyVariableProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(topographyVariableProp);

    shadingVariableProp = MNWPActorVarProperty("Shading variable");
    shadingVariableProp.setConfigKey("shading_var");
    shadingVariableProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(shadingVariableProp);

    // Bounding box of the actor.
    insertBoundingBoxProperty(&actorPropertiesSupGroup);
}


MNWPSurfaceTopographyActor::~MNWPSurfaceTopographyActor()
{
    if (texUnitShadowMap >= 0)
    {
        releaseTextureUnit(texUnitShadowMap);
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWPSurfaceTopographyActor::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");
    shaderProgram->compileFromFile_Met3DHome("src/glsl/surface_topography.fx.glsl");
}


QList<MVerticalLevelType> MNWPSurfaceTopographyActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>() << SINGLE_LEVEL);
}


MNWPActorVariable* MNWPSurfaceTopographyActor::createActorVariable(
        const MSelectableDataVariable& dataVariable)
{
    auto* newVar = new MNWP2DHorizontalActorVariable(this);

    // Remove property groups not needed by surface topography actor.
    newVar->varRenderingGroupProp.removeSubProperty(
                newVar->renderSettings.renderModeProp);
    newVar->varRenderingGroupProp.removeSubProperty(
                newVar->renderSettings.contourSetGroupProp);
    newVar->varRenderingGroupProp.removeSubProperty(
                newVar->contourLabelSuffixProp);
    newVar->varRenderingGroupProp.removeSubProperty(
                newVar->spatialTransferFunctionProp);

    newVar->dataSourceID = dataVariable.dataSourceID;
    newVar->levelType = dataVariable.levelType;
    newVar->variableName = dataVariable.variableName;

    return newVar;
}


void MNWPSurfaceTopographyActor::saveConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::saveConfiguration(settings);
    MBoundingBoxInterface::saveConfiguration(settings);
}


void MNWPSurfaceTopographyActor::loadConfiguration(QSettings *settings)
{
    MNWPMultiVarActor::loadConfiguration(settings);
    MBoundingBoxInterface::loadConfiguration(settings);
}


void MNWPSurfaceTopographyActor::loadConfigurationPrior_V_1_14(
        QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationPrior_V_1_14(settings);
    MBoundingBoxInterface::loadConfiguration(settings);

    settings->beginGroup(MNWPSurfaceTopographyActor::getSettingsID());

    int tmpTopographyVariableIndex = settings->value(
            "topographyVariableIndex").toInt();
    // Check index bounds in case any actor variable was not loaded correctly.
    tmpTopographyVariableIndex = std::max(0, tmpTopographyVariableIndex);
    tmpTopographyVariableIndex = std::min(variables.size()-1, tmpTopographyVariableIndex);

    int tmpShadingVariableIndex = settings->value(
            "shadingVariableIndex").toInt();
    tmpShadingVariableIndex = std::max(0, tmpShadingVariableIndex);
    tmpShadingVariableIndex = std::min(variables.size()-1, tmpShadingVariableIndex);

    topographyVariableProp.setIndex(tmpTopographyVariableIndex);
    shadingVariableProp.setIndex(tmpShadingVariableIndex);

    settings->endGroup();
}


void MNWPSurfaceTopographyActor::onBoundingBoxChanged()
{
    if (suppressActorUpdates())
    {
        return;
    }

    // The bbox position has changed. In the next render cycle, update the
    // render region, download target grid from GPU and update contours.
    updateRenderRegion = true;
    emitActorChangedSignal();
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MNWPSurfaceTopographyActor::initializeActorResources()
{
    // Parent initialisation.
    MNWPMultiVarActor::initializeActorResources();

    // Build list with NWPActorVariable names for GUI enum properties.
    for (MNWPActorVariable *var : variables)
    {
        topographyVariableProp.addVariable(var);
        shadingVariableProp.addVariable(var);
    }

    texUnitShadowMap = assignTextureUnit();

    // Compute the grid indices that correspond to the current bounding box
    // (the bounding box can have different extents than the data grid) during
    // the first render cycle.
    updateRenderRegion = true;

    // Load shader.
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    bool loadShaders = false;
    loadShaders |= glRM->generateEffectProgram("surfacetopography_shader",
                                               shaderProgram);

    if (loadShaders) reloadShaderEffects();
}


void MNWPSurfaceTopographyActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (variables.empty() || getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }
    if (texUnitShadowMap < 0)
    {
        texUnitShadowMap = assignTextureUnit();
    }

    // Shortcuts to the variable's properties.
    auto* varTopo = dynamic_cast<MNWP2DHorizontalActorVariable*>(
            topographyVariableProp.value());
    auto* varShading = dynamic_cast<MNWP2DHorizontalActorVariable*>(
            shadingVariableProp.value());

    if ( !varTopo->hasData() )
    {
        return;
    }

    // UPDATE REGION PARAMETERS if bounding box has changed.
    // =====================================================
    if (updateRenderRegion)
    {
        // This method might already be called between initial data request and
        // all data fields being available. Return if not all variables
        // contain valid data yet.
        bool canCompute = true;
        for (MNWPActorVariable *var : variables)
        {
            if (!var->hasData())
            {
                canCompute = false;
            }
        }

        if (canCompute)
        {
            computeRenderRegionParameters();
            updateRenderRegion = false;
        }
    }


    // Bind shader program that renders the volume slice.
    shaderProgram->bind();

    bool useTransferFunction = varShading && varShading->transferFunction && varShading->hasData();
    if (useTransferFunction)
    {
        varShading->transferFunction->getTexture()->bindToTextureUnit(
                varShading->textureUnitTransferFunction);
        shaderProgram->setUniformValue(
                "transferFunction", varShading->textureUnitTransferFunction);
        shaderProgram->setUniformValue(
                "scalarMinimum", varShading->transferFunction->getMinimumValue());
        shaderProgram->setUniformValue(
                "scalarMaximum", varShading->transferFunction->getMaximumValue());

        // Bind shading variable.
        varShading->textureDataField->bindToTextureUnit(varShading->textureUnitDataField);
        shaderProgram->setUniformValue("dataField", varShading->textureUnitDataField);
    }
    else
    {
        shaderProgram->setUniformValue("dataField", varTopo->textureUnitDummy2D);
        shaderProgram->setUniformValue("transferFunction", varTopo->textureUnitDummy1D);
    }

    shaderProgram->setUniformValue("useTransferFunction", useTransferFunction);

    // Model-view-projection matrix from the current scene view.
    shaderProgram->setUniformValue(
            "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));

    // Texture bindings for Lat/Lon axes (1D textures).
    varTopo->textureLonLatLevAxes->bindToTextureUnit(varTopo->textureUnitLonLatLevAxes);
    shaderProgram->setUniformValue("latLonAxesData", varTopo->textureUnitLonLatLevAxes);
    shaderProgram->setUniformValue("latOffset", GLint(varTopo->grid->nlons));

    // Texture bindings for data field (2D texture).
    varShading->textureDataField->bindToTextureUnit(varShading->textureUnitDataField);
    shaderProgram->setUniformValue("dataField", varShading->textureUnitDataField);

    // Texture bindings for surface topography (2D texture).
    varTopo->textureDataField->bindToTextureUnit(varTopo->textureUnitDataField);
    shaderProgram->setUniformValue(
            "surfaceTopography", varTopo->textureUnitDataField);
    shaderProgram->setUniformValue(
            "pToWorldZParams", sceneView->pressureToWorldZParameters());

    // Grid offsets to render only the requested subregion.
    shaderProgram->setUniformValue("iOffset", GLint(varTopo->i0)); CHECK_GL_ERROR;
    shaderProgram->setUniformValue("jOffset", GLint(varTopo->j0)); CHECK_GL_ERROR;
    shaderProgram->setUniformValue(
            "bboxLons", QVector2D(getBBoxConnection()->westLon(),
                                  getBBoxConnection()->eastLon())); CHECK_GL_ERROR;
    shaderProgram->setUniformValue(
            "isCyclicGrid",
            GLboolean(varTopo->grid->gridIsCyclicInLongitude())); CHECK_GL_ERROR;
    shaderProgram->setUniformValue(
            "leftGridLon", GLfloat(varTopo->grid->lons[0])); CHECK_GL_ERROR;
    shaderProgram->setUniformValue(
            "eastGridLon", GLfloat(varTopo->grid->lons[varTopo->grid->nlons - 1]));
    CHECK_GL_ERROR;
    shaderProgram->setUniformValue(
            "shiftForWesternLon",
            GLfloat(varTopo->shiftForWesternLon)); CHECK_GL_ERROR;

    sceneView->bindShadowMap(shaderProgram, texUnitShadowMap);

    // Use instanced rendering to avoid geometry upload (see notes 09Feb2012).
    glPolygonMode(GL_FRONT_AND_BACK,
                  renderAsWireFrameProp ? GL_LINE : GL_FILL); CHECK_GL_ERROR;
    glDrawArraysInstanced(GL_TRIANGLE_STRIP,
                          0,
                          varTopo->nlons * 2,
                          varTopo->nlats - 1); CHECK_GL_ERROR;
}


void MNWPSurfaceTopographyActor::dataFieldChangedEvent()
{
    emitActorChangedSignal();
}


void MNWPSurfaceTopographyActor::computeRenderRegionParameters()
{
    if (getBBoxConnection()->getBoundingBox() == nullptr)
    {
        return;
    }

    // Compute render region parameters for each variable.
    for (int vi = 0; vi < variables.size(); vi++)
    {
        MNWP2DHorizontalActorVariable* var =
                static_cast<MNWP2DHorizontalActorVariable*> (variables.at(vi));

        var->computeRenderRegionParameters(
            getBBoxConnection()->westLon(), getBBoxConnection()->southLat(),
            getBBoxConnection()->eastLon(), getBBoxConnection()->northLat());
    }
}


void MNWPSurfaceTopographyActor::onDeleteActorVariable(
        MNWPActorVariable *var)
{
    topographyVariableProp.removeVariable(var);
    shadingVariableProp.removeVariable(var);
}


void MNWPSurfaceTopographyActor::onAddActorVariable(
        MNWPActorVariable *var)
{
    topographyVariableProp.addVariable(var);
    shadingVariableProp.addVariable(var);

    updateRenderRegion = true;
}


void MNWPSurfaceTopographyActor::onChangeActorVariable(MNWPActorVariable *var)
{
    enableActorUpdates(false);
    topographyVariableProp.changeVariable(var);
    shadingVariableProp.changeVariable(var);
    enableActorUpdates(true);

    updateRenderRegion = true;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/


} // namespace Met3D
