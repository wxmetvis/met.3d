/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Alexander Klassen
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "geometryactor.h"

// standard library imports
#include <iomanip>
#include <utility>

// related third party imports
#include <QFile>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "util/mutil.h"
#include "util/mfiletypes.h"

namespace Met3D
{

const QString MESH_VERTICES_REQUEST_KEY = "geometry_vertices_actor#";
const QString MESH_NORMALS_REQUEST_KEY = "geometry_normals_actor#";
const QString MESH_UV_REQUEST_KEY = "geometry_uv_actor#";
const QString MESH_VERTEX_ATTRIB_REQUEST_KEY = "geometry_attribute_actor#";
const int SHADER_VERTEX_ATTRIBUTE = 0;
const int SHADER_NORMAL_ATTRIBUTE = 1;
const int SHADER_UV_ATTRIBUTE = 2;
const int SHADER_EXTRA_ATTRIBUTE = 3;

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MGeometryActor::MGeometryActor()
        : MNWPMultiVarActor(),
          spatialTransferFunction(nullptr),
          textureUnitSpatialTransferFunction(-1),
          texUnitShadowMap(-1),
          meshReader(nullptr)
{
    enablePicking(true);

    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    fileNameProp = MFileProperty("OBJ file", "");
    fileNameProp.setConfigKey("obj_file");
    fileNameProp.setFileFilter(FileTypes::M_WAVEFRONT_OBJ);
    fileNameProp.setDialogTitle("Open OBJ file");
    fileNameProp.setDialogDirectory("/home/");
    fileNameProp.registerValueCallback([=]()
    {
        requestMesh(fileNameProp);
    });
    actorPropertiesSupGroup.addSubProperty(fileNameProp);

    attFileNameProp = MFileProperty("Attribute file", "");
    attFileNameProp.setConfigKey("attribute_file");
    attFileNameProp.setFileFilter(FileTypes::M_TXT);
    attFileNameProp.setDialogTitle("Open attribute .txt file");
    attFileNameProp.setDialogDirectory("/home/");
    attFileNameProp.registerValueCallback([=]()
    {
        transferFunctionProp.setEnabled(!attFileNameProp.value().isEmpty());
        requestMesh(fileNameProp);
    });
    actorPropertiesSupGroup.addSubProperty(attFileNameProp);

    verticalCoordSpaceProp = MEnumProperty("Vertical coordinate units", {"hPa", "m", "km", "graphics space Z"});
    verticalCoordSpaceProp.setConfigKey("vertical_coordinate_units");
    verticalCoordSpaceProp.registerValueCallback([=]()
    {
        requestMesh(fileNameProp);
    });
    actorPropertiesSupGroup.addSubProperty(verticalCoordSpaceProp);

    // Add transform component and its properties.
    transformComponent = this->addComponentOfType<MTransformComponent>(
            QString("Transform"), true, true, true, "Mesh transformation");

    transferFunctionProp = MTransferFunction1DProperty("Transfer function");
    transferFunctionProp.setConfigKey("transfer_function");
    transferFunctionProp.setEnabled(!attFileNameProp.value().isEmpty());
    transferFunctionProp.registerValueCallback(this, [=]()
    {
        spatialTransferFunction = dynamic_cast<MTransferFunction1D*>(transferFunctionProp.value());
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(transferFunctionProp);

    varProp = MNWPActorVarProperty("Shading variable");
    varProp.setConfigKey("shading_var");
    varProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(varProp);

    // Hide label properties as they are not used in this actor.
    labelProps.enabledProp.setHidden(true);
}


MGeometryActor::~MGeometryActor()
{
    // Release all vertex buffers.
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    for (MeshRenderData& data : buffers)
    {
        if (data.vertexBuffer)
        {
            glRM->releaseGPUItem(data.vertexBuffer);
        }

        if (data.normalBuffer)
        {
            glRM->releaseGPUItem(data.normalBuffer);
        }

        if (data.uvBuffer)
        {
            glRM->releaseGPUItem(data.uvBuffer);
        }

        if (data.attribBuffer)
        {
            glRM->releaseGPUItem(data.attribBuffer);
        }
    }

    // Release mesh.
    if (meshReader)
    {
        for (MMesh *mesh : meshes)
        {
            meshReader->releaseData(mesh);
        }

        delete meshReader;
    }

    if (texUnitShadowMap >= 0)
    {
        releaseTextureUnit(texUnitShadowMap);
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MGeometryActor::reloadShaderEffects()
{
    LOG4CPLUS_DEBUG(mlog, "Loading shader programs.");

    beginCompileShaders(1);

    compileShadersFromFileWithProgressDialog(
            simpleGeometryEffect, "src/glsl/simple_mesh_shader.fx.glsl");

    endCompileShaders();
}


void MGeometryActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationPrior_V_1_14(settings);

    settings->beginGroup(MGeometryActor::getSettingsID());

    bool zIsMeters = settings->value("zIsMeters", false).toBool();

    int meterScale = settings->value("meterScale", 1).toInt();

    bool zIsPressure = settings->value("zIsPressure", false).toBool();

    if(zIsMeters)
    {
        if (meterScale == 1000)
        {
            verticalCoordSpaceProp = VerticalCoordSpace::KM;
        }
        else
        {
            verticalCoordSpaceProp = VerticalCoordSpace::M;
        }
    }
    else if (zIsPressure)
    {
        verticalCoordSpaceProp = VerticalCoordSpace::HPA;
    }
    else
    {
        verticalCoordSpaceProp = VerticalCoordSpace::Z;
    }

    fileNameProp = settings->value("filename").toString();

    attFileNameProp = settings->value("attributeFile", "").toString();

    int tf = settings->value("transferFunction", 0).toInt();
    transferFunctionProp.setIndex(tf);
    transferFunctionProp.setEnabled(!attFileNameProp.value().isEmpty());

    int varIndex = static_cast<int8_t>(settings->value("varIndex").toInt());
    varProp.setIndex(varIndex + 1);

    if (!fileNameProp.value().isEmpty())
    {
        requestMesh(fileNameProp);
    }

    settings->endGroup();
}


void MGeometryActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    float scale = verticalCoordSpaceProp != VerticalCoordSpace::Z ? sceneView->getVerticalScaling() : 0.0f;
    if (!meshes.contains(scale))
    {
        requestMesh(fileNameProp); // Request the meshes again.
        return;
    }

    MMesh *mesh = meshes[scale];
    MeshRenderData& data = buffers[scale];

    // Bind shader program.
    if (!data.vertexBuffer)
    {
        return;
    }
    if (!mesh)
    {
        return;
    }

    QMatrix4x4 vp = *(sceneView->getModelViewProjectionMatrix());
    QMatrix4x4 modelMatrix = transformComponent->getTransformMatrix(true, true, true);
    QMatrix4x4 normalModelMatrix = transformComponent->getTransformMatrix(false, true, true);
    normalModelMatrix = normalModelMatrix.inverted().transposed();

    MNWPActorVariable *var = varProp.value();

    bool varColor = var != nullptr && var->hasData() && var->transferFunction != nullptr;
    bool vertexAttribColor = !attFileNameProp.value().isEmpty() && !mesh->vertexAttributes.isEmpty() && spatialTransferFunction != nullptr;

    if (varColor)
    {
        simpleGeometryEffect->bindProgram("VarShadedMesh");
    }
    else if (vertexAttribColor)
    {
        simpleGeometryEffect->bindProgram("AttributeTexturedMesh");
    }
    else
    {
        simpleGeometryEffect->bindProgram("UntexturedMesh");
    }

    if (texUnitShadowMap == -1)
    {
        texUnitShadowMap = assignTextureUnit();
    }

    sceneView->bindShadowMap(simpleGeometryEffect, texUnitShadowMap);

    // Set uniform and attribute values.
    simpleGeometryEffect->setUniformValue("vpMatrix", vp);
    simpleGeometryEffect->setUniformValue("modelMatrix", modelMatrix);
    simpleGeometryEffect->setUniformValue("normalMatrix", normalModelMatrix);
    simpleGeometryEffect->setUniformValue(
            "pToWorldZParams", sceneView->pressureToWorldZParameters());

    if(vertexAttribColor && data.attribBuffer && spatialTransferFunction && !varColor)
    {
        spatialTransferFunction->getTexture()->bindToTextureUnit(textureUnitSpatialTransferFunction);
        simpleGeometryEffect->setUniformValue("transferFunction", textureUnitSpatialTransferFunction);
        simpleGeometryEffect->setUniformValue("scalarMinimum", spatialTransferFunction->getMinimumValue());
        simpleGeometryEffect->setUniformValue("scalarMaximum", spatialTransferFunction->getMaximumValue());
    }
    else
    {
        simpleGeometryEffect->setUniformValue("geometryColor", QVector3D(0.8, 0.8, 0.8));
    }

    if (varColor)
    {
        setVarSpecificShaderVars(simpleGeometryEffect, sceneView, "dataExtent",
                                 "dataVolume","transferFunction",
                                 "pressureTable", "surfacePressure",
                                 "hybridCoefficients", "lonLatLevAxes",
                                 "pressureTexCoordTable2D", "auxPressureField3D_hPa");
    }

    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.indexBuffer);
    CHECK_GL_ERROR;

    data.vertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE);
    data.normalBuffer->attachToVertexAttribute(SHADER_NORMAL_ATTRIBUTE);

    //uvBuffer->attachToVertexAttribute(SHADER_UV_ATTRIBUTE);

    if (vertexAttribColor && data.attribBuffer)
    {
        data.attribBuffer->attachToVertexAttribute(SHADER_EXTRA_ATTRIBUTE);
    }

    glPolygonMode(GL_FRONT_AND_BACK, renderAsWireFrameProp ? GL_LINE : GL_FILL);
    CHECK_GL_ERROR;

    glDrawElements(GL_TRIANGLES, mesh->indices.length(), GL_UNSIGNED_INT, nullptr);
    CHECK_GL_ERROR;

    glDisableVertexAttribArray(SHADER_VERTEX_ATTRIBUTE);
    glDisableVertexAttribArray(SHADER_NORMAL_ATTRIBUTE);
    glDisableVertexAttribArray(SHADER_UV_ATTRIBUTE);
    glDisableVertexAttribArray(SHADER_EXTRA_ATTRIBUTE);

    // Unbind VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    CHECK_GL_ERROR;
}


void MGeometryActor::dragEvent(MSceneViewGLWidget *sceneView, int handleID,
                               float clipX, float clipY)
{
    emitActorChangedSignal();
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MGeometryActor::initializeActorResources()
{
    bool loadShaders = false;
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    loadShaders |=
            glRM->generateEffectProgram("geometry_shader_program", simpleGeometryEffect);

    if (loadShaders) reloadShaderEffects();

    for (MNWPActorVariable* variable : variables)
    {
        varProp.addVariable(variable);
    }
}


void MGeometryActor::uploadGeometryToGPU(float id)
{
    MMesh *mesh = meshes[id];
    MeshRenderData& data = buffers[id];

    if (!mesh)
    {
        return;
    }

    auto *glRM = MGLResourcesManager::getInstance();

    // Upload geometry data to VBO.
    const QString vertexRequestKey = MESH_VERTICES_REQUEST_KEY +
            QString::number(getID()) + "_s_" + QString::number(id);

    // Release once, if it already exists.
    if (data.vertexBuffer)
    {
        glRM->releaseGPUItem(vertexRequestKey);
    }

    uploadVec3ToVertexBuffer(mesh->vertices, vertexRequestKey, &data.vertexBuffer);

    if (!mesh->normals.isEmpty())
    {
        const QString normalsRequestKey = MESH_NORMALS_REQUEST_KEY +
                QString::number(getID()) + "_s_" + QString::number(id);

        // Release once, if it already exists.
        if (data.normalBuffer)
        {
            glRM->releaseGPUItem(normalsRequestKey);
        }

        uploadVec3ToVertexBuffer(mesh->normals, normalsRequestKey, &data.normalBuffer);
    }
    if (!mesh->textureCoords.isEmpty())
    {
        const QString uvRequestKey = MESH_UV_REQUEST_KEY +
                QString::number(getID()) + "_s_" + QString::number(id);

        // Release once, if it already exists.
        if (data.uvBuffer)
        {
            glRM->releaseGPUItem(uvRequestKey);
        }

        uploadVec2ToVertexBuffer(mesh->textureCoords, uvRequestKey, &data.uvBuffer);
    }
    if (!mesh->vertexAttributes.empty())
    {
        const QString attribRequestKey = MESH_VERTEX_ATTRIB_REQUEST_KEY +
                QString::number(getID()) + "_s_" + QString::number(id);

        // Release once, if it already exists.
        if (data.attribBuffer)
        {
            glRM->releaseGPUItem(attribRequestKey);
        }

        uploadFloatToVertexBuffer(mesh->vertexAttributes, attribRequestKey, &data.attribBuffer);
    }

    if (!mesh->indices.empty())
    {
        glGenBuffers(1, &data.indexBuffer);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, data.indexBuffer);
        CHECK_GL_ERROR;
        glBufferData(GL_ELEMENT_ARRAY_BUFFER,
                     static_cast<GLsizeiptr>(mesh->indices.length() * sizeof(GLuint)),
                     mesh->indices.data(), GL_STATIC_DRAW);
        CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        CHECK_GL_ERROR;
    }

    if (textureUnitSpatialTransferFunction == -1)
    {
        textureUnitSpatialTransferFunction = assignTextureUnit();
    }

    emitActorChangedSignal();
}


void MGeometryActor::requestMesh(const QString &filename)
{
    if (!meshReader)
    {
        meshReader = new MObjReader();

        // connect async signal
        bool success = connect(meshReader, &MObjReader::dataRequestCompleted,
                        this, &MGeometryActor::asynchronousDataAvailable);
        Q_ASSERT(success);
    }

    if (filename.isEmpty()) return;

    if (!QFileInfo(filename).exists())
    {
        LOG4CPLUS_ERROR(mlog, "Given geometry filename does not exist. "
                              "Cannot read file " << filename << ".");
        fileNameProp = "";
        return;
    }

    bool zIsMeters = verticalCoordSpaceProp == VerticalCoordSpace::M
            || verticalCoordSpaceProp == VerticalCoordSpace::KM;
    bool zIsPressure = verticalCoordSpaceProp == VerticalCoordSpace::HPA;
    int meterScale = 1;

    if (zIsMeters)
    {
        if (verticalCoordSpaceProp == VerticalCoordSpace::KM)
        {
            meterScale = 1000;
        }
    }

    MDataRequestHelper rh;
    rh.insert("FILENAME", filename);
    rh.insert("CONVERT_Z_M_TO_HPA", zIsMeters);
    rh.insert("CONVERT_Z_P_TO_Z", zIsPressure);
    rh.insert("METER_SCALE", meterScale);
    rh.insert("ATTRIBUTE_FILE", attFileNameProp);

    currentRequests.clear();

    if (zIsMeters || zIsPressure)
    {
        for (MSceneViewGLWidget* view : getViews())
        {
            float scale = view->getVerticalScaling();

            // We only need one mesh per vertical scale, not per scene.
            if (currentRequests.contains(scale)) continue;

            QVector2D params = view->pressureToWorldZParameters();
            auto query = QString("%1/%2").arg(params.x()).arg(params.y());
            rh.insert("P_TO_Z_PARAMS", query);
            rh.insert("SCENE_SCALE", QString("%1").arg(scale));
            MDataRequest request = rh.request();
            currentRequests.insert(scale, request);
            meshReader->requestData(request);
        }
    }
    else // If we load directly to world coordinates, we only need 1 mesh.
    {
        rh.insert("P_TO_Z_PARAMS", "0.0f/0.0f");
        rh.insert("SCENE_SCALE", QString("%1").arg(0.0f));
        MDataRequest request = rh.request();
        currentRequests.insert(0.0f, request);
        meshReader->requestData(request);
    }
}


void MGeometryActor::asynchronousDataAvailable(MDataRequest request)
{
    MDataRequestHelper rh(request);
    float scale = rh.value("SCENE_SCALE").toFloat();

    if (!currentRequests.contains(scale)) return;

    MMesh *newMesh = dynamic_cast<MMesh *>(meshReader->getData(std::move(request)));

    if (newMesh)
    {
        if (meshes.contains(scale) && meshes[scale] != nullptr)
        {
            if (buffers[scale].indexBuffer > 0)
            {
                glDeleteBuffers(1, &buffers[scale].indexBuffer);
                CHECK_GL_ERROR;
            }
            meshReader->releaseData(meshes[scale]);
        }
        meshes[scale] = newMesh;
        if (!buffers.contains(scale))
        {
            buffers.insert(scale, {});
        }
        uploadGeometryToGPU(scale);
    }
}


QList<MVerticalLevelType> MGeometryActor::supportedLevelTypes()
{
    return { PRESSURE_LEVELS_3D, HYBRID_SIGMA_PRESSURE_3D, AUXILIARY_PRESSURE_3D };
}


MNWPActorVariable *
MGeometryActor::createActorVariable(const MSelectableDataVariable &dataVariable)
{
    auto* newVar = new MNWPActorVariable(this);

    newVar->dataSourceID = dataVariable.dataSourceID;
    newVar->levelType = dataVariable.levelType;
    newVar->variableName = dataVariable.variableName;

    return newVar;
}


void MGeometryActor::onDeleteActorVariable(MNWPActorVariable *variable)
{
    MNWPMultiVarActor::onDeleteActorVariable(variable);

    varProp.removeVariable(variable);
}


void MGeometryActor::onAddActorVariable(MNWPActorVariable *variable)
{
    MNWPMultiVarActor::onAddActorVariable(variable);

    varProp.addVariable(variable);
}


void MGeometryActor::onChangeActorVariable(MNWPActorVariable *variable)
{
    MNWPMultiVarActor::onChangeActorVariable(variable);

    varProp.changeVariable(variable);
}


void MGeometryActor::setVarSpecificShaderVars(
        std::shared_ptr<GL::MShaderEffect>& shader,
        MSceneViewGLWidget* sceneView,
        const QString& structName,
        const QString& volumeName,
        const QString& transferFuncName,
        const QString& pressureTableName,
        const QString& surfacePressureName,
        const QString& hybridCoeffName,
        const QString& lonLatLevAxesName,
        const QString& pressureTexCoordTable2DName,
        const QString& auxPressureField3DName
)
{
    // Reset optional textures to avoid draw errors.
    // =============================================

    MNWPActorVariable *var = varProp.value();

    // 1D textures...
    var->textureDummy1D->bindToTextureUnit(var->textureUnitDummy1D);
    shader->setUniformValue(pressureTableName, var->textureUnitDummy1D);
    CHECK_GL_ERROR;
    shader->setUniformValue(hybridCoeffName, var->textureUnitDummy1D);
    CHECK_GL_ERROR;
    if (!transferFuncName.isNull())
    {
        shader->setUniformValue(transferFuncName,
                                var->textureUnitTransferFunction);
        CHECK_GL_ERROR;
    }

    // 2D textures...
    var->textureDummy2D->bindToTextureUnit(var->textureUnitDummy2D);
    shader->setUniformValue(surfacePressureName,
                            var->textureUnitDummy2D);
    CHECK_GL_ERROR;
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
    shader->setUniformValue(pressureTexCoordTable2DName,
                            var->textureUnitDummy2D);
    CHECK_GL_ERROR;
#endif

    // 3D textures...
    var->textureDummy3D->bindToTextureUnit(var->textureUnitDummy3D);
    shader->setUniformValue(auxPressureField3DName,
                            var->textureUnitDummy3D);
    CHECK_GL_ERROR;


    // Bind textures and set uniforms.
    // ===============================

    // Bind volume data
    var->textureDataField->bindToTextureUnit(
            var->textureUnitDataField);
    CHECK_GL_ERROR;
    shader->setUniformValue(
            volumeName,
            var->textureUnitDataField);
    CHECK_GL_ERROR;

    // Texture bindings for transfer function for data field (1D texture from
    // transfer function class).
    if (var->transferFunction && !transferFuncName.isNull())
    {
        var->transferFunction->getTexture()->bindToTextureUnit(
                var->textureUnitTransferFunction);
        shader->setUniformValue(
                transferFuncName,
                var->textureUnitTransferFunction);
        CHECK_GL_ERROR;

        shader->setUniformValue(
                structName + ".tfMinimum",
                var->transferFunction->getMinimumValue());
        CHECK_GL_ERROR;
        shader->setUniformValue(
                structName + ".tfMaximum",
                var->transferFunction->getMaximumValue());
        CHECK_GL_ERROR;
    }
    else
    {
        shader->setUniformValue(structName + ".tfMinimum", 0.);
        CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".tfMaximum", 0.);
        CHECK_GL_ERROR;
    }

    var->textureLonLatLevAxes->bindToTextureUnit(var->textureUnitLonLatLevAxes);
    shader->setUniformValue(lonLatLevAxesName, var->textureUnitLonLatLevAxes);
    CHECK_GL_ERROR;

    // Set uniforms specific to data var level type.
    // =============================================

    QVector3D dataNWCrnr = var->grid->getNorthWestTopDataVolumeCorner_lonlatp();
    dataNWCrnr.setZ(static_cast<float>(sceneView->worldZfromPressure(dataNWCrnr.z())));
    QVector3D dataSECrnr = var->grid
                              ->getSouthEastBottomDataVolumeCorner_lonlatp();
    dataSECrnr.setZ(static_cast<float>(sceneView->worldZfromPressure(dataSECrnr.z())));

    if (var->grid->getLevelType() == PRESSURE_LEVELS_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(0));
        CHECK_GL_ERROR;

        // Bind pressure to texture coordinate LUT.
        var->texturePressureTexCoordTable->bindToTextureUnit(
                var->textureUnitPressureTexCoordTable);
        shader->setUniformValue(
                pressureTableName,
                var->textureUnitPressureTexCoordTable);
        CHECK_GL_ERROR;

        // Helper variables for texture coordinate LUT.
        const GLint nPTable = var->texturePressureTexCoordTable->getWidth();
        const GLfloat deltaZ_PTable =
                abs(dataSECrnr.z() - dataNWCrnr.z()) / ((float)nPTable - 1);
        const GLfloat upperPTableBoundary =
                dataNWCrnr.z() + deltaZ_PTable / 2.0f;
        const GLfloat vertPTableExtent =
                abs(dataNWCrnr.z() - dataSECrnr.z()) + deltaZ_PTable;
        // shader->setUniformValue(structName + ".nPTable", nPTable); CHECK_GL_ERROR;
        // shader->setUniformValue(structName + ".deltaZ_PTable", deltaZ_PTable); CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".upperPTableBoundary",
                                upperPTableBoundary);
        CHECK_GL_ERROR;
        shader->setUniformValue(structName + ".vertPTableExtent",
                                vertPTableExtent);
        CHECK_GL_ERROR;
    }

    else if (var->grid->getLevelType() == LOG_PRESSURE_LEVELS_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(2));
        CHECK_GL_ERROR;
    }

    else if (var->grid->getLevelType() == HYBRID_SIGMA_PRESSURE_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(1));
        CHECK_GL_ERROR;

        // Bind hybrid coefficients
        var->textureHybridCoefficients
           ->bindToTextureUnit(var->textureUnitHybridCoefficients);
        shader->setUniformValue(
                hybridCoeffName,
                var->textureUnitHybridCoefficients);
        CHECK_GL_ERROR;

        // Bind surface pressure
        var->textureSurfacePressure
           ->bindToTextureUnit(var->textureUnitSurfacePressure);
        shader->setUniformValue(
                surfacePressureName,
                var->textureUnitSurfacePressure);
        CHECK_GL_ERROR;

#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
        // Bind pressure to texture coordinate LUT.
        var->texturePressureTexCoordTable->bindToTextureUnit(
                var->textureUnitPressureTexCoordTable);
        shader->setUniformValue(
                pressureTexCoordTable2DName,
                var->textureUnitPressureTexCoordTable);
        CHECK_GL_ERROR;
#endif
    }

    else if (var->grid->getLevelType() == AUXILIARY_PRESSURE_3D)
    {
        shader->setUniformValue(structName + ".levelType", GLint(2));
        CHECK_GL_ERROR;

        // Bind pressure field.
        var->textureAuxiliaryPressure
           ->bindToTextureUnit(var->textureUnitAuxiliaryPressure);
        shader->setUniformValue(
                auxPressureField3DName,
                var->textureUnitAuxiliaryPressure);
        CHECK_GL_ERROR;
    }

    // Precompute data extent variables and store in uniform struct.
    // =============================================================
    const GLfloat westernBoundary =
            dataNWCrnr.x() - var->grid->getDeltaLon() / 2.0f;
    const GLfloat eastWestExtent =
            dataSECrnr.x() - dataNWCrnr.x() + var->grid->getDeltaLon();
    const GLfloat northernBoundary =
            dataNWCrnr.y() + var->grid->getDeltaLat() / 2.0f;
    const GLfloat northSouthExtent =
            dataNWCrnr.y() - dataSECrnr.y() + var->grid->getDeltaLat();

    const auto nLon = static_cast<GLint>(var->grid->nlons);
    const auto nLat = static_cast<GLint>(var->grid->nlats);
    const auto nLev = static_cast<GLint>(var->grid->nlevs);
    const GLfloat deltaLnP =
            std::abs(dataSECrnr.z() - dataNWCrnr.z()) / ((float)nLev - 1.0f);
    const GLfloat upperBoundary = dataNWCrnr.z() + deltaLnP / 2.0f;
    const GLfloat verticalExtent =
            abs(dataNWCrnr.z() - dataSECrnr.z()) + deltaLnP;

    // Assume that lat/lon spacing is the same.
    shader->setUniformValue(structName + ".deltaLat", var->grid->getDeltaLat());
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".deltaLon", var->grid->getDeltaLon());
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".dataSECrnr", dataSECrnr);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".dataNWCrnr", dataNWCrnr);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".westernBoundary", westernBoundary);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".eastWestExtent", eastWestExtent);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".northernBoundary", northernBoundary);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".northSouthExtent", northSouthExtent);
    CHECK_GL_ERROR;
    shader->setUniformValue(
            structName + ".gridIsCyclicInLongitude",
            var->grid->gridIsCyclicInLongitude());
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".nLon", nLon);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".nLat", nLat);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".nLev", nLev);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".deltaLnP", deltaLnP);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".upperBoundary", upperBoundary);
    CHECK_GL_ERROR;
    shader->setUniformValue(structName + ".verticalExtent", verticalExtent);
    CHECK_GL_ERROR;

    if (var->grid->leveltype == PRESSURE_LEVELS_3D)
    {
        shader->setUniformSubroutineByName(GL_VERTEX_SHADER, {"samplePressureLevel", "pressureLevelGradient"});
    }
    else if (var->grid->leveltype == HYBRID_SIGMA_PRESSURE_3D)
    {
        shader->setUniformSubroutineByName(GL_VERTEX_SHADER, {"sampleHybridLevel", "hybridLevelGradient"});
    }
    else if (var->grid->leveltype == AUXILIARY_PRESSURE_3D)
    {
        shader->setUniformSubroutineByName(GL_VERTEX_SHADER, {"sampleAuxiliaryPressure", "auxiliaryPressureGradient"});
    }
}

} // namespace Met3D
