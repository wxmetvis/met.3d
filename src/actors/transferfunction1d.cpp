/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2023-2024 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany

**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "transferfunction1d.h"

// standard library imports
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QFileDialog>

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "util/mutil.h"
#include "transferfunctioneditor/transferfunctioneditor.h"
#include "mainwindow.h"
#include "gxfw/transferfunctionselector/mtransferfunctionselectiondialog.h"


namespace Met3D
{

using namespace TFEditor;

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTransferFunction1D::MTransferFunction1D(QObject *parent)
    : MTransferFunction(parent),
      editor(nullptr)
{
    // Create reference to system manager
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    // Create and initialise properties.
    // ===============================================

    setActorType(staticActorType());
    setName(getActorType());

    auto updateTfCallback = [=]()
    {
        if (suppressActorUpdates()) return;

        generateTransferTexture();
        generateBarGeometry();
        emitActorChangedSignal();
    };


    // Properties related to labelling the colour bar.
    // ===============================================

    scaleFactorProp = MFloatProperty("Label value scaling", 1.0);
    scaleFactorProp.setConfigKey("label_value_scaling");
    scaleFactorProp.setDecimals(5);
    scaleFactorProp.setStep(0.1);
    scaleFactorProp.registerValueCallback(updateTfCallback);
    labelProps.enabledProp.addSubProperty(scaleFactorProp);

    // Disable label bbox by default.
    labelProps.enableBBoxProp = false;

    // Add callback to properties from parent class.
    minimumValueProp.registerValueCallback(updateTfCallback);
    maximumValueProp.registerValueCallback(updateTfCallback);
    maxNumTicksProp.registerValueCallback(updateTfCallback);
    maxNumLabelsProp.registerValueCallback(updateTfCallback);
    positionProp.registerValueCallback(updateTfCallback);
    tickWidthProp.registerValueCallback(updateTfCallback);
    labelSpacingProp.registerValueCallback(updateTfCallback);
    labelProps.fontSizeProp.registerValueCallback(updateTfCallback);
    labelProps.fontColourProp.registerValueCallback(updateTfCallback);
    labelProps.enableBBoxProp.registerValueCallback(updateTfCallback);
    labelProps.bboxColourProp.registerValueCallback(updateTfCallback);
    descriptionLabelProp.registerValueCallback(updateTfCallback);
    flipDescriptionProp.registerValueCallback(updateTfCallback);

    valueSignificantDigitsProp.registerValueCallback([=]()
    {
        int sigDigits = valueSignificantDigitsProp;
        minimumValueProp.setSignificantDigits(sigDigits);
        maximumValueProp.setSignificantDigits(sigDigits);
        valueStepProp.setSignificantDigits(sigDigits);

        if (suppressActorUpdates()) return;

        // Texture remains unchanged; only geometry needs to be updated.
        generateBarGeometry();
        emitActorChangedSignal();
    });
    valueStepProp.registerValueCallback([=]()
    {
        float step = valueStepProp;
        minimumValueProp.setStep(step);
        maximumValueProp.setStep(step);
    });


    // Properties related to data range.
    // =================================
    rangeGroupProp.removeSubProperty(valueOptionsGroupProp);

    numStepsProp = MIntProperty("Steps", 50);
    numStepsProp.setConfigKey("steps");
    numStepsProp.setMinMax(2, 32768);
    numStepsProp.registerValueCallback(updateTfCallback);
    rangeGroupProp.addSubProperty(numStepsProp);

    rangeGroupProp.addSubProperty(valueOptionsGroupProp);

    // General properties.
    // ===================

    enableAlphaProp = MBoolProperty("Opacity", true);
    enableAlphaProp.setConfigKey("opacity_enabled");
    enableAlphaProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(enableAlphaProp);

    reverseTFRangeProp = MBoolProperty("Reverse range", false);
    reverseTFRangeProp.setConfigKey("reverse_range_enabled");
    reverseTFRangeProp.registerValueCallback(updateTfCallback);
    actorPropertiesSupGroup.addSubProperty(reverseTFRangeProp);

    // Properties related to type of colourmap.
    // ========================================

    QStringList cmapTypes = {"HCL", "Editor", "Predefined"};

    colourmapTypeProp = MEnumProperty("Colourmap type", cmapTypes);
    colourmapTypeProp.setConfigKey("colourmap_type");
    colourmapTypeProp.registerValueCallback([=]()
    {
        auto cmaptype = MColourmapType(colourmapTypeProp.value());

        switch (cmaptype)
        {
        case PREDEFINED:
            predefCMapGroupProp.setHidden(false);
            hclCMapGroupProp.setHidden(true);
            editorClickProp.setHidden(true);

            clampHclColoursProp.setEnabled(false);
            reverseTFRangeProp.setEnabled(true);
            break;
        case HCL:
            predefCMapGroupProp.setHidden(true);
            hclCMapGroupProp.setHidden(false);
            editorClickProp.setHidden(true);

            clampHclColoursProp.setEnabled(true);
            reverseTFRangeProp.setEnabled(true);
            break;
        case EDITOR:
            predefCMapGroupProp.setHidden(true);
            hclCMapGroupProp.setHidden(true);
            editorClickProp.setHidden(false);

            clampHclColoursProp.setEnabled(false);
            reverseTFRangeProp.setEnabled(false);
            break;
        case INVALID:
            break;
        }

        if (suppressActorUpdates()) return;

        generateTransferTexture();
        generateBarGeometry();
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(colourmapTypeProp);

    // HCL ...

    clampHclColoursProp = MBoolProperty("Clamp HCL colours", true);
    clampHclColoursProp.setTooltip("When converting HCL to RGB, clamp converted values to the RGB domain."
                                       "Note that enabling this option can result in visually non-linear colour-maps, "
                                       "but it will avoid black regions in colourmap definitions, that cannot be fully "
                                       "converted to valid RGB values.");
    clampHclColoursProp.setConfigKey("clamp_hcl_colours");
    clampHclColoursProp.registerValueCallback([=]()
    {
        editor->clampHclColour(clampHclColoursProp);

        if (suppressActorUpdates()) return;

        generateTransferTexture();
        generateBarGeometry();
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(clampHclColoursProp);

    hclCMapGroupProp = MProperty("HCL");
    hclCMapGroupProp.setEnabled(true);
    hclCMapGroupProp.setConfigGroup("hcl");
    actorPropertiesSupGroup.addSubProperty(hclCMapGroupProp);

    QStringList hclTypes = QStringList() << "Diverging" << "Qualitative"
                                         << "Sequential single hue"
                                         << "Sequential multiple hue";
    hclTypeProp = MEnumProperty("Type", hclTypes, 3);
    hclTypeProp.setConfigKey("type");
    hclTypeProp.setValue(MHCLType::SEQUENTIAL_MULTIPLE_HUE);
    hclTypeProp.registerValueCallback([=]()
    {
        updateHCLProperties();

        if (suppressActorUpdates()) return;

        generateTransferTexture();
        generateBarGeometry();
        emitActorChangedSignal();
    });
    hclCMapGroupProp.addSubProperty(hclTypeProp);

    hclHue1Prop = MFloatProperty("Hue 1", 0.f);
    hclHue1Prop.setConfigKey("hue_1");
    hclHue1Prop.setMinMax(-360.f, 360.f);
    hclHue1Prop.setDecimals(1);
    hclHue1Prop.setStep(1.);
    hclHue1Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclHue1Prop);

    hclHue2Prop = MFloatProperty("Hue 2", 90.f);
    hclHue2Prop.setConfigKey("hue_2");
    hclHue2Prop.setMinMax(-360.f, 360.f);
    hclHue2Prop.setDecimals(1);
    hclHue2Prop.setStep(1.);
    hclHue2Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclHue2Prop);

    hclChroma1Prop = MFloatProperty("Chroma 1", 80.f);
    hclChroma1Prop.setConfigKey("chroma_1");
    hclChroma1Prop.setMinMax(0.f, 100.f);
    hclChroma1Prop.setDecimals(1);
    hclChroma1Prop.setStep(1.);
    hclChroma1Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclChroma1Prop);

    hclChroma2Prop = MFloatProperty("Chroma 2", 30.f);
    hclChroma2Prop.setConfigKey("chroma_2");
    hclChroma2Prop.setMinMax(0.f, 100.f);
    hclChroma2Prop.setDecimals(1);
    hclChroma2Prop.setStep(1.);
    hclChroma2Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclChroma2Prop);

    hclLuminance1Prop = MFloatProperty("Luminance 1", 30.f);
    hclLuminance1Prop.setConfigKey("luminance_1");
    hclLuminance1Prop.setMinMax(0.f, 100.f);
    hclLuminance1Prop.setDecimals(1);
    hclLuminance1Prop.setStep(1.);
    hclLuminance1Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclLuminance1Prop);

    hclLuminance2Prop = MFloatProperty("Luminance 2", 90.f);
    hclLuminance2Prop.setConfigKey("luminance_2");
    hclLuminance2Prop.setMinMax(0.f, 100.f);
    hclLuminance2Prop.setDecimals(1);
    hclLuminance2Prop.setStep(1.);
    hclLuminance2Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclLuminance2Prop);

    hclPower1Prop = MFloatProperty("Power 1/C", 0.2f);
    hclPower1Prop.setConfigKey("power_1_c");
    hclPower1Prop.setMinMax(0.f, 100.f);
    hclPower1Prop.setDecimals(2);
    hclPower1Prop.setStep(0.1f);
    hclPower1Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclPower1Prop);

    hclPower2Prop = MFloatProperty("Power 2/L", 2.f);
    hclPower2Prop.setConfigKey("power_2_l");
    hclPower2Prop.setMinMax(0.f, 100.f);
    hclPower2Prop.setDecimals(2);
    hclPower2Prop.setStep(0.1f);
    hclPower2Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclPower2Prop);

    hclAlpha1Prop = MFloatProperty("Alpha 1", 1.0f);
    hclAlpha1Prop.setConfigKey("alpha_1");
    hclAlpha1Prop.setMinMax(0.f, 1.f);
    hclAlpha1Prop.setDecimals(3);
    hclAlpha1Prop.setStep(0.01f);
    hclAlpha1Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclAlpha1Prop);

    hclAlpha2Prop = MFloatProperty("Alpha 2", 1.0f);
    hclAlpha2Prop.setConfigKey("alpha_2");
    hclAlpha2Prop.setMinMax(0.f, 1.f);
    hclAlpha2Prop.setDecimals(3);
    hclAlpha2Prop.setStep(0.01f);
    hclAlpha2Prop.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclAlpha2Prop);

    hclPowerAlphaProp = MFloatProperty("Power alpha", 1.0f);
    hclPowerAlphaProp.setConfigKey("power_alpha");
    hclPowerAlphaProp.setMinMax(0.f, 100.f);
    hclPowerAlphaProp.setDecimals(3);
    hclPowerAlphaProp.setStep(0.01f);
    hclPowerAlphaProp.registerValueCallback(updateTfCallback);
    hclCMapGroupProp.addSubProperty(hclPowerAlphaProp);

    updateHCLProperties();


    // Editor
    editorClickProp = MButtonProperty("Editor", "Open");
    editorClickProp.setHidden(true);
    editorClickProp.registerValueCallback([=]()
    {
        editor->show();
    });
    actorPropertiesSupGroup.addSubProperty(editorClickProp);

    // Predefined ...

    predefCMapGroupProp = MProperty("Predefined");
    predefCMapGroupProp.setConfigGroup("predefined");
    predefCMapGroupProp.setHidden(true);
    actorPropertiesSupGroup.addSubProperty(predefCMapGroupProp);

    colourmapPool = sysMC->getColourmapPool();
    QStringList availableColourmaps = colourmapPool->availableColourmaps();

    if (availableColourmaps.isEmpty())
    {
        availableColourmaps << QString();
    }

    predefColourmapSelectionProp = MArrayProperty("Colourmap");
    predefColourmapSelectionProp.setShowPropertyLabels(false);
    predefCMapGroupProp.addSubProperty(predefColourmapSelectionProp);

    predefColourmapProp = MStringProperty("Colourmap", availableColourmaps[0]);
    predefColourmapProp.setConfigKey("colourmap");
    predefColourmapProp.setEditable(false);
    predefColourmapProp.registerValueCallback(updateTfCallback);
    predefColourmapSelectionProp.append(&predefColourmapProp);

    predefSelectionProp = MButtonProperty("Select colourmap", "...");
    predefSelectionProp.registerValueCallback([=]()
    {
        int numSteps = numStepsProp;
        MTransferFunctionSelectionDialog dialog(numSteps);

        if (dialog.exec() == QDialog::Accepted)
        {
            QString map = dialog.getSelectedColourmap();

            if (map.isEmpty()) return;

            predefColourmapProp = map;

            if (suppressActorUpdates()) return;

            generateTransferTexture();
            generateBarGeometry();
            emitActorChangedSignal();
        }
    });
    predefColourmapSelectionProp.append(&predefSelectionProp);

    predefLightnessAdjustProp = MIntProperty("Lightness adjustment", 0);
    predefLightnessAdjustProp.setConfigKey("lightness_adjustment");
    predefLightnessAdjustProp.registerValueCallback(updateTfCallback);
    predefCMapGroupProp.addSubProperty(predefLightnessAdjustProp);

    predefSaturationAdjustProp = MIntProperty("Saturation adjustment", 0);
    predefSaturationAdjustProp.setConfigKey("saturation_adjustment");
    predefSaturationAdjustProp.registerValueCallback(updateTfCallback);
    predefCMapGroupProp.addSubProperty(predefSaturationAdjustProp);

    predefCMapAlpha1Prop = MFloatProperty("Alpha 1", 1.0f);
    predefCMapAlpha1Prop.setConfigKey("alpha_1");
    predefCMapAlpha1Prop.setMinMax(0.0f, 1.0f);
    predefCMapAlpha1Prop.setDecimals(3);
    predefCMapAlpha1Prop.setStep(0.01f);
    predefCMapAlpha1Prop.registerValueCallback(updateTfCallback);
    predefCMapGroupProp.addSubProperty(predefCMapAlpha1Prop);

    predefCMapAlpha2Prop = MFloatProperty("Alpha 2", 1.0f);
    predefCMapAlpha2Prop.setConfigKey("alpha_2");
    predefCMapAlpha2Prop.setMinMax(0.0f, 1.0f);
    predefCMapAlpha2Prop.setDecimals(3);
    predefCMapAlpha2Prop.setStep(0.01f);
    predefCMapAlpha2Prop.registerValueCallback(updateTfCallback);
    predefCMapGroupProp.addSubProperty(predefCMapAlpha2Prop);

    predefCMapPowerAlphaProp = MFloatProperty("Power alpha", 1.0f);
    predefCMapPowerAlphaProp.setConfigKey("power_alpha");
    predefCMapPowerAlphaProp.setMinMax(0.0f, 100.0f);
    predefCMapPowerAlphaProp.setDecimals(3);
    predefCMapPowerAlphaProp.setStep(0.01f);
    predefCMapPowerAlphaProp.registerValueCallback(updateTfCallback);
    predefCMapGroupProp.addSubProperty(predefCMapPowerAlphaProp);

    // Assign main window as parent so editor gets closed automatically if the
    // user closes the main window.
    editor = new MTransferFunctionEditor(sysMC->getMainWindow());
    editor->resize(700, 200);
    editor->clampHclColour(clampHclColoursProp);

    connect(editor, SIGNAL(transferFunctionChanged()),
            this, SLOT(onEditorTransferFunctionChanged()));
}


MTransferFunction1D::~MTransferFunction1D()
{
    delete editor;

    editor = nullptr;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


#define SHADER_VERTEX_ATTRIBUTE  0
#define SHADER_TEXTURE_ATTRIBUTE 1

void MTransferFunction1D::selectPredefinedColourmap(
        const QString& name, bool reversed, int saturation, int lightness, float alpha1, float alpha2, float powerAlpha)
{
    if (colourmapPool->hasColourmap(name))
    {
        enableActorUpdates(false);

       colourmapTypeProp.setValue(int(PREDEFINED));
        predefColourmapProp = name;
        reverseTFRangeProp = reversed;
        predefSaturationAdjustProp = saturation;
        predefLightnessAdjustProp = lightness;
        predefCMapAlpha1Prop = alpha1;
        predefCMapAlpha2Prop = alpha2;
        predefCMapPowerAlphaProp = powerAlpha;

        predefCMapGroupProp.setEnabled(true);
        hclCMapGroupProp.setEnabled(false);
        editorClickProp.setEnabled(false);

        enableActorUpdates(true);

        if (isInitialized())
        {
            generateTransferTexture();
            generateBarGeometry();
            emitActorChangedSignal();
        }
    }
    else
    {
        QStringList colourmaps = colourmapPool->availableColourmaps();

        if (colourmaps.length() <= 0)
        {
            LOG4CPLUS_WARN(mlog, "No predefined colourmaps loaded.");
            return;
        }

        QString alternative = colourmaps[0];

        LOG4CPLUS_WARN(mlog, "Predefined colourmap " << name << " does not exist. "
                             "Loading " << alternative << " instead.");

        enableActorUpdates(false);

        colourmapTypeProp.setValue(int(PREDEFINED));
        predefColourmapProp = alternative;
        reverseTFRangeProp = reversed;
        predefSaturationAdjustProp = saturation;
        predefLightnessAdjustProp = lightness;
        predefCMapAlpha1Prop = alpha1;
        predefCMapAlpha2Prop = alpha2;
        predefCMapPowerAlphaProp = powerAlpha;

        predefCMapGroupProp.setEnabled(true);
        hclCMapGroupProp.setEnabled(false);
        editorClickProp.setEnabled(false);

        enableActorUpdates(true);

        if (isInitialized())
        {
            generateTransferTexture();
            generateBarGeometry();
            emitActorChangedSignal();
        }
    }
}


void MTransferFunction1D::selectHCLColourmap(
        MTransferFunction1D::MHCLType type,
        float hue1, float hue2, float chroma1, float chroma2,
        float luminance1, float luminance2, float power1, float power2,
        float alpha1, float alpha2, float poweralpha, bool reversed)
{
    enableActorUpdates(false);

    colourmapTypeProp.setValue(int(HCL));
    hclTypeProp.setValue(int(type));
    hclHue1Prop = hue1;
    hclHue2Prop = hue2;
    hclChroma1Prop = chroma1;
    hclChroma2Prop = chroma2;
    hclLuminance1Prop = luminance1;
    hclLuminance2Prop = luminance2;
    hclPower1Prop = power1;
    hclPower2Prop = power2;
    hclAlpha1Prop = alpha1;
    hclAlpha2Prop = alpha2;
    hclPowerAlphaProp = poweralpha;
    reverseTFRangeProp = reversed;

    predefCMapGroupProp.setEnabled(false);
    hclCMapGroupProp.setEnabled(true);
    editorClickProp.setEnabled(false);

    enableActorUpdates(true);

    if (isInitialized())
    {
        generateTransferTexture();
        generateBarGeometry();
        emitActorChangedSignal();
    }
}


void MTransferFunction1D::selectEditor()
{
    enableActorUpdates(false);

    colourmapTypeProp.setValue(int(EDITOR));
    predefCMapGroupProp.setEnabled(false);
    hclCMapGroupProp.setEnabled(false);
    editorClickProp.setEnabled(true);

    enableActorUpdates(true);
}


void MTransferFunction1D::setSteps(int steps)
{
    numStepsProp = steps;
}


void MTransferFunction1D::saveConfiguration(QSettings *settings)
{
    MTransferFunction::saveConfiguration(settings);

    settings->beginGroup(MTransferFunction1D::getSettingsID());

    // Properties related to type of colourmap.
    // ========================================
    auto cmaptype = MColourmapType(colourmapTypeProp.value());

    switch (cmaptype)
    {
    case PREDEFINED:
    {
        int n = numStepsProp;

        settings->beginWriteArray("predefinedColourMapCache", n);

        MColourmap *cmap = colourmapPool->colourmap(predefColourmapProp);
        QColor rgba;
        for (int i = 0; i < n; i++)
        {
            settings->setArrayIndex(i);

            float scalar = float(i) / float(n-1);

            if (cmap != nullptr)
            {
                rgba = cmap->scalarToColour(scalar);
            }

            settings->setValue("rgb", rgba);
        }

        settings->endArray();
        break;
    }
    case HCL:
    {
        break;
    }
    case EDITOR:
    {
        const auto colourNodes = editor->getTransferFunction()->getColourNodes();
        const auto alphaNodes = editor->getTransferFunction()->getAlphaNodes();

        settings->beginWriteArray("colourNode");
        for (int i= 0; i < colourNodes->getNumNodes(); i++)
        {
            settings->setArrayIndex(i);
            settings->setValue("position", colourNodes->xAt(i));

            MColourXYZ64 colour = colourNodes->colourAt(i);
            QByteArray array((char*)&colour, sizeof(MColourXYZ64));
            settings->setValue("colour", array);
        }
        settings->endArray();

        settings->beginWriteArray("alphaNode");
        for (int i= 0; i < alphaNodes->getNumNodes(); i++)
        {
            settings->setArrayIndex(i);
            settings->setValue("position", alphaNodes->xAt(i));
            settings->setValue("alpha", alphaNodes->yAt(i));
        }
        settings->endArray();

        QString type = editor->interpolationCSpaceToString(editor->getCSpaceForCNodeInterpolation());
        settings->setValue("colourSpaceForColourNodeInterpolation", type);

        break;
    }
    case INVALID:
    {
        break;
    }
    }

    settings->endGroup();
}


void MTransferFunction1D::loadConfiguration(QSettings *settings)
{
    MTransferFunction::loadConfiguration(settings);

    auto cmapType = static_cast<MColourmapType>(colourmapTypeProp.value());
    colourmapTypeProp = cmapType;

    QString colourMapTypeString = colourMapTypeToString(cmapType);

    settings->beginGroup(MTransferFunction1D::getSettingsID());

    switch (cmapType)
    {
        // Display error message and continue with colour map type 'hcl'
        // initialisation.
    case INVALID:
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(QString("Error reading configuration file: "
                               "Could not find colour map type '%1'.\n"
                               "Setting colour map type to 'hcl'.")
                               .arg(colourMapTypeString));
        msgBox.exec();

        colourmapTypeProp.setEnumItem(colourMapTypeToString(PREDEFINED));
    }
    case HCL:
    {
        break;
    }
    case EDITOR:
    {
        auto colourNodes = editor->getTransferFunction()->getColourNodes();
        auto alphaNodes = editor->getTransferFunction()->getAlphaNodes();

        colourNodes->clear();
        alphaNodes->clear();

        int numColourNodes = settings->beginReadArray("colourNode");
        for (int i = 0; i < numColourNodes; i++)
        {
            settings->setArrayIndex(i);
            float pos = settings->value("position", 0).toFloat();

            QByteArray array =
                    settings->value("colour", QByteArray()).toByteArray();
            MColourXYZ64 colour;
            if (array.size() == sizeof(MColourXYZ64))
                memcpy(&colour, array.data(), sizeof(MColourXYZ64));
            colour.clampHclColour(clampHclColoursProp);
            colourNodes->push_back(pos, colour);
        }
        settings->endArray();

        int numAlphaPoints = settings->beginReadArray("alphaNode");
        for (int i= 0; i < numAlphaPoints; i++)
        {
            settings->setArrayIndex(i);
            float pos = settings->value("position", 0).toFloat();
            float alpha = settings->value("alpha", 0.0f).toFloat();

            alphaNodes->push_back(pos, alpha);
        }
        settings->endArray();
        QString typeString =
                settings->value("colourSpaceForColourNodeInterpolation",
                                "hcl").toString();
        ColourSpaceForColourNodeInterpolation type =
                editor->stringToInterpolationCSpace(typeString);
        // Display error message and set type to hcl for strings in configuration
        // not defining a type.
        if (type == ColourSpaceForColourNodeInterpolation::INVALID)
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText(QString("Error reading configuration file: "
                                   "Could not find colour space '%1' for interpolation.\n"
                                   "Setting colour space to 'hcl'.")
                                   .arg(typeString));
            msgBox.exec();

            type = editor->stringToInterpolationCSpace(QString("hcl"));
        }
        editor->setCSpaceForCNodeInterpolation(type);

        editor->resetUI();
        selectEditor();
        break;
    }
    case PREDEFINED:
    {
        QStringList availableColourmaps = colourmapPool->availableColourmaps();
        QString defaultColourMap = availableColourmaps.empty() ? QString() : availableColourmaps[0];

        availableColourmaps.sort();

        QString colourmapName = predefColourmapProp;

        // Colourmap doesn't exist in pool, reconstruct from cached colour values as linear segmented colourmap.
        if (!colourmapPool->hasColourmap(colourmapName))
        {
            int n = numStepsProp;

            MColourmapInterpolationNodes nodes;

            settings->beginReadArray("predefinedColourMapCache");

            for (int i = 0; i < n; i++)
            {
                settings->setArrayIndex(i);

                float scalar = float(i) / float(n-1);

                QColor color = settings->value("rgb", QColor()).value<QColor>();

                MColourNode red{scalar, color.redF()};
                MColourNode green{scalar, color.greenF()};
                MColourNode blue{scalar, color.blueF()};
                MColourNode alpha{scalar, color.alphaF()};

                nodes[0].push_back(red);
                nodes[1].push_back(green);
                nodes[2].push_back(blue);
                nodes[3].push_back(alpha);
            }

            settings->endArray();

            colourmapPool->addColourmap(colourmapName, new MLinearSegmentedColourmap(nodes));
        }

        selectPredefinedColourmap(colourmapName,
                                  reverseTFRangeProp,
                                  predefSaturationAdjustProp,
                                  predefLightnessAdjustProp,
                                  predefCMapAlpha1Prop,
                                  predefCMapAlpha2Prop,
                                  predefCMapPowerAlphaProp);

        break;
    }
    }

    settings->endGroup();

    if (isInitialized())
    {
        generateTransferTexture();
        generateBarGeometry();
    }
}


void MTransferFunction1D::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MTransferFunction::loadConfigurationPrior_V_1_14(settings);

    auto cmapType = static_cast<MColourmapType>(colourmapTypeProp.value());
    colourmapTypeProp = cmapType;

    settings->beginGroup(MTransferFunction1D::getSettingsID());

    // Properties related to labelling the colour bar.
    // ===============================================
    scaleFactorProp =
            settings->value("labelValueScaling", 1.).toFloat();

    // Properties related to data range.
    // =================================
    setSteps(settings->value("numSteps", 50).toInt());

    // Properties related to type of colourmap.
    // ========================================
    QString colourMapTypeString =
            settings->value("colourMapType", "hcl").toString();
    cmapType = stringToColourMapType(colourMapTypeString);

    clampHclColoursProp = settings->value("hclColourClamp", true).toBool();

    switch (cmapType)
    {
        // Display error message and continue with colour map type 'hcl'
        // initialisation.
    case INVALID:
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(QString("Error reading configuration file: "
                               "Could not find colour map type '%1'.\n"
                               "Setting colour map type to 'hcl'.")
                               .arg(colourMapTypeString));
        msgBox.exec();

        colourmapTypeProp.setEnumItem(colourMapTypeToString(PREDEFINED));
    }
    case HCL:
    {
        selectHCLColourmap(MHCLType(settings->value(
                                                    "hclType",
                                                    MHCLType::SEQUENTIAL_MULTIPLE_HUE)
                                            .toInt()),
                           settings->value("hue1", 0.f).toFloat(),
                           settings->value("hue2", 90.f).toFloat(),
                           settings->value("chroma1", 80.f).toFloat(),
                           settings->value("chroma2", 30.f).toFloat(),
                           settings->value("luminance1", 30.f).toFloat(),
                           settings->value("luminance2", 90.f).toFloat(),
                           settings->value("power1", .2f).toFloat(),
                           settings->value("power2", 2.f).toFloat(),
                           settings->value("alpha1", 1.f).toFloat(),
                           settings->value("alpha2", 1.f).toFloat(),
                           settings->value("poweralpha", 1.f).toFloat(),
                           settings->value("reverseColourMap", false).toBool());
        break;
    }
    case EDITOR:
    {
        auto colourNodes = editor->getTransferFunction()->getColourNodes();
        auto alphaNodes = editor->getTransferFunction()->getAlphaNodes();

        colourNodes->clear();
        alphaNodes->clear();

        int numColourNodes = settings->beginReadArray("colourNode");
        for (int i = 0; i < numColourNodes; i++)
        {
            settings->setArrayIndex(i);
            float pos = settings->value("position", 0).toFloat();

            QByteArray array =
                    settings->value("colour", QByteArray()).toByteArray();
            MColourXYZ64 colour;
            if (array.size() == sizeof(MColourXYZ64))
                memcpy(&colour, array.data(), sizeof(MColourXYZ64));
            colour.clampHclColour(clampHclColoursProp);
            colourNodes->push_back(pos, colour);
        }
        settings->endArray();

        int numAlphaPoints = settings->beginReadArray("alphaNode");
        for (int i = 0; i < numAlphaPoints; i++)
        {
            settings->setArrayIndex(i);
            float pos = settings->value("position", 0).toFloat();
            float alpha = settings->value("alpha", 0.0f).toFloat();

            alphaNodes->push_back(pos, alpha);
        }
        settings->endArray();
        QString typeString =
                settings->value("colourSpaceForColourNodeInterpolation",
                                "hcl").toString();
        ColourSpaceForColourNodeInterpolation type =
                editor->stringToInterpolationCSpace(typeString);
        // Display error message and set type to hcl for strings in configuration
        // not defining a type.
        if (type == ColourSpaceForColourNodeInterpolation::INVALID)
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText(QString("Error reading configuration file: "
                                   "Could not find colour space '%1' for interpolation.\n"
                                   "Setting colour space to 'hcl'.")
                                   .arg(typeString));
            msgBox.exec();

            type = editor->stringToInterpolationCSpace(QString("hcl"));
        }
        editor->setCSpaceForCNodeInterpolation(type);

        editor->resetUI();
        selectEditor();
        break;
    }
    case PREDEFINED:
    {
        QStringList availableColourmaps = colourmapPool->availableColourmaps();
        QString defaultColourMap = availableColourmaps.empty() ? QString()
                                                               : availableColourmaps[0];

        availableColourmaps.sort();

        QString colourmapName = predefColourmapProp;

        colourmapName = settings->value("predefinedColourMap", defaultColourMap)
                                .toString();

        // Colourmap doesn't exist in pool, reconstruct from cached colour values as linear segmented colourmap.
        if (!colourmapPool->hasColourmap(colourmapName))
        {
            int n = numStepsProp;

            MColourmapInterpolationNodes nodes;

            settings->beginReadArray("predefinedColourMapCache");

            for (int i = 0; i < n; i++)
            {
                settings->setArrayIndex(i);

                float scalar = float(i) / float(n - 1);

                QColor color = settings->value("rgb", QColor()).value<QColor>();

                MColourNode red{scalar, color.redF()};
                MColourNode green{scalar, color.greenF()};
                MColourNode blue{scalar, color.blueF()};
                MColourNode alpha{scalar, color.alphaF()};

                nodes[0].push_back(red);
                nodes[1].push_back(green);
                nodes[2].push_back(blue);
                nodes[3].push_back(alpha);
            }

            settings->endArray();

            colourmapPool->addColourmap(colourmapName,
                                        new MLinearSegmentedColourmap(nodes));
        }

        selectPredefinedColourmap(colourmapName,
                                  settings->value("reverseColourMap",
                                                  false).toBool(),
                                  settings->value("saturationAdjust", 0)
                                          .toInt(),
                                  settings->value("lightnessAdjust", 0).toInt(),
                                  settings->value("predefinedAlpha1", 1.0f)
                                          .toFloat(),
                                  settings->value("predefinedAlpha2", 1.0f)
                                          .toFloat(),
                                  settings->value("predefinedAlphaPower")
                                          .toFloat());

        break;
    }
    }

    settings->endGroup();

    if (isInitialized())
    {
        generateTransferTexture();
        generateBarGeometry();
    }
}


QColor MTransferFunction1D::getColorValue(const float scalar) const
{
    float t = (scalar - minimumValue) / (maximumValue - minimumValue);
    t = std::min(std::max(t, 0.0f), 1.0f);
    const int numColors = colorValues.size() / 4;

    float index = 0;
    float fract = std::modf(t * (numColors - 1), &index);
    index = std::max(std::min(index, float(numColors - 1)), 0.0f);

    int minIndex = static_cast<int>(index) * 4;
    int maxIndex = std::min(static_cast<int>(index + 1), numColors - 1) * 4;

    auto r = static_cast<unsigned char>((fract * colorValues[maxIndex++]
                                 + (1 - fract) * colorValues[minIndex++]));
    auto g = static_cast<unsigned char>((fract * colorValues[maxIndex++]
                                 + (1 - fract) * colorValues[minIndex++]));
    auto b = static_cast<unsigned char>((fract * colorValues[maxIndex++]
                                 + (1 - fract) * colorValues[minIndex++]));
    auto a = static_cast<unsigned char>((fract * colorValues[maxIndex]
                                 + (1 - fract) * colorValues[minIndex]));

    return QColor(r, g, b, a);
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MTransferFunction1D::generateTransferTexture()
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    // The number of steps into which the range minValue..maxValue of this
    // colourbar is divided. A 1D texture of width "numSteps" is generated.
    // It can be used as lookup table for actors using the colourbar.
    int numSteps = numStepsProp;

    // Generate an RGBA * numSteps float array to accomodate the texture.
    colorValues.resize(4 * numSteps);

    // Which type of colourmap are we using?
    MColourmapType cmaptype = MColourmapType(colourmapTypeProp.value());

    MColourmap *cmap = nullptr;
    bool deleteColourMap = false;
    bool reverse = reverseTFRangeProp;

    int lightnessAdjust = 0;
    int saturationAdjust = 0;

    switch (cmaptype)
    {
    case PREDEFINED:
    {
        // Get the selected colourmap and info on whether it shall be reversed.
        QString colourmapName = predefColourmapProp;
        cmap = colourmapPool->colourmap(colourmapName);
        lightnessAdjust = predefLightnessAdjustProp;
        saturationAdjust = predefSaturationAdjustProp;

        // Fill the texture with the chosen colourmap.
        int n = 0;
        int hslH, hslS, hslL, alpha;

        float alpha1 = predefCMapAlpha1Prop;
        float alpha2 = predefCMapAlpha2Prop;
        float poweralpha = predefCMapPowerAlphaProp;

        for (int i = 0; i < numSteps; i++)
        {
            float  scalar = float(i) / float(numSteps-1);

            QColor rgba;

            if (cmap != nullptr)
            {
                rgba = cmap->scalarToColour(reverse ? 1.-scalar : scalar);
            }

            rgba.getHsl(&hslH, &hslS, &hslL, &alpha);
            rgba.setHsl(hslH,
                        std::max(std::min(hslS + saturationAdjust, 255), 0),
                        std::max(std::min(hslL + lightnessAdjust, 255), 0),
                        alpha);

            float alphaP = alpha2 - pow(reverse ? 1.-scalar : scalar, poweralpha) * (alpha2-alpha1);

            colorValues[n++] = (unsigned char)(rgba.redF() * 255);
            colorValues[n++] = (unsigned char)(rgba.greenF() * 255);
            colorValues[n++] = (unsigned char)(rgba.blueF() * 255);
            colorValues[n++] = (unsigned char)(alphaP * rgba.alphaF() * 255);
        }

        break;
    }
    case HCL:
    {
        float hue1 = hclHue1Prop;
        float hue2 = hclHue2Prop;
        float chroma1 = hclChroma1Prop;
        float chroma2 = hclChroma2Prop;
        float luminance1 = hclLuminance1Prop;
        float luminance2 = hclLuminance2Prop;
        float power1 = hclPower1Prop;
        float power2 = hclPower2Prop;
        float alpha1 = hclAlpha1Prop;
        float alpha2 = hclAlpha2Prop;
        float poweralpha = hclPowerAlphaProp;
        deleteColourMap = true;

        // Get type of HCL map and instantiate corresponding class.
        // The types are the same as on http://hclwizard.org.
        MHCLType hcltype = MHCLType(hclTypeProp.value());
        switch (hcltype)
        {
        case DIVERGING:
            cmap = new MHCLColourmap(hue1, hue2, chroma1, chroma1,
                                     luminance1, luminance2, power1, power1,
                                     alpha1, alpha2, poweralpha,
                                     true, clampHclColoursProp); // enable divergence equations
            break;
        case QUALITATIVE:
            cmap = new MHCLColourmap(hue1, hue2, chroma1, chroma1,
                                     luminance1, luminance1, 1., 1.,
                                     alpha1, alpha2, poweralpha, false, clampHclColoursProp);
            break;
        case SEQENTIAL_SINGLE_HUE:
            cmap = new MHCLColourmap(hue1, hue1, chroma1, chroma2,
                                     luminance1, luminance2, power1, power1,
                                     alpha1, alpha2, poweralpha, false, clampHclColoursProp);
            break;
        case SEQUENTIAL_MULTIPLE_HUE:
            cmap = new MHCLColourmap(hue1, hue2, chroma1, chroma2,
                                     luminance1, luminance2, power1, power2,
                                     alpha1, alpha2, poweralpha, false, clampHclColoursProp);
            break;
        }

        // Fill the texture with the chosen colourmap.
        int n = 0;
        for (int i = 0; i < numSteps; i++)
        {
            float  scalar = float(i) / float(numSteps-1);
            QColor rgba   = cmap->scalarToColour(reverse ? 1.-scalar : scalar);
            colorValues[n++] = (unsigned char)(rgba.redF() * 255);
            colorValues[n++] = (unsigned char)(rgba.greenF() * 255);
            colorValues[n++] = (unsigned char)(rgba.blueF() * 255);
            colorValues[n++] = (unsigned char)(rgba.alphaF() * 255);
        }

        break;
    }
    case EDITOR:
    {
        editor->updateNumSteps(numSteps);
        const auto& tex = *editor->getTransferFunction()->getSampledBuffer();

        for (int i = 0; i < numSteps; i++)
        {
            colorValues[i * 4 + 0] = (unsigned char)qRed(tex[i]);
            colorValues[i * 4 + 1] = (unsigned char)qGreen(tex[i]);
            colorValues[i * 4 + 2] = (unsigned char)qBlue(tex[i]);
            colorValues[i * 4 + 3] = (unsigned char)qAlpha(tex[i]);
        }
        break;
    }
    case INVALID:
            break;
    }

    if (deleteColourMap && (cmap != nullptr)) delete cmap;

    // Upload the texture to GPU memory:
    if (tfTexture == nullptr)
    {
        // No texture exists. Create a new one and register with memory manager.
        QString textureID = QString("transferFunction_#%1").arg(getID());
        tfTexture = new GL::MTexture(textureID, GL_TEXTURE_1D, GL_RGBA8UI,
                                     numSteps);

        if ( !glRM->tryStoreGPUItem(tfTexture) )
        {
            delete tfTexture;
            tfTexture = nullptr;
        }
    }

    if ( tfTexture )
    {
        tfTexture->updateSize(numSteps);

        glRM->makeCurrent();
        tfTexture->bindToLastTextureUnit();

        // Set texture parameters: wrap mode and filtering.
        // NOTE: GL_NEAREST is required here to avoid interpolation between
        // discrete colour levels -- the colour bar should reflect the actual
        // number of colour levels in the texture.
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_1D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        // Upload data array to GPU.
        glTexImage1D(GL_TEXTURE_1D,             // target
                     0,                         // level of detail
                     GL_RGBA,                   // internal format
//TODO (mr, 01Feb2015) -- why does GL_RGBA8UI not work?
//                     GL_RGBA8UI,                // internal format
                     numSteps,                  // width
                     0,                         // border
                     GL_RGBA,                   // format
                     GL_UNSIGNED_BYTE,          // data type of the pixel data
                     colorValues.data()); CHECK_GL_ERROR;
    }
}


void MTransferFunction1D::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    Q_UNUSED(sceneView);
}


void MTransferFunction1D::renderToCurrentContextUiLayer(MSceneViewGLWidget *sceneView)
{
    Q_UNUSED(sceneView);

    tfTexture->bindToTextureUnit(textureUnit);

    // First draw the colourbar itself. glPolygonOffset is used to displace
    // the colourbar's z-value slightly to the back, to that the frame drawn
    // afterwards is rendered correctly.
    colourbarShader->bindProgram("colourbarTF");
    colourbarShader->setUniformValue("transferTexture", textureUnit);
    colourbarShader->setUniformValue("enableAlpha", GLboolean(enableAlphaProp));

    vertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE, 3, false,
                                          4 * sizeof(float),
                                          (const GLvoid*)(0 * sizeof(float)));
    vertexBuffer->attachToVertexAttribute(SHADER_TEXTURE_ATTRIBUTE, 1, false,
                                          4 * sizeof(float),
                                          (const GLvoid*)(3 * sizeof(float)));

    glPolygonOffset(.01f, 1.0f);
    glEnable(GL_POLYGON_OFFSET_FILL);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
    glDrawArrays(GL_TRIANGLE_STRIP, 0, numVertices); CHECK_GL_ERROR;
    glDisable(GL_POLYGON_OFFSET_FILL);

    // Next draw a black frame around the colourbar.
    simpleGeometryShader->bindProgram("Simple"); CHECK_GL_ERROR;
    simpleGeometryShader->setUniformValue("colour", QColor(0, 0, 0)); CHECK_GL_ERROR;
    vertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE, 3, false,
                                          4 * sizeof(float),
                                          (const GLvoid*)(8 * sizeof(float)));

    glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
    sceneView->setLineWidth(1);
    glDrawArrays(GL_LINE_LOOP, 0, numVertices); CHECK_GL_ERROR;

    // Finally draw the tick marks.
    vertexBuffer->attachToVertexAttribute(SHADER_VERTEX_ATTRIBUTE, 3, false,
                                          0,
                                          (const GLvoid*)(24 * sizeof(float)));

    glDrawArrays(GL_LINES, 0, 2 * numTicks); CHECK_GL_ERROR;

    // Unbind VBO.
    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
}


void MTransferFunction1D::generateBarGeometry()
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
    bool horizontal = orientation == HORIZONTAL;

    // Create geometry for a box filled with the colourbar texture and for tick
    // marks, and place labels at the tick marks.

    // Get user-defined upper left corner x, y, z and width, height in clip
    // space.
    QRectF positionRect = positionProp;
    float ulcrnr[3] = { float(positionRect.x()),
                        float(positionRect.y()),
                        -1. };
    float lrcrnr[3] = { float(positionRect.x() + positionRect.width()),
                        float(positionRect.y() - positionRect.height()),
                        -1. };
    float width     = positionRect.width();
    float height    = positionRect.height();

    // This array accomodates the geometry for two filled triangles showing the
    // actual colourbar (GL_TRIANGLE_STRIP). The 3rd, 4th, and the additional
    // 5th and 6th vertices are used to draw a box around the colourbar
    // (GL_LINE_LOOP).
    float coordinates[24] =
    {
        ulcrnr[0]        , ulcrnr[1]         , ulcrnr[2], 1, // ul
        ulcrnr[0]        , ulcrnr[1] - height, ulcrnr[2], 0, // ll
        ulcrnr[0] + width, ulcrnr[1]         , ulcrnr[2], 1, // ur
        ulcrnr[0] + width, ulcrnr[1] - height, ulcrnr[2], 0, // lr
        ulcrnr[0]        , ulcrnr[1] - height, ulcrnr[2], 0, // ll for frame
        ulcrnr[0]        , ulcrnr[1]         , ulcrnr[2], 1  // ul for frame
    };

    // Update UV coordinates for horizontal rendering.
    if (horizontal)
    {
        coordinates[3] = 0;
        coordinates[7] = 0;
        coordinates[11] = 1;
        coordinates[15] = 1;
    }

    // ========================================================================
    // Next, generate the tickmarks. maxNumTicks tickmarks are drawn, but not
    // more than colour steps.
    int numSteps = numStepsProp;
    int maxNumTicks = maxNumTicksProp;
    numTicks = std::min(numSteps + 1, maxNumTicks);

    // This array accomodates the tickmark geometry.
	// float tickmarks[6 * numTicks]; VLAs not supported in msvc
    float* tickmarks = new float[6 * numTicks];

    // Width of the tickmarks in clip space.
    float tickwidth = tickWidthProp;

    int n = 0;

    if (horizontal)
    {
        // Treat numTicks equals 1 as a special case to avoid divison by zero.
        if (numTicks != 1)
        {
            for (uint i = 0; i < numTicks; i++)
            {
                tickmarks[n++] = lrcrnr[0] - i * (width / (numTicks - 1));
                tickmarks[n++] = lrcrnr[1];
                tickmarks[n++] = lrcrnr[2];
                tickmarks[n++] = lrcrnr[0] - i * (width / (numTicks - 1));
                tickmarks[n++] = lrcrnr[1] - tickwidth;
                tickmarks[n++] = lrcrnr[2];
            }
        }
        else
        {
            tickmarks[n++] = lrcrnr[0];
            tickmarks[n++] = lrcrnr[1];
            tickmarks[n++] = lrcrnr[2];
            tickmarks[n++] = lrcrnr[0];
            tickmarks[n++] = lrcrnr[1] - tickwidth;
            tickmarks[n++] = lrcrnr[2];
        }
    }
    else
    {
        // Treat numTicks equals 1 as a special case to avoid divison by zero.
        if (numTicks != 1)
        {
            for (uint i = 0; i < numTicks; i++)
            {
                tickmarks[n++] = ulcrnr[0];
                tickmarks[n++] = ulcrnr[1] - i * (height / (numTicks - 1));
                tickmarks[n++] = ulcrnr[2];
                tickmarks[n++] = ulcrnr[0] - tickwidth;
                tickmarks[n++] = ulcrnr[1] - i * (height / (numTicks - 1));
                tickmarks[n++] = ulcrnr[2];
            }
        }
        else
        {
            tickmarks[n++] = ulcrnr[0];
            tickmarks[n++] = ulcrnr[1];
            tickmarks[n++] = ulcrnr[2];
            tickmarks[n++] = ulcrnr[0] - tickwidth;
            tickmarks[n++] = ulcrnr[1];
            tickmarks[n++] = ulcrnr[2];
        }
    }


    // ========================================================================
    // Now we can upload the two geometry arrays to the GPU. Switch to the
    // shared background context so all views can access the VBO generated
    // here.
    glRM->makeCurrent();

    QString requestKey = QString("vbo_transfer_function_actor_%1").arg(getID());

    GL::MVertexBuffer* vb =
            static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));


    if (vb)
    {
        vertexBuffer = vb;
        GL::MFloatVertexBuffer* buf = dynamic_cast<GL::MFloatVertexBuffer*>(vb);
        // reallocate buffer if size has changed
        buf->reallocate(nullptr, 24 + numTicks * 6);
        buf->update(coordinates, 0, 0, sizeof(coordinates));
        buf->update(tickmarks, 0, sizeof(coordinates),
                    sizeof(float) * 6 * numTicks);

    }
    else
    {
        GL::MFloatVertexBuffer* newVB = nullptr;
        newVB = new GL::MFloatVertexBuffer(requestKey, 24 + numTicks * 6);
        if (glRM->tryStoreGPUItem(newVB))
        {
            newVB->reallocate(nullptr, 24 + numTicks * 6, 0, true);
            newVB->update(coordinates, 0, 0, sizeof(coordinates));
            newVB->update(tickmarks, 0, sizeof(coordinates),
                          sizeof(float) * 6 * numTicks);

        }
        else
        {
            delete newVB;
        }
        vertexBuffer = static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));
    }

    // Required for the glDrawArrays() call in renderToCurrentContext().
    numVertices = 4;

    // ========================================================================
    // Finally, place labels at the tickmarks:

    minimumValue = minimumValueProp;
    maximumValue = maximumValueProp;
    int maxNumLabels = maxNumLabelsProp;

    // Obtain a shortcut to the application's text manager to register the
    // labels generated in the loops below.
    MTextManager* tm = glRM->getTextManager();

    // Remove all text labels of the old geometry.
    while (!labels.isEmpty())
    {
        tm->removeText(labels.takeLast());
    }

    // Draw no labels if either numTicks or maxNumLabels equal 0.
    if (numTicks == 0 || maxNumLabels == 0)
    {
        return;
    }

    // A maximum of maxNumLabels are placed. The approach taken here is to
    // compute a "tick step size" from the number of ticks drawn and the
    // maximum number of labels to be drawn. A label will then be placed
    // every tickStep-th tick. The formula tries to place a label at the
    // lower and upper end of the colourbar, if possible.
    int tickStep = ceil(double(numTicks - 1) / double(maxNumLabels - 1));

    float x = tickmarks[3];
    float y = tickmarks[4];
    float z = tickmarks[5];

    MTextManager::TextAnchor anchor = horizontal ? MTextManager::UPPERCENTRE : MTextManager::MIDDLERIGHT;

    // Register the labels with the text manager.
    // Treat numTicks equals 1 as a special case to avoid divison by zero.
    if (numTicks != 1)
    {
        for (uint i = 0; i < numTicks; i += tickStep)
        {
            x = tickmarks[6*i + 3];
            y = tickmarks[6*i + 4];
            z = tickmarks[6*i + 5];

            if (horizontal)
            {
                y -= labelSpacingProp;
            }
            else
            {
                x -= labelSpacingProp;
            }

            float value = (maximumValue - float(i) / float(numTicks-1)
                           * (maximumValue - minimumValue)) * scaleFactorProp;
            QString labelText = minimumValueProp.getValueAsText(value);
            labels.append(tm->addText(
                                  labelText,
                                  MTextManager::CLIPSPACE,
                                  x,
                                  y,
                                  z,
                                  labelProps.fontSizeProp,
                                  labelProps.fontColourProp,
                                  anchor,
                                  labelProps.enableBBoxProp,
                                  labelProps.bboxColourProp)
                          );
        }
    }
    else
    {
        if (horizontal)
        {
            y -= labelSpacingProp;
        }
        else
        {
            x -= labelSpacingProp;
        }

        float value = maximumValue * scaleFactorProp;
        QString labelText = maximumValueProp.getValueAsText(value);
        labels.append(tm->addText(
                              labelText,
                              MTextManager::CLIPSPACE,
                              x,
                              y,
                              z,
                              labelProps.fontSizeProp,
                              labelProps.fontColourProp,
                              anchor,
                              labelProps.enableBBoxProp,
                              labelProps.bboxColourProp)
                      );
    }

    if (!descriptionLabelProp.value().isEmpty())
    {
        QString labelText = descriptionLabelProp;
        anchor = MTextManager::UPPERCENTRE;
        float x = positionRect.x() + width + labelSpacingProp;
        float y = positionRect.y() - height / 2.0;
        float z = -1;
        float rotation = 90.0f;

        if (horizontal)
        {
            anchor = MTextManager::LOWERCENTRE;

            x = positionRect.x() + width / 2.0;
            y = positionRect.y() + labelSpacingProp;

            rotation = 0.f;
        }
        else
        {
            if (flipDescriptionProp)
            {
                anchor = MTextManager::LOWERCENTRE;
                rotation += 180.0f;
            }
        }

        labels.append(tm->addText(labelText,
                                  MTextManager::CLIPSPACE,
                                  x, y, z,
                                  labelProps.fontSizeProp,
                                  labelProps.fontColourProp,
                                  anchor,
                                  labelProps.enableBBoxProp,
                                  labelProps.bboxColourProp,
                                  0.1,
                                  rotation));
    }

    int significantDigits = valueSignificantDigitsProp;
    int minimumExponent = minimumValueProp;
    int switchNotationExponent = minimumValueProp;

    editor->setRange(minimumValue, maximumValue, scaleFactorProp,
                     maxNumTicks, maxNumLabels, numSteps, significantDigits,
                     minimumExponent, switchNotationExponent);

	delete[] tickmarks;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/


void MTransferFunction1D::updateHCLProperties()
{
    MHCLType hcltype = MHCLType(hclTypeProp.value());

    switch (hcltype)
    {
    case DIVERGING:
        hclHue1Prop.setEnabled(true);
        hclHue2Prop.setEnabled(true);
        hclChroma1Prop.setEnabled(true);
        hclChroma2Prop.setEnabled(false);
        hclLuminance1Prop.setEnabled(true);
        hclLuminance2Prop.setEnabled(true);
        hclPower1Prop.setEnabled(true);
        hclPower2Prop.setEnabled(false);
        hclAlpha1Prop.setEnabled(true);
        hclAlpha2Prop.setEnabled(true);
        hclPowerAlphaProp.setEnabled(true);
        break;
    case QUALITATIVE:
        hclHue1Prop.setEnabled(true);
        hclHue2Prop.setEnabled(true);
        hclChroma1Prop.setEnabled(true);
        hclChroma2Prop.setEnabled(false);
        hclLuminance1Prop.setEnabled(true);
        hclLuminance2Prop.setEnabled(false);
        hclPower1Prop.setEnabled(false);
        hclPower2Prop.setEnabled(false);
        hclAlpha1Prop.setEnabled(true);
        hclAlpha2Prop.setEnabled(true);
        hclPowerAlphaProp.setEnabled(true);
        break;
    case SEQENTIAL_SINGLE_HUE:
        hclHue1Prop.setEnabled(true);
        hclHue2Prop.setEnabled(false);
        hclChroma1Prop.setEnabled(true);
        hclChroma2Prop.setEnabled(true);
        hclLuminance1Prop.setEnabled(true);
        hclLuminance2Prop.setEnabled(true);
        hclPower1Prop.setEnabled(true);
        hclPower2Prop.setEnabled(false);
        hclAlpha1Prop.setEnabled(true);
        hclAlpha2Prop.setEnabled(true);
        hclPowerAlphaProp.setEnabled(true);
        break;
    case SEQUENTIAL_MULTIPLE_HUE:
        hclHue1Prop.setEnabled(true);
        hclHue2Prop.setEnabled(true);
        hclChroma1Prop.setEnabled(true);
        hclChroma2Prop.setEnabled(true);
        hclLuminance1Prop.setEnabled(true);
        hclLuminance2Prop.setEnabled(true);
        hclPower1Prop.setEnabled(true);
        hclPower2Prop.setEnabled(true);
        hclAlpha1Prop.setEnabled(true);
        hclAlpha2Prop.setEnabled(true);
        hclPowerAlphaProp.setEnabled(true);
        break;
    }
}


void MTransferFunction1D::onEditorTransferFunctionChanged()
{
    generateTransferTexture();
    emitActorChangedSignal();
}


QString MTransferFunction1D::colourMapTypeToString(MColourmapType colourMapType)
{
    switch (colourMapType)
    {
    case MColourmapType::PREDEFINED:
        return QString("predefined");
    case MColourmapType::HCL:
        return QString("hcl");
    case MColourmapType::EDITOR:
        return QString("editor");
    default:
        return QString("");
    }
}


MTransferFunction1D::MColourmapType
MTransferFunction1D::stringToColourMapType(const QString& colourMapTypeName)
{
    // NOTE: Colour map type identification was changed in Met.3D version 1.1.
    // For compatibility with version 1.0, the old numeric identifiers are
    // considered here as well.
    if (colourMapTypeName == QString("predefined")
            || colourMapTypeName == QString("0")) // compatibility with Met.3D 1.0
    {
        return MColourmapType::PREDEFINED;
    }
    else if (colourMapTypeName == QString("hcl")
             || colourMapTypeName == QString("1"))
    {
        return MColourmapType::HCL;
    }
    else if (colourMapTypeName == QString("editor")
             || colourMapTypeName == QString("3"))
    {
        return MColourmapType::EDITOR;
    }
    else
    {
        return MColourmapType::INVALID;
    }
}

} // namespace Met3D
