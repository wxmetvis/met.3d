/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2015      Michael Kern [+]
**  Copyright 2015-2017 Bianca Tost [+]
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2024      Christoph Fischer [*]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef LONLATHYBHSECACTOR_H
#define LONLATHYBHSECACTOR_H

// standard library imports
#include <memory> // for use of shared_ptr

// related third party imports
#include <GL/glew.h>

// local application imports
#include "gxfw/nwpmultivaractor.h"
#include "gxfw/nwpactorvariable.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "actors/graticuleactor.h"
#include "data/vectorvisualization/mvectorvisualizationsettings.h"
#include "data/vectorvisualization/streambarbs/horizontalstreambarbsdatasource.h"
#include "gxfw/boundingbox/boundingbox.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mcolorproperty.h"
#include "trajectories/pointsource/pointgeneratorinterface.h"

class MSceneViewGLWidget;

namespace Met3D
{

/**
  @brief MNWPHorizontalSectionActor renders a horizontal cross section from
  multiple model level or pressure level data variables.
  */
class MNWPHorizontalSectionActor
    : public MNWPMultiVarActor,
      public MBoundingBoxInterface,
      public MSynchronizedObject,
      public MPointGeneratorInterface
{
    Q_OBJECT

public:
    MNWPHorizontalSectionActor();
    ~MNWPHorizontalSectionActor();

    static QString staticActorType() { return "Horizontal cross-section"; }

    static QString staticIconFileName() { return "hsec.png"; }

    void reloadShaderEffects();

    /**
      Implements MActor::checkIntersectionWithHandle().

      Checks is the mouse position in clip space @p clipX and @p clipY
      "touches" one of the four corner points of this cross-section. If a
      corner point is matched, its index is returned.
      */
    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                    float clipX, float clipY) override;

    void addPositionLabel(MSceneViewGLWidget *sceneView, int handleID,
                          float clipX, float clipY);

    /**
      Implements MActor::dragEvent().

      Drags the corner point at index @p handleID to the vertical position that
      the mouse cursor points at on a plane perpendicular to the x/y plane,
      updates the vertical slice position and triggers a redraw of the scene.
      */
    void dragEvent(MSceneViewGLWidget *sceneView,
                   int handleID, float clipX, float clipY) override;

    /**
     * Implements MActor::releaseEvent().
     * Used for computations only triggered on mouse release.
     */
    void releaseEvent(MSceneViewGLWidget *sceneView, int handleID) override;

    /**
      Get a pointer to the horizontal section's graticule actor.
     */
    MGraticuleActor* getGraticuleActor() const { return graticuleActor; }

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "NWPHorizontalSectionActor"; }

    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    QList<MVerticalLevelType> supportedLevelTypes() override;

    MNWPActorVariable *createActorVariable(
            const MSelectableDataVariable& dataVariable) override;

    bool isConnectedTo(MActor *actor) override;

    void onBoundingBoxChanged() override;


    const QRectF getHorizontalBBox()
    { return getBBoxConnection()->horizontal2DCoords(); }

    const double getSectionElevation_hPa() const { return slicePosition_hPa_prop; }

    /**
     If a map projection is used, this method computes a horizontal 2D grid
     that contains basis vectors for eastward and northward vector components
     for the projection.
     */
    void updateMapProjectionVectorCorrectionField();

    /** Implement MPointGeneratorInterface interface. */

    QString getPointInterfaceName() const override { return getName(); }

    QStringList getPointGeneratingSourceBaseRequestKeys() const override
    { return {"HSEC_POINT_SOURCE"}; }

    MPoints *
    generatePointsFromString(const QString &requestString) const override;

    QString generateStringForPointGeneration(
        MPointGeneratorInterfaceSettings *uiSrc) const override;

public slots:
    /**
     * Set the pressure at which the section is rendered.
     */
    void setSlicePosition(double pressure_hPa);

    /**
     * Slot to call whenever the connected graticule actor changed. So
     * we can actually trigger a redraw.
     */
    void graticuleChanged();


private slots:
    /**
     * Called whenever a request for StreamBarbs has been completed.
     * @param request The request which has been completed.
     */
    void streamBarbsAvailable(MDataRequest request);

    /**
     * Called whenever the map projection of the graticule connected with this
     * actor changed. Triggers a computation of the vector field to correct
     * directions in the projection (for glyphs and streamlines).
     */
    void graticuleMapProjectionChanged();

signals:
    void slicePositionChanged(double pressure_hPa);

protected:
    void initializeActorResources();

    void onOtherActorCreated(MActor *actor) override;

    void onOtherActorDeleted(MActor *actor) override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    /**
      Implements MNWPActor::dataFieldChangedEvent() to update the target grid
      if the data field has changed.
      */
    void dataFieldChangedEvent();

    /**
      Computes the array indices of the data field covered by the current
      bounding box. Computation is done per-variable.

      @see MNWP2DHorizontalActorVariable::computeRenderRegionParameters()
      */
    void computeRenderRegionParameters();

    /**
      Updates the label that shows the vertical position of the section.
      */
    void updateDescriptionLabel(bool deleteOldLabel=true);

    void onDeleteActorVariable(MNWPActorVariable* var) override;

    void onAddActorVariable(MNWPActorVariable* var) override;

    void onChangeActorVariable(MNWPActorVariable *var) override;

private:
    friend class MVectorVisualizationSettings;
    friend class MStreamBarbsSettings;
    friend class MVectorGlyphsSettings;
    /** Methods to render the horizontal section. */
    void computeVerticalInterpolation(MNWP2DHorizontalActorVariable* var);

    void computeVerticalInterpolationDifference(
                                MNWP2DHorizontalActorVariable* var,
                                MNWP2DHorizontalActorVariable* varDiff);

    void renderFilledContours(MSceneViewGLWidget *sceneView,
                              MNWP2DHorizontalActorVariable* var);

    void renderPseudoColour(MSceneViewGLWidget *sceneView,
                            MNWP2DHorizontalActorVariable* var);

    void renderLineCountours(MSceneViewGLWidget *sceneView,
                             MNWP2DHorizontalActorVariable* var);

    void renderTexturedContours(MSceneViewGLWidget *sceneView,
                             MNWP2DHorizontalActorVariable* var);

    void renderVectorGlyphs(
            MSceneViewGLWidget *sceneView,
            std::shared_ptr<GL::MShaderEffect> glyphsShader);

    /**
     * Render the stream barbs in @p currentStreamBarbs.
     * @param sceneView The scene view to render to.
     */
    void renderStreamBarbs(MSceneViewGLWidget* sceneView) const;

    /**
     * Request stream barbs based on the current state of the synchronization
     * and the current settings.
     * @return True if a valid request could be sent.
     */
    bool asynchronousStreamBarbsRequest() const;

    /**
    * Request stream barbs based on the current state of the synchronization
     * and the current settings, but given the level in @p level_hPa. Used
     * for batch requests of multiple levels.
     * @param level_hPa The level to request in hPa.
     * @return True if a valid request could be sent.
     */
    bool asynchronousStreamBarbsRequest(float level_hPa) const;

    /**
     * Requests a batch of streambarbs for the current time. Requests levels
     * from top to bottom in 10 hPa intervals.
     */
    void asynchronousRequestStreamBarbsBatch() const;

    /**
     * Upload the streambarbs in @p currentStreamBarbs to the GPU.
     */
    void uploadStreamBarbs();

    /**
     Synchronize this actor (time, ensemble) with the synchronization control
     @p sync.
     TODO (cf 2024-01-31) currently realized as in the TrajectoryActor.
     Needs a more thought-out rework of the pipeline at some point.
     */
    void synchronizeWith(MSyncControl *sync, bool updateGUIProperties=true) override;
    bool synchronizationEvent(MSynchronizationType syncType, QVector<QVariant> data) override;

    void renderContourLabels(MSceneViewGLWidget *sceneView,
                             MNWP2DHorizontalActorVariable* var);

    void updateMouseHandlePositions();

    // Allow vector visualization methods to handle resources.
    friend class MStreamBarbsSettings;

    std::shared_ptr<GL::MShaderEffect> glVerticalInterpolationEffect;
    std::shared_ptr<GL::MShaderEffect> glFilledContoursShader;
    std::shared_ptr<GL::MShaderEffect> glTexturedContoursShader;
    std::shared_ptr<GL::MShaderEffect> glPseudoColourShader;
    std::shared_ptr<GL::MShaderEffect> glMarchingSquaresShader;
    std::shared_ptr<GL::MShaderEffect> glVectorGlyphsShader;
    std::shared_ptr<GL::MShaderEffect> gl3DVectorGlyphsShader;
    std::shared_ptr<GL::MShaderEffect> positionSpheresShader;
    std::shared_ptr<GL::MShaderEffect> streambarbsDrawShader;

    MSciDoubleProperty slicePosition_hPa_prop;
    MSciDoubleProperty slicePositionGranularity_hPa_prop;

    MEnumProperty synchronizeSlicePosWithOtherActorProp;
    MNWPHorizontalSectionActor *slicePosSynchronizationActor;

    bool crossSectionGridsNeedUpdate;
    bool updateRenderRegion;

    /** Mouse handles */
    QVector<QVector3D> mouseHandlePoints;
    GL::MVector3DVertexBuffer *vbMouseHandlePoints;
    int selectedMouseHandle;

    MEnumProperty differenceModeProp;

    /** Graticule that is overlaid on the horizontal section. */
    MGraticuleActor *graticuleActor;

    /** Settings for windbarb labels */
    GL::MVertexBuffer* vectorGlyphsVertexBuffer;

    MVectorVisualizationSettings *vectorVisSettings;

    // Streambarbs.
    MHorizontalStreamBarbsDataSource *streambarbsSource;
    MStreamBarbsData *currentStreamBarbs; // data to render

    MStructuredGrid *vectorCorrForProjMapsGrid;
    GL::MTexture *textureCorrForProjMapsGrid;
    int textureUnitCorrForProjMapsGrid;

    // Vertex buffers for streambarbs.
    GL::MVertexBuffer *vboStreambarbsLines;
    GL::MVertexBuffer *vboStreambarbsBarbs;

    /**
     * Texture unit for shadow map binding.
     */
    int textureUnitShadowMap;

    // If the user picks the handle not in its centre, we cannot move the handle
    // by setting the centre point to the mouse position so we need this offset
    // to place the handle relative to the mouse position.
    QVector2D offsetPickPositionToHandleCentre;
};

} // namespace Met3D

#endif // LONLATHYBHSECACTOR_H
