/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MDWDHDF5RADARREADER_H
#define MDWDHDF5RADARREADER_H

// standard library imports

// related third party imports
#include <QtCore>
#include <netcdf>

// local application imports
#include "radar/ncdwdradarfile.h"
#include "radar/radargrid.h"
#include "radar/radarreader.h"

namespace Met3D
{

// Define a hierarchy of dictionaries that provide access to where a
// specific data field is stored.

struct MRadarDatafieldInfo
{
    QString filename; // File in which the variable is stored.
};

// Maps an elevation index to a file in which the variable is stored.
typedef QMap<unsigned int, MRadarDatafieldInfo> MElevationMap;

// Maps time steps to radar elevations.
typedef QMap<QDateTime, MElevationMap> MTimeMap;

struct MRadarVariableInfo
{
    // This struct stores variable-specific information, the hierarchy of
    // maps is continued in the field "timeMap".
    MTimeMap timeMap;
    double lat, lon, height;
    QString variablename; // NetCDF variable name
};

// Maps a radar variable to a structure storing different time steps of that variable.
typedef QMap<QString, MRadarVariableInfo *> MRadarVariableNameMap;


class MDWDHDF5RadarReader : public MRadarReader
{
public:
    explicit MDWDHDF5RadarReader(QString identifier);

    ~MDWDHDF5RadarReader();

    void scanDataRoot() override;

    MRadarGrid *readGrid(const QString &variableName, const QDateTime &time,
                         bool approximateConstAzimuths,
                         bool useUndetectedValue) override;

    QStringList
    availableVariables(MVerticalLevelType levelType) override;

    QList<QDateTime>
    availableInitTimes(MVerticalLevelType levelType,
                       const QString &variableName) override;

    QList<QDateTime>
    availableValidTimes(MVerticalLevelType levelType,
                        const QString &variableName,
                        const QDateTime &initTime) override;

    QList<MVerticalLevelType> availableLevelTypes() override;

    QString variableLongName(MVerticalLevelType levelType,
                                     const QString &variableName) override;

    QString variableStandardName(MVerticalLevelType levelType,
                                         const QString &variableName) override;

    QString variableUnits(MVerticalLevelType levelType,
                                  const QString &variableName) override;

    QSet<unsigned int>
    availableEnsembleMembers(MVerticalLevelType levelType,
                             const QString &variableName) override;

protected:
    /**
     * Determine the name of the file that contains the specified data field.
     */
    QString dataFieldFile(const QString &variableName, const QDateTime &time,
                          const unsigned int &elevationNumber);

    /**
     * Initialize the map of variable names to units.
     */
    void initializeVariableUnits();

    /**
     * Initialize the map of variable names (identifiers) to long names (descriptions).
     */
    void initializeVariableLongNames();

    // Dictionaries of available data. Access needs to be protected
    // by the provided read/write lock.
    MRadarVariableNameMap availableDataFields;
    QReadWriteLock availableItemsLock;

    QMap<QString, QString> unitMap; // maps radar variable name to unit
    QMap<QString, QString> longNameMap; // maps radar variable name to long name
};

} // namespace Met3D

#endif // MDWDHDF5RADARREADER_H
