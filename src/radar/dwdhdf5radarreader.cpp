/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "dwdhdf5radarreader.h"

// standard library imports
#include <iostream>
#include <memory>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <netcdf>

// local application imports
#include "ncdwdradarfile.h"
#include "data/nccfvar.h"
#include "util/metroutines.h"
#include "util/mexception.h"
#include "util/mstopwatch.h"
#include "util/mutil.h"

using namespace netCDF;
using namespace netCDF::exceptions;

namespace Met3D
{

// Map for in what order the DWD measures the elevations
// See figure:
// https://www.dwd.de/DE/leistungen/radarprodukte/radarprodukte.html
// maps the DWD Scan Index to the Top to Bottom Order (i.e. the Elevation with the DWD Index 2, is the 3rd elevation
// from the bottom)
const unsigned int mapBothwaysBetweenDWDScanIndexAndBottomToTopOrder[10] = {
    5, 4, 3, 2, 1, 0, 6, 7, 8, 9};


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MDWDHDF5RadarReader::MDWDHDF5RadarReader(QString identifier)
    : MRadarReader(identifier)
{
    initializeVariableUnits();
    initializeVariableLongNames();
}

MDWDHDF5RadarReader::~MDWDHDF5RadarReader()
{
    for (MRadarVariableInfo *vinfo : availableDataFields)
    {
        delete vinfo;
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QStringList
MDWDHDF5RadarReader::availableVariables(MVerticalLevelType levelType)
{
    if (levelType != MVerticalLevelType::RADAR_LEVELS_3D)
    {
        QStringList emptyList;
        return emptyList;
    }
    // Reading values with []-operator results in non deterministic crashes
    // when using multi threading and QReadLocker. Thus it is necessary to use
    // QMap's value()-method or QWriteLocker. But since these operations should
    // only be reading operations, using the value()-method is prefered.
    QReadLocker availableItemsReadLocker(&availableItemsLock);

    return availableDataFields.keys();
}


QList<QDateTime>
MDWDHDF5RadarReader::availableInitTimes(MVerticalLevelType levelType,
                                        const QString &variableName)
{
    if (levelType != MVerticalLevelType::RADAR_LEVELS_3D)
    {
        QList<QDateTime> emptyList;
        return emptyList;
    }
    return availableDataFields.value(variableName)->timeMap.keys();
}


QList<QDateTime>
MDWDHDF5RadarReader::availableValidTimes(MVerticalLevelType levelType,
                                         const QString &variableName,
                                         const QDateTime &initTime)
{
    if (levelType != MVerticalLevelType::RADAR_LEVELS_3D)
    {
        QList<QDateTime> emptyList;
        return emptyList;
    }
    return availableDataFields.value(variableName)->timeMap.keys();
}


QString MDWDHDF5RadarReader::variableLongName(MVerticalLevelType levelType,
                                              const QString &variableName)
{
    if (levelType != MVerticalLevelType::RADAR_LEVELS_3D)
    {
        return "";
    }
    return longNameMap[variableName];
}


QString MDWDHDF5RadarReader::variableStandardName(MVerticalLevelType levelType,
                                                  const QString &variableName)
{
    if (levelType != MVerticalLevelType::RADAR_LEVELS_3D)
    {
        return "";
    }
    return variableName;
}


QString MDWDHDF5RadarReader::variableUnits(MVerticalLevelType levelType,
                                           const QString &variableName)
{
    if (levelType != MVerticalLevelType::RADAR_LEVELS_3D)
    {
        return "";
    }
    return unitMap[variableName];
}


QList<MVerticalLevelType> MDWDHDF5RadarReader::availableLevelTypes()
{
    QList<MVerticalLevelType> list;
    list.append(MVerticalLevelType::RADAR_LEVELS_3D);
    return list;
}


QSet<unsigned int>
MDWDHDF5RadarReader::availableEnsembleMembers(MVerticalLevelType levelType,
                                              const QString &variableName)
{
    QSet<unsigned int> set;
    set.insert(0);
    return set;
}


void MDWDHDF5RadarReader::scanDataRoot()
{
    // Lock access to all available data fields.
    QWriteLocker availableItemsWriteLocker(&availableItemsLock);

    LOG4CPLUS_INFO(mlog, "Scanning directory "
                             << dataRoot.absolutePath().toStdString() << " "
                             << "for files with HDF5 radar data.");
    LOG4CPLUS_INFO(mlog, "Using file filter: " << dirFileFilters.toStdString());

    LOG4CPLUS_INFO(mlog, "Available data files:");

    // Get a list of all files in the directory that match the wildcard name
    // filter given in "dirFileFilters".
    QStringList availableFiles;
    getAvailableFilesFromFilters(availableFiles);

    // Create and initialise progress bar.
    auto progressBar = MProgressBar::getInstance();
    auto progressHandles = progressBar->addTasks(availableFiles.size(), "Reading HDF5 radar file...");

    // For each file, open the file and extract information about the contained
    // variables, measurement times and radar station location. Store the file
    // name in a nested dictionary.
    for (const QString &fileName : availableFiles)
    {
        LOG4CPLUS_INFO(mlog, "\t** Parsing file " << fileName.toStdString() << " ..");

        // NetCDF library is not thread-safe (at least the regular C/C++
        // interface is not; hence all NetCDF calls need to be serialized
        // globally in Met.3D! (notes Feb2015).
        QMutexLocker ncAccessMutexLocker(&staticNetCDFAccessMutex);

        // Open the file.
        NcDwdRadarFile *dwdFile;
        try
        {
            dwdFile = new NcDwdRadarFile(dataRoot.filePath(fileName));
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot open file \""
                                      << fileName.toStdString() << "\"..");
            progressBar->taskCompleted(progressHandles.takeFirst());
            continue;
        }

        QString varName = dwdFile->getVariableIdentifier();
        unsigned int elevationIndex =
            mapBothwaysBetweenDWDScanIndexAndBottomToTopOrder
                [dwdFile->getScanIndex()];

        // Create a new MRadarVariableInfo struct and store available
        // variable information in this field.
        MRadarVariableInfo *varInfo;

        if (availableDataFields.contains(varName))
        {
            varInfo = availableDataFields.value(varName);
        }
        else
        {
            varInfo = new MRadarVariableInfo;
            varInfo->variablename = varName;

            // Coordinates of the radar station.
            varInfo->lat = dwdFile->getLat();
            varInfo->lon = dwdFile->getLon();
            varInfo->height = dwdFile->getHeight();
        }

        // Start time of the first elevation in this sweep (shared by all
        // elevations in one sweep).
        QDateTime sweepStartDateTime = dwdFile->getSweepStartDatetime();

        MRadarDatafieldInfo datafieldInfo;
        datafieldInfo.filename = fileName;
        varInfo->timeMap[sweepStartDateTime].insert(elevationIndex,
                                                    datafieldInfo);

        // Insert the new MRadarVariableInfo struct into the variable name map.
        availableDataFields.insert(varInfo->variablename, varInfo);

        delete dwdFile;
        progressBar->taskCompleted(progressHandles.takeFirst());
    } //for (availableFiles)
}


MRadarGrid *MDWDHDF5RadarReader::readGrid(const QString &variableName,
                                          const QDateTime &time,
                                          bool approximateConstAzimuths,
                                          bool useUndetectedValue)
{
#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
    LOG4CPLUS_DEBUG(mlog, "Stopwatch enabled");
#endif

    MRadarGrid *grid = new MRadarGrid(approximateConstAzimuths);

    // Coordinates of the radar station.
    double lat = availableDataFields.value(variableName)->lat;
    double lon = availableDataFields.value(variableName)->lon;
    double height = availableDataFields.value(variableName)->height;
    grid->setRadarStation(lon, lat, height);

    grid->variableName = variableName;

    double scaleFactor;
    double offset;
    bool firstEl = true; // Is the current elevation the first in the iteration.
    float availableScanCount = availableDataFields[variableName]->timeMap[time].keys().length();

    // Store meta data and data of each elevation.

    for (const unsigned int &elevationIndex :
             availableDataFields[variableName]->timeMap[time].keys())
    {
        MRadarElevation *radarElevation = new MRadarElevation;

        // Determine file name of data file that holds the requested field.
        QString filename;
        try
        {
            filename = dataFieldFile(variableName, time, elevationIndex);
        }
        catch (MBadDataFieldRequest &e)
        {
            LOG4CPLUS_ERROR(mlog, "Invalid data field requested. Please check,"
                                  " that the variable is defined for the "
                                  "requested timestep. [Returning a nullptr.]");
            return nullptr;
        }

        LOG4CPLUS_INFO(
            mlog, "Reading HDF5 data for variable <"
                      << "name=" << variableName.toStdString()
                      << "; elevation=" << elevationIndex << "; time step="
                      << time.toString(Qt::ISODate).toStdString()
                      << "> from file <" << filename.toStdString() << "> ...");

        // Try opening the HDF5 file.
        NcDwdRadarFile *dwdFile;
        try
        {
            // NetCDF library is not thread-safe, see climateforecastreader.
            QMutexLocker ncAccessMutexLocker(&staticNetCDFAccessMutex);
            dwdFile = new NcDwdRadarFile(filename);
            ncAccessMutexLocker.unlock();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot open file " << filename.toStdString()
                                                      << " -- " << e.what());
            return nullptr;
        }

        QMutexLocker ncAccessMutexLocker(&staticNetCDFAccessMutex);

        // Check if data is empty.
        if (dwdFile->getDataSet().isNull())
        {
            LOG4CPLUS_ERROR(mlog, "Data in file " << filename.toStdString()
                                                  << " returned null.");
            return nullptr;
        }

        // Read the metadata and data from the HDF5 file.

        try
        {
            // Start and end time of scanning the current elevation.
            radarElevation->starttime = dwdFile->getStartDateTime();
            radarElevation->endtime = dwdFile->getEndDateTime();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot read start or endtime of elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        try
        {
            radarElevation->range = dwdFile->getRange();
            if (radarElevation->range > grid->maxRadarRange)
            {
                grid->maxRadarRange = radarElevation->range;
            }
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot read range of elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        try
        {
            radarElevation->verticalBeamwidth = dwdFile->getVerticalBeamwidth();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot read vertical beamwidth of elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        try
        {
            radarElevation->horizontalBeamwidth =
                dwdFile->getHorizontalBeamwidth();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(
                mlog, "Cannot read horizontal beamwidth of elevation "
                          << elevationIndex << " in file "
                          << filename.toStdString() << " --- " << e.what());
            return nullptr;
        }

        try
        {
            grid->scanCount = dwdFile->getScanCount();
        }
        catch (NcException& e)
        {
            LOG4CPLUS_ERROR(mlog, "ERROR: cannot read scan count of elevation "
                                      << elevationIndex << " in file " << filename.toStdString() << " --- " << e.what());
            return nullptr;
        }
        if (availableScanCount != grid->scanCount)
        {
            LOG4CPLUS_WARN(mlog, "Number of available scans " << availableScanCount
                                     << " differs from scan count " << grid->scanCount
                         << " in file " << filename.toStdString() << ".");
        }

        // The number of ray cells saved in the file may differ from the default,
        // usually because an angle is missing or because an angle had to be
        // re-recorded due to an error.
        uint nraysInFile;
        try
        {
            nraysInFile = dwdFile->getNrays();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot read number of rays of elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        // Read ray angles: Start and end angle of each azimuth angle section.
        double startAngles[nraysInFile], stopAngles[nraysInFile];
        try
        {
            dwdFile->getStartazA(startAngles);
            dwdFile->getStopazA(stopAngles);
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Couldn't read start or stop azimuth "
                                  "angles for elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        ncAccessMutexLocker.unlock();

        // Store azimuth angles and their number. They can either be approximated
        // be constant and equally spaced, or stored exactly as in the file.
        if (approximateConstAzimuths) // Equally spaced angles.
        {
            radarElevation->nrays = standardAzimuthGridsize;
            radarElevation->rayAngles.resize(radarElevation->nrays);

            for (uint i = 0; i < radarElevation->nrays; i++)
            {
                radarElevation->rayAngles[i] =
                    float(i) * 360.0 / float(radarElevation->nrays);
            }
        }
        else // Angles as given in file.
        {
            radarElevation->nrays = nraysInFile;
            radarElevation->rayAngles.resize(radarElevation->nrays);

            for (uint i = 0; i < radarElevation->nrays; i++)
            {
                radarElevation->rayAngles[i] = startAngles[i];
            }
        }

        ncAccessMutexLocker.relock();

        try
        {
            radarElevation->nbins = dwdFile->getNbins();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot read number of bins of elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        // Query linear transformation factors gain (scale factor) and offset.
        // Used to calculate variable value from stored raw value.
        try
        {
            scaleFactor = dwdFile->getGain();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Couldn't read scale factor for elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        try
        {
            offset = dwdFile->getOffset();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Couldn't read offset factor for elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        // Query raw value representing data fields void of data.
        float missingValue;
        try
        {
            missingValue = dwdFile->getNodataValue();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Couldn't read missing value for elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        // Query raw value representing data fields below detection threshold.
        float localUndetectValue;
        try
        {
            localUndetectValue = dwdFile->getUndetectValue();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Couldn't read undetect value for elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        // Store the undetectValue of the first elevation if it is used.
        if (firstEl)
        {
            if (useUndetectedValue)
            {
                grid->undetectValue = float(localUndetectValue * scaleFactor + offset);
            }
            else
            {
                grid->undetectValue = M_MISSING_VALUE;
            }
        }

        // Read bin step size/bin distance.
        try
        {
            radarElevation->binDistance = dwdFile->getRscale();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Couldn't read rscale value for elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        // Read start of the first bin (in meters from the radar site).
        try
        {
            radarElevation->rangeStart = dwdFile->getRstart();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(mlog, "Couldn't read rstart value for elevation "
                                      << elevationIndex << " in file "
                                      << filename.toStdString() << " --- "
                                      << e.what());
            return nullptr;
        }

        // Read elevation angle.
        try
        {
            radarElevation->elevationAngle = dwdFile->getElAngle();
        }
        catch (NcException &e)
        {
            LOG4CPLUS_ERROR(
                mlog, "Couldn't read elevation angle value for elevation "
                          << elevationIndex << " in file "
                          << filename.toStdString() << " --- " << e.what());
            return nullptr;
        }

        // Store the radar variable data.

        double originalData[nraysInFile * radarElevation->nbins];
        dwdFile->getDataSet().getVar(
            originalData); // Would be correct without interpolation.

        ncAccessMutexLocker.unlock();

        radarElevation->data.resize(radarElevation->nrays
                                    * radarElevation->nbins);
        radarElevation->data.fill(M_MISSING_VALUE);

        /* For each data value, an azimuth start and stop value of the
         * measurement is stored in the original file.
         * This interval usually spans 1°, but it can sometimes be less or more.
         * In the grid used by radarElevation->data, the azimuth angles are
         * going to be equally spaced and in ascending order from 0° to 360°.
         * Therefore, for every value in the original file, we have to determine
         * which positions in radarElevation->data get assigned this value:
         * This is every index between azStartDataIdx and azStopDataIdx, which
         * are then looped over to assign the value.*/
        for (uint azStoredIdx = 0; azStoredIdx < nraysInFile; azStoredIdx++)
        {
            // Round start and stop angles towards closest equally spaced angle. (nearest neighbor)
            int azStartDataIdx, azStopDataIdx;
            if (approximateConstAzimuths)
            {
                azStartDataIdx = int(round(startAngles[azStoredIdx]
                                           * radarElevation->nrays / 360));
                azStopDataIdx = int(round(stopAngles[azStoredIdx]
                                          * radarElevation->nrays / 360));
                if (azStopDataIdx < azStartDataIdx)
                {
                    // If 0° line lies between start and stop angle, shift stop
                    // angle by 360° so that azDataIdx loop still works.
                    azStopDataIdx += radarElevation->nrays;
                }
            }
            else
            {
                azStartDataIdx = azStoredIdx;
                azStopDataIdx = azStoredIdx + 1;
            }

            for (int azDataIdx = azStartDataIdx; azDataIdx < azStopDataIdx;
                 azDataIdx++)
            {
                // Stored angle can only be between 0° and 360°, therefore
                // modulo is applied here to revert shifted stop angle.
                int dataIndex =
                    (azDataIdx % radarElevation->nrays) * radarElevation->nbins;

                for (unsigned int binIdx = 0; binIdx < radarElevation->nbins;
                     binIdx++, dataIndex++)
                {
                    // Fills the data fields between the start and stop angle.

                    radarElevation->data[dataIndex] =
                        float(originalData[azStoredIdx * radarElevation->nbins
                                           + binIdx]);

                    // Fields outside azimuth start-stop intervals already are M_MISSING_VALUE.
                    // Use the same value if data was marked with 'missingValue':
                    if (floatIsAlmostEqualRelativeAndAbs(
                            radarElevation->data[dataIndex], missingValue,
                            1.E-6f))
                    {
                        radarElevation->data[dataIndex] = M_MISSING_VALUE;
                    }
                    else if (floatIsAlmostEqualRelativeAndAbs(
                                 radarElevation->data[dataIndex],
                                 localUndetectValue, 1.E-6f))
                    {
                        // If it is used, the 'undetectValue' needs to be the
                        // same across elevations, so it's always set to the
                        // value in the metadata (which can also be the
                        // standard missing value).
                        radarElevation->data[dataIndex] =
                            grid->undetectValue;
                    }
                    // Apply offset and scale to 'normal' values.
                    // (Check last and separate, as value can have changed to
                    // missing value in the last else-if.)
                    else
                    {
                        /* Radar grid stores the data as a float, but for calculation,
                        double is used here to reduce the chance of inaccuracies,
                        as the data and metadata is stored as double in the
                        hdf5 files. */
                        radarElevation->data[dataIndex] =
                            float(double(radarElevation->data[dataIndex])
                                      * scaleFactor
                                  + offset);
                    }
                }
            }
        }

        radarElevation->scanIndex =
            mapBothwaysBetweenDWDScanIndexAndBottomToTopOrder[elevationIndex];
        radarElevation->elevationIndexKey = elevationIndex;
        grid->insertElevation(radarElevation, elevationIndex);

        delete dwdFile;
        firstEl = false;
    }

    grid->standardAzimuthGridsize = standardAzimuthGridsize;

    // Store the index of which elevation has the largest extent.
    grid->largestHorizontalExtentElevationIndex =
        grid->calculateLargestHorizontalExtentElevationIndex();

    // Calculate and store cartesian coordinates for each grid cell.
    for (MRadarElevation *const elevation : grid->elevationMap)
    {
        for (unsigned int j = 0; j < elevation->nrays; j++)
        {
            for (unsigned int k = 0; k < elevation->nbins; k++)
            {
                // TODO (sf, 05Apr2024) Should be at the center of the cell for raycaster.
                QVector3D cartCoords = grid->radToGlobalCartFromIndices(
                    elevation->elevationIndexKey, j, k);
                elevation->cartesianCoordinates.push_back(cartCoords);
            }
        }
    }

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "Reading radar-grid finished in "
                              << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                              << " seconds");
#endif

    // Return data field.
    return grid;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

QString MDWDHDF5RadarReader::dataFieldFile(const QString &variableName,
                                           const QDateTime &time,
                                           const unsigned int &elevationNumber)
{
    QString filename = "";

    QReadLocker availableItemsReadLocker(&availableItemsLock);

    // Get name of file that contains variable name, time and elevation.
    if (availableDataFields.contains(variableName))
    {
        filename = availableDataFields.value(variableName)
                       ->timeMap.value(time)
                       .value(elevationNumber)
                       .filename;
    }

    if (filename.length() == 0)
    {
        QString msg = QString("Cannot find datafield %1/%2/%3")
                          .arg(variableName)
                          .arg(time.toString(Qt::ISODate))
                          .arg(elevationNumber);
        LOG4CPLUS_ERROR(mlog, msg.toStdString());

        throw MBadDataFieldRequest("No such datafield available", __FILE__,
                                   __LINE__);
    }

    return dataRoot.filePath(filename);
}


void MDWDHDF5RadarReader::initializeVariableUnits()
{
    unitMap["TH"] = "dbZ";
    unitMap["TV"] = "dbZ";
    unitMap["DBZH"] = "dbZ";
    unitMap["DBZV"] = "dbZ";

    unitMap["ZDR"] = "db";
    unitMap["UZDR"] = "db";

    unitMap["RHOHV"] = "-";
    unitMap["URHOHV"] = "-";

    unitMap["LDR"] = "db";
    unitMap["ULDR"] = "db";

    unitMap["PHIDP"] = "°";
    unitMap["UPHIDP"] = "°";

    unitMap["PIA"] = "dB";
    unitMap["KDP"] = "°/km";
    unitMap["UKDP"] = "°/km";

    unitMap["SQIH"] = "-";
    unitMap["USQIH"] = "-";
    unitMap["SQIV"] = "-";
    unitMap["USQIV"] = "-";

    unitMap["SNR"] = "dbZ";
    unitMap["SNRHC"] = "db";
    unitMap["SNRHX"] = "db";
    unitMap["SNRVC"] = "db";
    unitMap["SNRVX"] = "db";
    unitMap["USNRHC"] = "db";
    unitMap["USNRVC"] = "db";

    unitMap["CCORH"] = "db";
    unitMap["CCORV"] = "db";
    unitMap["CPA"] = "-";

    unitMap["RATE"] = "mm/h";
    unitMap["URATE"] = "mm/h";
    unitMap["POR"] = "-";

    unitMap["HI"] = "dbZ";
    unitMap["POH"] = "-";
    unitMap["POSH"] = "-";

    unitMap["MESH"] = "mm";
    unitMap["ACRR"] = "mm";
    unitMap["HGHT"] = "metres";
    unitMap["VIL"] = "kg/m²";

    unitMap["VRADH"] = "m/s";
    unitMap["UVRADH"] = "m/s";
    unitMap["VRADV"] = "m/s";
    unitMap["UVRADV"] = "m/s";

    unitMap["VRADDH"] = "m/s";
    unitMap["VRADDV"] = "m/s";
    unitMap["WRADH"] = "m/s";
    unitMap["UWRADH"] = "m/s";

    unitMap["WRADV"] = "m/s";
    unitMap["UWRADV"] = "m/s";

    unitMap["UWND"] = "m/s";
    unitMap["VWND"] = "m/s";

    unitMap["RSHR"] = "m/s km";
    unitMap["ASHR"] = "m/s km";
}


void MDWDHDF5RadarReader::initializeVariableLongNames()
{
    longNameMap["TH"] =
        "Logged horizontally-polarized total (uncorrected) reflectivity factor";
    longNameMap["TV"] =
        "Logged vertically-polarized total (uncorrected) reflectivity factor";
    longNameMap["DBZH"] =
        "Logged horizontally-polarized (corrected) reflectivity factor";
    longNameMap["DBZV"] =
        "Logged vertically-polarized (corrected) reflectivity factor";

    longNameMap["ZDR"] = "Logged differential reflectivity";
    longNameMap["UZDR"] = "Logged differential reflectivity (unfiltered)";

    longNameMap["RHOHV"] = "Correlation coefficient between Z_h and Z_v";
    longNameMap["URHOHV"] = "Correlation coefficient between Z_h and Z_v "
                            "(unfiltered and uncorrected)";

    longNameMap["LDR"] = "Linear depolarization ratio";
    longNameMap["ULDR"] = "Linear depolarization ratio (unfiltered)";

    longNameMap["PHIDP"] = "Differential phase";
    longNameMap["UPHIDP"] = "Differential phase (unfiltered and uncorrected)";

    longNameMap["PIA"] = "Path Integrated Attenuation";
    longNameMap["KDP"] = "Specific differential phase";
    longNameMap["UKDP"] =
        "Specific differential phase (unfiltered and uncorrected)";

    longNameMap["SQIH"] = "Signal quality index - horizontally-polarized";
    longNameMap["USQIH"] =
        "Signal quality index - horizontally-polarized, unfiltered.";
    longNameMap["SQIV"] = "Signal quality index - vertically-polarized.";
    longNameMap["USQIV"] =
        "Signal quality index - vertically-polarized, unfiltered.";

    longNameMap["SNR"] = "Signal-to-noise ratio";
    longNameMap["SNRHC"] = "Signal-to-noise ratio co-polar H";
    longNameMap["SNRHX"] = "Signal-to-noise ratio cross-polar H";
    longNameMap["SNRVC"] = "Signal-to-noise ratio co-polar V";
    longNameMap["SNRVX"] = "Signal to noise ratio cross-polar V";
    longNameMap["USNRHC"] = "Signal-to-noise ratio co-polar H (unfiltered)";
    longNameMap["USNRVC"] = "Signal-to-noise ratio co-polar V (unfiltered)";

    longNameMap["CCORH"] = "Clutter correction - horizontally-polarized";
    longNameMap["CCORV"] = "Clutter correction - vertically-polarized";
    longNameMap["CPA"] = "Clutter phase alignment";

    longNameMap["RATE"] = "Rain rate";
    longNameMap["URATE"] = "Uncorrected rain rate";
    longNameMap["POR"] = "Probability of rain.";

    longNameMap["HI"] = "dbZ";
    longNameMap["POH"] = "Probability of hail.";
    longNameMap["POSH"] = "Probability of severe hail.";

    longNameMap["MESH"] = "Maximum expected severe hail size";
    longNameMap["ACRR"] = "Accumulated precipitation";
    longNameMap["HGHT"] = "Height above mean sea level";
    longNameMap["VIL"] = "Vertical integrated liquid water";

    longNameMap["VRADH"] = "Radial velocity - horizontally-polarized";
    longNameMap["UVRADH"] =
        "Radial velocity - horizontally-polarized (unfiltered and uncorrected)";
    longNameMap["VRADV"] = "Radial velocity - vertically-polarized";
    longNameMap["UVRADV"] =
        "Radial velocity - vertically-polarized (unfiltered and uncorrected)";

    longNameMap["VRADDH"] = "Dealiased horizontally-polarized radial velocity";
    longNameMap["VRADDV"] = "Dealiased vertically-polarized radial velocity";
    longNameMap["WRADH"] =
        "Spectral width of radial velocity - horizontally-polarized";
    longNameMap["UWRADH"] =
        "Spectral width of radial velocity - horizontally-polarized "
        "(unfiltered and uncorrected)";

    longNameMap["WRADV"] =
        "Spectral width of radial velocity - vertically-polarized";
    longNameMap["UWRADV"] = "Spectral width of radial velocity - "
                            "vertically-polarized (unfiltered and uncorrected)";

    longNameMap["UWND"] = "Component of wind in x-direction";
    longNameMap["VWND"] = "Component of wind in y-direction";

    longNameMap["RSHR"] = "Radial shear";
    longNameMap["ASHR"] = "Azimuthal shear";
}

} //namespace Met3D
