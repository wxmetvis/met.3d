/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MRADARREADER_H
#define MRADARREADER_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/abstractdatareader.h"
#include "radar/radardatasource.h"
#include "radar/radargrid.h"

namespace Met3D
{

/**
  @brief Base class to readers that read radar measurement data.
  */
class MRadarReader :
        public MRadarDataSource, public MAbstractDataReader
{
public:
    MRadarReader(QString identifier);

    /**
      Parses a request and returns a  @ref MRadarGrid pointer which needs to be deleted by the caller.
      */
    MRadarGrid* produceData(MDataRequest request) override;

    MTask* createTaskGraph(MDataRequest request) override;

    void setStandardAzimuthGridsize(int standardAzimuthGridsize) {this->standardAzimuthGridsize = standardAzimuthGridsize;}

protected:

    /**
      Reads the requested data field from disk. The returned @ref
      MRadarGrid pointer needs to be deleted by the caller.
      */
    virtual MRadarGrid* readGrid(const QString& variable,
                                      const QDateTime& initTime,
                                      bool interpolateAzimuth, bool useUndetectedValue) = 0;

    const QStringList locallyRequiredKeys() override;

    int standardAzimuthGridsize;
};

} // namespace Met3D

#endif // MRADARREADER_H
