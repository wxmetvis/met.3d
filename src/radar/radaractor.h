/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2024      Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MRADARACTOR_H
#define MRADARACTOR_H

// standard library imports

// related third party imports

// local application imports
#include "gxfw/nwpmultivaractor.h"
#include "gxfw/boundingbox/boundingbox.h"
#include "gxfw/properties/mnwpactorvarproperty.h"
#include "gxfw/properties/mpropertytemplates.h"

namespace Met3D
{

class MRadarActor : public MNWPMultiVarActor
{
    Q_OBJECT // Required for the signals and slots mechanism.

public:
    MRadarActor();
    ~MRadarActor() override;

    static QString staticActorType() { return "Radar actor"; }

    static QString staticIconFileName() { return "radar.png"; }

    void reloadShaderEffects() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "RadarActor"; }

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    QList<MVerticalLevelType> supportedLevelTypes() override;

    MNWPActorVariable *createActorVariable(
            const MSelectableDataVariable& dataSource) override;

protected:
    void initializeActorResources() override;

    void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

    void dataFieldChangedEvent() override;

    void onDeleteActorVariable(MNWPActorVariable* var) override;

    void onAddActorVariable(MNWPActorVariable* var) override;

    void onChangeActorVariable(MNWPActorVariable *var) override;

private:
    // Shaders.

    std::shared_ptr<GL::MShaderEffect> glRadarShader;
    std::shared_ptr<GL::MShaderEffect> glGraticuleShader;

    // Selected variable.
    MNWPActorVarProperty variableProp;


    // Properties.

    // Render mode property.
    enum class RenderMode
    {
        pseudoColor2D = 0,
        pseudoColor3D = 1,
        verticalSection = 2,
        horizontalSection = 3
    };
    RenderMode renderMode;
    MEnumProperty renderModeProp;
    bool showVolumeThickness;

    // Treatment of 'undetect' (no signal) cells.
    MBoolProperty colourUndetectEnabledProp;
    MColorProperty undetectColourProp;

    // Elevation angle related properties.
    MProperty elevationGroupProp;
    MIntProperty minElevationProp;
    MIntProperty maxElevationProp;
    MBoolProperty showOnlyOneElevationProp;
    MIntProperty singleElevationProp;
    unsigned int minEl;
    unsigned int maxEl;

    // Azimuth angle related properties.
    MProperty azimuthGroupProp;
    MFloatProperty minAzimuthProp;
    MFloatProperty maxAzimuthProp;
    MFloatProperty vSectionAzProp;

    // CAPPI/horizontal section properties.
    enum class CappiMode
    {
        Invalid = -1,
        Pressure = 0,
        Height = 1
    };
    CappiMode cappiMode;

    MProperty cappiGroupProp;
    MBoolProperty cappiSingleAltitudeBoolProp;
    bool cappiSingleAltitudeBool;
    MEnumProperty cappiModeProp;
    MPropertyTemplates::VerticalExtentHPa cappiPressureLimitsProp;
    MPropertyTemplates::VerticalExtentM cappiHeightLimitsProp;
    MFloatProperty cappiSinglePressureProp;
    MFloatProperty cappiSingleHeightProp;
    float cappiSinglePressure;
    float cappiMinimumPressure;
    float cappiMaximumPressure;

    // Shadow properties.
    MProperty shadowGroupProp;
    MBoolProperty shadowDataEnabledProp;
    MBoolProperty shadowFullRangeEnabledProp;
    MColorProperty shadowColourProp;
    MColorProperty shadowFullRangeColourProp;
    MFloatProperty shadowElevation_hPa_prop;

    // Grid line properties.
    MBoolProperty graticuleEnabledProp;
    MColorProperty graticuleColourProp;
    MFloatProperty graticuleThicknessProp;

    MIntProperty graticuleAzimuthStepProp;
    MIntProperty graticuleRangeStepProp;

    /**
     * @brief Render volumetric radar data in 2D cells (without vertical beam extent)
     * or 3D cells (condidering vertical beam extent).
     */
    void renderRadarVolume(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Render the shadow of volumetric radar data.
     * @param fullDataRange If it is set true, the shadow should cover the entire grid, otherwise only
     * grid cells with visible data will cast a shadow.
     */
    void renderShadowElevation(MSceneViewGLWidget *sceneView, MNWPActorVariable *var, bool fullDataRange);

    /**
     * @brief Tries to set uniform values in glPseudoColourShader which are used by multiple render functions for colourmapping.
     * @return Returns false if setting uniforms was incomplete, e.g. due to missing transfer function.
     */
    bool setUniformValuesColourmapping(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Set uniform values in glPseudoColourShader which are used by multiple render functions for drawing graticule lines.
     */
    void setUniformValuesGraticule(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Renders a vertical section at the azimuth angle selected in the settings.
     */
    void renderVerticalSection(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Renders a horizontal section at the height selected in the settings.
     */
    void renderHorizontalSection(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Draws a graticule to illustrate radar grid cells. Which lines are drawn depends on the selected
     *  render mode and the settings for step sizes in azimuth and range directions.
     */
    void renderGraticule(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Draws graticule lines along the radar beam direction for different azimuth and elevation angles.
     */
    void renderGraticuleBeams(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Draws horizontal graticule circles perpendicular to the directions of the radar beams, and therefore,
     * separating range bins.
     */
    void renderGraticuleCircles(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * @brief Draws vertical graticule lines perpendicular to the directions of the radar beams, and therefore,
     * separating range bins. Parallel to the vertical edge of grid cells, and therefore only appropriate for
     * render modi considering the vertical extent, such as 3D and vertical sections.
     */
    void renderGraticuleBinLines(MSceneViewGLWidget *sceneView, MNWPActorVariable *var);

    /**
     * Change which altitude properties are enabled depending on if pressure or
     * height mode is used and if a single altitude or an altitude range should
     * be selected.
     */
    void updateAltitudePropertiesEnabledStatus();

    /**
    * Updates the maximum pressure to be above maximum pressure, and height
    * accordingly.
     * */
    void adjustMaximumPressureToMinimum();

    /**
     * Updates the minimum pressure to be below maximum pressure, and height
     * accordingly.
     */
    void adjustMinimumPressureToMaximum();

    /**
     * Updates the horizontal section data to the current single height/pressure.
     */
    void updateHorizontalSection();

};

} // namespace Met3D

#endif // MRADARACTOR_H
