/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef RADARREGRIDDER_H
#define RADARREGRIDDER_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/datarequest.h"
#include "radar/radardatasource.h"
#include "radar/radargrid.h"
#include "data/structuredgrid.h"
#include "data/weatherpredictiondatasource.h"

namespace Met3D
{

/**
  @brief MRadarRegridder regrids an @ref MRadarGrid to an @ref MRegularLonLatGrid.
  */
class MRadarRegridder : public MWeatherPredictionDataSource
{
public:
    MRadarRegridder();

    /**
      Set input (radar data) source for regridding calculation.
      @param s is a pointer to a MRadarDataSource instance.
     */
    void setInputSource(MRadarDataSource *s);

    /**
      Processes a request, performs the corresponding regridding of radar data and returns
      a @ref MStructuredGrid pointer which needs to be deleted by the caller.
     */
    MStructuredGrid *produceData(MDataRequest request) override;

    MTask *createTaskGraph(MDataRequest request) override;

    QList<MVerticalLevelType> availableLevelTypes() override;

    /**
      Returns a @c QStringList containing the names of the regridded variables
      available for level type @p levelType.
     */
    QStringList availableVariables(MVerticalLevelType levelType) override;

    /**
      Returns a set with a single member "0", since radar data isn't part of an
      ensemble forecast.
     */
    QSet<unsigned int>
    availableEnsembleMembers(MVerticalLevelType levelType,
                             const QString &variableName) override;

    QList<QDateTime> availableInitTimes(MVerticalLevelType levelType,
                                        const QString &variableName) override;

    QList<QDateTime> availableValidTimes(MVerticalLevelType levelType,
                                         const QString &variableName,
                                         const QDateTime &initTime) override;

    QString variableLongName(MVerticalLevelType levelType,
                             const QString &variableName) override;

    QString variableStandardName(MVerticalLevelType levelType,
                                 const QString &variableName) override;

    QString variableUnits(MVerticalLevelType levelType,
                          const QString &variableName) override;

protected:
    const QStringList locallyRequiredKeys() override;

    MRadarDataSource *inputSource;

    // Stores the radar grid indices for each regular lon*lat*lev grid.
    // Used if radar grid stays the same between time steps (i.e., if azimuth
    // angles are interpolated), and the requested lon*lat*lev coordinates are
    // unchanged. Didn't use QVector<int> due to bad_alloc problem in resize.
    std::vector<int> nearestNeighbors; // [nlons][nlats][nlevs][3]
};

} // namespace Met3D

#endif // RADARREGRIDDER_H
