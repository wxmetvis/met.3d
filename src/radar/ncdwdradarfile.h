/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef NCDWDRADARFILE_H
#define NCDWDRADARFILE_H

// standard library imports

// related third party imports
#include <QtCore>
#include <netcdf>

// local application imports

// C++ API for netCDF4.
namespace netCDF
{

/**
  @brief NcDwdRadarFile represents a NetCDF file that follows the EUMETNET OPERA
  weather radar information model for implementation with the HDF5 file format.

  NcDwdRadarFile inherits the NcFile class and adds methods that enable access
  to metadata of the OPERA Data Information Model.
  */
class NcDwdRadarFile : public NcFile
{
public:
    /**
     * Constructs a new (read-only) NcDwdRadarFile object for a given path.
     * @param filePath The file path.
     */
    NcDwdRadarFile(const QString &filePath);

    ~NcDwdRadarFile();

    /**
     * @return The file's variable data set.
     */
    NcVar getDataSet();

    /**
     * @brief Returns gain coefficient in quantity_value = offset + gain×raw_value
     * used to convert to physical unit.
     * Default value is 1.0.
     * @return gain = scale factor
     */
    double getGain();

    /**
     * @return Raw value used to denote areas void of data (never radiated).
     * Not to be confused with undetect value.
     */
    float getNodataValue();

    /**
     * @return Raw value used to denote areas below the measurement detection
     * threshold. Not to be confused with nodata value.
     */
    double getUndetectValue() const;

    /**
     * @return Offset coefficient in quantity_value = offset + gain×raw_value
     * used to convert to physical unit.
     * Default value is 1.0.
     */
    double getOffset();

    /**
     * @return Variable identifier (quantity), e.g. DBZH = Logged
     * horizontally-polarized (corrected) reflectivity factor (in dBZ),
     * VRADH = Radial velocity - horizontally-polarized (in m/s).
     */
    QString getVariableIdentifier();

    /**
     * @return Start Year, Month, Day, Hour, Minute, and Second (YYYYMMDDHHmmss)
     * for the data saved in this file.
     */
    QDateTime getStartDateTime();

    /**
     * @return Start Year, Month, Day, Hour, Minute, and Second (YYYYMMDDHHmmss)
     * of all elevations in the sweep this file belongs to.
     */
    QDateTime getSweepStartDatetime();

    /**
     * @return Year, Month, Day, Hour, Minute, and Second (YYYYMMDDHHmmss) for
     * the data saved in this file.
     */
    QDateTime getEndDateTime();

    /**
     * @brief Stores the azimuthal start angles of each gate.
     * @param pointer in which the azimuthal start angles get stored.
     */
    void getStartazA(double *ptr);

    /**
     * @brief Stores the azimuthal stop angles of each gate.
     * @param pointer in which the azimuthal stop angles get stored.
     */
    void getStopazA(double *ptr);

    /**
     * @brief Gets rscale from file.
     * @return Distance between range bins in meters.
     */
    double getRscale();

    /**
     * @brief Gets rstart from file.
     * @return The range (distance to radar antenna) of the start of the first
     * range bin in meters.
     */
    double getRstart();

    /**
     * @brief Gets nbins from file.
     * @return Number of range bins in each ray.
     */
    unsigned int getNbins();

    /**
     * @return Number of azimuth or elevation gates (rays) in the object.
     */
    unsigned int getNrays();

    /**
     * @brief Returns the scan index. Note: starts with 0 instead of 1 as in
     * the DWD data.
     * @return Which scan this is in the temporal sequence (starting with 0) of
     * the total number of scans comprising the volume. Is typically different
     * from the ascending spatial order of elevations.
     */
    unsigned int getScanIndex();

    /**
     * @return Antenna elevation angle above the horizon, in degree.
     */
    double getElAngle();

    /**
     * @return Latitude position of the radar antenna in degree north,
     * normalized to the WGS-84 reference ellipsoid and datum.
     * Fractions of a degree are given in decimal notation.
     */
    double getLat();

    /**
     * @return Longitude position of the radar antenna in degree east,
     * normalized to the WGS-84 reference ellipsoid and datum.
     * Fractions of a degree are given in decimal notation.
     */
    double getLon();

    /**
     * @return Height of the centre of the antenna above mean sea level, in meters.
     */
    double getHeight();

    /**
     * Maxmimum Radar range of the elevation, in meters.
     */
    double getRange();

    /**
     * @return Geometrical vertical half-power (-3 dB) beamwidth, in degrees.
     */
    double getVerticalBeamwidth();

    /**
     * @return Geometrical horizontal half-power (-3 dB) beamwidth, in degrees.
     */
    double getHorizontalBeamwidth();

    /**
     * @return The total number of scans (elevations) comprising the volume.
     */
    double getScanCount();


private:

    /**
     * @return Start Year, Month, and Day (YYYYMMDD) for the data saved in this file.
     */
    std::string getStartDate();

    /**
     * @return Start Hour, Minute, and Second (HHmmss) for the data saved in this file.
     */
    std::string getStartTime();

    /**
     * @return Start Hour, Minute, and Second (HHmmss) f for all elevations in
     * the sweep this file belongs to.
     */
    std::string getSweepStartTime();

    /**
     * @return Start Year, Month, and Day (YYYYMMDD) for all elevations in the
     * sweep this file belongs to.
     */
    std::string getSweepStartDate();

    /**
     * @return End Year, Month, and Day (YYYYMMDD) for the data saved in this file.
     */
    std::string getEndDate();

    /**
     * @return End Hour, Minute, and Second (HHmmss) for the data saved in this file.
     */
    std::string getEndTime();
};

} //namespace netCDF

#endif // NCDWDRADARFILE_H
