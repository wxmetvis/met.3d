/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "ncdwdradarfile.h"

// standard library imports

// related third party imports
#include <QString>
#include <QStringList>

// local application imports
#include "util/mutil.h"

using namespace netCDF;
using namespace netCDF::exceptions;

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

NcDwdRadarFile::NcDwdRadarFile(const QString &filePath)
    : NcFile(filePath.toStdString(), NcFile::read)
{
}


NcDwdRadarFile::~NcDwdRadarFile() {}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

NcVar NcDwdRadarFile::getDataSet()
{
    return getGroup("dataset1").getGroup("data1").getVar("data");
}


double NcDwdRadarFile::getGain()
{
    double gain;
    getGroup("dataset1")
        .getGroup("data1")
        .getGroup("what")
        .getAtt("gain")
        .getValues(&gain);
    return gain;
}


float NcDwdRadarFile::getNodataValue()
{
    float nodata;
    getGroup("dataset1")
        .getGroup("data1")
        .getGroup("what")
        .getAtt("nodata")
        .getValues(&nodata);
    return nodata;
}


double NcDwdRadarFile::getUndetectValue() const
{
    double undetect;
    getGroup("dataset1")
        .getGroup("data1")
        .getGroup("what")
        .getAtt("undetect")
        .getValues(&undetect);
    return undetect;
}


double NcDwdRadarFile::getOffset()
{
    double offset;
    getGroup("dataset1")
        .getGroup("data1")
        .getGroup("what")
        .getAtt("offset")
        .getValues(&offset);
    return offset;
}


QString NcDwdRadarFile::getVariableIdentifier()
{
    QString quantity;
    char buffer[20];
    getGroup("dataset1")
        .getGroup("data1")
        .getGroup("what")
        .getAtt("quantity")
        .getValues(&buffer);
    quantity = QString::fromStdString(std::string(buffer)).trimmed();
    return quantity;
}


QDateTime NcDwdRadarFile::getStartDateTime()
{
    std::string startDateString = getStartDate();
    std::string startTimeString = getStartTime();
    QString time_format = "yyyyMMddHHmmss";
    std::string dateTimeStdString = startDateString + startTimeString;

    QString dateTimeString = QString::fromStdString(dateTimeStdString);
    QDateTime dateTime = QDateTime::fromString(dateTimeString, time_format);
    dateTime.setTimeSpec(Qt::UTC);

    return dateTime;
}


QDateTime NcDwdRadarFile::getEndDateTime()
{
    std::string endDateString = getEndDate();
    std::string endTimeString = getEndTime();
    QString time_format = "yyyyMMddHHmmss";
    std::string dateTimeStdString = endDateString + endTimeString;

    QString dateTimeString = QString::fromStdString(dateTimeStdString);
    QDateTime dateTime = QDateTime::fromString(dateTimeString, time_format);
    dateTime.setTimeSpec(Qt::UTC);

    return dateTime;
}


QDateTime NcDwdRadarFile::getSweepStartDatetime()
{
    std::string startDateString = getSweepStartDate();
    std::string startTimeString = getSweepStartTime();
    QString time_format = "yyyyMMddHHmmss";
    std::string dateTimeStdString = startDateString + startTimeString;

    QString dateTimeString = QString::fromStdString(dateTimeStdString);
    QDateTime dateTime = QDateTime::fromString(dateTimeString, time_format);
    dateTime.setTimeSpec(Qt::UTC);

    return dateTime;
}


void NcDwdRadarFile::getStartazA(double *ptr)
{
    getGroup("dataset1").getGroup("how").getAtt("startazA").getValues(ptr);
}


void NcDwdRadarFile::getStopazA(double *ptr)
{
    getGroup("dataset1").getGroup("how").getAtt("stopazA").getValues(ptr);
}


double NcDwdRadarFile::getRscale()
{
    double rscale;
    getGroup("dataset1").getGroup("where").getAtt("rscale").getValues(&rscale);
    return rscale;
}


double NcDwdRadarFile::getRstart()
{
    double rstart;
    getGroup("dataset1").getGroup("where").getAtt("rstart").getValues(&rstart);
    return rstart;
}


unsigned int NcDwdRadarFile::getNbins()
{
    unsigned int nbins;
    getGroup("dataset1").getGroup("where").getAtt("nbins").getValues(&nbins);
    return nbins;
}


unsigned int NcDwdRadarFile::getNrays()
{
    unsigned int nrays;
    getGroup("dataset1").getGroup("where").getAtt("nrays").getValues(&nrays);
    return nrays;
}


unsigned int NcDwdRadarFile::getScanIndex()
{
    unsigned int tmp;
    getGroup("dataset1").getGroup("how").getAtt("scan_index").getValues(&tmp);
    return tmp - 1;
}


double NcDwdRadarFile::getElAngle()
{
    double elAngle;
    getGroup("dataset1")
        .getGroup("where")
        .getAtt("elangle")
        .getValues(&elAngle);
    return elAngle;
}


double NcDwdRadarFile::getLat()
{
    double lat;
    getGroup("where").getAtt("lat").getValues(&lat);
    return lat;
}


double NcDwdRadarFile::getLon()
{
    double lon;
    getGroup("where").getAtt("lon").getValues(&lon);
    return lon;
}


double NcDwdRadarFile::getHeight()
{
    double height;
    getGroup("where").getAtt("height").getValues(&height);
    return height;
}


double NcDwdRadarFile::getRange()
{
    double range;
    getGroup("dataset1").getGroup("how").getAtt("range").getValues(&range);
    return range;
}


double NcDwdRadarFile::getVerticalBeamwidth()
{
    double beamwidth;
    getGroup("how").getAtt("beamwV").getValues(&beamwidth);
    return beamwidth;
}


double NcDwdRadarFile::getHorizontalBeamwidth()
{
    double beamwidth;
    getGroup("how").getAtt("beamwH").getValues(&beamwidth);
    return beamwidth;
}


double NcDwdRadarFile::getScanCount() {
    double scanCount;
    getGroup("how").getAtt("scan_count").getValues(&scanCount);
    return scanCount;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

std::string NcDwdRadarFile::getStartDate()
{
    std::string startDateString;
    getGroup("dataset1")
        .getGroup("what")
        .getAtt("startdate")
        .getValues(startDateString);
    startDateString.resize(8);
    return startDateString;
}


std::string NcDwdRadarFile::getStartTime()
{
    std::string startTimeString;
    getGroup("dataset1")
        .getGroup("what")
        .getAtt("starttime")
        .getValues(startTimeString);
    startTimeString.resize(6);
    return startTimeString;
}


std::string NcDwdRadarFile::getSweepStartTime()
{
    std::string startTimeString;
    getGroup("what").getAtt("time").getValues(startTimeString);
    startTimeString.resize(6);
    return startTimeString;
}


std::string NcDwdRadarFile::getSweepStartDate()
{
    std::string startDateString;
    getGroup("what").getAtt("date").getValues(startDateString);
    startDateString.resize(8);
    return startDateString;
}


std::string NcDwdRadarFile::getEndDate()
{
    std::string endDateString;
    getGroup("dataset1")
        .getGroup("what")
        .getAtt("enddate")
        .getValues(endDateString);
    endDateString.resize(8);
    return endDateString;
}


std::string NcDwdRadarFile::getEndTime()
{
    std::string endTimeString;
    getGroup("dataset1")
        .getGroup("what")
        .getAtt("endtime")
        .getValues(endTimeString);
    endTimeString.resize(6);
    return endTimeString;
}