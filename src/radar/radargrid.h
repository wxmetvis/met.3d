/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MRADARGRID_H
#define MRADARGRID_H

// standard library imports

// related third party imports
#include <GL/glew.h>
#include <QGLWidget>

// local application imports
#include "data/abstractdataitem.h"
#include "data/abstractgrid.h"
#include "gxfw/gl/shaderstoragebufferobject.h"
#include "gxfw/gl/texture.h"
#include "gxfw/msceneviewglwidget.h"


namespace Met3D
{

/******************************************************************************
***                             CLASSES                                     ***
*******************************************************************************/

struct MRadarElevation {
        unsigned int elevationIndexKey; // Index of the scan, sorted by ascending elevation angle.
        unsigned int nbins; // Number of range bins (grid cells along the ray) in the data.
        unsigned int nrays; // Number of azimuth angles (ray angles) in the data.
        unsigned int scanIndex; // Index of the scan in the temporal sequence.
        float range; // Maximum range (maximum scanned distance from the radar station) in metres.
        float binDistance; // Distance in metre between the centre of range bins, equal to the length of one range bin.
        float rangeStart; // Distance in metre between the radar station and the start of the first range bin.
        float elevationAngle; // Antenna elevation angle above the horizon, in degrees.
        float verticalBeamwidth; // Geometrical vertical half-power (-3 dB) beamwidth.
        float horizontalBeamwidth; // Geometrical horizontal half-power (-3 dB) beamwidth.
        QDateTime starttime; // Start time of recording this elevation.
        QDateTime endtime; // End time of recording this elevation.
        QVector<float> rayAngles; // Start azimuth angles for which data is stored, in the same order as data values.
        QVector<float> data; // Data values.
        QVector<QVector3D> cartesianCoordinates; // Cartesian coordinates of each radar grid cell, in the same order as data values.
};

class MRadarGrid : public MAbstractGrid
{
public:
    explicit MRadarGrid(bool interpolateAzimuth = false);

    /** Destructor - frees memory fields. */
    ~MRadarGrid();

    /** Releases all GPU items from memory - textures and buffers **/
    void releaseGPUItems();

    /** Memory required for the data field in kilobytes. */
    unsigned int getMemorySize_kb();

    /*** Getter/Setter Functions ***/

    /** Adds a new elevation @p elevation at @p id to the grid.
     * @param elevation Pointer to elevation structure.
     * @param id Index of the new elevation in the list of elevations.
     * */
    void insertElevation(MRadarElevation *elevation, unsigned int id);

    /** Returns the @ref MRadarElevation with the index @p elevationIndex.
     * @param elevationIdx Index of the requested elevation.
     * @return The selected elevation structure.
     * */
    const MRadarElevation *const getElevation(unsigned int elevationIdx) const;

    /** Returns the longitude position of the radar antenna, in degrees. */
    float getRadarStationLon() const { return lonRadar; }

    /** Returns the latitude position of the radar antenna, in degrees. */
    float getRadarStationLat() const { return latRadar; }

    /** Returns the height of the centre of the radar antenna in metres above mean sea level. */
    float getRadarHeight() const { return heightRadar; }

    /** Returns a @ref QVector3D containing the longitude [°], latitude [°] and height [m] of the radar antenna. */
    QVector3D getRadarStationLonLatHeight() const
    {
        return QVector3D(lonRadar, latRadar, heightRadar);
    }

    /** Sets the longitude [°], latitude [°] and height [m] of the radar antenna.  */
    void setRadarStation(float lon, float lat, float height);

    /** Returns a @ref QVector3D containing the Cartesian coordinates of the radar antenna. */
    QVector3D getRadarStationCartesianCoordinates() const
    {
        return radarPositionCartesian;
    }

    /**
     * Returns the information if each elevation stores equidistant rounded ray angles (true),
     * or if the original azimuth angles are used, which can have missing or additional angles (false).
    */
    bool areAzimuthAnglesInterpolated() const
    {
        return areAzimuthAnglesSmoothed;
    }

    /** Returns the number of radar elevations stored in the grid.*/
    int getNumberOfElevations() const { return elevationMap.size(); }

    /** Returns the ideal number of radar elevations (scans) in the grid. */
    int getScanCount() const {return scanCount; }

    /** Returns the sorted list of indices for which elevations are stored. */
    QList<unsigned int> getElevationIndexList() const
    {
        return elevationMap.keys();
    }

    /** Returns the antenna elevation angle above the horizon in degrees for one elevation.*/
    float getElevationAngle(unsigned int elIdx) const
    {
        return elevationMap[elIdx]->elevationAngle;
    }

    /** Returns the radar’s half-power vertical beamwidth for elevation @p elIdx.
     * @param elIdx Index of the requested elevation.
     * */
    float getVerticalBeamwidth(unsigned int elIdx) const
    {
        return elevationMap[elIdx]->verticalBeamwidth;
    }

    /** Returns the radar’s half-power horizontal beamwidth for elevation @p elIdx.
     * @param elIdx Index of the requested elevation.
     * */
    float getHorizontalBeamwidth(unsigned int elIdx) const
    {
        return elevationMap[elIdx]->horizontalBeamwidth;
    }

    /** Deletes the elevation of a given index. */
    void deleteElevation(uint elIdx) { delete elevationMap[elIdx]; }

    float getMaxRadarRange() const { return maxRadarRange; }

    /** Calculates the index of the elevation with the largest horizontal extent.  */
    uint calculateLargestHorizontalExtentElevationIndex();

    /** Returns the index of the elevation with the largest horizontal extent.  */
    uint getLargestHorizontalExtentElevationIndex() {return largestHorizontalExtentElevationIndex; };

    /** Returns the antenna elevation angle above the horizon in degrees for one elevation. */
    float getUndetectValue() const { return undetectValue; }

    /** Returns the number of ray angles in the dataset when smoothing is applied. */
    int getNrays() const
    {
        return standardAzimuthGridsize;
    }

    /** Returns the number of ray angles of the elevation @p elIdx.
     * @param elevationIdx Index of the requested elevation.
     * @return The number of ray angles.*/
    int getNrays(unsigned int elIdx) const;

    /** Returns the number of range bins of the elevation @p elIdx.
     * @param elevationIdx Index of the requested elevation.
     * @return The number of range bins.*/
    int getNbins(unsigned int elIdx) const;

    /** Returns the number data values in the elevation @p elIdx.
     * @param elevationIdx Index of the requested elevation.
     * @return The number of data points.*/
    int getNumValues(uint elIdx) const;

    /** Returns the number of range bins in the current horizontal section. */
    int getNumberHorizontalBins() const { return nHorizontalBins; }

    /** Returns the distance between range bins or length of one range bin of
     * the elevation @p elIdx.
     * @param elevationIdx Index of the requested elevation.
     * @return The range bin distance [m].
     * */
    float getBinDistance(unsigned int elIdx) const;

    /** Returns the distance between range bins or length of one range bin of
     * the elevation @p elIdx.
     * @param elevationIdx Index of the requested elevation.
     * @return The range bin start [m].
     * */
    float getRangeStart(unsigned int elIdx) const;

    /**
     * Get the data value at position @p elIdx, @p rayIdx, @p binIdx;
     * @param elIdx Index in elevation (cone) direction.
     * @param rayIdx Index in azimuth angle (rotation) direction.
     * @param binIdx Index in range bin direction (along one ray).
     * @return
     */
    float getValue(int elIdx, int rayIdx, int binIdx) const;

    /** Get the value at position @n in elevation @p elIdx. */
    float getValue(uint elIdx, uint n) const;

    void setValue(unsigned int elIdx, unsigned int rayIdx, unsigned int binIdx,
                  float value);

    /**
      Minimum value across all elevations of the data grid.
     */
    float min() override;

    /**
      Maximum value across all elevations of the data grid.
     */
    float max() override;

    /** Sets the values of all grid points to zero. */
    void setToZero() override;

    /** Sets the values of all grid points to @p val. */
    void setToValue(float val) override;
#
    /** Get the data in elevation @p elIdx. */
    const float* getData(uint elIdx) const;

    /** Returns the geographic coordinate of the most northern point (longitude,
     * latitude, height) in the grid. The z-coordinate is in metres.
     * */
    QVector3D getMostNorthernPointLonLatMetre() const;

    /** Returns the geographic coordinate of the most southern point (longitude,
     * latitude, height) in the grid. The z-coordinate is in metres.
     * */
    QVector3D getMostSouthernPointLonLatMetre() const;

    /** Returns the geographic coordinate of the most northern point (longitude,
     * latitude, height) in the grid. The z-coordinate is in metres.
     * */
    QVector3D getMostEasternPointLonLatMetre() const;

    /** Returns the geographic coordinate of the most southern point (longitude,
     * latitude, height) in the grid. The z-coordinate is in metres.
     * */
    QVector3D getMostWesternPointLonLatMetre() const;

    /** Returns the maximum height in the grid in metre.
     * */
    float getTopHeightMetre() const;

    /** Returns the maximum height in the grid in hPa.
     * */
    float getTopHeight_hPa() const;

    /**
      Returns the handle to a texture containing the cartesian coordinates and variable value
      for each radar grid cell. The handle needs to be released when it is no longer required.
      Used by the raycaster actor.
     */
    GL::MTexture *getTexture(QGLWidget *currentGLContext = nullptr,
                             bool nullTexture = false);

    /** Release a texture acquired with getTexture(). */
    void releaseTexture() override;

    /**
      Returns the handle to a texture containing the azimuth angles. The handle
      needs to be released with if not required any longer.
      Used by raycaster actor.
     */
    GL::MTexture *getRayTexture(QGLWidget *currentGLContext = nullptr,
                                bool nullTexture = false);

    /**
      Returns the handle to a texture containing the radar variable data of a single elevation.
      The handle needs to be released with if not required any longer. Used by the Radar actor.
     */
    GL::MTexture *
    getRadarElevationDataTexture(uint elIdx,
                                 QGLWidget *currentGLContext = nullptr);

    /** Calculates which radar grid cells are crossed by a horizontal section at @p heightMetre.
     * The geographic coordinates of the centre of the calculated cells are stored in @p lonlats,
     * their data values in @p data.
     * @param heightMetre Height [m] of the horizontal section.
     * @param lonlats Vector to store grid point coordinates in.
     * @param data Vector to store grid point data values in.
     **/
    void calculateHorizontalRadarSection(float heightMetre,
                                         QVector<float> &lonlats,
                                         QVector<float> &data);

    /**
     * Returns the handle to a texture containing the geographic coordinates of a horizontal section, passed in @p lonlats.
     * The handle needs to be released with if not required any longer. Used by raycaster actor.
     *
     * @param lonlats Vector containing the grid point coordinates of the section.
     */
    GL::MTexture *getHSecLonLatTexture(QVector<float> lonlats,
                                        QGLWidget *currentGLContext = nullptr);

    /**
     * Returns the handle to a texture containing the data of a horizontal section, passed in @p data.
     * The handleneeds to be released with if not required any longer. Used by raycaster actor.
     *
     * @param data Vector containing the grid point values of the section.
     */
    GL::MTexture *getHSecDataTexture(QVector<float> data,
                                      QGLWidget *currentGLContext = nullptr);
    /**
     * Returns a buffer object containing radar meta data for all elevations.
     */
    GL::MShaderStorageBufferObject *getMetadataBuffer();

    /** Transformation functions */

    /**
     * Transforms radar coordinates (elevation angle [°], azimuth angle[°], range [metre from antenna] to Cartesian coordinates.
     */
    QVector3D radToGlobalCartFromValues(float elevationAngle,
                                        float azimuthAngle, float range);

    /**
     * Transforms radar coordinates (elevation angle [°], azimuth angle[°], range [metre from antenna]) to
     * geographic coordinates (longitude[°], latitude[°], height[metres]).
     */
    QVector3D radToLonLatMetreFromValues(float elevationAngle,
                                         float azimuthAngle, float range) const;

    /**
     * Transforms radar coordinates (elevation angle [°], azimuth angle[°], range [metre from antenna]) to
     * geographic coordinates (longitude[°], latitude[°], pressure[Pa]).
     */
    QVector3D radToLonLatPressure_Pa_FromValues(float elevationAngle,
                                                float azimuthAngle,
                                                float range);

    /**
     * Transforms radar coordinates (elevation angle [°], azimuth angle[°], range [metre from antenna]) to
     * geographic coordinates (longitude[°], latitude[°], pressure[hPa]).
     */
    QVector3D radToLonLatPressure_hPa_FromValues(float elevationAngle,
                                                 float azimuthAngle,
                                                 float range);

    /**
     * Transforms indices of this grid (elevation index, ray index, range bin index) to
     * geographic coordinates (longitude[°], latitude[°], height[metres]).
     */
    QVector3D radToGlobalCartFromIndices(unsigned int elevationIdx,
                                         unsigned int rayIdx,
                                         unsigned int binIdx);

    /**
     * Transforms indices of this grid (elevation index, ray index, range bin index) to Cartesian coordinates.
     */
    QVector3D radToLonLatMetreFromIndices(unsigned int elevationIdx,
                                          unsigned int rayIdx,
                                          unsigned int binIdx);

    /**
     * Transforms indices of this grid (elevation index, ray index, range bin index) to
     * geographic coordinates (longitude[°], latitude[°], pressure[Pa]).
     */
    QVector3D radToLonLatPressure_Pa_FromIndices(unsigned int elevationIdx,
                                                 unsigned int rayIdx,
                                                 unsigned int binIdx);

    /**
     * Transforms a height in metres above sea level to the range distance from the antenna
     * for a given elevation angle.
     */
    float
    heightToRange(float heightMetre,
                  float elevationAngleDegree);

    /*** Interpolation Functions ***/
    float interpolateValue(float lon, float lat, float p_hPa) override;

    /**
     * Transforms geographic coordinates (lon [°], lat [°], pressure [hPa]) to radar coordinates.
     * @param nearestNeighbors If it isn't a nullpointer, it receives the indices of the nearest radar grid cells.
     * @return data value of the intepolated radar grid cell.
     */
    double inverseTransformationInterpolation(float lon, float lat,
                                              float pressure_Pa,
                                              int *nearestNeighbors = nullptr);

    /*** Math/Calculation Functions ***/

    /** calculate the maximal distance a nearestNeighbor is allowed to be away
     * to still get interpolated. Used in regridder.
     */
    double calculateMaxAllowedDistance(int elevationIdx, int binIdx);

    /*** Functions used in raycaster actor: ***/
    bool gridIsCyclicInLongitude() { return false; }

    float getAbsDistanceLon() const override;
    float getAbsDistanceLat() const override;

    QVector3D getNorthWestTopDataVolumeCorner_lonlatp() override;

    QVector3D getSouthEastBottomDataVolumeCorner_lonlatp() override;

    GL::MTexture* getMinMaxAccelTexture3D(QGLWidget *currentGLContext = nullptr) override;

    void releaseMinMaxAccelTexture3D() override;


protected:
    friend class MRadarDataSource;
    friend class MRadarReader;
    friend class MDWDHDF5RadarReader;

    QMap<unsigned int, MRadarElevation *> elevationMap;
    //radar location coordinates

    float scanCount;
    float lonRadar, latRadar, heightRadar;
    QVector3D radarPositionCartesian;
    float maxRadarRange;
    uint largestHorizontalExtentElevationIndex;
    int standardAzimuthGridsize;

    float undetectValue;
    // Data value if measurement was below detection threshold.

    QDateTime time;
    QString variableName;

    bool areAzimuthAnglesSmoothed;
    // Is constant number of azimuth angles used instead of possibly varying number in original data?

    /** Texture parameters **/
    QString dataTextureID; // Radar data basis (elevation index is added).
    QString metadataBufferID; // Radar metadata for each elevation,
                              // such as number of grid points, elevation angle,
                              // beamwidths, bin distance, etc.
    QString dataRaycasterTextureID; // Full scan radar data texture for raycaster.
    QString raysRaycasterTextureID; // Azimuth angles texture for raycaster.
    QString hSecDataTextureID;  // Horizontal section radar data.
    QString hSecLonLatsTextureID; // Horizontal section grid points in lon/lat/pressure.
    int nHorizontalBins;

private:
};

} //namespace Met3D


#endif // MRADARGRID_H
