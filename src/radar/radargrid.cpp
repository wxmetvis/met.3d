/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "radargrid.h"

// standard library imports
#include <array>
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <netcdf>

// local application imports
#include "util/metroutines.h"
#include "util/mexception.h"

using namespace netCDF;
using namespace netCDF::exceptions;


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/


MRadarGrid::MRadarGrid(bool interpolateAzimuth)
    : MAbstractGrid(RADAR_LEVELS_3D, RADAR_GRID),
      lonRadar(0),
      latRadar(0),
      heightRadar(0),
      maxRadarRange(0),
      undetectValue(0),
      areAzimuthAnglesSmoothed(interpolateAzimuth),
      nHorizontalBins(0)
{
    dataTextureID = getID() + "data";
    metadataBufferID = getID() + "radarMetadataBuffer";
    dataRaycasterTextureID = getID() + "radarDataTexture";
    raysRaycasterTextureID = getID() + "rayTexture";
    hSecDataTextureID = getID() + "CappiData";
    hSecLonLatsTextureID = getID() + "CappiLonLat";

    setTextureParameters();
}


MRadarGrid::~MRadarGrid()
{
    releaseGPUItems();

    // Delete elevations.
    for (const int i : elevationMap.keys())
    {
        delete elevationMap[i];
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


unsigned int MRadarGrid::getMemorySize_kb()
{
    uint grid_size = sizeof(MRadarGrid);
    for (auto el : elevationMap)
    {
        uint size_el = el->rayAngles.size() * sizeof(float)
                       + el->data.size() * sizeof(float)
                       + el->cartesianCoordinates.size() * sizeof(float);
        grid_size += size_el;
    }

    grid_size /= 1024;

    return grid_size;
}


void MRadarGrid::releaseGPUItems()
{
    if (!MGLResourcesManager::hasInstance()) return;

    for (const uint &elIdx: getElevationIndexList())
    {
        QString dataTextureElIdxID = dataTextureID + QString::number(elIdx);
        if (MGLResourcesManager::getInstance())
        {
            MGLResourcesManager::getInstance()->releaseGPUItem(dataTextureElIdxID);
        }
    }
    if (MGLResourcesManager::getInstance())
    {
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(
            dataTextureID);
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(
            metadataBufferID);
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(
            dataRaycasterTextureID);
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(
            raysRaycasterTextureID);
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(
            hSecLonLatsTextureID);
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(
            hSecDataTextureID);
    }
}


void MRadarGrid::insertElevation(MRadarElevation *elevation, unsigned int id)
{
    if (elevationMap.contains(id))
    {
        LOG4CPLUS_WARN(mlog,
                       "Overwriting existing elevation with id " << id << ".");
    }
    elevationMap[id] = elevation;
}


const MRadarElevation *const
MRadarGrid::getElevation(unsigned int elevationIdx) const
{
    if (!elevationMap.contains(elevationIdx))
    {
        LOG4CPLUS_ERROR(
            mlog, "ERROR: requested elevation"
                      << elevationIdx
                      << " is not in list of elevations. Returning nullptr.");
        return nullptr;
    }
    return elevationMap.value(elevationIdx);
}


void MRadarGrid::setRadarStation(float lon, float lat, float height)
{
    lonRadar = lon;
    latRadar = lat;
    heightRadar = height;
    radarPositionCartesian = lonLatMetreToGlobalCart(lon, lat, height);
}


uint MRadarGrid::calculateLargestHorizontalExtentElevationIndex()
{
    uint idxMax = 0;
    double latMaxNorth = getRadarStationLat();

    for (const uint &elIdx : getElevationIndexList())
    {
        // Upper end of the elevation.
        double alpha = getElevationAngle(elIdx) + 0.5*getVerticalBeamwidth(elIdx);
        int nbins = getNbins(elIdx);
        float binDistance = getBinDistance(elIdx);
        // Calculate the position furthest north in the elevation.
        float elevationMaxNorth =
            radToLonLatMetreFromValues(alpha, 0, nbins * binDistance).y();

        if (elevationMaxNorth > latMaxNorth)
        {
            latMaxNorth = elevationMaxNorth;
            idxMax = elIdx;
        }
    }
    return idxMax;
}


int MRadarGrid::getNrays(unsigned int elevationIdx) const
{
    if (!elevationMap.contains(elevationIdx))
    {
        LOG4CPLUS_ERROR(
            mlog, "ERROR: requested elevation"
                      << elevationIdx
                      << " is not in list of elevations. Returning nullptr.");
        return -1;
    }
    return elevationMap.value(elevationIdx)->nrays;
}


int MRadarGrid::getNbins(unsigned int elevationIdx) const
{
    if (!elevationMap.contains(elevationIdx))
    {
        LOG4CPLUS_ERROR(
            mlog, "ERROR: requested elevation"
                      << elevationIdx
                      << " is not in list of elevations. Returning nullptr.");
        return -1;
    }
    return elevationMap.value(elevationIdx)->nbins;
}


int MRadarGrid::getNumValues(uint elevationIdx) const
{
    if (!elevationMap.contains(elevationIdx))
    {
        LOG4CPLUS_ERROR(
            mlog, "ERROR: requested elevation"
                      << elevationIdx
                      << " is not in list of elevations. Returning nullptr.");
        return -1;
    }
    return elevationMap.value(elevationIdx)->nrays * elevationMap.value(elevationIdx)->nbins;
}



float MRadarGrid::getBinDistance(unsigned int elIdx) const
{
    if (elevationMap.contains(elIdx) == false)
    {
        LOG4CPLUS_ERROR(mlog, "ERROR: requested elevation is not in list of "
                              "elevations yet. Returning zero.");
    }
    return elevationMap[elIdx]->binDistance;
}


float MRadarGrid::getRangeStart(unsigned int elIdx) const
{
    if (elevationMap.contains(elIdx) == false)
    {
        LOG4CPLUS_ERROR(mlog, "ERROR: requested elevation is not in list of "
                              "elevations yet. Returning zero.");
    }
    return elevationMap[elIdx]->rangeStart;
}


float MRadarGrid::getValue(int elIdx, int rayIdx, int binIdx) const
{
    if (elIdx < 0 || rayIdx < 0 || binIdx < 0)
    {
        // Index '-1' used to indicate position outside radar grid.
        return M_MISSING_VALUE;
    }
    else if (elevationMap.count(elIdx) == 0
             || rayIdx > static_cast<int>(elevationMap.value(elIdx)->nrays) - 1
             || binIdx > static_cast<int>(elevationMap.value(elIdx)->nbins) - 1)
    {
        QString msg = QString("Cannot find value with indices %1/%2/%3 - "
                              "Returning missing value")
                          .arg(elIdx)
                          .arg(rayIdx)
                          .arg(binIdx);
        LOG4CPLUS_ERROR(mlog, msg.toStdString());
        return M_MISSING_VALUE;
    }

    unsigned int nbins = elevationMap.value(elIdx)->nbins;

    return elevationMap.value(elIdx)->data.at(rayIdx * nbins + binIdx);
}


float MRadarGrid::getValue(uint elIdx, uint n) const
{
    return elevationMap[elIdx]->data[n];
}


void MRadarGrid::setValue(unsigned int elIdx, unsigned int rayIdx,
                          unsigned int binIdx, float value)
{
    if (elevationMap.count(elIdx) == 0
        || rayIdx > elevationMap.value(elIdx)->nrays - 1
        || binIdx > elevationMap.value(elIdx)->nbins - 1)
    {
        LOG4CPLUS_ERROR(mlog, "One of the indicies is out of bound "
                              "- cant set value. ");
        return;
    }
    MRadarElevation *elevation = elevationMap.value(elIdx);

    elevation->data[rayIdx * elevation->nbins + binIdx] = value;
}


float MRadarGrid::min()
{
    float max = std::numeric_limits<float>::lowest();
    for (MRadarElevation *elevation : elevationMap)
    {
        for (const float &val : elevation->data)
            if (val != M_MISSING_VALUE && val > max)
            {
                max = val;
            }
    }
    return max;
}


float MRadarGrid::max()
{
    float min = std::numeric_limits<float>::min();
    for (MRadarElevation *elevation : elevationMap)
    {
        for (const float &val : elevation->data)
            if (val != M_MISSING_VALUE && val < min)
            {
                min = val;
            }
    }
    return min;
}


void MRadarGrid::setToZero()
{
    for (MRadarElevation *elevation : elevationMap)
    {
        for (float &x : elevation->data)
        {
            x = 0.;
        }
    }
}


void MRadarGrid::setToValue(float val)
{
    for (MRadarElevation *elevation : elevationMap)
    {
        for (float &x : elevation->data)
        {
            x = val;
        }
    }
}


const float* MRadarGrid::getData(uint elIdx) const
{
    return elevationMap[elIdx]->data.constData();
}


QVector3D MRadarGrid::getMostNorthernPointLonLatMetre() const
{
    float azAngleNorth = 0;

    QVector3D coord = getRadarStationLonLatHeight();
    for (MRadarElevation *el : elevationMap)
    {
        // The position furthest out is at the bottom of the grid cells.
        float elAngleLowest = el->elevationAngle - 0.5 * el->verticalBeamwidth;
        QVector3D temp =
            radToLonLatMetreFromValues(elAngleLowest, azAngleNorth, el->range);

        // Find maximum latitude.
        if (temp.y() > coord.y())
        {
            coord = temp;
        }
    }

    return coord;
}


QVector3D MRadarGrid::getMostSouthernPointLonLatMetre() const
{
    float azAngleSouth = 180;

    QVector3D coord = getRadarStationLonLatHeight();
    for (MRadarElevation *el : elevationMap)
    {
        // The position furthest out is at the bottom of the grid cells.
        float elAngleLowest = el->elevationAngle - 0.5 * el->verticalBeamwidth;
        QVector3D temp =
            radToLonLatMetreFromValues(elAngleLowest, azAngleSouth, el->range);

        // Find minimum (maximum negative) latitude.
        if (temp.y() < coord.y())
        {
            coord = temp;
        }
    }

    return coord;
}


QVector3D MRadarGrid::getMostEasternPointLonLatMetre() const
{
    float azAngleEast = 90;

    QVector3D coord = getRadarStationLonLatHeight();
    for (MRadarElevation *el : elevationMap)
    {
        // The position furthest out is at the bottom of the grid cells.
        float elAngleLowest = el->elevationAngle - 0.5 * el->verticalBeamwidth;
        QVector3D temp =
            radToLonLatMetreFromValues(elAngleLowest, azAngleEast, el->range);

        // Find maximum positive longitude distance.
        if (temp.x() > coord.x())
        {
            coord = temp;
        }
    }

    return coord;
}


QVector3D MRadarGrid::getMostWesternPointLonLatMetre() const
{
    float azAngleWest = 270;

    QVector3D coord = getRadarStationLonLatHeight();
    for (MRadarElevation *el : elevationMap)
    {
        // The position furthest out is at the bottom of the grid cells.
        float elAngleLowest = el->elevationAngle - 0.5 * el->verticalBeamwidth;
        QVector3D temp =
            radToLonLatMetreFromValues(elAngleLowest, azAngleWest, el->range);

        // Find minimum (maximum negative) longitude.
        if (temp.x() < coord.x())
        {
            coord = temp;
        }
    }

    return coord;
}


float MRadarGrid::getTopHeightMetre() const
{
    float topHeight = getRadarHeight();
    for (MRadarElevation *el : elevationMap)
    {
        // The top of each radar cell is considered as half the beam width above
        // the elevation angle.
        float elAngleHighest = el->elevationAngle + 0.5 * el->verticalBeamwidth;

        // Get the maximum height for this elevation. (Same for all azAngles)
        float maxHeightEl =
            radToLonLatMetreFromValues(elAngleHighest, 0, el->range).z();

        topHeight = std::max(topHeight, maxHeightEl);
    }
    return topHeight;
}


float MRadarGrid::getTopHeight_hPa() const
{
    return metre2pressure_standardICAO(getTopHeightMetre()) / 100.;
}


GL::MTexture *MRadarGrid::getTexture(QGLWidget *currentGLContext,
                                     bool nullTexture)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MTexture *t = static_cast<GL::MTexture *>(
        glRM->getGPUItem(dataRaycasterTextureID));
    if (t)
    {
        return t;
    }

    // Creates a texture in which the size is constant for all elevations, and
    // set to the size of the lowest elevation. Empty grid cells are filled with
    // M_MISSING_VALUE. If other elevations have more range bins than the lowest,
    // their outer bins are ignored.
    // TODO (sf, 15Apr2024) -- Currently assumes that the lowest elevation has
    //  the largest grid. Better generalize this to other cases.
    const MRadarElevation *const bottomElevation = elevationMap.first();

    std::vector<float> data;

    // this is necessary since the radar data isn't stored in one continuous array
    for (MRadarElevation *elevation : elevationMap)
    {
        int i = elevation->elevationIndexKey;

        for (unsigned int j = 0; j < bottomElevation->nrays; j++)
        {
            for (unsigned int k = 0; k < bottomElevation->nbins; k++)
            {
                if (elevationMap.value(i)->nrays <= j
                    || elevationMap.value(i)->nbins <= k)
                {
                    data.push_back(M_MISSING_VALUE);
                    data.push_back(M_MISSING_VALUE);
                    data.push_back(M_MISSING_VALUE);
                    data.push_back(M_MISSING_VALUE);
                }
                else
                {
                    QVector3D cartCoords = elevation->cartesianCoordinates.at(
                        j * elevation->nbins + k);
                    data.push_back(cartCoords.x());
                    data.push_back(cartCoords.y());
                    data.push_back(cartCoords.z());
                    data.push_back(
                        elevation->data.at(j * elevation->nbins + k));
                }
            }
        }
    }

    GLint textureInternalFormat = GL_RGBA32F;
    GLenum textureFormat = GL_RGBA;
    // No texture with this item's data exists. Create a new one.
    // width, height, depth
    t = new GL::MTexture(dataRaycasterTextureID, GL_TEXTURE_3D,
                         textureInternalFormat, bottomElevation->nbins,
                         bottomElevation->nrays, elevationMap.size());

    if (glRM->tryStoreGPUItem(t))
    {
        // The new texture was successfully stored in the GPU memory manger.
        // We can now upload the data.
        glRM->makeCurrent();

        t->bindToLastTextureUnit();

        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_BORDER);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        if (nullTexture)
        {
            glTexImage3D(GL_TEXTURE_3D, 0, textureInternalFormat,
                         elevationMap.size(), bottomElevation->nrays,
                         bottomElevation->nbins, 0, textureFormat, GL_FLOAT,
                         NULL);
        }
        else
        {
            glTexImage3D(GL_TEXTURE_3D, 0, textureInternalFormat,
                         bottomElevation->nbins, bottomElevation->nrays,
                         elevationMap.size(), 0, textureFormat, GL_FLOAT,
                         data.data());
        }
        CHECK_GL_ERROR;

        if (currentGLContext)
        {
            currentGLContext->makeCurrent();
        }
    }
    else
    {
        delete t;
    }

    return static_cast<GL::MTexture *>(
        glRM->getGPUItem(dataRaycasterTextureID));
}


void MRadarGrid::releaseTexture()
{
    MGLResourcesManager::getInstance()->releaseGPUItem(getID());
}


GL::MTexture *MRadarGrid::getRayTexture(QGLWidget *currentGLContext,
                                        bool nullTexture)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's rayArray already exists in GPU memory.
    GL::MTexture *t =
        static_cast<GL::MTexture *>(glRM->getGPUItem(raysRaycasterTextureID));
    if (t)
    {
        return t;
    }

    float rayArray[elevationMap.size()][standardAzimuthGridsize];

    // this is necessary since the radar rayArray isnt stored in one continuous array
    int i = 0;
    for (const int elIdx : elevationMap.keys())
    {
        const MRadarElevation *const elevation = elevationMap.value(elIdx);

        for (int azIdx = 0; azIdx < standardAzimuthGridsize; azIdx++)
        {
            if (azIdx < static_cast<int>(elevation->nrays))
            {
                rayArray[i][azIdx] = elevation->rayAngles.at(azIdx);
            }
            else
            {
                rayArray[i][azIdx] = M_MISSING_VALUE;
            }
        }
        i++;
    }

    GLint textureInternalFormat = GL_R32F; // RGB, 16 bit unsigned integer
    GLenum textureFormat = GL_RED;
    // No texture with this item's rayArray exists. Create a new one.
    t = new GL::MTexture(raysRaycasterTextureID, GL_TEXTURE_2D,
                         textureInternalFormat, standardAzimuthGridsize,
                         elevationMap.size());

    if (glRM->tryStoreGPUItem(t))
    {
        // The new texture was successfully stored in the GPU memory manger.
        // We can now upload the rayArray.
        glRM->makeCurrent();

        t->bindToLastTextureUnit();

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        if (nullTexture)
        {
            glTexImage2D(GL_TEXTURE_2D, 0, textureInternalFormat,
                         elevationMap.size(), standardAzimuthGridsize, 0,
                         textureFormat, GL_FLOAT, NULL);
        }
        else
        {
            glTexImage2D(GL_TEXTURE_2D, 0, textureInternalFormat,
                         standardAzimuthGridsize, elevationMap.size(), 0,
                         textureFormat, GL_FLOAT, rayArray);
        }
        CHECK_GL_ERROR;

        if (currentGLContext)
        {
            currentGLContext->makeCurrent();
        }
    }
    else
    {
        delete t;
    }

    return static_cast<GL::MTexture *>(
        glRM->getGPUItem(raysRaycasterTextureID));
}


void MRadarGrid::calculateHorizontalRadarSection(float heightMetre,
                                                 QVector<float> &lonlats,
                                                 QVector<float> &data)
{
    int nrays = standardAzimuthGridsize;

    float heightPa = float(metre2pressure_standardICAO(heightMetre) / 100.);

    float rangeCenter, rangeMin, rangeMax;
    int binRangeMin, binRangeMax;

    // counts the current range bin index ACROSS ELEVATIONS.
    int counter = 0;

    for (const uint &elIdx : getElevationIndexList())
    {
        float elAngle = elevationMap[elIdx]->elevationAngle;
        float verticalBeamwidth = elevationMap[elIdx]->verticalBeamwidth;
        float rangeStart = getRangeStart(elIdx);
        float binDistance = getBinDistance(elIdx);

        // The smaller the elevation angle, the further outwards
        // it overlaps with the horizontal profile
        float upperElAngle = elAngle + verticalBeamwidth / 2;
        float lowerElAngle = elAngle - verticalBeamwidth / 2;
        int nbins = getNbins(elIdx);

        rangeMin = heightToRange(heightMetre, upperElAngle); // in m
        rangeMax = heightToRange(heightMetre, lowerElAngle); // in m
        binRangeMin = int((rangeMin - rangeStart) / binDistance);
        binRangeMax = int((rangeMax - rangeStart) / binDistance);

        // start at the most outer range bin and move inwards. Break if outside of interval.
        for (int binIdx = binRangeMax; binIdx >= binRangeMin && binIdx < nbins;
             binIdx--)
        {
            rangeCenter = rangeStart + (binIdx + 0.5) * binDistance; // in m

            for (int azIdx = 0; azIdx < nrays; azIdx++)
            {
                // The stored rayAngles are at the start of each azimuth interval. For the point to be at the center,
                // half an interval (0.5 * 360.0 / nrays) is added.
                float azAngle =
                    elevationMap[elIdx]->rayAngles[azIdx] + 0.5 * 360.0 / nrays;
                QVector3D geoCoord =
                    radToLonLatMetreFromValues(elAngle, azAngle, rangeCenter);

                // Keep height exactly constant.
                geoCoord[2] = heightPa;

                for (int i = 0; i < 3; i++)
                {
                    lonlats << geoCoord[i];
                }
                data << elevationMap[elIdx]->data[nbins * azIdx + binIdx];
            }
            counter++;
        }
    }
    nHorizontalBins = counter;
}


GL::MTexture *
MRadarGrid::getRadarElevationDataTexture(uint elIdx,
                                         QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    QString dataTextureElIdxID = dataTextureID + QString::number(elIdx);

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MTexture *t =
        static_cast<GL::MTexture *>(glRM->getGPUItem(dataTextureElIdxID));
    if (t)
    {
        return t;
    }

    uint nbins = elevationMap[elIdx]->nbins;
    uint nrays = elevationMap[elIdx]->nrays;

    // No texture with this item's data exists. Create a new one.
    t = new GL::MTexture(dataTextureElIdxID, GL_TEXTURE_2D, GL_R32F,
                         nbins, nrays);

    if (glRM->tryStoreGPUItem(t))
    {
        // The new texture was successfully stored in the GPU memory manger.
        // We can now upload the data.
        glRM->makeCurrent();

        t->bindToLastTextureUnit();

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        float *dataPointer = elevationMap[elIdx]->data.data();

        glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, nbins, nrays, 0,
                     GL_RED, GL_FLOAT, dataPointer);
        CHECK_GL_ERROR;


        if (currentGLContext)
        {
            currentGLContext->makeCurrent();
        }
    }
    else
    {
        delete t;
    }

    return static_cast<GL::MTexture *>(glRM->getGPUItem(dataTextureElIdxID));
}


GL::MTexture *MRadarGrid::getHSecLonLatTexture(QVector<float> lonlats,
                                                QGLWidget *currentGLContext)
{
    int nrays = standardAzimuthGridsize;
    // Bins in horizontal slice can differ from nbins in 3d radar grid.
    int nbinsH = lonlats.length() / (nrays * 3);


    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MTexture *t =
        static_cast<GL::MTexture *>(glRM->getGPUItem(hSecLonLatsTextureID));

    if (!t)
    {
        t = new GL::MTexture(hSecLonLatsTextureID, GL_TEXTURE_3D,
                             GL_R32F, 3, nrays, nbinsH);
        glRM->tryStoreGPUItem(t);
    }

    // The new texture was successfully stored in the GPU memory manger.
    // We can now upload the data.
    glRM->makeCurrent();

    t->bindToLastTextureUnit();

    // Set texture parameters: wrap mode and filtering.
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // upload:
    glTexImage3D(GL_TEXTURE_3D,    // target
                 0,                // level of detail
                 GL_R32F,  // internal format
                 3, nrays, nbinsH, // width, height, depth
                 0,                // border
                 GL_RED,         // format
                 GL_FLOAT,         // data type of the pixel data
                 lonlats.data());
    CHECK_GL_ERROR;


    if (currentGLContext)
    {
        currentGLContext->makeCurrent();
    }

    return static_cast<GL::MTexture *>(glRM->getGPUItem(hSecLonLatsTextureID));
}


GL::MTexture *MRadarGrid::getHSecDataTexture(QVector<float> data,
                                              QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MTexture *t =
        static_cast<GL::MTexture *>(glRM->getGPUItem(hSecDataTextureID));

    int nrays = standardAzimuthGridsize;
    // Bins in horizontal slice can differ from nbins in 3d radar grid.
    int nbinsH = data.length() / nrays;

    if (! t)
    {
        // No texture with this item's data exists. Create a new one.
        t = new GL::MTexture(hSecDataTextureID, GL_TEXTURE_2D, GL_R32F,
                             nrays, nbinsH);

        glRM->tryStoreGPUItem(t);
    }

    // The new texture was successfully stored in the GPU memory manger.
    // We can now upload the data.
    glRM->makeCurrent();

    t->bindToLastTextureUnit();

    // Set texture parameters: wrap mode and filtering (RTVG p. 64).
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    // upload.
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, nrays, nbinsH, 0, GL_RED,
                 GL_FLOAT, data.data());
    CHECK_GL_ERROR;

    if (currentGLContext)
    {
        currentGLContext->makeCurrent();
    }

    return static_cast<GL::MTexture *>(glRM->getGPUItem(hSecDataTextureID));
}


GL::MShaderStorageBufferObject *MRadarGrid::getMetadataBuffer()
{
    struct RadarMetadata
    {
        GLuint nbins;
        GLuint nrays;
        GLuint elevationNumber;
        GLuint scanOrderIndex;
        GLfloat range;
        GLfloat binDistance;
        GLfloat elevationAngleDegrees;
        GLfloat verticalBeamwidth;
        GLfloat horizontalBeamwidth;
        GLfloat rangeStart;
    };

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a buffer with this item's data already exists in GPU memory.
    GL::MShaderStorageBufferObject *ssbo =
        static_cast<GL::MShaderStorageBufferObject *>(
            glRM->getGPUItem(metadataBufferID));
    if (ssbo)
    {
        return ssbo;
    }

    QVector<RadarMetadata> metadata;
    for (int key : elevationMap.keys())
    {
        const MRadarElevation *const elevation = elevationMap.value(key);
        metadata.push_back({elevation->nbins, elevation->nrays,
                            elevation->elevationIndexKey, elevation->scanIndex,
                            static_cast<GLfloat>(elevation->range),
                            static_cast<GLfloat>(elevation->binDistance),
                            static_cast<GLfloat>(elevation->elevationAngle),
                            static_cast<GLfloat>(elevation->verticalBeamwidth),
                            static_cast<GLfloat>(elevation->horizontalBeamwidth),
                            static_cast<GLfloat>(elevation->rangeStart)});
    }

    ssbo = new GL::MShaderStorageBufferObject(metadataBufferID,
                                              sizeof(RadarMetadata),
                                              elevationMap.size());
    if (glRM->tryStoreGPUItem(ssbo))
    {
        ssbo->upload(metadata.data(), GL_STATIC_READ);
    }

    return static_cast<GL::MShaderStorageBufferObject *>(
        glRM->getGPUItem(metadataBufferID));
}


/*****                    TRANSFORMATION FUNCTIONS                        *****/

QVector3D MRadarGrid::radToLonLatMetreFromValues(float elevationAngle,
                                                 float azimuthAngle,
                                                 float range) const
{
    QVector3D pos;

    double R_e = MetConstants::EARTH_RADIUS_km * 1000; // Earth radius in m.
    double R_r = 4 * R_e; // Refraction radius of the radar beam.
    double alpha = degreesToRadians(elevationAngle);
    double delta = degreesToRadians(azimuthAngle);

    double s = range;
    double h0 = heightRadar;

    double beta = s / R_r;
    double latRad = degreesToRadians(latRadar);

    float kappa = M_PI/2 + alpha - beta;
    // Coordinates of point P in a coord. system that has its centre in the
    // centre of the refraction circle.
    float x_refract = R_r * cos(kappa);
    float y_refract = R_r * sin(kappa);

    // Distances between the centre of the Earth and the centre of the
    // refraction circle.
    float d = R_r * sin(alpha);
    float c = R_r * cos(alpha) - (R_e + h0);

    // Coordinates of point P in a coord. system that has its centre in the
    // centre of the Earth.
    float x = x_refract + d;
    float y = y_refract - c;

    // Height of point P in metres (distance between P and the Earth centre via
    // Pythagoras, minus Earth radius).
    pos[2]= sqrt(x*x + y*y) - R_e;

    // Angle between radar station, centre of the Earth, and P.
    float ThetaE = asin(x / sqrt(x*x + y*y));

    // Latitude and longitude of P via spherical laws of sines and cosines.
    float latitudeRadians = asin(sin(latRad) * cos(ThetaE) +
                                 cos(latRad) * sin(ThetaE) * cos(delta));
    float tau = asin(sin(delta) * sin(ThetaE) / cos(latitudeRadians));
    pos[0] = lonRadar + radiansToDegrees(tau); // Longitude in °.
    pos[1] = radiansToDegrees(latitudeRadians); // Latitude in °.

    return pos;
}


QVector3D MRadarGrid::radToLonLatMetreFromIndices(unsigned int elevationIdx,
                                                  unsigned int rayIdx,
                                                  unsigned int binIdx)
{
    return radToLonLatMetreFromValues(
        elevationMap.value(elevationIdx)->elevationAngle,
        elevationMap.value(elevationIdx)->rayAngles.at(rayIdx),
        elevationMap.value(elevationIdx)->binDistance * ((float)binIdx + 0.5));
}


QVector3D MRadarGrid::radToLonLatPressure_Pa_FromValues(float elevationAngle,
                                                        float azimuthAngle,
                                                        float range)
{
    QVector3D pos =
        radToLonLatMetreFromValues(elevationAngle, azimuthAngle, range);
    if (pos[0] == -1 && pos[1] == -1 && pos[2] == -1)
    {
        return pos;
    }
    // calculate pressure in Pa:
    pos[2] = metre2pressure_standardICAO(pos[2]);
    return pos;
}


QVector3D MRadarGrid::radToLonLatPressure_hPa_FromValues(float elevationAngle,
                                                         float azimuthAngle,
                                                         float range)
{
    QVector3D pos =
        radToLonLatPressure_Pa_FromValues(elevationAngle, azimuthAngle, range);
    pos[2] /= 100;

    return pos;
}


QVector3D MRadarGrid::radToLonLatPressure_Pa_FromIndices(
    unsigned int elevationIdx, unsigned int rayIdx, unsigned int binIdx)
{
    QVector3D pos = radToLonLatMetreFromIndices(elevationIdx, rayIdx, binIdx);
    if (pos[0] == -1 && pos[1] == -1 && pos[2] == -1)
    {
        return pos;
    }
    // calculate pressure in Pa:
    pos[2] = metre2pressure_standardICAO(pos[2]);
    return pos;
}


QVector3D MRadarGrid::radToGlobalCartFromValues(float elevationAngle,
                                                float azimuthAngle, float range)
{
    return lonLatMetreToGlobalCart(
        radToLonLatMetreFromValues(elevationAngle, azimuthAngle, range));
}


QVector3D MRadarGrid::radToGlobalCartFromIndices(unsigned int elevationIdx,
                                                 unsigned int rayIdx,
                                                 unsigned int binIdx)
{
    return lonLatMetreToGlobalCart(
        radToLonLatMetreFromIndices(elevationIdx, rayIdx, binIdx));
}


float MRadarGrid::heightToRange(float heightMetre, float elevationAngleDegree)
{
    double R_e = MetConstants::EARTH_RADIUS_km * 1000; // Earth radius in meters
    double R_f =
        (4 / 3)
        * R_e; // effective radius of a fictitious earth that compensates for refraction
    double z_p = heightMetre;
    double alpha = degreesToRadians(elevationAngleDegree);
    double h0 = getRadarHeight();

    // range in metre
    double s = sqrt(pow((R_f + h0) * sin(alpha), 2) + 2 * (z_p - h0) * R_f
                    + z_p * z_p - h0 * h0)
               - (R_f + h0) * sin(alpha);

    return s;
}


/*****                     INTERPOLATION FUNCTIONS                        *****/
float MRadarGrid::interpolateValue(float lon, float lat, float p_hPa)
{
    return inverseTransformationInterpolation(lon, lat, p_hPa * 100);
}


double MRadarGrid::inverseTransformationInterpolation(float lonDeg,
                                                      float latDeg,
                                                      float pressure_Pa,
                                                      int *nearestNeighbors)
{
    if (nearestNeighbors != nullptr)
    {
        nearestNeighbors[0] = -1;
        nearestNeighbors[1] = -1;
        nearestNeighbors[2] = -1;
    }

    float Re = MetConstants::EARTH_RADIUS_km * 1000; // Earth radius in metres.
    float R = 4 * Re; // Approximated radius of the radar beam curvature.
    float h0 = heightRadar;

    // check if the point is roughly inside the radar range.
    QVector3D radarCart = radarPositionCartesian;
    QVector3D pointCart =
        lonLatPressureToGlobalCart(lonDeg, latDeg, pressure_Pa);
    QVector3D distanceVector = pointCart - radarCart;
    // The length of the straight line between the radar and the point of interest.
    float directDistance = distanceVector.length();

    // The angle of a curved beam of radius R between the radar site and the point of interest.
    double beta = asin(directDistance / (2 * R)) * 2;
    // The length of the potential radar ray between the radar site and the point of interest.
    double rayDist = beta * R;

    // Points outside the radar range are discarded.
    if (rayDist > maxRadarRange)
    {
        return M_MISSING_VALUE;
    }

    // Elevation angle and range
    // Height the point of interest above ground in meters.
    double zp = pressure2metre_standardICAO(pressure_Pa);

    // The potential radar ray elevation angle in radians.
    double elevationAngleRad =
        acos((pow((Re + h0), 2) + pow(directDistance, 2) - pow((Re + zp), 2))
             / (2 * (Re + h0) * directDistance))
        - (M_PI - beta) / 2;
    double elevationAngleDeg = radiansToDegrees(elevationAngleRad);

    QVector3D indexOfClosestPoint = QVector3D(-1, -1, -1);

    // Check which elevation includes the point of interest.
    float smallestAngleDifference = 1.0 / 0.0;
    int closestElevationIdx = -1;
    int closestBinIdx = -1;

    for (const int elIdx : elevationMap.keys())
    {
        // Angular difference to central angle of this elevation.
        float dif =
            abs(elevationAngleDeg - elevationMap.value(elIdx)->elevationAngle);

        // Beam extent in one direction.
        float vHalfWidth = elevationMap.value(elIdx)->verticalBeamwidth / 2.0;

        // Is the potential beam angle under which the point is reached covered by this beam?
        if (dif < vHalfWidth && dif < smallestAngleDifference)
        {
            closestElevationIdx = elIdx;

            // Check if a bin of this elevation includes the point of interest. For that, calculate the point's hypothetical bin index,
            // and compare it with the maximum index of the elevation.
            float binDistance = getBinDistance(elIdx);
            float rangeStart = elevationMap.value(elIdx)->rangeStart;
            unsigned int binIdx = std::floor(
                (rayDist - rangeStart)
                / binDistance); // The bins start at rangeStart, and each is binDistance long.
            if (binIdx < elevationMap.value(elIdx)->nbins)
            {
                // If the point is inside an elevation and bin: remember it.
                smallestAngleDifference = dif;
                closestBinIdx = binIdx;
                // Found the correct elevation and bin.
                // If the elevations don't overlap, we can stop here.
            }
        }
    }

    if (closestElevationIdx == -1 || closestBinIdx == -1)
    {
        return M_MISSING_VALUE;
    }

    // Azimuth angle -----------------------------------------------------------
    double azimuth = gcAzimuth_deg(lonRadar, latRadar, lonDeg, latDeg);
    int closestAzimuthIdx = static_cast<int>(
        std::floor(azimuth * standardAzimuthGridsize / 360.0));
    // -------------------------------------------------------------------------


    indexOfClosestPoint[0] = closestElevationIdx;
    indexOfClosestPoint[1] = closestAzimuthIdx;
    indexOfClosestPoint[2] = closestBinIdx;

    // if used, write nearest Neighbor Indices to nearestNeighbor array
    if (nearestNeighbors != nullptr)
    {
        nearestNeighbors[0] = indexOfClosestPoint[0];
        nearestNeighbors[1] = indexOfClosestPoint[1];
        nearestNeighbors[2] = indexOfClosestPoint[2];
    }

    double value = getValue(indexOfClosestPoint[0], indexOfClosestPoint[1],
                            indexOfClosestPoint[2]);

    return value;
}


/*****                   CALCULATIONS/MATH FUNCTIONS                      *****/

double MRadarGrid::calculateMaxAllowedDistance(int elevationIdx, int binIdx)
{
    // A single radar volume is approximated as a truncated pyramid, with the pyramid's 'base area' pointing away from the radar site.
    // The max. distance from the center of the volume is calculated via the Pythagorean theorem.
    const MRadarElevation *const i = elevationMap.value(elevationIdx);
    // The max. allowed distances in v. and h. directions are calculated on the volumes largest side (pointing away from the radar).
    double R = i->binDistance * ((double)binIdx + 0.5);
    double maxAllowedDistanceVertical =
        sin(degreesToRadians(i->verticalBeamwidth / 2)) * R;
    double maxAllowedDistanceHorizontal =
        sin(degreesToRadians(i->horizontalBeamwidth / 2)) * R;
    // The max. distance from the center in ray direction is always half the bin size.
    double maxAllowedDistanceRayDirection = i->binDistance / 2;
    double maxAllowedDistanceSquared =
        pow(maxAllowedDistanceHorizontal, 2)
        + pow(maxAllowedDistanceVertical, 2)
        + pow(maxAllowedDistanceRayDirection, 2.0);
    return sqrt(maxAllowedDistanceSquared);
}


float MRadarGrid::getAbsDistanceLon() const
{
    // Most eastern/western points of the radar grid.
    QVector3D maxEast = getMostEasternPointLonLatMetre();
    QVector3D maxWest = getMostWesternPointLonLatMetre();

    // Maximum distance in longitude direction.
    float maxDistLonDeg = maxEast.x() - maxWest.x();
    return fabs(maxDistLonDeg);
}


float MRadarGrid::getAbsDistanceLat() const
{
    // Most northern/southern points of the radar grid.
    QVector3D maxNorth = getMostNorthernPointLonLatMetre();
    QVector3D maxSouth = getMostSouthernPointLonLatMetre();

    // Maximum distance in longitude direction.
    float maxDistLatDeg = maxNorth.y() - maxSouth.y();
    return fabs(maxDistLatDeg);

}


QVector3D MRadarGrid::getNorthWestTopDataVolumeCorner_lonlatp()
{
    QVector3D dataNWCrnr;

    double radarLat = getRadarStationLat();
    double radarLon = getRadarStationLon();

    // Calculate the positions most north and east in the data, and calculate
    // the distance to the radar position.
    float lonMax = 0;
    float latMax = 0;
    double pressureMin_Pa = metre2pressure_standardICAO(0);

    for (const uint &elIdx : getElevationIndexList())
    {
        double alpha = getElevationAngle(elIdx);
        int nbins = getNbins(elIdx);
        float binDistance = getBinDistance(elIdx);
        // Assumption here: radar data collection starts north, and rotates
        // clockwise, therefore 0° points north, 90° points east.
        QVector3D posMaxNorth =
            radToLonLatMetreFromValues(alpha, 0, nbins * binDistance);
        QVector3D posMaxEast =
            radToLonLatMetreFromValues(alpha, 90, nbins * binDistance);
        latMax = std::max(latMax, posMaxNorth.y());
        lonMax = std::max(lonMax, posMaxEast.x());

        // Calculate the min. pressure (max. height) of a last bin.
        // Lon/lat position doesn't matter here.
        pressureMin_Pa = std::min(pressureMin_Pa,
                                  metre2pressure_standardICAO(posMaxNorth.z()));
    }
    // Maximum distance to the radar position in longitude and latitude direction.
    latMax -= radarLat;
    lonMax -= radarLon;

    // North West corner
    dataNWCrnr[0] = radarLon - lonMax;
    dataNWCrnr[1] = radarLat + latMax;
    dataNWCrnr[2] = pressureMin_Pa/100.; //Convert to hPa.

    return dataNWCrnr;
}


QVector3D MRadarGrid::getSouthEastBottomDataVolumeCorner_lonlatp()
{
    QVector3D dataSECrnr;

    double radarLat = getRadarStationLat();
    double radarLon = getRadarStationLon();

    // Calculate the positions most north and east in the data, and calculate the distance to the radar position.
    float lonMax = 0;
    float latMax = 0;
    for (const uint &elIdx : getElevationIndexList())
    {
        double alpha = getElevationAngle(elIdx);
        int nbins = getNbins(elIdx);
        float binDistance = getBinDistance(elIdx);
        // Assumption here: radar data collection starts north, and rotates clockwise, therefore 0° points north, 90° points east.
        QVector3D posMaxNorth =
            radToLonLatMetreFromValues(alpha, 0, nbins * binDistance);
        QVector3D posMaxEast =
            radToLonLatMetreFromValues(alpha, 90, nbins * binDistance);

        latMax = std::max(latMax, posMaxNorth.y());
        lonMax = std::max(lonMax, posMaxEast.x());
    }

    // Maximum distance to the radar position in longitude and latitude direction.
    latMax -= radarLat;
    lonMax -= radarLon;

    // Get the pressure at the radar antenna (start of the beam).
    double pressureMax_Pa = metre2pressure_standardICAO(heightRadar);

    // South East corner
    dataSECrnr[0] = radarLon + lonMax;
    dataSECrnr[1] = radarLat - latMax;
    dataSECrnr[2] = pressureMax_Pa/100.; //Convert to hPa.

    return dataSECrnr;
}


GL::MTexture *MRadarGrid::getMinMaxAccelTexture3D(
        QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MTexture *t = static_cast<GL::MTexture*>(glRM->getGPUItem(minMaxAccelID));
    if (t) return t;

    // Size of acceleration structure.
    int nAccLon = 32;
    int nAccLat = 32;
    int nAccLnP = 32;

    if (minMaxAccel == nullptr)
    {
#ifdef ENABLE_MET3D_STOPWATCH
        MStopwatch stopwatch;
        LOG4CPLUS_INFO(mlog, "Creating new acceleration structure ...");
#endif

        // Get data volume corners in lon/lat/pressure space.
        QVector3D nwtDataCrnr = getNorthWestTopDataVolumeCorner_lonlatp();
        QVector3D sebDataCrnr = getSouthEastBottomDataVolumeCorner_lonlatp();

        // Convert pressure to ln(pressure) to divide vertically in ln(p).
        nwtDataCrnr.setZ(log(nwtDataCrnr.z()));
        sebDataCrnr.setZ(log(sebDataCrnr.z()));

        float deltaAccLon = (sebDataCrnr.x() - nwtDataCrnr.x()) / nAccLon;
        float deltaAccLat = (sebDataCrnr.y() - nwtDataCrnr.y()) / nAccLat;
        float deltaAccLnP = (sebDataCrnr.z() - nwtDataCrnr.z()) / nAccLnP;

        minMaxAccel = new MMemoryManagedArray<float>(2 * nAccLon * nAccLat * nAccLnP);
        MDataRequestHelper rh(getGeneratingRequest());
        rh.insert("AUXDATA", "MINMAXACCEL");
        minMaxAccel->setGeneratingRequest(rh.request());

        float min = std::numeric_limits<float>::max();
        float max = std::numeric_limits<float>::lowest();

        // Initialize acceleration structure.
        for (int iAcc = 0; iAcc < nAccLon; iAcc++)
        {
            for (int jAcc = 0; jAcc < nAccLat; jAcc++)
            {
                for (int kAcc = 0; kAcc < nAccLnP; kAcc++)
                {
                    minMaxAccel->data[INDEX4zyxc(kAcc, jAcc, iAcc, 0,
                             nAccLat, nAccLon, 2)] = min;
                    minMaxAccel->data[INDEX4zyxc(kAcc, jAcc, iAcc, 1,
                                                 nAccLat, nAccLon, 2)] = max;
                }
            }
        }

        // Iterate over all radar cells.
        for (const uint &elIdx : elevationMap.keys())
        {
            MRadarElevation* el = elevationMap[elIdx];
            float elAngle0 = el->elevationAngle - el->horizontalBeamwidth; // bottom
            float elAngle1 = el->elevationAngle + el->horizontalBeamwidth; // top
            for (unsigned int azIdx = 0; azIdx < el->nrays; azIdx++)
            {
                float azAngle0 = (float(azIdx) / el->nrays) * standardAzimuthGridsize; // left
                float azAngle1 = (float(azIdx+1) / el->nrays) * standardAzimuthGridsize; // right

                for (unsigned int binIdx = 0; binIdx < el->nbins; binIdx++)
                {
                    float range0 = el->rangeStart + el->binDistance * binIdx; // inner
                    float range1 = el->rangeStart + el->binDistance * (binIdx + 1); // outer

                    // Calculate radar cell corner positions in lon/lat/pressure.
                    QVector3D llp000 = radToLonLatPressure_hPa_FromValues(
                        elAngle0, azAngle0, range0);
                    QVector3D llp001 = radToLonLatPressure_hPa_FromValues(
                        elAngle0, azAngle0, range1);
                    QVector3D llp010 = radToLonLatPressure_hPa_FromValues(
                        elAngle0,  azAngle1, range0);
                    QVector3D llp011 = radToLonLatPressure_hPa_FromValues(
                        elAngle0, azAngle1, range1);
                    QVector3D llp100 = radToLonLatPressure_hPa_FromValues(
                        elAngle1, azAngle0, range0);
                    QVector3D llp101 = radToLonLatPressure_hPa_FromValues(
                        elAngle1, azAngle0, range1);
                    QVector3D llp110 = radToLonLatPressure_hPa_FromValues(
                        elAngle1,  azAngle1, range0);
                    QVector3D llp111 = radToLonLatPressure_hPa_FromValues(
                        elAngle1, azAngle1, range1);

                    QList<QVector3D> cornerList;
                    cornerList << llp000 << llp001 << llp010 << llp011
                    << llp100 << llp101 << llp110 << llp111;

                    float val = getValue(elIdx, azIdx, binIdx);

                    int minI = nAccLon;
                    int minJ = nAccLat;
                    int minK = nAccLnP;
                    int maxI = 0;
                    int maxJ = 0;
                    int maxK = 0;

                    for (QVector3D corner : cornerList)
                    {
                        float lon = corner.x();
                        float lat = corner.y();
                        float lnP = std::log(corner.z());

                        // Calculate indices of the brick surrounding this corner.
                        int iAcc = std::floor(
                            (lon - nwtDataCrnr.x()) / deltaAccLon);

                        int jAcc = std::floor(
                            (lat - nwtDataCrnr.y()) / deltaAccLat);

                        int kAcc = std::floor(
                            (lnP - nwtDataCrnr.z()) / deltaAccLnP);

                        minI = std::min(minI, iAcc);
                        minJ = std::min(minJ, jAcc);
                        minK = std::min(minK, kAcc);
                        maxI = std::max(maxI, iAcc);
                        maxJ = std::max(maxJ, jAcc);
                        maxK = std::max(maxK, kAcc);
                    }

                    for (int kAcc = minK; kAcc <= maxK; kAcc++)
                    {
                        for (int jAcc = minJ; jAcc <= maxJ; jAcc++)
                        {
                            for (int iAcc = minI; iAcc <= maxI; iAcc++)
                            {
                                // Update the acceleration array if the value of the current
                                // radar cell exceeds the brick's minimum/maximum.
                                minMaxAccel->data[INDEX4zyxc(kAcc, jAcc, iAcc, 0,
                                                             nAccLat, nAccLon, 2)] = std::min(
                                        val, minMaxAccel->data[INDEX4zyxc(
                                                kAcc, jAcc, iAcc, 0,
                                                nAccLat, nAccLon, 2)]);
                                minMaxAccel->data[INDEX4zyxc(kAcc, jAcc, iAcc, 1,
                                                             nAccLat, nAccLon, 2)] = std::max(
                                        val, minMaxAccel->data[INDEX4zyxc(
                                                kAcc, jAcc, iAcc, 1,
                                                nAccLat, nAccLon, 2)]);
                            }
                        }
                    }
                }
            }
        }


        if (memoryManager)
        {
            try
            {
                // Store in memory manager and get reference to stored item.
                // storeData() increases the fields reference counter in the
                // memory manager; the field is hence released in the
                // destructor.
                if ( !memoryManager->storeData(this, minMaxAccel) )
                {
                    // In the unlikely event that another thread has stored
                    // the same field in the mean time delete this one.
                    delete minMaxAccel;
                }
                minMaxAccel = static_cast<MMemoryManagedArray<float>*>(
                            memoryManager->getData(this, rh.request())
                            );
            }
            catch (MMemoryError const&)
            {
            }
        }
#ifdef ENABLE_MET3D_STOPWATCH
        stopwatch.split();
        LOG4CPLUS_INFO(mlog, "Acceleration structure created in "
                        << stopwatch.getElapsedTime(MStopwatch::SECONDS)
                        << " seconds.");
#endif
    }

    // No texture with this item's data exists. Create a new one.
    t = new GL::MTexture(minMaxAccelID, GL_TEXTURE_3D, GL_RG32F,
                         nAccLon, nAccLat, nAccLnP);

    if ( glRM->tryStoreGPUItem(t) )
    {
        // The new texture was successfully stored in the GPU memory manger.
        // We can now upload the data.
        glRM->makeCurrent();

        t->bindToLastTextureUnit();

        // Set texture parameters: wrap mode and filtering (RTVG p. 64).
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP);CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP);CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP);CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);CHECK_GL_ERROR;

        glTexImage3D(GL_TEXTURE_3D, 0, GL_RG32F,
                     nAccLon, nAccLat, nAccLnP,
                     0, GL_RG, GL_FLOAT, minMaxAccel->data);
        CHECK_GL_ERROR;

        if (currentGLContext) currentGLContext->makeCurrent();
    }
    else
    {
        delete t;
    }

    return static_cast<GL::MTexture*>(glRM->getGPUItem(minMaxAccelID));
}


void MRadarGrid::releaseMinMaxAccelTexture3D()
{
    MGLResourcesManager::getInstance()->releaseGPUItem(minMaxAccelID);
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/


/******************************************************************************
***                          PRIVATE METHODS                              ***
*******************************************************************************/


} // namespace Met3D
