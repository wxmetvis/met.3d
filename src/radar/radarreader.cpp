/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "radarreader.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/
MRadarReader::MRadarReader(QString identifier)
    : MRadarDataSource(),
      MAbstractDataReader(identifier),
      standardAzimuthGridsize(360)
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/
MRadarGrid* MRadarReader::produceData(MDataRequest request)
{
    MDataRequestHelper rh(request);
    // get values from request: variable, time step, if azimuth angles should be interpolated.
    QString variable           = rh.value("VARIABLE");
    QDateTime time             = rh.timeValue("VALID_TIME");

    bool interpolateAzimuthAngles = true;
    bool useUndetectVariable = false;
    if (rh.contains("USE_UNDETECT"))
    {
        useUndetectVariable = rh.intValue("USE_UNDETECT");
    }

    MRadarGrid* result;

    try
    {
        result = readGrid(variable, time, interpolateAzimuthAngles, useUndetectVariable);
    }
    catch (const std::exception& e)
    {
        LOG4CPLUS_ERROR(mlog, "ERROR: failed to read variable data from file "
                              "-- " << e.what());
        result = nullptr;
    }

    return result;
}

MTask* MRadarReader::createTaskGraph(MDataRequest request)
{
    // No dependencies, so we create a plain task.
    MTask* task = new MTask(request, this);
    // However, this task accesses the hard drive.
    task->setDiskReaderTask();
    return task;
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MRadarReader::locallyRequiredKeys()
{
    return (QStringList() << "VARIABLE" << "VALID_TIME" << "USE_UNDETECT");
}

} // namespace Met3D

