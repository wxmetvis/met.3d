/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2024      Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "radaractor.h"

// standard library imports

// related third party imports

// local application imports
#include "util/metroutines.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MRadarActor::MRadarActor()
    : MNWPMultiVarActor(),
      renderMode(RenderMode::pseudoColor3D),
      showVolumeThickness(true),
      cappiMode(CappiMode::Pressure),
      cappiSingleAltitudeBool(false)
{
    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setActorType(staticActorType());
    setName(getActorType());

    // Variable properties.
    variableProp = MNWPActorVarProperty("Variable");
    variableProp.setConfigKey("var");
    variableProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(variableProp);

    // General render properties.
    QStringList renderModesList = {"Pseudocolour 2D", "Pseudocolour 3D",
                                   "Vertical section", "Horizontal section"};
    renderModeProp = MEnumProperty("Render mode", renderModesList, int(renderMode));
    renderModeProp.setConfigKey("render_mode");
    renderModeProp.registerValueCallback([=]()
    {
        enableActorUpdates(false);
        renderMode = RenderMode(renderModeProp.value());
        if (renderMode == RenderMode::pseudoColor3D
                || renderMode == RenderMode::verticalSection)
        {
            showVolumeThickness = true;
        }
        else
        {
            showVolumeThickness = false;
        }

        // Different azimuth selection for vertical section (single angle)
        minAzimuthProp.setEnabled(renderMode != RenderMode::verticalSection);
        maxAzimuthProp.setEnabled(renderMode != RenderMode::verticalSection);
        vSectionAzProp.setEnabled(renderMode == RenderMode::verticalSection);

        // Horizontal section doesn't have elevation selection.
        if (renderMode == RenderMode::horizontalSection)
        {
            minElevationProp.setEnabled(false);
            maxElevationProp.setEnabled(false);
            singleElevationProp.setEnabled(false);
            showOnlyOneElevationProp.setEnabled(false);
        }
        else
        {
            showOnlyOneElevationProp.setEnabled(true);
            minElevationProp.setEnabled(!showOnlyOneElevationProp);
            maxElevationProp.setEnabled(!showOnlyOneElevationProp);
            singleElevationProp.setEnabled(showOnlyOneElevationProp);
        }

        // Horizontal section mode always shows only one height, therefore,
        // 'single altitude' selection is disabled.
        cappiSingleAltitudeBoolProp.setEnabled(
                renderMode != RenderMode::horizontalSection);

        cappiSingleAltitudeBool =
                (cappiSingleAltitudeBoolProp
                        || renderMode == RenderMode::horizontalSection);
        updateAltitudePropertiesEnabledStatus();

        // update textures.
        if (renderMode == RenderMode::horizontalSection)
        {
            updateHorizontalSection();
        }

        enableActorUpdates(true);
        emitActorChangedSignal();
    });
    actorPropertiesSupGroup.addSubProperty(renderModeProp);

    colourUndetectEnabledProp = MBoolProperty("Use colour for cells without signal", false);
    colourUndetectEnabledProp.setConfigKey("use_colour_for_cells_without_signal");
    colourUndetectEnabledProp.setTooltip(
        "If activated, cells in which no signal was received are drawn in a "
        "custom background colour ('no signal detected' colour).");
    colourUndetectEnabledProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(colourUndetectEnabledProp);

    undetectColourProp = MColorProperty("Colour for 'no signal detected'", {240, 240, 240, 255});
    undetectColourProp.setConfigKey("colour_for_no_signal_detected");
    undetectColourProp.setTooltip(
        "Background colour applied to cells without signal, if 'use colour "
        "for cells without signal' is active.");
    undetectColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(undetectColourProp);

    // Elevation properties.
    elevationGroupProp = MProperty("Elevation range");
    actorPropertiesSupGroup.addSubProperty(elevationGroupProp);

    showOnlyOneElevationProp = MBoolProperty("Limit to single elevation", false);
    showOnlyOneElevationProp.setConfigKey("limit_to_single_elevation");
    showOnlyOneElevationProp.registerValueCallback([=]()
    {
        enableActorUpdates(false);
        if (showOnlyOneElevationProp)
        {
            singleElevationProp.setEnabled(true);
            minElevationProp.setEnabled(false);
            maxElevationProp.setEnabled(false);

            minEl = singleElevationProp;
            maxEl = singleElevationProp;
        }
        else
        {
            singleElevationProp.setEnabled(false);
            minElevationProp.setEnabled(true);
            maxElevationProp.setEnabled(true);

            minEl = minElevationProp;
            maxEl = maxElevationProp;
        }
        enableActorUpdates(true);
        emitActorChangedSignal();
    });
    elevationGroupProp.addSubProperty(showOnlyOneElevationProp);

    minEl = 0;
    minElevationProp = MIntProperty("Minimum elevation", minEl);
    minElevationProp.setConfigKey("min_elevation");
    minElevationProp.setTooltip("Index of the lowest elevation angle shown.");
    minElevationProp.setMinMax(0, 9);
    minElevationProp.registerValueCallback([=]()
    {
        enableActorUpdates(false);
        minEl = minElevationProp;
        if (maxEl < minEl)
        {
            maxEl = minEl;
            maxElevationProp = maxEl;
        }
        enableActorUpdates(true);
        emitActorChangedSignal();
    });
    elevationGroupProp.addSubProperty(minElevationProp);

    maxEl = 9;
    maxElevationProp = MIntProperty("Maximum elevation", maxEl);
    maxElevationProp.setConfigKey("max_elevation");
    maxElevationProp.setTooltip("Index of the highest elevation angle shown.");
    maxElevationProp.setMinMax(0, 9);
    maxElevationProp.registerValueCallback([=]()
    {
        enableActorUpdates(false);
        maxEl = maxElevationProp;
        if (maxEl < minEl)
        {
            maxEl = minEl;
            maxElevationProp = maxEl;
        }
        enableActorUpdates(true);
        emitActorChangedSignal();
    });
    elevationGroupProp.addSubProperty(maxElevationProp);

    minElevationProp.setEnabled(!showOnlyOneElevationProp);
    maxElevationProp.setEnabled(!showOnlyOneElevationProp);

    singleElevationProp = MIntProperty("Single elevation", 0);
    singleElevationProp.setConfigKey("single_elevation");
    singleElevationProp.setEnabled(showOnlyOneElevationProp);
    singleElevationProp.setMinMax(0, 9);
    singleElevationProp.registerValueCallback([=]()
    {
        minEl = singleElevationProp;
        maxEl = singleElevationProp;

        emitActorChangedSignal();
    });
    elevationGroupProp.addSubProperty(singleElevationProp);

    // Azimuth properties.
    azimuthGroupProp = MProperty("Azimuth angle range");
    actorPropertiesSupGroup.addSubProperty(azimuthGroupProp);

    minAzimuthProp = MFloatProperty("Minimum azimuth angle", 0);
    minAzimuthProp.setConfigKey("min_azimuth_angle");
    minAzimuthProp.setMinMax(0.0f, 360.0f);
    minAzimuthProp.setDecimals(1);
    minAzimuthProp.setStep(1.0f);
    minAzimuthProp.setSuffix("°");
    minAzimuthProp.setEnabled(renderMode != RenderMode::verticalSection);
    minAzimuthProp.registerValueCallback([=]()
    {
        emitActorChangedSignal();
    });
    azimuthGroupProp.addSubProperty(minAzimuthProp);

    maxAzimuthProp = MFloatProperty("Maximum azimuth angle", 360.);
    maxAzimuthProp.setConfigKey("max_azimuth_angle");
    maxAzimuthProp.setMinMax(0.0f, 360.0f);
    maxAzimuthProp.setDecimals(1);
    maxAzimuthProp.setStep(1.0f);
    maxAzimuthProp.setSuffix("°");
    maxAzimuthProp.setEnabled(renderMode != RenderMode::verticalSection);
    maxAzimuthProp.registerValueCallback([=]()
    {
        emitActorChangedSignal();
    });
    azimuthGroupProp.addSubProperty(maxAzimuthProp);

    vSectionAzProp = MFloatProperty("Vertical section azimuth angle", 0.0f);
    vSectionAzProp.setConfigKey("vertical_section_azimuth_angle");
    vSectionAzProp.setTooltip("Single azimuth angle used in vertical section render mode.");
    vSectionAzProp.setMinMax(0.0f, 360.0f);
    vSectionAzProp.setDecimals(0);
    vSectionAzProp.setStep(1);
    vSectionAzProp.setSuffix("°");
    vSectionAzProp.setEnabled(renderMode == RenderMode::verticalSection);
    vSectionAzProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    azimuthGroupProp.addSubProperty(vSectionAzProp);

    // CAPPI/horizontal section properties.
    float absMinPressure = 20.0;
    float absMaxPressure = 1013.25;
    auto absMinHeight = float(
            pressure2metre_standardICAO(absMaxPressure * 100));
    auto absMaxHeight = float(
            pressure2metre_standardICAO(absMinPressure * 100));

    cappiGroupProp = MProperty("Altitude range");
    actorPropertiesSupGroup.addSubProperty(cappiGroupProp);

    QStringList cappiModesList = {"Pressure", "Height"};
    cappiModeProp = MEnumProperty("Specify altitude in ", cappiModesList, int(cappiMode));
    cappiModeProp.setConfigKey("specify_altitude_in");
    cappiModeProp.setTooltip("Set the altitude either in pressure [hPa] or height [m] units.");
    cappiModeProp.registerValueCallback([=]()
    {
        cappiMode = CappiMode(cappiModeProp.value());
        updateAltitudePropertiesEnabledStatus();
        emitActorChangedSignal();
    });
    cappiGroupProp.addSubProperty(cappiModeProp);

    // single altitude.
    if (renderMode == RenderMode::horizontalSection)
    {
        cappiSingleAltitudeBool = true;
    }
    else
    {
        cappiSingleAltitudeBool = false;
    }
    cappiSingleAltitudeBoolProp = MBoolProperty("Limit to single altitude", false);
    cappiSingleAltitudeBoolProp.setConfigKey("limit_to_single_altitude");
    cappiSingleAltitudeBoolProp.setEnabled(renderMode != RenderMode::horizontalSection);
    cappiSingleAltitudeBoolProp.registerValueCallback([=]()
    {
        cappiSingleAltitudeBool = cappiSingleAltitudeBoolProp;
        if (cappiSingleAltitudeBool)
        {
            cappiSinglePressure = cappiSinglePressureProp;
            cappiMinimumPressure = cappiSinglePressure;
            cappiMaximumPressure = cappiSinglePressure;
            /* Minimum and maximum pressure are both set to the single pressure
             * value, but their properties are NOT changed, as will be needed
             * when a pressure interval is selected again in the future.
             */
        }
        else // pressure interval selected
        {
            cappiMinimumPressure = cappiPressureLimitsProp.topProp;
            cappiMaximumPressure = cappiPressureLimitsProp.bottomProp;
        }

        updateAltitudePropertiesEnabledStatus();

        emitActorChangedSignal();
    });
    cappiGroupProp.addSubProperty(cappiSingleAltitudeBoolProp);

    cappiSinglePressure = 900.0f;
    cappiSinglePressureProp = MFloatProperty("Altitude", cappiSinglePressure);
    cappiSinglePressureProp.setConfigKey("altitude_hPa");
    cappiSinglePressureProp.setMinMax(absMinPressure, absMaxPressure);
    cappiSinglePressureProp.setDecimals(2);
    cappiSinglePressureProp.setStep(1.0f);
    cappiSinglePressureProp.setSuffix(" hPa");
    cappiSinglePressureProp.setHidden(
            !cappiSingleAltitudeBool || cappiMode != CappiMode::Pressure);
    cappiSinglePressureProp.registerValueCallback([=]()
    {
        cappiSinglePressure = cappiSinglePressureProp;

        cappiSingleHeightProp.suppressValueEvent(true);
        cappiSingleHeightProp = float(
                pressure2metre_standardICAO(cappiSinglePressure * 100));
        cappiSingleHeightProp.suppressValueEvent(false);

        if (renderMode == RenderMode::horizontalSection)
        {
            updateHorizontalSection();
        }
        else
        {
            cappiMinimumPressure = cappiSinglePressure;
            cappiMaximumPressure = cappiSinglePressure;
            /* Minimum and maximum pressure are both set to the single pressure
             * value, but their properties are NOT changed, as will be needed
             * when a pressure interval is selected again in the future.
             * */
        }

        emitActorChangedSignal();
    });
    cappiGroupProp.addSubProperty(cappiSinglePressureProp);

    auto singleHeight = float(pressure2metre_standardICAO(cappiSinglePressure * 100));
    cappiSingleHeightProp = MFloatProperty("Altitude",singleHeight);
    cappiSingleHeightProp.setConfigKey("altitude_m");
    cappiSingleHeightProp.setMinMax(absMinHeight, absMaxHeight);
    cappiSingleHeightProp.setDecimals(1);
    cappiSingleHeightProp.setStep(100.0f);
    cappiSingleHeightProp.setSuffix(" m");
    cappiSingleHeightProp.setHidden(!cappiSingleAltitudeBool
                                                || cappiMode != CappiMode::Height);
    cappiSingleHeightProp.registerValueCallback([=]()
    {
        float cappiSingleHeight = cappiSingleHeightProp;
        cappiSinglePressure = float(metre2pressure_standardICAO(cappiSingleHeight) / 100);
        cappiSinglePressureProp = cappiSinglePressure;
    });
    cappiGroupProp.addSubProperty(cappiSingleHeightProp);

    // Altitude range.
    // Pressure in hPa.
    cappiMaximumPressure = 1013;
    cappiMinimumPressure = 20;
    cappiPressureLimitsProp = MPropertyTemplates::VerticalExtentHPa(cappiMaximumPressure, cappiMinimumPressure);
    cappiPressureLimitsProp.groupProp.setHidden(cappiSingleAltitudeBool || cappiMode != CappiMode::Pressure);
    cappiPressureLimitsProp.bottomProp.setConfigKey("max_pressure");
    cappiPressureLimitsProp.topProp.setConfigKey("min_pressure");
    cappiPressureLimitsProp.bottomProp.registerValueCallback([=]()
    {
        cappiMaximumPressure = cappiPressureLimitsProp.bottomProp;
        adjustMinimumPressureToMaximum();
        cappiHeightLimitsProp.bottomProp = float(pressure2metre_standardICAO(cappiMaximumPressure * 100));
        emitActorChangedSignal();
    });
    cappiPressureLimitsProp.topProp.registerValueCallback([=]()
    {
        cappiMinimumPressure = cappiPressureLimitsProp.topProp;
        adjustMinimumPressureToMaximum();
        cappiHeightLimitsProp.topProp = float(pressure2metre_standardICAO(cappiMinimumPressure * 100));
        emitActorChangedSignal();
    });
    cappiGroupProp.addSubProperty(cappiPressureLimitsProp.groupProp);

    // Height in km.
    auto minHeight = float(pressure2metre_standardICAO(cappiMaximumPressure * 100));
    auto maxHeight = float(pressure2metre_standardICAO(cappiMinimumPressure * 100));
    cappiHeightLimitsProp = MPropertyTemplates::VerticalExtentM(minHeight, maxHeight);
    cappiHeightLimitsProp.groupProp.setHidden(cappiSingleAltitudeBool || cappiMode != CappiMode::Height);
    cappiHeightLimitsProp.bottomProp.setConfigKey("min_height");
    cappiHeightLimitsProp.topProp.setConfigKey("max_height");
    cappiHeightLimitsProp.bottomProp.registerValueCallback([=]()
    {
        cappiMaximumPressure = float(metre2pressure_standardICAO(cappiHeightLimitsProp.bottomProp) / 100);
        cappiPressureLimitsProp.bottomProp = cappiMaximumPressure;

        adjustMinimumPressureToMaximum();
        emitActorChangedSignal();
    });
    cappiHeightLimitsProp.topProp.registerValueCallback([=]()
    {
        cappiMinimumPressure = float(metre2pressure_standardICAO(cappiHeightLimitsProp.topProp) / 100);
        cappiPressureLimitsProp.topProp = cappiMinimumPressure;

        adjustMinimumPressureToMaximum();
        emitActorChangedSignal();
    });
    cappiGroupProp.addSubProperty(cappiHeightLimitsProp.groupProp);

    // Grid properties.
    graticuleEnabledProp = MBoolProperty("Graticule", false);
    graticuleEnabledProp.setConfigKey("graticule_enabled");
    graticuleEnabledProp.setTooltip("Draw lines indicating the borders of the radar data, with adjustable step sizes.");
    graticuleEnabledProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(graticuleEnabledProp);

    graticuleColourProp = MColorProperty("Colour", Qt::black);
    graticuleColourProp.setConfigKey("colour");
    graticuleColourProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    graticuleEnabledProp.addSubProperty(graticuleColourProp);

    graticuleThicknessProp = MFloatProperty("Thickness", 1.0f);
    graticuleThicknessProp.setConfigKey("thickness");
    graticuleThicknessProp.setMinMax(0.0f, 10.0f);
    graticuleThicknessProp.setDecimals(1);
    graticuleThicknessProp.setStep(1.0f);
    graticuleThicknessProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    graticuleEnabledProp.addSubProperty(graticuleThicknessProp);

    graticuleAzimuthStepProp = MIntProperty("Azimuth step between lines", 15);
    graticuleAzimuthStepProp.setConfigKey("azimuth_step_between_lines");
    graticuleAzimuthStepProp.setMinMax(1, 360);
    graticuleAzimuthStepProp.setSuffix("°");
    graticuleAzimuthStepProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    graticuleEnabledProp.addSubProperty(graticuleAzimuthStepProp);

    graticuleRangeStepProp = MIntProperty("Range step between rings", 30000);
    graticuleRangeStepProp.setConfigKey("range_step_between_rings");
    graticuleRangeStepProp.setMinMax(250, 250000);
    graticuleRangeStepProp.setStep(250);
    graticuleRangeStepProp.setSuffix(" m");
    graticuleRangeStepProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    graticuleEnabledProp.addSubProperty(graticuleRangeStepProp);
}


MRadarActor::~MRadarActor() = default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MRadarActor::reloadShaderEffects()
{
    LOG4CPLUS_DEBUG(mlog, "loading shader programs");

    beginCompileShaders(2);

    compileShadersFromFileWithProgressDialog(
        glRadarShader, "src/glsl/radar_pseudocolour.fx.glsl");
    compileShadersFromFileWithProgressDialog(
        glGraticuleShader, "src/glsl/radar_graticule.fx.glsl");
    endCompileShaders();
}

void MRadarActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MNWPMultiVarActor::loadConfigurationPrior_V_1_14(settings);

    settings->beginGroup(MRadarActor::getSettingsID());

    // Variable settings.
    // ==============
    QStringList varNameList;
    for (MNWPActorVariable *v : variables)
    {
        varNameList << v->variableName;
        LOG4CPLUS_DEBUG(mlog, v->variableName.toStdString());
    }
    int variableIndex = settings->value("variableIndex", 0).toInt();
    variableProp = variables.empty() ? nullptr : variables.at(variableIndex);

    // General render settings.
    // ==============
    renderMode = static_cast<RenderMode>(
        settings->value("renderMode", int(renderMode)).toInt());

    colourUndetectEnabledProp =
        settings->value("colourUndetectEnabled", colourUndetectEnabledProp.value())
            .toBool();

    undetectColourProp =
        settings->value("undetectColour", undetectColourProp.value()).value<QColor>();

    // Elevation properties.
    // ==============
    showOnlyOneElevationProp =
        settings->value("showOnlyOneElevation", showOnlyOneElevationProp.value()).toBool();

    minElevationProp = settings->value("minEl", minEl).toInt();
    maxElevationProp = settings->value("maxEl", maxEl).toInt();
    singleElevationProp = minEl;

    minElevationProp.setEnabled(!showOnlyOneElevationProp);
    maxElevationProp.setEnabled(!showOnlyOneElevationProp);
    singleElevationProp.setEnabled(showOnlyOneElevationProp);

    // Azimuth properties.
    // ==============
    minAzimuthProp.setEnabled(renderMode != RenderMode::verticalSection);
    minAzimuthProp = settings->value("minAz", 0).toFloat();

    maxAzimuthProp.setEnabled(renderMode != RenderMode::verticalSection);
    maxAzimuthProp = settings->value("maxAz", 360).toFloat();

    vSectionAzProp.setEnabled(renderMode == RenderMode::verticalSection);
    vSectionAzProp = settings->value("vSectionAz", vSectionAzProp.value()).toFloat();

    // CAPPI/horizontal section properties.
    // ==============
    enableActorUpdates(false); //  Suppress new calculations of pressures/heights.
    cappiModeProp = settings->value("cappiMode", int(cappiMode)).toInt();

    // Single altitude.
    cappiSingleAltitudeBool =
        settings->value("cappiSingleAltitudeBool", cappiSingleAltitudeBoolProp.value())
            .toBool();
    cappiSingleAltitudeBoolProp.setEnabled(
        renderMode != RenderMode::horizontalSection);

    cappiSinglePressureProp =
        settings->value("cappiSinglePressure", cappiSinglePressure).toFloat();
    cappiSinglePressureProp.setEnabled(
        cappiSingleAltitudeBool && cappiMode == CappiMode::Pressure);

    cappiSingleHeightProp = pressure2metre_standardICAO(cappiSinglePressure * 100);
    cappiSingleHeightProp.setEnabled(cappiSingleAltitudeBool
                                          && cappiMode == CappiMode::Height);

    // Altitude range. (height and pressure)
    cappiMinimumPressure =
        settings->value("cappiMinimumPressure", cappiMinimumPressure).toFloat();
    cappiMaximumPressure =
        settings->value("cappiMaximumPressure", cappiMaximumPressure).toFloat();

    cappiHeightLimitsProp.bottomProp = pressure2metre_standardICAO(cappiMaximumPressure * 100);
    cappiHeightLimitsProp.topProp = pressure2metre_standardICAO(cappiMinimumPressure * 100);
    updateAltitudePropertiesEnabledStatus();
    enableActorUpdates(true);

    // Graticule properties.
    // =====================
    graticuleEnabledProp =
        settings->value("graticuleEnabled", graticuleEnabledProp.value()).toBool();
    graticuleColourProp =
        settings->value("graticuleColour", graticuleColourProp.value()).value<QColor>();
    graticuleThicknessProp =
        settings->value("graticuleThickness", graticuleThicknessProp.value()).toFloat();
    graticuleAzimuthStepProp =
        settings->value("graticuleAzimuthStep", graticuleAzimuthStepProp.value()).toInt();
    graticuleRangeStepProp =
        settings->value("graticuleRangeStep", graticuleRangeStepProp.value()).toInt();

    settings->endGroup();
}


QList<MVerticalLevelType> MRadarActor::supportedLevelTypes()
{
    return (QList<MVerticalLevelType>() << RADAR_LEVELS_3D);
}


MNWPActorVariable *
MRadarActor::createActorVariable(const MSelectableDataVariable &dataSource)
{
    auto *newVar = new MNWPActorVariable(this);

    newVar->dataSourceID = dataSource.dataSourceID;
    newVar->levelType = dataSource.levelType;
    newVar->variableName = dataSource.variableName;

    return newVar;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MRadarActor::initializeActorResources()
{
    // Parent initialisation (triggers loading of initial data fields).
    MNWPMultiVarActor::initializeActorResources();

    // Variable initialization
    if (!variables.empty())
    {
        variableProp = variables.at(0);
    }

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    bool loadShaders = false;
    loadShaders |=
        glRM->generateEffectProgram("radar_pseudocolor", glRadarShader);
    loadShaders |=
        glRM->generateEffectProgram("radar_graticule", glGraticuleShader);
    if (loadShaders)
    {
        reloadShaderEffects();
    }
}


void MRadarActor::renderToCurrentContext(MSceneViewGLWidget *sceneView)
{
    if (variables.empty() || variableProp.value() == nullptr)
    {
        return;
    }

    // Only render if variable and its grid isn't empty and has a transfer function.
    if (variableProp->radarGrid == nullptr || variableProp->transferFunction == nullptr
        || !variableProp->hasData())
    {
        return;
    }

    if (renderMode == RenderMode::verticalSection)
    {
        renderVerticalSection(sceneView, variableProp);
        if (graticuleEnabledProp)
        {
            renderGraticule(sceneView, variableProp);
        }
    }
    else if (renderMode == RenderMode::horizontalSection)
    {
        renderHorizontalSection(sceneView, variableProp); // data
    }
    else if (renderMode == RenderMode::pseudoColor2D
             || renderMode == RenderMode::pseudoColor3D)
    {
        renderRadarVolume(sceneView, variableProp);
        if (graticuleEnabledProp)
        {
            renderGraticule(sceneView, variableProp);
        }
    }
}


void MRadarActor::dataFieldChangedEvent()
{
    emitActorChangedSignal();
}


void MRadarActor::onDeleteActorVariable(MNWPActorVariable *var)
{
    variableProp.removeVariable(var);
}


void MRadarActor::onAddActorVariable(MNWPActorVariable *var)
{
    variableProp.addVariable(var);
}


void MRadarActor::onChangeActorVariable(MNWPActorVariable *var)
{
    variableProp.changeVariable(var);
}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MRadarActor::renderRadarVolume(MSceneViewGLWidget *sceneView,
                                    MNWPActorVariable *var)
{
    if (renderMode == RenderMode::pseudoColor3D)
    {
        glRadarShader->bindProgram("PseudoColours3D");
        CHECK_GL_ERROR;
    }
    else
    {
        glRadarShader->bindProgram("PseudoColours2D");
        CHECK_GL_ERROR;
        glRadarShader->setUniformValue("isShadow", false);
        CHECK_GL_ERROR;
        glRadarShader->setUniformValue("shadowWorldZ", -1.0);
        CHECK_GL_ERROR;
    }
    glRadarShader->setUniformValue("undetectColour", undetectColourProp);
    CHECK_GL_ERROR;

    if (!setUniformValuesColourmapping(sceneView, var))
    {
        return;
    }

    // Azimuth angle interval.
    glRadarShader->setUniformValue("minAz", minAzimuthProp);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("maxAz", maxAzimuthProp);
    CHECK_GL_ERROR;

    // Altitude interval.
    glRadarShader->setUniformValue("selectAltitude", true);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("max_hPa", cappiMaximumPressure);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("min_hPa", cappiMinimumPressure);
    CHECK_GL_ERROR;

    // Render each elevation.
    for (const uint &elIdx : var->radarGrid->getElevationIndexList())
    {
        if (elIdx > maxEl)
        {
            break;
        }
        if (elIdx < minEl)
        {
            continue;
        }

        glRadarShader->setUniformValue("elIdx", elIdx);
        CHECK_GL_ERROR;

        // Texture bindings for 2D variable data.
        var->textureRadarDataMap[elIdx]->bindToTextureUnit(
            var->textureUnitRadarDataMap);
        CHECK_GL_ERROR;
        glRadarShader->setUniformValue("dataField2D",
                                       var->textureUnitRadarDataMap);
        CHECK_GL_ERROR;

        int nbins = var->radarGrid->getNbins(elIdx);
        int nrays = var->radarGrid->getNrays(elIdx);

        glEnable(GL_POLYGON_OFFSET_FILL);
        CHECK_GL_ERROR;
        glPolygonOffset(.8f, 1.0f);
        CHECK_GL_ERROR;
        glPolygonMode(GL_FRONT_AND_BACK, renderAsWireFrameProp ? GL_LINE : GL_FILL);
        CHECK_GL_ERROR;
        sceneView->setLineWidth(1);
        CHECK_GL_ERROR;
        glDrawArraysInstanced(GL_POINTS, 0, nrays, nbins);
        CHECK_GL_ERROR;
        glDisable(GL_POLYGON_OFFSET_FILL);
    }
}


void MRadarActor::renderShadowElevation(MSceneViewGLWidget *sceneView,
                                        MNWPActorVariable *var,
                                        bool fullDataRange)
{
    glRadarShader->bindProgram("ShadowElevation");
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("shadowForFullDataRange", fullDataRange);
    CHECK_GL_ERROR;

    if (!setUniformValuesColourmapping(sceneView, var))
    {
        return;
    }

    // Azimuth angle interval.
    glRadarShader->setUniformValue("minAz", minAzimuthProp);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("maxAz", maxAzimuthProp);
    CHECK_GL_ERROR;

    // Altitude interval.
    glRadarShader->setUniformValue("selectAltitude", !fullDataRange);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("max_hPa", cappiMaximumPressure);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("min_hPa", cappiMinimumPressure);
    CHECK_GL_ERROR;

    for (const uint &elIdx : var->radarGrid->getElevationIndexList())
    {
        if (fullDataRange)
        {
            // To show the full data range, draw only the horizontally largest elevation.
            if (elIdx != var->radarGrid->getLargestHorizontalExtentElevationIndex())
            {
                continue;
            }
        }
        else
        {
            if (elIdx > maxEl)
            {
                break;
            }
            else if (elIdx < minEl)
            {
                continue;
            }
        }

        glRadarShader->setUniformValue("elIdx", elIdx);
        CHECK_GL_ERROR;

        // Texture bindings for 2D variable data.
        var->textureRadarDataMap[elIdx]->bindToTextureUnit(
            var->textureUnitRadarDataMap);
        CHECK_GL_ERROR;
        glRadarShader->setUniformValue("dataField2D",
                                       var->textureUnitRadarDataMap);
        CHECK_GL_ERROR;

        int nbins = var->radarGrid->getNbins(elIdx);
        int nrays = var->radarGrid->getNrays(elIdx);

        glEnable(GL_POLYGON_OFFSET_FILL);
        CHECK_GL_ERROR;
        if (!fullDataRange)
        {
            // Different offsets to avoid z-fighting between elevation shadows.
            glPolygonOffset(0.0f, -1.0f * float(11 - elIdx));
            CHECK_GL_ERROR;
        }

        glPolygonMode(GL_FRONT_AND_BACK, renderAsWireFrameProp ? GL_LINE : GL_FILL);
        CHECK_GL_ERROR;
        sceneView->setLineWidth(1);
        CHECK_GL_ERROR;
        glDrawArraysInstanced(GL_POINTS, 0, nrays, nbins);
        CHECK_GL_ERROR;
        glDisable(GL_POLYGON_OFFSET_FILL);
    }
}


bool MRadarActor::setUniformValuesColourmapping(MSceneViewGLWidget *sceneView,
                                                MNWPActorVariable *var)
{
    // Model-view-projection matrix from the current scene view.
    glRadarShader->setUniformValue(
        "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("pToWorldZParams",
                                   sceneView->pressureToWorldZParameters());
    CHECK_GL_ERROR;

    // Transfer function and colour settings.
    if (!var->transferFunction)
    {
        return false;
    }

    var->transferFunction->getTexture()->bindToTextureUnit(
        var->textureUnitTransferFunction);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("transferFunction",
                                   var->textureUnitTransferFunction);
    CHECK_GL_ERROR;

    glRadarShader->setUniformValue("scalarMinimum",
                                   var->transferFunction->getMinimumValue());
    CHECK_GL_ERROR;
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("scalarMaximum",
                                   var->transferFunction->getMaximumValue());
    CHECK_GL_ERROR;
    CHECK_GL_ERROR;

    // set the colour for cells without detected radar signal.
    glRadarShader->setUniformValue("colourUndetectEnabled",
                                   colourUndetectEnabledProp);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("undetectValue",
                                   var->radarGrid->getUndetectValue());
    CHECK_GL_ERROR;


    // Bind radar metadata for all elevations.
    var->radarMetadataBuffer->bindToIndex(9);
    CHECK_GL_ERROR;

    glRadarShader->setUniformValue(
        "lonLatHeightRadar", var->radarGrid->getRadarStationLonLatHeight());
    CHECK_GL_ERROR;

    return true;
}


void MRadarActor::setUniformValuesGraticule(MSceneViewGLWidget *sceneView,
                                            MNWPActorVariable *var)
{
    // Model-view-projection matrix from the current scene view.
    glGraticuleShader->setUniformValue(
        "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    CHECK_GL_ERROR;
    glGraticuleShader->setUniformValue("pToWorldZParams",
                                   sceneView->pressureToWorldZParameters());
    CHECK_GL_ERROR;

    // Graticule specific.
    glGraticuleShader->setUniformValue("graticuleColour", graticuleColourProp);
    CHECK_GL_ERROR;

    // Bind radar metadata for all elevations.
    var->radarMetadataBuffer->bindToIndex(9);
    CHECK_GL_ERROR;

    glGraticuleShader->setUniformValue(
        "lonLatHeightRadar", var->radarGrid->getRadarStationLonLatHeight());
    CHECK_GL_ERROR;

    //    glPseudoColourShader->setUniformValue("minAz", minAz); CHECK_GL_ERROR;
    //    glPseudoColourShader->setUniformValue("maxAz", maxAz); CHECK_GL_ERROR;
}


void MRadarActor::renderVerticalSection(MSceneViewGLWidget *sceneView,
                                        MNWPActorVariable *var)
{
    // Settings to render the data.
    glRadarShader->bindProgram("PseudoColoursVerticalSection");
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("undetectColour", undetectColourProp);
    CHECK_GL_ERROR;

    if (!setUniformValuesColourmapping(sceneView, var))
    {
        return;
    }

    // Height interval.
    glRadarShader->setUniformValue("max_hPa", cappiMaximumPressure);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("min_hPa", cappiMinimumPressure);
    CHECK_GL_ERROR;

    glRadarShader->setUniformValue("vSectionAz", vSectionAzProp);
    CHECK_GL_ERROR;

    // Iterate over all selected elevations.
    for (const uint &elIdx : var->radarGrid->getElevationIndexList())
    {
        if (elIdx > maxEl)
        {
            break;
        }
        if (elIdx < minEl)
        {
            continue;
        }

        glRadarShader->setUniformValue("elIdx", elIdx);
        CHECK_GL_ERROR;

        int nbins = var->radarGrid->getNbins(elIdx);

        // Texture bindings for 2D variable data.
        var->textureRadarDataMap[elIdx]->bindToTextureUnit(
            var->textureUnitRadarDataMap);
        CHECK_GL_ERROR;
        glRadarShader->setUniformValue("dataField2D",
                                       var->textureUnitRadarDataMap);
        CHECK_GL_ERROR;

        glPolygonOffset(.8f, 1.0f);
        CHECK_GL_ERROR;
        glEnable(GL_POLYGON_OFFSET_FILL);
        CHECK_GL_ERROR;
        glPolygonMode(GL_FRONT_AND_BACK, renderAsWireFrameProp ? GL_LINE : GL_FILL);
        CHECK_GL_ERROR;
        sceneView->setLineWidth(5);
        CHECK_GL_ERROR;
        glDrawArraysInstanced(GL_POINTS, 0, 1, nbins);
        CHECK_GL_ERROR;
        glDisable(GL_POLYGON_OFFSET_FILL);
    }
}


void MRadarActor::renderHorizontalSection(MSceneViewGLWidget *sceneView,
                                          MNWPActorVariable *var)
{
    glRadarShader->bindProgram("PseudoColoursHorizontalSection");
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("undetectColour", undetectColourProp);
    CHECK_GL_ERROR;
    CHECK_GL_ERROR;

    // Model-view-projection matrix from the current scene view.
    glRadarShader->setUniformValue(
        "mvpMatrix", *(sceneView->getModelViewProjectionMatrix()));
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("pToWorldZParams",
                                   sceneView->pressureToWorldZParameters());
    CHECK_GL_ERROR;

    // Transfer function.
    glRadarShader->setUniformValue("scalarMinimum",
                                   var->transferFunction->getMinimumValue());
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("scalarMaximum",
                                   var->transferFunction->getMaximumValue());
    CHECK_GL_ERROR;

    // Azimuth angle interval.
    glRadarShader->setUniformValue("minAz", minAzimuthProp);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("maxAz", maxAzimuthProp);
    CHECK_GL_ERROR;

    // Texture bindings for transfer function for data field (1D texture from
    // transfer function class).
    var->transferFunction->getTexture()->bindToTextureUnit(
        var->textureUnitTransferFunction);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("transferFunction",
                                   var->textureUnitTransferFunction);
    CHECK_GL_ERROR;

    // set the colour for cells without detected radar signal.
    glRadarShader->setUniformValue("colourUndetectEnabled",
                                   colourUndetectEnabledProp);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("undetectValue",
                                   var->radarGrid->getUndetectValue());
    CHECK_GL_ERROR;

    // Texture bindings for Lat/Lon axes (3D textures).
    var->textureHSectionRadarLonLats->bindToTextureUnit(
        var->textureUnitHSectionRadarLonLats);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("lonLatPresField",
                                   var->textureUnitHSectionRadarLonLats);
    CHECK_GL_ERROR;

    // Texture bindings for 2D variable data.
    var->textureHSectionRadarData->bindToTextureUnit(var->textureUnitRadarDataMap);
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("dataField2D", var->textureUnitRadarDataMap);
    CHECK_GL_ERROR;

    glRadarShader->setUniformValue("nbinsH",
                                   var->radarGrid->getNumberHorizontalBins());
    CHECK_GL_ERROR;
    glRadarShader->setUniformValue("nraysH", var->radarGrid->getNrays());
    CHECK_GL_ERROR;

    glEnable(GL_POLYGON_OFFSET_FILL);

    glPolygonMode(GL_FRONT_AND_BACK, renderAsWireFrameProp ? GL_LINE : GL_FILL);
    CHECK_GL_ERROR;
    sceneView->setLineWidth(1);
    CHECK_GL_ERROR;
    glDrawArraysInstanced(GL_POINTS, 0, var->radarGrid->getNrays(),
                          var->radarGrid->getNumberHorizontalBins());
    CHECK_GL_ERROR;
    glDisable(GL_POLYGON_OFFSET_FILL);
}


void MRadarActor::renderGraticule(MSceneViewGLWidget *sceneView,
                                  MNWPActorVariable *var)
{
    // Graticule beam lines.
    renderGraticuleBeams(sceneView, var);

    // Graticule rings.
    if (renderMode != RenderMode::verticalSection)
    {
        renderGraticuleCircles(sceneView, var);
    }


    // Vertical grid cell edges.
    if (showVolumeThickness)
    {
        renderGraticuleBinLines(sceneView, var);
    }
}


void MRadarActor::renderGraticuleBeams(MSceneViewGLWidget *sceneView,
                                       MNWPActorVariable *var)
{
    glGraticuleShader->bindProgram("GraticuleBeams");
    CHECK_GL_ERROR;

    setUniformValuesGraticule(sceneView, var);

    glGraticuleShader->setUniformValue("showVolumeThickness", showVolumeThickness);
    CHECK_GL_ERROR;

    // graticule specific.
    glGraticuleShader->setUniformValue("graticuleAzimuthStep",
                                       graticuleAzimuthStepProp);
    CHECK_GL_ERROR;

    // Vertical section angle.
    float vSectionAzAngle =
            (renderMode == RenderMode::verticalSection) ? this->vSectionAzProp : -1.0;
    glGraticuleShader->setUniformValue("vSectionAz", vSectionAzAngle);

    // Azimuth angle interval.
    glGraticuleShader->setUniformValue("minAz", minAzimuthProp);
    CHECK_GL_ERROR;
    glGraticuleShader->setUniformValue("maxAz", maxAzimuthProp);
    CHECK_GL_ERROR;

    for (const uint &elIdx : var->radarGrid->getElevationIndexList())
    {
        if (elIdx > maxEl)
        {
            break;
        }
        if (elIdx < minEl)
        {
            continue;
        }

        glGraticuleShader->setUniformValue("elIdx", elIdx);
        CHECK_GL_ERROR;

        int nbins = var->radarGrid->getNbins(elIdx);
        int nrays = var->radarGrid->getNrays(elIdx);

        sceneView->setLineWidth(graticuleThicknessProp);
        CHECK_GL_ERROR;
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        CHECK_GL_ERROR;

        // If ray vertical thickness is shown in 3D, render lines on both upper
        // and lower side of the grid cells.
        int numRayLines = (renderMode == RenderMode::verticalSection)
                              ? 1
                              : (nrays / graticuleAzimuthStepProp);
        glGraticuleShader->setUniformValue("numRayLines", numRayLines);
        CHECK_GL_ERROR;
        glDrawArraysInstanced(GL_LINE_STRIP, 0, nbins + 1,
                              (showVolumeThickness) ? numRayLines * 2
                                                    : numRayLines);
        CHECK_GL_ERROR;
    }
}


void MRadarActor::renderGraticuleCircles(MSceneViewGLWidget *sceneView,
                                         MNWPActorVariable *var)
{
    // Graticule concentric circles.
    glGraticuleShader->bindProgram("GraticuleRings");
    CHECK_GL_ERROR;

    setUniformValuesGraticule(sceneView, var);

    glGraticuleShader->setUniformValue("showVolumeThickness", showVolumeThickness);
    CHECK_GL_ERROR;

    // Step size between circles.
    glGraticuleShader->setUniformValue("graticuleRangeStep", graticuleRangeStepProp);
    CHECK_GL_ERROR;

    // Azimuth angle interval.
    glGraticuleShader->setUniformValue("minAz", minAzimuthProp);
    CHECK_GL_ERROR;
    glGraticuleShader->setUniformValue("maxAz", maxAzimuthProp);
    CHECK_GL_ERROR;

    for (const uint &elIdx : var->radarGrid->getElevationIndexList())
    {
        if (elIdx > maxEl)
        {
            break;
        }
        if (elIdx < minEl)
        {
            continue;
        }

        glGraticuleShader->setUniformValue("elIdx", elIdx);
        CHECK_GL_ERROR;

        int nbins = var->radarGrid->getNbins(elIdx);
        int nrays = var->radarGrid->getNrays(elIdx);
        float binDist = var->radarGrid->getBinDistance(elIdx);

        // Azimuth step size (usually 1.0).
        float deltaAz = 360.0/float(nrays);
        glGraticuleShader->setUniformValue("deltaAz", deltaAz);
        CHECK_GL_ERROR;

        sceneView->setLineWidth(graticuleThicknessProp);
        CHECK_GL_ERROR;
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        CHECK_GL_ERROR;

        // Number of circles to draw. Extra circle for outer boundary.
        int ncircles =
            int(std::ceil(float(nbins) * binDist / float(graticuleRangeStepProp)))
            + 1;
        glGraticuleShader->setUniformValue("ncircles", ncircles);
        CHECK_GL_ERROR;

        // If ray vertical thickness is shown in 3D, render circles on both upper and lower side of the grid cells.
        glDrawArraysInstanced(GL_LINE_STRIP, 0, nrays + 1,
                              (showVolumeThickness) ? ncircles * 2 : ncircles);
        CHECK_GL_ERROR;
    }
}


void MRadarActor::renderGraticuleBinLines(MSceneViewGLWidget *sceneView,
                                          MNWPActorVariable *var)
{
    glGraticuleShader->bindProgram("GraticuleBinLines");
    CHECK_GL_ERROR;

    setUniformValuesGraticule(sceneView, var);

    // Graticule specific.
    glGraticuleShader->setUniformValue("graticuleRangeStep", graticuleRangeStepProp);
    CHECK_GL_ERROR;
    glGraticuleShader->setUniformValue("graticuleAzimuthStep",
                                       graticuleAzimuthStepProp);
    CHECK_GL_ERROR;

    float vSectionAzAngle =
            (renderMode == RenderMode::verticalSection) ? this->vSectionAzProp : -1.0;
    glGraticuleShader->setUniformValue("vSectionAz", vSectionAzAngle);

    // Azimuth angle interval.
    glGraticuleShader->setUniformValue("minAz", minAzimuthProp);
    CHECK_GL_ERROR;
    glGraticuleShader->setUniformValue("maxAz", maxAzimuthProp);
    CHECK_GL_ERROR;

    for (const uint &elIdx : var->radarGrid->getElevationIndexList())
    {
        if (elIdx > maxEl)
        {
            break;
        }
        if (elIdx < minEl)
        {
            continue;
        }

        glGraticuleShader->setUniformValue("elIdx", elIdx);
        CHECK_GL_ERROR;

        int nbins = var->radarGrid->getNbins(elIdx);
        int nrays = var->radarGrid->getNrays(elIdx);
        float binDist = var->radarGrid->getBinDistance(elIdx);

        // Number of vertical lines is the same as the number of circles to draw. Extra for outer boundary.
        int ncircles =
            int(std::ceil(float(nbins) * binDist / float(graticuleRangeStepProp)))
            + 1;
        glGraticuleShader->setUniformValue("ncircles", ncircles);
        CHECK_GL_ERROR;

        int numRayLines = (renderMode == RenderMode::verticalSection)
                              ? 1
                              : (nrays / graticuleAzimuthStepProp);

        sceneView->setLineWidth(graticuleThicknessProp);
        CHECK_GL_ERROR;
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        CHECK_GL_ERROR;
        glDrawArraysInstanced(GL_POINTS, 0, ncircles, numRayLines);
        CHECK_GL_ERROR;
    }
}


void MRadarActor::updateAltitudePropertiesEnabledStatus()
{
    bool isPressureMode = (cappiMode == CappiMode::Pressure);
    enableActorUpdates(false);
    // Min/max pressure properties.
    cappiPressureLimitsProp.groupProp.setHidden(cappiSingleAltitudeBool || !isPressureMode);

    // Min/max height properties.
    cappiHeightLimitsProp.groupProp.setHidden(cappiSingleAltitudeBool || isPressureMode);

    // Single pressure/height properties.
    cappiSinglePressureProp.setHidden(!cappiSingleAltitudeBool || !isPressureMode);
    cappiSingleHeightProp.setHidden(!cappiSingleAltitudeBool || isPressureMode);
    enableActorUpdates(true);
}


void MRadarActor::adjustMaximumPressureToMinimum()
{
    if (cappiMaximumPressure < cappiMinimumPressure)
    {
        cappiMaximumPressure = cappiMinimumPressure;
        cappiPressureLimitsProp.bottomProp = cappiMaximumPressure;

        float cappiMinimumHeight =
            float(pressure2metre_standardICAO(cappiMaximumPressure * 100));
        cappiHeightLimitsProp.bottomProp = cappiMinimumHeight;
    }
}


void MRadarActor::adjustMinimumPressureToMaximum()
{
    if (cappiMaximumPressure < cappiMinimumPressure)
    {
        cappiMinimumPressure = cappiMaximumPressure;
        cappiPressureLimitsProp.topProp = cappiMinimumPressure;
        float cappiMaximumHeight = float(
            pressure2metre_standardICAO(cappiMinimumPressure * 100));
        cappiHeightLimitsProp.topProp = cappiMaximumHeight;
    }
}


void MRadarActor::updateHorizontalSection()
{
    float cappiSingleHeight =
        pressure2metre_standardICAO(cappiSinglePressure * 100);

    for (MNWPActorVariable *v : variables)
    {
        if (v->radarGrid == nullptr) continue;

        v->setSectionHeight(cappiSingleHeight);
        if (v->radarGrid->areAzimuthAnglesInterpolated())
        {
            v->generateHorizontalSectionGeometry();
        }
    }
}


} // namespace Met3D
