/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MRADARDATASOURCE_H
#define MRADARDATASOURCE_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/weatherpredictiondatasource.h"

namespace Met3D
{

/**
  @brief MRadarDataSource is the abstract base class for radar
  data sources (e.g. radar data reader).
  The class defines the interface common to all classes
  that provide radar data.
  */
class MRadarDataSource : public MWeatherPredictionDataSource
{
public:
    MRadarDataSource();

    /**
     * @brief returnsRadarGrid
     * @return True if the data source is radar data source.
     */
    virtual const bool returnsRadarGrid() const override { return true; }
};

} // namespace Met3D

#endif // MRADARDATASOURCE_H
