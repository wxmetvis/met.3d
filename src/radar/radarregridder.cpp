/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "radarregridder.h"

// standard library imports
#include <cassert>
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "radar/radargrid.h"
#include "util/metroutines.h"
#include "util/mexception.h"
#include "util/mutil.h"

#include <QString>

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MRadarRegridder::MRadarRegridder()
    : MWeatherPredictionDataSource(),
      inputSource(nullptr)
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MRadarRegridder::setInputSource(MRadarDataSource *s)
{
    inputSource = s;
    registerInputSource(inputSource);
    enablePassThrough(s);
}

MStructuredGrid *MRadarRegridder::produceData(MDataRequest request)
{
    assert(inputSource != nullptr);

    // request radar grid
    MDataRequestHelper rh(request);
    MStructuredGrid *regriddedGrid = nullptr;
    MRadarGrid *inputGrid = this->inputSource->getRadarData(rh.request());

    assert(inputGrid != nullptr);

    // Parse request
    QString regridLevels = rh.value("RADAR_REGRID_LEVELS");
    QString lonLatDistance = rh.value("RADAR_REGRID_LONLAT_DISTANCE");
    bool hasTopologyChanged =
        rh.value("RADAR_REGRIDDER_TOPOLOGY_HAS_CHANGED").toInt();

    // Format of lonLatDistance is "NUM_POINTS/<numLonPoints>/<numLatPoints>"
    // or "DELTA/<deltaLonPoints>/<deltaLatPoints>". Extract the elements:
    QStringList lonLatDistanceStrings = lonLatDistance.split("/");
    // Way to set size of regridded grid (NUM_POINTS or DELTA):
    const QString &lonLatDistanceType = lonLatDistanceStrings.at(0);
    // Number of longitude points or delta lon in meters:
    const QString &lonDistanceValueString = lonLatDistanceStrings.at(1);
    // Number of latitude points or delta lat in meters:
    const QString &latDistanceValueString = lonLatDistanceStrings.at(2);

    // regridLevelMode starts with level type, separated by '/' from the levels.
    QStringList levelTypeStrings = regridLevels.split("/");
    QString levelTypeDefinition = levelTypeStrings.at(0);

    // New grid dimensions.
    uint nlats, nlons, nlevs;

    // User selected pressure level modes:
    // EDPL = Equidistant Pressure Levels;
    // EDHL = Equidistant Height Levels;
    // UD = User Defined.
    if (levelTypeDefinition == "EDPL" || levelTypeDefinition == "EDHL")
    {
        nlevs = levelTypeStrings[1].toInt();
    }
    else // levelTypeDefinition == "UD"
    {
        nlevs = levelTypeStrings.size() - 1; // First string is level type.

        if (nlevs == 0 || nlevs == 1)
        {
            LOG4CPLUS_ERROR(mlog, "Entered list of pressure levels contains "
                            "less than two levels. Returning nullpointer.");
            return regriddedGrid;
        }
    }

    // Radar site position.
    float posLon = inputGrid->getRadarStationLon();
    float posLat = inputGrid->getRadarStationLat();

    // Extreme points of the radar grid.
    QVector3D maxNorth = inputGrid->getMostNorthernPointLonLatMetre();
    QVector3D maxSouth = inputGrid->getMostSouthernPointLonLatMetre();
    QVector3D maxEast = inputGrid->getMostEasternPointLonLatMetre();
    QVector3D maxWest = inputGrid->getMostWesternPointLonLatMetre();

    // Calculate width of the data in degrees.
    float maxDistLatDeg = maxNorth.y() - maxSouth.y();
    float maxDistLonDeg = maxEast.x() - maxWest.x();
    // The width of the data is increased by a small factor to make sure all
    // data is contained and the first and last point are outside the grid.
    float factor = 1.03;
    float widthLatDeg = maxDistLatDeg * factor;
    float widthLonDeg = maxDistLonDeg * factor;

    // The step sizes in longitude and latitude direction.
    float deltaLonDegree, deltaLatDegree;

    // User selection "Lon/Lat distance" between "number of points" and
    // "delta between points"
    if (lonLatDistanceType == "NUM_POINTS")
    {
        // Size of grid is determined by number of grid points.
        nlons = lonDistanceValueString.toInt();
        nlats = latDistanceValueString.toInt();

        // The number of steps between grid points is one smaller than the number
        // of grid points.
        deltaLonDegree = float(widthLonDeg) / float(nlons - 1);
        deltaLatDegree = float(widthLatDeg) / float(nlats - 1);
    }
    else // lonLatDistanceType == "DELTA"
    {
        // Size of grid is determined by step size between new grid points in meters.
        double deltaLonMetre = lonDistanceValueString.toDouble();
        double deltaLatMetre = latDistanceValueString.toDouble();

        // Step size between two grid points is converted to degrees.
        deltaLonDegree = longitudeLengthMeterToDegree(deltaLonMetre, posLat);
        deltaLatDegree = latitudeLengthMeterToDegree(deltaLatMetre);

        // The width is generally not divisible by the step size without a
        // remainder. It is rounded up so that all radar data is included.
        // The number of grid points is one larger than the number of steps between them.
        nlons = ceil(widthLonDeg / deltaLonDegree) + 1;
        nlats = ceil(widthLatDeg / deltaLatDegree) + 1;

        // The new width is a multiple of the step size.
        widthLonDeg = deltaLonDegree * (nlons - 1);
        widthLatDeg = deltaLatDegree * (nlats - 1);
    }

    // The structured grid this function returns.
    MRegularLonLatStructuredPressureGrid *result =
        new MRegularLonLatStructuredPressureGrid(nlevs, nlats, nlons);

    // Store longitude and latitude, starting at northwestern corner of the data.
    if (nlons > 1)
    {
        result->lons[0] = posLon - widthLonDeg / 2.0;
    }
    else
    {
        result->lons[0] = posLon;
    }

    if (nlats > 1)
    {
        result->lats[0] = posLat + widthLatDeg / 2.0;
    }
    else
    {
        result->lats[0] = posLat;
    }


    // Set latitude and longitude grid point positions, starting in the north-west corner.
    for (uint i = 1; i < result->nlons; i++)
    {
        result->lons[i] = result->lons[i - 1] + deltaLonDegree;
    }
    // Latitudes need to be defined so that lats[0] is the largest and
    // lats[nlats-1] is the smallest (see raycaster).
    for (uint j = 1; j < result->nlats; j++)
    {
        result->lats[j] = result->lats[j - 1] - deltaLatDegree;
    }

    // uniform pressure distance
    if (levelTypeDefinition == "EDPL") // EDPL = Equidistant Pressure Levels
    {
        // Lowest level (highest pressure).
        double radarPressure_hPa =
            metre2pressure_standardICAO(inputGrid->getRadarHeight())
            / 100.0f; // hPa

        // Lowest level = highest pressure.
        result->levels[result->nlevs - 1] = radarPressure_hPa;

        // Highest level = lowest pressure.
        float topPressure_hPa = inputGrid->getTopHeight_hPa();

        // Level step size.
        float deltaLevel_hPa =
            (radarPressure_hPa - topPressure_hPa) / float(result->nlevs - 1);

        // Set the pressure levels in the new grid.
        for (int k = result->nlevs - 2; k >= 0; k--)
        {
            result->levels[k] = result->levels[k + 1] - deltaLevel_hPa;
        }
    }
    // uniform height distance
    else if (levelTypeDefinition == "EDHL")
    {
        float topMetre = inputGrid->getTopHeightMetre();
        float bottomMetre = inputGrid->getRadarHeight();
        float deltaHeightMetre =
            (topMetre - bottomMetre) / float(result->nlevs - 1);

        // Set the pressure levels in the new structured grid iterating over levels;
        // the last level has the lowest height and highest pressure.
        float currentMetre = bottomMetre;
        for (int k = result->nlevs - 1; k >= 0; k--)
        {
            float pressure = metre2pressure_standardICAO(currentMetre) / 100;
            result->levels[k] = pressure;
            currentMetre += deltaHeightMetre;
        }
    }
    // user defined pressure levels
    else if (levelTypeDefinition == "UD")
    {
        //  Sort user defined pressure levels in ascending order.
        QVector<int> tmpLevels(nlevs);
        for (uint k = 0; k < result->nlevs; k++)
        {
            // read from the list given by the user starting at index 1 (first string is "UD").
            tmpLevels[k] = levelTypeStrings.at(k + 1).toFloat();
        }
        std::sort(tmpLevels.begin(), tmpLevels.end());

        // Set the pressure levels in the new structured grid.
        for (uint k = 0; k < result->nlevs; k++)
        {
            result->levels[k] = tmpLevels[k];
        }
    }

    result->setToValue(M_MISSING_VALUE);


    // If the radar grid shape is constant across time steps (isShapeConstant),
    // the nearest radar grid point positions to each point in the structured
    // grid can be reused as long as the topology is unchanged.
    int *nearestNeighborsPointer = nullptr;

    bool usePreviousNearestNeighbors = false;
    bool areNearestNeighborsCalculated = ! nearestNeighbors.empty();
    bool isShapeConstant = inputGrid->areAzimuthAnglesInterpolated();

    if (isShapeConstant)
    {
        // Reuse last grid shape if one exists and it's still the same.
        if (areNearestNeighborsCalculated && ! hasTopologyChanged)
        {
            usePreviousNearestNeighbors = true;
            nearestNeighborsPointer = nearestNeighbors.data();
        }
        else if (inputGrid->getNumberOfElevations()
                 == inputGrid->getScanCount() // Radar grid is complete.
        )
        {
            // If there aren't any radar nearest neighbors stored for the current
            // structured grid shape, we store new ones. To be reusable later,
            // the radar grid needs to be complete (all elevations contained).

            nearestNeighbors.clear();
            try
            {
                nearestNeighbors.resize(nlons * nlats * nlevs * 3);
            }
            catch (const std::bad_alloc &e)
            {
                LOG4CPLUS_ERROR(mlog, "Requested grid size is too large.");
                return result; // Currently filled with missing values.
            }

            nearestNeighborsPointer = nearestNeighbors.data();
        }
        else
        {
            // Nothing.
            // If neither previous nearestNeighbors exist nor a new one is
            // calculated, 'pointer' is a nullpointer, which isn't a problem in
            // the interpolation.
        }
    }


#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
    LOG4CPLUS_DEBUG(mlog, "Stopwatch enabled - regridding");
#endif

    // Iterate over structured grid points and interpolate/get its value.
#pragma omp parallel for
    for (uint idxLon = 0; idxLon < result->nlons; idxLon++)
    {
        for (uint idxLat = 0; idxLat < result->nlats; idxLat++)
        {
            for (uint idxLev = 0; idxLev < result->nlevs; idxLev++)
            {
                unsigned int idx = 0;
                double value = M_MISSING_VALUE;

                if (nearestNeighborsPointer != nullptr)
                {
                    // Needed if nearestNeighbors vector is either written to or read from.
                    idx = idxLev * nlons * nlats * 3 + idxLon * nlats * 3
                          + idxLat * 3;
                }

                if (usePreviousNearestNeighbors)
                {
                    // Use stored nearest neighbor positions to get data values
                    // from the radar grid.
                    value = inputGrid->getValue(nearestNeighbors.at(idx),
                                                nearestNeighbors.at(idx + 1),
                                                nearestNeighbors.at(idx + 2));
                }
                else
                {
                    // Determine which radar grid cell contains the lon/lat/lev
                    // grid point; return its data value and store the radar grid
                    // cell in pointer to nearestNeighbors if it is used.
                    value = inputGrid->inverseTransformationInterpolation(
                        result->lons[idxLon], result->lats[idxLat],
                        result->levels[idxLev] * 100., nearestNeighborsPointer + idx);
                }
                result->setValue(idxLev, idxLat, idxLon, value);
            }
        }
    }

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "radar regridding finished in "
                    << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                    << " seconds");
#endif

    regriddedGrid = result;

    inputSource->releaseData(inputGrid);

    return regriddedGrid;
}


MTask *MRadarRegridder::createTaskGraph(MDataRequest request)
{
    assert(inputSource != nullptr);

    MTask *task = new MTask(request, this);

    MDataRequestHelper rh(request);
    task->addParent(inputSource->getTaskGraph(rh.request()));

    return task;
}


QList<MVerticalLevelType> MRadarRegridder::availableLevelTypes()
{
    QList<MVerticalLevelType> list;

    // Currently only available as pressure levels.
    list.append(MVerticalLevelType::PRESSURE_LEVELS_3D);
    return list;
}

QStringList MRadarRegridder::availableVariables(MVerticalLevelType levelType)
{
    assert(inputSource != nullptr);
    // Only return radar variables.
    return inputSource->availableVariables(MVerticalLevelType::RADAR_LEVELS_3D);
}


QSet<unsigned int>
MRadarRegridder::availableEnsembleMembers(MVerticalLevelType levelType,
                                          const QString &variableName)
{
    // Radar data doesn't have an ensemble.
    QSet<unsigned int> set;
    set.insert(0);
    return set;
}


QList<QDateTime>
MRadarRegridder::availableInitTimes(MVerticalLevelType levelType,
                                    const QString &variableName)
{
    assert(inputSource != nullptr);
    // Only return times of radar variables.
    return inputSource->availableInitTimes(MVerticalLevelType::RADAR_LEVELS_3D,
                                           variableName);
}


QList<QDateTime>
MRadarRegridder::availableValidTimes(MVerticalLevelType levelType,
                                     const QString &variableName,
                                     const QDateTime &initTime)
{
    assert(inputSource != nullptr);
    // Only return times of radar variables.
    return inputSource->availableValidTimes(MVerticalLevelType::RADAR_LEVELS_3D,
                                            variableName, initTime);
}


QString MRadarRegridder::variableLongName(MVerticalLevelType levelType,
                                          const QString &variableName)
{
    assert(inputSource != nullptr);
    // Only return names of radar variables.
    return inputSource->variableLongName(MVerticalLevelType::RADAR_LEVELS_3D,
                                         variableName);
}


QString MRadarRegridder::variableStandardName(MVerticalLevelType levelType,
                                              const QString &variableName)
{
    assert(inputSource != nullptr);
    // Only return names of radar variables.
    return inputSource->variableStandardName(
        MVerticalLevelType::RADAR_LEVELS_3D, variableName);
}


QString MRadarRegridder::variableUnits(MVerticalLevelType levelType,
                                       const QString &variableName)
{
    assert(inputSource != nullptr);
    // Only return units of radar variables.
    return inputSource->variableUnits(MVerticalLevelType::RADAR_LEVELS_3D,
                                      variableName);
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MRadarRegridder::locallyRequiredKeys()
{
    return QStringList() << "VARIABLE" << "VALID_TIME" << "RADAR_REGRID_LEVELS"
           << "RADAR_REGRID_LONLAT_DISTANCE"
           << "USE_UNDETECT"
           << "RADAR_REGRIDDER_TOPOLOGY_HAS_CHANGED";
}
} // namespace Met3D
