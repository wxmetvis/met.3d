/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2016      Christoph Heidelmann [+]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2023      Thorwin Vogt [*]
**  Copyright 2024      Susanne Fuchs [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "adddatasetdialog.h"
#include "ui_adddatasetdialog.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "inputvarsedittable.h"
#include "mglresourcesmanager.h"
#include "msystemcontrol.h"
#include "system/pipelineconfiguration.h"
#include "util/mfiletypes.h"


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MAddDatasetDialog::MAddDatasetDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::MAddDatasetDialog),
    isEnsemble(true)
{
    ui->setupUi(this);
    okButton = ui->buttonBox->buttons().at(0);
    okButton->setEnabled(false);
    connect(ui->saveConfigPushButton, SIGNAL(clicked()),
            this, SLOT(saveConfigurationToFile()));

    connect(ui->nameEdit                  , SIGNAL(textChanged(QString)),
            this, SLOT(inputFieldChanged()));

    connect(ui->nwpBrowseButton           , SIGNAL(clicked(bool))       ,
            this, SLOT(browsePath()));
    connect(ui->trajectoriesBrowseButton  , SIGNAL(clicked(bool))       ,
            this, SLOT(browsePath()));
    connect(ui->radarBrowseButton           , SIGNAL(clicked(bool))       ,
            this, SLOT(browsePath()));

    connect(ui->nwpPathEdit               , SIGNAL(textChanged(QString)),
            this, SLOT(inputFieldChanged()));
    connect(ui->nwpFileFilterEdit         , SIGNAL(textChanged(QString)),
            this, SLOT(inputFieldChanged()));
    connect(ui->trajectoriesPathEdit      , SIGNAL(textChanged(QString)),
            this, SLOT(inputFieldChanged()));
    connect(ui->radarPathEdit             , SIGNAL(textChanged(QString)),
              this, SLOT(inputFieldChanged()));
    connect(ui->radarFileFilterEdit         , SIGNAL(textChanged(QString)),
            this, SLOT(inputFieldChanged()));

    connect(ui->pipelineTypeTabWidget     , SIGNAL(currentChanged(int)) ,
            this, SLOT(inputFieldChanged()));
    connect(ui->pipelineTypeTabWidget     , SIGNAL(currentChanged(int)) ,
            this, SLOT(setDefaultMemoryManager()));

    connect(ui->editInputVarsInTableButton, SIGNAL(clicked()),
            this, SLOT(editInputVarsInTable()));
    resetAddDatasetGUI();
}


MAddDatasetDialog::~MAddDatasetDialog()
{
    delete ui;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

PipelineType MAddDatasetDialog::getSelectedPipelineType()
{
    if (ui->pipelineTypeTabWidget->currentWidget() == ui->nwpTab)
    {
        return PipelineType::NWP_PIPELINE;
    }
    else if (ui->pipelineTypeTabWidget->currentWidget()  == ui->trajectoriesTab)
    {
        return PipelineType::TRAJECTORIES_PIPELINE;
    }
    else if (ui->pipelineTypeTabWidget->currentWidget() == ui->radarTab)
    {
        return PipelineType::RADAR_PIPELINE;
    }

    return PipelineType::INVALID_PIPELINE_TYPE;
}


MNWPPipelineConfigurationInfo MAddDatasetDialog::getNWPPipelineConfigurationInfo()
{
    MNWPPipelineConfigurationInfo d;

    d.name = ui->nameEdit->text();
    d.fileDir = ui->nwpPathEdit->text();
    d.fileFilter = ui->nwpFileFilterEdit->text();
    d.dataFormat =
            (MNWPReaderFileFormat) (ui->nwpFileFormatCombo->currentIndex() + 1);

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    QStringList memoryManagers = sysMC->getMemoryManagerIdentifiers();
    d.memoryManagerID = memoryManagers.at(ui->memoryMCombo->currentIndex());
    d.enableProbabiltyRegionFilter = ui->propRegBool->isChecked();
    d.treatRotatedGridAsRegularGrid =
            ui->treatRotatedAsRegularCheckBox->isChecked();
    d.treatProjectedGridAsRegularLonLatGrid =
            ui->treatProjectedAsRegularLonLatCheckBox->isChecked();
    d.surfacePressureFieldType = ui->surfacePressureTypeLineEdit->text();
    d.convertGeometricHeightToPressure_ICAOStandard =
            ui->convertGeometricHeightToPressureICAOStandardCheckBox->isChecked();
    d.auxiliary3DPressureField = ui->auxiliary3DPressureFieldLineEdit->text();
    d.consistencyCheckReferenceVariables = ui->consistencyRefVarsLineEdit->text();
    d.disableGridConsistencyCheck =
            ui->disableGridConsistencyCheckCheckBox->isChecked();

    QString inputVarsForDerivedVars = ui->inputVarsForDerivedVarsLineEdit->text();
    d.inputVarsForDerivedVars = inputVarsStringToMap(inputVarsForDerivedVars);

    d.addTimesAndMembersToSyncControl = ui->addTimesToSyncControlCheckBox->isChecked();
    return d;
}


MTrajectoriesPipelineConfigurationInfo
MAddDatasetDialog::getTrajectoriesPipelineConfigurationInfo()
{
    MTrajectoriesPipelineConfigurationInfo d;

    d.name = ui->nameEdit->text();
    d.isEnsemble = isEnsemble;
    d.fileDir = ui->trajectoriesPathEdit->text();
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    QStringList memoryManagers = sysMC->getMemoryManagerIdentifiers();
    d.memoryManagerID = memoryManagers.at(ui->memoryMCombo->currentIndex());
    d.boundaryLayerTrajectories = ui->ablTrajectoriesCheckBox->isChecked();

    d.addTimesAndMembersToSyncControl = ui->addTimesToSyncControlCheckBox->isChecked();
    return d;
}


MRadarPipelineConfigurationInfo MAddDatasetDialog::getRadarPipelineConfigurationInfo()
{
    MRadarPipelineConfigurationInfo d;

    d.name = ui->nameEdit->text();
    d.fileDir = ui->radarPathEdit->text();
    d.fileFilter = ui->radarFileFilterEdit->text();
    d.standardAzimuthGridsize = ui->azimuthAngleBox->value();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    QStringList memoryManagers = sysMC->getMemoryManagerIdentifiers();
    d.memoryManagerID = memoryManagers.at(ui->memoryMCombo->currentIndex());

    d.addTimesAndMembersToSyncControl = ui->addTimesToSyncControlCheckBox->isChecked();
    return d;
}


void MAddDatasetDialog::resetAddDatasetGUI()
{
    ui->memoryMCombo->clear();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    for (const QString &mmID : sysMC->getMemoryManagerIdentifiers())
    {
        ui->memoryMCombo->addItem(mmID);
    }

    setDefaultMemoryManager();
}


void MAddDatasetDialog::saveConfiguration(QSettings *settings)
{
    // Save configuration for NWP Pipeline.
    // ====================================
    if (ui->pipelineTypeTabWidget->currentWidget() == ui->nwpTab)
    {
        MNWPPipelineConfigurationInfo info = getNWPPipelineConfigurationInfo();
        info.saveConfiguration(settings);
    }
    // Save configuration for Trajectories Pipeline.
    // =============================================
    else if (ui->pipelineTypeTabWidget->currentWidget() == ui->trajectoriesTab)
    {
        MTrajectoriesPipelineConfigurationInfo info = getTrajectoriesPipelineConfigurationInfo();
        info.saveConfiguration(settings);
    }
    else if (ui->pipelineTypeTabWidget->currentWidget() == ui->radarTab)
    {
        MRadarPipelineConfigurationInfo info = getRadarPipelineConfigurationInfo();
        info.saveConfiguration(settings);
    }
}


void MAddDatasetDialog::loadConfiguration(QSettings *settings)
{
    QStringList groups = settings->childGroups();
    if (MNWPPipelineConfigurationInfo::isNWPConfig(settings))
    {
        ui->pipelineTypeTabWidget->setCurrentWidget(ui->nwpTab);

        MNWPPipelineConfigurationInfo info;
        info.loadConfiguration(settings);

        ui->nameEdit->setText(info.name);
        ui->nwpPathEdit->setText(info.fileDir);
        ui->nwpFileFilterEdit->setText(info.fileFilter);
        ui->memoryMCombo->setCurrentText(info.memoryManagerID);
        ui->nwpFileFormatCombo->setCurrentText(info.getNWPFormatString());
        ui->regriddingBool->setChecked(info.enableRegridding);
        ui->propRegBool->setChecked(info.enableProbabiltyRegionFilter);
        ui->treatRotatedAsRegularCheckBox->setChecked(info.treatRotatedGridAsRegularGrid);
        ui->treatProjectedAsRegularLonLatCheckBox->setChecked(info.treatProjectedGridAsRegularLonLatGrid);
        ui->convertGeometricHeightToPressureICAOStandardCheckBox->setChecked(info.convertGeometricHeightToPressure_ICAOStandard);
        ui->surfacePressureTypeLineEdit->setText(info.surfacePressureFieldType);
        ui->auxiliary3DPressureFieldLineEdit->setText(info.auxiliary3DPressureField);
        ui->consistencyRefVarsLineEdit->setText(info.consistencyCheckReferenceVariables);
        ui->disableGridConsistencyCheckCheckBox->setChecked(info.disableGridConsistencyCheck);
        ui->inputVarsForDerivedVarsLineEdit->setText(inputVarsMapToString(info.inputVarsForDerivedVars));
    }
    else if (MTrajectoriesPipelineConfigurationInfo::isTrajectoryConfig(settings))
    {
        ui->pipelineTypeTabWidget->setCurrentWidget(ui->trajectoriesTab);

        MTrajectoriesPipelineConfigurationInfo info;
        info.loadConfiguration(settings);

        ui->nameEdit->setText(info.name);
        isEnsemble = info.isEnsemble;
        ui->memoryMCombo->setCurrentText(info.memoryManagerID);

        ui->trajectoriesPathEdit->setText(info.fileDir);
        ui->ablTrajectoriesCheckBox->setChecked(info.boundaryLayerTrajectories);
    }
    else if (MRadarPipelineConfigurationInfo::isRadarConfig(settings))
    {
        ui->pipelineTypeTabWidget->setCurrentWidget(ui->radarTab);

        MRadarPipelineConfigurationInfo info;
        info.loadConfiguration(settings);

        ui->nameEdit->setText(info.name);
        ui->radarPathEdit->setText(info.fileDir);
        ui->radarFileFilterEdit->setText(info.fileFilter);
        ui->memoryMCombo->setCurrentText(info.memoryManagerID);
        ui->azimuthAngleBox->setValue(info.standardAzimuthGridsize);
    }
}


void MAddDatasetDialog::openDialog()
{
    if (exec() == QDialog::Accepted)
    {
        PipelineType pipelineType = getSelectedPipelineType();

        switch (pipelineType)
        {
        case NWP_PIPELINE:
        {
            MNWPPipelineConfigurationInfo pipelineConfig =
                getNWPPipelineConfigurationInfo();

            LOG4CPLUS_INFO(mlog, "Adding new dataset: " << pipelineConfig.name);

            MPipelineConfiguration newPipelineConfig;
            newPipelineConfig.initializeNWPPipeline(pipelineConfig);
            break;
        }
        case TRAJECTORIES_PIPELINE:
        {
            MTrajectoriesPipelineConfigurationInfo pipelineConfig =
                getTrajectoriesPipelineConfigurationInfo();

            LOG4CPLUS_INFO(mlog, "Adding new dataset: " << pipelineConfig.name);

            MPipelineConfiguration newPipelineConfig;
            newPipelineConfig.initializePrecomputedTrajectoriesPipeline(
                    pipelineConfig);
            break;
        }
        case RADAR_PIPELINE:
        {
            MRadarPipelineConfigurationInfo pipelineConfig =
                    getRadarPipelineConfigurationInfo();

            LOG4CPLUS_INFO(mlog, "Adding new dataset: " << pipelineConfig.name);

            MPipelineConfiguration newPipelineConfig;
            newPipelineConfig.initializeRadarPipeline(pipelineConfig);
            break;
        }
        default:
        {
            break;
        }
        }
    }
}


void MAddDatasetDialog::createAndOpen(QSettings *settings)
{
    MAddDatasetDialog addDatasetDialog;
    if (settings)
    {
        addDatasetDialog.loadConfiguration(settings);
    }
    addDatasetDialog.openDialog();
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MAddDatasetDialog::saveConfigurationToFile(QString filename)
{
    if (filename.isEmpty())
    {
        QString directory =
                MSystemManagerAndControl::getInstance()
                        ->getMet3DWorkingDirectory().absoluteFilePath("config/pipelines");
        QDir().mkpath(directory);
        filename = MFileUtils::getSaveFileName(
                    this, "Save pipeline configuration",
                    FileTypes::M_PIPELINE_CONFIG,
                    directory,
                    "default");

        if (filename.isEmpty())
        {
            return;
        }
    }

    // Overwrite if the file exists.
    if (QFile::exists(filename))
    {
        QFile::remove(filename);
    }

    LOG4CPLUS_INFO(mlog, "Saving configuration to " << filename);

    QSettings *settings = new QSettings(filename, QSettings::IniFormat);

    saveConfiguration(settings);

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been saved.");
}


bool MAddDatasetDialog::loadConfigurationFromFile(QString filename)
{
    if (filename.isEmpty())
    {
        filename = MFileUtils::getOpenFileName(
                    this, "Load pipeline configuration",
                    FileTypes::M_PIPELINE_CONFIG,
                    MSystemManagerAndControl::getInstance()
                    ->getMet3DWorkingDirectory().absoluteFilePath(
                        "config/pipelines"));

        if (filename.isEmpty())
        {
            return false;
        }
    }

    QFileInfo fileInfo(filename);
    if (!fileInfo.exists())
    {
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText(QString("Pipeline configuration file"
                            " ' %1 ' does not exist.").arg(filename));
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
        return false;
    }

    LOG4CPLUS_INFO(mlog, "Loading pipeline configuration from " << filename);

    QSettings *settings = new QSettings(filename, QSettings::IniFormat);

    QStringList groups = settings->childGroups();
    if (!(MNWPPipelineConfigurationInfo::isNWPConfig(settings)
            || MTrajectoriesPipelineConfigurationInfo::isTrajectoryConfig(settings)))
    {
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText("The selected file does not contain configuration data "
                    "for nwp or trajectories pipeline.");
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
        return false;
    }

    loadConfiguration(settings);

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been loaded.");

    return true;
}


void MAddDatasetDialog::editInputVarsInTable()
{
    QString currentInputVarString = ui->inputVarsForDerivedVarsLineEdit->text();
    MInputVarsEditTable table(currentInputVarString, this);

    if (! table.exec())
    {
        // Cancelled dialog, do not make any changes.
        return;
    }
    QString selectedVars = table.currentMappingAsString();
    ui->inputVarsForDerivedVarsLineEdit->setText(selectedVars);
}

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

void MAddDatasetDialog::browsePath()
{
    QString dir = ui->nwpPathEdit->text();

    if (dir.isEmpty())
    {
        dir = MSystemManagerAndControl::getInstance()
                ->getMet3DWorkingDirectory().path();
    }

    QString path = MFileUtils::getOpenDirectory(
                this, tr("Select path to data files"), dir);

    if (path != nullptr)
    {
        if (ui->pipelineTypeTabWidget->currentWidget() == ui->nwpTab)
        {
            ui->nwpPathEdit->setText(path);
        }
        else if (ui->pipelineTypeTabWidget->currentWidget() == ui->trajectoriesTab)
        {
            ui->trajectoriesPathEdit->setText(path);
        }
        else
        {
            ui->radarPathEdit->setText(path);
        }
    }
}


void MAddDatasetDialog::inputFieldChanged()
{
    if (ui->nameEdit->text()       != ""
            &&
            ((ui->pipelineTypeTabWidget->currentIndex() == 0
              && ui->nwpPathEdit->text()                != ""
              && ui->nwpFileFilterEdit->text()          != ""
              )
             || (ui->pipelineTypeTabWidget->currentIndex()        == 1
                 && ui->trajectoriesPathEdit->text()              != "")
             || (ui->pipelineTypeTabWidget->currentIndex()        == 2
                 && ui->radarPathEdit->text()               != ""
                 && ui->radarFileFilterEdit->text()         != "")
             )
            )
    {
        okButton->setEnabled(true);
    }
    else
    {
        okButton->setEnabled(false);
    }
}


void MAddDatasetDialog::setDefaultMemoryManager()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    if (ui->pipelineTypeTabWidget->currentWidget() == ui->nwpTab)
    {
        if (sysMC->getDefaultMemoryManagers()->value("NWP") != "")
        {
            ui->memoryMCombo->setCurrentText(
                        sysMC->getDefaultMemoryManagers()->value("NWP"));
        }
    }
    else if (ui->pipelineTypeTabWidget->currentWidget() == ui->trajectoriesTab)
    {
        if (sysMC->getDefaultMemoryManagers()->value("Trajectories") != "")
        {
            ui->memoryMCombo->setCurrentText(
                        sysMC->getDefaultMemoryManagers()->value("Trajectories"));
        }
    }
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/


} // namespace Met3D
