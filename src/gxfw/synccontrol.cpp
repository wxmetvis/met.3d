/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2017 Bianca Tost [+]
**  Copyright 2022-2023 Thorwin Vogt [*]
**  Copyright 2024 Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "synccontrol.h"
#include "ui_synccontrol.h"

// standard library imports
#include <iostream>
#include <algorithm>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QMenu>
#include <QLineEdit>

// local application imports
#include "util/mutil.h"
#include "gxfw/msystemcontrol.h"
#include "gxfw/msceneviewglwidget.h"
#include "datasource/selectdatasourcedialog.h"
#include "data/weatherpredictiondatasource.h"
#include "trajectories/source/trajectoryreader.h"
#include "util/mfiletypes.h"

// Uncomment the following define to enable debug output for sync events.
//#define SYNC_DEBUG_OUTPUT

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MLabelledWidgetAction::MLabelledWidgetAction(
        const QString &labelFront, const QString &labelBack,
        QWidget *customWidget, QWidget *parent)
    : QWidgetAction(parent)
{
    QWidget* widget = new QWidget(parent);
    QHBoxLayout* layout = new QHBoxLayout();

    QLabel *label1 = new QLabel(labelFront, parent);
    layout->addWidget(label1);

    this->customWidget = customWidget;
    layout->addWidget(customWidget);

    if ( !labelBack.isEmpty() )
    {
        QLabel *label2 = new QLabel(labelBack, parent);
        layout->addWidget(label2);
    }

    widget->setLayout(layout);
    setDefaultWidget(widget);
}


MSyncControl::MSyncControl(QString id, QWidget *parent) :
        QWidget(parent),
        ui(new Ui::MSyncControl),
        currentAnimDelay(1000),
        saveAnimationSceneView(nullptr),
        overwriteAnimationImageSequence(false),
        syncID(std::move(id)),
        frameNumber(0),
        animatorAdvanceTime(false),
        synchronizationInProgress(false),
        validDateTimeHasChanged(false),
        lastFocusWidget(nullptr),
        currentSyncType(SYNC_UNKNOWN),
        integrationType(SYNC_UNKNOWN),
        deltaTimeStep(6),
        timeStepUnit(2),
        validTime(),
        initTime(),
        selectedEnsMember(0),
        showEnsembleMean(false)
{
    selectedDataSourceActionList.clear();
    synchronizedObjects = QList<MSynchronizedObject*>();
    synchronizedObjects.clear();

    ui->setupUi(this);

    // Initialise a timer to control the animation. We use a single shot timer
    // that gets started once synchronization of the current time has finished,
    // and gets triggered after a specified delay to advance the time.
    animationTimer = new QTimer(this);
    animationTimer->setSingleShot(true);
    animationTimer->setInterval(currentAnimDelay);

    connect(animationTimer, &QTimer::timeout,
            this, &MSyncControl::timeAnimationAdvanceTimeStep);

    // Configuration control elements.
    // =========================================================================

    configurationDropdownMenu = new QMenu(this);
    // Load configuration.
    loadConfigurationAction = new QAction(this);
    loadConfigurationAction->setText(
                "load synchronisation configuration");
    configurationDropdownMenu->addAction(loadConfigurationAction);
    connect(loadConfigurationAction, SIGNAL(triggered()),
            SLOT(loadConfigurationFromFile()));

    // Save configuration.
    saveConfigurationAction = new QAction(this);
    saveConfigurationAction->setText(
                "save synchronisation configuration");
    configurationDropdownMenu->addAction(saveConfigurationAction);
    connect(saveConfigurationAction, SIGNAL(triggered()),
            SLOT(saveConfigurationToFile()));

    configurationDropdownMenu->addSeparator();

    // Select data source.
    selectDataSourcesAction = new QAction(this);
    selectDataSourcesAction->setText(
                "select data sources for allowed times and members");
    configurationDropdownMenu->addAction(selectDataSourcesAction);

    // Selected data sources.
    configurationDropdownMenu->addSeparator();
    configurationDropdownMenu->addAction("Selected data sources:")
            ->setEnabled(false);

    ui->configurationButton->setMenu(configurationDropdownMenu);

    connect(selectDataSourcesAction, SIGNAL(triggered()),
            SLOT(selectDataSources()));

    // Show menu also if the users clicks the button not only if only the arrow
    // was clicked.
    connect(ui->configurationButton, SIGNAL(clicked()),
            ui->configurationButton, SLOT(showMenu()));

    // Time control elements.
    // =========================================================================

    // Time steps for navigating valid/init time. (Default value: 6 hours).
    setDeltaTimeStep(deltaTimeStep);
    setDeltaTimeStepUnit(timeStepUnit);
    integrationType = static_cast<MSynchronizationType>(ui->stepChooseVTITComboBox->currentIndex());
    setIntegrationType(integrationType);

    restrictControlToDataSources();

    // Only initialise with initTime and validTime if they are set properly.
    if (!initTime.isNull() && !validTime.isNull())
    {
        // Initialise with minimum init and valid time.
        setInitDateTime(initTime);
        setValidDateTime(validTime);
    }
    else
    {
        // Initialise with 00 UTC of current date.
        setInitDateTime(QDateTime::currentDateTimeUtc().date().startOfDay(), true);
        setValidDateTime(QDateTime::currentDateTimeUtc().date().startOfDay(), true);
    }
    updateTimeDifference();

    connect(ui->validTimeEdit, SIGNAL(dateTimeChanged(QDateTime)),
            SLOT(onValidDateTimeChange(QDateTime)));
    connect(ui->initTimeEdit, SIGNAL(dateTimeChanged(QDateTime)),
            SLOT(onInitDateTimeChange(QDateTime)));
    connect(ui->timeForwardButton, SIGNAL(clicked()),
            SLOT(onTimeForwardClicked()));
    connect(ui->timeBackwardButton, SIGNAL(clicked()),
            SLOT(onTimeBackwardClicked()));
    connect(ui->timeStepSpinBox, SIGNAL(valueChanged(int)),
            SLOT(onTimeStepChanged(int)));
    connect(ui->timeUnitsComboBox, SIGNAL(currentIndexChanged(int)),
            SLOT(onTimeStepUnitChanged(int)));
    connect(ui->stepChooseVTITComboBox, SIGNAL(currentIndexChanged(int)),
            SLOT(onIntegrationTypeChanged(int)));

    // Initialise a drop down menu that provides time animation settings.
    // ==================================================================
    timeAnimationDropdownMenu = new QMenu(this);

    timeAnimationDelaySpinBox = new QSpinBox(this);
    timeAnimationDelaySpinBox->setMinimum(10);
    timeAnimationDelaySpinBox->setMaximum(100000);
    timeAnimationDelaySpinBox->setValue(1000);
    MLabelledWidgetAction *animationDelaySpinBoxAction =
            new MLabelledWidgetAction("delay between animation steps:", "ms",
                                      timeAnimationDelaySpinBox, this);
    timeAnimationDropdownMenu->addAction(animationDelaySpinBoxAction);

    timeAnimationDropdownMenu->addSeparator();

    // "from" entry of drop down menu.
    // -------------------------------

    // Width for all "copy IT/VT to from/to" buttons.
    int widthOfCopyButtons = 30;

    timeAnimationFromWidget = new QWidget(this);

    timeAnimationFrom = new QDateTimeEdit(timeAnimationFromWidget);
    timeAnimationFrom->setDisplayFormat("ddd yyyy-MM-dd hh:mm:ss UTC");
    timeAnimationFrom->setTimeSpec(Qt::UTC);
    copyInitTimeToAnimationFromButton = new QPushButton("IT", timeAnimationFromWidget);
    copyInitTimeToAnimationFromButton->setMinimumWidth(widthOfCopyButtons);
    copyInitTimeToAnimationFromButton->setMaximumWidth(widthOfCopyButtons);
    copyInitTimeToAnimationFromButton->setToolTip("copy current init time");
    copyValidTimeToAnimationFromButton = new QPushButton("VT", timeAnimationFromWidget);
    copyValidTimeToAnimationFromButton->setMinimumWidth(widthOfCopyButtons);
    copyValidTimeToAnimationFromButton->setMaximumWidth(widthOfCopyButtons);
    copyValidTimeToAnimationFromButton->setToolTip("copy current valid time");

    timeAnimationFromLayout = new QHBoxLayout();
    timeAnimationFromLayout->addWidget(timeAnimationFrom);
    timeAnimationFromLayout->addWidget(copyInitTimeToAnimationFromButton);
    timeAnimationFromLayout->addWidget(copyValidTimeToAnimationFromButton);

    timeAnimationFromWidget->setLayout(timeAnimationFromLayout);

    MLabelledWidgetAction *animateFromTimeAction =
            new MLabelledWidgetAction("from", "", timeAnimationFromWidget, this);
    timeAnimationDropdownMenu->addAction(animateFromTimeAction);

    connect(copyInitTimeToAnimationFromButton, SIGNAL(clicked()),
            SLOT(copyInitToFrom()));
    connect(copyValidTimeToAnimationFromButton, SIGNAL(clicked()),
            SLOT(copyValidToFrom()));

    // "to" entry of drop down menu.
    // -----------------------------

    timeAnimationToWidget = new QWidget(this);

    timeAnimationTo = new QDateTimeEdit(timeAnimationToWidget);
    timeAnimationTo->setDisplayFormat("ddd yyyy-MM-dd hh:mm:ss UTC");
    timeAnimationTo->setTimeSpec(Qt::UTC);
    copyInitTimeToAnimationToButton = new QPushButton("IT", timeAnimationToWidget);
    copyInitTimeToAnimationToButton->setMinimumWidth(widthOfCopyButtons);
    copyInitTimeToAnimationToButton->setMaximumWidth(widthOfCopyButtons);
    copyInitTimeToAnimationToButton->setToolTip("copy current init time");
    copyValidTimeToAnimationToButton = new QPushButton("VT", timeAnimationToWidget);
    copyValidTimeToAnimationToButton->setMinimumWidth(widthOfCopyButtons);
    copyValidTimeToAnimationToButton->setMaximumWidth(widthOfCopyButtons);
    copyValidTimeToAnimationToButton->setToolTip("copy current valid time");

    timeAnimationToLayout = new QHBoxLayout();
    timeAnimationToLayout->addWidget(timeAnimationTo);
    timeAnimationToLayout->addWidget(copyInitTimeToAnimationToButton);
    timeAnimationToLayout->addWidget(copyValidTimeToAnimationToButton);
    timeAnimationToWidget->setLayout(timeAnimationToLayout);

    MLabelledWidgetAction *animateToTimeAction =
            new MLabelledWidgetAction("to", "", timeAnimationToWidget, this);
    timeAnimationDropdownMenu->addAction(animateToTimeAction);

    connect(copyInitTimeToAnimationToButton, SIGNAL(clicked()),
            SLOT(copyInitToTo()));
    connect(copyValidTimeToAnimationToButton, SIGNAL(clicked()),
            SLOT(copyValidToTo()));

    // As default, copy current init/valid times to from/to fields.
    copyInitToFrom();
    copyValidToTo();

    timeAnimationDropdownMenu->addSeparator();

    timeAnimationSinglePassAction = new QAction(this);
    timeAnimationSinglePassAction->setCheckable(true);
    timeAnimationSinglePassAction->setChecked(true);
    timeAnimationSinglePassAction->setText("Single pass");
    timeAnimationDropdownMenu->addAction(timeAnimationSinglePassAction);

    timeAnimationLoopTimeAction = new QAction(this);
    timeAnimationLoopTimeAction->setCheckable(true);
    timeAnimationLoopTimeAction->setText("Loop");
    timeAnimationDropdownMenu->addAction(timeAnimationLoopTimeAction);

    timeAnimationBackForthTimeAction = new QAction(this);
    timeAnimationBackForthTimeAction->setCheckable(true);
    timeAnimationBackForthTimeAction->setText("Back and forth");
    timeAnimationDropdownMenu->addAction(timeAnimationBackForthTimeAction);

    timeAnimationLoopGroup = new QActionGroup(this);
    timeAnimationLoopGroup->setExclusive(true);
    timeAnimationLoopGroup->addAction(timeAnimationSinglePassAction);
    timeAnimationLoopGroup->addAction(timeAnimationLoopTimeAction);
    timeAnimationLoopGroup->addAction(timeAnimationBackForthTimeAction);

    timeAnimationReverseTimeDirectionAction = new QAction(this);
    timeAnimationReverseTimeDirectionAction->setCheckable(true);
    timeAnimationReverseTimeDirectionAction->setText("Reverse time direction");
    timeAnimationDropdownMenu->addAction(timeAnimationReverseTimeDirectionAction);

    timeAnimationUseSequenceAction = new QAction(this);
    timeAnimationUseSequenceAction->setCheckable(true);
    timeAnimationUseSequenceAction->setChecked(false);
    timeAnimationUseSequenceAction->setText("Use camera Sequence");
    timeAnimationDropdownMenu->addAction(timeAnimationUseSequenceAction);

    connect(timeAnimationLoopGroup, SIGNAL(triggered(QAction*)),
            this, SLOT(onAnimationLoopGroupChanged(QAction*)));

    // Save animation.
    // ===============
    timeAnimationDropdownMenu->addSeparator();


    saveAnimationImages = MBoolProperty("Save images to files", false);
    saveAnimationImages.setLabel("Save images to files");
    saveAnimationImages.setTooltip(
                "Activate to automatically save an image of\n"
                "the selected view after each\n"
                "synchronisation event.");
    saveAnimationImages.registerValueCallback([=]()
    {
        activateTimeAnimationImageSaving(saveAnimationImages);
    });

    auto *saveAnimationLayout = new QHBoxLayout();
    saveAnimationLayout->addWidget(saveAnimationImages.createEditorWidget(nullptr));
    saveAnimationLayout->setAlignment(saveAnimationLayout, Qt::AlignLeft);

    auto *saveAnimationWidget = new QWidget();
    saveAnimationWidget->setLayout(saveAnimationLayout);

    auto *saveAnimationAction = new QWidgetAction(this);
    saveAnimationAction->setDefaultWidget(saveAnimationWidget);
    timeAnimationDropdownMenu->addAction(saveAnimationAction);

    saveAnimationSceneViewsComboBox = new QComboBox();
    QStringList sceneViewsIdentifiers;
    for (MSceneViewGLWidget *sceneView :
             MSystemManagerAndControl::getInstance()->getRegisteredViews())
    {
        sceneViewsIdentifiers << QString("view #%1").arg(sceneView->getID()+1);
    }
    saveAnimationSceneViewsComboBox->addItems(sceneViewsIdentifiers);
    QHBoxLayout *sceneViewLayout = new QHBoxLayout();
    sceneViewLayout->addWidget(saveAnimationSceneViewsComboBox);
    QWidget *sceneViewWidget = new QWidget();
    sceneViewWidget->setLayout(sceneViewLayout);
    MLabelledWidgetAction *sceneViewAction =
            new MLabelledWidgetAction("Save images of scene view:", "",
                                      sceneViewWidget, this);
    timeAnimationDropdownMenu->addAction(sceneViewAction);

    saveAnimationDirectoryLabel = new QLabel(
                MSystemManagerAndControl::getInstance()
                ->getMet3DWorkingDirectory().absoluteFilePath("images")
                );
    // Create directory to save images to if it does not exist.
    QDir().mkpath(saveAnimationDirectoryLabel->text());
    saveAnimationDirectoryLabel->setFixedWidth(175);
    // Set fixed size so the label won't expand the menu.
    saveAnimationDirectoryLabel->setToolTip(saveAnimationDirectoryLabel->text());
    adjustSaveAnimationDirectoryLabelText();
    saveAnimationDirectoryChangeButton = new QPushButton("...");
    QHBoxLayout *directoryLayout = new QHBoxLayout();
    directoryLayout->addWidget(saveAnimationDirectoryLabel);
    directoryLayout->addWidget(saveAnimationDirectoryChangeButton);
    QWidget *directoryWidget = new QWidget();
    directoryWidget->setLayout(directoryLayout);
    MLabelledWidgetAction *directoryAction =
            new MLabelledWidgetAction("to directory:", "", directoryWidget, this);
    timeAnimationDropdownMenu->addAction(directoryAction);

    saveAnimationFileNameLineEdit = new QLineEdit();
    saveAnimationFileNameLineEdit->setFixedWidth(190);
    saveAnimationFileNameLineEdit->setText("%fn-met3d-image.%it.%vt.%m");
    saveAnimationFileNameLineEdit->setToolTip(
                "Press return to save image. "
                "(Only if 'save animation' is active.)");
    saveAnimationFileExtensionComboBox = new QComboBox();
    QStringList imageFileExtensions;
    imageFileExtensions << ".png" << ".jpg" << ".bmp" << ".jpeg" << ".webp";
    saveAnimationFileExtensionComboBox->addItems(imageFileExtensions);
    QHBoxLayout *fileNameLayout = new QHBoxLayout();
    fileNameLayout->addWidget(saveAnimationFileNameLineEdit);
    fileNameLayout->addWidget(saveAnimationFileExtensionComboBox);
    QWidget *fileNameWidget = new QWidget();
    fileNameWidget->setLayout(fileNameLayout);
    MLabelledWidgetAction *fileNameAction =
            new MLabelledWidgetAction("file names:", "", fileNameWidget, this);
    timeAnimationDropdownMenu->addAction(fileNameAction);

    QLabel *fileNameLabel = new QLabel(
                "Use placeholder: %fn=frame number, %vt=valid time, %it=init time, %m=member");
    fileNameLabel->setToolTip("Use these placeholders to insert the according "
                              "values into the filename-string.");
    QHBoxLayout *fileNameLabelLayout = new QHBoxLayout();
    fileNameLabelLayout->addWidget(fileNameLabel);
    fileNameLabelLayout->setAlignment(fileNameLabelLayout, Qt::AlignLeft);
    QWidget *fileNameLabelWidget = new QWidget();
    fileNameLabelWidget->setLayout(fileNameLabelLayout);
    QWidgetAction *fileNameLabelAction = new QWidgetAction(this);
    fileNameLabelAction->setDefaultWidget(fileNameLabelWidget);
    timeAnimationDropdownMenu->addAction(fileNameLabelAction);

    connect(saveAnimationDirectoryChangeButton, SIGNAL(clicked()),
            SLOT(changeSaveAnimationDirectory()));
    connect(saveAnimationSceneViewsComboBox, SIGNAL(currentIndexChanged(QString)),
            this, SLOT(switchSelectedView(QString)));
    ui->animationPlayButton->setMenu(timeAnimationDropdownMenu);

    connect(ui->animationPlayButton, SIGNAL(clicked()),
            SLOT(startTimeAnimation()));
    connect(ui->animationStopButton, SIGNAL(clicked()),
            SLOT(stopTimeAnimation()));

    // Initialise camera animation controller
    cameraAnimationController = new MCameraAnimationController();

    restrictControlToDataSources();

    // Ensemble control elements.
    // =========================================================================

    connect(ui->showMeanCheckBox,
            SIGNAL(stateChanged(int)),
            SLOT(onEnsembleMeanChanged(int)));
    connect(ui->ensembleMemberComboBox,
            SIGNAL(currentIndexChanged(int)),
            SLOT(onEnsembleMemberChanged(int)));
}


MSyncControl::~MSyncControl()
{
    delete ui;
    selectedDataSourceActionList.clear();
    selectedDataSources.clear();
    delete configurationDropdownMenu;
    delete selectDataSourcesAction;
    delete cameraAnimationController;

    // Deregister all registered synchronized object since otherwise this might
    // lead to a system crash if the actor is deleted later.
    disconnectSynchronizedObjects();
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QDateTime MSyncControl::validDateTime() const
{
    return validTime;
}


void MSyncControl::setValidDateTime(const QDateTime &dateTime, bool noEvent)
{
    // Only restrict valid time to available valid times,
    // if the available valid times are set yet.
    if (!availableValidDateTimes.empty())
    {
        // Resetting to previous time - do nothing.
        if (validTime == dateTime)
        {
            return;
        }
        // Check if selected time is part of available times. If not, reset to
        // previous time.
        if (!availableValidDateTimes.contains(dateTime))
        {
            QDateTime time = handleMissingDateTime(dateTime,
                                                   validTime,
                                                   availableValidDateTimes);
            setValidDateTime(time, noEvent);
            return;
        }

    }
    validTime = dateTime;
    ui->validTimeEdit->setDateTime(validTime);
    updateTimeDifference();

#ifdef LOG_EVENT_TIMES
    LOG4CPLUS_DEBUG(mlog, "valid time change has been triggered at "
                    << MSystemManagerAndControl::getInstance()
                    ->elapsedTimeSinceSystemStart(MStopwatch::MILLISECONDS)
                    << " ms");
#endif
#ifdef DIRECT_SYNCHRONIZATION
    if (noEvent) return;
    queueSynchronizationEvent(SYNC_VALID_TIME, {QVariant(validTime)});
#else
    emit beginSynchronization();
    updateTimeDifference();
    emit validDateTimeChanged(datetime);
    emit endSynchronization();
    emitSaveImageSignal();
#endif
}


QDateTime MSyncControl::initDateTime() const
{
    return initTime;
}


void MSyncControl::setInitDateTime(const QDateTime &dateTime, bool noEvent)
{
    // Only restrict init time to available init times if they are set yet.
    if (!availableInitDateTimes.empty())
    {
        // Resetting to previous time - do nothing.
        if (initTime == dateTime)
        {
            // Don't prevent synchronisation event if both valid and init time
            // should change and valid time has changed.
            if ((integrationType == SYNC_INIT_VALID_TIME)
                    && !validDateTimeHasChanged)
            {
                return;
            }
        }
        // Check if selected time is part of available times. If not, reset to
        // previous time.
        if (!availableInitDateTimes.contains(dateTime))
        {
            QDateTime time = handleMissingDateTime(dateTime,
                                                   initTime,
                                                   availableInitDateTimes);
            setInitDateTime(time, noEvent);
            return;
        }
    }

    initTime = dateTime;
    ui->initTimeEdit->setDateTime(initTime);
    updateTimeDifference();

#ifdef DIRECT_SYNCHRONIZATION
    if (noEvent) return;
    queueSynchronizationEvent(SYNC_INIT_TIME, {QVariant(initTime)});
#else
    emit beginSynchronization();
    updateTimeDifference();
    emit initDateTimeChanged(datetime);
    emit endSynchronization();
    emitSaveImageSignal();
#endif
}


void MSyncControl::setJointInitValidDateTime(
        const QDateTime &initDateTime, const QDateTime &validDateTime)
{
    // If we cannot change either of init and valid time, just do the other one.
    if (!wouldInitDateTimeChange(initDateTime))
    {
        setValidDateTime(validDateTime);
        return;
    }
    if (!wouldValidDateTimeChange(validDateTime))
    {
        setInitDateTime(initDateTime);
        return;
    }

    // Valid time needs to be set first.
    setValidDateTime(validDateTime, true);
    setInitDateTime(initDateTime, true);
    queueSynchronizationEvent(SYNC_INIT_VALID_TIME, {{initTime}, {validTime}});
}


void MSyncControl::selectInitTimeAsListPosition(int posInAvailableInitTimes)
{
    if (availableInitDateTimes.empty())
    {
        LOG4CPLUS_ERROR(mlog, "Currently there are no init time available "
                        "in this data source - cannot select any.");
        return;
    }

    QDateTime selectedInitTime;
    if (posInAvailableInitTimes == -1)
    {
        // If the list position is specified as "-1" take the last item
        // in the list.
        selectedInitTime = availableInitDateTimes.last();
    }
    else if (posInAvailableInitTimes >= 0
             && posInAvailableInitTimes <= availableInitDateTimes.size())
    {
        selectedInitTime = availableInitDateTimes.at(posInAvailableInitTimes);
    }
    else
    {
        selectedInitTime = availableInitDateTimes.first();
    }

    // Try to select a valid time equal to the init time (i.e., lead time = 0) ...
    QDateTime selectedValidTime = selectedInitTime;
    if (!availableValidDateTimes.contains(selectedValidTime))
    {
        // ... if that time is not available as valid time, search for the
        // subsequent time in the list of valid times.
        for (QDateTime vtime : availableValidDateTimes)
        {
            if (vtime > selectedValidTime)
            {
                selectedValidTime = vtime;
                break;
            }
        }
    }

    setJointInitValidDateTime(selectedInitTime, selectedValidTime);
}


void MSyncControl::copyValidTimeToTimeAnimationFromTo()
{
//TODO (mr, 22Mar2016): Update from data sources -- MSynchronizedObject needs
//                      to provide limits of valid/init time and ens members.
    timeAnimationFrom->setDateTime(validTime);
    timeAnimationTo->setDateTime(validTime);
}


int MSyncControl::ensembleMember() const
{
    if (showEnsembleMean)
        return -1;
    else
        return selectedEnsMember;
}


void MSyncControl::registerSynchronizedClass(MSynchronizedObject *object)
{
    if (object != nullptr && ! synchronizedObjects.contains(object))
    {
        synchronizedObjects.append(object);
    }
}


void MSyncControl::deregisterSynchronizedClass(MSynchronizedObject *object)
{
    if (synchronizedObjects.contains(object))
    {
        synchronizedObjects.removeOne(object);
    }
}


void MSyncControl::updateSynchronizationOrder(MSynchronizedObject *first,
                                              MSynchronizedObject *second)
{
    if (first == nullptr || second == nullptr) return;

    int firstIndex = synchronizedObjects.indexOf(first);
    if (firstIndex == -1) return;
    int secondIndex = synchronizedObjects.indexOf(second);
    if (secondIndex == -1) return;

    if (firstIndex > secondIndex)
    {
        synchronizedObjects.swapItemsAt(firstIndex, secondIndex);
    }
}


void MSyncControl::synchronizationCompleted(MSynchronizedObject *object)
{
#ifdef SYNC_DEBUG_OUTPUT
    LOG4CPLUS_DEBUG(mlog, "SYNC: synchronizationCompleted() called by object "
                    << object);
    debugOutputSyncStatus("start of synchronizationCompleted()");
#endif

//NOTE: Each object to which in processSynchronizationEvent() a synchronization
// event was sent is allowed to call synchronizationCompleted() exactly ONCE.
// It is the responsibility of the synchronized object to make sure that no
// multiple calls are issued. If the synchronized object does not call this
// method when the event is completed, the synchronization control remains
// disabled and Met.3D gets in a locked state.

// HINT for DEBUGGING: enable "SYNC_DEBUG_OUTPUT" and make sure that
// pendingSynchronizations are empty after all computations have finished.
// -- (mr, 03Apr2019), (cf, Jul2024)

    if (object != nullptr)
    {
        if (pendingSynchronizations.contains(object))
        {
            pendingSynchronizations.remove(object);
        }
    }

    if (pendingSynchronizations.empty())
    {
        // Enable GUI for next event.
        setSynchronizationGUIEnabled(true);

        // In animation mode force an immediate repaint of the valid and init
        // time displays (otherwise they may not update during animation).
        if (animationIsRunning)
        {
            ui->validTimeEdit->repaint();
            ui->initTimeEdit->repaint();
        }

        // Last active QWdiget looses focus through disabling of sync frame
        // -- give it back.
        if (lastFocusWidget) lastFocusWidget->setFocus();

        currentSyncType = SYNC_UNKNOWN;

        endSceneSynchronization();

        // Invalidate the current sync event.
        auto finishedSyncEvent = currentSyncEvent;
        currentSyncEvent.syncType = currentSyncType;
        currentSyncEvent.syncVariants.clear();

        // Save the just completed image to file, if applicable in animation
        // mode.
        emitSaveImageSignal();
        onSynchronizationCompleted(finishedSyncEvent);
        synchronizationInProgress = false;

        // If the animation is running, start the timer until we advance
        // the time.
        if (animationIsRunning)
        {
            animationTimer->start(currentAnimDelay);
        }
    }
#ifdef SYNC_DEBUG_OUTPUT
    debugOutputSyncStatus("end of synchronizationCompleted()");
#endif
}


void MSyncControl::disconnectSynchronizedObjects()
{
    for (MSynchronizedObject *synchronizedObject : synchronizedObjects)
    {
        synchronizedObject->synchronizeWith(nullptr);
    }
    synchronizedObjects.clear();
}


unsigned int MSyncControl::getAnimationDelay_ms()
{
    return timeAnimationDelaySpinBox->value();
}


void MSyncControl::setOverwriteAnimationImageSequence(bool overwrite)
{
    overwriteAnimationImageSequence = overwrite;
}


void MSyncControl::setAnimationTimeRange(QDateTime startTime, QDateTime endTime)
{
    timeAnimationFrom->setDateTime(startTime);
    timeAnimationTo->setDateTime(endTime);
}

void MSyncControl::setupCameraSequenceBatchMode(bool useCameraSequence)
{
    timeAnimationUseSequenceAction->setChecked(useCameraSequence);
}


void MSyncControl::addDataSourceToControl(const QString& dataSourceIdentifier)
{
    QStringList updatedSelectedDataSources = this->selectedDataSources;
    updatedSelectedDataSources << dataSourceIdentifier;
    restrictControlToDataSources(updatedSelectedDataSources);
}


bool MSyncControl::dateTimeLimitReached(const QDateTime &dateTime,
                                        bool backward) const
{
    if (backward)
    {
        return dateTime <= timeAnimationFrom->dateTime();
    }
    else
    {
        return dateTime >= timeAnimationTo->dateTime();
    }
}


void MSyncControl::timeForward()
{
    integrateTimeStep(false, timeAnimationFrom->dateTime());
}


void MSyncControl::timeBackward()
{
    integrateTimeStep(true, timeAnimationTo->dateTime());
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


void MSyncControl::onTimeForwardClicked()
{
    auto *cmd = new AdvanceTimeCommand(this, false);
    MUndoStack::getInstance()->push(cmd);
}


void MSyncControl::onTimeBackwardClicked()
{
    auto *cmd = new AdvanceTimeCommand(this, true);
    MUndoStack::getInstance()->push(cmd);
}


void MSyncControl::selectDataSources()
{
    // Ask the user for data sources to which times and ensemble members the
    // sync control should be restricted to.
    MSelectDataSourceDialog dialog(this);
    if (dialog.exec() == QDialog::Rejected)
    {
        return;
    }

    QStringList dataSources = dialog.getTable()->getSelectedDataSourceIDs();

    if (dataSources.empty())
    {
        // The user has selected an emtpy set of data sources. Display a
        // warning and do NOT accept the empty set.
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("You need to select at least one data source.");
        msgBox.exec();
        return;
    }

    auto *cmd = new SelectDataSourcesCommand(this, dataSources);
    MUndoStack::run(cmd);
}


void MSyncControl::saveConfigurationToFile(QString filename)
{
    if (filename.isEmpty())
    {
        QString directory =
                MSystemManagerAndControl::getInstance()
                ->getMet3DWorkingDirectory().absoluteFilePath("config/synccontrol");
        QDir().mkpath(directory);
        filename = MFileUtils::getSaveFileName(
                nullptr,
                    "Save sync control configuration",
                    FileTypes::M_SYNC_CONTROL_CONFIG,
                    directory,
                    getID());

        if (filename.isEmpty())
        {
            return;
        }
    }

    LOG4CPLUS_INFO(mlog, "Saving configuration to " << filename);

    QSettings *settings = new QSettings(filename, QSettings::IniFormat);

    // Overwrite if the file exists.
    if (QFile::exists(filename))
    {
        QFile::remove(filename);
    }

    settings->beginGroup("FileFormat");
    // Save version id of Met.3D.
    settings->setValue("met3dVersion", met3dVersionString);
    settings->endGroup();


    settings->beginGroup("MSyncControl");
    saveConfiguration(settings);
    settings->endGroup();

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been saved.");
}


void MSyncControl::loadConfigurationFromFile(QString filename)
{
    if (filename.isEmpty())
    {
        filename = MFileUtils::getOpenFileName(
                    nullptr,
                    "Load sync control configuration",
                    FileTypes::M_SYNC_CONTROL_CONFIG,
                    MSystemManagerAndControl::getInstance()
                    ->getMet3DWorkingDirectory().absoluteFilePath("config/synccontrol"));

        if (filename.isEmpty())
        {
            return;
        }
    }

    LOG4CPLUS_INFO(mlog, "Loading configuration from " << filename);

    QSettings *settings = new QSettings(filename, QSettings::IniFormat);

    QStringList groups = settings->childGroups();
    if ( !groups.contains("MSyncControl") )
    {
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText("The selected file does not contain configuration data "
                    "for sync control.");
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
        delete settings;
        return;
    }

    settings->beginGroup("MSyncControl");
    loadConfiguration(settings);
    settings->endGroup();

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been loaded.");
}


void MSyncControl::saveConfiguration(QSettings *settings)
{
    settings->beginGroup("General");
    settings->setValue("initTime", initTime);
    settings->setValue("validTime", validTime);
    settings->setValue("stepChooseVtIt",
                      ui->stepChooseVTITComboBox->currentText());
    settings->setValue("timeStep", QString::number(deltaTimeStep)
                       + " " + ui->timeUnitsComboBox->currentText());
    settings->setValue("showMean", showEnsembleMean);
    settings->setValue("dataSources", selectedDataSources);
    settings->setValue("selectedMember",
                       QString::number(selectedEnsMember));
    settings->endGroup();

    settings->beginGroup("Animation");
    settings->setValue("animationTimeStep",
                      timeAnimationDelaySpinBox->value());
    settings->setValue("fromTime",
                      timeAnimationFrom->dateTime());
    settings->setValue("toTime",
                      timeAnimationTo->dateTime());
    settings->setValue("timeAnimationLoop",
                      timeAnimationLoopGroup->actions().indexOf(
                          timeAnimationLoopGroup->checkedAction()));
    settings->setValue("reverseTimeDirection",
                      timeAnimationReverseTimeDirectionAction->isChecked());
    settings->setValue("useCameraSequence", timeAnimationUseSequenceAction->isChecked());
    settings->endGroup();

    settings->beginGroup("TimeSeries");
    settings->setValue("fileName", saveAnimationFileNameLineEdit->text());
    settings->setValue("fileExtension",
                       saveAnimationFileExtensionComboBox->currentText());
    settings->setValue("sceneView", saveAnimationSceneViewsComboBox->currentText());
    settings->setValue("directory", saveAnimationDirectoryLabel->toolTip());
    settings->endGroup();
}


void MSyncControl::loadConfiguration(QSettings *settings)
{
    settings->beginGroup("General");

    int idx = ui->stepChooseVTITComboBox->findText(
                    settings->value("stepChooseVtIt", "valid").toString());
    setIntegrationType(static_cast<MSynchronizationType>(idx));

    QStringList timeStep =
            settings->value("timeStep", "6 hours").toString().split(" ");
    if (timeStep.size() != 2)
    {
        timeStep.clear();
        timeStep << "6" << "hours";
    }
    setDeltaTimeStep(timeStep.first().toInt());
    timeStepUnit = ui->timeUnitsComboBox->findText(timeStep.last());
    setDeltaTimeStepUnit(timeStepUnit);
    toggleEnsembleMean(settings->value("showMean", false).toBool());
    selectedDataSources =
            settings->value("dataSources",
                            MSystemManagerAndControl::getInstance()
                            ->getDataSourceIdentifiers()).toStringList();

    static const QString ENS_FILTER_SUFFIX = " ENSFilter";
    for (QString& dataSource : selectedDataSources)
    {
        if (dataSource.endsWith(ENS_FILTER_SUFFIX))
        {
            LOG4CPLUS_WARN(mlog, "Loading session that was "
                                 "saved with an older version of Met.3D. "
                                 "The selected dataset ID for the sync control "
                                 "contains the ' ENSFilter' suffix,"
                                 "which is no longer used. Removing it. "
                                 "Please save the actor or session again to avoid "
                                 "this warning in the future.");

            dataSource.replace(dataSource.length() - ENS_FILTER_SUFFIX.length(),
                               ENS_FILTER_SUFFIX.length(), QString());
        }
    }

    restrictToDataSourcesFromSettings(selectedDataSources);

    // Load times after restricting the sync control since otherwise the times
    // might not be set correctly.
    setInitDateTime(settings->value("initTime", QDateTime()).value<QDateTime>());
    setValidDateTime(settings->value("validTime", QDateTime()).value<QDateTime>());

    unsigned int selectedMember = settings->value("selectedMember", 0).toUInt();
    if (availableEnsembleMembers.contains(selectedMember))
    {
        setEnsembleMember(selectedMember);
    }
    else if (selectedMember != 0)
    {
        LOG4CPLUS_WARN(mlog, "Synchronization Control: "
                             "Member '" << selectedMember << "' is not available. "
                             "Reset to 0.");
    }

    settings->endGroup();

    settings->beginGroup("Animation");
    timeAnimationDelaySpinBox->setValue(
                settings->value("animationTimeStep", 1000).toInt());
    timeAnimationFrom->setDateTime(settings->value("fromTime").toDateTime());
    timeAnimationTo->setDateTime(settings->value("toTime").toDateTime());
    timeAnimationLoopGroup->actions().at(
                settings->value("timeAnimationLoop", 0).toInt())->setChecked(true);
    timeAnimationReverseTimeDirectionAction->setChecked(
                settings->value("reverseTimeDirection", false).toBool());
    timeAnimationUseSequenceAction->setChecked(settings->value("useCameraSequence", false).toBool());
    settings->endGroup();

    settings->beginGroup("TimeSeries");

    saveAnimationFileNameLineEdit->setText(
                settings->value("fileName",
                                "met3d-image.%it.%vt.%m").toString());

    int index = saveAnimationFileExtensionComboBox->findText(
            settings->value("fileExtension", ".png").toString());
    if (index < 0)
    {
        QString fileExt = settings->value("fileExtension", ".png").toString();
        QMessageBox::warning(this, this->getID(),
                             "The file extension '" + fileExt
                             + "' is invalid.\n"
                               "Setting file extension to '.png'.");
        index = saveAnimationFileExtensionComboBox->findText(".png");
    }
    saveAnimationFileExtensionComboBox->setCurrentIndex(index);

    QString sceneView0 = saveAnimationSceneViewsComboBox->itemText(0);
    index = saveAnimationSceneViewsComboBox->findText(
                settings->value("sceneView", sceneView0).toString());
    if (index < 0)
    {
        QString sceneView = settings->value("sceneView", sceneView0).toString();
        LOG4CPLUS_WARN(mlog, "Scene view '" + sceneView
                             + "' is invalid. Setting scene view to '"
                             + sceneView0 + "'.");
        index = 0;
    }
    saveAnimationSceneViewsComboBox->setCurrentIndex(index);

    QString defaultDir = QDir::home().absoluteFilePath("met3d/screenshots");
    QString dir = settings->value("directory", defaultDir).toString();
    QFileInfo info(dir);
    if (!(info.isDir() && info.isWritable()))
    {
        LOG4CPLUS_WARN(
            mlog, "The directory '" << dir
                      << "' referenced in the session file to save screenshots "
                         "to is either no directory or not writable. Setting "
                         "image series save directory to '"
                      << defaultDir << "'.");
        dir = defaultDir;
        // Create default directory to save screenshots to if it does not exist
        // already.
        QDir().mkpath(defaultDir);
    }
    saveAnimationDirectoryLabel->setToolTip(dir);
    saveAnimationDirectoryLabel->setText(dir);
    adjustSaveAnimationDirectoryLabelText();

    settings->endGroup();
}


void MSyncControl::restrictToDataSourcesFromSettings(
        const QStringList& dataSources)
{
    QStringList suitableDataSources;
    suitableDataSources.clear();

    if (!dataSources.empty())
    {
        MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

        // Check if at least one data source is available with values to load.
        for (const QString &dataSourceID : dataSources)
        {
            auto *source =
                    dynamic_cast<MWeatherPredictionDataSource *>
                    (sysMC->getDataSource(dataSourceID));

            // Only add data source to list of suitable data sources if it
            // contains init times, valid times and ensemble member informations.
            // Also check if it is a trajectory data source.
            if (MSelectDataSourceTable::checkDataSourceForData(source)
                || MSelectDataSourceTable::checkForTrajectoryDataSource(dataSourceID))
            {
                suitableDataSources.append(dataSourceID);
            }
            else
            {
                LOG4CPLUS_WARN(mlog, "Synchronization Control: " << dataSourceID
                                                                 << " does not exist. "
                                                                    "It should have been loaded beforehand.");
            }
        }

        if (suitableDataSources.empty())
        {
            // None of the data sources defined in frontend or save file contains init times,
            // valid time and ensemble members.
            LOG4CPLUS_WARN(mlog, "Synchronization Control: "
                                 "None of the defined data sources are currently loaded. "
                                 "Searching all currently loaded data sources for suitable ones.");

            QStringList availableDataSources = sysMC
                    ->getDataSourceIdentifiers();

            // Check for registered data source containing init times, valid
            // times and ensemble members.
            for (const QString &dataSourceID : availableDataSources)
            {
                auto *source = dynamic_cast<MWeatherPredictionDataSource *>
                (sysMC->getDataSource(dataSourceID));

                // Only add data source to list of suitable data sources if it
                // contains init times, valid times and ensemble members
                // information.
                if (MSelectDataSourceTable::checkDataSourceForData(source))
                {
                    suitableDataSources.append(dataSourceID);
                }
            }

            // None of the registered data sources contains times and ensemble
            // members information. Inform user and return.
            if (suitableDataSources.empty())
            {
                LOG4CPLUS_WARN(mlog, "Synchronization Control: "
                                     "No suitable data sources could be found.");

                if (sysMC->isUiReady())
                {
                    QMessageBox::warning(this, "Synchronization Error",
                                         "Could not load synchronization control. "
                                         "None of the required datasets are currently loaded "
                                         "and no suitable replacements have been found.");
                }
                else
                {
                    LOG4CPLUS_WARN(mlog, "Synchronization Control: "
                                         "Could not load synchronization control. "
                                         "None of the required datasets are currently loaded "
                                         "and no suitable replacements have been found.");
                }
            }
        }
    }

    // Restrict the sync control's allowed init/valid times to those available
    // from the selected data sources; also update the current from/to times
    // in the animation menu.
    restrictControlToDataSources(suitableDataSources);
    copyInitToFrom();
    copyValidToTo();
}


void MSyncControl::addAvailableTimesFromNWPSource(MWeatherPredictionDataSource* source)
{
    QList<MVerticalLevelType> levelTypes = source->availableLevelTypes();
    for (MVerticalLevelType levelType : levelTypes)
    {
        QStringList variables = source->availableVariables(levelType);

        for (QString var : variables)
        {
            QList<QDateTime> currentInitTimes = source->availableInitTimes(levelType, var);

            if (currentInitTimes.empty())
            {
                continue;
            }

            for (QDateTime initTime : currentInitTimes)
            {
                if (!availableInitDateTimes.contains(initTime))
                {
                    availableInitDateTimes.append(initTime);
                }

                QList<QDateTime> currentValidTimes = source->availableValidTimes(
                    levelType, var, initTime);
                if (currentValidTimes.empty())
                {
                    continue;
                }

                for (QDateTime validTime : currentValidTimes)
                {
                    if (!availableValidDateTimes.contains(validTime))
                    {
                        availableValidDateTimes.append(validTime);
                    }
                } // validTimes
            } // initTimes

            availableEnsembleMembers =
                availableEnsembleMembers.unite(
                    source->availableEnsembleMembers(levelType, var));

            // Sort available times for finding the nearest time if the
            // user selects a missing time.
            std::sort(availableInitDateTimes.begin(), availableInitDateTimes.end());
            std::sort(availableValidDateTimes.begin(), availableValidDateTimes.end());
        } // variables
    } // levelTypes
}


void MSyncControl::addAvailableTimesFromTrajectorySource(MTrajectoryReader* source)
{
    QList<QDateTime> currentInitTimes = source->availableInitTimes();

    if (currentInitTimes.empty())
    {
        return;
    }

    for (QDateTime initTime : currentInitTimes)
    {
        if (!availableInitDateTimes.contains(initTime))
        {
            availableInitDateTimes.append(initTime);
        }

        QList<QDateTime> currentValidTimes = source->availableValidTimes(initTime);
        if (currentValidTimes.empty())
        {
            continue;
        }

        for (QDateTime validTime : currentValidTimes)
        {
            if (!availableValidDateTimes.contains(validTime))
            {
                availableValidDateTimes.append(validTime);
            }
        } // validTimes
    } // initTimes

    availableEnsembleMembers =
        availableEnsembleMembers.unite(
            source->availableEnsembleMembers());

    // Sort available times for finding the nearest time if the
    // user selects a missing time.
    std::sort(availableInitDateTimes.begin(), availableInitDateTimes.end());
    std::sort(availableValidDateTimes.begin(), availableValidDateTimes.end());
}


void MSyncControl::restrictControlToDataSources(QStringList selectedDataSources)
{
    MSystemManagerAndControl* sysMC = MSystemManagerAndControl::getInstance();

    // Clear GUI drop down menu displaying selected data sources.
    for (QAction *action : selectedDataSourceActionList)
    {
        configurationDropdownMenu->removeAction(action);
    }
    selectedDataSourceActionList.clear();

// TODO (bt, 23Feb2017): If updated to Qt 5.0 use QSets and unite instead of
// lists and contains since for version 4.8 there is no qHash method for QDateTime
// and thus it is not possible to use toSet on QList<QDateTime>.
// (See: http://doc.qt.io/qt-5/qhash.html#qHashx)
    availableInitDateTimes.clear();
    availableValidDateTimes.clear();
    availableEnsembleMembers.clear();

    // If no list of data sources is passed to this function, use all data
    // sources registered in the system.
    if (selectedDataSources.empty())
    {
        QStringList availableDataSources = sysMC->getDataSourceIdentifiers();

        // Check for each data sourcs if it is suitable to restrict control.
        // Since in the list of all data sources might be data sources without
        // time and member informations.
        for (QString dataSourceID : availableDataSources)
        {
            MWeatherPredictionDataSource* source =
                    dynamic_cast<MWeatherPredictionDataSource*>
                    (sysMC->getDataSource(dataSourceID));
            // Only add data source to list of suitable data sources if it
            // contains init times, valid times and ensemble member
            // informations.
            if (MSelectDataSourceTable::checkDataSourceForData(source))
            {
                selectedDataSources.append(dataSourceID);
            }

            // Same for trajectory data.
            dataSourceID.chop(7);
            if (MSelectDataSourceTable::checkForTrajectoryDataSource(dataSourceID))
            {
                selectedDataSources.append(dataSourceID);
            }

        }
    }

    // If no data sources are available return from this method.
    if (selectedDataSources.empty())
    {
        return;
    }

    QStringList variables;
    QList<QDateTime> currentInitTimes;
    QList<QDateTime> currentValidTimes;
    variables.clear();
    currentInitTimes.clear();
    currentValidTimes.clear();

    this->selectedDataSources = selectedDataSources;

    // From each data source in the list, determine the available init and
    // valid times.
    for (QString dataSourceID : selectedDataSources)
    {
        MWeatherPredictionDataSource* nwpSource =
                dynamic_cast<MWeatherPredictionDataSource*>
                (sysMC->getDataSource(dataSourceID));

        if (nwpSource)
        {
            addAvailableTimesFromNWPSource(nwpSource);
        }

        MTrajectoryReader *trajReader = dynamic_cast<MTrajectoryReader *>(
            sysMC->getDataSource(dataSourceID + " Reader"));
        if (trajReader)
        {
            addAvailableTimesFromTrajectorySource(trajReader);
        }

        // Add selected data source as action to the configuration drop down
        // menu and insert the action into a list for easy remove from the menu.
        selectedDataSourceActionList.append(
                    configurationDropdownMenu->addAction(dataSourceID));

        // Make initial guess whether to synchronize only valid or both init
        // and valid times as default. Only do this when the first data set has
        // been loaded in Met.3D. Afterwards, do not force changes to the user
        // anymore.
        if (! initialVTITGuessMade)
        {
            updateSynchronizationTypeBestGuess();
            updateTimeSelectionWidgetsFromDataSources();
            initialVTITGuessMade = true;
        }
    } // dataSources
}


void MSyncControl::updateTimeSelectionWidgetsFromDataSources()
{
    if (availableInitDateTimes.isEmpty() || availableValidDateTimes.isEmpty())
    {
        // Cannot update the widget's range if no times are available.
        return;
    }

    // Store previous date times of init/valid GUI fields.
    QDateTime previousInitTime = initTime;
    QDateTime previousValidTime = validTime;
    int previousEnsembleMember = ensembleMember();

    // Search for earliest and latest init date values to restrict the init
    // time edits to them.
    QDateTime earliestDateTime = availableInitDateTimes.first();
    QDateTime latestDateTime = earliestDateTime;
    for (const QDateTime& dateTime : availableInitDateTimes)
    {
        earliestDateTime = std::min(dateTime, earliestDateTime);
        latestDateTime = std::max(dateTime, latestDateTime);
    }

    // Set time range to full day since otherwise it is not possible to change
    // the time properly for the first and last day of the range.
    ui->initTimeEdit->blockSignals(true);
    ui->initTimeEdit->setTimeRange(QTime(0,0,0), QTime(23,59,59));
    ui->initTimeEdit->setDateRange(earliestDateTime.date(), latestDateTime.date());
    ui->initTimeEdit->blockSignals(false);

    // The same for valid times.
    earliestDateTime = availableValidDateTimes.first();
    latestDateTime = earliestDateTime;
    for (const QDateTime& dateTime : availableValidDateTimes)
    {
        earliestDateTime = std::min(dateTime, earliestDateTime);
        latestDateTime = std::max(dateTime, latestDateTime);
    }

    // Set time range to full day since otherwise it is not possible to change
    // the time properly for the first and last day of the range.
    ui->validTimeEdit->blockSignals(true);
    ui->validTimeEdit->setTimeRange(QTime(0,0,0), QTime(23,59,59));
    ui->validTimeEdit->setDateRange(earliestDateTime.date(), latestDateTime.date());
    ui->validTimeEdit->blockSignals(false);

    QDateTime newInitTime;
    QDateTime newValidTime;

    // Check if previous init/valid times are still in new range of available
    // times -- if not, reset to first available init/valid time.
    if (availableInitDateTimes.contains(previousInitTime))
    {
        newInitTime = previousInitTime;
    }
    else
    {
        newInitTime = availableInitDateTimes.first();
    }

    if (availableValidDateTimes.contains(previousValidTime))
    {
        newValidTime = previousValidTime;
    }
    else
    {
        newValidTime = availableValidDateTimes.first();
    }

    // If the current sync selection is both init and valid time synchronization,
    // set both init and valid to the first same occurrence.
    if (integrationType == SYNC_INIT_VALID_TIME)
    {
        for (const auto& dateTime: availableInitDateTimes)
        {
            if (availableValidDateTimes.contains(dateTime))
            {
                newInitTime = dateTime;
                newValidTime = dateTime;
                break;
            }
        }
    }

    setJointInitValidDateTime(newInitTime, newValidTime);

    QStringList memberList;
    QList<unsigned int> intMemberList;
    memberList.clear();
    ui->ensembleMemberComboBox->clear();
    // Get list of member to be able to sort them from smallest to greatest
    // value.
    intMemberList = availableEnsembleMembers.values();
    std::sort(intMemberList.begin(), intMemberList.end());
    for (unsigned int member : intMemberList)
    {
        memberList.append(QString("%1").arg(member));
    }
    ui->ensembleMemberComboBox->addItems(memberList);
    // Restore previous ensemble member, if possible.
    setEnsembleMember(previousEnsembleMember);

    // Disable all data source entries since they are supposed to be just labels.
    for (QAction *action : selectedDataSourceActionList)
    {
        action->setEnabled(false);
    }
}


void MSyncControl::timeAnimationAdvanceTimeStep()
{
    // This method is called by the animationTimer to advance the animation
    // to the next time step.

#ifdef DIRECT_SYNCHRONIZATION
    // In case the previous request hasn't been completed yet, do nothing!
    // We will advance the time step the next time the timer times out.
    if (synchronizationInProgress)
    {
        return;
    }
#endif

    bool useAnimator = cameraAnimationController->isRunning();

    if (useAnimator)
    {
        animatorAdvanceTime |= cameraAnimationController->advanceCurrentAnimation();
    }

    if (!animatorAdvanceTime && useAnimator)
    {
        emitSaveImageSignal();
        frameNumber++;
        if (cameraAnimationController->isFinished())
        {
            stopTimeAnimation();
        }
        return;
    }

    animatorAdvanceTime = false;

    if (timeAnimationReverseTimeDirectionAction->isChecked())
    {
        timeBackward();
    }
    else
    {
        timeForward();
    }
    frameNumber++;

    if (useAnimator && cameraAnimationController->isFinished())
    {
        stopTimeAnimation();
    }
}


void MSyncControl::startTimeAnimation()
{
    if (ui->animationPlayButton->isChecked())
    {
        // Disable time control GUI elements; enable STOP button.
        ui->animationPlayButton->setEnabled(false);
        ui->animationStopButton->setEnabled(true);
        timeAnimationDropdownMenu->setEnabled(false);
        setTimeSynchronizationGUIEnabled(false);

        // Set the init/valid datetime edits to the animation start time.
        if (timeAnimationLoopGroup->checkedAction()
                == timeAnimationSinglePassAction)
        {
            if (timeAnimationReverseTimeDirectionAction->isChecked())
            {
                setAnimationTimeToStartTime(timeAnimationTo->dateTime());
            }
            else
            {
                setAnimationTimeToStartTime(timeAnimationFrom->dateTime());
            }
        }

        // Pass to scene view that generates animation images: Force to
        // overwrite images in case files already exist?
        MSceneViewGLWidget *scene = getSceneViewChosenInAnimationPane();
        scene->forceOverwriteImageSequence(
                    overwriteAnimationImageSequence);

        // Should animate through camera sequence?
        if (timeAnimationUseSequenceAction->isChecked())
        {
            MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

            int index = scene->getCameraSequenceIndex();
            if (index >= 0)
            {
                MCameraSequence *sequence = sysMC->getCameraSequence(index);
                if (sequence == nullptr)
                {
                    LOG4CPLUS_ERROR(mlog, "There is no valid camera sequence "
                                          "for index " << index << ". "
                                          "Camera sequence won't be used.");
                }
                else
                {
                    if (sequence->loopSequence)
                    {
                        bool advancedTimestepOnce = false;
                        for (MSequenceKey* key : sequence->keys)
                        {
                            if (key->advanceTimestep)
                                advancedTimestepOnce = true;
                        }
                        if (!advancedTimestepOnce)
                        {
                            LOG4CPLUS_WARN(mlog, "Camera animation sequence loops "
                                                 "and does not advance the timestep "
                                                 "at least once, resulting in an "
                                                 "infinite animation.");
                        }
                    }

                    // Set animation delay to sequence frametime.
                    // Useful for batchmode, where only the first image needs to be delayed.
                    currentAnimDelay = sequence->sequenceFrameTime;
                    animatorAdvanceTime = false;

                    cameraAnimationController->initAnimation(scene, sequence);
                }
            }
        }

        // Emit the "timeAnimationBegins" signal.
        emit timeAnimationBegins();

        // Save the current image (the next image will be stored after the next
        // synchronization event, triggered by the animationTimer, has
        // completed) UNLESS the previous synchronization request is still in
        // progress -- in this case the image will be stored upon completion
        // of synchronization (see synchronizationCompleted()).
        if (!synchronizationInProgress)
        {
            emitSaveImageSignal();
        }

        // Start the animation timer. It will periodically call
        // timeAnimationAdvanceTimeStep().

        frameNumber = 0;
        // Don't use sync control animation delay if animation controller
        // is running. We use the animations frame time instead.
        if (!cameraAnimationController->isRunning())
        {
            currentAnimDelay = timeAnimationDelaySpinBox->value();
        }

        animationTimer->start(currentAnimDelay);
        animationIsRunning = true;
    }
}


void MSyncControl::stopTimeAnimation()
{
    // Stop the animation timer.
    animationTimer->stop();
    animationIsRunning = false;

    cameraAnimationController->stopAnimation();

    // Enable time control GUI elements; disable STOP button.
    ui->animationPlayButton->setEnabled(true);
    ui->animationPlayButton->setChecked(false);
    timeAnimationDropdownMenu->setEnabled(true);
    setTimeSynchronizationGUIEnabled(true);
    ui->animationStopButton->setEnabled(false);

    // Reset option to overwrite existing images.
    getSceneViewChosenInAnimationPane()->forceOverwriteImageSequence(false);

    // Emit the "timeAnimationEnds" signal.
    emit timeAnimationEnds();
}


void MSyncControl::startTimeAnimationProgrammatically(bool saveImages)
{
    saveAnimationImages = saveImages;

    ui->animationPlayButton->setChecked(true);
    startTimeAnimation();
}


bool MSyncControl::active() const
{
    return animationIsRunning;
}


void MSyncControl::onValidDateTimeChange(const QDateTime &datetime)
{
    if (datetime == validTime) return;

    auto *cmd = new SetInitValidTimeCommand(this, datetime, true);
    MUndoStack::run(cmd);
}


void MSyncControl::onInitDateTimeChange(const QDateTime &datetime)
{
    if (datetime == initTime) return;

    auto *cmd = new SetInitValidTimeCommand(this, datetime, false);
    MUndoStack::run(cmd);
}


void MSyncControl::onEnsembleMemberChanged(int index)
{
    int member = ui->ensembleMemberComboBox->itemText(index).toInt();

    if (member == selectedEnsMember) return;

    auto *cmd = new SetEnsembleMemberCommand(this, member);
    MUndoStack::run(cmd);
}


void MSyncControl::onEnsembleMeanChanged(int index)
{
    bool showMean = ui->showMeanCheckBox->isChecked();

    if (showMean == showEnsembleMean) return;

    auto *cmd = new ToggleEnsembleMeanModeCommand(this, showMean);
    MUndoStack::run(cmd);
}


void MSyncControl::copyInitToFrom()
{
    timeAnimationFrom->setDateTime(initDateTime());
}


void MSyncControl::copyValidToFrom()
{
    timeAnimationFrom->setDateTime(validDateTime());
}


void MSyncControl::copyInitToTo()
{
    timeAnimationTo->setDateTime(initDateTime());
}


void MSyncControl::copyValidToTo()
{
    timeAnimationTo->setDateTime(validDateTime());
}


void MSyncControl::onAnimationLoopGroupChanged(QAction *action)
{
    if (action == timeAnimationSinglePassAction)
    {
        saveAnimationImages = true;
    }
    else
    {
        if (saveAnimationImages)
        {
            saveAnimationImages = false;
        }
        saveAnimationImages.setEnabled(false);
    }
}


MSceneViewGLWidget* MSyncControl::getSceneViewChosenInAnimationPane()
{
    unsigned int sceneViewID =
            saveAnimationSceneViewsComboBox->currentText().split("#").at(1).toUInt();

    for (auto* sView : MSystemManagerAndControl::getInstance()->getRegisteredViews())
    {
        if (sView->getID() + 1 == sceneViewID)
        {
            return sView;
        }
    }

    return nullptr;
}


void MSyncControl::activateTimeAnimationImageSaving(bool activate)
{
    MSceneViewGLWidget *animSceneView = getSceneViewChosenInAnimationPane();

    if (activate)
    {
        saveAnimationSceneView = animSceneView;

        connect(this, SIGNAL(imageOfTimeAnimationReady(QString, QString)),
                saveAnimationSceneView,
                SLOT(saveAnimationImage(QString, QString)));

        // Connect editable save animation gui elements to achieve saving if
        // one is changed.
        connect(saveAnimationFileNameLineEdit, SIGNAL(returnPressed()),
                this, SLOT(emitSaveImageSignal()));

        if (!animSceneView->isVisible())
        {
            QMessageBox::warning(
                        this, "Warning",
                        QString("View #%1 selected in time animation pane is not visible.\n"
                                "Please select another view or view layout.\n"
                                "(No images will be saved.)").arg(animSceneView->getID()+1));
            saveAnimationImages = false;
            return;
        }
    }
    else
    {
        disconnect(this, SIGNAL(imageOfTimeAnimationReady(QString, QString)),
                   saveAnimationSceneView,
                   SLOT(saveAnimationImage(QString, QString)));

        // Disconnect editable save animation gui elements.
        disconnect(saveAnimationFileNameLineEdit, SIGNAL(returnPressed()),
                   this, SLOT(emitSaveImageSignal()));

        animSceneView->forceOverwriteImageSequence(false);
    }
}


void MSyncControl::switchSelectedView(QString viewID)
{
    if (saveAnimationImages)
    {
        unsigned int sceneViewID = viewID.split("#").at(1).toUInt();
        MSceneViewGLWidget *currentSceneView = nullptr;
        for (MSceneViewGLWidget *sceneView :
                 MSystemManagerAndControl::getInstance()->getRegisteredViews())
        {
            if (sceneView->getID() + 1 == sceneViewID)
            {
                currentSceneView = sceneView;
                break;
            }
        }

        // Check if current view is visible. If not, deactivate auto save.
        // (This results in s disconnect-call, thus disconnect needs only to be
        // called if selected view is visible.)
        if (!currentSceneView->isVisible())
        {
            QMessageBox::warning(
                        this, "Warning",
                        QString("View #%1 is not visible.\n"
                                "Please select another view or view layout.\n"
                                "(No images will be saved.)").arg(sceneViewID));
            saveAnimationImages = false;
            return;
        }
        // Disconnect previous scene view.
        disconnect(this, SIGNAL(imageOfTimeAnimationReady(QString, QString)),
                   saveAnimationSceneView,
                   SLOT(saveAnimationImage(QString, QString)));

        saveAnimationSceneView = currentSceneView;

        // Connect selected scene view.
        connect(this, SIGNAL(imageOfTimeAnimationReady(QString, QString)),
                saveAnimationSceneView,
                SLOT(saveAnimationImage(QString, QString)));
    }
}


void MSyncControl::changeSaveAnimationDirectory()
{
    QString path = MFileUtils::getOpenDirectory(
                this, "Select directory in which image file shall be stored",
                saveAnimationDirectoryLabel->toolTip());
    if (path != "")
    {
        // Only change to directory to which Met.3D has write access.
        if (QFileInfo(path).isWritable())
        {
            saveAnimationDirectoryLabel->setText(path);
            saveAnimationDirectoryLabel->setToolTip(path);
            adjustSaveAnimationDirectoryLabelText();
        }
        else
        {
            QMessageBox msg;
            msg.setWindowTitle("Error");
            msg.setText("No write access to ''" + path + "''.");
            msg.setIcon(QMessageBox::Warning);
            msg.exec();
            return;
        }
    }
}


void MSyncControl::adjustSaveAnimationDirectoryLabelText()
{
    QString path = saveAnimationDirectoryLabel->text();
    int textWidth = saveAnimationDirectoryLabel->fontMetrics().horizontalAdvance(path);
    if (textWidth > saveAnimationDirectoryLabel->width())
    {
        int dotsWidth =
                saveAnimationDirectoryLabel->fontMetrics().horizontalAdvance("...");
        while (textWidth + dotsWidth > saveAnimationDirectoryLabel->width())
        {
            path.chop(1);
            textWidth =
                    saveAnimationDirectoryLabel->fontMetrics().horizontalAdvance(path);
        }
        saveAnimationDirectoryLabel->setText(path + "...");
    }
}


void MSyncControl::onTimeStepChanged(int newValue)
{
    if (newValue != deltaTimeStep)
    {
        auto *cmd = new ChangeDeltaTimeCommand(this, newValue);
        MUndoStack::run(cmd);
    }
}


void MSyncControl::onTimeStepUnitChanged(int index)
{
    if (timeStepUnit == index) return;

    auto *cmd = new ChangeDeltaTimeUnitCommand(this, index);
    MUndoStack::run(cmd);
}


void MSyncControl::onIntegrationTypeChanged(int index)
{
    auto syncType = static_cast<MSynchronizationType>(index);

    if (syncType == integrationType) return;

    auto *cmd = new ChangeIntegrationTypeCommand(this, syncType);
    MUndoStack::run(cmd);
}


void MSyncControl::onSynchronizationCompleted(Met3D::MSyncEvent event)
{
    if (! pendingEvents.isEmpty())
    {
        processSynchronizationEvent(pendingEvents.dequeue());
    }
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

QDateTime MSyncControl::getRelativeTimeStepTo(QDateTime time, int sign) const
{
    int timeUnit = timeStepUnit;
    int timeStep = deltaTimeStep;
    switch (timeUnit)
    {
    case 0: // seconds
    {
        return time.addSecs(sign * timeStep);
    }
    case 1: // minutes
    {
        timeStep *= 60;
        return time.addSecs(sign * timeStep);
    }
    case 2: // hours
    {
        timeStep *= 3600;
        return time.addSecs(sign * timeStep);
    }
    case 3: // days
    {
        return time.addDays(sign * timeStep);
    }
    case 4: // months
    {
        return time.addMonths(sign * timeStep);
    }
    case 5: // years
    {
        return time.addYears(sign * timeStep);
    }
    default:
        break;
    }
    return time;
}


bool MSyncControl::wouldInitDateTimeChange(const QDateTime& newInitDateTime) const
{
    if (this->initDateTime() == newInitDateTime)
    {
        return false;
    }
    if (!availableInitDateTimes.contains(newInitDateTime))
    {
        return false;
    }
    return true;
}


bool MSyncControl::wouldValidDateTimeChange(const QDateTime& newValidDateTime) const
{
    if (this->validDateTime() == newValidDateTime)
    {
        return false;
    }
    if (!availableValidDateTimes.contains(newValidDateTime))
    {
        return false;
    }
    return true;
}


void MSyncControl::updateTimeDifference()
{
    int64_t timeDifferenceSecs = initTime.secsTo(validTime);
    int64_t timeDifferenceHrs = timeDifferenceSecs / 3600;
    timeDifferenceSecs = timeDifferenceSecs % 3600;
    int64_t timeDifferenceMin = timeDifferenceSecs / 60;
    timeDifferenceSecs = timeDifferenceSecs % 60;
    QString s = QString("%1:%2:%3 hrs from").arg(
                timeDifferenceHrs, 2, 10, QLatin1Char('0')).arg(
                timeDifferenceMin, 2, 10, QLatin1Char('0')).arg(
                timeDifferenceSecs, 2, 10, QLatin1Char('0'));

    ui->differenceValidInitLabel->setText(s);
}


QDateTime MSyncControl::handleMissingDateTime(const QDateTime &dateTime,
                                              const QDateTime &oldDateTime,
                                              const QList<QDateTime> &availableDateTimes)
{
    if (availableDateTimes.isEmpty()) return dateTime;

    QDateTime newDatetime;
    if (dateTime < availableDateTimes.first())
    {
        newDatetime = availableDateTimes.first();
    }
    else if (dateTime > availableDateTimes.last())
    {
        newDatetime = availableDateTimes.last();
    }
    else
    {
        // Moving forward in time.(Find next time step bigger than current one.)
        if (dateTime > oldDateTime)
        {
            for (const QDateTime &availableTime : availableDateTimes)
            {
                newDatetime = availableTime;
                if (availableTime > dateTime)
                {
                    break;
                }
            }
        }
        // Moving backward in time.(Find next time step smaller than current one.)
        else
        {
            newDatetime = availableDateTimes.first();
            for (const QDateTime &availableTime : availableDateTimes)
            {
                if (availableTime > dateTime)
                {
                    break;
                }
                newDatetime = availableTime;
            }
        }
    }
    return newDatetime;
}


void MSyncControl::beginSceneSynchronization()
{
#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
#endif

    for (MSceneViewGLWidget *view :
             MSystemManagerAndControl::getInstance()->getRegisteredViews())
    {
        view->setFreeze(true);
    }

    emit synchronizationStarted(currentSyncEvent);
}


void MSyncControl::endSceneSynchronization()
{
    for (MSceneViewGLWidget *view :
             MSystemManagerAndControl::getInstance()->getRegisteredViews())
    {
        view->setFreeze(false);
    }

#ifdef ENABLE_MET3D_STOPWATCH
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "synchronization event processed in "
                    << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                    << " seconds.");
#endif
}


void MSyncControl::processSynchronizationEvent(const MSyncEvent &event)
{
#ifdef SYNC_DEBUG_OUTPUT
    debugOutputSyncStatus("start of processSynchronizationEvent()");
#endif

    // Begin synchronization: disable sync GUI (unless the event is caused by
    // the animationTimer; in this case the GUI remains active so the user can
    // stop the animation), tell scenes that sync begins (so they can block
    // redraws).
    // Maintain a connection to the widget as we need to set it to a nullptr if it gets
    // destroyed before the synchronization is completed.
    if (lastFocusWidget)
    {
        disconnect(lastFocusWidget, &QObject::destroyed, this, nullptr);
    }
    lastFocusWidget = QApplication::focusWidget();
    currentSyncType = event.syncType;

    if (!animationTimer->isActive()) setSynchronizationGUIEnabled(false);

    currentSyncEvent = event;
    synchronizationInProgress = true;

    beginSceneSynchronization();

    // Insert all objects into pending list, then send sync events, to
    // prevent early termination of sync.
    for (MSynchronizedObject *syncObject : synchronizedObjects)
    {
        pendingSynchronizations.insert(syncObject);
    }

    // Send sync info to each registered synchronized object. Collect those
    // objects that will process the sync request (they return true) and
    // ignore those that do not.
    for (MSynchronizedObject *syncObject : synchronizedObjects)
    {
#ifdef SYNC_DEBUG_OUTPUT
        LOG4CPLUS_DEBUG(mlog, "SYNC: sending sync info to object " << syncObj);
#endif
        if (!syncObject->synchronizationEvent(event.syncType, event.syncVariants))
        {
            pendingSynchronizations.remove(syncObject);
            continue;
        }
#ifdef SYNC_DEBUG_OUTPUT
        LOG4CPLUS_DEBUG(mlog, "SYNC: object " << syncObj << " accepted "
                            "sync info.");
#endif
    }

    // If no object accepted the sync event we can finish the sync.
    if (pendingSynchronizations.empty()) synchronizationCompleted(nullptr);

#ifdef SYNC_DEBUG_OUTPUT
    debugOutputSyncStatus("end of processSynchronizationEvent()");
#endif
}


MSyncEvent MSyncControl::queueSynchronizationEvent(
        Met3D::MSynchronizationType syncType, const QVector<QVariant>& syncVariants)
{
    MSyncEvent event = {syncType, syncVariants};
    pendingEvents.enqueue(event);

    // Process the sync event directly, if the queue is empty.
    if (pendingEvents.size() == 1)
    {
        processSynchronizationEvent(pendingEvents.dequeue());
    }

    return event;
}


void MSyncControl::setTimeSynchronizationGUIEnabled(bool enabled)
{
    ui->initTimeEdit->setEnabled(enabled);
    ui->validTimeEdit->setEnabled(enabled);
    ui->timeBackwardButton->setEnabled(enabled);
    ui->timeForwardButton->setEnabled(enabled);
    ui->timeStepSpinBox->setEnabled(enabled);
    ui->timeUnitsComboBox->setEnabled(enabled);
    ui->stepChooseVTITComboBox->setEnabled(enabled);
}


void MSyncControl::setSynchronizationGUIEnabled(bool enabled)
{
    ui->syncFrame->setEnabled(enabled);
    ui->timeBackwardButton->blockSignals(!enabled);
    ui->timeForwardButton->blockSignals(!enabled);
}


void MSyncControl::emitSaveImageSignal()
{
    if (!saveAnimationImages)
    {
        return;
    }

    // Get content of file name line edit.
    QString filename = saveAnimationFileNameLineEdit->text();

    // Replace placeholders with their according values.
    filename.replace("%fn", QString("FN%1").arg((int)frameNumber, (int)5, (int)10, QChar('0')));
    // Remove ':' from times to be able to send files to a Windows OS.
    QString initTimeStr = initDateTime().toString(Qt::ISODate);
    initTimeStr.remove(':');
    filename.replace("%it", QString("IT%1").arg(initTimeStr));

    QString validTimeStr = validDateTime().toString(Qt::ISODate);
    validTimeStr.remove(':');
    filename.replace("%vt", QString("VT%1").arg(validTimeStr));

    QString memberString = QString("M%1").arg(ensembleMember());
    // Use 'mean' instead of selected ensemble member if mean is checked.
    if (ensembleMember() == -1)
    {
        memberString = "mean";
    }
    filename.replace("%m", memberString);
    // Use tool tip to get directory since the text of the label might be
    // shorten and only the tool tip holds the whole path.
    emit imageOfTimeAnimationReady(
                saveAnimationDirectoryLabel->toolTip(),
                filename + saveAnimationFileExtensionComboBox->currentText());
}


void MSyncControl::setAnimationTimeToStartTime(const QDateTime& startDateTime)
{
    if (integrationType == SYNC_VALID_TIME)
    {
        setValidDateTime(startDateTime);
    }
    else if (integrationType == SYNC_INIT_TIME)
    {
        setInitDateTime(startDateTime);
    }
    else
    {
        setJointInitValidDateTime(startDateTime, startDateTime);
    }

}


void MSyncControl::debugOutputSyncStatus(QString callPoint)
{
    QString s = QString("SYNC: status at call point %1:\n").arg(callPoint);

    s += QString("\ncurrent sync type: %1\n").arg(currentSyncType);

    s += "\nregistered synchronized objects:\n";
    for (MSynchronizedObject* o : synchronizedObjects)
    {
        // https://stackoverflow.com/questions/8881923/how-to-convert-a-pointer-value-to-qstring
        s += QString("%1 / ").arg((quintptr)o, QT_POINTER_SIZE * 2, 16,
                                  QChar('0'));
    }

    s += "\npending synchronizations:\n";
    for (MSynchronizedObject* o : pendingSynchronizations)
    {
        s += QString("%1 / ").arg((quintptr)o, QT_POINTER_SIZE * 2, 16,
                                  QChar('0'));
    }

    s += "\n\n";

    LOG4CPLUS_DEBUG(mlog, s);
}


void MSyncControl::updateSynchronizationTypeBestGuess()
{
    MSynchronizationType guessedType = SYNC_INIT_VALID_TIME;

    for (auto &dateTime : availableValidDateTimes)
    {
        if (! availableInitDateTimes.contains(dateTime))
        {
            guessedType = SYNC_VALID_TIME;
        }
    }

    setIntegrationType(guessedType);
}


void MSyncControl::setDeltaTimeStep(int delta)
{
    deltaTimeStep = delta;
    ui->timeStepSpinBox->setValue(deltaTimeStep);
}


void MSyncControl::setDeltaTimeStepUnit(int newUnit)
{
    timeStepUnit = newUnit;
    ui->timeUnitsComboBox->setCurrentIndex(timeStepUnit);
}


void MSyncControl::setIntegrationType(MSynchronizationType type)
{
    integrationType = type;
    ui->stepChooseVTITComboBox->setCurrentIndex(type);
}


void MSyncControl::integrateTimeStep(bool backwards, const QDateTime &start)
{
    int sign = backwards ? -1 : 1;

    if (integrationType == SYNC_VALID_TIME)
    {
        if (active() && dateTimeLimitReached(validTime, backwards))
        {
            if (timeAnimationLoopTimeAction->isChecked())
            {
                setValidDateTime(start);
            }
            else if (timeAnimationBackForthTimeAction->isChecked())
            {
                timeAnimationReverseTimeDirectionAction->toggle();
            }
            else
            {
                stopTimeAnimation();
            }
        }
        else
        {
            setValidDateTime(getRelativeTimeStepTo(validTime, sign));
        }
    }
    else if (integrationType == SYNC_INIT_TIME)
    {
        if (active() && dateTimeLimitReached(initTime, backwards))
        {
            if (timeAnimationLoopTimeAction->isChecked())
            {
                setInitDateTime(start);
            }
            else if (timeAnimationBackForthTimeAction->isChecked())
            {
                timeAnimationReverseTimeDirectionAction->toggle();
            }
            else
            {
                stopTimeAnimation();
            }
        }
        else
        {
            setInitDateTime(getRelativeTimeStepTo(initTime, sign));
        }
    }
    else if (integrationType == SYNC_INIT_VALID_TIME)
    {
        if (active())
        {
            if (dateTimeLimitReached(validTime, backwards))
            {
                if (timeAnimationLoopTimeAction->isChecked())
                {
                    setValidDateTime(start);
                }
                else if (timeAnimationBackForthTimeAction->isChecked())
                {
                    timeAnimationReverseTimeDirectionAction->toggle();
                }
                else
                {
                    stopTimeAnimation();
                }
                return;
            }
            else if (dateTimeLimitReached(initTime, false))
            {
                if (timeAnimationLoopTimeAction->isChecked())
                {
                    setInitDateTime(start);
                }
                else if (timeAnimationBackForthTimeAction->isChecked())
                {
                    timeAnimationReverseTimeDirectionAction->toggle();
                }
                else
                {
                    stopTimeAnimation();
                }
                return;
            }
        }
        setJointInitValidDateTime(
                getRelativeTimeStepTo(initTime, sign),
                getRelativeTimeStepTo(validTime, sign));
    }
}


void MSyncControl::setEnsembleMember(int member)
{
    int index = ui->ensembleMemberComboBox->findText(QString("%1").arg(member));

    if (index == -1)
    {
        index = 0;
    }

    selectedEnsMember = ui->ensembleMemberComboBox->itemText(index).toInt();
    ui->ensembleMemberComboBox->setCurrentIndex(index);

#ifdef DIRECT_SYNCHRONIZATION
    if (synchronizationInProgress) return;
    synchronizationInProgress = true;
#else
    emit beginSynchronization();
#endif
    int syncMember = -1;

    if (!showEnsembleMean)
    {
        syncMember = selectedEnsMember;
    }

#ifdef DIRECT_SYNCHRONIZATION
    queueSynchronizationEvent(SYNC_ENSEMBLE_MEMBER, {QVariant(syncMember)});
#else
    emit ensembleMemberChanged(syncMember);
    emit endSynchronization();
    emitSaveImageSignal();
#endif
}


void MSyncControl::toggleEnsembleMean(bool enable)
{
    showEnsembleMean = enable;
    ui->showMeanCheckBox->setChecked(enable);

#ifdef DIRECT_SYNCHRONIZATION
    if (synchronizationInProgress) return;
    synchronizationInProgress = true;
#else
    emit beginSynchronization();
#endif

    int syncMember = selectedEnsMember;

    if (showEnsembleMean)
    {
        // Ensemble mean.
        ui->ensembleMemberComboBox->setEnabled(false);
        ui->ensembleMemberLabel->setEnabled(false);
        syncMember = -1;
    }
    else
    {
        // Change to specified ensemble member.
        ui->ensembleMemberComboBox->setEnabled(true);
        ui->ensembleMemberLabel->setEnabled(true);
    }

#ifdef DIRECT_SYNCHRONIZATION
    queueSynchronizationEvent(SYNC_ENSEMBLE_MEMBER, {QVariant(syncMember)});
#else
    emit ensembleMemberChanged(syncMember);
    emit endSynchronization();
    emitSaveImageSignal();
#endif
}


void MSyncControl::setSelectedDataSources(const QStringList &dataSources)
{
    // Restrict the sync control's allowed init/valid times to those available
    // from the selected data sources; also update the current from/to times
    // in the animation menu.
    restrictControlToDataSources(dataSources);
    // Update displayed time if previously selected time is not in restriction,
    // jump to the restricted times then.
    if (! availableInitDateTimes.contains(initDateTime()) ||
            ! availableValidDateTimes.contains(validDateTime()))
    {
        updateTimeSelectionWidgetsFromDataSources();
    }
    copyInitToFrom();
    copyValidToTo();
}


MSyncControl::AdvanceTimeCommand::AdvanceTimeCommand(MSyncControl *sync,
                                                     bool backwards)
        : QUndoCommand(backwards ? "Backtrack time" : "Advance time"),
          sync(sync),
          backward(backwards)
{
    originalInitTime = sync->initDateTime();
    originalValidTime = sync->validDateTime();
}


void MSyncControl::AdvanceTimeCommand::undo()
{
    sync->setJointInitValidDateTime(originalInitTime, originalValidTime);
}


void MSyncControl::AdvanceTimeCommand::redo()
{
    if (backward)
    {
        sync->timeBackward();
    }
    else
    {
        sync->timeForward();
    }
}


MSyncControl::ChangeDeltaTimeCommand::ChangeDeltaTimeCommand(MSyncControl *sync,
                                                             int deltaTime)
        : QUndoCommand("Set time step"),
          sync(sync),
          deltaTime(deltaTime),
          oldDeltaTime(sync->deltaTimeStep)
{
}


void MSyncControl::ChangeDeltaTimeCommand::undo()
{
    sync->setDeltaTimeStep(oldDeltaTime);
}


void MSyncControl::ChangeDeltaTimeCommand::redo()
{
    sync->setDeltaTimeStep(deltaTime);
}


MSyncControl::ChangeDeltaTimeUnitCommand::ChangeDeltaTimeUnitCommand(
        MSyncControl *sync, int unit)
        : QUndoCommand("Set time step unit"),
          sync(sync),
          unit(unit),
          oldUnit(sync->timeStepUnit)
{}


void MSyncControl::ChangeDeltaTimeUnitCommand::undo()
{
    sync->setDeltaTimeStepUnit(oldUnit);
}


void MSyncControl::ChangeDeltaTimeUnitCommand::redo()
{
    sync->setDeltaTimeStepUnit(unit);
}


MSyncControl::ChangeIntegrationTypeCommand::ChangeIntegrationTypeCommand(
        MSyncControl *sync, MSynchronizationType type)
        : QUndoCommand("Set integration type"),
          sync(sync),
          type(type),
          oldType(sync->integrationType)
{}


void MSyncControl::ChangeIntegrationTypeCommand::undo()
{
    sync->setIntegrationType(oldType);
}


void MSyncControl::ChangeIntegrationTypeCommand::redo()
{
    sync->setIntegrationType(type);
}


MSyncControl::SetInitValidTimeCommand::SetInitValidTimeCommand(
        MSyncControl *sync, QDateTime dateTime, bool isValidTime)
        : QUndoCommand("Set init time"),
          sync(sync),
          oldTime(sync->initTime),
          newTime(std::move(dateTime)),
          setValidTime(isValidTime)
{
    if (isValidTime)
    {
        setText("Set valid time");
        oldTime = sync->validTime;
    }
}


void MSyncControl::SetInitValidTimeCommand::undo()
{
    if (setValidTime)
    {
        sync->setValidDateTime(oldTime);
    }
    else
    {
        sync->setInitDateTime(oldTime);
    }
}


void MSyncControl::SetInitValidTimeCommand::redo()
{
    if (setValidTime)
    {
        sync->setValidDateTime(newTime);
    }
    else
    {
        sync->setInitDateTime(newTime);
    }
}


MSyncControl::SetEnsembleMemberCommand::SetEnsembleMemberCommand(
        MSyncControl *sync, int ensMember)
        : QUndoCommand("Change ensemble member"),
          sync(sync),
          oldEnsMember(sync->ensembleMember()),
          newEnsMember(ensMember)
{}


void MSyncControl::SetEnsembleMemberCommand::undo()
{
    sync->setEnsembleMember(oldEnsMember);
}


void MSyncControl::SetEnsembleMemberCommand::redo()
{
    sync->setEnsembleMember(newEnsMember);
}


MSyncControl::ToggleEnsembleMeanModeCommand::ToggleEnsembleMeanModeCommand(
        MSyncControl *sync, bool enable)
        : QUndoCommand("Toggle ensemble mean"),
          sync(sync),
          enable(enable)
{}


void MSyncControl::ToggleEnsembleMeanModeCommand::undo()
{
    sync->toggleEnsembleMean(!enable);
}


void MSyncControl::ToggleEnsembleMeanModeCommand::redo()
{
    sync->toggleEnsembleMean(enable);
}


MSyncControl::SelectDataSourcesCommand::SelectDataSourcesCommand(
        MSyncControl *sync, QStringList dataSources)
        : QUndoCommand("Select data sources"),
          sync(sync),
          oldDataSources(sync->selectedDataSources),
          newDataSources(std::move(dataSources))
{}


void MSyncControl::SelectDataSourcesCommand::undo()
{
    sync->setSelectedDataSources(oldDataSources);
}


void MSyncControl::SelectDataSourcesCommand::redo()
{
    sync->setSelectedDataSources(newDataSources);
}
} // namespace Met3D
