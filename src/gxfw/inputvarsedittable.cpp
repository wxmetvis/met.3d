/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "inputvarsedittable.h"

// standard library imports

// related third party imports
#include <QDialogButtonBox>
#include <QHeaderView>
#include <QVBoxLayout>
#include <QLineEdit>

// local application imports
#include "data/derivedvars/derivedmetvarsdatasource.h"
#include "util/mutil.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MInputVarsEditTable::MInputVarsEditTable(const QString &inputString,
                                         QWidget *parent)
    : QDialog(parent)
{
    setWindowTitle("Set variable names for derived data field computations");

    QMap<QString, QString> inputMap =
        inputVarsStringToMap(inputString);

    // TODO (cf, Aug2024) To get all standard names available to the derived
    //   data source, we currently need to create a temporary data source
    //   to retrieve these. Could maybe be refactored to a derived data source
    //   factory which gets populated on application start, instead of knowing
    //   the var names only after creating a derived data source.
    auto* derivedSrc = new MDerivedMetVarsDataSource();
    QStringList standardNames = derivedSrc->registeredStandardNames();
    delete derivedSrc;

    table = new QTableWidget(standardNames.size(), 2, this);
    // Set delegate which forbids special characters.
    table->setItemDelegate(new MInputVarsEditTable::MInputVarItemDelegate());
    table->setHorizontalHeaderLabels({"Standard name", "Variable in data set"});

    // Iterate over all registered standard names and create a table row for it.
    for (int i = 0; i < standardNames.size(); i++)
    {
        QString standardName = standardNames[i];
        auto standardNameItem = new QTableWidgetItem(standardName);
        standardNameItem->setFlags(standardNameItem->flags()
                                   ^ Qt::ItemIsEditable);
        table->setItem(i, 0, standardNameItem);

        // Check if standard name is provided in the input string. If so, fill
        // the cell with the corresponding variable name, otherwise leave empty.
        QString selectedVarName = inputMap.value(standardName, "");

        auto selectedNameItem = new QTableWidgetItem(selectedVarName);
        selectedNameItem->setFlags(selectedNameItem->flags()
                                   | Qt::ItemIsEditable);
        table->setItem(i, 1, selectedNameItem);
    }

    // Adjust the column widths to fit content.
    for (int column = 0; column < 2; ++column)
    {
        table->resizeColumnToContents(column);
    }

    // Set the table to stretch to available space and disable scrolling.
    table->horizontalHeader()->setStretchLastSection(true);
    table->verticalHeader()->setVisible(false);
    table->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContents);
    table->setSelectionMode(QAbstractItemView::NoSelection);
    table->sortByColumn(0, Qt::AscendingOrder);

    // Create button box with Accept and Cancel buttons.
    auto *buttonBox = new QDialogButtonBox(
        QDialogButtonBox::Ok | QDialogButtonBox::Cancel, this);

    connect(buttonBox, &QDialogButtonBox::accepted, this,
            &MInputVarsEditTable::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this,
            &MInputVarsEditTable::reject);

    // Create a layout and add the table and button box to it.
    auto *layout = new QVBoxLayout(this);
    layout->addWidget(table);
    layout->addWidget(buttonBox);

    setLayout(layout);
    adjustSize();
}


MInputVarsEditTable::~MInputVarsEditTable()
{
    delete table;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QString MInputVarsEditTable::currentMappingAsString() const
{
    QMap<QString, QString> map = currentMappingAsMap();
    return inputVarsMapToString(map);
}


QMap<QString, QString> MInputVarsEditTable::currentMappingAsMap() const
{
    QMap<QString, QString> map;

    for (int row = 0; row < table->rowCount(); row++)
    {
        // Only push to map if variable name not empty.
        QString varName = table->item(row, 1)->text();
        if (varName.trimmed().isEmpty())
        {
            continue;
        }
        map.insert(table->item(row, 0)->text(), varName.trimmed());
    }

    return map;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

QWidget *
MInputVarsEditTable::MInputVarItemDelegate::createEditor(QWidget *parent,
                                            const QStyleOptionViewItem &option,
                                            const QModelIndex &index) const
{
    auto *lineEdit = new QLineEdit(parent);

    // Set validator: Do not allow ':' and '/' as they are special characters
    // in the serialized string of the input vars. So match all strings of
    // all lengths as long as they don't (^) contain a ':' or '/'.
    QRegExp rx("[^:/]*");
    QValidator *validator = new QRegExpValidator(rx);

    lineEdit->setValidator(validator);
    return lineEdit;
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // namespace Met3D