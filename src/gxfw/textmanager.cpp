/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2023-2025 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "textmanager.h"

// standard library imports
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QApplication>
#include <QImage>
#include <QPointF>
#include <QVector>
#include <QRectF>
#include <QTextLayout>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "gxfw/msceneviewglwidget.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTextManager::MTextManager()
    : MActor(),
      fontPixelSize(-1)
{
    setName("Labels");

    // The vertex buffer is initialised in "initialize()". Assign 0 here so
    // that glDeleteBuffers() in the destructor works in case the buffer is
    // never allocated.
    directRenderingTextVBO = 0;
    directRenderingBBoxVBO = 0;

    int minASCII = 32;
    int maxASCII = 128;
    for (int i = minASCII; i < maxASCII; i++)
    {
        defaultAsciiChars.append(QChar(i));
    }
}


MTextManager::~MTextManager()
{
    // If the buffer has never been initialised, the value of
    // vertexBufferObject is 0 (see constructor). According to
    // http://www.opengl.org/sdk/docs/man4/, "glDeleteBuffers silently ignores
    // 0's and names that do not correspond to existing buffer objects.".
    glDeleteBuffers(1, &directRenderingTextVBO); CHECK_GL_ERROR;
    glDeleteBuffers(1, &directRenderingBBoxVBO); CHECK_GL_ERROR;

    // Free label resources (GPU and CPU memory).
    for (MLabel *label : labelPool)
    {
        glDeleteBuffers(1, &(label->vbo)); CHECK_GL_ERROR;
        delete label;
    }

    for (TextAtlas *atlas : fontAtlases)
    {
        delete atlas;
    }
}


void MTextManager::setDefaultFont(QString fontFile, int pixelSize)
{
    this->fontPixelSize = pixelSize;

    // Id of the font in the application font database.
    // Used to identify the font later.
    int id = QFontDatabase::addApplicationFont(fontFile);

    if (id < 0)
    {
        LOG4CPLUS_ERROR(mlog, "Could not register font " << fontFile << " in the Qt system.");
        return;
    }

    QStringList families = QFontDatabase::applicationFontFamilies(id);

    defaultFont = QFont(families.first());
}


QFont MTextManager::getDefaultFont() const
{
    return defaultFont;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

#define SHADER_VERTEX_ATTRIBUTE  0
#define SHADER_TEXTURE_ATTRIBUTE 1


void MTextManager::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");
    textEffect->compileFromFile_Met3DHome(
                "src/glsl/text.fx.glsl");
}


MLabel *MTextManager::addText(QString text,
                              Met3D::MTextManager::CoordinateSystem coordSys,
                              float x, float y, float z, float size,
                              QColor colour,
                              Met3D::MTextManager::TextAnchor anchor, bool bbox,
                              QColor bboxColour, float bboxPadFraction,
                              float rotationDeg)
{
    return addText(text, "", coordSys, x, y, z, size, colour, anchor,
                   bbox, bboxColour, bboxPadFraction, rotationDeg);
}


MLabel *MTextManager::addText(QString text, QString fontFamily,
                              CoordinateSystem coordsys, float x, float y,
                              float z, float size, QColor colour,
                              TextAnchor anchor, bool bbox, QColor bboxColour,
                              float bboxPadFraction, float rotationDeg)
{
    if (!isInitialized())
        throw MInitialisationError("cannot add text labels before the OpenGL "
                                   "context has been initialised",
                                   __FILE__, __LINE__);

    // If no font family is given, use the default font.
    if (fontFamily.isEmpty())
    {
        fontFamily = defaultFont.family();
    }

    // If the font is not yet loaded, load it.
    if (!fontAtlases.contains(fontFamily))
    {
        addFont(QFont(fontFamily));
    }

    if (text.isEmpty())
    {
        return nullptr;
    }

    // Min/max Y coordinate (character pixel space) of bounding box, updated in
    // loop below.
    float maxYOfBBox = -9999;
    float minYOfBBox =  9999;

    // Curser position in 2D character bitmap pixel space.
    float cursorX = 0;
    float cursorY = 0;

    // "coordinates" stores vertex and texture coordinates of the triangles
    // that will represent the string. The first 8 entries are used for
    // bounding box coordinates. n is a counter used to index coordinates.
	std::vector<float> coordinates(8 + 24 * text.size());
    int n = 8; // reserve first 8 entries for bbox coordinates

    // Estimate width of label in pixel size (mk: used for contour labels)
    float textWidth = 0;

    TextAtlas *fontAtlas = fontAtlases[fontFamily];

    // Layout the text and get the glyphs.
    QTextLayout layout{text, fontAtlas->font};
    layout.beginLayout();
    QTextLine line = layout.createLine();
    line.setLineWidth(999);
    line.setPosition({});
    layout.endLayout();

    // The glyph runs of the single line of text.
    // Will only contain a single run, since we only use a single font.
    QList<QGlyphRun> run = layout.glyphRuns();
    QGlyphRun textRun = run.first();
    auto indices = textRun.glyphIndexes();

    for (int i = 0; i < indices.size(); i++)
    {
        quint32 index = indices[i];

        if (!fontAtlas->contains(index))
        {
            if (fontAtlas->isFull()) continue;

            fontAtlas->insertGlyph(index);
        }

        const MTextureAtlasCharacterInfo &ci = fontAtlas->characterInfos[index];

        // Coordinate x specifies the "curser position", y the position of
        // the base line of the string. To correctly place the quad that
        // accomodates the character texture, we compute the coordinates
        // of the upper left corner of the quad, as well as its width and
        // height in clip coordinates. The variables bitmapLeft and bitmapTop
        // are provided by FreeType and describe the offset of the character
        // with respect to cursor position and baseline for correct alignment.
        float xUpperLeft = cursorX + ci.bitmapLeft;
        float yUpperLeft = cursorY + ci.bitmapTop;
        float charWidth  = static_cast<float>(ci.bitmapWidth);
        float charHeight = static_cast<float>(ci.bitmapHeight);

        textWidth += charWidth;

        // Update min/max Y coordinate (clip space) of bounding box.
        maxYOfBBox = std::max(maxYOfBBox, yUpperLeft);
        minYOfBBox = std::min(minYOfBBox, yUpperLeft - charHeight);

        // Structure of ctriangles: x1, y1, s1, t1, x2, y2, ... with x, y being
        // the clip space coordinates of each point ([-1..1x-1..1]) and s, t
        // being the corresponding texture coordinates ([0..1x0..1]).

        // FIRST TRIANGLE (lower left)
        // lower left corner of the quad
        coordinates[n++] = xUpperLeft;
        coordinates[n++] = yUpperLeft - charHeight;
        coordinates[n++] = ci.xOffset_TexCoords;
        coordinates[n++] = ci.yOffset_TexCoords;
        // upper left corner of the quad
        coordinates[n++] = xUpperLeft;
        coordinates[n++] = yUpperLeft;
        coordinates[n++] = ci.xOffset_TexCoords;
        coordinates[n++] = ci.bitmapHeight / fontAtlas->atlasHeight + ci.yOffset_TexCoords;
        // lower right corner of the quad
        coordinates[n++] = xUpperLeft + charWidth;
        coordinates[n++] = yUpperLeft - charHeight;
        coordinates[n++] = ci.xOffset_TexCoords + ci.bitmapWidth / fontAtlas->atlasWidth;
        coordinates[n++] = ci.yOffset_TexCoords;

        // SECOND TRIANGLE (upper right)
        // upper left corner of the quad
        coordinates[n++] = xUpperLeft;
        coordinates[n++] = yUpperLeft;
        coordinates[n++] = ci.xOffset_TexCoords;
        coordinates[n++] = ci.bitmapHeight / fontAtlas->atlasHeight + ci.yOffset_TexCoords;
        // lower right corner of the quad
        coordinates[n++] = xUpperLeft + charWidth;
        coordinates[n++] = yUpperLeft - charHeight;
        coordinates[n++] = ci.xOffset_TexCoords + ci.bitmapWidth / fontAtlas->atlasWidth;
        coordinates[n++] = ci.yOffset_TexCoords;
        // upper right corner of the quad
        coordinates[n++] = xUpperLeft + charWidth;
        coordinates[n++] = yUpperLeft;
        coordinates[n++] = ci.xOffset_TexCoords + ci.bitmapWidth / fontAtlas->atlasWidth;
        coordinates[n++] = ci.bitmapHeight / fontAtlas->atlasHeight + ci.yOffset_TexCoords;

        // Advance the cursor position for the next character.
        cursorX += ci.advanceX;
    }

    // Store bounding box coordinates:
    float pad = fontAtlas->maxCharHeight * bboxPadFraction;
    // lower left (lower left x, y of first triangle from ctriangles).
    coordinates[0] = coordinates[8  ] - pad;
    coordinates[1] = minYOfBBox       - pad;
    // upper left
    coordinates[2] = coordinates[8  ] - pad;
    coordinates[3] = maxYOfBBox       + pad;
    // lower right
    coordinates[4] = coordinates[n-4] + pad;
    coordinates[5] = minYOfBBox       - pad;
    // upper right (upper right x, y of the last triangle).
    coordinates[6] = coordinates[n-4] + pad;
    coordinates[7] = maxYOfBBox       + pad;

    QVector2D offset = QVector2D(0, 0); // (anchor == BASELINELEFT)
    if (anchor == BASELINERIGHT)
        offset = QVector2D(coordinates[8]-coordinates[n-4], 0);
    if (anchor == BASELINECENTRE)
        offset = QVector2D((coordinates[8]-coordinates[n-4])/2., 0);
    else if (anchor == UPPERLEFT)
        offset = QVector2D(0, cursorY-maxYOfBBox);
    else if (anchor == UPPERRIGHT)
        offset = QVector2D(coordinates[8]-coordinates[n-4], cursorY-maxYOfBBox);
    else if (anchor == UPPERCENTRE)
        offset = QVector2D((coordinates[8]-coordinates[n-4])/2., cursorY-maxYOfBBox);
    else if (anchor == LOWERLEFT)
        offset = QVector2D(0, cursorY-minYOfBBox);
    else if (anchor == LOWERRIGHT)
        offset = QVector2D(coordinates[8]-coordinates[n-4], cursorY-minYOfBBox);
    else if (anchor == LOWERCENTRE)
        offset = QVector2D((coordinates[8]-coordinates[n-4])/2., cursorY-minYOfBBox);
    else if (anchor == MIDDLELEFT)
        offset = QVector2D(0, cursorY-minYOfBBox - (maxYOfBBox-minYOfBBox)/2.);
    else if (anchor == MIDDLERIGHT)
        offset = QVector2D(coordinates[8]-coordinates[n-4],
                           cursorY-minYOfBBox - (maxYOfBBox-minYOfBBox)/2.);
    else if (anchor == MIDDLECENTRE)
        offset = QVector2D((coordinates[8]-coordinates[n-4])/2.,
                           cursorY-minYOfBBox - (maxYOfBBox-minYOfBBox)/2.);

    // Apply offset to coordinates.
    for (int i = 0; i < 8; i += 2)
    {
        // bounding box
        coordinates[i]   += offset.x();
        coordinates[i+1] += offset.y();
    }

    for (int i = 8; i < n; i += 4)
    {
        // letter "triangles"
        coordinates[i]   += offset.x();
        coordinates[i+1] += offset.y();
    }

    // All clip space labels should be on the same depth.
    if (coordsys == CLIPSPACE)
    {
        z = -1.0f;
    }

    // Create an MLabel object that stores the information necessary for
    // rendering the text later.
    MLabel* label = new MLabel();
    label->anchor           = QVector3D(x, y, z);
    label->anchorOffset     = QVector3D(); // can be set per-frame by "owner"
    label->coordinateSystem = coordsys;
    label->textColour       = colour;
    label->numCharacters    = text.size();
    label->size             = size;
    label->width            = textWidth + pad;
    label->drawBBox         = bbox;
    label->bboxColour       = bboxColour;
    label->rotationAngleDeg = rotationDeg;
    label->boundingBoxRectPx  = QRectF(QPointF(coordinates[2], coordinates[3]),
                                       QPointF(coordinates[4], coordinates[5]));
    label->fontFamily = fontFamily;

    // Make sure that "MGLResourcesManager" is the currently active context,
    // otherwise glDrawArrays on the VBO generated here will fail in any other
    // context than the currently active. The "MGLResourcesManager" context is
    // shared with all visible contexts, hence modifying the VBO there works
    // fine.
    auto c = const_cast<QGLContext*>(QGLContext::currentContext());
    MGLResourcesManager::getInstance()->makeCurrent();

    // Generate a vertex buffer object for the geometry/texture coordinates.
    glGenBuffers(1, &(label->vbo)); CHECK_GL_ERROR;
    LOG4CPLUS_TRACE(mlog, "uploading label \"" << text
                    << "\" to VBO " << label->vbo);

    // Upload data to GPU.
    glBindBuffer(GL_ARRAY_BUFFER, label->vbo); CHECK_GL_ERROR;
	// glBufferData(GL_ARRAY_BUFFER, sizeof(coordinates), !! does only get size of float pointer !!
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * coordinates.size(), 
		coordinates.data(), GL_STATIC_DRAW); CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;

    // Store label information in "labels" list.
    labelPool.insert(label);

    c->makeCurrent();

    return label;
}


void MTextManager::removeText(MLabel *label)
{
    LOG4CPLUS_TRACE(mlog, "removing label on VBO " << label->vbo);
    glDeleteBuffers(1, &(label->vbo)); CHECK_GL_ERROR;
    labelPool.remove(label);
    delete label;
}


void MTextManager::renderLabelList(MSceneViewGLWidget *sceneView,
                                   const QList<MLabel*>& labelList)
{
    // First draw the bounding boxes of all labels in the pool.
    textEffect->bindProgram("Background");

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glEnableVertexAttribArray(SHADER_VERTEX_ATTRIBUTE);
    glBindVertexArray(sceneView->defaultVao);

    for (int i = 0; i < labelList.size(); i++)
    {
        MLabel *label = labelList.at(i);

        TextAtlas *fontAtlas = fontAtlases[label->fontFamily];

        if (!label->drawBBox) continue;

        // Scales to convert from pixel space to [-1..1] clip space.
        float scale  = label->size / fontAtlas->maxCharHeight; // both in px
        float scaleX = scale * 2. / sceneView->getRenderResolutionWidth(); // latter in px
        float scaleY = scale * 2. / sceneView->getRenderResolutionHeight(); // latter in px

        QVector3D anchorInClipSpace;
        QVector3D anchorPlusOffset = label->anchor + label->anchorOffset;
        if (label->coordinateSystem == WORLDSPACE)
        {
            // Convert from world space to clip space.
            anchorInClipSpace = *(sceneView->getModelViewProjectionMatrix())
                    * anchorPlusOffset;
        }
        else if (label->coordinateSystem == LONLATP)
        {
            anchorInClipSpace = sceneView->lonLatPToClipSpace(anchorPlusOffset);
        }
        else
        {
            // labels[i]->coordinateSystem == CLIPSPACE
            anchorInClipSpace = anchorPlusOffset;
        }

        QTransform transform;
        transform.translate(anchorInClipSpace.x(), anchorInClipSpace.y());
        transform.rotate(-label->rotationAngleDeg, Qt::ZAxis);
        transform.scale(scaleX, scaleY);


        textEffect->setUniformValue("transform", transform);
        textEffect->setUniformValue("zAnchor", anchorInClipSpace.z());
        textEffect->setUniformValue("colour", label->bboxColour);

        glBindBuffer(GL_ARRAY_BUFFER, label->vbo); CHECK_GL_ERROR;
        glVertexAttribPointer(SHADER_VERTEX_ATTRIBUTE, 2, GL_FLOAT,
                              GL_FALSE, 0, (const GLvoid *)0);
        glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    }

    glDisable(GL_POLYGON_OFFSET_FILL);

    textEffect->bindProgram("TextPool");

    glEnableVertexAttribArray(SHADER_VERTEX_ATTRIBUTE); CHECK_GL_ERROR;
    glEnableVertexAttribArray(SHADER_TEXTURE_ATTRIBUTE); CHECK_GL_ERROR;

    for (int i = 0; i < labelList.size(); i++)
    {
        MLabel *label = labelList.at(i);

        TextAtlas *fontAtlas = fontAtlases[label->fontFamily];

        QMatrix2x2 rotation;
        rotation(0, 0) = std::cos(label->rotationAngleDeg * (M_PI / 180.f));
        rotation(0, 1) = -std::sin(label->rotationAngleDeg * (M_PI / 180.f));
        rotation(1, 0) = std::sin(label->rotationAngleDeg * (M_PI / 180.f));
        rotation(1, 1) = std::cos(label->rotationAngleDeg * (M_PI / 180.f));

        // Scales to convert from pixel space to [-1..1] clip space.
        float scale  = label->size / fontAtlas->maxCharHeight; // both in px
        float scaleX = scale * 2. / sceneView->getRenderResolutionWidth(); // latter in px
        float scaleY = scale * 2. / sceneView->getRenderResolutionHeight(); // latter in px

        QVector3D anchorInClipSpace;
        QVector3D anchorPlusOffset = label->anchor + label->anchorOffset;
        if (label->coordinateSystem == WORLDSPACE)
        {
            // Convert from world space to clip space.
            anchorInClipSpace = *(sceneView->getModelViewProjectionMatrix())
                    * anchorPlusOffset;
        }
        else if (label->coordinateSystem == LONLATP)
        {
            anchorInClipSpace = sceneView->lonLatPToClipSpace(anchorPlusOffset);
        }
        else
        {
            // labels[i]->coordinateSystem == CLIPSPACE
            anchorInClipSpace = anchorPlusOffset;
        }

        QTransform transform;
        transform.translate(anchorInClipSpace.x(), anchorInClipSpace.y());
        transform.rotate(-label->rotationAngleDeg, Qt::ZAxis);
        transform.scale(scaleX, scaleY);

        glActiveTexture(GL_TEXTURE0); CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D, fontAtlas->atlasTexture->getTextureObject()); CHECK_GL_ERROR;
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
        glDepthFunc(GL_LEQUAL);

        textEffect->setUniformValue("transform", transform);
        textEffect->setUniformValue("zAnchor", anchorInClipSpace.z());
        textEffect->setUniformValue("colour", label->textColour);
        textEffect->setUniformValue("textAtlas", 0);

        glBindBuffer(GL_ARRAY_BUFFER, label->vbo); CHECK_GL_ERROR;
        glVertexAttribPointer(SHADER_VERTEX_ATTRIBUTE, 2, GL_FLOAT,
                              GL_FALSE, 4 * sizeof(float),
                              (const GLvoid *)(8 * sizeof(float))); // offset for vertex positions
        glVertexAttribPointer(SHADER_TEXTURE_ATTRIBUTE, 2, GL_FLOAT,
                              GL_FALSE, 4 * sizeof(float),
                              (const GLvoid *)(10 * sizeof(float))); // offset textures
        glDrawArrays(GL_TRIANGLES, 0, 6 * label->numCharacters);
    }

    glDepthFunc(GL_LESS);
    glDisableVertexAttribArray(SHADER_VERTEX_ATTRIBUTE);
    glDisableVertexAttribArray(SHADER_TEXTURE_ATTRIBUTE);
}


QRectF MTextManager::getLabelClipSpaceBounds(
        Met3D::MSceneViewGLWidget *sceneView, Met3D::MLabel *label)
{

    TextAtlas *fontAtlas = fontAtlases[label->fontFamily];
    float scale  = label->size / fontAtlas->maxCharHeight; // both in px
    float scaleX = scale * 2. / sceneView->getRenderResolutionWidth(); // latter in px
    float scaleY = scale * 2. / sceneView->getRenderResolutionHeight(); // latter in px

    QVector3D anchorInClipSpace;
    QVector3D anchorPlusOffset = label->anchor + label->anchorOffset;
    if (label->coordinateSystem == WORLDSPACE)
    {
        // Convert from world space to clip space.
        anchorInClipSpace = *(sceneView->getModelViewProjectionMatrix())
                * anchorPlusOffset;
    }
    else if (label->coordinateSystem == LONLATP)
    {
        anchorInClipSpace = sceneView->lonLatPToClipSpace(anchorPlusOffset);
    }
    else
    {
        anchorInClipSpace = anchorPlusOffset;
    }

    QRectF clipSpacePos = label->boundingBoxRectPx;
    QPointF blc = clipSpacePos.bottomLeft();
    QPointF trc = clipSpacePos.topRight();
    blc.setX(blc.x() * scaleX);
    blc.setY(blc.y() * scaleY);
    trc.setX(trc.x() * scaleX);
    trc.setY(trc.y() * scaleY);
    clipSpacePos.setBottomLeft(blc);
    clipSpacePos.setTopRight(trc);
    clipSpacePos.translate(anchorInClipSpace.x(), anchorInClipSpace.y());

    return clipSpacePos;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MTextManager::initializeActorResources()
{
    // Font size must be specified before this method is called.
    // The default font is always initialized, either by setting it to
    // the font file provided in the frontend configuration, or when that is
    // not provided, the default application font for qt is used instead.
    if ((fontPixelSize < 0))
    {
        throw MInitialisationError("Font size must be specified "
                                   "before OpenGL intialisation of "
                                   "MTextManager.", __FILE__, __LINE__);
    }

    addFont(defaultFont);

    // Generate vertex buffer objects.
    glGenBuffers(1, &directRenderingTextVBO); CHECK_GL_ERROR;
    glGenBuffers(1, &directRenderingBBoxVBO); CHECK_GL_ERROR;

    // Load shader.
    bool loadShaders = false;
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    loadShaders |= glRM->generateEffectProgram("text_shader", textEffect);

    if (loadShaders) reloadShaderEffects();
}


void MTextManager::addFont(const QFont &font)
{
    auto *atlas = new TextAtlas(font, fontPixelSize);

    addTextAtlas(atlas);
}


void MTextManager::addTextAtlas(MTextManager::TextAtlas *atlas)
{
    if (!atlas->rawFont.isValid())
    {
        return;
    }

    QString name = atlas->font.family();

    if (fontAtlases.contains(name))
    {
        return;
    }

    atlas->generateAtlas(defaultAsciiChars);

    supportedFontFamilies.append(name);
    fontAtlases.insert(name, atlas);
}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/


MTextManager::TextAtlas::TextAtlas(const QFont &font, int pixelSize)
    : font(font),
      rawFont(QRawFont::fromFont(font)),
      atlasTexture(nullptr),
      atlasHeight(0),
      atlasWidth(0),
      maxCharWidth(0),
      maxCharHeight(0),
      numGlyphs(0)
{
    rawFont.setPixelSize(pixelSize);
}


MTextManager::TextAtlas::~TextAtlas()
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    glRM->releaseGPUItem(atlasTexture);
}


bool MTextManager::TextAtlas::generateAtlas(const QVector<QChar> &chars)
{
    // Reference:
    // http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_01
    LOG4CPLUS_DEBUG(mlog, "Generating font atlas for " << font.family());

    // Insert a horizontal padding of "pad" pixels between all characters to
    // avoid rendering artefacts due to interpolation in texture space (e.g.
    // a vertical line on the right of a rendered "O" resulting from the right
    // border of the adjacent "P". One pixel should usually be enough.
    // Also pad vertically.
    const static int pad = 1;

    if (!rawFont.isValid())
    {
        LOG4CPLUS_ERROR(mlog, "Could not load font " << font.family());
        return false;
    }

    maxCharWidth = rawFont.maxCharWidth() + pad * 2;
    maxCharHeight = rawFont.ascent() + rawFont.descent() + 1 + pad * 2;
    atlasWidth = maxCharWidth * charsPerRow;
    atlasHeight = maxCharHeight * charsPerCol;

    LOG4CPLUS_DEBUG_FMT(mlog, "\ttexture atlas: width %i, height %i",
                        atlasWidth, atlasHeight);

    // Check if a texture of this size is supported by the graphics hardware.
    GLint maxTexSize;
    glGetIntegerv(GL_MAX_TEXTURE_SIZE, &maxTexSize);
    if ((atlasWidth > maxTexSize) || (atlasHeight > maxTexSize))
    {
        QString error = QString(
                "texture atlas is too large, textures can "
                "have a maximum size of %1 pixels in each "
                "dimension -- try to decrease font size").arg(maxTexSize);
        throw MInitialisationError(error.toStdString(), __FILE__, __LINE__);
    }

    // Obtain a texture object name from the resources' manager. If successfull,
    // create an empty texture of the determined size, fill it with zeros, then
    // write bitmaps of the individual characters to the texture.

    bool existed = false;

    auto *glRM = MGLResourcesManager::getInstance();

    atlasTexture = glRM->createTexture("font_atlas_" + font.family() + "_" + rawFont.styleName(),
                            &existed,GL_TEXTURE_2D, GL_R8,atlasWidth,atlasHeight);

    if (!existed)
    {
        LOG4CPLUS_DEBUG(mlog, "\tloading texture atlas to texture object "
                << atlasTexture->getTextureObject());

        // Bind texture object and set its parameters.
        glActiveTexture(GL_TEXTURE0); CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D, atlasTexture->getTextureObject()); CHECK_GL_ERROR;
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1); CHECK_GL_ERROR;

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

        // Allocate GPU memory and fill the texture with zeros. If this is
        // omitted the texture contains undefined regions (e.g. below a
        // vertically small character such as "-"), which result in similar
        // rendering artefacts as the "O"-"P" problem described above.

        // NOTE: The texture is stored in the alpha channel -- this way the
        // alpha component can be directly used in the shader to render the
        // text with "transparent" whitespace, e.g. in "O".

        unsigned char* nullData = new unsigned char[atlasWidth * atlasHeight];
        for (int i = 0; i < atlasWidth * atlasHeight; i++)
        {
            nullData[i] = 0;
        }

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RED,
                     atlasWidth, atlasHeight,
                     0, GL_RED, GL_UNSIGNED_BYTE,
                     nullData); CHECK_GL_ERROR;

        // Load a bitmap of each character (glyph) and write it to the texture.
        // Save information about the position of the glyph, its width and
        // height and alignment in the array "characterInfo".

        // Start at the left of the image and advance this "cursor" with every
        // character.

        for (int yc = 0; yc < charsPerCol; yc++)
        {
            for (int xc = 0; xc < charsPerRow; xc++)
            {
                int i = xc + yc * charsPerRow;

                if (i >= chars.length()) break;

                insertChar(chars.at(i));
            }
        }

        // Generate a mipmap for the texture.
        glGenerateMipmap(GL_TEXTURE_2D); CHECK_GL_ERROR;

        delete[] nullData;

        return true;
    }

    return false;
}


bool MTextManager::TextAtlas::insertChar(const QChar &c)
{
    if (numGlyphs >= charsPerRow * charsPerCol) return false;

    auto indices = rawFont.glyphIndexesForString({c});

    if (indices.empty()) return false;

    // The glyph index for the char. Some chars are assembled from multiple
    // glyphs, but that is not yet supported.
    quint32 idx = indices.first();

    return insertGlyph(idx);
}


bool MTextManager::TextAtlas::insertGlyph(const quint32 &idx)
{
    if (characterInfos.contains(idx)) return false;

    int xCoord_TextureSpace = (numGlyphs % charsPerRow) * maxCharWidth;
    int yCoord_TextureSpace = (numGlyphs / charsPerRow) * maxCharHeight;

    QImage bitmap = rawFont.alphaMapForGlyph(idx, QRawFont::PixelAntialiasing);
    // The advance for the glyph. This is the distance from
    // the left start of the glyph to the next glyph in pixels.
    QPointF advance;
    rawFont.advancesForGlyphIndexes(&idx, &advance, 1);
    auto bounds = rawFont.boundingRect(idx);

    const int channels = 1;
    QVector<GLubyte> colData(bitmap.width() * bitmap.height() * channels);
    for (int y = 0; y < bitmap.height(); y++)
    {
        for (int x = 0; x < bitmap.width(); x++)
        {
            QRgb val = bitmap.pixel(x, y);
            uint32_t index = ((bitmap.height() - 1 - y) * bitmap.width() + x) * channels;
            colData[index] = qAlpha(val);
        }
    }

    // Bind texture object and set its parameters.
    glActiveTexture(GL_TEXTURE0); CHECK_GL_ERROR;
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    // Upload image to texture.
    glTexSubImage2D(GL_TEXTURE_2D, 0, xCoord_TextureSpace, yCoord_TextureSpace,
                    bitmap.width(), bitmap.height(),
                    GL_RED, GL_UNSIGNED_BYTE, colData.data());
    CHECK_GL_ERROR;

    characterInfos.insert(idx, {});
    MTextureAtlasCharacterInfo &info = characterInfos[idx];
    numGlyphs++;

    info.advanceX = static_cast<float>(std::round(advance.x()));
    info.advanceY = static_cast<float>(std::round(advance.y()));
    info.bitmapWidth = bitmap.width();
    info.bitmapHeight = bitmap.height();
    info.bitmapLeft = bounds.x();
    info.bitmapTop = -bounds.y();
    info.xOffset_TexCoords = (float) xCoord_TextureSpace / atlasWidth;
    info.yOffset_TexCoords = (float) yCoord_TextureSpace / atlasHeight;
    return true;
}

} // namespace Met3D
