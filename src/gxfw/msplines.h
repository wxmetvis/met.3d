/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MSPLINES_H
#define MET_3D_MSPLINES_H

// standard library imports
#include <algorithm>
#include <iostream>
#include <cmath>

// related third party imports
#include <QVector>
#include <QVector2D>
#include <QVector3D>
#include <QStack>

// local application imports

namespace Met3D
{

/**
 * An abstract class representing a generic spline implementation.
 * @tparam T The type of the spline points and interpolated values.
 */
template<typename T>
class MAbstractSpline
{
public:
    MAbstractSpline()
            : splinePoints(),
              splineLength(0),
              closed(false),
              autoUpdates(false)
    {}


    /**
     * @return A vector of all points the spline consists of.
     */
    QVector<T> &points() const
    {
        return splinePoints;
    }


    /**
     * @return The amount of points in the spline.
     */
    int size() const
    {
        return splinePoints.length();
    }


    /**
     * @return The total length of the spline.
     */
    float length() const
    {
        return splineLength;
    }


    /**
     * Set whether the spline should connect the first and last points or not.
     * @param closeSpline Whether to close the spline or not.
     */
    void setClosed(const bool closeSpline)
    {
        closed = closeSpline;
    }


    /**
     * Set the spline to automatically update itself with each change that occurs to it.
     * @param autoUpdate Whether to auto-update or not.
     */
    void setAutomaticUpdate(const bool autoUpdate)
    {
        autoUpdates = autoUpdate;
    }


    /**
     * Gets the spline point at the given index, if it exists.
     * @param pointIndex The index of the point in the spline.
     * @return The point corresponding to the given index, or the default value of T.
     */
    T get(const int pointIndex) const
    {
        if (pointIndex < 0 || pointIndex >= splinePoints.length()) return T();
        return splinePoints[pointIndex];
    }


    /**
     * Adds a single point to the spline.
     * @param point The spline point to add.
     * @return The index of the point in the spline, or -1 if it couldn't be added.
     */
    virtual int add(T point) = 0;

    /**
     * Adds multiple points to the spline.
     * @param points A Vector of points being added to the spline.
     * @return The start index of the new points in the spline, or -1 if they couldn't be added.
     */
    virtual int add(const QVector<T> &points) = 0;

    /**
     * Insert a single new point into the spline.
     * @param point Point to be inserted.
     * @param index Index, at which the point is inserted into.
     * @return Whether or not the point was successfully inserted.
     */
    virtual bool insert(T point, int index) = 0;

    /**
     * Remove a single spline point from the given index from the spline.
     * @param index Index of the point that should be removed.
     * @return True, if the point could be removed, false otherwise.
     */
    virtual bool remove(int index) = 0;

    /**
     * Interpolate a point on the spline at the given distance from the splines starting point.
     * @param distance Distance to sample the point at.
     * @return The point at the given distance.
     */
    virtual T interpolate(float distance) const = 0;

    /**
     * Trigger a manual recalculation of the whole spline from its current points.
     */
    virtual void update() = 0;

protected:
    QVector<T> splinePoints;
    float splineLength;
    bool closed;
    bool autoUpdates;
};


/**
 * A basic implementation of the cubic hermetic spline.
 * @tparam T The type of the spline points and interpolated values.
 */
template<typename T>
class MCubicHermeticSpline : public MAbstractSpline<T>
{
protected:
    using MAbstractSpline<T>::splinePoints;
    using MAbstractSpline<T>::splineLength;
    using MAbstractSpline<T>::closed;
    using MAbstractSpline<T>::autoUpdates;

public:
    int add(T point) override
    {
        splinePoints.append(point);
        int index = splinePoints.length() - 1;

        if (autoUpdates)
        {
            updateSegment(index - 2);
            updateSegment(index - 1);
            updateSegment(index);
        }
        return index;
    }


    int add(const QVector<T> &points) override
    {
        splinePoints.append(points);
        int index = splinePoints.length() - points.length();

        if (autoUpdates)
        {
            updateSegment(index - 2);
            updateSegment(index - 1);
            for (int i = 0; i < points.length(); i++)
            {
                updateSegment(index + i);
            }
        }

        return index;
    }


    bool insert(T point, int index) override
    {
        if (index > splinePoints.length() || index < 0) return false;
        splinePoints.insert(index, point);

        if (autoUpdates)
        {
            updateSegment(index - 2);
            updateSegment(index - 1);
            updateSegment(index);
            updateSegment(index + 1);
            updateSegment(index + 2);
        }

        return true;
    }


    bool remove(const int index) override
    {
        if (index >= splinePoints.length() || index < 0) return false;
        splinePoints.remove(index);

        if (autoUpdates)
        {
            updateSegment(index - 1);
            updateSegment(index);
            updateSegment(index + 1);
        }

        return true;
    }


    T interpolate(float distance) const override
    {
        // This condition checks, whether the spline is initialized (first update when not using auto update).
        if (splinePoints.length() > 0 && segmentStarts.empty()) return T();

        int lastSegment = closed ? splinePoints.length() - 1 : splinePoints.length() - 2;

        if (closed)
        {
            distance = fmod(distance, splineLength);
        }

        // 1. Find segment that contains the given distance.
        int segment;
        for (segment = lastSegment; segment >= 0; segment--)
        {
            if (segmentStarts[segment] <= distance) break;
        }

        double distanceInSegment = distance - segmentStarts[segment];

        // 2. Find closest t within segment for the searched distance.
        double t = 0.0;
        double deltaT = 0.1;
        double currentDistance = 0.0;
        T currentPoint = splinePoints[segment];

        // Search for the closest t until a certain threshold is met,
        // 100% percent accuracy is not possible, when searching for
        // a point at a certain distance from start.
        while (abs(currentDistance - distanceInSegment) > 0.0001 && t <= 1.0 && t >= 0.0)
        {
            // 1. Loop forward, until we step over the searched distance.
            while (currentDistance < distanceInSegment && t <= 1.0)
            {
                t += deltaT;

                T p2 = interpolate(segment, t);

                currentDistance += euclideanDistance(currentPoint, p2);

                currentPoint = p2;
            }

            // 2. Make delta smaller, to refine search.
            deltaT /= 10.0;

            // 3. Loop backwards again, until we overstep again.
            while (currentDistance > distanceInSegment && t >= 0.0)
            {
                t -= deltaT;

                T p2 = interpolate(segment, t);

                currentDistance -= euclideanDistance(currentPoint, p2);

                currentPoint = p2;
            }

            // 4. Again, make delta smaller to refine the search.
            deltaT /= 10.0;
        }

        // 3. Return point
        return currentPoint;
    }


    void update() override
    {
        segmentLengths.clear();
        segmentStarts.clear();

        splineLength = 0;

        double segmentLength;
        for (int segment = 0; segment < splinePoints.length();)
        {
            segmentStarts.append(splineLength);
            segmentLength = distanceOnSpline(segment, 0.0, 1.0);
            segmentLengths.append(segmentLength);

            splineLength += segmentLength;

            segment++;

            // If the spline is not closed, the last spline point is not a segment start.
            if (!closed && segment == splinePoints.length() - 1) break;
        }
    }


    /**
     * Updates segment k of the spline. This re-calculates its length and start distance,
     * as well as all following segment start distances.
     * @param k Segment to update.
     */
    void updateSegment(int k)
    {
        if (k < 0) return;
        if (k > splinePoints.length() - 1 && !closed) return;
        if (splinePoints.length() == 1) return;

        k = k % splinePoints.length();

        double length = distanceOnSpline(k, 0.0, 1.0);

        bool shouldAppend = segmentStarts.length() <= k;

        if (shouldAppend)
        {
            double start = 0.0;

            if (k > 0)
            {
                start = segmentStarts[k - 1] + segmentLengths[k - 1];
            }

            segmentStarts.append(start);
            segmentLengths.append(length);
        }
        else
        {
            splineLength -= segmentLengths[k];
            segmentLengths[k] = length;

            // Update all following section starts.
            for (int i = k + 1; i < segmentStarts.length(); i++)
            {
                segmentStarts[i] = segmentStarts[i - 1] + segmentLengths[i - 1];
            }
        }

        splineLength += length;
    }


    /**
     * Segment wise interpolation for the spline. Interpolates a point in section k of the spline
     * at t.
     * @param k Index of the spline segment.
     * @param t Value in range [0,1] that specifies where the point is located in the spline segment.
     * @return The interpolated point at t in spline segment k.
     */
    T interpolate(int k, double t) const
    {
        if (k < 0) return T();
        if (k >= splinePoints.length() - 1 && !closed) return T();

        if (closed) k = k % splinePoints.length();

        int k2 = (k + 1) % splinePoints.length();

        T p0 = splinePoints[k];
        T p1 = splinePoints[k2];

        t = std::min(std::max(t, 0.0), 1.0);

        double h00 = 2.0 * t * t * t - 3 * t * t + 1;
        double h10 = t * t * t - 2.0 * t * t + t;
        double h01 = -2.0 * t * t * t + 3.0 * t * t;
        double h11 = t * t * t - t * t;

        T m0 = tangent(k);
        T m1 = tangent(k2);

        return h00 * p0 + h10 * m0 + h01 * p1 + h11 * m1;
    }


    /**
     * Calculates the distance between two points of a single spline segment.
     * @param k Index of the spline segment.
     * @return The arc-length of the segment.
     */
    double distanceOnSpline(int k, double t1, double t2) const
    {
        if (k < 0) return 0;
        if (k > splinePoints.length() - 1 && !closed) return 0;

        int n = splinePoints.length();
        k = (k % splinePoints.length() + n) % n;

        if (t1 > t2)
        {
            std::swap(t1, t2);
        }

        t1 = fmax(t1, 0.0);
        t2 = fmin(t2, 1.0);

        // 2 ways to implement.
        // 1. start at t1, move in small t intervals towards t2 and calculate distance (faster)
        // 2. Start in the middle of t1 and t2, calculate distance, check whether individual segments are small enough, if not, split. (more accurate)
        // Check speed and accuracy on complex splines to decide which implementation we take.
#define BINARY_CALC_SPLINE
#ifdef BINARY_CALC_SPLINE
        QStack<QPair<double, double>> stack;
        double distance = 0;
        double lastHalfDistance = -1;

        stack.push(QPair<double, double>(t1, t2));

        while (!stack.empty())
        {
            QPair<double, double> section = stack.pop();
            double tStart = section.first;
            double tEnd = section.second;

            T p1 = interpolate(k, tStart);
            T p2 = interpolate(k, tEnd);

            double d = euclideanDistance(p1, p2);

            if (d > 0.01)
            {
                // Check if we actually do get a more accurate result with this split,
                // if we do not, we already have our result.
                if (lastHalfDistance != d)
                {
                    stack.push(QPair<double, double>(tStart, (tEnd - tStart) / 2.0 + tStart));
                    stack.push(QPair<double, double>((tEnd - tStart) / 2.0 + tStart, tEnd));

                    lastHalfDistance = d / 2.0;
                    continue;
                }
                else
                {
                    distance += d;
                }
            }
            else
            {
                distance += d;
            }
        }
        return distance;
#else
        double t = t1;
        double dT = 0.001;
        double d = 0;

        T p1 = interpolate(k, t);
        T p2;
        while (t < t2)
        {
            t += dT;

            p2 = interpolate(k, t);

            d += euclideanDistance(p1, p2);
        }

        return d;
#endif
    }


    /**
     * Get the value at t of the first derivative of the spline segment k.
     * @param k Index of the spline segment.
     * @return The value of the first derivative of the spline segment k at t [0,1]
     */
    T derivative(int k, double t) const
    {
        if (k < 0) return T();
        if (k >= splinePoints.length() - 1 && !closed) return T();

        if (closed) k = k % splinePoints.length();

        int k2 = (k + 1) % splinePoints.length();

        T p0 = splinePoints[k];
        T p1 = splinePoints[k2];

        t = std::min(std::max(t, 0.0), 1.0);

        double h00 = 6.0 * t * t - 6.0 * t;
        double h01 = -6.0 * t * t + 6.0 * t;
        double h10 = 3.0 * t * t - 4.0 * t + 1.0;
        double h11 = 3.0 * t * t - 2.0 * t;

        T m0 = tangent(k);
        T m1 = tangent(k2);

        return h00 * p0 + h10 * m0 + h01 * p1 + h11 * m1;
    }


    /**
     * Get the segment start distance of segment k from the spline start point.
     * @param k Segment index.
     * @return The segment start distance or -1 if the spline is invalid or k is invalid.
     */
    double segmentStart(int k) const
    {
        if (k < 0) return -1.0;
        if (k >= splinePoints.length() - 1 && !closed) return -1.0;

        if (closed) k = k % splinePoints.length();

        if (segmentStarts.length() <= k) return -1.0;

        return segmentStarts[k];
    }


    /**
     * Get the segment length for segment k.
     * @param k Segment index.
     * @return Segment length of k or -1 if the spline is invalid or k is invalid.
     */
    double segmentLength(int k) const
    {
        if (k < 0) return -1.0;
        if (k >= splinePoints.length() - 1 && !closed) return -1.0;

        if (closed) k = k % splinePoints.length();

        if (segmentLengths.length() <= k) return -1.0;

        return segmentLengths[k];
    }


    /**
     * Search for the segment that the given distance from the spline start lies in.
     * @param distance Distance from spline start.
     * @return The segment index or -1 if it does not exist or the spline is invalid.
     */
    int findSegment(double distance) const
    {
        if (splinePoints.length() > 0 && segmentStarts.empty()) return -1;

        int lastSegment = closed ? splinePoints.length() - 1 : splinePoints.length() - 2;
        distance = fmod(distance, splineLength);

        // 1. Find segment that contains the given distance.
        int segment;
        for (segment = lastSegment; segment >= 0; segment--)
        {
            if (segmentStarts[segment] <= distance) return segment;
        }

        return -1;
    }


    /**
     * Calculate the tangent at the spline point k.
     * @param k Index of the spline point, where the tangent is calculated.
     * @return The tangent at the spline point at index k.
     */
    virtual T tangent(int k) const
    {
        if (k < 0) return T();
        if (k > splinePoints.length() - 1 && !closed) return T();

        if (closed) k = k % splinePoints.length();

        int n = splinePoints.length();

        int kLeft = ((k - 1) % n + n) % n;
        int kRight = (k + 1) % n;

        if (!closed)
        {
            if (k == 0) kLeft = 0;
            if (k == n - 1) kRight = n - 1;
        }

        T pLeft = splinePoints[kLeft];
        T pRight = splinePoints[kRight];
        T pK = splinePoints[k];

        // Finite distance tangent.
        return (pK - pLeft) / 2.0 + (pRight - pK) / 2.0;
    }


protected:
    /**
     * Calculates the euclidean distance between points p1 and p2.
     * @param p1 Point 1.
     * @param p2 Point 2.
     * @return Euclidean distance between point 1 and 2.
     */
    double euclideanDistance(const T &p1, const T &p2) const;

private:
    QVector<double> segmentLengths;
    QVector<double> segmentStarts;
};

// Template specializations for euclidean distance, as vector 2d and 3d need a special calculation.

template<typename T>
inline double MCubicHermeticSpline<T>::euclideanDistance(const T &p1, const T &p2) const
{
    return abs(p2 - p1);
}


template<>
inline double MCubicHermeticSpline<QVector2D>::euclideanDistance(const QVector2D &p1, const QVector2D &p2) const
{
    return p1.distanceToPoint(p2);
}


template<>
inline double MCubicHermeticSpline<QVector3D>::euclideanDistance(const QVector3D &p1, const QVector3D &p2) const
{
    return p1.distanceToPoint(p2);
}


/**
 * A generic implementation of a catmull rom spline.
 * @tparam T The type of the spline points and interpolated values.
 */
template<typename T>
class MCatmullRomSpline : public MCubicHermeticSpline<T>
{
protected:
    using MAbstractSpline<T>::splinePoints;
    using MAbstractSpline<T>::closed;

public:
    T tangent(int k) const override
    {
        if (k < 0) return T();
        if (k > splinePoints.length() - 1 && !closed) return T();

        if (closed) k = k % splinePoints.length();

        int n = splinePoints.length();

        int kLeft = ((k - 1) % n + n) % n;
        int kRight = (k + 1) % n;

        if (!closed)
        {
            if (k == 0) kLeft = 0;
            if (k == n - 1) kRight = n - 1;
        }

        T pLeft = splinePoints[kLeft];
        T pRight = splinePoints[kRight];

        if (kRight - kLeft == 1)
            return (pRight - pLeft);

        return (pRight - pLeft) / 2.0;
    }
};

/**
 * A generic implementation of a cardinal spline.
 * @tparam T The type of the spline points and interpolated values.
 */
template<typename T>
class MCardinalSpline : public MCubicHermeticSpline<T>
{
protected:
    using MAbstractSpline<T>::splinePoints;
    using MAbstractSpline<T>::closed;
    using MAbstractSpline<T>::autoUpdates;

public:
    T tangent(int k) const override
    {
        if (k < 0) return T();
        if (k > splinePoints.length() - 1 && !closed) return T();

        if (closed) k = k % splinePoints.length();

        int n = splinePoints.length();

        int kLeft = ((k - 1) % n + n) % n;
        int kRight = (k + 1) % n;

        if (!closed)
        {
            if (k == 0) kLeft = 0;
            if (k == n - 1) kRight = n - 1;
        }

        T pLeft = splinePoints[kLeft];
        T pRight = splinePoints[kRight];

        if (kRight - kLeft == 1)
            return (1 - c) * (pRight - pLeft);

        return (1 - c) * (pRight - pLeft) / 2.0;
    }


    /**
     * Sets the tension factor of the cardinal spline.
     * Needs updating of the whole spline.
     * @param tension Tension factor in range [0,1]
     */
    void setTension(const float tension)
    {
        c = std::min(std::max(tension, -1.0f), 1.0f);

        if (autoUpdates)
        {
            MCubicHermeticSpline<T>::update();
        }
    }


    /**
     * @return The tension factor of the cardinal spline.
     */
    float tension() const
    {
        return c;
    }


protected:
    float c = 0;
};

} // Met3D

#endif //MET_3D_MSPLINES_H
