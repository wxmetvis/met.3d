/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2017 Bianca Tost [+]
**  Copyright 2022-2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSCENEVIEWGLWIDGET_H
#define MSCENEVIEWGLWIDGET_H

// standard library imports

// related third party imports
#include "GL/glew.h"
#include <QGLWidget>
#include <QGLShaderProgram>

// local application imports
#include "util/mstopwatch.h"
#include "gxfw/camera.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/mscenecontrol.h"
#include "gxfw/msystemcontrol.h"
#include "gxfw/mtypes.h"
#include "gxfw/mresizewindowdialog.h"
#include "gxfw/gl/shaderstoragebufferobject.h"
#include "gxfw/gl/uniformbufferobject.h"

#include "gxfw/properties/marrayproperty.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/mstringproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mcolorproperty.h"
#include "gxfw/properties/mpointfproperty.h"
#include "gxfw/properties/mvector3dproperty.h"
#include "gxfw/properties/mpropertytemplates.h"

namespace Met3D
{

// Forward declarations
class MLightActor;

typedef QVector<QVector3D> QVectorOfQVector3D;

/**
  Stores the reference to a currently picked actor and the ID of the picked
  handle.
  */
struct PickActor {
    MActor *actor;
    int    handleID;
};

/**
  The SceneBounds struct stores information about the size of the scene and its bounding box.
 */
struct SceneBounds
{
    double westLon = 180;
    double southLat = 90;
    double eastLon = -180;
    double northLat = -90;
    double bottomPressure_hPa = 1050;
    double topPressure_hPa = 0;

    /**
      @brief center Center of scene in world units (lon, lat, z)
     */
    QVector3D center;

    /**
      @brief extent Extents of scene bounds in world units (lon, lat, z)
     */
    QVector3D extent;

    /**
      @brief radius Radius of bounding sphere, length of @ref extent
     */
    double radius;
};


/**
  @brief MSceneViewGLWidget implements a view on a given scene (which is
  represented by an @ref MSceneControl instance).
  */
class MSceneViewGLWidget : public QGLWidget, public MSynchronizedObject
{
    Q_OBJECT

public:
    enum CameraActions {
        CAMERA_NORTHUP = 0,
        CAMERA_UPRIGHT = 1,
        CAMERA_TOPVIEW = 2,
        CAMERA_SAVETOFILE = 3,
        CAMERA_LOADFROMFILE = 4
    };

    enum SceneNavigationMode {
        MOVE_CAMERA             = 0,
        ROTATE_SCENE            = 1,
        TOPVIEW_2D              = 2,
        SINGLE_FULLSCREEN_ACTOR = 3
    };

    enum FSRQuality {
        OFF = 0,
        ULTRA_QUALITY = 1,
        QUALITY = 2,
        BALANCED = 3,
        PERFORMANCE = 4
    };

    enum ShadowResolution
    {
        Disabled = 0,
        LowRes,
        NormalRes,
        HighRes,
        VeryHighRes,
        UltraRes
    };

    enum RenderPassStep
    {
        SHADOW_MAPPING = 0,
        RENDER_SCENE,
        RENDER_UI,
        INACTIVE
    };

    /**
     * C++ representation for the lighting uniform buffer object.
     * The alignas() keyword is used to specify the OpenGL memory alignment,
     * so that the c++ struct correctly aligns with the OpenGL memory.
     * TODO (tv, 08May2024): This struct is not really optimal,
     *     as we need to define all members to represent the memory layout
     *     of the OpenGL buffer. Using the Qt objects like QVector3D is not possible,
     *     as their memory layout is not the same as a vec4 or a float[4].
     *     Using a dedicated library such as glm that correctly represents the
     *     GL data types in c++ would probably be best but is not feasible at this
     *     point.
     */
    struct LightingUniforms
    {
        /**
         * View matrices of the shadow light.
         * One 4x4 matrix per cascade / split.
         */
        alignas(16) GLfloat shadowLightMatrix[48];

        /**
         * The view matrix of this scenes camera.
         */
        alignas(16) GLfloat camViewMatix[16];

        /**
         * Direction of the shadow light.
         */
        alignas(16) GLfloat shadowLightDirection[3];

        /**
         * Indicates, whether the shadow map is rendered or not.
         * Just a flag for the shader to disable the code that
         * renders the shadow map onto fragments when no shadow map exists.
         * Booleans are saved as unsigned integers internally.
         */
        alignas(4) GLuint shadowMapAvailable;

        /**
         * Ambient color for the scene.
         * float[4] for alignment.
         */
        alignas(16) GLfloat ambientColor[3];

        /**
         * Ambient light amount
         */
        alignas(4) GLfloat ambientLight;

        /**
         * Camera position in world space used
         * for lighting.
         * float[4] for alignment.
         */
        alignas(16) GLfloat cameraPositionWS[3];

        /**
         * Diffuse light amount
         */
        alignas(4) GLfloat diffuseLight;

        /**
         * UV coordinate scale of one pixel on the shadow map.
         */
        alignas(8) GLfloat shadowMapTextMapScale[2];

        /**
         * Specular light amount
         */
        alignas(4) GLfloat specularStrength;

        /**
         * Split distances.
         */
        alignas(4) GLfloat splitDistances[3];

        /**
         * Whether shadows can be received from the shadow map or not.
         * This setting should be updated per actor.
         */
        alignas(4) GLuint receiveShadows;

        /**
         * The amount of samples used in the shadow map.
         */
        alignas(4) GLuint shadowMapSampleCount;
    };

    MSceneViewGLWidget();

    ~MSceneViewGLWidget();

    /**
      Sets the @ref MSceneControl instance @p scene that shall be rendered.
      */
    void setScene(MSceneControl *scene);

    void removeCurrentScene();

    QSize minimumSizeHint() const;

    QSize sizeHint() const;

    void setBackgroundColour(const QColor &color);

    MSceneControl* getScene() { return scene; } // implicit inline

    /**
     * @return The camera currently used for rendering. Might be different
     * depending on the render pass.
     */
    MCamera* getCamera() { return currentCamera; }

    /**
     * @return The camera used by the user to view the scene.
     */
    MCamera* getSceneCamera() { return &camera; }

    /**
      @return The currently used FBO of this scene.
     */
    GLuint getCurrentFBO() const { return currentFbo; }

    /**
     * Get the render step in the rendering pipeline,
     * which this scene view currently renders.
     */
    RenderPassStep renderStep() const { return currentRenderStep; }

    /**
      Returns the current model-view-projection matrix.
      */
    QMatrix4x4* getModelViewProjectionMatrix()
    { return &modelViewProjectionMatrix; }

    /**
      Returns the current inverted model-view-projection matrix.
      */
    QMatrix4x4* getModelViewProjectionMatrixInverted()
    { return &modelViewProjectionMatrixInverted; }

    /**
      @return The current view matrix.
     */
    QMatrix4x4* getViewMatrix() { return &viewMatrix; }

    /**
      @return The current projection matrix.
     */
    QMatrix4x4* getProjectionMatrix() { return &projectionMatrix; }

    /**
      Compute a world z-coordinate from a pressure value @p p_hPa (in hPa).
      */
    double worldZfromPressure(double p_hPa);

    static double worldZfromPressure(double p_hPa, double log_pBottom_hPa,
                                     double deltaZ_deltaLogP);

    double pressureFromWorldZ(double z);

    double worldZMeterScaling();

    /**
      Returns a 2D vector containing [ ln (p_bot), dz / dln(p) ]. These two
      values are needed to convert pressure (in hPa) to this scene view's
      world z coordinate.
      */
    QVector2D pressureToWorldZParameters();

    /**
      Converts a vector whose components are given as lat/lon/pressure to clip
      space (i.e. converts pressure to world z and applies the
      model-view-projection matrix.).
      */
    QVector3D lonLatPToClipSpace(const QVector3D& lonlatp);

    QVector3D worldToClipSpace(const QVector3D& worldSpace);

    QVector3D clipSpaceToLonLatWorldZ(const QVector3D& clipPos);

    QVector3D clipSpaceToLonLatP(const QVector3D& clipPos);

    /**
     * Projects a clip space location to a world space location (Graphics space)
     * in reference to a world space location.
     * The reference location is used to create a plane to project the clip space
     * location onto.
     * The normal vector is used to create this plane and is the normal of the
     * projection plane.
     * @param clip The clip space coordinate to project.
     * @param ref The reference point to project onto.
     * @param n The normal vector from the reference point that represents the projection
     * planes normal.
     * @return The projected location.
     */
    QVector3D projectClipSpaceToWorld(const QPointF &clip, const QVector3D &ref, const QVector3D &n);

    /**
      Returns true if visualisation parameters have changed that might make the
      recomputation of actor properties necessary. Example: Vertical sections
      need to recompute the "targetGrid" if the view's vertical scaling has
      changed.
      */
    bool visualisationParametersHaveChanged()
    { return visualizationParameterChange; }

    /**
      Returns the scene's light direction. (Use this in actors that use
      shading.)
      */
    QVector3D getLightDirection();

    /**
     * Bind the shadow map for light rendering.
     * @param shader The shader to bind the shadowmap for.
     * @param texUnit The texture unit to bind to.
     * @ref lighting.glsl
     */
    void bindShadowMap(std::shared_ptr<GL::MShaderEffect> shader, GLuint texUnit);

    /**
      Returns a unique number that identifies the scene view.
      */
    unsigned int getID() { return myID; }

    /**
      Returns a @ref QtProperty instance acting as parent to all the properties
      of this view (i.e. representing the properties "group"). This instance
      can be added to a @ref QtAbstractPropertyBrowser.

      @see http://doc.qt.nokia.com/solutions/4/qtpropertybrowser/index.html
      */
    MProperty* getPropertyGroup() { return &groupProp; }

    /**
      @return The resolution width the 3D scene is currently rendered at.
     */
    int getSceneResolutionWidth() const { return fsrViewPortWidth; }

    /**
      @return The resolution height the 3D scene is currently rendered at.
     */
    int getSceneResolutionHeight() const { return fsrViewPortHeight; }

    /**
      @return The resolution width the renderer is currently at. When using upscaling, this is the upscaled resolution.
     */
    int getRenderResolutionWidth() const { return renderResultWidth; }

    /**
      @return The resolution height the renderer is currently at. When using upscaling, this is the upscaled resolution.
     */
    int getRenderResolutionHeight() const { return renderResultHeight; }

    /**
     * @return The handle size for handles in this scene.
     */
    float getHandleSize() const { return handleSizeProp; }

    /**
     * Set the handle size of this scene view.
     * @param size The handle size.
     */
    void setHandleSize(float size) { handleSizeProp = size; }

    /**
      @return The scaling of the viewport relative to the render resolution. If upscaling is enabled, this value is the upscaling factor.
     */
    float getViewportUpscaleFactor() const;

    void setVerticalScaling(float scaling);

    float getVerticalScaling();

    void setInteractionMode(bool enabled);

    void setAnalysisMode(bool enabled);

    void setAutoRotationMode(bool enabled);

    void setFreeze(bool enabled);

    bool interactionModeEnabled() { return actorInteractionModeProp; }

    bool analysisModeEnabled() { return analysisModeProp; }

    bool userIsInteractingWithScene() { return userIsInteracting; }

    bool userIsScrollingWithMouse();

    bool orthographicModeEnabled() { return sceneNavigationMode == TOPVIEW_2D; }

    bool isViewPortResized();

    void addCameraSync(MSceneViewGLWidget *otherSceneView)
    { syncCameras.insert(otherSceneView); }

    void removeCameraSync(MSceneViewGLWidget *otherSceneView)
    { syncCameras.remove(otherSceneView); }

    /**
      Updates the label that displays the camera position in the scene view
      control's property browser.
      */
    void updateCameraPositionDisplay();

    /**
      Updates the scene camera display and redraws the scene.
     */
    void updateCamera();

    /**
      If the scene view is in "actor interaction mode": make @p actor the only
      actor in the scene with which the user can interact. (Used, e.g., to
      display a pole to select a scene rotation centre).
     */
    void setSingleInteractionActor(MActor *actor);

    void setSceneNavigationMode(SceneNavigationMode mode);

    void setSceneRotationCentre(QVector3D centre);

    /**
     Save the scene view configuration to the file @p filename.
     */
    void saveConfigurationToFile(QString filename = "");
    /**
     Load the scene view configuration from the file @p filename.
     */
    void loadConfigurationFromFile(QString filename = "");

    /**
     Save scene view-specific configuration to the @ref QSettings object @p
     settings.
     */
    void saveConfiguration(QSettings *settings);
    /**
     Load scene view-specific configuration from the @ref QSettings object @p
     settings.
     */
    void loadConfiguration(QSettings *settings);

    /**
     * Compatibility case to load configuration files pre 1.14
     */
    void loadConfigurationPrior_1_14(QSettings *settings);

    void forceOverwriteImageSequence(bool overwrite)
    { overwriteImageSequence = overwrite; }

    void onHandleSizeChanged();

    virtual void synchronizeWith(
            MSyncControl *sync, bool updateGUIProperties=true);

    virtual bool synchronizationEvent(MSynchronizationType, QVector<QVariant>);

    /**
      Returns the currently set animation sequence index for this scene.
     */
    int getCameraSequenceIndex() const { return cameraSequenceAnimationProp - 1; }

    bool sceneLightUpdated() const
    {return needSceneLightUpdate; }

    /**
     * Sets the line width for line rendering.
     * If line rendering is unsupported on the machine, it will not be set
     * and false will be returned.
     * @param lineWidth The requested line width.
     * @return Whether the line width was set.
     */
    bool setLineWidth(GLfloat lineWidth);

    /**
      Calculate the size of the scene based on actors using bounding boxes
      (Such as bounding box actor, graticule actor or horizontal section actor).
      If no actors using bounding boxes are found, the whole earth is assumed as the scene bounds.
     */
    SceneBounds getSceneBounds();

    /**
     * @return The light of this scene or nullptr if none is active.
     */
    MLightActor *getSceneDirectionalLight() const { return sceneDirectionalLight; };

    /**
     * @return The current shadow map resolution.
     */
    int getShadowMapResolution() const;

    /**
     * @return The number of samples used during multisampling.
     */
    int getSampleCount() const { return multiSamplingSamplesProp; }

    QList<MLightActor*> lights;
    GLuint defaultVao;

signals:
    /**
      Emitted when a mouse button is released on the GL canvas.
      */
    void clicked();

    /**
      Emitted when the user interacts with the scene in any way, such as moving the camera.
     */
    void sceneInteractedWith(MSceneViewGLWidget *widget);

public slots:
    /**
      Modifies the camera position and view direction according to the camera
      action @p action and updates the widget.
      */
    void executeCameraAction(int action, bool ignoreWithoutFocus=true);

    /**
      Sets the valid and init time displayed in the upper left corner of the
      view.
      */
    void updateDisplayTime();

    /**
      */
    void updateFPSTimer();

    void stopFPSMeasurement();

    void updateSceneLabel();

    void saveAnimationImage(QString path, QString filename);

    void updateSyncControlProperty();

    /**
     Connects to the MGLResourcesManager::actorCreated() signal. If the new
     actor supports full-screen mode, it is added to the list of full-screen
     actors displayed by the fullScreenActorProperty.
     */
    void onActorCreated(MActor *actor);

    /**
     Connects to the MGLResourcesManager::actorDeleted() signal. If the deleted
     actor supports full-screen mode, update the list of full-screen actors
     displayed by the fullScreenActorProperty, possibly disconnect from the
     full-screen actor.
     */
    void onActorDeleted(MActor *actor);

    /**
     Connects to the MGLResourcesManager::actorRenamed() signal. If the renamed
     actor supports full-screen mode, it is renamed in the list of full-screen
     actors displayed by the fullScreenActorProperty.
     */
    void onActorRenamed(MActor *actor, QString oldName);

    /**
      Directly connect change signal of full-screen actor to @ref updateGL() to
      allow the user to select actors as full-screen actors which are not
      connected to the scene view's scene.
     */
    void onFullScreenActorUpdate();

    /**
      Connects to each light actor to receive a signal,
      when it was updated to rerender the lighting.
     */
    void onLightChanged();

    /**
     * Drag/Drop interface: Event called when drag enters widget.
     * @param event The event
     */
    void dragEnterEvent(QDragEnterEvent *event) override;

    /**
     * Drag/Drop interface: Called when previously accepted
     * (@c dragEnterEvent) dragged object gets dropped into this widget.
     * @param event The event
     */
    void dropEvent(QDropEvent *event) override;

protected:
    void initializeGL();

    void updateGL();

    void paintGL();

    void resizeGL(int width, int height);

    void mouseDoubleClickEvent(QMouseEvent *event);

    void mousePressEvent(QMouseEvent *event);

    /**
      @brief refreshCameraSequenceProperty Refreshes the list of camera sequences in the system settings panel of this scene.
     */
    void refreshCameraSequenceProperty();

    /**
      Overloads QGLWidget::mouseMoveEvent().

      In interaction mode (actor elements can be changed interactively, e.g.
      the waypoints of a vertical section) this method handles pick&drag
      events. Otherwise it handles camera moves. In interaction mode, Qt mouse
      tracking is enabled (in @ref propertyChange()); mouseMoveEvent() is hence
      called on any mouse move on the canvas. This allows the implementation of
      "hovering" effects when the mouse cursor moves over a pickable
      element. For camera moves, mouse tracking is disabled and mouseMoveEvent()
      is only called when a mouse button has been pressed.
      */
    void mouseMoveEvent(QMouseEvent *event);

    void mouseReleaseEvent(QMouseEvent *event);

    void wheelEvent(QWheelEvent *event);

    void keyPressEvent(QKeyEvent *event);

    void updateSynchronizedCameras();

    /**
      Switches the currently used Framebuffer for this scene.
      Use this function instead of glBindFramebuffer, so @ref currentFBO
      gets set to the correct framebuffer object.
      @param fbo FBO to switch to.
     */
    void switchFramebuffer(GLuint fbo);

    /**
     * Reloads the shaders used in the scene view.
     */
    void reloadShaders();

    /**
     * Updates the GPU scene lights buffer.
     */
    void updateSceneLightsBuffer();

    /*+
     * Updates the uniform buffer needed to render scene lighting.
     */
    void updateLightingUniformBuffer();

    /**
     * Sets the OpenGL state for the current multisample settings.
     */
    void setMultisampleGLState();

    QList<MLabel*> staticLabels;

protected slots:

    /**
      Checks every 100 milliseconds if the user is scrolling with the mouse.
     */
    void checkUserScrolling();

    /**
      Called by the auto-rotation timer to rotate the camera in auto-rotation
      mode.
     */
    void autoRotateCamera();

    /**
      Called when a new camera sequence was registered.
      Used to update camera sequence selection in the scene settings.
     */
    void onSequenceRegistered();

    /**
      Called when the camera sequence at @param index was removed.
      Used to update camera sequence selection in the scene settings.
      @param index Index of the removed sequence.
     */
    void onSequenceRemoved(int index);

    /**
      Called when the camera sequence at @param index was renamed to @param name
      @param index Index of the renamed sequence.
      @param name New name of the sequence.
     */
    void onSequenceRenamed(int index, QString name);

    /**
     * Called when the widget gains focus.
     * @param event The Focus Event.
     */
    void focusInEvent(QFocusEvent *event) override;

    /**
      Handles opening of file dialog and letting the user choose where to save
      the image, how to call it and as which image file type.
     */
    void saveToImageFile();

    void resizeView();

private:
    /**
      Handles taking and saving an image of the scene to @p filename.
     */
    void saveToImageFileName(QString filename);

    /**
      @return The lod bias used for the fsr result texture. AMD recommends using lod bias for better results.
     */
    float getFSRlodBias() const;

    /**
      Positions the camera such that it views the whole scene, without changing its rotation.
      Does not update GL.
     */
    void recoverSceneInViewPort();

    MSceneControl *scene;

    /**
      Framebuffer variables:
      Used for 3 stage render process
      First, render scene like before to sceneFboMsaa, so that Msaa can be used.
      Then blit framebuffer to sceneFBO,
      so we can apply post-processing on the scene.
      Afterward, switch to the finalColorFBO and render the final image onto a quad
      spanning the whole scene, so we can see the finalColor
      result. The final colour result can be in any resolution, so we render that back onto the main frame buffer,
      so that it fits our viewport nicely. This approach also allows us to use upscaling techniques
      for the rendered scene to mitigate performance impact of some actors.
    */
    // Framebuffer used as a non-multisampled mirror of the actual scene framebuffer.
    GLuint sceneFbo;
    // Framebuffer used for 3D scene rendering at a custom resolution.
    GLuint sceneFboMsaa;
    // Framebuffer used to render the final scene at a custom resolution with 2D elements on top of 3D scene.
    GLuint finalResultFbo;
    // Stores the currently bound framebuffer of the scene.
    GLuint currentFbo;
    // Framebuffer used to render the shadow map.
    GLuint shadowMappingFbo;
    GL::MTexture *tex2DSceneColor;
    GL::MTexture *tex2DDepthBuffer;
    GL::MTexture *tex2DFSRResult;
    GL::MTexture *tex2DFinalColor;
    GL::MTexture *tex2DFinalDepthBuffer;
    GL::MTexture *tex2DShadowMap;
    GL::MTexture *tex2DShadowResult;
    GL::MShaderStorageBufferObject *sceneLightsBuffer;
    GL::MUniformBufferObject<LightingUniforms> *lightUniformBuffer;
    GLuint texUnitFSRResult;
    GLuint texUnitDepthBuffer;
    GLuint texUnitSceneColor;
    GLuint texUnitFinalColor;
    GLuint texUnitFinalDepthBuffer;
    GLuint texUnitShadowMap;
    GLuint sceneColorRbo;
    GLuint sceneDepthRbo;
    std::shared_ptr<GL::MShaderEffect> renderToViewportShader;
    std::shared_ptr<GL::MShaderEffect> fsrPassShader;
    GL::MVertexBuffer* vboViewportQuad;

    // Current render pass.
    RenderPassStep currentRenderStep;

    // Stores the position of the mourse cursor, used in the mouse??Event()
    // methods.
    QPoint lastPos;
    QVector3D lastPoint;

    MCamera camera;
    MCamera *currentCamera;
    QMatrix4x4 modelViewProjectionMatrix;
    QMatrix4x4 modelViewProjectionMatrixInverted;
    QMatrix4x4 viewMatrix;
    QMatrix4x4 projectionMatrix;
    QMatrix4x4 lightViewProjMatrix[3];
    GLfloat splits[4];
    MLightActor *sceneDirectionalLight;
    bool needSceneLightUpdate;
    QMatrix4x4 sceneRotationMatrix;
    SceneNavigationMode sceneNavigationMode;
    QTimer *cameraAutoRotationTimer;
    QVector3D cameraAutoRotationAxis;
    float cameraAutoRotationAngle;

    // Status variables.
    MBoolProperty renderLabelsWithDepthTestProp;
    MBoolProperty actorInteractionModeProp;
    MProperty handleGroupProp;
    MFloatProperty handleSizeProp;
    MBoolProperty cameraAutorotationModeProp;
    MBoolProperty analysisModeProp;
    int freezeMode;
    bool viewIsInitialised;
    bool userIsInteracting; // Is the user currently moving the camera?
    bool userIsScrolling;   // Is user currently scrolling with the mouse?
    bool viewportResized;   // Was the viewport resized?
    bool isLoadingFromConfig; // If the scene is currently reading its config file. Used to suppress updates.


    QElapsedTimer scrollTimer;
    QElapsedTimer resizeTimer;
    QTimer        checkScrollTimer;

    // Framerate measurements:
    QTimer     *fpsTimer;      // Timer to take a measurement every xx seconds.
    MStopwatch *fpsStopwatch;  // Stopwatch to get the elapsed time.
    short  frameCount;         // Count how many frames have been rendered.
    bool   splitNextFrame;     // Status var to indicate when to measure.
    float *fpsTimeseries;      // Field to store an fps time series.
    int    fpsTimeseriesSize;  // Length of the time series.
    int    fpsTimeseriesIndex; // Current index within the time series.

    // Size of the OpenGL viewport belonging to this widget.
    int viewPortWidth;
    int viewPortHeight;
    // Size of the downscaled rendered image which will be up-scaled to viewport size.
    int fsrViewPortHeight;
    int fsrViewPortWidth;
    // Size of the final rendered image.
    int renderResultWidth;
    int renderResultHeight;

    // Vertical axis limits: Bottom and top pressure (in hPa) and corresponding
    // z coordinates.
    double pbot;
    double ptop;
    double logpbot;
    double zbot;
    double ztop;
    double slopePtoZ; // dZ / dlnP

    QGLShaderProgram *focusShader;
    std::shared_ptr<GL::MShaderEffect> northArrowShader;

    // In modification mode, stores the currently picked actor.
    PickActor pickedActor;

    // Identification number.
    static unsigned int idCounter;
    unsigned int myID;

    MArrayProperty groupProp;
    MButtonProperty saveConfigProp;
    MButtonProperty loadConfigProp;

    MButtonProperty sceneSaveToImageProp;

    MArrayProperty cameraAndNavGroupProp;
    MStringProperty cameraPositionProp;
    MArrayProperty setCameraProp;
    MButtonProperty cameraSetNorthUpProp;
    MButtonProperty cameraSetUprightProp;
    MButtonProperty cameraSetTopViewProp;
    MButtonProperty cameraSaveToFileProp;
    MButtonProperty cameraLoadFromFileProp;
    MButtonProperty cameraRecoverSceneProp;
    MEnumProperty cameraSequenceAnimationProp;
    MFloatProperty sceneNavigationSensitivityProp;
    MEnumProperty sceneNavigationModeProp;
    MVector3DProperty sceneRotationCenterProp;
    MButtonProperty selectSceneRotationCentreProp;
    MActor *fullScreenActor;
    MEnumProperty fullScreenActorProp;

    MProperty interactionGroupProp;
    MButtonProperty resizeProp;
    MEnumProperty syncCameraWithViewProp;
    MBoolProperty posLabelIsEnabledProp;

    MProperty appearanceGroupProp;
    MColorProperty backgroundColourProp;
    MProperty lightingGroupProp;
    MColorProperty ambientLightColorProp;
    MFloatProperty ambientLightProp;
    MFloatProperty diffuseLightProp;
    MFloatProperty specularStrengthProp;
    MDoubleProperty verticalScalingProp;
    MPropertyTemplates::Labels displayDateTimeLabelProps;
    MPointFProperty displayDateTimePositionProp;
    MEnumProperty displayDateTimeSyncControlProp;
    MBoolProperty sceneViewLabelEnableProp;

    MProperty renderingGroupProp;
    MFloatProperty farPlaneDistanceProp;
    MBoolProperty multisamplingEnabledProp;
    MIntProperty multiSamplingSamplesProp;
    MBoolProperty enableOITProp;
    MBoolProperty antialiasingEnabledProp;
    MEnumProperty shadowMapResolutionProp;
    ShadowResolution shadowMapResProp;
    MFloatProperty shadowDistanceProp;
    MEnumProperty fsrQualityProp;
    FSRQuality fsrQuality;
    MBoolProperty useCustomResolutionProp;
    MArrayProperty customResolutionProp;
    MIntProperty customResolutionWidthProp;
    MIntProperty customResolutionHeightProp;
    MButtonProperty reloadShadersProp;

    /** Synchronisation with MSyncControl. */
    MSyncControl *synchronizationControl;
    MLabel       *displayDateTimeLabel;

    struct NorthArrow
    {
        NorthArrow() {}

        MBoolProperty enabledProp;
        MColorProperty colourProp;
        MArrayProperty scaleProp;
        MFloatProperty horizontalScaleProp;
        MFloatProperty verticalScaleProp;
        MVector3DProperty positionProp;
    };
    NorthArrow northArrow;

    /**
      List of static labels to display in this view.
     */
    MLabel *sceneNameLabel;

    bool measureFPS;
    uint measureFPSFrameCount;
    MButtonProperty measureFPSProp;

    bool visualizationParameterChange;

    QSet<MSceneViewGLWidget*> syncCameras;
    MSceneViewGLWidget *cameraSynchronizedWith;

    MActor *singleInteractionActor;

    MResizeWindowDialog *resizeViewDialog;

    // Overwrite already existing image files of a time series without
    // asking the user.
    bool overwriteImageSequence;

    // The filename of the current time animation screenshot that will be saved after rendering is finished.
    QString nextFrameScreenshotFilename;
};

} // namespace Met3D

#endif
