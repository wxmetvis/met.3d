/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2021-2022 Christoph Neuhauser [+]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef TRANSFERFUNCTION_H
#define TRANSFERFUNCTION_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>

// local application imports
#include "gxfw/mactor.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/texture.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mrectproperty.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mstringproperty.h"

namespace Met3D
{

/**
  @brief Base class for transfer function actors.
 */
class MTransferFunction : public MActor
{
    Q_OBJECT
public:
    explicit MTransferFunction(QObject *parent = nullptr);
    ~MTransferFunction() override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "TransferFunction"; }

    void reloadShaderEffects() override;

    /**
      Returns the minimum scalar value of the current transfer function
      settings.
      */
    float getMinimumValue() const { return minimumValue; }

    /**
      Returns the maximum scalar value of the current transfer function
      settings.
      */
    float getMaximumValue() const { return maximumValue; }

    /**
      Returns the texture that represents the transfer function texture.
      */
    GL::MTexture* getTexture() { return tfTexture; }

    void setMinimumValue(float value);

    void setMaximumValue(float value);

    virtual void setValueSignificantDigits(int significantDigits);

    virtual void setValueStep(float step);

    void setPosition(QRectF position);

    void setNumTicks(int num);

    void setNumLabels(int num);

    QString transferFunctionName();

    /**
      @brief Static method which can be called if an object misses the transfer
      function it should be connected to when loading a configuration file.

      Tells the user that the transfer function in the configuration file
      does not exist and asks the user whether he/she wants to load a transfer
      function from a configuration file.

      @p callerName should contain the name of the object missing the transfer
      function while @p callerDescriptor can contain a description of the
      object (e.g. "Variable") to give further information about the object to
      the user. The descriptor can be defined with or without a trailed space
      (The method will append a space if not present).

      @p settings contains the settings object from which the configuration of
      the object is loaded.
     */
    static bool loadMissingTransferFunction(
            const QString& tfName, QString tfActorType, QString callerDescriptor,
            const QString& callerName, QSettings *settings);

    int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView, float clipX,
                                    float clipY) override;

    void dragEvent(MSceneViewGLWidget *sceneView, int handleID, float clipX,
                   float clipY) override;

    void releaseEvent(MSceneViewGLWidget *sceneView, int handleID) override;

signals:
    
public slots:

protected:
    void initializeActorResources() override;

    virtual void generateTransferTexture() {}

    virtual void generateBarGeometry() {}

    GL::MTexture *tfTexture;
    GLint textureUnit;

    GL::MVertexBuffer *vertexBuffer;
    uint numVertices;

    std::shared_ptr<GL::MShaderEffect> simpleGeometryShader;
    std::shared_ptr<GL::MShaderEffect> colourbarShader;

    // General properties.
    MRectProperty positionProp;

    // Properties related to ticks and labels.
    MIntProperty maxNumTicksProp;
    uint        numTicks;
    MIntProperty maxNumLabelsProp;
    MDoubleProperty tickWidthProp;
    MFloatProperty labelSpacingProp;
    MStringProperty descriptionLabelProp;
    MBoolProperty flipDescriptionProp;

    // Properties related to value range.
    MProperty rangeGroupProp;
    MSciFloatProperty minimumValueProp;
    MSciFloatProperty maximumValueProp;
    MProperty valueOptionsGroupProp;
    MIntProperty valueSignificantDigitsProp;
    MSciFloatProperty valueStepProp;
    float       minimumValue;
    float       maximumValue;

    enum TfOrientation : int
    {
        VERTICAL = 0,
        HORIZONTAL = 1
    };

    MEnumProperty orientation;

    // Moving the transfer function.
    bool isDragging = false;
    int currentDragHandleID;
    float mouseDragStartX = 0;
    float mouseDragStartY = 0;
    qreal posDragStartX = 0;
    qreal posDragStartY = 0;

    // Resizing the transfer function.
    qreal leftDragStart = 0;
    qreal rightDragStart = 0;
    qreal topDragStart = 0;
    qreal bottomDragStart = 0;
};

} // namespace Met3D

#endif // TRANSFERFUNCTION_H
