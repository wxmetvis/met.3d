/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mpropertyloadingerrordialog.h"

// standard library imports

// related third party imports
#include <QTextEdit>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "msystemcontrol.h"
#include "util/mutil.h"

namespace Met3D
{
MPropertyLoadingErrorDialog::MPropertyLoadingErrorDialog(
        const QVector<MProperty *>& properties, QWidget *parent)
{
    QString detailText;
    QMap<QString, QStringList> rootPathMap;
    for (MProperty *prop : properties)
    {
        MProperty *parentProp = prop;

        QString path;
        QString root;

        while (parentProp != nullptr)
        {
            if (parentProp->getParent() == nullptr)
            {
                root = parentProp->getName();

                if (prop == parentProp)
                {
                    path = root;
                }
            }
            else
            {
                if (!path.isEmpty())
                {
                    path = " >> " + path;
                }
                path = parentProp->getName() + path;
            }
            parentProp = parentProp->getParent();
        }
        rootPathMap[root].append(path);
    }

    for (auto i = rootPathMap.cbegin(); i != rootPathMap.cend(); i++)
    {
        detailText.append(i.key() + ": \n");
        for (const QString &path : i.value())
        {
            detailText.append("  " + path + "\n");
        }
    }

    setDetailedText(detailText);
    setIcon(Warning);

    auto *detailTextEdit = findChild<QTextEdit*>();
    if (detailTextEdit != nullptr)
    {
        detailTextEdit->setLineWrapMode(QTextEdit::NoWrap);
    }
}


int MPropertyLoadingErrorDialog::exec()
{
    LOG4CPLUS_WARN(mlog, "\n" + text());
    LOG4CPLUS_WARN(mlog, "\n" + detailedText());

    bool isInBatchMode = MSystemManagerAndControl::getInstance()->isInBatchMode();
    if (! isInBatchMode)
    {
        return QDialog::exec();
    }
    return QDialog::Accepted;
}
} // Met3D