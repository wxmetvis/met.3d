/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MINPUTVARSEDITTABLE_H
#define MET_3D_MINPUTVARSEDITTABLE_H

// standard library imports

// related third party imports
#include <QDialog>
#include <QItemDelegate>
#include <QTableWidget>

// local application imports


namespace Met3D
{
/**
 * This class implements a table called from the add data source dialog. It
 * provides a convenient way to set a mapping from input variable standard
 * names used for the computation of derived variables, to input variables in
 * the data set.
 */
class MInputVarsEditTable : public QDialog
{
    Q_OBJECT

public:
    /**
     * Creates a dialog with the table representing the mapping. Keys for the
     * mapping are obtained from the derived data source. Values are prefilled
     * based on the given string representation of the 'inputVarsForDerivedVars'
     * field, e.g. from the pipeline config.
     * @param currentMapping The current mapping to prefill the table with.
     */
    explicit MInputVarsEditTable(const QString &currentMapping = "",
                                 QWidget *parent = nullptr);

    ~MInputVarsEditTable() override;

    /**
     * @return The currently selected mapping in the table in the string
     * representation used for pipeline configs. Calls @c currentMappingAsMap()
     * and converts the result via @c inputVarsStringToMap()
     */
    QString currentMappingAsString() const;

    /**
     * @return The current table entries as map. Only adds entries to the map
     * if the corresponding values are not empty.
     */
    QMap<QString, QString> currentMappingAsMap() const;

private:
    QTableWidget *table;

    /**
     * Item delegate for this table to restrict inputs. The delegate validates
     * the inputs and makes sure that no special characters (':' and '/') are
     * contained in the inputs.
     */
    class MInputVarItemDelegate : public QItemDelegate
    {
    public:
        QWidget *createEditor(QWidget *parent,
                              const QStyleOptionViewItem &option,
                              const QModelIndex &index) const override;
    };
};
} // Met3D

#endif //MET_3D_MINPUTVARSEDITTABLE_H
