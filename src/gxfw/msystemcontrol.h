/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2022-2023 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSCENEVIEWCONTROL_H
#define MSCENEVIEWCONTROL_H

// standard library imports

// related third party imports

// local application imports
#include "data/abstractmemorymanager.h"
#include "data/abstractdatasource.h"
#include "data/abstractdatareader.h"
#include "gxfw/synccontrol.h"
#include "data/waypoints/waypointstablemodel.h"
#include "util/mstopwatch.h"
#include "boundingbox/boundingbox.h"
#include "boundingbox/bboxdockwidget.h"
#include "colourmap.h"
#include "data/sequencer/mcamerasequence.h"

#include "gxfw/properties/mpropertytree.h"
#include "gxfw/properties/marrayproperty.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/mnumberproperty.h"

namespace Met3D
{

class MMainWindow;
class MSceneViewGLWidget;


/**
  @brief MSystemManagerAndControl manages a number of system resources of the
  Met.3D system (including memory managers) and provides a
  GUI widget that allows the user to view and modify system properties.

  Only a single instance of this control exisits (singleton pattern).
  */
class MSystemManagerAndControl : public QWidget
{
    Q_OBJECT

public:
    ~MSystemManagerAndControl();

    /**
     Returns the (singleton) instance of the system control. If getInstance()
     is called for the first time an optional parent widget can be passed as
     argument.
     */
    static MSystemManagerAndControl* getInstance(QWidget *parent=0);

    void storeApplicationCommandLineArguments(QCommandLineParser *arguments);

    /**
     * Checks, whether the command line option @param optionName was passed to the application.
     * @return True, if it was passed, otherwise false.
     */
    bool isCommandLineArgumentSet(const QString& optionName) const;

    /**
     * Returns the command line option value found for the given option optionName, or an empty string if not found.
     * @param optionName Name of the command line argument
     */
    QString getCommandLineArgumentValue(const QString& optionName) const;

    /**
     * Returns all command line option values found for the given option optionName, or an empty string if not found.
     * @param optionName Name of the command line argument
     */
    QStringList getCommandLineArgumentValues(const QString& optionName) const;

    const QDir& getMet3DHomeDir() const;

    const QDir& getMet3DBaseDir() const;

    QDir& getMet3DWorkingDirectory() { return met3DWorkingDirectory; }
    void setMet3DWorkingDirectory(QString workingDir);

    MPropertyTree* getSystemPropertiesBrowser()
    { return systemPropertiesBrowser; }

    MProperty *getMemoryManagersPropertyGroup()
    { return &memoryManagersGroupProp; }

    void setMainWindow(MMainWindow *window);

    MMainWindow* getMainWindow();

    /**
     * Check whether the UI is ready.
     * @return Whether or not the UI is ready.
     */
    bool isUiReady() const;

    /**
     * Called when the UI is ready. This should only be done once.
     * This triggers loading of actors and sessions if requested by
     * the frontend file.
     */
    void uiIsReady();

    void registerSceneView(MSceneViewGLWidget *view);

    MSceneViewGLWidget *getLastInteractedSceneView();

    QList<MSceneViewGLWidget*>& getRegisteredViews() { return registeredViews; }

    void registerMemoryManager(const QString& id,
                               MAbstractMemoryManager* memoryManager);

    MAbstractMemoryManager* getMemoryManager(const QString& id) const;

    QStringList getMemoryManagerIdentifiers() const;

    void registerDataSource(const QString& id,
                            MAbstractDataSource* dataSource);

    MAbstractDataSource* getDataSource(const QString& id) const;

    QStringList getDataSourceIdentifiers() const;

    void registerSyncControl(MSyncControl* syncControl);

    MSyncControl* getSyncControl(const QString& id) const;

    QStringList getSyncControlIdentifiers() const;

    void removeSyncControl(MSyncControl *syncControl);

    void registerWaypointsModel(MWaypointsTableModel* wps);

    MWaypointsTableModel* getWaypointsModel(const QString& id) const;

    QStringList getWaypointsModelsIdentifiers() const;

    int registerCameraSequence(MCameraSequence* sequence);
    void removeAllCameraSequences();
    void removeCameraSequence(int index);
    void renameCameraSequence(int index, QString name);
    MCameraSequence* getCameraSequence(const int index) const;
    QList<MCameraSequence*> getCameraSequences() const;

    void registerBoundingBox(MBoundingBox* bbox);
    void deleteBoundingBox(const QString& id);
    void renameBoundingBox(const QString& oldId, MBoundingBox *bbox);
    MBoundingBox* getBoundingBox(const QString& id) const;
    QStringList getBoundingBoxesIdentifiers() const;
    MBoundingBoxDockWidget *getBoundingBoxDock() const;

    MStopwatch& getSystemStopwatch() { return systemStopwatch; }

    double elapsedTimeSinceSystemStart(const MStopwatch::TimeUnits units);

    /**
      Returns true after @ref MGLResourcesManager::initializeGL() has been executed
      and the application is entirely initialized (i.e. all config files
      have been loaded and GL resources have been initialized).
     */
    bool applicationIsInitialized() { return met3dAppIsInitialized; }

    bool isConnectedToMetview() { return connectedToMetview; }

    QMap<QString, QString> *getDefaultMemoryManagers()
    { return &defaultMemoryManagers; }

    void setBatchMode(bool isActive, QString animType, QString syncName,
                      QString dataSourceIDForStartTime, int timeRange_sec,
                      bool quitWhenCompleted, bool overwriteImages);

    bool isInBatchMode();

    /**
     * Initializes the colourmap pool for this Met.3D instance.
     * Loads all colourmaps found in directories.
     * @param directories Directories to search for colourmaps in.
     */
    void initializeColourmapPool(QStringList directories);

    /**
     * Retunrs the applications colourmap pool.
     * The colourmap pool contains all currently loaded, predefined colourmaps.
     * @return A pointer to the colourmap pool object.
     */
    MColourmapPool *getColourmapPool() const;

    /**
      Executes Met.3D in batch mode to automatically produce imagery.
      This function should only be called from @ref MMainWindow::show().

      Behaviour as of v1.7/21.09 (mr, 27Sept2021):

      When the frontend file is read by
      @ref MFrontendConfiguration::initializeFrontendFromConfigFile(),
      the session file specified in the frontend config file is loaded first.
      This implies that the init/valid times that are specified in the session
      file are attempted to be loaded - if these times are available from the
      current dataset, the load will execute and the corresponding image
      displayed by Met.3D.

      Note that these dates are usually not the dates intended to be visualized
      by the batch mode (e.g., this will target at producing imagery of the
      most recent init time). However, in the current implementation, it is
      not straightforward to change how the sessions are loaded. Currently,
      hence, we need to wait for the first (undesired) data to load before
      Met.3D can switch to the most recent data.

      Also note that the current implementation of the MSyncControl time
      animation does not include the first time step into the animation.
      That means that the init/valid times of the first time step need to
      be set first (and loaded!), then the time animation starts by saving
      the exisiting current image as a first time step in the animation
      sequence.

      @todo For future releases, it would be useful to load all batch
      configuration BEFORE any thing corresponding to a session is loaded.
      Then, the session loader could be notified of which data to load
      right away.
     */
    void executeBatchMode();

    void setApplicationConfigurationValue(QString key, QVariant item);

    QVariant getApplicationConfigurationValue(QString key);

    /**
     * @return The quality of images generated by Met.3D in percent.
     */
    int getImageFileQuality() const { return screenshotQualityProp; }

signals:
    void boundingBoxCreated();
    void boundingBoxDeleted(QString name);
    void boundingBoxRenamed();

    /**
      Emitted when a new camera sequence was registered.
     */
    void cameraSequenceRegistered();

    /**
      Emitted when the camera sequences get cleared.
      Usually done when switching sessions.
     */
    void cameraSequencesCleared();

    /**
      Emitted when the camera sequence at @param index was removed.
      @param index The index of the removed camera sequence.
     */
    void cameraSequenceRemoved(int index);

    /**
      Emitted when the camera sequence at @param index was
      renamed to @param name,
     */
    void cameraSequenceRenamed(int index, QString name);

public slots:
    /**
      Closes the Met.3D main window, thus quitting the application.
     */
    void closeMainWindow();

    /**
     * Sets the last focused scene view to @c widget.
     * @param widget Current interacted scene view widget
     */
    void setLastFocus(MSceneViewGLWidget *widget);

protected:
    friend class MGLResourcesManager;

    /**
      This method is only called once by @ref MGLResourcesManager::initializeGL().
      All other methods can query whether the application has already been
      entirely initialized by calling @ref applicationIsInitialized().
     */
    void setApplicationIsInitialized();

private:
    /**
     Constructor is private, as it should only be called from getInstance().
     See https://en.wikipedia.org/wiki/Singleton_pattern#Lazy_initialization.
     */
    MSystemManagerAndControl(QWidget *parent=0);

    /** Singleton instance of the system control. */
    static MSystemManagerAndControl* instance;

    bool met3dAppIsInitialized;
    bool connectedToMetview;
    bool uiReady;

    bool batchModeIsActive;
    QString batchModeAnimationType;
    QString syncControlForBatchModeAnimation;
    QString batchModeDataSourceIDToGetStartTime;
    int batchModeTimeRange_sec;
    bool batchModeQuitWhenCompleted;
    bool batchModeOverwriteImages;

    QCommandLineParser *commandLineArguments;
    QDir met3DHomeDir;
    QDir met3DBaseDir;

    QDir met3DWorkingDirectory;

    QVBoxLayout *sysManagerLayout;
    MPropertyTree *systemPropertiesBrowser;

    MProperty appConfigProp;
    MProperty imageExportGroupProp;
    MIntProperty screenshotQualityProp;

    MProperty memoryManagersGroupProp;
    MButtonProperty debugSchedulerProp;
    MProperty allSceneViewsGroupProp;

    QList<MSceneViewGLWidget*> registeredViews;
    MSceneViewGLWidget *lastFocusedSceneView;

    MMainWindow *mainWindow;

    QMap<QString, MAbstractMemoryManager*> memoryManagerPool;
    QMap<QString, MAbstractDataSource*>    dataSourcePool;
    QMap<QString, MSyncControl*>           syncControlPool;
    QMap<QString, MWaypointsTableModel*>   waypointsTableModelPool;

    QMap<QString, MBoundingBox*>           boundingBoxPool;

    /**
      The list of currently registered camera sequences.
      Unique identifier of a camera sequence is the index it
      is saved at. When removing or adding camera sequences
      emit the corresponding update signals, so other classes
      can update their referenced indices to camera sequences.
     */
    QList<MCameraSequence*>           cameraSequencePool;

    MStopwatch systemStopwatch;

    QMap<QString, QString>                 defaultMemoryManagers;

    QMap<QString, QVariant> applicationConfigurationValues;

    MColourmapPool *colourmapPool;

    QMetaObject::Connection batchModeSchedulerSignalSlot;
};

} // namespace Met3D

#endif // MSCENEVIEWCONTROL_H
