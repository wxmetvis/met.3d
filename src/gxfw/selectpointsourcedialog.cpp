/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "selectpointsourcedialog.h"
#include "ui_selectpointsourcedialog.h"

// standard library imports

// related third party imports

// local application imports
#include "trajectories/pointsource/pointgeneratorinterface.h"
#include "trajectories/pointsource/pointsourcefactory.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectPointSourceDialog::MSelectPointSourceDialog(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::MSelectPointSourceDialog)
{
    ui->setupUi(this);

    createSourceEntries();
}


MSelectPointSourceDialog::~MSelectPointSourceDialog()
{
    delete ui;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPointGeneratorInterface* MSelectPointSourceDialog::getSelectedInterface()
{
    int row = ui->seedSourceTable->currentRow();

    QString typeString = ui->seedSourceTable->item(row, 0)->text();
    MPointGeneratorInterfaceType type = MPointGeneratorInterface::stringToInterfaceType(typeString);
    QString identifier = ui->seedSourceTable->item(row, 1)->text();

    return MPointSourceFactory::getInterface(type, identifier);
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/


void MSelectPointSourceDialog::createSourceEntries()
{
    QTableWidget *table = ui->seedSourceTable;

    // Set the data field table's header.
    table->setColumnCount(2);
    table->setHorizontalHeaderLabels(QStringList() << "Source type" << "Source identifier");

    MPointSourceFactory *mgr = MPointSourceFactory::getInstance();
    auto interfaces = mgr->getPointGeneratingInterfaces();

    for (MPointGeneratorInterface* generatorInterface : interfaces)
    {
        // Get the type and the name of the generator interface, and put them
        // into the table.
        int row = table->rowCount();
        table->setRowCount(row + 1);
        MPointGeneratorInterfaceType sourceType = generatorInterface->getType();
        QString sourceTypeStr = MPointGeneratorInterface::generatorTypeToString(sourceType);

        // Insert the element.
        table->setItem(row, 0, new QTableWidgetItem(sourceTypeStr));
        table->setItem(row, 1,
            new QTableWidgetItem(generatorInterface->getPointInterfaceName()));
    }

    // Resize the table's columns to fit actor names.
    table->resizeColumnsToContents();
    // Set table width to always fit window size.
    table->horizontalHeader()->setStretchLastSection(true);
    // Disable resize of column by user.
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Fixed);
    // Resize widget to fit table size.
    this->resize(table->width(), table->height());
}

} // namespace Met3D
