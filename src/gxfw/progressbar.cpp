/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "progressbar.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QHBoxLayout>
#include <QToolTip>
#include <QPalette>

// local application imports
#include "util/mutil.h"
#include "system/scheduling/task.h"
#include "msystemcontrol.h"

namespace Met3D
{

MProgressBar *MProgressBar::instance = nullptr;


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MProgressBar::MProgressBar(QWidget *parent) :
        QWidget(parent), progress(0), maximum(0), redrawRequested(false),
        updateTimer(new QTimer(this))
{
    // Initialize the label and the progress bar.
    progressBarLabel = new QLabel("", this);
    progressBarLabel->setAlignment(Qt::AlignRight | Qt::AlignVCenter);
    progressBarLabel->setContentsMargins(0, 0, 0, 0);

    progressBar = new QProgressBar(this);
    progressBar->setRange(0, 0);
    // Set progressbar color to orange.
    QPalette p = progressBar->palette();
    p.setColor(QPalette::Highlight, QColorConstants::Svg::darkorange);
    setPalette(p);

    // Create a layout containing both widgets.
    auto layout = new QHBoxLayout(this);
    layout->addWidget(progressBarLabel);
    layout->addWidget(progressBar);
    layout->setContentsMargins(0, 0, 0, 0);

    // Set the progress bar itself to a fixed width and maximum height.
    progressBar->setContentsMargins(0, 0, 0, 0);
    progressBar->setFixedWidth(150);
    progressBar->setAlignment(Qt::AlignCenter);

    // Align height of progress bar and label with parent.
    progressBar->setFixedHeight(parent->height());
    progressBarLabel->setFixedHeight(parent->height());

    // Set the update timer interval for the progress bar.
    updateTimer->setInterval(200);  // Update progress every 200ms
    connect(updateTimer, &QTimer::timeout, this,
            &MProgressBar::redrawIfRequested, Qt::QueuedConnection);

    // Send update call of progress bar to main thread, as all GUI updates must occur in the main thread.
    connect(this, &MProgressBar::updateProgressBar, this,
            &MProgressBar::onUpdateProgressBar, Qt::QueuedConnection);
    toggleProgressBar(false);
}


MProgressBar::~MProgressBar()
{
    delete updateTimer;
    delete progressBarLabel;
    delete progressBar;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


MProgressBar *MProgressBar::getInstance()
{
    if (instance == nullptr)
    {
        LOG4CPLUS_ERROR(mlog, "The progress bar needs to be initialized before first access.");
        return nullptr;
    }
    return instance;
}


void MProgressBar::initialize(QMenuBar *menubar)
{
    if (instance != nullptr)
    {
        return;
    }

    instance = new MProgressBar(menubar);

    // Set the size of this widget to the menu bar size.
    int height = menubar->size().height();
    instance->setContentsMargins(0, 0, 0, 0);
    instance->setFixedHeight(height);

    menubar->setCornerWidget(instance);
    instance->show();
}


MProgressBarTaskHandle *MProgressBar::addTask(const QString &displayName)
{
    m_mutex.lock();

    auto taskHandle = new MProgressBarTaskHandle(displayName);
    activeTasks.append(taskHandle);
    maximum += 1;

    // Unlock so the subsequent rendering call is allowed to add additional tasks.
    m_mutex.unlock();

    emit updateProgressBar();
    return taskHandle;
}


void MProgressBar::addTask(MTask *task)
{
    QString label = task->getProcessingLabel();
    auto handle = addTask(label);
    task->setProgressBarTaskHandle(handle);

    emit updateProgressBar();
}


QList<MProgressBarTaskHandle *> MProgressBar::addTasks(int numTasks, const QString &displayName)
{
    m_mutex.lock();
    QList<MProgressBarTaskHandle *> newTasks;

    for (int i = 0; i < numTasks; i++)
    {
        newTasks.append(new MProgressBarTaskHandle(displayName));
    }

    activeTasks.append(newTasks);
    maximum += numTasks;

    // Unlock so the subsequent rendering call is allowed to add additional tasks.
    m_mutex.unlock();

    emit updateProgressBar();
    return newTasks;
}


void MProgressBar::taskCompleted(MProgressBarTaskHandle *handle)
{
    m_mutex.lock();

    progress += 1;
    if (activeTasks.removeOne(handle))
    {
        delete handle;
    }

    m_mutex.unlock();
    emit updateProgressBar();
}


void MProgressBar::taskCompleted(MTask* task)
{
    taskCompleted(task->getProgressBarTaskHandle());
}


bool MProgressBar::firstTaskCompleted(QList<MProgressBarTaskHandle *> &handles)
{
    if (handles.isEmpty())
    {
        return false;
    }
    auto handle = handles.takeFirst();
    taskCompleted(handle);
    return true;
}


void MProgressBar::tasksCompleted(QList<MProgressBarTaskHandle *>& handles)
{
    while (!handles.isEmpty())
    {
        taskCompleted(handles.takeFirst());
    }
}

void MProgressBar::onUpdateProgressBar()
{
    if (progressBar->value() == progress)
    {
        // No update required. Might happen if between two UI render cycles
        // multiple tasks have been completed and therefore multiple calls
        // to this function are queued.
        return;
    }

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    if (!sysMC->isUiReady())
    {
        return;
    }

    // Update progress of bar.
    progressBar->setValue(progress);
    progressBar->setMaximum(maximum);

    // Lock so active tasks does not become empty before accessing the first task.
    m_mutex.lock();
    if (!activeTasks.isEmpty())
    {
        // Update task label to the oldest one.
        progressBarLabel->setText("Met.3D status: " + activeTasks[0]->displayName);
    }
    m_mutex.unlock();

    if (progress == progressBar->maximum())
    {
        // Progress bar has finished.
        resetProgressBar();
    }
    else if (progressBar->isHidden())
    {
        toggleProgressBar(true);
    }

    updateToolTip();
    redrawRequested = true;
    // Start the UI update trigger if not active.
    if (! updateTimer->isActive()) updateTimer->start();
}


void MProgressBar::resetProgressBar()
{
    progress = 0;
    maximum = 0;

    // Delete all remaining active task handles.
    for (auto handle: activeTasks)
    {
        delete handle;
    }
    activeTasks.clear();

    progressBar->reset();
    progressBar->setMaximum(maximum);

    toggleProgressBar(false);
}


void MProgressBar::toggleProgressBar(bool toggleOn)
{
    if (toggleOn && progressBar->isHidden())
    {
        progressBar->show();

        progressBarLabel->setContentsMargins(0, 0, 0, 0);
        progressBarLabel->setStyleSheet("QLabel { color : black; }");

        // Need to force set the corner widget again so the extents are properly updated
        // on the first cycle.
        dynamic_cast<QMenuBar*>(parentWidget())->setCornerWidget(this);
    }
    else if (!toggleOn && !progressBar->isHidden())
    {
        progressBar->hide();

        progressBarLabel->setText("Met.3D status: Idle");
        progressBarLabel->setStyleSheet("QLabel { color : green; }");
        progressBarLabel->setContentsMargins(0, 0, 10, 0);
    }
}


void MProgressBar::updateToolTip()
{
    // Update tooltip (remaining tasks).
    int remainingTasks = activeTasks.size();
    QString message("");
    if (remainingTasks)
    {
        message = QString::number(remainingTasks) + " remaining";
    }
    setToolTip(message);

    if (!QToolTip::isVisible())
        return;

    QPoint p = mapFromGlobal(QCursor::pos());
    if (p.x() >= 0 && p.y() >= 0 && p.x() < width() && p.y() < height())
        QToolTip::showText(QCursor::pos(), message);
}


void MProgressBar::redrawIfRequested()
{
    if (redrawRequested)
    {
        if (MSystemManagerAndControl::getInstance()->applicationIsInitialized())
        {
            qApp->processEvents();
            redrawRequested = false;
        }
    }
    // Stop timer if no tasks currently active.
    if (activeTasks.isEmpty()) updateTimer->stop();
}

} // Met3D