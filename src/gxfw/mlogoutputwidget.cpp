/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mlogoutputwidget.h"
#include "ui_mlogoutputwidget.h"

// standard library imports

// related third party imports
#include <QWidget>
#include <QScrollBar>

// local application imports
#include "util/mfileutils.h"
#include "util/mfiletypes.h"
#include "util/mutil.h"

namespace Met3D
{

static const auto SEARCH_HIGHLIGHT_COLOR = QColor(Qt::yellow).lighter(130.0f);


MLogOutputWidget::MLogOutputWidget(QWidget *parent) :
        QWidget(parent),
        ui(new Ui::MLogOutputWidget),
        searchMode(false),
        searchText()
{
    ui->setupUi(this);
    ui->searchWidget->setVisible(searchMode);

    QStyle *style = ui->searchNext->style();
    ui->searchNext->setIcon(style->standardIcon(QStyle::SP_ArrowDown));
    ui->searchPrevious->setIcon(style->standardIcon(QStyle::SP_ArrowUp));

    // Get root log level and set the desired filters accordingly.
    log4cplus::LogLevel ll = log4cplus::Logger::getRoot().getLogLevel();

    if (log4cplus::INFO_LOG_LEVEL >= ll)
    {
        logLevelCheckboxes
                .insert(log4cplus::INFO_LOG_LEVEL, new QCheckBox(tr("info")));
    }
    if (log4cplus::DEBUG_LOG_LEVEL >= ll)
    {
        logLevelCheckboxes
                .insert(log4cplus::DEBUG_LOG_LEVEL, new QCheckBox(tr("debug")));
    }
    if (log4cplus::TRACE_LOG_LEVEL >= ll)
    {
        logLevelCheckboxes
                .insert(log4cplus::TRACE_LOG_LEVEL, new QCheckBox(tr("trace")));
    }
    if (log4cplus::WARN_LOG_LEVEL >= ll)
    {
        logLevelCheckboxes.insert(log4cplus::WARN_LOG_LEVEL,
                                  new QCheckBox(tr("warning")));
    }
    if (log4cplus::ERROR_LOG_LEVEL >= ll)
    {
        logLevelCheckboxes
                .insert(log4cplus::ERROR_LOG_LEVEL, new QCheckBox(tr("error")));
    }
    if (log4cplus::FATAL_LOG_LEVEL >= ll)
    {
        logLevelCheckboxes
                .insert(log4cplus::FATAL_LOG_LEVEL, new QCheckBox(tr("fatal")));
    }

    // Add checkboxes to the group box. IMPORTANT: Order of checkboxes is the definition order of the MLogLevel enum.
    for (QCheckBox *checkBox : logLevelCheckboxes)
    {
        checkBox->setChecked(true);
        ui->logLevelsGroupBox->layout()->addWidget(checkBox);
        connect(checkBox, &QCheckBox::stateChanged, this,
                &MLogOutputWidget::onLogLevelFilterChanged);
    }

    connect(&MLogger::Instance, &MLogger::onLogMessageReceived, this,
            &MLogOutputWidget::onLogMessageReceived);

    for (auto msg : MLogger::Instance)
    {
        printLogMessage(msg);
    }
}


MLogOutputWidget::~MLogOutputWidget()
{
    delete ui;
}


void MLogOutputWidget::keyPressEvent(QKeyEvent *event)
{
    Qt::KeyboardModifiers mods = event->modifiers();

    // Could also do this via ui->searchButton->setShortcut, but this way we can do additional condition checks.
    if (mods == Qt::NoModifier && (event->key() == Qt::Key_Enter
            || event->key() == Qt::Key_Return))
    {
        onSearchNext();
    }

    if (mods == Qt::ShiftModifier && (event->key() == Qt::Key_Enter
            || event->key() == Qt::Key_Return))
    {
        onSearchPrevious();
    }

    if (event->key() == Qt::Key_F && mods == Qt::ControlModifier)
    {
        onSearchButtonPressed();
    }
}


void MLogOutputWidget::onLogMessageReceived(MLogger::MLogMessage *message)
{
    printLogMessage(message);
}


void MLogOutputWidget::onSave()
{
    QString fileName = MFileUtils::getSaveFileName(this, "Save log file",
                                                   FileTypes::M_LOG_FILE);

    if (fileName.isEmpty())
    {
        return;
    }

    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly))
    {
        QTextStream outStream(&file);
        for (MLogger::MLogMessage *logMsg : MLogger::Instance)
        {
            outStream << logMsg->msg.c_str() << "\n";
        }
        file.close();
    }
}


void MLogOutputWidget::onClear()
{
    MLogger::Instance.clear();
    ui->logOutput->clear();
    if (searchMode)
    {
        ui->logOutput->setExtraSelections({});
        searchHighlights.clear();
    }
}


void MLogOutputWidget::onSearchButtonPressed()
{
    searchMode = !searchMode;
    ui->searchWidget->setVisible(searchMode);
    ui->searchEdit->setFocus(Qt::ShortcutFocusReason);

    if (!searchMode)
    {
        searchHighlights.clear();
        // Clear search highlights
        ui->logOutput->setExtraSelections(searchHighlights);
    }
    else
    {
        // Search anew, and highlight results.
        highlightSearchResults();
    }
}


void MLogOutputWidget::onSearchTextChanged(const QString &text)
{
    searchText = text;

    if (!searchMode) return;
    highlightSearchResults();
}


void MLogOutputWidget::onLogTextChanged()
{
    // When we are currently searching for text, should we update the results?
    if (searchMode)
    {
        highlightNewSearchResults();
    }
}


void MLogOutputWidget::onSearchPrevious()
{
    if (searchMode)
    {
        if (searchHighlights.isEmpty())
        {
            QMessageBox::warning(this, "No search results found",
                                 "Your search could not be found in the log.");
            return;
        }
        if (!ui->logOutput->find(searchText, QTextDocument::FindBackward))
        {
            QMessageBox::StandardButton ret = QMessageBox::question(this,
                                                                    "Continue search?",
                                                                    "The search reached the start of the log.\n"
                                                                    "Should the search continue from the end?");

            if (ret == QMessageBox::Yes)
            {
                ui->logOutput->moveCursor(QTextCursor::End);
                ui->logOutput->find(searchText, QTextDocument::FindBackward);
            }
        }
    }
}


void MLogOutputWidget::onSearchNext()
{
    if (searchMode)
    {
        if (searchHighlights.isEmpty())
        {
            QMessageBox::warning(this, "No search results found",
                                 "Your search could not be found in the log.");
            return;
        }
        if (!ui->logOutput->find(searchText))
        {
            QMessageBox::StandardButton ret = QMessageBox::question(this,
                                                                    "Continue search?",
                                                                    "The search reached the end of the log.\n"
                                                                    "Should the search continue from the start?");

            if (ret == QMessageBox::Yes)
            {
                ui->logOutput->moveCursor(QTextCursor::Start);
                ui->logOutput->find(searchText);
            }
        }
    }
}


void MLogOutputWidget::onLogLevelFilterChanged(int state)
{
    Q_UNUSED(state);

    ui->logOutput->clear();

    bool suppressSearch = false;
    if (searchMode)
    {
        searchMode = false;
        suppressSearch = true;
    }

    for (auto msg : MLogger::Instance)
    {
        printLogMessage(msg);
    }

    if (suppressSearch)
    {
        searchMode = true;
        highlightSearchResults();
    }
}


void MLogOutputWidget::printLogMessage(MLogger::MLogMessage *message)
{
    if (!logLevelCheckboxes.contains(message->level)) return;
    if (!logLevelCheckboxes[message->level]->isChecked()) return;
    QTextCharFormat format;

    // The text colours are taken from this five-class qualitative colour
    // map from ColorBrewer2.org:
    // https://colorbrewer2.org/#type=qualitative&scheme=Set1&n=5
    // Additionally, the INFO level is printed in black.
    switch (message->level)
    {
    case log4cplus::FATAL_LOG_LEVEL:
        format.setForeground(QColor(152, 78, 163));
        format.setFontWeight(QFont::Bold);
        break;
    case log4cplus::ERROR_LOG_LEVEL:
        format.setForeground(QColor(228, 26, 28));
        break;
    case log4cplus::WARN_LOG_LEVEL:
        format.setForeground(QColor(255, 127, 0));
        break;
    case log4cplus::TRACE_LOG_LEVEL:
        format.setForeground(QColor(77, 175, 74));
        break;
    case log4cplus::DEBUG_LOG_LEVEL:
        format.setForeground(QColor(55, 126, 184));
        break;
    case log4cplus::INFO_LOG_LEVEL:
        format.setForeground(Qt::black);
        break;
    }
    ui->logOutput->setCurrentCharFormat(format);

    // Remember old cursor position and check,
    // if we had a selection.
    QTextCursor cursor = ui->logOutput->textCursor();
    int anchor = cursor.anchor();
    int end = cursor.position();
    bool hasSelection = anchor != end;

    // Should we continue to scroll to the end of the new message or
    // stay on the current message.
    QScrollBar *vScrollBar = ui->logOutput->verticalScrollBar();
    bool scrollToEnd = vScrollBar->maximum() - vScrollBar->value() < vScrollBar->singleStep();
    scrollToEnd = scrollToEnd && cursor.atEnd();

    ui->logOutput->appendPlainText(QString(message->msg.c_str()));

    // If the previous cursor was a selection,
    // reset it here. Otherwise, let the widget move the cursor
    // to the end, so that it scrolls with the new log messages.
    if (hasSelection)
    {
        QTextCursor c(ui->logOutput->document());
        c.setPosition(anchor, QTextCursor::MoveAnchor);
        c.setPosition(end, QTextCursor::KeepAnchor);

        ui->logOutput->setTextCursor(c);
    }
    else if (scrollToEnd)
    {
        ui->logOutput->moveCursor(QTextCursor::End);
    }
}


void MLogOutputWidget::highlightSearchResults()
{
    if (!searchMode) return;

    if (searchText.isEmpty())
    {
        searchHighlights.clear();
        ui->logOutput->setExtraSelections(searchHighlights);
        return;
    }

    ui->logOutput->moveCursor(QTextCursor::Start);

    searchHighlights.clear();

    // First, find all results and highlight them.
    while (ui->logOutput->find(searchText))
    {
        QTextEdit::ExtraSelection extra;
        extra.format.setBackground(SEARCH_HIGHLIGHT_COLOR);
        extra.cursor = ui->logOutput->textCursor();

        searchHighlights.append(extra);
    }

    ui->logOutput->setExtraSelections(searchHighlights);
}


void MLogOutputWidget::highlightNewSearchResults()
{
    if (!searchMode) return;

    if (searchText.isEmpty())
    {
        searchHighlights.clear();
        ui->logOutput->setExtraSelections(searchHighlights);
        return;
    }

    if (searchHighlights.isEmpty())
    {
        highlightSearchResults();
        return;
    }

    // Remember old cursor position and check,
    // if we had a selection.
    QTextCursor cursor = ui->logOutput->textCursor();
    int anchor = cursor.anchor();
    int end = cursor.position();

    // Remove the last search result and move cursor to the one before.
    if (!searchHighlights.isEmpty())
    {
        if (searchHighlights.last().cursor.atEnd())
        {
            ui->logOutput->setTextCursor(searchHighlights.last().cursor);
            searchHighlights.removeLast();

            // Move to the second to last result from last search.
            ui->logOutput->find(searchText, QTextDocument::FindBackward);
        }
    }

    // Find all results from the current cursor and highlight them.
    while (ui->logOutput->find(searchText))
    {
        QTextEdit::ExtraSelection extra;
        extra.format.setBackground(SEARCH_HIGHLIGHT_COLOR);
        extra.cursor = ui->logOutput->textCursor();

        searchHighlights.append(extra);
    }

    ui->logOutput->setExtraSelections(searchHighlights);

    // Move the cursor to the old position,
    // so that only the new search results are highlighted,
    // but the cursor didn't move.
    QTextCursor c(ui->logOutput->document());
    c.setPosition(anchor, QTextCursor::MoveAnchor);
    c.setPosition(end, QTextCursor::KeepAnchor);

    ui->logOutput->setTextCursor(c);
}
}