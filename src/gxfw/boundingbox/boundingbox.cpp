/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017      Marc Rautenhaus [*, previously +]
**  Copyright 2017      Bianca Tost [+]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "boundingbox.h"

// standard library imports
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "actors/nwphorizontalsectionactor.h"
#include "actors/nwpverticalsectionactor.h"
#include "actors/nwpvolumeraycasteractor.h"
#include "gxfw/mactor.h"
#include "gxfw/msystemcontrol.h"
#include "trajectories/pointsource/pointgeneratorinterfacesettings.h"
#include "trajectories/pointsource/pointsourcefactory.h"

namespace Met3D
{

/******************************************************************************
***                              M2DBoundingBox                             ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MBoundingBox::MBoundingBox(QString name, double westLon, double southLat,
                           double eastWestExtent, double northSouthExtent,
                           double bottomPressure_hPa, double topPressure_hPa)
    : MPointGeneratorInterface(MPointGeneratorInterfaceType::BOUNDING_BOX),
      name(name),
      horizontal2DCoords(
          QRectF(westLon, southLat, eastWestExtent, northSouthExtent)),
      eastLon(westLon + eastWestExtent),
      northLat(southLat + northSouthExtent),
      bottomPressure_hPa(bottomPressure_hPa),
      topPressure_hPa(topPressure_hPa),
      signalEmitEnabled(true)
{
    MPointSourceFactory::getInstance()
        ->registerPointGeneratingInterface<MBoundingBoxPointGeneratorSettings>(this);
}


MBoundingBox::~MBoundingBox()
{
    MPointSourceFactory::getInstance()
        ->deregisterPointGeneratingInterface(this);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MBoundingBox::setName(QString bName)
{
    this->name = bName;
    emit nameChanged(bName);
}


void MBoundingBox::setWestLon(double westLon)
{
    // Only apply change if there is a change.
    if (qreal(westLon) != horizontal2DCoords.x())
    {
        horizontal2DCoords.moveLeft(westLon);
        eastLon = horizontal2DCoords.x() + horizontal2DCoords.width();
        emitHorizontal2DCoordsChanged();
    }
}


void MBoundingBox::setSouthLat(double southLat)
{
    // Only apply change if there is a change.
    if (qreal(southLat) != horizontal2DCoords.y())
    {
        horizontal2DCoords.moveTop(southLat);
        northLat = horizontal2DCoords.y() + horizontal2DCoords.height();
        emitHorizontal2DCoordsChanged();
    }
}


void MBoundingBox::setEastWestExtent(double eastWestExtent)
{
    // Only apply change if there is a change.
    if (qreal(eastWestExtent) != horizontal2DCoords.width())
    {
        horizontal2DCoords.setWidth(eastWestExtent);
        eastLon = horizontal2DCoords.x() + horizontal2DCoords.width();
        emitHorizontal2DCoordsChanged();
    }
}


void MBoundingBox::setNorthSouthExtent(double northSouthExtent)
{
    // Only apply change if there is a change.
    if (qreal(northSouthExtent) != horizontal2DCoords.height())
    {
        horizontal2DCoords.setHeight(northSouthExtent);
        northLat = horizontal2DCoords.y() + horizontal2DCoords.height();
        emitHorizontal2DCoordsChanged();
    }
}


void MBoundingBox::setBottomPressure_hPa(double bottomPressure_hPa)
{
    // Only apply change if there is a change.
    if (this->bottomPressure_hPa != bottomPressure_hPa)
    {
        this->bottomPressure_hPa = bottomPressure_hPa;
        emitPressureLevelChanged();
    }
}


void MBoundingBox::setTopPressure_hPa(double topPressure_hPa)
{
    // Only apply change if there is a change.
    if (this->topPressure_hPa != topPressure_hPa)
    {
        this->topPressure_hPa = topPressure_hPa;
        emitPressureLevelChanged();
    }
}


void MBoundingBox::enableEmitChangeSignals(bool enable)
{
    signalEmitEnabled = enable;
}

void MBoundingBox::emitChangeSignal()
{
    if (signalEmitEnabled)
    {
        emit horizontal2DCoordsChanged();
        emit pressureLevelChanged();
        emit coords3DChanged();
        pointInterfaceChanged();
    }
}


void MBoundingBox::emitHorizontal2DCoordsChanged()
{
    if (signalEmitEnabled)
    {
        emit horizontal2DCoordsChanged();
        emit coords3DChanged();
        pointInterfaceChanged();
    }
}


void MBoundingBox::emitPressureLevelChanged()
{
    if (signalEmitEnabled)
    {
        emit pressureLevelChanged();
        emit coords3DChanged();
        pointInterfaceChanged();
    }
}


MPoints *
MBoundingBox::generatePointsFromString(const QString& requestString) const
{
    QStringList reqParameters = requestString.split('/');
    // Get bounding box bounds from request string.
    float minLon = reqParameters[0].toFloat();
    float minLat = reqParameters[1].toFloat();
    float minP = reqParameters[2].toFloat();
    float maxLon = reqParameters[3].toFloat();
    float maxLat = reqParameters[4].toFloat();
    float maxP = reqParameters[5].toFloat();
    float stepLon = reqParameters[6].toFloat();
    float stepLat = reqParameters[7].toFloat();
    float stepP = reqParameters[8].toFloat();

    if (stepLon <= 0.0 || stepLat <= 0.0 || stepP <= 0.0)
    {
        return nullptr;
    }

    auto *seedPts = new MPoints();

    // Iterate over the three dimensions and create evenly spaced points.
    // Order of dimensions is important here for thin out filter to work
    // correctly.
    float lat = minLat;
    while (lat <= maxLat)
    {
        float lon = minLon;
        while (lon <= maxLon)
        {
            float p = minP;
            while (p <= maxP)
            {
                seedPts->append(QVector3D(lon, lat, p));
                p += stepP;
            }
            lon += stepLon;
        }
        lat += stepLat;
    }

    int nlons = static_cast<int>(floor((maxLon - minLon) / stepLon)) + 1;
    int nlats = static_cast<int>(floor((maxLat - minLat) / stepLat)) + 1;
    int nlevs = static_cast<int>(floor((maxP - minP) / stepP)) + 1;

    // Set up the start grid.
    auto startGrid = new MRegularLonLatStructuredPressureGrid(
        nlevs, nlats, nlons);

    for (int p = 0; p < nlats; p++)
    {
        startGrid->setLat(p, minLat + p * stepLat);
    }
    for (int p = 0; p < nlons; p++)
    {
        startGrid->setLon(p, minLon + p * stepLon);
    }
    for (int p = 0; p < nlevs; p++)
    {
        startGrid->setLevel(p, minP + p * stepP);
    }
    seedPts->setSourceGrid(startGrid);

    return seedPts;
}


QString MBoundingBox::generateStringForPointGeneration(
    MPointGeneratorInterfaceSettings* uiSrc) const
{
    auto ui = dynamic_cast<MBoundingBoxPointGeneratorSettings*>(uiSrc);

    // Build request string from the current bounding box state and the
    // currently set UI values.
    QString reqString =
            QString("%1/%2/%3/%4/%5/%6/%7/%8/%9")
                    .arg(getWestLon())
                    .arg(getSouthLat())
                    .arg(getTopPressure_hPa())
                    .arg(getEastLon())
                    .arg(getNorthLat())
                    .arg(getBottomPressure_hPa())
                    .arg(ui->spacingProp.x())
                    .arg(ui->spacingProp.y())
                    .arg(ui->spacingProp.z());

    return reqString;
}


void MBoundingBox::registerPointSource(MPointGeneratorSource* source)
{
    MPointGeneratorInterface::registerPointSource(source);
    connect(this, &MBoundingBox::nameChanged, source->getUI(),
            &MPipelineElementSettings::renameGroup);
}


void MBoundingBox::deregisterPointSource(MPointGeneratorSource* source)
{
    MPointGeneratorInterface::deregisterPointSource(source);
    disconnect(this, &MBoundingBox::nameChanged, source->getUI(),
            &MPipelineElementSettings::renameGroup);
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/


/******************************************************************************
***                          MBoundingBoxInterface                          ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MBoundingBoxInterface::MBoundingBoxInterface(
        MActor *child, MBoundingBoxConnectionType bBoxType,
        MBoundingBoxConnection *parentBBoxConnection)
    : child(child),
      connectionType(bBoxType)
{
    if (parentBBoxConnection)
    {
        bBoxConnections = { parentBBoxConnection };
    }
}


MBoundingBoxInterface::~MBoundingBoxInterface()
{
    for (auto bBoxConnection : bBoxConnections)
    {
        delete bBoxConnection;
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MBoundingBoxInterface::saveConfiguration(QSettings *settings)
{
    for (int i = 0; i < bBoxConnections.size(); i++)
    {
        // Save all connections.
        // The first connection has no suffix for compatibility
        // (useBoundingBox), all other bounding boxes have their index as
        // suffix (useBoundingBox1, useBoundingBox2, ...).
        auto bBoxConnection = bBoxConnections[i];
        QString suffix = "";
        if (i > 0)
        {
            suffix = QString::number(i);
        }

        settings->setValue("useBoundingBox" + suffix, getBoundingBoxName(i));
        if (bBoxConnection->getBoundingBox() != nullptr)
        {
            QRectF coord2D(bBoxConnection->westLon(),
                           bBoxConnection->southLat(),
                           bBoxConnection->eastWestExtent(),
                           bBoxConnection->northSouthExtent());
            settings->setValue("horizontal2DCoords" + suffix, coord2D);
            settings->setValue("bottomPressure_hPa" + suffix,
                               bBoxConnection->bottomPressure_hPa());
            settings->setValue("topPressure_hPa" + suffix,
                               bBoxConnection->topPressure_hPa());
        }
    }
}


void MBoundingBoxInterface::loadConfiguration(QSettings *settings)
{
    QVersionNumber version = readConfigVersion(settings);
    QString name = "None";    
    QRectF horziontal2DCoords = QRectF();
    double bottomPressure_hPa = 1045.;
    double topPressure_hPa = 20.;
    QStringList bBoxIdentifieres = MSystemManagerAndControl::getInstance()
            ->getBoundingBoxesIdentifiers();

    // Drop support for bounding boxes before 1.2.
    if (version < QVersionNumber(1, 2))
    {
        LOG4CPLUS_WARN(
            mlog, QString("You are loading an actor configuration file"
                          " (Actor: %1) that has been written with a previous"
                          " version of Met.3D. Bounding box handling"
                          " has been changed since that version, therefore the"
                          " bounding box of this actor cannot be loaded.")
                      .arg(child->getName()));
        return;
    }

    // Load all connections.
    // The first connection has no suffix for compatibility (useBoundingBox),
    // all other bounding boxes have their index as suffix (useBoundingBox1,
    // useBoundingBox2, ...). Iterate until the given key does not exist.
    int currentBoundingBoxIndex = 0;
    QString suffix = "";
    while (true)
    {
        if (currentBoundingBoxIndex > 0)
        {
            suffix = QString::number(currentBoundingBoxIndex);
        }
        if (! settings->contains("useBoundingBox" + suffix))
        {
            break;
        }

        // Switch to none bbox, so when loading the correct one we actually
        // refresh the property and emit the change events.
        getBBoxConnection(currentBoundingBoxIndex)->bBoxProp.suppressValueEvent(true);
        switchToBoundingBox("None", currentBoundingBoxIndex);
        getBBoxConnection(currentBoundingBoxIndex)->bBoxProp.suppressValueEvent(false);
        // For later versions load name of bounding box.
        name = settings->value("useBoundingBox" + suffix, "None").toString();
        if (! (bBoxIdentifieres.contains(name)))
        {
            horziontal2DCoords =
                settings->value("horizontal2DCoords" + suffix, QRectF()).toRectF();
            if (! (horziontal2DCoords.isEmpty()))
            {
                // Offer the user to use the saved coordinates to create new
                // bounding box if the bounding box is missing and configuration
                // contains coordinates.
                int answer = QMessageBox::question(
                    nullptr, child->getName(),
                    "Could not find bounding box '" + name
                        + "'.\nDo you want to create a new bounding box"
                          " object from the coordinates specified in the"
                          " configuration file?\n"
                          "[Actor: "
                        + child->getName() + "]",
                    QMessageBox::Yes, QMessageBox::No);
                if (QMessageBox::StandardButton(answer) == QMessageBox::Yes)
                {
                    bottomPressure_hPa =
                        settings->value("bottomPressure_hPa" + suffix, 1045.).toDouble();
                    topPressure_hPa =
                        settings->value("topPressure_hPa" + suffix, 20.).toDouble();
                    ;
                    MSystemManagerAndControl::getInstance()
                        ->getBoundingBoxDock()
                        ->addBoundingBox(name, &horziontal2DCoords,
                                         bottomPressure_hPa, topPressure_hPa);
                }
                else
                {
                    name = "None";
                }
            }
            else
            {
                QMessageBox::warning(
                    nullptr, "Warning",
                    "Could find neither bounding box '" + name
                        + "' nor bounding box coordinates to set up new"
                          " bounding box.");
                name = "None";
            }

            if (name == "None")
            {
                // Since 'None' is also part of the bounding box identifiers,
                // we need to check if the number of identifieres is greater
                // one.
                if (bBoxIdentifieres.size() > 1)
                {
                    // Offer the user to select one of the existing bounding
                    // boxes.
                    int answer = QMessageBox::question(
                        nullptr, child->getName(),
                        "Do you want to select one of the existing"
                        " bounding boxes?\n"
                        "[Actor: "
                            + child->getName() + "]",
                        QMessageBox::Yes, QMessageBox::No);
                    if (QMessageBox::StandardButton(answer) == QMessageBox::Yes)
                    {
                        bool ok;
                        bBoxIdentifieres.removeOne("None");
                        name = QInputDialog::getItem(
                            nullptr, child->getName(),
                            "Bounding Box: ", bBoxIdentifieres, 0, false, &ok);
                        // Reset name to "None" if user canceled selection.
                        if (! ok)
                        {
                            name = "None";
                        }
                    }
                }
            }

            // If user chose to not create or select a bounding box, inform
            // him/her that the bounding box will be set to "None".
            if (name == "None")
            {
                QMessageBox::information(nullptr, child->getName(),
                                         "Setting bounding box to 'None'.");
            }
        }

        switchToBoundingBox(name, currentBoundingBoxIndex);
        currentBoundingBoxIndex += 1;
    }
}


QString MBoundingBoxInterface::getBoundingBoxName(int index)
{
    if (bBoxConnections[index]->getBoundingBox() == nullptr)
    {
        return "None";
    }
    else
    {
        return bBoxConnections[index]->getBoundingBox()->getID();
    }
}


void MBoundingBoxInterface::switchToBoundingBox(QString bBoxName, int index)
{
    if (index >= bBoxConnections.size())
    {
        return;
    }

    if (!(MSystemManagerAndControl::getInstance()
          ->getBoundingBoxesIdentifiers().contains(bBoxName)))
    {
        QMessageBox::warning(
                    nullptr, "Warning",
                    "Could not find bounding box '" + bBoxName
                    + "'.\nSetting bounding box to 'None'.");
        bBoxConnections[index]->switchToBoundingBox("None");
    }
    else
    {
        bBoxConnections[index]->switchToBoundingBox(bBoxName);
    }
}


int MBoundingBoxInterface::insertBoundingBoxProperty(MProperty *parentGroup)
{
    // Add a new bounding box with the same type as the first one.
    bBoxConnections.append(new MBoundingBoxConnection(this, connectionType));

    int newIndex = bBoxConnections.size() - 1;
    parentGroup->addSubProperty(bBoxConnections[newIndex]->getProperty());
    return newIndex;
}


int MBoundingBoxInterface::insertBoundingBoxProperty(
        Met3D::MProperty &parentGroup)
{
    return insertBoundingBoxProperty(&parentGroup);
}


/******************************************************************************
***                          MBoundingBoxConnection                         ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MBoundingBoxConnection::MBoundingBoxConnection(
        MBoundingBoxInterface *actor, MBoundingBoxConnectionType type)
    : QObject(),
      actor(actor),
      type(type),
      boundingBox(nullptr),
      drawBBox(true),
      suppressUpdates(false)
{
    sysMC = MSystemManagerAndControl::getInstance();

    // Create and initialise properties for the GUI.
    // ===============================================
    bBoxProp = MEnumProperty("Bounding box");
    bBoxProp.setConfigKey("bounding_box");
    QStringList bBoxList = sysMC->getBoundingBoxDock()
            ->getSortedBoundingBoxesList();
    bBoxProp.setEnumNames(bBoxList);
    bBoxProp.saveAsEnumName(true);
    bBoxProp.registerValueCallback([=]()
    {
        if (suppressUpdates)
        {
            return;
        }
        setBoundingBox(bBoxProp.getSelectedEnumName());
        onBoundingBoxChanged();
    });

    // Set default bounding box to second entry if there exists a second entry.
    if (bBoxList.length() > 1)
    {
        suppressUpdates = true;
        bBoxProp.setValue(1);
        setBoundingBox(bBoxList.at(1));
        suppressUpdates = false;
    }

    connect(sysMC, SIGNAL(boundingBoxCreated()),
            this, SLOT(onBoundingBoxCreated()));
    connect(sysMC, SIGNAL(boundingBoxDeleted(QString)),
            this, SLOT(onBoundingBoxDeleted(QString)));
    connect(sysMC, SIGNAL(boundingBoxRenamed()),
            this, SLOT(onBoundingBoxRenamed()));
}


MBoundingBoxConnection::~MBoundingBoxConnection()
{
    // Disconnect signals otherwise program might crash if signal is sent,
    // but the actor was deleted previously. (This applies especially to
    // signals send by sysMC.)

    if (boundingBox != nullptr)
    {
        switch (type)
        {
        case MBoundingBoxConnectionType::HORIZONTAL:
            disconnect(boundingBox, SIGNAL(horizontal2DCoordsChanged()),
                       this, SLOT(onBoundingBoxChanged()));
            break;
        case MBoundingBoxConnectionType::VERTICAL:
            disconnect(boundingBox, SIGNAL(pressureLevelChanged()),
                       this, SLOT(onBoundingBoxChanged()));
            break;
        case MBoundingBoxConnectionType::VOLUME:
            disconnect(boundingBox, SIGNAL(coords3DChanged()),
                       this, SLOT(onBoundingBoxChanged()));
            break;
        default:
            break;
        }
    }

    disconnect(sysMC, SIGNAL(boundingBoxCreated()),
               this, SLOT(onBoundingBoxCreated()));
    disconnect(sysMC, SIGNAL(boundingBoxDeleted(QString)),
               this, SLOT(onBoundingBoxDeleted(QString)));
    disconnect(sysMC, SIGNAL(boundingBoxRenamed()),
               this, SLOT(onBoundingBoxRenamed()));
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MBoundingBoxConnection::switchToBoundingBox(const QString& name)
{
    bBoxProp.setEnumItem(name);
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MBoundingBoxConnection::onBoundingBoxChanged()
{
    // The bounding box has changed (extend, position or selected). Inform actor
    // about it.
    actor->onBoundingBoxChanged();
}


void MBoundingBoxConnection::onBoundingBoxCreated()
{
    // Replace bounding box list by updated list.
    QStringList bBoxList = sysMC->getBoundingBoxDock()
            ->getSortedBoundingBoxesList();
    // Suppress updates since bounding box doesn't change.
    bBoxProp.setEnumNames(bBoxList, bBoxProp.value());
    // Since 'None' is always first in the list, there is no need to change the
    // position if no bounding box is set.
    if (boundingBox != nullptr)
    {
        // Since the bounding box names are sorted alphanumerically the new name
        // may result in a new position in the list. Thus it is necessary to set
        // the enum item to the new name.
        bBoxProp.setEnumItem(boundingBox->getID());
    }
    suppressUpdates = false;
}


void MBoundingBoxConnection::onBoundingBoxDeleted(QString name)
{
    // Get current bounding box name to check whether it was deleted or not.
    QString currentBBoxName = bBoxProp.getSelectedEnumName();

    // Replace bounding box list by updated list.
    QStringList bBoxList = sysMC->getBoundingBoxDock()
            ->getSortedBoundingBoxesList();
    // Suppress updates since bounding box only changes if current bounding box
    // was deleted.
    suppressUpdates = true;
    bBoxProp.setEnumNames(bBoxList, bBoxProp.value());
    // Since 'None' is always first in the list, there is no need to change the
    // position if no bounding box is set.
    if (boundingBox != nullptr)
    {
        // Current bounding box was deleted thus set use bbox to "None" and tell
        // the actor the bounding box has changed. (No need to change the enum
        // item since replacing the list automatically switches to the first
        // item.)
        if (name == currentBBoxName)
        {
            // Bounding box was deleted thus pointer should be set to nullptr.
            boundingBox = nullptr;
            actor->onBoundingBoxChanged();
        }
        else
        {
            // Since the bounding box names are sorted alphanumerically the new
            // name may result in a new position in the list. Thus it is
            // necessary to set the enum item to the new name.
            bBoxProp.setEnumItem(boundingBox->getID());
        }
    }
    suppressUpdates = false;
}


void MBoundingBoxConnection::onBoundingBoxRenamed()
{
    // Replace bounding box list by updated list.
    QStringList bBoxList = sysMC->getBoundingBoxDock()
            ->getSortedBoundingBoxesList();
    // Suppress updates since bounding box doesn't change.
    suppressUpdates = true;
    bBoxProp.setEnumNames(bBoxList, bBoxProp.value());
    // Since 'None' is always first in the list, there is no need to change the
    // position if no bounding box is set.
    if (boundingBox != nullptr)
    {
        // Since the bounding box names are sorted alphanumerically the new name
        // may result in a new position in the list. Thus it is necessary to set
        // the enum item to the new name.
        bBoxProp.setEnumItem(boundingBox->getID());
    }
    suppressUpdates = false;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MBoundingBoxConnection::setBoundingBox(QString bBoxID)
{
    if (boundingBox != nullptr)
    {
        switch (type)
        {
        case MBoundingBoxConnectionType::HORIZONTAL:
            disconnect(boundingBox, SIGNAL(horizontal2DCoordsChanged()),
                       this, SLOT(onBoundingBoxChanged()));
            break;
        case MBoundingBoxConnectionType::VERTICAL:
            disconnect(boundingBox, SIGNAL(pressureLevelChanged()),
                       this, SLOT(onBoundingBoxChanged()));
            break;
        case MBoundingBoxConnectionType::VOLUME:
            disconnect(boundingBox, SIGNAL(coords3DChanged()),
                       this, SLOT(onBoundingBoxChanged()));
            break;
        }
    }

    boundingBox = sysMC->getBoundingBox(bBoxID);

    if (boundingBox != nullptr)
    {
        switch (type)
        {
        case MBoundingBoxConnectionType::HORIZONTAL:
            connect(boundingBox, SIGNAL(horizontal2DCoordsChanged()),
                    this, SLOT(onBoundingBoxChanged()));
            break;
        case MBoundingBoxConnectionType::VERTICAL:
            connect(boundingBox, SIGNAL(pressureLevelChanged()),
                    this, SLOT(onBoundingBoxChanged()));
            break;
        case MBoundingBoxConnectionType::VOLUME:
            connect(boundingBox, SIGNAL(coords3DChanged()),
                    this, SLOT(onBoundingBoxChanged()));
            break;
        }
    }
}

} // namespace Met3D

