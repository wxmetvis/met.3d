/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2022-2023 Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MBOUNDINGBOX_H
#define MBOUNDINGBOX_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "gxfw/properties/menumproperty.h"
#include "trajectories/pointsource/pointgeneratorinterface.h"

namespace Met3D
{

/**
  @brief MBoundingBox represents a bounding box defining a domain used by
  actors inheriting from @ref MBoundingBoxInterface as render region.
 */
class MBoundingBox : public QObject, public MPointGeneratorInterface
{
    Q_OBJECT

public:
    MBoundingBox(QString name, double westLon, double southLat,
                 double eastWestExtent, double northSouthExtent,
                 double bottomPressure_hPa, double topPressure_hPa);

    ~MBoundingBox();

    QString getID() const { return name; }

    double getWestLon() const { return horizontal2DCoords.x(); }
    double getSouthLat() const { return horizontal2DCoords.y(); }
    double getEastWestExtent() const { return horizontal2DCoords.width(); }
    double getNorthSouthExtent() const { return horizontal2DCoords.height(); }
    QRectF getHorizontal2DCoords() const { return horizontal2DCoords; }
    double getEastLon() const { return eastLon; }
    double getNorthLat() const { return northLat; }

    double getBottomPressure_hPa() const { return bottomPressure_hPa; }
    double getTopPressure_hPa() const { return topPressure_hPa; }

    void setName(QString bName);

    /**
      setWestLon checks if @p westLon differs from x coordinate of
      @ref MBoundingBox::horizontal2DCoords and triggers changed-signal only for
      actual updates.
     */
    void setWestLon(double westLon);
    /**
      setSouthLat checks if @p southLat differs from y coordinate of
      @ref MBoundingBox::horizontal2DCoords and triggers changed-signal only for
      actual updates.
     */
    void setSouthLat(double southLat);
    /**
      setEastWestExtent checks if @p eastWestExtent differs from width of
      @ref MBoundingBox::horizontal2DCoords and triggers changed-signal only for
      actual updates.
     */
    void setEastWestExtent(double eastWestExtent);
    /**
      setNorthSouthExtent checks if @p northSouthExtent differs from height of
      @ref MBoundingBox::horizontal2DCoords and triggers changed-signal only for
      actual updates.
     */
    void setNorthSouthExtent(double northSouthExtent);
    /**
      setBottomPressure_hPa checks if @p bottomPressure_hPa differs from
      @ref MBoundingBox::bottomPressure_hPa and triggers changed-signal only for
      actual updates.
     */
    void setBottomPressure_hPa(double bottomPressure_hPa);
    /**
      setTopPressure_hPa checks if @p topPressure_hPa differs from
      @ref MBoundingBox::topPressure_hPa and triggers changed-signal only for
      actual updates.
     */
    void setTopPressure_hPa(double topPressure_hPa);

    /**
      Enables or disables emission of change signals.
     */
    void enableEmitChangeSignals(bool enable);

    /**
      Inform all actor types (horizontal, vertical, 3D) about changes of the
      bounding box coordinates.
     */
    void emitChangeSignal();

    /**
      Emit signals @ref horizontal2DCoordsChanged() and @ref coords3DChanged()
      if signal emitting is enabled.
     */
    void emitHorizontal2DCoordsChanged();
    /**
      Emit signals @ref pressureLevelChanged() and @ref coords3DChanged()
      if signal emitting is enabled.
     */
    void emitPressureLevelChanged();

    /** Implement MPointGeneratorInterface interface. */

    QString getPointInterfaceName() const override { return name; }

    QStringList getPointGeneratingSourceBaseRequestKeys() const override
    { return { "BBOX_POINT_SOURCE" }; }

    MPoints *
    generatePointsFromString(const QString& requestString) const override;

    QString generateStringForPointGeneration(
        MPointGeneratorInterfaceSettings* uiSrc) const override;

    /**
     * Override to connect name change signal of bounding box to new point
     * source.
     */
    void registerPointSource(MPointGeneratorSource* source) override;

    /**
     * Override to disconnect name change signal of bounding box to source to
     * be deleted.
     */
    void deregisterPointSource(MPointGeneratorSource* source) override;

signals:
    // Use separated signals for 2D and 3D to avoid duplicated invocation of
    // computation for 3D actors (emitChangeSignal()).
    void horizontal2DCoordsChanged();
    void pressureLevelChanged();
    void coords3DChanged();
    void nameChanged(QString newName);

protected:
    QString name;

    QRectF horizontal2DCoords;
    double eastLon;
    double northLat;
    double bottomPressure_hPa;
    double topPressure_hPa;

    // Indicator to enable or disable signal emitting.
    bool signalEmitEnabled;
};

enum class MBoundingBoxConnectionType
{
    HORIZONTAL = 0,
    VERTICAL   = 1,
    VOLUME     = 2
};

class MBoundingBoxConnection;
class MActor;

/**
  @brief MBoundingBoxInterface is the abstract base class for all actors using
  bounding boxes.

  It holds a pointer to @ref MBoundingBoxConnection, which handles the
  connection between actor and bounding box by using signals and slots.
  @ref MBoundingBoxConnection is necessary because if MBoundingBoxInterface
  would inheritate from QObject this would lead to a diamond inheritance which
  cannot be solved due to the static_cast automatically created in Qt's
  moc-files when using signals and slots.
  (cp. http://www.drdobbs.com/cpp/multiple-inheritance-considered-useful/184402074).

  NOTE: To use bounding boxes it is mandatory to insert the bounding box
  property with @ref insertBoundingBoxProperty() during creation of properties
  in the constructor of the child class!

  NOTE (cf, 2024-04-05): Added support for multiple bounding box per actor.
  The interface now holds a list of connections, which can be individually
  accessed. For backward compatibility with existing code and config files,
  the already present methods will just work on the first connection in the
  list.
 */
class MBoundingBoxInterface
{
public:
    MBoundingBoxInterface(MActor *child,
                          MBoundingBoxConnectionType bBoxType,
                          MBoundingBoxConnection *parentBBoxConnection = nullptr);
    ~MBoundingBoxInterface();

     /**
      * Implements the reaction of the actor to the change of a bounding box.
      */
    virtual void onBoundingBoxChanged() = 0;

    void saveConfiguration(QSettings *settings);
    void loadConfiguration(QSettings *settings);

    /**
     * Returns pointer to actor inheriting from this interface object.
     */
    MActor *getChild() { return child; }

    /**
     * Returns the associated bounding box connection.
     */
    MBoundingBoxConnection *getBBoxConnection(int index = 0) const
    {
        return bBoxConnections[index];
    }

    /**
      Returns name of the currently selected bounding box if present otherwise
      this method returns "None".
      Optional: Index to the list of bounding box connections used by the actor..
     */
     /**
      * Returns the name of the currently selected bounding box. If no bounding
      * box is selected, returns "None".
      * @param index Index which of the bounding box connections should be
      * requested. Defaults to 0 for actors only having one bounding box.
      * @return The name of the bounding box.
      */
    QString getBoundingBoxName(int index = 0);

     /**
      * Switches to bounding box with @p name.
      * @param bBoxName The name of the bounding box.
      * @param index Optional the index if the actor at this interface handles
      * multiple bounding boxes.
      */
    void switchToBoundingBox(QString bBoxName, int index = 0);

     /**
      * Insert a new bounding box property as subproperty of @p parentGroup.
      * This method registers a new connection to the interface.
      * @param parentGroup The parent group to add the bounding box to.
      * @return The assigned index of the bounding box connection. Can be used
      * to fetch one of the actor bounding boxes with getBoundingBoxName(int).
      * If the actor only has one bounding box, its index is zero and the return
      * value does not have to be saved by the caller.
      */
    int insertBoundingBoxProperty(MProperty *parentGroup);

    int insertBoundingBoxProperty(MProperty &parentGroup);

    // Pointer to actor inheriting from the interface.
    MActor *child;

private:
    MBoundingBoxConnectionType connectionType;
    // Object realising and handling connection between actor and bounding box.
    QList<MBoundingBoxConnection*> bBoxConnections;

};


class MSystemManagerAndControl;

/**
  @brief The MBoundingBoxConnection class handles the connection between a
  bounding box and the actor using it.
 */
class MBoundingBoxConnection : public QObject
{
    // Only allow interfaces to create connections to bounding boxes.
    friend class MBoundingBoxInterface;
    Q_OBJECT
private:
    MBoundingBoxConnection(MBoundingBoxInterface *actor,
                           MBoundingBoxConnectionType type);
public:
    ~MBoundingBoxConnection();

    MBoundingBox *getBoundingBox() { return boundingBox; }
    MProperty *getProperty() { return &bBoxProp; }
    MBoundingBoxInterface *getActor() { return actor; }
    MBoundingBoxConnectionType getType() { return type; }

    // Methods to access bounding box coordinates.

    double westLon() { return boundingBox->getWestLon(); }
    double southLat() { return boundingBox->getSouthLat(); }
    double eastLon() { return boundingBox->getEastLon(); }
    double northLat() { return boundingBox->getNorthLat(); }
    double eastWestExtent() { return boundingBox->getEastWestExtent(); }
    double northSouthExtent() { return boundingBox->getNorthSouthExtent(); }
    QRectF horizontal2DCoords() { return boundingBox->getHorizontal2DCoords(); }
    double bottomPressure_hPa() { return boundingBox->getBottomPressure_hPa(); }
    double topPressure_hPa() { return boundingBox->getTopPressure_hPa(); }

    /**
      @brief Switches to bounding box called @p name.
     */
    void switchToBoundingBox(const QString& name);

public slots:
    /**
      Calls onBoundingBoxChanged() of @ref bboxactor.
     */
    void onBoundingBoxChanged();

    /**
     Connects to the MSystemManagerAndControl::boundingBoxCreated() signal.
     It updates the list of @ref bBoxProperty ensuring the original bounding box
     is still selected afterwards.
     */
    void onBoundingBoxCreated();

    /**
     Connects to the MSystemManagerAndControl::boundingBoxDeleted(QString) signal.
     It updates the list of @ref bBoxProperty ensuring the original bounding box
     is still selected afterwards if it is still present. If the current
     selected bounding box was deleted, it switches to "None".
     */
    void onBoundingBoxDeleted(QString name);

    /**
     Connects to the MSystemManagerAndControl::boundingBoxRenamed() signal.
     It updates the list of @ref bBoxProperty ensuring the original bounding box
     is still selected afterwards.
     */
    void onBoundingBoxRenamed();

protected:
    void setBoundingBox(QString bBoxID);

    MSystemManagerAndControl *sysMC;
    MBoundingBoxInterface *actor;
    MBoundingBoxConnectionType type;
    MBoundingBox *boundingBox;

    MEnumProperty bBoxProp;

    bool drawBBox;
    bool suppressUpdates;
};

} // namespace Met3D

#endif // MBOUNDINGBOX_H
