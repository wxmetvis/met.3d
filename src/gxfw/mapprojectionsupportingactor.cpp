/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020 Kameswarro Modali [*]
**  Copyright 2017 Bianca Tost [+]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mapprojectionsupportingactor.h"

// standard library imports
#include <cstdio>

// related third party imports

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/properties/mpropertytemplates.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MMapProjectionSupportingActor::MMapProjectionSupportingActor(QList<MapProjectionType> supportedProjections)
    : MActor(),
      mapProjection(MAPPROJECTION_CYLINDRICAL),
      previousMapProjection(MAPPROJECTION_CYLINDRICAL),
      projLibraryDefaultString(
          "+proj=stere +a=6378273 +b=6356889.44891 +lat_0=90 +lat_ts=70 +lon_0=0"),
      projLibraryString(projLibraryDefaultString)
{
    // Create and initialise QtProperties for the GUI.
    // ===============================================
    setName("Grid projection support enabled");

    mapProjectionPropertiesSubGroup = MProperty("Map projection support");
    mapProjectionPropertiesSubGroup.setConfigGroup(MMapProjectionSupportingActor::getSettingsID());

    // Drop-down list for choosing type of projection.
    QStringList gridProjectionNames;
    for (MapProjectionType proj : supportedProjections)
    {
        gridProjectionNames << mapProjectionToString(proj);
    }
    mapProjectionTypesProp = MEnumProperty("Type of projection");
    mapProjectionTypesProp.setEnumNames(gridProjectionNames);
    mapProjectionTypesProp.setValue(mapProjection);
    mapProjectionTypesProp.saveAsEnumName(true);
    mapProjectionTypesProp.setConfigKey("type_of_projection");
    mapProjectionPropertiesSubGroup.addSubProperty(mapProjectionTypesProp);

    // Inputs for rotated lat-lon projection.
    rotatedNorthPoleProp = MPropertyTemplates::GeographicPosition2D("Rotated north pole", QPointF(-170., 40.));
    rotatedNorthPoleProp.setEnabled(false);
    rotatedNorthPoleProp.setConfigKey("rotated_north_pole");
    mapProjectionPropertiesSubGroup.addSubProperty(rotatedNorthPoleProp);

    // Proj.org string, see https://proj.org/operations/projections/index.html.
    projLibraryStringProp = MStringProperty("Proj-string", projLibraryDefaultString);
    projLibraryStringProp.setConfigKey("proj_string");
    projLibraryStringProp.setTooltip("Enter a valid proj-string, see: https://proj.org/operations/projections/index.html\n"
                                          "Note that this does NOT project the data - only this actor's geometry.");
    projLibraryStringProp.setEnabled(false);
    mapProjectionPropertiesSubGroup.addSubProperty(projLibraryStringProp);

    projLibraryApplyProp = MButtonProperty("Apply projection", "Apply");
    projLibraryApplyProp.setEnabled(false);
    projLibraryApplyProp.registerValueCallback([=]()
    {
       this->projLibraryString = projLibraryStringProp;
    });
    mapProjectionPropertiesSubGroup.addSubProperty(projLibraryApplyProp);
}


MMapProjectionSupportingActor::~MMapProjectionSupportingActor()
= default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MMapProjectionSupportingActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MActor::loadConfigurationPrior_V_1_14(settings);
    settings->beginGroup(MMapProjectionSupportingActor::getSettingsID());

    QString projection = settings->value("mapProjection",
                                         mapProjectionToString(MAPPROJECTION_CYLINDRICAL)).toString();

    mapProjectionTypesProp.setEnumItem(projection);

    rotatedNorthPoleProp = settings->value("rotatedNorthPole", QPointF(-180., 90.)).toPointF();

    // Explicitly load value into string variable here as changing the property
    // will not trigger a re-computation of graticule/map geometry.
    projLibraryString = settings->value("projString",
                                        projLibraryDefaultString).toString();

    settings->endGroup();
}

void MMapProjectionSupportingActor::updateMapProjectionProperties()
{    
    enableActorUpdates(false);

    MapProjectionType projIndex = stringToMapProjection(
            mapProjectionTypesProp.getSelectedEnumName());

    previousMapProjection = mapProjection;

    switch (projIndex)
    {
    case MAPPROJECTION_CYLINDRICAL:
        mapProjection = MAPPROJECTION_CYLINDRICAL;
        rotatedNorthPoleProp.setEnabled(false);
        projLibraryStringProp.setEnabled(false);
        projLibraryApplyProp.setEnabled(false);
        break;
    case MAPPROJECTION_ROTATEDLATLON:
        mapProjection = MAPPROJECTION_ROTATEDLATLON;
        rotatedNorthPoleProp.setEnabled(true);
        projLibraryStringProp.setEnabled(false);
        projLibraryApplyProp.setEnabled(false);
        break;
    case MAPPROJECTION_PROJ_LIBRARY:
        mapProjection = MAPPROJECTION_PROJ_LIBRARY;
        rotatedNorthPoleProp.setEnabled(false);
        projLibraryStringProp.setEnabled(true);
        projLibraryApplyProp.setEnabled(true);
        break;
    }

    enableActorUpdates(true);
}


QString MMapProjectionSupportingActor::getProjectionIdentifier() const
{
    QString identifier = mapProjectionToString(mapProjection);
    if (mapProjection == MAPPROJECTION_ROTATEDLATLON)
    {
        identifier += QString::number(rotatedNorthPoleProp.x())
            + "_" + QString::number(rotatedNorthPoleProp.y());
    }
    else if (mapProjection == MAPPROJECTION_PROJ_LIBRARY)
    {
        // Remove the '='/';' from the Proj string to keep it conform
        // for data requests.
        QString strippedProj4 = QString(projLibraryString)
            .remove('=').remove(';');
        identifier += strippedProj4;
    }
    return identifier;
}


bool MMapProjectionSupportingActor::isMapProjectionProj4() const
{
    return mapProjection == MAPPROJECTION_PROJ_LIBRARY;
}


bool MMapProjectionSupportingActor::isMapProjectionRotated() const
{
    return mapProjection == MAPPROJECTION_ROTATEDLATLON;
}


MMapProjectionSupportingActor::MapProjectionType
MMapProjectionSupportingActor::stringToMapProjection(const QString& gridProjectionName)
{
    if (gridProjectionName == "cylindrical")
    {
        return MAPPROJECTION_CYLINDRICAL;
    }
    else if (gridProjectionName == "rotated lat.-lon.")
    {
        return MAPPROJECTION_ROTATEDLATLON;
    }
    else if (gridProjectionName == "proj.org projection")
    {
        return MAPPROJECTION_PROJ_LIBRARY;
    }
    else
    {
        return MAPPROJECTION_CYLINDRICAL;
    }
}


QString MMapProjectionSupportingActor::mapProjectionToString(
        MapProjectionType gridProjection)
{
    switch (gridProjection)
    {
    case MAPPROJECTION_CYLINDRICAL: return "cylindrical";
    case MAPPROJECTION_ROTATEDLATLON: return "rotated lat.-lon.";
    case MAPPROJECTION_PROJ_LIBRARY: return "proj.org projection";
    default: return "cylindrical";
    }
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/


} // namespace Met3D
