/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2021-2022 Christoph Neuhauser [+]
**  Copyright 2024      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "transferfunction.h"

// standard library imports
#include <iostream>
#include <utility>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "util/mutil.h"
#include "mainwindow.h"
#include "gxfw/properties/mpropertytemplates.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTransferFunction::MTransferFunction(QObject *parent) :
        MActor(parent, false),
        tfTexture(nullptr),
        textureUnit(-1),
        vertexBuffer(nullptr),
        numVertices(0),
        numTicks(0),
        minimumValue(0.f),
        maximumValue(100.f),
        currentDragHandleID(-1)
{
    enablePicking(true);
    // Create and initialise QtProperties for the GUI.
    // ===============================================

    // Properties related to labelling the colour bar.
    // ===============================================

    maxNumTicksProp = MIntProperty("Number of tick marks", 11);
    maxNumTicksProp.setConfigKey("number_of_tick_marks");
    maxNumTicksProp.setMinimum(0);
    labelProps.enabledProp.addSubProperty(maxNumTicksProp);

    maxNumLabelsProp = MIntProperty("Number of tick labels", 6);
    maxNumLabelsProp.setConfigKey("number_of_tick_labels");
    maxNumLabelsProp.setMinimum(0);
    labelProps.enabledProp.addSubProperty(maxNumLabelsProp);

    tickWidthProp = MDoubleProperty("Tick mark length", 0.015);
    tickWidthProp.setConfigKey("tick_mark_length");
    tickWidthProp.setDecimals(3);
    tickWidthProp.setStep(0.001);
    labelProps.enabledProp.addSubProperty(tickWidthProp);

    labelSpacingProp = MFloatProperty("Space between tick and label", 0.01);
    labelSpacingProp.setConfigKey("space_label_tick");
    labelSpacingProp.setDecimals(3);
    labelSpacingProp.setStep(0.001);
    labelProps.enabledProp.addSubProperty(labelSpacingProp);

    descriptionLabelProp = MStringProperty("Description", "");
    descriptionLabelProp.setConfigKey("display_description");
    labelProps.enabledProp.addSubProperty(descriptionLabelProp);

    flipDescriptionProp = MBoolProperty("Flip description label", false);
    flipDescriptionProp.setConfigKey("flip_description_label");
    labelProps.enabledProp.addSubProperty(flipDescriptionProp);

    // Properties related to data range.
    // =================================

    rangeGroupProp = MProperty("Range");
    actorPropertiesSupGroup.addSubProperty(rangeGroupProp);

    int significantDigits = 3;
    float step = 1.;

    minimumValueProp = MSciFloatProperty("Minimum value", minimumValue);
    minimumValueProp.setConfigKey("min_value");
    minimumValueProp.setSignificantDigits(significantDigits);
    minimumValueProp.setStep(step);
    rangeGroupProp.addSubProperty(minimumValueProp);

    maximumValueProp = MSciFloatProperty("Maximum value", maximumValue);
    maximumValueProp.setConfigKey("max_value");
    maximumValueProp.setSignificantDigits(significantDigits);
    maximumValueProp.setStep(step);
    rangeGroupProp.addSubProperty(maximumValueProp);

    valueOptionsGroupProp = MProperty("Minimum / Maximum adjustment");
    rangeGroupProp.addSubProperty(valueOptionsGroupProp);

    valueSignificantDigitsProp = MIntProperty("Significant digits", significantDigits);
    valueSignificantDigitsProp.setConfigKey("significant_digits");
    valueSignificantDigitsProp.setMinMax(0, 9);
    valueOptionsGroupProp.addSubProperty(valueSignificantDigitsProp);

    valueStepProp = MSciFloatProperty("Step", step);
    valueStepProp.setConfigKey("step");
    valueStepProp.setSignificantDigits(significantDigits);
    valueStepProp.setStep(0.1);
    valueOptionsGroupProp.addSubProperty(valueStepProp);

    // General properties.
    // ===================

    positionProp = MRectProperty("Viewport position and size", QRectF(0.9, 0.9, 0.05, 0.5));
    positionProp.setConfigKey("viewport_position_and_size");
    positionProp.setStep(0.1);
    actorPropertiesSupGroup.addSubProperty(positionProp);

    orientation = MEnumProperty("Orientation", {"Vertical", "Horizontal"}, 0);
    orientation.setConfigKey("orientation");
    orientation.registerValueCallback([=]()
    {
        QRectF rect = positionProp;
        double width = positionProp.value().height();
        double height = positionProp.value().width();

        if (orientation == HORIZONTAL)
        {
            rect.setX(rect.x() - width / 2.0f);
            rect.setY(rect.y() - height / 2.0f);

            flipDescriptionProp.setHidden(true);
        }
        else if (orientation == VERTICAL)
        {
            rect.setX(rect.x() + height / 2.0f);
            rect.setY(rect.y() + width / 2.0f);

            flipDescriptionProp.setHidden(false);
        }

        rect.setWidth(width);
        rect.setHeight(height);

        positionProp = rect;
    });
    actorPropertiesSupGroup.addSubProperty(orientation);
}


MTransferFunction::~MTransferFunction()
= default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MTransferFunction::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MActor::loadConfigurationPrior_V_1_14(settings);
    settings->beginGroup(MTransferFunction::getSettingsID());

    // Properties related to labelling the colour bar.
    // ===============================================
    maxNumTicksProp = settings->value("maxNumTicks", 11).toInt();
    maxNumLabelsProp = settings->value("maxNumLabels", 6).toInt();

    tickWidthProp = settings->value("tickLength", 0.015).toDouble();
    labelSpacingProp = settings->value("labelSpacing", 0.010).toFloat();

    // Properties related to data range.
    // =================================
    int significantDigits;

    // Support of old configuration files.
    if (settings->contains("valueDecimals"))
    {
        significantDigits = settings->value("valueDecimals", 3).toInt();
    }
    else
    {
        significantDigits = settings->value("valueSignificantDigits", 3)
                                    .toInt();
    }

    setValueSignificantDigits(significantDigits);
    setValueStep(settings->value("valueStep", 1.).toFloat());
    setMinimumValue(settings->value("minimumValue", 0.).toFloat());
    setMaximumValue(settings->value("maximumValue", 100.).toFloat());

    // General properties.
    // ===================
    setPosition(settings->value("position",
                                QRectF(0.9, 0.9, 0.05, 0.5)).toRectF());

    settings->endGroup();
}


void MTransferFunction::reloadShaderEffects()
{
    LOG4CPLUS_INFO(mlog, "Loading shader programs...");

    beginCompileShaders(2);

    compileShadersFromFileWithProgressDialog(
                simpleGeometryShader,
                "src/glsl/simple_coloured_geometry.fx.glsl");
    compileShadersFromFileWithProgressDialog(
                colourbarShader,
                "src/glsl/colourbar.fx.glsl");

    endCompileShaders();
}


void MTransferFunction::setMinimumValue(float value)
{
    minimumValueProp = value;
}


void MTransferFunction::setMaximumValue(float value)
{
    maximumValueProp = value;
}


void MTransferFunction::setValueSignificantDigits(int significantDigits)
{
    valueSignificantDigitsProp = significantDigits;
    minimumValueProp.setSignificantDigits(significantDigits);
    maximumValueProp.setSignificantDigits(significantDigits);
    valueStepProp.setSignificantDigits(significantDigits);
}


void MTransferFunction::setValueStep(float step)
{
    valueStepProp = step;
    minimumValueProp.setStep(step);
    maximumValueProp.setStep(step);
}


void MTransferFunction::setPosition(QRectF position)
{
    positionProp = position;
}


void MTransferFunction::setNumTicks(int num)
{
    maxNumTicksProp = num;
}


void MTransferFunction::setNumLabels(int num)
{
    maxNumLabelsProp = num;
}


QString MTransferFunction::transferFunctionName()
{
    return getName();
}


bool MTransferFunction::loadMissingTransferFunction(
        const QString& tfName, QString tfActorType, QString callerDescriptor,
        const QString& callerName, QSettings *settings)
{
    // Append space to caller descriptor if missing.
    if (!callerDescriptor.isEmpty() && !callerDescriptor.endsWith(" "))
    {
        callerDescriptor.append(" ");
    }

    QMessageBox msgBox;
    msgBox.setIcon(QMessageBox::Warning);
    msgBox.setWindowTitle(callerName);
    msgBox.setText(QString("%1'%2' requires a transfer function "
                           "'%3' that does not exist.\n"
                           "Would you like to load the transfer function "
                           "from file?")
                   .arg(callerDescriptor, callerName, tfName));
    msgBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    msgBox.button(QMessageBox::Yes)->setText("Load transfer function");
    msgBox.button(QMessageBox::No)->setText("Discard dependency");
    msgBox.exec();
    if (msgBox.clickedButton() == msgBox.button(QMessageBox::Yes))
    {
        MSystemManagerAndControl *sysMC =
                MSystemManagerAndControl::getInstance();
        // Create default actor to get name of actor factory.
        sysMC->getMainWindow()->getSceneManagementDialog()
                ->loadRequiredActorFromFile(std::move(tfActorType),
                                            tfName,
                                            settings->fileName());
    }
    else
    {
        return false;
    }
    return true;
}


int
MTransferFunction::checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                               float clipX, float clipY)
{
    if (isDragging) return currentDragHandleID;
    QPointF mousePos = QPointF(clipX, clipY);
    QRectF pos = positionProp;

    // Invert height for correct intersection check.
    pos.setHeight(-pos.height());

    const float margin = 0.01f;
    const float halfMargin = margin / 2.0f;

    int resizeDir = 0;

    QRectF left;
    left.setRect(pos.left() - halfMargin, pos.top(), margin, pos.height());

    QRectF right;
    right.setRect(pos.right() - halfMargin, pos.top(), margin, pos.height());

    QRectF top;
    top.setRect(pos.left(), pos.top() - halfMargin, pos.width(), margin);

    QRectF bottom;
    bottom.setRect(pos.left(), pos.bottom() - halfMargin, pos.width(), margin);

    if (left.contains(mousePos))
    {
        resizeDir |= 2;
    }
    if (right.contains(mousePos))
    {
        resizeDir |= 4;
    }
    if (top.contains(mousePos))
    {
        resizeDir |= 8;
    }
    if (bottom.contains(mousePos))
    {
        resizeDir |= 16;
    }

    if (resizeDir == 0 && pos.contains(mousePos))
    {
        return 1;
    }
    else if (resizeDir != 0)
    {
        return resizeDir;
    }

    return -1;
}


void MTransferFunction::dragEvent(MSceneViewGLWidget *sceneView, int handleID,
                                  float clipX, float clipY)
{
    QRectF pos = positionProp;

    const qreal minDim = 0.01;

    // Start dragging
    if (!isDragging && handleID != 0)
    {
        isDragging = true;
        currentDragHandleID = handleID;

        // Move tf window
        if (handleID == 1)
        {
            mouseDragStartX = clipX;
            mouseDragStartY = clipY;
            posDragStartX = pos.x();
            posDragStartY = pos.y();
        }
        else // Resize
        {
            mouseDragStartX = clipX;
            mouseDragStartY = clipY;
            leftDragStart = pos.left();
            topDragStart = pos.top();
            rightDragStart = pos.right();
            bottomDragStart = pos.top() - pos.height();
        }
    }

    // Drag in progress
    if (isDragging)
    {
        float deltaX = clipX - mouseDragStartX;
        float deltaY = clipY - mouseDragStartY;
        if (handleID == 1)
        {

            QRectF p = QRectF(posDragStartX + deltaX, posDragStartY + deltaY,
                               pos.width(), pos.height());

            positionProp.setUndoableValue(p, dragEventID);
        }
        else
        {
            // Invert height for correct clip coordinates.
            pos.setHeight(-pos.height());

            if (handleID & 2) // left
            {
                qreal left = leftDragStart + deltaX;
                left = std::min(pos.right() - minDim, left);
                pos.setLeft(left);
            }
            if (handleID & 4) // right
            {
                qreal right = rightDragStart + deltaX;
                right = std::max(pos.left() + minDim, right);
                pos.setRight(right);
            }
            if (handleID & 8) // top
            {
                qreal top = topDragStart + deltaY;
                top = std::max(pos.bottom() + minDim, top);
                pos.setTop(top);
            }
            if (handleID & 16) // bottom
            {
                qreal bottom = bottomDragStart + deltaY;
                bottom = std::min(pos.top() - minDim, bottom);
                pos.setBottom(bottom);
            }

            pos.setHeight(-pos.height());

            positionProp.setUndoableValue(pos, dragEventID);
        }
    }
}


void
MTransferFunction::releaseEvent(MSceneViewGLWidget *sceneView, int handleID)
{
    isDragging = false;
    currentDragHandleID = 0;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MTransferFunction::initializeActorResources()
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    textureUnit = assignTextureUnit();

    generateTransferTexture();

    // Load shader programs.
    bool loadShaders = false;

    loadShaders |= glRM->generateEffectProgram("transfer_colourbar",
                                               colourbarShader);
    loadShaders |= glRM->generateEffectProgram("transfer_geom",
                                               simpleGeometryShader);

    if (loadShaders) reloadShaderEffects();

    generateBarGeometry();
}

} // namespace Met3D
