/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MBOOLPROPERTY_H
#define MET_3D_MBOOLPROPERTY_H

// standard library imports

// related third party imports

// local application imports
#include "mproperty.h"

// forward declarations
class QCheckBox;

namespace Met3D
{

/**
 * An implementation of the @c MValueProperty for bool types.
 */
class MBoolProperty : public MValueProperty<bool>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<bool>::operator bool;
    using MValueProperty<bool>::operator const bool &;
    using MValueProperty<bool>::operator=;

    MBoolProperty();
    MBoolProperty(const QString &name, bool defaultValue);

    MBoolProperty(const MBoolProperty &other);

    /**
     * Sets the label shown when the property is set to true.
     * @param text The new label text.
     */
    void setTrueLabel(const QString &text);

    /**
     * Sets the label shown when the property is set to false.
     * @param text The new label text.
     */
    void setFalseLabel(const QString &text);

    /**
     * Sets the label shown when the property is set to true and false.
     * @param text The new label text.
     */
    void setLabel(const QString &text);

    /**
     * @return The label shown in the properties current state.
     */
    const QString &getLabel() const;

    MBoolProperty &operator=(const MBoolProperty &other);

protected:
    /**
     * Declaration of editor class, so
     * that it is no longer visible to the
     * outside.
     */
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<bool>::Data
    {
        /**
        * The text shown, when the property is true.
        */
        QString trueText = "Enabled";

        /**
        * The text shown, when the property is false.
        */
        QString falseText = "Disabled";


        Data(const QString &name, bool defaultValue)
                : MValueProperty<bool>::Data(name, defaultValue)
        {}
    };
};

/**
 * The editor widget of a bool property.
 */
class MBoolProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MBoolProperty *property);

    ~Editor() override;

    void updateSettings() override;

    void updateValue() override;

public slots:

    void onStateChanged(int state);

protected:
    QCheckBox *box;
};


} // Met3D

#endif //MET_3D_MBOOLPROPERTY_H
