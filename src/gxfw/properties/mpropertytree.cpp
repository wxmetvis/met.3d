/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mpropertytree.h"

// standard library imports
#include <algorithm>

// related third party imports
#include <QPainter>
#include <QMouseEvent>
#include <QMenu>
#include <QScrollArea>
#include <QVBoxLayout>
#include <QMap>
#include <QLabel>
#include <QLineEdit>
#include <QToolButton>
#include <QScrollBar>


// local application imports
#include "gxfw/msystemcontrol.h"

namespace Met3D
{

const int LEVEL_INDENTATION_PX = 10;

MPropertyTree::Item::Item(MPropertyTree *tree, MPropertyTree::Item *parent,
                          MProperty *property, int depth)
        : QWidget(nullptr),
          parentLayout(nullptr),
          itemLayout(nullptr),
          itemWidget(nullptr),
          expandButton(nullptr),
          propertyLabel(nullptr),
          editor(nullptr),
          treeDepth(depth),
          property(property),
          fitsFilter(true),
          treeWidget(tree),
          parent(parent),
          enabled(false),
          currentSplit(-1),
          minRequiredSplit(0),
          maxRequiredSplit(-1)
{
    assert(property != nullptr);

    setContentsMargins(0, 0, 0, 0);

    connect(property, &MProperty::propertyAdded,
            this, &Item::onSubPropertyAdded);
    connect(property, &MProperty::propertyInserted,
            this, &Item::onSubPropertyInserted);
    connect(property, &MProperty::propertyRemoved,
            this, &Item::onSubPropertyRemoved);
    connect(property, &MProperty::toggledEnabled,
            this, &Item::onToggleEnabled);
    connect(property, &MProperty::tooltipChanged,
            this, &Item::onTooltipChanged);
    connect(property, &MProperty::nameChanged,
            this, &Item::onNameChanged);
    connect(property, &MProperty::backgroundColorChanged,
            this, &Item::onColorChanged);
    connect(property, &MProperty::toggleExpand,
            this, &Item::onToggleExpand);
    connect(property, &MProperty::toggleHidden,
            this, &Item::onToggleHidden);

    if (parent != nullptr)
    {
        connect(this, &Item::requiredSplitChanged,
                parent, &Item::onRequiredSplitChanged);
    }

    parentLayout = new QVBoxLayout();
    parentLayout->setMargin(0);
    parentLayout->setSpacing(0);
    parentLayout->setSizeConstraint(QLayout::SetMinimumSize);

    setLayout(parentLayout);
    setFocusPolicy(Qt::ClickFocus);

    itemWidget = new QWidget(this);
    itemWidget->setContentsMargins(0, 0, 0, 0);
    itemWidget->setToolTip(property->getTooltip());
    itemWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    itemWidget->setFixedHeight(treeWidget->rowHeight + margin * 2);

    itemLayout = new QHBoxLayout();
    itemLayout->setMargin(0);
    itemLayout->setSpacing(0);
    itemLayout->addSpacing(treeDepth * LEVEL_INDENTATION_PX);

    itemWidget->setLayout(itemLayout);

    parentLayout->addWidget(itemWidget);

    propertyWidget = new QFrame(this);

    // Remember color, so that we can set the editor color later
    // back to this one.
    QColor originalBase = palette().color(QPalette::Base);
    
    setColors(treeWidget->levelColors, treeWidget->colorGenMethod, treeWidget->colorMethodStrength);

    // Set margins. Left and bottom margin is doubled, so that we can draw
    // a drop shadow around the widget at these edges.
    propertyWidget->setContentsMargins(margin * 2, margin, margin, margin * 2);
    propertyWidget->setToolTip(property->getTooltip());
    propertyWidget->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    propertyWidget->setFixedHeight(treeWidget->rowHeight + margin * 3);

    propertyLayout = new QHBoxLayout();
    propertyLayout->setMargin(0);
    propertyLayout->setSpacing(0);

    propertyWidget->setLayout(propertyLayout);

    itemLayout->addWidget(propertyWidget);

    expandButton = new MTreeExpandButton(propertyWidget, 2);
    updateExpandButtonVisibility();
    expandButton->setAutoFillBackground(false);
    expandButton->setExpanded(false);

    connect(expandButton, &MTreeExpandButton::pressed,
            this, &Item::onExpandButtonPressed);

    connect(expandButton, &MTreeExpandButton::alternativePressed,
            this, &Item::onExpandButtonAltPressed);

    propertyLayout->addWidget(expandButton);

    propertyLabel = new QLabel(property->getName());
    propertyLayout->addWidget(propertyLabel);

    spacer = new QSpacerItem(10, 10, QSizePolicy::Fixed, QSizePolicy::Ignored);
    propertyLayout->addSpacerItem(spacer);

    editor = property->createEditorWidget(propertyWidget);

    if (editor != nullptr)
    {
        propertyLayout->addWidget(editor);
        QPalette editorPalette;
        editorPalette.setColor(QPalette::Base, originalBase);
        editor->setPalette(editorPalette);
        editor->setExpanding(true);
        editor->setFixedHeight(treeWidget->rowHeight);
        editor->updateSize(treeWidget->rowHeight);

        this->setToolTip(property->getTooltip());
        editor->setToolTip(property->getTooltip());
    }

    minRequiredSplit = calcMinRequiredSplit();
    maxRequiredSplit = minRequiredSplit;

    for (MProperty *prop : property->getSubProperties())
    {
        onSubPropertyAdded(prop);
    }

    updateVisibility();
    onToggleEnabled();
}


MPropertyTree::Item::~Item()
{
    if (treeWidget != nullptr)
    {
        if (treeWidget->selected == this)
        {
            treeWidget->selectPropertyItem(nullptr);
        }
    }
    subItems.clear();
}

void MPropertyTree::Item::expand()
{
    if (subItems.empty()) return;

    expandButton->setExpanded(true);

    for (auto item : subItems)
    {
        item->updateVisibility();
    }

    updateMaxRequiredSplit(calcMaxRequiredSplit());
}


void MPropertyTree::Item::collapse()
{
    if (subItems.empty()) return;

    expandButton->setExpanded(false);

    for (auto item : subItems)
    {
        item->updateVisibility();
    }

    updateMaxRequiredSplit(minRequiredSplit, true);

    // Check if the selected property is part of this sub-tree
    if (treeWidget->selected != nullptr && treeWidget->selected != this)
    {
        MPropertyTree::Item *up = treeWidget->selected->parent;
        while (up != nullptr)
        {
            if (up == this)
            {
                treeWidget->selectPropertyItem(nullptr);
                break;
            }
            up = up->parent;
        }
    }
}


void MPropertyTree::Item::expandAll()
{
    if (subItems.empty()) return;

    expandButton->setExpanded(true);

    for (auto item : subItems)
    {
        item->updateVisibility();
        item->expandAll();
    }

    updateMaxRequiredSplit(calcMaxRequiredSplit());
}


void MPropertyTree::Item::collapseAll()
{
    if (subItems.empty()) return;

    expandButton->setExpanded(false);

    for (auto item : subItems)
    {
        item->updateVisibility();
        item->collapseAll();
    }

    updateMaxRequiredSplit(minRequiredSplit, true);
}


bool MPropertyTree::Item::filterProperties(const TreeFilter &filter)
{
    // This is a special case where this item will be deleted the next
    // Qt event loop execution.
    if (property == nullptr) return false;

    bool fitsName = property->getName().contains(filter.nameFilter, Qt::CaseInsensitive);
    bool fitsItem = filter.itemFilter == nullptr || filter.itemFilter == this;
    bool fitsSubItem = false;

    TreeFilter newFilter = filter;

    if (fitsName)
    {
        newFilter.nameFilter = "";
    }
    if (fitsItem)
    {
        newFilter.itemFilter = nullptr;
    }

    for (auto sub : subItems)
    {
        fitsSubItem |= sub->filterProperties(newFilter);
    }

    fitsFilter = (fitsName && fitsItem) || fitsSubItem;

    subPropertyFitsFilter(fitsSubItem, filter);

    updateMaxRequiredSplit(calcMaxRequiredSplit(), true);

    return fitsFilter;
}


void MPropertyTree::Item::setSplit(int split)
{
    // If we have no editor, we have no split.
    if (editor == nullptr)
    {
        currentSplit = split;
        for (Item *sub : subItems)
        {
            sub->setSplit(split);
        }
        return;
    }

    QRect endGeo = propertyLabel->geometry();
    QSize labelSizeHint = propertyLabel->sizeHint();
    endGeo.setWidth(labelSizeHint.width());
    QRect spacerGeo = spacer->geometry();

    propertyLabel->setGeometry(endGeo);

    QSize buttonSize = expandButton->size();
    QSize labelSize = propertyLabel->sizeHint();
    int maxLabelWidth = propertyLabel->maximumWidth();
    int leftMargin = propertyWidget->contentsMargins().left();

    int width = leftMargin + std::min(labelSize.width(), maxLabelWidth) + buttonSize.width()
            + treeDepth * LEVEL_INDENTATION_PX;

    int splitterWidth = split - width + 10;

    if (spacerGeo.width() != splitterWidth || currentSplit != split)
    {
        spacer->changeSize(splitterWidth, 10, QSizePolicy::Maximum,
                           QSizePolicy::Ignored);
        propertyLayout->invalidate();
    }

    currentSplit = split;

    for (Item *sub : subItems)
    {
        sub->setSplit(split);
    }
}


void MPropertyTree::Item::setColors(const QVector<QColor> &colors,
                                    const MPropertyTree::ColorMethod &genMethod,
                                    int strength)
{
    QPalette p = palette();
    QColor c = p.color(QPalette::Base);

    if (colors.length() > treeDepth)
    {
        c = colors[treeDepth];
    }
    else if (!colors.isEmpty())
    {
        c = colors.last();

        switch (genMethod)
        {
        case DARKEN:
            c = c.darker(100 + (treeDepth - colors.length() + 1) * strength);
            break;
        case LIGHTEN:
            c = c.lighter(100 + (treeDepth - colors.length() + 1) * strength);
            break;
        case KEEP:
            break;
        }
    }

    p.setColor(QPalette::Base, c);
    propertyWidget->setPalette(p);

    for (Item *sub : subItems)
    {
        sub->setColors(colors, genMethod, strength);
    }
}


void MPropertyTree::Item::toggleSelection(bool select)
{
    if (select)
    {
        propertyWidget->setBackgroundRole(QPalette::Highlight);
    }
    else
    {
        propertyWidget->setBackgroundRole(QPalette::Base);
    }
}


void MPropertyTree::Item::invalidate()
{
    property->disconnect(this);
    property = nullptr;
}


void MPropertyTree::Item::setMaxLabelWidth(int width)
{

    if (editor != nullptr)
    {
        QSize buttonSize = expandButton->size();
        int leftMargin = propertyWidget->contentsMargins().left();

        int localWidth = width - leftMargin - buttonSize.width() - treeDepth * LEVEL_INDENTATION_PX;
        localWidth = std::max(localWidth, 0);

        if (localWidth != propertyLabel->maximumWidth())
        {
            propertyLabel->setMaximumWidth(localWidth);
            propertyLabel->setText(propertyLabel->fontMetrics().elidedText(property->getName(), Qt::ElideRight, localWidth));

            minRequiredSplit = calcMinRequiredSplit();
            updateMaxRequiredSplit(calcMaxRequiredSplit(), true);
        }
    }

    for (auto subItem : subItems)
    {
        subItem->setMaxLabelWidth(width);
    }
}


void MPropertyTree::Item::onExpandButtonPressed()
{
    bool expanded = expandButton->isExpanded();

    if (expanded)
    {
        expand();
    }
    else
    {
        collapse();
    }
}


void MPropertyTree::Item::onExpandButtonAltPressed()
{
    bool expanded = expandButton->isExpanded();

    if (expanded)
    {
        expandAll();
    }
    else
    {
        collapseAll();
    }
}


void MPropertyTree::Item::onSubPropertyAdded(Met3D::MProperty *subProperty)
{
    if (subItems.contains(subProperty)) return;

    auto item = new MPropertyTree::Item(treeWidget, this, subProperty, treeDepth + 1);
    item->setMaxLabelWidth(treeWidget->getMinColumnWidth());

    subItems.insert(subProperty, item);

    parentLayout->addWidget(item);

    if (!expandButton->isExpanded())
    {
        item->updateVisibility();
    }

    updateExpandButtonVisibility();

    filterPropertiesPartial(treeWidget->getFilter());

    item->onToggleEnabled();

    item->setSplit(currentSplit);
}


void MPropertyTree::Item::onSubPropertyInserted(int index,
                                                Met3D::MProperty *subProperty)
{
    if (subItems.contains(subProperty)) return;

    updateExpandButtonVisibility();

    auto item = new MPropertyTree::Item(treeWidget, this, subProperty, treeDepth + 1);
    item->setMaxLabelWidth(treeWidget->getMinColumnWidth());

    subItems.insert(subProperty, item);

    parentLayout->insertWidget(index + 1, item);

    if (!expandButton->isExpanded())
    {
        item->updateVisibility();
    }

    filterPropertiesPartial(treeWidget->getFilter());

    item->onToggleEnabled();

    item->setSplit(currentSplit);
}


void MPropertyTree::Item::onSubPropertyRemoved(Met3D::MProperty *subProperty)
{
    if (!subItems.contains(subProperty)) return;

    MPropertyTree::Item *item = subItems[subProperty];
    parentLayout->removeWidget(item);

    subItems.remove(subProperty);

    int newSplit = calcMaxRequiredSplit();
    if (newSplit < maxRequiredSplit)
    {
        updateMaxRequiredSplit(newSplit, true);
    }

    item->invalidate();
    item->deleteLater();

    updateExpandButtonVisibility();
}


void MPropertyTree::Item::onToggleEnabled()
{
    enabled = property->isEnabled();

    if (parent != nullptr)
    {
        enabled = enabled && parent->enabled;
    }

    if (editor != nullptr)
    {
        editor->setEnabled(enabled);
    }

    // Refresh sub items.
    for (auto sub : subItems)
    {
        sub->onToggleEnabled();
    }
}


void MPropertyTree::Item::onTooltipChanged()
{
    setToolTip(property->getTooltip());

    if (editor != nullptr)
    {
        editor->setToolTip(property->getTooltip());
    }
}


void MPropertyTree::Item::onNameChanged()
{
    propertyLabel->setText(propertyLabel->fontMetrics().elidedText(property->getName(),
                                                                   Qt::ElideRight,
                                                                   propertyLabel->maximumWidth()));

    minRequiredSplit = calcMinRequiredSplit();
    updateMaxRequiredSplit(calcMaxRequiredSplit(), true);
    setSplit(currentSplit);

    filterPropertiesPartial(treeWidget->getFilter());
}


void MPropertyTree::Item::onColorChanged()
{
    repaint();
}


void MPropertyTree::Item::onToggleExpand(bool expanded, MPropertyTree *tree)
{
    if (tree != nullptr && tree != treeWidget) return;
    if (expandButton->isExpanded() == expanded) return;

    expandButton->setExpanded(expanded);
}


void MPropertyTree::Item::onToggleHidden()
{
    updateVisibility();
    emit requiredSplitChanged(-1, true);
}


void MPropertyTree::Item::onRequiredSplitChanged(int split, bool refreshAll)
{
    if (split > maxRequiredSplit)
    {
        updateMaxRequiredSplit(split);
    }
    else if (refreshAll)
    {
        updateMaxRequiredSplit(calcMaxRequiredSplit(), true);
    }
}


void MPropertyTree::Item::updateVisibility()
{
    // This item is invalid and will be deleted in the next Qt event loop execution.
    if (property == nullptr) return;

    bool treeExpanded = true;

    if (parent != nullptr)
    {
        treeExpanded = parent->expandButton->isExpanded();
    }

    setVisible(treeExpanded && fitsFilter && !property->isHidden());

    if (treeWidget->selected == this && isHidden())
    {
        treeWidget->selectPropertyItem(nullptr);
    }
}


void MPropertyTree::Item::updateExpandButtonVisibility()
{
    for (MPropertyTree::Item *sub : subItems)
    {
        if (!sub->property->isHidden())
        {
            expandButton->setShowArrow(true);
            return;
        }
    }
    expandButton->setShowArrow(false);
}


void MPropertyTree::Item::filterPropertiesPartial(const TreeFilter &filter)
{
    bool fitsSubItem = filterProperties(filter);
    fitsFilter = fitsFilter || fitsSubItem;

    Item *p = this;
    while (p != nullptr)
    {
        p->subPropertyFitsFilter(fitsSubItem, filter);

        fitsSubItem = p->fitsFilter;
        p = p->parent;
    }
}


void MPropertyTree::Item::subPropertyFitsFilter(bool subFitsFilter, const TreeFilter &filter)
{
    if (subFitsFilter && !filter.nameFilter.isEmpty())
    {
        expand();
    }
    updateVisibility();
}


MPropertyTree::Item *MPropertyTree::Item::findNext(bool ignoreSubTree)
{
    if (expandButton->isExpanded() && !subItems.isEmpty() && !ignoreSubTree)
    {
        for (MProperty *prop : property->getSubProperties())
        {
            if (subItems[prop]->isHidden()) continue;
            return subItems[prop];
        }
    }
    if (parent != nullptr)
    {
        const QList<MProperty*> &neighbours = parent->property->getSubProperties();
        int index = neighbours.indexOf(property);

        for (int i = index + 1; i < neighbours.length(); i++)
        {
            MPropertyTree::Item *item = parent->subItems[neighbours[i]];
            if (item->isHidden()) continue;
            return item;
        }

        return parent->findNext(true);
    }
    else if (parent == nullptr)
    {
        // This means this is a top-level item.
        const QVector<MPropertyTree::Item *> &topLevelItems = treeWidget->getTopLevelItems();

        int index = topLevelItems.indexOf(this);
        for (int i = index + 1; i < topLevelItems.length(); i++)
        {
            if (topLevelItems[i]->isHidden()) continue;
            return topLevelItems[i];
        }
    }
    return nullptr;
}


MPropertyTree::Item *MPropertyTree::Item::findPrevious()
{
    if (parent == nullptr)
    {
        // This means this is a top-level item.
        const QVector<MPropertyTree::Item *> &topLevelItems = treeWidget->getTopLevelItems();

        int index = topLevelItems.indexOf(this);
        for (int i = index - 1; i >= 0; i--)
        {
            if (topLevelItems[i]->isHidden()) continue;
            if (topLevelItems[i]->expandButton->isExpanded() && !topLevelItems[i]->subItems.empty())
            {
                return topLevelItems[i]->findLast();
            }
            return topLevelItems[i];
        }

        return nullptr;
    }
    else if (parent->property->getSubProperties().first() == property)
    {
        return parent;
    }
    else
    {
        const QList<MProperty*> &neighbours = parent->property->getSubProperties();

        int index = neighbours.indexOf(property);

        for (int i = index - 1; i >= 0; i--)
        {
            MPropertyTree::Item *item = parent->subItems[neighbours[i]];
            if (item->isHidden()) continue;
            if (item->expandButton->isExpanded() && !item->subItems.empty())
            {
                return item->findLast();
            }
            return item;
        }

        return parent;
    }
}


MPropertyTree::Item *MPropertyTree::Item::findLast()
{
    if (property->subPropertyCount() == 0) return nullptr;
    const QList<MProperty *> &subProps = property->getSubProperties();
    MProperty *last = nullptr;
    MPropertyTree::Item *lastItem = nullptr;

    for (int i = subProps.length() - 1; i >= 0; i--)
    {
        last = subProps[i];
        lastItem = subItems[last];

        if (lastItem->isHidden()) continue;
        if (lastItem->expandButton->isExpanded() && last->subPropertyCount() > 0)
        {
            return lastItem->findLast();
        }
        return lastItem;
    }

    return nullptr;
}


int MPropertyTree::Item::calcMaxRequiredSplit() const
{
    int width = minRequiredSplit;

    if (expandButton->isExpanded())
    {
        for (Item *sub : subItems)
        {
            if (sub->isHidden()) continue;
            width = std::max(width, sub->getRequiredSplit());
        }
    }

    return width;
}


int MPropertyTree::Item::calcMinRequiredSplit() const
{
    int width = 0;

    // If we have no editor, we want to ignore label width.
    if (editor != nullptr)
    {
        QSize buttonSize = expandButton->size();
        QSize labelSize = propertyLabel->sizeHint();
        int maxLabelWidth = propertyLabel->maximumWidth();
        int leftMargin = propertyWidget->contentsMargins().left();

        width = leftMargin + std::min(labelSize.width(), maxLabelWidth) + buttonSize.width()
                + treeDepth * LEVEL_INDENTATION_PX;
    }

    return width;
}


void MPropertyTree::Item::updateMaxRequiredSplit(int split, bool refreshAll)
{
    if (maxRequiredSplit >= split && !refreshAll) return;
    bool needFullRefresh = split < maxRequiredSplit;
    maxRequiredSplit = split;
    if (!isHidden())
    {
        emit requiredSplitChanged(maxRequiredSplit, needFullRefresh);
    }
}


void MPropertyTree::Item::paintEvent(QPaintEvent *event)
{
    // This item is invalid and will be deleted in the next Qt event loop execution.
    if (property == nullptr) return;

    // Get the geometry of the widget, including margins.
    QRect geo = propertyWidget->geometry();

    // Get the shadow margins of the widget.
    // These are the margins from the widgets edge into which a drop shadow
    // is drawn.
    QMargins shadowMargin{margin, 0, 0, margin};

    // Geometry of the widget without its shadow margin.
    // This is the real widget geometry with the shadow margins removed.
    QRect innerGeo = geo.marginsRemoved(shadowMargin);

    // The rectangle to draw the background to.
    // Left and bottom bounds are the inner geometry, so that we can draw
    // the drop shadow into the margin. Top and right have no shadow, so we draw
    // the background over it.
    QRect bgRect = QRect(innerGeo.left(), innerGeo.top(), innerGeo.right(), innerGeo.bottom());

    // Extend Background all the way to the left for top level widgets.
    if (treeDepth == 0)
    {
        innerGeo.setLeft(innerGeo.left() - margin * 2);
        bgRect.setLeft(bgRect.left() - margin * 2);
    }

    QPainter painter(this);

    // Manually draw background.
    QBrush bgColor = propertyWidget->palette()
                                   .brush(propertyWidget->backgroundRole());

    // If a color is set use it as the background color.
    QColor propColor = property->getBackgroundColor();
    if (propColor.alpha() != 0)
    {
        bgColor.setColor(propColor);
    }

    // Draw background.
    painter.fillRect(bgRect, bgColor);

    // Draw drop shadows to the left and bottom edges.
    painter.setPen(Qt::NoPen);

    // Shadow gradient.
    QLinearGradient gradient;
    gradient.setColorAt(1.0, QColor(120, 120, 120, 0));
    gradient.setColorAt(0.0, QColor(120, 120, 120, 255));

    // Left shadow
    gradient.setStart(bgRect.left(), 0.0f);
    gradient.setFinalStop(geo.left(), 0.0f);
    painter.setBrush(QBrush(gradient));
    painter.drawRoundedRect(QRectF(geo.left(), geo.top(),
                                   shadowMargin.left(), bgRect.height()),
                            0.0, 0.0);

    // Bottom shadow
    gradient.setStart(0.0f, bgRect.bottom());
    gradient.setFinalStop(0.0f, geo.bottom());
    painter.setBrush(QBrush(gradient));
    painter.drawRoundedRect(QRectF(bgRect.left(), bgRect.bottom(),
                                   bgRect.width(), shadowMargin.bottom()),
                            0.0, 0.0);

    gradient.setColorAt(1.0, QColor(120, 120, 120, 0));
    gradient.setColorAt(0.0, QColor(120, 120, 120, 125));

    // Bottom left corner shadow
    gradient.setStart(bgRect.left(), bgRect.bottom());
    gradient.setFinalStop(geo.left(), geo.bottom());
    painter.setBrush(QBrush(gradient));
    painter.drawRoundedRect(QRectF(QPointF(bgRect.left(), bgRect.bottom()),
                                   QPointF(geo.left(), geo.bottom())), margin,
                            margin);

    // Draw widget.
    QWidget::paintEvent(event);

    // Draw top shadow as line.
    painter.setPen(QColor(120, 120, 120, 125));
    painter.drawLine(bgRect.x(), bgRect.top(), bgRect.right(), bgRect.top());

    // Draw separator lines.
    if (treeDepth > 0)
    {
        painter.setPen(palette().dark().color());

        // Horizontal line
        painter.drawLine(bgRect.x(), bgRect.bottom(), bgRect.right(),
                         bgRect.bottom());

        painter.drawLine(bgRect.x(), bgRect.bottom(), bgRect.x(), bgRect.top());
        painter.drawLine(bgRect.right(), bgRect.bottom(), bgRect.right(),
                         bgRect.top());
    }
}


void MPropertyTree::Item::mouseDoubleClickEvent(QMouseEvent *event)
{
    QWidget::mouseDoubleClickEvent(event);

    if (propertyWidget->geometry().contains(event->pos()))
    {
        event->accept();
        if (expandButton->isExpanded())
        {
            collapse();
        }
        else
        {
            expand();
        }
    }
}


void MPropertyTree::Item::mousePressEvent(QMouseEvent *event)
{
    QWidget::mousePressEvent(event);

    if (event->button() != Qt::LeftButton) return;
    if (!propertyWidget->geometry().contains(event->pos())) return;

    event->accept();

    if (treeWidget->selected == this)
    {
        treeWidget->selectPropertyItem(nullptr);
    }
    else
    {
        treeWidget->selectPropertyItem(this);
    }
}


void MPropertyTree::Item::contextMenuEvent(QContextMenuEvent *event)
{
    QWidget::contextMenuEvent(event);
    // This item is invalid and will be deleted in the next Qt event loop execution.
    if (property == nullptr) return;
    if (!propertyWidget->geometry().contains(event->pos())) return;

    QVector<QAction*> actions = property->getContextMenu();

    event->accept();

    auto *menu = new QMenu(property->getName(), this);

    if (!actions.empty())
    {
        for (QAction *action : actions)
        {
            menu->addAction(action);
        }
    }

    QAction *collapseAll;
    QAction *expandAll;

    collapseAll = new QAction("Collapse all sub-items");
    expandAll = new QAction("Expand all sub-items");

    connect(collapseAll, &QAction::triggered, [=]()
    {
        this->property->collapseAll(this->treeWidget);
    });

    connect(expandAll, &QAction::triggered, [=]()
    {
        this->property->expandAll(this->treeWidget);
    });

    menu->addAction(expandAll);
    menu->addAction(collapseAll);

    collapseAll->setEnabled(property->subPropertyCount() > 0);
    expandAll->setEnabled(property->subPropertyCount() > 0);

    menu->exec(event->globalPos());

    delete menu;
    delete expandAll;
    delete collapseAll;
}


void MPropertyTree::Item::keyPressEvent(QKeyEvent *event)
{
    if (treeWidget->selected != this) return;

    MPropertyTree::Item *next;
    const QVector<MPropertyTree::Item *> &topLevelItems = treeWidget
            ->getTopLevelItems();

    switch(event->key())
    {
    case Qt::Key_Down:
        event->accept();
        if (event->modifiers() & Qt::ShiftModifier && parent != nullptr)
        {
            next = parent->findLast();
            if (next != nullptr)
                treeWidget->selectPropertyItem(next);
        }
        else
        {
            next = findNext();
            if (next != nullptr)
                treeWidget->selectPropertyItem(next);
        }
        break;
    case Qt::Key_Up:
        event->accept();
        if (event->modifiers() & Qt::ShiftModifier && parent != nullptr)
        {
            treeWidget->selectPropertyItem(parent);
        }
        else
        {
            next = findPrevious();
            if (next != nullptr)
                treeWidget->selectPropertyItem(next);
        }
        break;
    case Qt::Key_Right:
        event->accept();
        expand();
        break;
    case Qt::Key_Left:
        event->accept();
        collapse();
        break;
    case Qt::Key_Home:
        event->accept();
        // Move to the first property
        for (int i = 0; i < topLevelItems.length(); i++)
        {
            if (topLevelItems[i]->isHidden()) continue;
            treeWidget->selectPropertyItem(topLevelItems[i]);
            break;
        }
        break;
    case Qt::Key_End:
        event->accept();
        // Move to the last property
        for (int i = topLevelItems.length() - 1; i >= 0; i--)
        {
            if (topLevelItems[i]->isHidden()) continue;
            next = topLevelItems[i]->findLast();
            if (next != nullptr)
            {
                treeWidget->selectPropertyItem(next);
            }
            else
            {
                treeWidget->selectPropertyItem(topLevelItems[i]);
            }
            break;
        }
        break;
    }
}


MPropertyTree::ViewLayout::ViewLayout(QWidget *parent)
        : QVBoxLayout(parent),
          currentSplit(0)
{

}


void MPropertyTree::ViewLayout::setGeometry(const QRect &rect)
{
    int split = -1;
    for (MPropertyTree::Item *item : items)
    {
        if (item->isHidden()) continue;
        split = std::max(item->getRequiredSplit(), split);
    }

    if (split != currentSplit)
    {
        currentSplit = split;

        for (MPropertyTree::Item *item : items)
        {
            item->setSplit(currentSplit);
        }
    }

    QBoxLayout::setGeometry(rect);
}


void MPropertyTree::ViewLayout::addTreeItem(MPropertyTree::Item *item)
{
    QVBoxLayout::addWidget(item);
    items.append(item);
    item->setSplit(currentSplit);
}


void MPropertyTree::ViewLayout::insertTreeItem(int index,
                                               MPropertyTree::Item *item)
{
    QVBoxLayout::insertWidget(index, item);
    if (index < 0)
    {
        items.append(item);
        return;
    }
    items.insert(index, item);
    item->setSplit(currentSplit);
}


void MPropertyTree::ViewLayout::insertTreeItemAfter(MPropertyTree::Item *before,
                                                    MPropertyTree::Item *item)
{
    int index = items.indexOf(before);
    if (index >= 0)
    {
        index++;
    }
    insertTreeItem(index, item);
}


void MPropertyTree::ViewLayout::removeTreeItem(MPropertyTree::Item *item)
{
    QVBoxLayout::addWidget(item);
    items.removeOne(item);
}


MPropertyTree::PropertyView::PropertyView(QWidget *parent)
        : QFrame(parent),
          layout(nullptr),
          propertyLayout(nullptr)
{
    // Set background to base color
    setBackgroundRole(QPalette::ColorRole::Base);
    setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));

    layout = new QVBoxLayout();
    layout->setSizeConstraint(QLayout::SetMinimumSize);
    layout->setMargin(0);
    layout->setSpacing(0);

    setLayout(layout);

    propertyLayout = new MPropertyTree::ViewLayout();
    propertyLayout->setSizeConstraint(QLayout::SetMinimumSize);
    propertyLayout->setMargin(0);
    propertyLayout->setSpacing(0);

    layout->addLayout(propertyLayout);
    layout->addStretch();
}


void MPropertyTree::PropertyView::addPropertyTreeItem(Item *item) const
{
    propertyLayout->addTreeItem(item);
}


void MPropertyTree::PropertyView::insertPropertyTreeItem(int index,
                                                         MPropertyTree::Item *item) const
{
    propertyLayout->insertTreeItem(index, item);
}


void MPropertyTree::PropertyView::insertPropertyTreeItemAfter(Item *before,
                                                              Item *item) const
{
    propertyLayout->insertTreeItemAfter(before, item);
}


void
MPropertyTree::PropertyView::removePropertyTreeItem(
        MPropertyTree::Item *item) const
{
    if (item->parentWidget() != this) return;
    propertyLayout->removeTreeItem(item);
}


MPropertyTree::MPropertyTree(QWidget *parent)
        : QWidget(parent),
          propertyTreeView(new MPropertyTree::PropertyView(this)),
          colorGenMethod(DARKEN),
          colorMethodStrength(10),
          rowHeight(20),
          selected(nullptr)
{
    layout = new QVBoxLayout();
    layout->setMargin(0);

    setLayout(layout);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);

    header = new QHBoxLayout();

    layout->addLayout(header);

    searchBox = new QLineEdit(this);
    searchBox->setClearButtonEnabled(true);
    searchBox->setPlaceholderText("Search...");

    filterButton = new QToolButton();
    QString icon = QDir(
            QProcessEnvironment::systemEnvironment().value("MET3D_HOME"))
            .absoluteFilePath(
                    "resources/icons/filter.png");
    filterButton
            ->setToolTip("Filter the search results to only include properties "
                         "in the sub-tree of the selected property, if one is selected.");
    filterButton->setIcon(QIcon(icon));
    filterButton->setCheckable(true);

    connect(filterButton, &QToolButton::toggled, [=](bool checked)
    {
        if (checked)
        {
            treeFilter.itemFilter = selected;
        }
        else
        {
            treeFilter.itemFilter = nullptr;
        }

        filterProperties();
    });

    toggleExpandButton = new QToolButton();
    toggleExpandButton->setToolTip("Expand all properties");
    toggleExpandButton->setArrowType(Qt::RightArrow);
    toggleExpandButton->setCheckable(false);

    connect(toggleExpandButton, &QToolButton::clicked, [=](bool checked)
    {
        if (items.isEmpty()) return;

        for (MPropertyTree::Item *item : items)
        {
            if (toggleExpandButton->arrowType() == Qt::RightArrow)
            {
                item->expandAll();
            }
            else
            {
                item->collapseAll();
            }
        }

        if (toggleExpandButton->arrowType() == Qt::RightArrow)
        {
            toggleExpandButton->setToolTip("Collapse all properties");
            toggleExpandButton->setArrowType(Qt::DownArrow);
        }
        else
        {
            toggleExpandButton->setToolTip("Expand all properties");
            toggleExpandButton->setArrowType(Qt::RightArrow);
        }
    });

    // Add all widgets to the header layout.
    header->addWidget(toggleExpandButton);
    header->addWidget(searchBox);
    header->addWidget(filterButton);

    area = new QScrollArea(this);
    area->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    area->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    area->setSizePolicy(
            QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding));
    area->setWidgetResizable(true);
    area->setSizeAdjustPolicy(QScrollArea::SizeAdjustPolicy::AdjustToContents);

    area->setWidget(propertyTreeView);
    layout->addWidget(area);

    levelColors.append(palette().color(QPalette::Base));

    connect(searchBox, &QLineEdit::textChanged, [=](const QString &text)
    {
        treeFilter.nameFilter = text;
        filterProperties();
    });
}


MPropertyTree::~MPropertyTree()
{
    items.clear();
    delete header;
    delete layout;
}


MProperty *MPropertyTree::addProperty(MProperty *property)
{
    if (items.contains(property)) return nullptr;

    auto item = new MPropertyTree::Item(this, nullptr, property);
    item->setMaxLabelWidth(getMinColumnWidth());

    items.insert(property, item);

    propertyTreeView->addPropertyTreeItem(item);

    connect(property, &MProperty::propertyDestroyed, this, &MPropertyTree::onPropertyDestroyed);

    return property;
}


MProperty *MPropertyTree::insertProperty(int index, MProperty *property)
{
    if (property == nullptr) return nullptr;

    MPropertyTree::Item *item;
    if (items.contains(property))
    {
        item = items[property];
        propertyTreeView->removePropertyTreeItem(item);
    }
    else
    {
        item = new MPropertyTree::Item(this, nullptr, property);
        item->setMaxLabelWidth(getMinColumnWidth());
        items.insert(property, item);
    }

    if (index >= items.count())
    {
        index = -1;
    }

    propertyTreeView->insertPropertyTreeItem(index, item);

    connect(property, &MProperty::propertyDestroyed, this, &MPropertyTree::onPropertyDestroyed);

    return property;
}


MProperty *MPropertyTree::insertPropertyAfter(Met3D::MProperty *before,
                                              Met3D::MProperty *property)
{
    if (before == nullptr || property == nullptr) return nullptr;
    if (!items.contains(before)) return nullptr;
    if (before == property) return nullptr;

    MPropertyTree::Item *item;
    if (items.contains(property))
    {
        item = items[property];
        propertyTreeView->removePropertyTreeItem(item);
    }
    else
    {
        item = new MPropertyTree::Item(this, nullptr, property);
        item->setMaxLabelWidth(getMinColumnWidth());
        items.insert(property, item);
    }

    propertyTreeView->insertPropertyTreeItemAfter(items[before], item);

    return property;
}


void MPropertyTree::removeProperty(Met3D::MProperty *property)
{
    if (items.contains(property))
    {
        MPropertyTree::Item *item = items[property];

        propertyTreeView->removePropertyTreeItem(item);

        items.remove(property);

        disconnect(property, &MProperty::propertyDestroyed, this, &MPropertyTree::onPropertyDestroyed);

        item->invalidate();
        item->deleteLater();
    }
}


void MPropertyTree::setBaseLevelColor(const QColor &color)
{
    levelColors.clear();
    levelColors.append(color);

    for (Item *item : items)
    {
        item->setColors(levelColors, colorGenMethod, colorMethodStrength);
    }
}


void MPropertyTree::setLevelColors(const QVector<QColor> &colors)
{
    levelColors = colors;

    for (Item *item : items)
    {
        item->setColors(levelColors, colorGenMethod, colorMethodStrength);
    }
}


void MPropertyTree::setBackgroundColor(const QColor &color)
{
    QPalette p = propertyTreeView->palette();

    p.setColor(QPalette::Base, color);

    propertyTreeView->setPalette(p);
}


void MPropertyTree::setColorGenerationMethod(const ColorMethod &method)
{
    colorGenMethod = method;

    for (Item *item : items)
    {
        item->setColors(levelColors, colorGenMethod, colorMethodStrength);
    }
}


void MPropertyTree::setColorGenerationMethodStrength(int strength)
{
    colorMethodStrength = strength;

    for (Item *item : items)
    {
        item->setColors(levelColors, colorGenMethod, colorMethodStrength);
    }
}


void MPropertyTree::filterProperties()
{
    for (auto item : items)
    {
        item->filterProperties(getFilter());
    }
}


const MPropertyTree::TreeFilter &MPropertyTree::getFilter() const
{
    return treeFilter;
}


const QVector<MPropertyTree::Item *> &MPropertyTree::getTopLevelItems() const
{
    return propertyTreeView->propertyLayout->getItems();
}


int MPropertyTree::getMinColumnWidth() const
{
    return geometry().width() / 2;
}


void MPropertyTree::setTreeCapitalization(QFont::Capitalization capitalization)
{
    QFont font = propertyTreeView->font();
    font.setCapitalization(capitalization);
    propertyTreeView->setFont(font);
}


void MPropertyTree::onPropertyDestroyed(Met3D::MProperty *prop)
{
    removeProperty(prop);
}


void MPropertyTree::selectPropertyItem(MPropertyTree::Item *item)
{
    if (selected == item) return;

    if (selected != nullptr)
    {
        selected->toggleSelection(false);
    }

    if (item != nullptr)
    {
        item->toggleSelection(true);
        item->setFocus();
        int scrollBarValue = area->horizontalScrollBar()->value();
        area->ensureWidgetVisible(item->getItemWidget(), 0, 0);
        area->horizontalScrollBar()->setValue(scrollBarValue);
    }

    selected = item;

    if (filterButton->isChecked())
    {
        treeFilter.itemFilter = selected;

        filterProperties();
    }
}


void MPropertyTree::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    int labelWidth = getMinColumnWidth();

    for (MPropertyTree::Item *item : items)
    {
        item->setMaxLabelWidth(labelWidth);
    }
}

} // Met3D