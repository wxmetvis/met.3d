/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mfontproperty.h"

// standard library imports

// related third party imports

// local application imports

namespace Met3D
{

MFontProperty::MFontProperty()
    : MValueProperty<QFont>(new MValueProperty<QFont>::Data("", {}))
{
}


MFontProperty::MFontProperty(QString name, QFont defaultValue)
    : MValueProperty<QFont>(new MValueProperty<QFont>::Data(name, defaultValue))
{
}


void MFontProperty::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(getConfigKey(), propertyValue.family());
}


void MFontProperty::loadFromConfiguration(QSettings *settings)
{
    MProperty::loadFromConfiguration(settings);

    QString family = settings->value(getConfigKey()).toString();

    QFontDatabase db;

    if (!db.hasFamily(family))
    {
        family = getDefaultValue().family();
    }

    setValue(QFont(family));
}


MProperty::Editor *MFontProperty::createEditor(QWidget *parentWidget)
{
    return new MFontProperty::Editor(parentWidget, this);
}


MFontProperty::Editor::Editor(QWidget *parent, MFontProperty *property)
    : MProperty::Editor(parent, property)
{
    box = new QFontComboBox(this);
    box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    box->setCurrentFont(property->value());
    box->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
    box->setWritingSystem(QFontDatabase::Latin);
    box->setEditable(false);
    layout->addWidget(box);

    property->setValue(box->currentFont());

    connect(box, &QFontComboBox::currentFontChanged,
            this, &MFontProperty::Editor::currentFontChanged);
}


MFontProperty::Editor::~Editor()
= default;


void MFontProperty::Editor::updateValue()
{
    auto *fontProp = dynamic_cast<MFontProperty*>(property);
    box->setCurrentFont(fontProp->value());
}


void MFontProperty::Editor::updateSize(int height)
{
    box->setFixedHeight(height);
}


void MFontProperty::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        box->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    }
    else
    {
        box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}


void MFontProperty::Editor::currentFontChanged(const QFont &font)
{
    auto *fontProp = dynamic_cast<MFontProperty*>(property);
    fontProp->setValue(font);
}
} // Met3D