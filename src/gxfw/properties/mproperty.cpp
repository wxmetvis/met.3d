/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mproperty.h"

// standard library imports
#include <utility>

// related third party imports
#include <QWidget>
#include <QHBoxLayout>
#include <QList>
#include <QAction>

// local application imports

namespace Met3D
{
MProperty::MProperty(QString name)
        : QObject(nullptr)
{
    d = new MProperty::Data(std::move(name));

    connect(this, &MProperty::loadingFailed,
            MPropertyErrorHandler::getInstance(), &MPropertyErrorHandler::onPropertyLoadingFailed);
}


MProperty::MProperty(const Met3D::MProperty &other)
        : QObject(nullptr)
{
    // Initialize copy of other property.
    d = new MProperty::Data(other.getName());

    // Call assignment operator to copy over the other property.
    *this = other;

    connect(this, &MProperty::loadingFailed,
            MPropertyErrorHandler::getInstance(), &MPropertyErrorHandler::onPropertyLoadingFailed);
}


MProperty::MProperty(MProperty::Data *data)
        : QObject(nullptr),
          d(data)
{
    connect(this, &MProperty::loadingFailed,
            MPropertyErrorHandler::getInstance(), &MPropertyErrorHandler::onPropertyLoadingFailed);
}


MProperty::~MProperty()
{
    for (MProperty *sub : d->subProperties)
    {
        sub->d->parent = nullptr;
        emit propertyRemoved(sub);
    }
    d->subProperties.clear();

    if (d->parent != nullptr)
    {
        d->parent->removeSubProperty(this);
    }

    for (QAction *action : d->contextMenu)
    {
        action->deleteLater();
    }

    for (MProperty::Editor *editor : d->editors)
    {
        editor->disconnect();
    }

    d->editors.clear();

    emit propertyDestroyed(this);

    delete d;
}


MProperty *MProperty::addSubProperty(MProperty *property)
{
    if (d->subProperties.contains(property)) return nullptr;
    if (property == this) return nullptr;

    d->subProperties.append(property);

    property->d->parent = this;

    emit propertyAdded(property);

    return property;
}


MProperty *MProperty::addSubProperty(MProperty &property)
{
    return addSubProperty(&property);
}


MProperty *MProperty::insertSubProperty(int index, Met3D::MProperty *property)
{
    if (d->subProperties.contains(property)) return nullptr;
    if (property == this) return nullptr;

    if (index < 0) index = 0;
    if (index > d->subProperties.count()) index = d->subProperties.count();

    d->subProperties.insert(index, property);

    property->d->parent = this;

    emit propertyInserted(index, property);

    return property;
}


MProperty *MProperty::insertSubProperty(int index, Met3D::MProperty &property)
{
    return insertSubProperty(index, &property);
}


bool MProperty::removeSubProperty(MProperty *property)
{
    if (d->subProperties.contains(property))
    {
        int index = d->subProperties.indexOf(property);
        d->subProperties.removeAt(index);
        property->d->parent = nullptr;
        emit propertyRemoved(property);
        return true;
    }
    return false;
}


bool MProperty::removeSubProperty(MProperty &property)
{
    return removeSubProperty(&property);
}


MProperty *MProperty::getSubProperty(int index) const
{
    if (index < 0 || index >= subPropertyCount()) return nullptr;

    return d->subProperties[index];
}


const QList<MProperty *> &MProperty::getSubProperties() const
{
    return d->subProperties;
}


int MProperty::subPropertyCount() const
{
    return d->subProperties.size();
}


void MProperty::setName(const QString &newName)
{
    d->name = newName;

    emit nameChanged();
}


const QString &MProperty::getName() const
{
    return d->name;
}


void MProperty::setTooltip(const QString &text)
{
    d->tooltip = text;

    emit tooltipChanged();
}


const QString &MProperty::getTooltip() const
{
    return d->tooltip;
}


void MProperty::setEnabled(bool enable)
{
    d->enabled = enable;

    // Enable / disable editors.
    for (Editor *e : d->editors)
    {
        e->setEnabled(enable);
    }

    // Emit signal that the property was enabled / disabled,
    // so that others can react to it.
    emitToggledEnabled();
}


bool MProperty::isEnabled() const
{
    return d->enabled;
}


void MProperty::setConfigKey(const QString &key)
{
    d->configKey = key;
}


const QString &MProperty::getConfigKey() const
{
    return d->configKey;
}


void MProperty::setConfigGroup(const QString &group)
{
    d->configGroup = group;
}


const QString &MProperty::getConfigGroup() const
{
    return d->configGroup;
}


void MProperty::setBackgroundColor(const QColor &color)
{
    d->bgColor = color;

    emit backgroundColorChanged();
}


void MProperty::resetBackgroundColor()
{
    d->bgColor = QColor(0, 0, 0, 0);

    emit backgroundColorChanged();
}


QColor MProperty::getBackgroundColor() const
{
    return d->bgColor;
}


void MProperty::expand(MPropertyTree *tree)
{
    emit toggleExpand(true, tree);
}


void MProperty::collapse(MPropertyTree *tree)
{
    emit toggleExpand(false, tree);
}


void MProperty::expandAll(MPropertyTree *tree)
{
    emit toggleExpand(true, tree);

    for (auto sub : d->subProperties)
    {
        sub->expandAll();
    }
}


void MProperty::collapseAll(MPropertyTree *tree)
{
    emit toggleExpand(false, tree);

    for (auto sub : d->subProperties)
    {
        sub->collapseAll();
    }
}
void MProperty::setHidden(bool hidden)
{
    d->isHidden = hidden;
    emit toggleHidden();
}


bool MProperty::isHidden() const
{
    return d->isHidden;
}


void MProperty::saveAllToConfiguration(QSettings *settings) const
{
    // Create group if set.
    if (!d->configGroup.isEmpty())
    {
        settings->beginGroup(d->configGroup);
    }

    // Save this property.
    if (canSaveToConfig())
    {
        saveToConfiguration(settings);
    }

    // Save all children.
    for (MProperty *child : d->subProperties)
    {
        child->saveAllToConfiguration(settings);
    }

    // Close the group.
    if (!d->configGroup.isEmpty())
    {
        settings->endGroup();
    }
}


void MProperty::loadAllFromConfiguration(QSettings *settings)
{
    QString group = d->configGroup;
    // Open group if set.
    if (!d->configGroup.isEmpty())
    {
        settings->beginGroup(group);
    }

    // Load this property.
    if (canSaveToConfig())
    {
        if (!settings->contains(getConfigKey()) && !getConfigKey().isEmpty())
        {
            emit loadingFailed(this);
        }

        loadFromConfiguration(settings);
    }

    // Load all children.
    for (MProperty *child : d->subProperties)
    {
        child->loadAllFromConfiguration(settings);
    }

    // Close the group.
    if (!group.isEmpty())
    {
        settings->endGroup();
    }
}


MProperty::Editor *MProperty::createEditorWidget(QWidget *parentWidget)
{
    MProperty::Editor *editor = createEditor(parentWidget);
    if (editor == nullptr) return nullptr;
    d->editors.append(editor);

    connect(editor, &MProperty::Editor::editorDestroyed, [=]()
    {
        d->editors.removeAll(editor);
    });

    return editor;
}


void MProperty::suppressValueEvent(bool suppress)
{
    d->suppressValueChangedSignal = suppress;
}


void MProperty::addContextMenuAction(QAction *action)
{
    d->contextMenu.append(action);
}


void MProperty::removeContextMenuAction(QAction *action)
{
    d->contextMenu.removeOne(action);
}


void MProperty::removeContextMenuAction(const QString &actionName)
{
    d->contextMenu.erase(
    std::remove_if(d->contextMenu.begin(), d->contextMenu.end(), [=](QAction *a)
    {
        bool b = a->text() == actionName;

        if (b)
        {
            a->deleteLater();
        }

        return b;
    }), d->contextMenu.end());
}


const QVector<QAction*>& MProperty::getContextMenu() const
{
    return d->contextMenu;
}


MProperty *MProperty::getParent() const
{
    return d->parent;
}


MProperty &MProperty::operator=(const Met3D::MProperty &other)
{
    if (this == &other) return *this;

    setName(other.getName());
    setTooltip(other.getTooltip());
    setConfigGroup(other.getConfigGroup());
    setConfigKey(other.getConfigKey());
    setEnabled(other.isEnabled());
    suppressValueEvent(other.d->suppressValueChangedSignal);
    setBackgroundColor(other.d->bgColor);

    return *this;
}


void MProperty::saveToConfiguration(QSettings *settings) const
{
}


void MProperty::loadFromConfiguration(QSettings *settings)
{
}


MProperty::Editor *MProperty::createEditor(QWidget *parentWidget)
{
    return nullptr;
}


void MProperty::updateEditorSettings() const
{
    for (MProperty::Editor *editor : d->editors)
    {
        editor->updateSettings();
    }
}


void MProperty::updateEditorValues() const
{
    for (MProperty::Editor *editor : d->editors)
    {
        editor->updateValue();
    }
}


void MProperty::emitValueChanged()
{
    if (!d->suppressValueChangedSignal)
    {
        emit valueChanged();
    }
}


void MProperty::emitToggledEnabled()
{
    emit toggledEnabled();
}


void MProperty::setParent(MProperty *newParent)
{
    d->parent = newParent;
}


MProperty::Editor::Editor(QWidget *parent, MProperty *property)
        : QWidget(parent),
          property(property),
          layout(nullptr)
{
    layout = new QHBoxLayout();
    layout->setContentsMargins(0, 0, 0, 0);
    layout->setAlignment(Qt::AlignLeft);
    setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    setLayout(layout);
}


MProperty::Editor::~Editor()
{
    emit editorDestroyed();
    delete layout;
}


void MProperty::Editor::updateSize(int height)
{
}


void MProperty::Editor::updateSettings()
{
}


void MProperty::Editor::updateValue()
{
}


void MProperty::Editor::setExpanding(bool expanding)
{
    if (expanding)
    {
        setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    }
    else
    {
        setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}

MPropertyErrorHandler *MPropertyErrorHandler::instance = new MPropertyErrorHandler();

MPropertyErrorHandler *MPropertyErrorHandler::getInstance()
{
    return instance;
}


const QVector<MProperty *> &MPropertyErrorHandler::getLoadingErrors()
{
    return loadingErrors;
}


void MPropertyErrorHandler::clearLoadingErrors()
{
    loadingErrors.clear();
}


void MPropertyErrorHandler::onPropertyLoadingFailed(MProperty *property)
{
    loadingErrors.append(property);
}


MPropertyErrorHandler::MPropertyErrorHandler()
        : QObject(nullptr)
{
}

} // Met3D