/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPROPERTY_H
#define MET_3D_MPROPERTY_H

// standard library imports

// related third party imports
#include <QtContainerFwd>
#include <QObject>
#include <QColor>
#include <QWidget>
#include <QSettings>
#include <QHBoxLayout>

// local application imports
#include "system/mundostack.h"

// forward declarations
class QAction;

namespace Met3D
{

// Forward declaration.
class MPropertyTree;

/**
 * The base MProperty class for handling properties in Met.3D.
 * They can be displayed in a @c MPropertyTreeBrowser.
 * They can also be used as standalone properties which provide a widget
 * for the user to edit. This can be useful for configuration dialogs or windows.
 * The @c MProperty is itself its value. It doesnt carry any additional value field,
 * that needs to be handled in sub-classes, for example a MBoolProperty would hold
 * a bool value.
 *
 * A property should always be handled as if it is a value.
 * A property is completely unique in regards to its relations in the property tree.
 * Meaning, if you copy a property, it will be a new property with the same settings,
 * but no relations in a tree.
 * The creator of the property is always the owner and responsible for
 * deletion.
 * When the property is deleted, it will remove all sub-properties from its sub-tree
 * but will not delete them.
 * When a property is copied to another, sub-properties and its parent will not be copied.
 * They are unique to the property.
 */
class MProperty : public QObject
{
Q_OBJECT

public:
    /**
     * Declaration of basic editor.
     */
    class Editor;

    /**
     * Creates a new property.
     * The following settings are used to initialize the property.
     * @param name Name of the property.
     * @param saveKey The key to save the property as. If it is empty, the property is not saved.
     */
    explicit MProperty(QString name = "");

    /**
     * Copy constructor, copies everything except sub-properties, editors and parent.
     * @param other Other property to copy.
     */
    MProperty(const MProperty &other);

    /**
     * Deletes this property.
     */
    ~MProperty() override;


    /**
     * Creates and adds a property to this property as a sub.
     * @tparam T The type of property to create.
     * @tparam Args The constructor argument types.
     * @param args The constructor arguments.
     * @return The created property.
     */
    template<class T, typename... Args>
    T *createSubProperty(Args &&... args)
    {
        static_assert(std::is_base_of<MProperty, T>::value,
                      "type parameter must derive from MProperty.");

        T *prop = new T(std::forward<Args>(args)...);

        addSubProperty(prop);

        return prop;
    }


    /**
     * Adds a property to this property as a children in the property tree.
     * @param property The property to add as a sub property.
     * @return The added property or nullptr, if it was not added.
     */
    MProperty *addSubProperty(MProperty *property);

    /**
     * Overload of @c MProperty::addSubProperty(MProperty *property).
     * Internally, the property will still be referenced by pointer.
     * @param property The property to add as a sub property.
     * @return The added property or nullptr, if it was not added.
     */
    MProperty *addSubProperty(MProperty &property);

    /**
     * Inserts a property to this property as a children in the property tree
     * at the given index. If the index is negative or 0, the property will be prepended,
     * if the index too large, the property will be appended.
     * Also automatically sets the parent of the @p property.
     * @param property The property to add as a sub property.
     * @return The added property or nullptr, if it was not added.
     */
    MProperty *insertSubProperty(int index, MProperty *property);

    /**
     * Overload of @c MProperty::insertSubProperty(int index, MProperty *property).
     * Internally, the property will still be referenced by pointer.
     * @param property The property to add as a sub property.
     * @return The added property or nullptr, if it was not added.
     */
    MProperty *insertSubProperty(int index, MProperty &property);

    /**
     * Removes the sub-property @p property from this property.
     * If the property is not a sub-property of this property, return false.
     * @param property The sub-property to remove.
     * @return True if removal was successful. False if the property is not a sub-property of this property.
     */
    bool removeSubProperty(MProperty *property);

    /**
     * Overload of @c MProperty::removeSubProperty(MProperty *property).
     * @param property The sub-property to remove.
     * @return True if removal was successful. False if the property is not a sub-property of this property.
     */
    bool removeSubProperty(MProperty &property);

    /**
     * Gets the sub-property at the given @p index, or @c nullptr, if the index is invalid.
     * @param index Index of the sub-property.
     * @return The sub-property or @c nullptr.
     */
    MProperty *getSubProperty(int index) const;

    /**
     * Gets all sub properties of this property.
     * @return A const list reference to all sub properties.
     */
    const QList<MProperty *> &getSubProperties() const;

    /**
     * Get the amount of sub-properties of this property.
     */
    int subPropertyCount() const;

    /**
     * Set the display name of this property.
     * @param newName The name of this property.
     */
    void setName(const QString &newName);

    /**
     * Get the name of this property to display in the property browser.
     * @return A @c QString containing the property name.
     */
    const QString &getName() const;

    /**
     * Sets the tooltip of this property.
     * The description is shown in the property browser as help text.
     * @param text The description text.
     */
    void setTooltip(const QString &text);

    /**
     * Get the tooltip of this property to display as help text in the property browser.
     * @return A @c QString containing the tooltip of this property or an empty @c QString if none was set.
     */
    const QString &getTooltip() const;

    /**
     * Enables or disables the property.
     * If this property is currently in a disabled sub-tree,
     * the value will still be saved, but only applied once the
     * sub-tree is enabled again.
     */
    void setEnabled(bool enable);

    /**
     * Checks, if the property is enabled or not.
     * If the property is inside a disabled sub-tree, this method
     * will return false, regardless.
     * @return True, if the property is enabled.
     */
    bool isEnabled() const;

    /**
     * Optionally sets the name this property uses, when @c saveToConfiguration() or
     * loadFromConfiguration() is called.
     * If no key is specified or it is empty, the property will not be saved.
     * @param key The name of this property in the save config.
     */
    void setConfigKey(const QString &key);

    /**
     * The key used in the configuration file, when this property
     * is saved to one.
     * @return The configuration key.
     */
    const QString &getConfigKey() const;

    /**
     * Sets the group name, under which all sub-properties will be saved
     * to configuration.
     * The value of this property will also be saved in that group.
     * If the name is empty, no group is created.
     * @param group The group name.
     */
    void setConfigGroup(const QString &group);

    /**
     * Get the name of the configuration group, where the sub-properties
     * will be saved under. If the group name is empty, no group is created.
     * @return The group name.
     */
    const QString &getConfigGroup() const;

    /**
     * Sets the background color of this property.
     * Overrides the property tree color.
     * @param color The new color.
     */
    void setBackgroundColor(const QColor &color);

    /**
     * Resets the background color and instead uses
     * the tree color.
     */
    void resetBackgroundColor();

    /**
     * Get the background color of this property.
     * If none is set, the color will be fully transparent black (only 0).
     * @return The background color of this property.
     */
    QColor getBackgroundColor() const;

    /**
     * Expands the property sub-tree, showing all child properties.
     * This will expand the property in all trees it was added to,
     * except when @p tree is set.
     * @param tree Optional: The tree, in which the property should be expanded.
     */
    void expand(MPropertyTree *tree = nullptr);

    /**
     * Collapses the property sub-tree, hiding all child properties.
     * This will collapse the property in all trees it was added to.
     * @param tree Optional: The tree, in which the property should be expanded.
     */
    void collapse(MPropertyTree *tree = nullptr);

    /**
     * Expands the property sub-tree and all sub properties in it,
     * showing all properties in this sub tree.
     * This will expand the property in all trees it was added to,
     * except when @p tree is set.
     * @param tree Optional: The tree, in which the property should be expanded.
     */
    void expandAll(MPropertyTree *tree = nullptr);

    /**
     * Collapses the property sub-tree and all sub properties in it,
     * showing all properties in this sub tree.
     * This will collapse the property in all trees it was added to,
     * except when @p tree is set.
     * @param tree Optional: The tree, in which the property should be expanded.
     */
    void collapseAll(MPropertyTree *tree = nullptr);

    /**
     * Hides the property in the property tree.
     * @param hidden Whether to hide the property or not.
     */
    void setHidden(bool hidden);

    /**
     * Whether the property is currently hidden in the property tree or not.
     * @return True if hidden.
     */
    bool isHidden() const;

    /**
     * Helper method to connect to the @c valueChanged signal.
     * Connects the given simple function or lambda to the @c valueChanged signal.
     * The given function is called, when the value of this property changes.
     * The signals are automatically disconnected, when this property is destroyed.
     * @tparam Func Function type.
     * @param callback The callback to call on value changes.
     */
    template<class Func>
    void registerValueCallback(Func callback)
    {
        connect(this, &MProperty::valueChanged, callback);
    };


    /**
     * Helper method to connect to the @c valueChanged signal.
     * Connects the given member function or slot to the @c valueChanged signal.
     * The given function is called, when the value of this property changes.
     * The signals are automatically disconnected, when this property is destroyed.
     * @tparam C Class type that owns the given callback.
     * @tparam Func The function or slot type.
     * @param obj Object that owns the given callback.
     * @param callback The member or slot to connect as a callback.
     */
    template<class C, class Func>
    void registerValueCallback(C *obj, Func callback)
    {
        connect(this, &MProperty::valueChanged, obj, callback);
    };

    /**
     * Saves this property to the given @p settings parameter, if @c saveProperty is true.
     * Recursively calls this function on all sub properties, regardless.
     * @param settings The settings object, in which to save the property.
     */
    void saveAllToConfiguration(QSettings *settings) const;

    /**
     * Loads this property from the given @p settings parameter, if @c saveProperty is true.
     * Recursively calls this function on all sub properties, regardless.
     * @param settings The settings object, in which to save the property.
     */
    void loadAllFromConfiguration(QSettings *settings);

    /**
     * Creates an editor widget for this property and adds it to the
     * list of editors.
     * Calls @c createEditor.
     * Returning nullptr means, that this property has no editor.
     * @param parent Parent widget of the editor.
     * @return A new editor widget.
     */
    MProperty::Editor *createEditorWidget(QWidget *parentWidget);

    /**
     * Enables or disables the value event emission.
     * If this is true, setting the value of the property
     * will no longer emit the @c valueChanged() signal.
     * @param suppress Suppress the @c valueChanged() signal or not.
     */
    void suppressValueEvent(bool suppress);

    /**
     * Adds an action to the context menu of this property.
     * The action will be deleted with this property.
     * @param action The action to add.
     */
    void addContextMenuAction(QAction *action);

    /**
     * Removes an action from the context menu of this property.
     * @param action The action to remove.
     */
    void removeContextMenuAction(QAction *action);

    /**
     * Removes all actions from the context menu of this property,
     * that have the given string @c actionName as its label.
     * @param actionName The label or name of the action to remove.
     */
    void removeContextMenuAction(const QString &actionName);

    /**
     * @return A vector of actions that represent the context menu
     * of this property.
     */
    const QVector<QAction*>& getContextMenu() const;

    /**
     * Set the parent property of this property.
     * This can be used for parent relations, where this property
     * is not really a sub-property of a sub-tree, but part of another property.
     * @param newParent The new parent property.
     */
    void setParent(MProperty *newParent);

    /**
     * @return The parent property of this property.
     */
    MProperty *getParent() const;

    /**
     * Copies the property @c other to this one.
     * This will copy everything except sub-properties, editors and parent.
     * @param other The other property.
     * @return A reference to this property with the copied values.
     */
    MProperty &operator=(const MProperty &other);

signals:

    /**
     * Signal emitted, when a sub property was added.
     * @param subProperty The added sub property.
     */
    void propertyAdded(Met3D::MProperty *subProperty);

    /**
     * Signal emitted, when a sub property was inserted at the given index.
     * @param index The index, where the property was inserted.
     * @param subProperty The inserted sub property.
     */
    void propertyInserted(int index, Met3D::MProperty *subProperty);

    /**
     * Signal emitted, when a sub property was removed.
     * @param subProperty The removed sub property.
     */
    void propertyRemoved(Met3D::MProperty *subProperty);

    /**
     * Signal emitted, when this property is destructed.
     * @param property The destructed property.
     */
    void propertyDestroyed(Met3D::MProperty *property);

    /**
     * Emitted, when the value of this property changes.
     * It will also be emitted, when you set the value manually.
     * Use @c emitValueChanged() to emit this signal instead of manually doing
     * it.
     * @example For a button property, this is called when the button is pressed.
     * @example For a number field, this is called, when the number changes.
     */
    void valueChanged();

    /**
     * Emitted, when this property is enabled or disabled.
     * Only emitted after a call of @c setEnabled.
     */
    void toggledEnabled();

    /**
     * Emitted, when the description / tooltip of this property changes.
     */
    void tooltipChanged();

    /**
     * Emitted, when the name of this property changes.
     */
    void nameChanged();

    /**
     * Emitted, when the background color of this property changes.
     */
    void backgroundColorChanged();

    /**
     * Emitted, when this property and its sub-tree is collapsed or
     * expanded.
     * @param expanded Whether it is now expanded or not.
     * @param tree The property tree, in which the property should be expanded.
     */
    void toggleExpand(bool expanded, Met3D::MPropertyTree *tree);

    /**
     * Emitted, when this property is set hidden or shown in the property tree.
     */
    void toggleHidden();

    /**
     * Emitted, when this property could not be loaded with @c loadFromConfiguration.
     * @param property The property that failed to load.
     */
    void loadingFailed(Met3D::MProperty *property);

protected:
    struct Data;

    /**
     * Constructor for derived classes to the
     * @c data pointer of the class.
     * @param data Data pointer of the class.
     */
    explicit MProperty(MProperty::Data *data);

    /**
     * Saves this property to the given @p settings parameter.
     * Only called, if @c saveProperty is true.
     * This should be overridden in derived classes to handle specific
     * cases, on how to save the property.
     * @param settings The settings object, in which to save the property.
     */
    virtual void saveToConfiguration(QSettings *settings) const;

    /**
     * Loads this property from the given @p settings parameter.
     * Only called, if @c saveProperty is true.
     * This should be overridden in derived classes to handle specific
     * cases, on how to load the property.
     * @param settings The settings object, in which to save the property.
     */
    virtual void loadFromConfiguration(QSettings *settings);

    /**
     * Creates an editor widget for this property.
     * Returning nullptr means, that this property has no editor.
     * @param parent Parent widget of the editor.
     * @return A new editor widget.
     */
    virtual MProperty::Editor *createEditor(QWidget *parentWidget);

    /**
     * Update the settings for all editor widgets of this
     * property.
     */
    void updateEditorSettings() const;

    /**
     * Update the values for all editor widgets of this
     * property
     */
    void updateEditorValues() const;

    /**
     * Emits the value changed signal, if
     * it is not currently disabled.
     * Use this method instead of manually emitting
     * the signal.
     */
    void emitValueChanged();

    /**
     * Emits the @c toggledEnabled() signal with the correct
     * parameter value, whether this property is enabled or not.
     */
    void emitToggledEnabled();

    /**
     * Get the data block of this property
     * cast to the correct data block type of a derived property class.
     * No type checks are performed.
     * @tparam D Data block type, which the returned pointer points to.
     * @return The data block of this property cast to the correct type.
     */
    template<typename D>
    D* getData() const
    {
        static_assert(std::is_base_of<MProperty::Data, D>::value,
                      "D parameter is not a pointer to a child of MProperty::Data.");
        return static_cast<D*>(d);
    }

    /**
     * Checks, if this property can be saved to configuration.
     * Can be overwritten in sub-classes to add more checks.
     * @return True, if the property can be saved to a config file.
     */
    virtual bool canSaveToConfig() const { return !d->configKey.isEmpty(); }

    /**
     * Data of the property.
     * This is moved to an external data block,
     * so that the property itself only holds its value
     * directly.
     */
    struct Data
    {
        /**
         * The parent of this property.
         */
        MProperty *parent = nullptr;

        /**
         * The sub-properties of this property.
         */
        QList<MProperty *> subProperties;

        /**
         * The name or title of this property. Visible in the property browser.
         */
        QString name;

        /**
         * A tooltip for this property to explain it to the user.
         */
        QString tooltip = "";

        /**
         * Whether the property is enabled or not.
         */
        bool enabled = true;

        /**
         * Whether this property is hidden in the property tree or not.
         */
        bool isHidden = false;

        /**
         * The key of this property in the save configuration.
         */
        QString configKey;

        /**
         * The group which contains all sub-properties of this property in the
         * save configuration.
         */
        QString configGroup = "";

        /**
         * A list of all currently managed editors.
         */
        QVector<MProperty::Editor *> editors;

        /**
         * If this is true, the @c valueChanged signal should no longer get
         * emitted.
         */
        bool suppressValueChangedSignal = false;

        /**
         * The background color of this property.
         * If it is transparent, the tree color will be used.
         */
        QColor bgColor = QColor(0, 0, 0, 0);

        /**
         * Actions for the right click context menu of this property.
         * If it is empty, no context menu is shown.
         */
        QVector<QAction*> contextMenu;

        explicit Data(QString name)
                : name(std::move(name))
        {}
    };

    /**
     * Pointer to the data block of the property.
     * Use @c getData<D>() to get a pointer to this data block
     * with the correct type for derived classes.
     * @ref getData()
     */
    MProperty::Data *d;

private:
    using QObject::setParent;
    using QObject::parent;
};

/**
 * The base class of all property editors.
 */
class MProperty::Editor : public QWidget
{
    Q_OBJECT

public:
    Editor(QWidget *parent, MProperty *property);

    ~Editor() override;

    /**
     * Method called when subclasses should update the
     * size of all the widgets the editor is made of.
     */
    virtual void updateSize(int height);

    /**
     * Should update the editor settings from the property, such as min or max
     * settings. Should not update the value of the editor.
     */
    virtual void updateSettings();

    /**
     * Should update the editor value from the property.
     */
    virtual void updateValue();

    /**
     * Makes the property editor expand horizontally to the right.
     * If it is disabled, the property editor has its preferred size.
     * @param expanding Whether to expand or stay the preferred size.
     */
    virtual void setExpanding(bool expanding);

signals:
    /**
     * This signal is emitted, when this editor is getting destroyed.
     */
    void editorDestroyed();

protected:
    MProperty *property = nullptr;
    QHBoxLayout *layout = nullptr;
};

/**
 * A class that collects errors or warnings from all properties.
 */
class MPropertyErrorHandler : public QObject
{
    Q_OBJECT

public:
    /**
     * @return Static instance of this class.
     */
    static MPropertyErrorHandler *getInstance();

    /**
     * @return All errors that occurred during loading of properties.
     */
    const QVector<MProperty *> &getLoadingErrors();

    /**
     * Clear all loading errors.
     */
    void clearLoadingErrors();

public slots:
    /**
     * Slot that is connected to @c MProperty and called, when loading of a property fails.
     * @param property The property that failed loading.
     */
    void onPropertyLoadingFailed(Met3D::MProperty *property);

private:
    MPropertyErrorHandler();

    /**
     * The static, singleton instance of this class.
     */
    static MPropertyErrorHandler *instance;

    /**
     * The loading errors collected since the last clear.
     */
    QVector<MProperty *> loadingErrors;
};

/**
 * The template of a value holding property.
 * It handles assignment of values to the property via @c operator=.
 * The value itself can be retrieved with @c value or with @c operator*, by de-referencing the property object.
 * This class should be used as a super class for special value properties, such as a MBoolProperty.
 * Only the value of the property should be saved directly in the property.
 * Settings etc. should be contained in the data block @c d.
 * @tparam T Type of the value that this property holds.
 */
template<typename T>
class MValueProperty : public MProperty
{
public:
    ~MValueProperty() override = 0; // Pure virtual destructor to avoid instantiation.

    /**
     * Return the value of the property.
     * @return A const reference to the value of this property.
     */
    const T &value() const;

    /**
     * Sets the value of this property and emits @c valueChanged()
     * and updates all editors connected to this property,
     * if the new value is different.
     * If this property needs to verify its values, override this function to copy,
     * verify and modify @p value, and afterwards call @c MValueProperty::setValue() with the corrected value,
     * to handle setting the value and emitting the signal.
     * @param value The new value of this property.
     */
    virtual void setValue(const T &value);

    /**
     * Sets the value of this property to @p value and makes that
     * value change undo and redo-able for the user.
     * If @p id is set to something other than @c -1, this call
     * can be chained together with previous calls to this method with the same
     * id, meaning undoing will use to the earliest calls undo, while redo will
     * use to the latest calls redo value.
     * @param value The new value.
     * @param id Optional id to identify mergable undo commands.
     */
    void setUndoableValue(T value, int id = -1);

    /**
     * @return The default value of this property.
     */
    T getDefaultValue() const;

    /**
     * @return A reference to this properties value.
     */
    operator const T &();

    /**
     * @return A copy of this properties value.
     */
    operator T() const;

    /**
     * Sets the value of this property to @c other.
     * @param other The value to set this property to.
     * @return A reference to this property.
     */
    MValueProperty<T> &operator=(const T &other);

    MValueProperty<T> &operator=(const MValueProperty<T> &other);

protected:
    struct Data;

    explicit MValueProperty(MValueProperty::Data *data);
    MValueProperty(const MValueProperty<T> &other);

    /**
     * The internal value of this property.
     */
    T propertyValue;

    struct Data : MProperty::Data
    {
        /**
         * The default value of this property, if set.
         */
        T defaultValue;


        Data(const QString &name, T defaultValue)
                : MProperty::Data(name),
                  defaultValue(defaultValue)
        {}
    };

    /**
     * The undo command when the value of this property is set
     * by the user.
     */
    class SetValueCommand : public QUndoCommand
    {
    public:
        SetValueCommand(MValueProperty<T> *prop, T newValue, int id = -1);
        ~SetValueCommand() override;

        void undo() override;
        void redo() override;
        int id() const override;
        bool mergeWith(const QUndoCommand *other) override;

    protected:
        /**
         * The property which value was set.
         */
        MValueProperty<T> *property;

        /**
         * The previous value of the property.
         */
        T oldValue;

        /**
         * The new value of the property.
         */
        T newValue;

        /**
         * A reference to the connection which is called when the property is
         * destroyed. Saved here, so we can disconnect it when this command
         * is deleted.
         */
        QMetaObject::Connection destroyCallback;

        /**
         * The id for mergable commands. A command that is merged with another
         * will result in the first commands undo and the last commands redo.
         */
        int mergeId;
    };
};


template<typename T>
MValueProperty<T>::MValueProperty(MValueProperty<T>::Data *data)
        : MProperty(data),
          propertyValue(data->defaultValue)
{
}


template<typename T>
MValueProperty<T>::MValueProperty(const MValueProperty<T> &other)
    : MProperty(new MValueProperty<T>::Data(""))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


template<typename T>
MValueProperty<T>::~MValueProperty() = default;


template<typename T>
const T &MValueProperty<T>::value() const
{
    return propertyValue;
}


template<typename T>
void MValueProperty<T>::setValue(const T &value)
{
    // If the value doesn't change, ignore the call.
    if (propertyValue == value) return;

    propertyValue = value;

    updateEditorValues();
    emitValueChanged();
}


template<typename T>
void MValueProperty<T>::setUndoableValue(T value, int id)
{
    if (value == propertyValue) return;

    auto cmd = new SetValueCommand(this, value, id);

    // Executes the command.
    MUndoStack::run(cmd);
}


template<typename T>
T MValueProperty<T>::getDefaultValue() const
{
    auto *data = getData<MValueProperty<T>::Data>();
    return data->defaultValue;
}


template<typename T>
MValueProperty<T>::operator const T &()
{ return propertyValue; }


template<typename T>
MValueProperty<T>::operator T() const
{ return propertyValue; }


template<typename T>
MValueProperty<T> &MValueProperty<T>::operator=(const T &other)
{
    setValue(other);
    return *this;
}


template<typename T>
MValueProperty<T> &MValueProperty<T>::operator=(const MValueProperty<T> &other)
{
    MProperty::operator=(other);

    auto *data = getData<MValueProperty<T>::Data>();
    data->defaultValue = other.getDefaultValue();
    setValue(other.value());

    return *this;
}


template<typename T>
MValueProperty<T>::SetValueCommand::SetValueCommand(MValueProperty<T> *prop, T newValue, int id)
        : QUndoCommand("set " + prop->getName() + " value"),
          property(prop),
          oldValue(prop->propertyValue),
          newValue(newValue),
          mergeId(id)
{
    destroyCallback = connect(property, &MProperty::destroyed, [=]()
    {
        setObsolete(true);
    });
}


template<typename T>
MValueProperty<T>::SetValueCommand::~SetValueCommand()
{
    property->disconnect(destroyCallback);
}


template<typename T>
void MValueProperty<T>::SetValueCommand::undo()
{
    property->setValue(oldValue);
}


template<typename T>
void MValueProperty<T>::SetValueCommand::redo()
{
    property->setValue(newValue);
}


template<typename T>
int MValueProperty<T>::SetValueCommand::id() const
{
    return mergeId;
}


template<typename T>
bool MValueProperty<T>::SetValueCommand::mergeWith(const QUndoCommand *other)
{
    if (id() != other->id()) return false;
    auto *cmd = dynamic_cast<const MValueProperty<T>::SetValueCommand*>(other);

    if (cmd == nullptr) return false;
    if (cmd->property != property) return false;

    newValue = cmd->newValue;
    return true;
}


} // Met3D

#endif //MET_3D_MPROPERTY_H
