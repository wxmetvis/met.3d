/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MFONTPROPERTY_H
#define MET_3D_MFONTPROPERTY_H

// standard library imports

// related third party imports
#include <QFont>
#include <QFontComboBox>

// local application imports
#include "mproperty.h"

// forward declarations

namespace Met3D
{

class MFontProperty : public MValueProperty<QFont>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<QFont>::operator QFont;
    using MValueProperty<QFont>::operator const QFont &;
    using MValueProperty<QFont>::operator=;

    MFontProperty();
    MFontProperty(QString name, QFont defaultValue);

protected:
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;
};

class MFontProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MFontProperty *property);
    ~Editor() override;

    void updateValue() override;

    void updateSize(int height) override;

    void setExpanding(bool expanding) override;

public slots:
    void currentFontChanged(const QFont &font);

protected:
    QFontComboBox *box;
};

} // Met3D

#endif //MET_3D_MFONTPROPERTY_H
