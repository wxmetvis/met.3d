/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MRECTPROPERTY_H
#define MET_3D_MRECTPROPERTY_H

// standard library imports

// related third party imports
#include <QRectF>

// local application imports
#include "mproperty.h"
#include "mnumberproperty.h"

// forward declarations
class QLabel;

namespace Met3D
{

/**
 * A property that holds a @c QRectF, which is editable via automatically
 * created sub-properties.
 * This property cannot have sub-properties added to it.
 * TODO: (tv, 26Feb2024): Maybe add functions like setStep etc.
 *   to control the editor behaviour.
 */
class MRectProperty : public MValueProperty<QRectF>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<QRectF>::operator QRectF;
    using MValueProperty<QRectF>::operator const QRectF &;
    using MValueProperty<QRectF>::operator=;

    MRectProperty();
    MRectProperty(const QString &name, const QRectF &defaultValue);

    MRectProperty(const MRectProperty &other);

    void setValue(const QRectF &value) override;

    /**
      * Sets the step value of this property.
      * @param stepValue The step value used to increment the property via editor.
      */
    void setStep(double step);

    /**
     * Get the current step value.
     */
    double getStep() const;

    /**
     * Set the amount of decimals that this property should have.
     * Only used in its editor.
     * @param decimals Number of visible decimals.
     */
    void setDecimals(int decimals);

    /**
     * Gets the number of visible decimals.
     * @return The number of decimals.
     */
    int getDecimals() const;

public slots:

    /**
     * Called by the sub properties, when their value changes.
     */
    void onRectChanged();

protected:
    // Hide methods to add sub properties.
    using MProperty::addSubProperty;
    using MProperty::createSubProperty;
    using MProperty::removeSubProperty;

    /**
     * Declaration of the editor class.
     */
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<QRectF>::Data
    {
        /**
         * Sub property for the x value of the rect.
         */
        MDoubleProperty *xProp;

        /**
         * Sub property for the y value of the rect.
         */
        MDoubleProperty *yProp;

        /**
         * Sub property for the width of the rect.
         */
        MDoubleProperty *widthProp;

        /**
         * Sub property for the height of the rect.
         */
        MDoubleProperty *heightProp;


        Data(const QString &name, const QRectF &defaultValue)
                : MValueProperty<QRectF>::Data(name, defaultValue),
                  xProp(nullptr),
                  yProp(nullptr),
                  widthProp(nullptr),
                  heightProp(nullptr)
        {}
    };
};

/**
 * The editor for the rect property.
 * Only shows a string representation of the rect values.
 * The editing happens with the sub-properties.
 */
class MRectProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MRectProperty *property);

    void updateSize(int height) override;

    void updateValue() override;

    void setExpanding(bool expanding) override;

protected:
    static const QString LABEL_FORMAT;
    QLabel *valueLabel;
};

} // Met3D

#endif //MET_3D_MRECTPROPERTY_H
