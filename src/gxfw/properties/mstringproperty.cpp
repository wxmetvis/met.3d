/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mstringproperty.h"

// standard library imports

// related third party imports
#include <QHideEvent>
#include <QLabel>
#include <QLineEdit>

// local application imports

namespace Met3D
{
MStringProperty::MStringProperty()
        : MValueProperty<QString>(new MStringProperty::Data("", ""))
{}


MStringProperty::MStringProperty(const QString &name, const QString &defaultValue)
        : MValueProperty<QString>(new MStringProperty::Data(name, defaultValue))
{}


MStringProperty::MStringProperty(const MStringProperty &other)
        : MValueProperty<QString>(new MStringProperty::Data("", {}))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MStringProperty::setEditable(bool canEdit)
{
    auto *data = getData<MStringProperty::Data>();

    data->editable = canEdit;

    this->updateEditorSettings();
}


bool MStringProperty::isEditable() const
{
    auto *data = getData<MStringProperty::Data>();

    return data->editable;
}


MStringProperty &MStringProperty::operator=(const Met3D::MStringProperty &other)
{
    MValueProperty::operator=(other);

    setEditable(other.isEditable());

    return *this;
}


void MStringProperty::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(getConfigKey(), propertyValue);
}


void MStringProperty::loadFromConfiguration(QSettings *settings)
{
    QString value = settings->value(getConfigKey(),
                                    propertyValue).toString();

    MValueProperty<QString>::setValue(value);
}


MProperty::Editor *MStringProperty::createEditor(QWidget *parentWidget)
{
    return new MStringProperty::Editor(parentWidget, this);
}


MStringProperty::Editor::Editor(QWidget *parent,
                                MStringProperty *property)
        : MProperty::Editor(parent, property)
{
    lineEdit = new QLineEdit(this);
    lineEdit->setText(property->value());
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(10);
    lineEdit->setSizePolicy(sizePolicy);
    lineEdit->setReadOnly(!property->isEnabled() || !property->isEditable());
    lineEdit->setAlignment(Qt::AlignLeft);
    lineEdit->setCursorPosition(0);

    QFont font = lineEdit->font();
    font.setCapitalization(QFont::Capitalization::MixedCase);
    lineEdit->setFont(font);

    layout->addWidget(lineEdit);

    connect(lineEdit, &QLineEdit::editingFinished,
            this, &MStringProperty::Editor::onEditingFinished);
}


MStringProperty::Editor::~Editor()
= default;


void MStringProperty::Editor::updateSize(int height)
{
    lineEdit->setFixedHeight(height);
}


void MStringProperty::Editor::updateValue()
{
    auto prop = dynamic_cast<MStringProperty *>(property);
    bool b = lineEdit->blockSignals(true);
    lineEdit->setText(prop->value());
    lineEdit->setCursorPosition(0);
    prop->setValue(lineEdit->text());
    lineEdit->blockSignals(b);
}


void MStringProperty::Editor::updateSettings()
{
    auto *prop = dynamic_cast<MStringProperty *>(property);

    lineEdit->setReadOnly(!property->isEnabled() || !prop->isEditable());
}


void MStringProperty::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(10);
        lineEdit->setSizePolicy(sizePolicy);
    }
    else
    {
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(10);
        lineEdit->setSizePolicy(sizePolicy);
    }
}


void MStringProperty::Editor::onEditingFinished()
{
    QString text = lineEdit->text();
    auto *prop = dynamic_cast<MStringProperty *>(property);

    if (text != prop->value())
    {
        prop->setUndoableValue(text);
    }
}

} // Met3D