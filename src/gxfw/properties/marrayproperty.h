/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MARRAYPROPERTY_H
#define MET_3D_MARRAYPROPERTY_H

// standard library imports

// related third party imports

// local application imports
#include "mproperty.h"

// forward declarations
class QLabel;

namespace Met3D
{

/**
 * The array property can contain multiple properties
 * and lay them out horizontally in its editor.
 */
class MArrayProperty : public MProperty
{
public:
    explicit MArrayProperty(const QString &name = "");

    MArrayProperty(const MArrayProperty &other);

public:

    /**
     * Appends the given property to the array.
     * @param property Property to append.
     */
    void append(MProperty *property);

    /**
     * Removes the given property from the array.
     * @param property Property to remove.
     */
    void remove(MProperty *property);

    /**
     * Return the length of the array property.
     * @return The amount of properties currently stored in the array.
     */
    int length() const;

    MProperty *operator[](int idx);

    const MProperty *operator[](int idx) const;

    /**
     * Get the property at the given index from the array.
     * @param idx The index of the property.
     * @return The property or nullptr.
     */
    MProperty *at(int idx) const;

    /**
     * Get the currently stored properties.
     * @return A @c QVector containing the stored properties.
     */
    const QVector<MProperty *> &getProperties();

    /**
     * Enable or disable the labels being shown in the editor.
     * By default, the names of the properties in the array will
     * be added as labels to the editor.
     * @param show Whether to show labels or not.
     */
    void setShowPropertyLabels(bool show);

    /**
     * Whether the property editor labels are currently being shown or not.
     * @return True if the labels are shown.
     */
    bool showPropertyLabels() const;

    MArrayProperty &operator=(const MArrayProperty &other);

public slots:

    void onPropertyRenamed();

protected:
    /**
     * Declaration of editor class, so
     * that it is no longer visible to the
     * outside.
     */
    class Editor;

    struct Data;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    bool canSaveToConfig() const override;

    struct Data : MProperty::Data
    {
        /**
         * The properties in the array.
         */
        QVector<MProperty *> properties;

        /**
         * Whether labels are shown or not.
         */
        bool showLabels = true;


        explicit Data(const QString &name)
                : MProperty::Data(name)
        {}
    };
};

/**
 * The editor of the array property.
 * Lines the properties in the array up horizontally.
 */
class MArrayProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MArrayProperty *property);

    void updateSize(int height) override;

    void updateSettings() override;

    /**
     * Used to update the editor value.
     * In this case the child editors and labels.
     */
    void updateValue() override;

    /**
     * Called from the array property of this widget, when a new property
     * was added to the array.
     * @param prop Property that was added.
     */
    void propertyAdded(MProperty *prop);

    /**
     * Called from the array property of this widget, when a property
     * was removed from the array.
     * @param prop Property that was removed.
     */
    void propertyRemoved(MProperty *prop);

protected:
    /**
     * Map containing all editors of the properties in the array.
     */
    QMap<MProperty *, MProperty::Editor *> editors;

    /**
     * Map containing the labels of the properties in the array.
     */
    QMap<MProperty *, QLabel *> labels;
};

} // Met3D

#endif //MET_3D_MARRAYPROPERTY_H
