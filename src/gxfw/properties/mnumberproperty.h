/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MNUMBERPROPERTY_H
#define MET_3D_MNUMBERPROPERTY_H

// standard library imports
#include <algorithm>

// related third party imports
#include <QSpinBox>

// local application imports
#include "mproperty.h"
#include "scientificdoublespinbox.h"

namespace Met3D
{

/**
 * Template class for numeric properties.
 * This can be used as a base to implement int double and float properties.
 * It already provides implementations for @c setValue and @¢ saveToConfiguration.
 * It also provides a step value for editors, as well as an upper and lower value limit.
 * The property can additionally be decorated with a prefix and a suffix.
 * You need to implement @c MValueProperty<T>::loadFromConfiguration() and
 * @c MValueProperty<T>::createEditorWidget() at least.
 * @tparam T A numeric type.
 */
template<typename T>
class MNumberProperty : public MValueProperty<T>
{
public:
    MNumberProperty<T>(const MNumberProperty<T> &other);

    ~MNumberProperty() override = 0; // Pure virtual destructor to avoid this class being instantiated.

    void setValue(const T &value) override;

    /**
     * Sets the step value of this property.
     * @param stepValue The step value used to increment the property via editor.
     */
    void setStep(const T &stepValue);

    /**
     * Get the current step value.
     */
    T getStep() const;

    /**
     * Sets the minimum value of this property.
     * If the minimum is larger than the maximum, the maximum will be adjusted
     * to fit the new minimum.
     * @param minimum The new minimum value.
     */
    void setMinimum(const T &minimum);

    /**
     * Get the current minimum value.
     */
    T getMinimum() const;

    /**
     * Sets the maximum value of this property.
     * If the maximum is smaller than the minimum, the minimum will be adjusted
     * to fit the new maximum.
     * @param maximum The new maximum value.
     */
    void setMaximum(const T &maximum);

    /**
     * Get the current maximum value.
     */
    T getMaximum() const;

    /**
     * Sets both minimum and maximum.
     * If they are reversed, the maximum and minimum will be swapped.
     * @param minimum The new minimum value of this property.
     * @param maximum The new maximum value of this property.
     */
    void setMinMax(const T &minimum, const T &maximum);

    /**
     * Set the prefix of the editor.
     * @param prefix Prefix.
     */
    void setPrefix(const QString &prefix);

    /**
     * Gets the editor prefix.
     * @return The prefix of the editor.
     */
    const QString &getPrefix() const;

    /**
     * Set the suffix of the editor. Can be, for example, the unit.
     * @param suffix The suffix to set for the editor.
     */
    void setSuffix(const QString &suffix);

    /**
     * Gets the editors suffix.
     * @return The editor suffix.
     */
    const QString &getSuffix() const;

    MNumberProperty<T> &operator=(const MNumberProperty<T> &other);

protected:
    struct Data;

    explicit MNumberProperty(MNumberProperty::Data *data);

    void saveToConfiguration(QSettings *settings) const override;

    // Data block containing settings for the property.
    struct Data : MValueProperty<T>::Data
    {
        /**
         * Minimum value this property can have. Default is its numeric min limit.
         */
        T min;

        /**
         * Maximum value this property can have. Default is its numeric max limit.
         */
        T max;

        /**
         * Step of this property when editing. Default is the numeric default.
         */
        T step;

        /**
         * The editor prefix.
         */
        QString editorPrefix;

        /**
         * The editor suffix.
         */
        QString editorSuffix;


        Data(const QString &name, T defaultValue)
                : MValueProperty<T>::Data(name, defaultValue),
                  min(std::numeric_limits<T>::lowest()),
                  max(std::numeric_limits<T>::max()),
                  step(1)
        {}
    };
};


template<typename T>
MNumberProperty<T>::MNumberProperty(const MNumberProperty<T> &other)
        : MValueProperty<T>(new MNumberProperty<T>::Data("", {}))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


template<typename T>
MNumberProperty<T>::MNumberProperty(MNumberProperty::Data *data)
        : MValueProperty<T>(data)
{}


template<typename T>
MNumberProperty<T>::~MNumberProperty() = default;


template<typename T>
void MNumberProperty<T>::setValue(const T &value)
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    // Limit value to range.
    T val = std::min(data->max, std::max(data->min, value));

    MValueProperty<T>::setValue(val);
}


template<typename T>
void MNumberProperty<T>::setStep(const T &stepValue)
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    data->step = stepValue;

    MProperty::updateEditorSettings();
}


template<typename T>
T MNumberProperty<T>::getStep() const
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    return data->step;
}


template<typename T>
void MNumberProperty<T>::setMinimum(const T &minimum)
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();

    if (minimum > data->max)
    {
        data->max = minimum;
        data->min = minimum;
    }
    else
    {
        data->min = minimum;
    }

    MProperty::updateEditorSettings();

    // Reset the value, since range updated.
    setValue(this->propertyValue);
}


template<typename T>
T MNumberProperty<T>::getMinimum() const
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();

    return data->min;
}


template<typename T>
void MNumberProperty<T>::setMaximum(const T &maximum)
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    if (maximum < data->min)
    {
        data->min = maximum;
        data->max = maximum;
    }
    else
    {
        data->max = maximum;
    }

    MProperty::updateEditorSettings();

    // Reset the value, since range updated.
    setValue(this->propertyValue);
}


template<typename T>
T MNumberProperty<T>::getMaximum() const
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    return data->max;
}


template<typename T>
void MNumberProperty<T>::setMinMax(const T &minimum, const T &maximum)
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    data->min = std::min(minimum, maximum);
    data->max = std::max(minimum, maximum);

    MProperty::updateEditorSettings();

    // Reset the value, since range updated.
    setValue(this->propertyValue);
}


template<typename T>
void MNumberProperty<T>::setPrefix(const QString &prefix)
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    data->editorPrefix = prefix;
    MProperty::updateEditorSettings();
}


template<typename T>
const QString &MNumberProperty<T>::getPrefix() const
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    return data->editorPrefix;
}


template<typename T>
void MNumberProperty<T>::setSuffix(const QString &suffix)
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    data->editorSuffix = suffix;
    MProperty::updateEditorSettings();
}


template<typename T>
const QString &MNumberProperty<T>::getSuffix() const
{
    auto *data = this->template getData<MNumberProperty<T>::Data>();
    return data->editorSuffix;
}


template<typename T>
MNumberProperty<T> &MNumberProperty<T>::operator=(
        const MNumberProperty<T> &other)
{
    MValueProperty<T>::operator=(other);

    auto *otherData = other.template getData<MNumberProperty<T>::Data>();

    setMinMax(otherData->min, otherData->max);
    setStep(otherData->step);
    setPrefix(otherData->editorPrefix);
    setSuffix(otherData->editorSuffix);

    return *this;
}


template<typename T>
void MNumberProperty<T>::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(this->getConfigKey(), this->propertyValue);
}


/**
 * The template class for all floating point properties.
 * In addition to the limits introduced by @c MNumberProperty<T>,
 * it contains a decimal setting, limiting the amount of decimals visible
 * in the editor.
 * @tparam T Floating point type T.
 */
template<typename T>
class MFloatingPointProperty : public MNumberProperty<T>
{
public:
    MFloatingPointProperty(const MFloatingPointProperty<T> &other);

    ~MFloatingPointProperty() override = 0; // Pure virtual destructor to avoid instantiation.

    /**
     * Set the amount of decimals that this property should have.
     * Only used in its editor.
     * @param decimals Number of visible decimals.
     */
    void setDecimals(int decimals);

    /**
     * Gets the number of visible decimals.
     * @return The number of decimals.
     */
    int getDecimals() const;

    MFloatingPointProperty<T> &operator=(const MFloatingPointProperty<T> &other);

protected:
    class Editor;

    struct Data;

    explicit MFloatingPointProperty(MFloatingPointProperty<T>::Data *data);

    struct Data : MNumberProperty<T>::Data
    {
        /**
         * The number of floating point decimals. Default is 2.
         */
        int decimal;


        Data(const QString &name, T defaultValue)
                : MNumberProperty<T>::Data(name, defaultValue),
                  decimal(2)
        {}
    };
};


template<typename T>
MFloatingPointProperty<T>::MFloatingPointProperty(
        const MFloatingPointProperty<T> &other)
        : MNumberProperty<T>(new MFloatingPointProperty<T>::Data("", {}))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


template<typename T>
MFloatingPointProperty<T>::MFloatingPointProperty(
        MFloatingPointProperty<T>::Data *data)
        :MNumberProperty<T>(data)
{
}


template<typename T>
MFloatingPointProperty<T>::~MFloatingPointProperty() = default;


template<typename T>
void MFloatingPointProperty<T>::setDecimals(int decimals)
{
    auto *data = this->template getData<MFloatingPointProperty<T>::Data>();
    data->decimal = decimals;

    MProperty::updateEditorSettings();
}


template<typename T>
int MFloatingPointProperty<T>::getDecimals() const
{
    auto *data = this->template getData<MFloatingPointProperty<T>::Data>();
    return data->decimal;
}


template<typename T>
MFloatingPointProperty<T> &MFloatingPointProperty<T>::operator=(
        const MFloatingPointProperty<T> &other)
{
    MNumberProperty<T>::operator=(other);

    setDecimals(other.getDecimals());

    return *this;
}


template<typename T>
class MFloatingPointProperty<T>::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MFloatingPointProperty<T> *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateSettings() override;

    void updateValue() override;

    void setExpanding(bool expanding) override;

public slots:

    /**
     * Value changed slot for the spinbox' @c editingFinished signal.
     * Called when the value of the spinbox changes.
     */
    void onValueChanged();

protected:

    /**
     * Spinbox used to edit double or float properties.
     */
    QDoubleSpinBox *spinBox;
};


template<typename T>
MFloatingPointProperty<T>::Editor::Editor(QWidget *parent,
                                          MFloatingPointProperty<T> *property)
        : MProperty::Editor(parent, property)
{
    spinBox = new QDoubleSpinBox(this);
    spinBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    spinBox->setMinimum(property->getMinimum());
    spinBox->setMaximum(property->getMaximum());
    spinBox->setSingleStep(property->getStep());
    spinBox->setPrefix(property->getPrefix());
    spinBox->setSuffix(property->getSuffix());
    spinBox->setValue(property->value());
    spinBox->setDecimals(property->getDecimals());

    layout->addWidget(spinBox);

    connect(spinBox, &QDoubleSpinBox::editingFinished,
            this, &MFloatingPointProperty<T>::Editor::onValueChanged);
}


template<typename T>
MFloatingPointProperty<T>::Editor::~Editor()
= default;


template<typename T>
void MFloatingPointProperty<T>::Editor::updateSize(int height)
{
    spinBox->setFixedHeight(height);
}


template<typename T>
void MFloatingPointProperty<T>::Editor::updateSettings()
{
    auto prop = dynamic_cast<MFloatingPointProperty<T> *>(property);

    if (prop == nullptr) return;

    spinBox->setMinimum(prop->getMinimum());
    spinBox->setMaximum(prop->getMaximum());
    spinBox->setSingleStep(prop->getStep());
    spinBox->setPrefix(prop->getPrefix());
    spinBox->setSuffix(prop->getSuffix());
    spinBox->setDecimals(prop->getDecimals());
}


template<typename T>
void MFloatingPointProperty<T>::Editor::updateValue()
{
    auto prop = dynamic_cast<MFloatingPointProperty<T> *>(property);

    if (prop == nullptr) return;

    bool b = spinBox->blockSignals(true);
    spinBox->setValue(prop->value());
    prop->setValue(static_cast<T>(spinBox->value()));
    spinBox->blockSignals(b);
}


template<typename T>
void MFloatingPointProperty<T>::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        spinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    }
    else
    {
        spinBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}


template<typename T>
void MFloatingPointProperty<T>::Editor::onValueChanged()
{
    auto prop = dynamic_cast<MFloatingPointProperty<T> *>(property);
    const T val = static_cast<T>(spinBox->value());

    if (val != prop->value())
    {
        prop->setUndoableValue(val);
    }
}


/**
 * The template class for all floating point properties in scientific notation.
 * @tparam T Floating point type T.
 */
template<typename T>
class MSciFloatingPointProperty : public MFloatingPointProperty<T>
{
public:
    MSciFloatingPointProperty(const MSciFloatingPointProperty<T> &other);
    ~MSciFloatingPointProperty() override = 0; // Pure virtual destructor to avoid instantiation.

    /**
     * Sets the significant digits of this property.
     * @param digits The amount of significant digits.
     */
    void setSignificantDigits(int digits);

    /**
     * @return The significant digits of this property.
     */
    int getSignificantDigits() const;

    /**
     * Set the exponent, where the notation switches between regular
     * and scientific notation.
     * @param exponent The exponent.
     */
    void setSwitchExponent(int exponent);

    /**
     * Get the exponent, where the notation switches between regular
     * and scientific notation.
     */
    int getSwitchExponent() const;

    /**
     * Sets the minimum exponent. This is the same as
     * @c setDecimals() with additional checks.
     * @param minExponent The min exponent.
     */
    void setMinExponent(int minExponent);

    /**
     * Get the minimum exponent.
     */
    int getMinExponent() const;

    /**
     * Get the value of this property as formatted text.
     * @return The value as formatted text.
     */
    QString getValueAsText() const;

    /**
     * Get the specified value as formatted text, as it would be displayed in this property.
     * @param value The value to format.
     * @return The formatted text.
     */
    QString getValueAsText(const T &value) const;

    MSciFloatingPointProperty<T> &operator=(const MSciFloatingPointProperty<T> &other);

protected:
    using MFloatingPointProperty<T>::setDecimals;
    using MFloatingPointProperty<T>::getDecimals;

    class Editor;

    struct Data;

    explicit MSciFloatingPointProperty(
            MSciFloatingPointProperty<T>::Data *data);

    struct Data : MFloatingPointProperty<T>::Data
    {
        /**
         * The number of significant digits
         */
        int sigDigits;

        /**
         * The exponent, at which the switch between standard and scientific
         * notation occurs.
         */
        int switchExponent;


        Data(const QString &name, T defaultValue)
                : MFloatingPointProperty<T>::Data(name, defaultValue),
                  sigDigits(2),
                  switchExponent(3)
        {}
    };
};


template<typename T>
MSciFloatingPointProperty<T>::MSciFloatingPointProperty(
        const MSciFloatingPointProperty<T> &other)
        : MFloatingPointProperty<T>(new MSciFloatingPointProperty<T>::Data("", {}))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


template<typename T>
MSciFloatingPointProperty<T>::MSciFloatingPointProperty(
        MSciFloatingPointProperty<T>::Data *data)
        :MFloatingPointProperty<T>(data)
{
    this->setMinExponent(120);
}


template<typename T>
MSciFloatingPointProperty<T>::~MSciFloatingPointProperty() = default;


template<typename T>
void MSciFloatingPointProperty<T>::setSignificantDigits(int digits)
{
    auto *data = this->template getData<MSciFloatingPointProperty<T>::Data>();
    // Only in range from 1 to 9 (max significant digits of float value).
    data->sigDigits = qBound(1, digits, 9);
    MProperty::updateEditorSettings();
}


template<typename T>
int MSciFloatingPointProperty<T>::getSignificantDigits() const
{
    auto *data = this->template getData<MSciFloatingPointProperty<T>::Data>();
    return data->sigDigits;
}


template<typename T>
void MSciFloatingPointProperty<T>::setSwitchExponent(int exponent)
{
    auto *data = this->template getData<MSciFloatingPointProperty<T>::Data>();
    data->switchExponent = exponent;
    MProperty::updateEditorSettings();
}


template<typename T>
int MSciFloatingPointProperty<T>::getSwitchExponent() const
{
    auto *data = this->template getData<MSciFloatingPointProperty<T>::Data>();
    return data->switchExponent;
}


template<typename T>
void MSciFloatingPointProperty<T>::setMinExponent(int minExponent)
{
    int minExp = qBound(0, minExponent, std::numeric_limits<T>::max_exponent10);
    this->setDecimals(minExp);
    MProperty::updateEditorSettings();
}


template<typename T>
int MSciFloatingPointProperty<T>::getMinExponent() const
{
    return this->getDecimals();
}


template<typename T>
QString MSciFloatingPointProperty<T>::getValueAsText() const
{
    return getValueAsText(this->propertyValue);
}


template<typename T>
QString MSciFloatingPointProperty<T>::getValueAsText(const T &value) const
{
    int significDigits = std::max(0, this->getSignificantDigits() - 1);
    int switchNotationExp = this->getSwitchExponent();

    QString text = QLocale::system().toString(value, 'E', significDigits);
    T fValue = QLocale::system().toDouble(text);

    // Switch to scientific notation only if the absolute value of the exponent
    // is bigger than the threshold given.
    if (fValue != 0.
            && (static_cast<int>(fabs(floor(log10(fabs(fValue)))))
                    >= switchNotationExp))
    {
        text = QLocale::system().toString(fValue, 'E', significDigits);
    }
    else
    {
        int indexExponentialSign = text.indexOf(
                QLocale::system().exponential(), 0, Qt::CaseInsensitive);
        QString exponentString = text.right(
                text.length() - (indexExponentialSign + 1));
        int exponent = QLocale::system().toInt(exponentString);
        if (exponent < 0)
        {
            significDigits -= exponent;
        }

        text = QLocale::system().toString(fValue, 'f', significDigits);
    }

    // Remove trailing zeros after decimal point.
    int decimalPointIndex = text.indexOf(QLocale::system().decimalPoint());
    if (decimalPointIndex > 0)
    {
        int exponentIndex = text.indexOf(QLocale::system().exponential(), 0,
                                         Qt::CaseInsensitive);
        if (exponentIndex >= 0)
        {
            exponentIndex -= 1;
        }
        else
        {
            exponentIndex = text.length();
        }
        int nonZeroIndex = text.lastIndexOf(QRegExp("[^0]"), exponentIndex);
        if (nonZeroIndex >= decimalPointIndex)
        {
            if (nonZeroIndex != decimalPointIndex)
            {
                nonZeroIndex++;
            }
            text.remove(nonZeroIndex, exponentIndex + 1 - nonZeroIndex);
        }
    }

    return text;
}


template<typename T>
MSciFloatingPointProperty<T> &MSciFloatingPointProperty<T>::operator=(
        const MSciFloatingPointProperty<T> &other)
{
    MFloatingPointProperty<T>::operator=(other);

    setSignificantDigits(other.getSignificantDigits());
    setSwitchExponent(other.getSwitchExponent());

    return *this;
}


template<typename T>
class MSciFloatingPointProperty<T>::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MSciFloatingPointProperty<T> *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateSettings() override;

    void updateValue() override;

    void setExpanding(bool expanding) override;

public slots:

    /**
     * Value changed slot for the spinbox' @c editingFinished signal.
     * Called when the value of the spinbox changes.
     */
    void onValueChanged();

protected:

    /**
     * Spinbox used to edit double or float properties.
     */
    MScientificDoubleSpinBox *spinBox;
};


template<typename T>
MSciFloatingPointProperty<T>::Editor::Editor(QWidget *parent,
                                             MSciFloatingPointProperty<T> *property)
        : MProperty::Editor(parent, property)
{
    spinBox = new MScientificDoubleSpinBox(this);
    spinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    spinBox->setDecimals(property->getDecimals());
    spinBox->setSignificantDigits(property->getSignificantDigits());
    spinBox->setSwitchNotationExponent(property->getSwitchExponent());
    spinBox->setSingleStep(property->getStep());
    spinBox->setMinimum(property->getMinimum());
    spinBox->setMaximum(property->getMaximum());
    spinBox->setPrefix(property->getPrefix());
    spinBox->setSuffix(property->getSuffix());
    spinBox->setValue(property->value());

    layout->addWidget(spinBox);

    connect(spinBox, &QDoubleSpinBox::editingFinished,
            this, &MSciFloatingPointProperty<T>::Editor::onValueChanged);
}


template<typename T>
MSciFloatingPointProperty<T>::Editor::~Editor()
= default;


template<typename T>
void MSciFloatingPointProperty<T>::Editor::updateSize(int height)
{
    spinBox->setFixedHeight(height);
}


template<typename T>
void MSciFloatingPointProperty<T>::Editor::updateSettings()
{
    auto prop = dynamic_cast<MSciFloatingPointProperty<T> *>(property);

    if (prop == nullptr) return;

    spinBox->setMinimum(prop->getMinimum());
    spinBox->setMaximum(prop->getMaximum());
    spinBox->setSingleStep(prop->getStep());
    spinBox->setPrefix(prop->getPrefix());
    spinBox->setSuffix(prop->getSuffix());
    spinBox->setDecimals(prop->getDecimals());
    spinBox->setSignificantDigits(prop->getSignificantDigits());
    spinBox->setSwitchNotationExponent(prop->getSwitchExponent());
}


template<typename T>
void MSciFloatingPointProperty<T>::Editor::updateValue()
{
    auto prop = dynamic_cast<MSciFloatingPointProperty<T> *>(property);

    if (prop == nullptr) return;

    bool b = spinBox->blockSignals(true);
    spinBox->setValue(prop->value());
    prop->setValue(static_cast<T>(spinBox->value()));
    spinBox->blockSignals(b);
}


template<typename T>
void MSciFloatingPointProperty<T>::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        spinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    }
    else
    {
        spinBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}


template<typename T>
void MSciFloatingPointProperty<T>::Editor::onValueChanged()
{
    auto prop = dynamic_cast<MSciFloatingPointProperty<T> *>(property);
    const T val = static_cast<T>(spinBox->value());

    if (val != prop->value())
    {
        prop->setUndoableValue(val);
    }
}


/**
 * Class representing integer properties.
 */
class MIntProperty : public MNumberProperty<int32_t>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<int32_t>::operator=;
    using MValueProperty<int32_t>::operator int32_t;
    using MValueProperty<int32_t>::operator const int32_t &;

    MIntProperty();
    MIntProperty(const QString &name, int32_t defaultValue);

protected:
    class Editor;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;
};

/**
 * Editor for int properties.
 */
class MIntProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MIntProperty *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateSettings() override;

    void updateValue() override;

    void setExpanding(bool expanding) override;

public slots:

    /**
     * Value changed slot for the spinbox' @c editingFinished signal.
     * Called when the editing of the spinbox ends and its value changes.
     */
    void onValueChanged();

protected:
    /**
     * Spinbox used to edit int properties.
     */
    QSpinBox *spinBox;
};

/**
 * Property for double values.
 */
class MDoubleProperty : public MFloatingPointProperty<double>
{
public:
    using MValueProperty<double>::operator=;
    using MValueProperty<double>::operator double;
    using MValueProperty<double>::operator const double &;

    MDoubleProperty();
    MDoubleProperty(const QString &name, double defaultValue);


    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;
};

/**
 * Property for float values.
 */
class MFloatProperty : public MFloatingPointProperty<float>
{
public:
    using MValueProperty<float>::operator=;
    using MValueProperty<float>::operator float;
    using MValueProperty<float>::operator const float &;

    MFloatProperty();
    MFloatProperty(const QString &name, float defaultValue);

protected:
    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;
};


/**
 * Property for double values in scientific notation.
 */
class MSciDoubleProperty : public MSciFloatingPointProperty<double>
{
public:
    using MValueProperty<double>::operator=;
    using MValueProperty<double>::operator double;
    using MValueProperty<double>::operator const double &;

    MSciDoubleProperty();
    MSciDoubleProperty(const QString &name, double defaultValue);


    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;
};

/**
 * Property for float values in scientific notation.
 */
class MSciFloatProperty : public MSciFloatingPointProperty<float>
{
public:
    using MValueProperty<float>::operator=;
    using MValueProperty<float>::operator float;
    using MValueProperty<float>::operator const float &;

    MSciFloatProperty();
    MSciFloatProperty(const QString &name, float defaultValue);

protected:
    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;
};

}// Met3D

#endif //MET_3D_MNUMBERPROPERTY_H
