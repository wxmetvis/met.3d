/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPROPERTYTREE_H
#define MET_3D_MPROPERTYTREE_H

// standard library imports

// related third party imports
#include <QtContainerFwd>
#include <QFrame>

// local application imports
#include "mproperty.h"
#include "mtreeexpandbutton.h"

// forward declarations
class QScrollArea;
class QVBoxLayout;
class QLabel;
class QLineEdit;
class QToolButton;

namespace Met3D
{

/**
 * The basic property tree class.
 * Displays a tree of properties.
 * Deleting the tree widget, does not delete the properties within it.
 */
class MPropertyTree : public QWidget
{
Q_OBJECT

protected:
    class Item;
    class ViewLayout;
    class PropertyView;

    /**
     * A filter applied to the tree.
     */
    struct TreeFilter
    {
        Item *itemFilter = nullptr;
        QString nameFilter;
    };

public:
    /**
     * The method used to generate new colors for
     * tree depths, that have no dedicated color assigned.
     */
    enum ColorMethod
    {
        DARKEN, // Darken the last color each level.
        LIGHTEN, // Lighten the last color each level.
        KEEP // Keep the last color for all levels.
    };

    explicit MPropertyTree(QWidget *parent = nullptr);

    ~MPropertyTree() override;

    /**
     * Adds a top-level property to the tree.
     * Adding sub-properties is managed by the properties themselves.
     * The function returns the added property back, as that allows to directly
     * add the property to the tree while creating it in a single call,
     * instead of first creating it, then adding it.
     * @param property The top level property to add.
     * @return The pointer of the property that was added, or nullptr if it was not added.
     */
    MProperty *addProperty(MProperty *property);

    /**
     * Inserts a top-level property to the tree at the given index.
     * If the index is negative, it will be added to the end.
     * If the property is already part of the tree, it will be moved
     * to the new index.
     * Adding sub-properties is managed by the properties themselves.
     * The function returns the added property back, as that allows to directly
     * add the property to the tree while creating it in a single call,
     * instead of first creating it, then adding it.
     * @param index The index, where the property is inserted.
     * @param property The top level property to add.
     * @return The pointer of the property that was added, or nullptr if it was not added.
     */
    MProperty *insertProperty(int index, MProperty *property);

    /**
     * Inserts a top-level property to the tree after another property that is already
     * part of the tree.
     * If the new @p property is already part of the tree, it will be moved.
     * Adding sub-properties is managed by the properties themselves.
     * The function returns the added property back, as that allows to directly
     * add the property to the tree while creating it in a single call,
     * instead of first creating it, then adding it.
     * @param before The property in the tree that should be before / above the
     * new property.
     * @param property The new property that will be placed after / below
     * the @p before property.
     * @return The pointer of the property that was added, or nullptr if it was not added.
     */
    MProperty *insertPropertyAfter(MProperty *before, MProperty *property);


    /**
     * Creates and adds a property to this tree as a top-level property.
     * @tparam T The type of property to create.
     * @tparam Args The constructor argument types.
     * @param args The constructor arguments.
     * @return The created property.
     */
    template<class T, typename... Args>
    T *createProperty(Args &&... args)
    {
        static_assert(std::is_base_of<MProperty, T>::value,
                      "type parameter must derive from MProperty.");

        T *prop = new T(std::forward<Args>(args)...);

        addProperty(prop);

        return prop;
    }


    /**
     * Removes a top-level property from the tree.
     * This automatically deletes all sub-properties too.
     * @param property The top level property to remove.
     */
    void removeProperty(MProperty *property);

    /**
     * Sets the base color of this property tree.
     * This is the color for the rows of the first level of this tree.
     * Will overwrite any colors currently set.
     * The base color is the color of the top level items.
     * Each successive level gets lighter, darker or stays this color, depending
     * on the color method set in @c setColorGenerationMethod.
     * @param color The base color.
     */
    void setBaseLevelColor(const QColor &color);

    /**
     * Sets the colors of several levels in the property tree.
     * If the total depth of the tree is larger than the list of the
     * colors, the last color is taken and the remaining levels will be colored
     * lighter, darker or stays that color, depending
     * on the color method set in @c setColorGenerationMethod.
     * @param colors A vector containing the colors of the property tree.
     */
    void setLevelColors(const QVector<QColor> &colors);

    /**
     * Sets the background color of the widget, which is visible
     * behind the properties.
     * @param color The background color.
     */
    void setBackgroundColor(const QColor &color);

    /**
     * Sets the color generation method for tree levels, that have no
     * dedicated color assigned. If you previously assigned colors via @c setLevelColors,
     * the last color will be used and the given color method applied for each level
     * after the level with the last specified color. If you used @c setBaseLevelColor
     * that color is used and the selected method applied.
     * @param method The color generation method for each tree level.
     */
    void setColorGenerationMethod(const ColorMethod &method);

    /**
     * Sets the color generation method strength for @c LIGHTEN and @c DARKEN
     * modes. In percent, where @c 100 is 100%.
     * @param strength The strength from 0 to 100.
     */
    void setColorGenerationMethodStrength(int strength);

    /**
     * Filter all properties in this tree with the current filter.
     */
    void filterProperties();

    /**
     * Get the current filter used to filter the property tree.
     * @return A reference to the current filter.
     */
    const TreeFilter &getFilter() const;

    /**
     * @return All current top-level properties.
     */
    const QVector<MPropertyTree::Item *> &getTopLevelItems() const;

    /**
     * @return The min width of the given column in the tree. The right column can
     * expand further.
     */
    int getMinColumnWidth() const;

    /**
     * Sets the capitalization mode of the property tre.
     * @param capitalization The capitalization mode.
     */
    void setTreeCapitalization(QFont::Capitalization capitalization);

public slots:
    /**
     * Slot called, when one of the top-level properties is destroyed.
     * @param prop The destroyed property.
     */
    void onPropertyDestroyed(Met3D::MProperty *prop);

protected:

    /**
     * Selects a property tree item and unselects the old one.
     * @param item Item to select.
     */
    void selectPropertyItem(MPropertyTree::Item *item);

    void resizeEvent(QResizeEvent *event) override;

    /**
     * The widget containing the search box.
     */
    QLineEdit *searchBox;

    /**
     * Tool button to enable filtering.
     */
    QToolButton *filterButton;

    /**
     * Tool button used to expand and collapse all properties in the view.
     * It switches between the modes after clicking it.
     */
    QToolButton *toggleExpandButton;

    /**
     * The header layout containing the search box.
     */
    QHBoxLayout *header;

    /**
     * The scroll area for the property tree.
     */
    QScrollArea *area;

    /**
     * Layout for search and scroll area.
     */
    QVBoxLayout *layout;

    /**
     * The widget used to store all property items.
     */
    MPropertyTree::PropertyView *propertyTreeView;

    /**
     * A map of all top level properties to their representative
     * item widgets.
     */
    QMap<MProperty *, MPropertyTree::Item *> items;

    /**
     * Base colors.
     */
    QVector<QColor> levelColors;

    /**
     * The method used to generate colors for the different tree levels.
     */
    ColorMethod colorGenMethod;

    /**
     * The strength of the color method for @c DARKEN and @c LIGHTEN modes.
     */
    int colorMethodStrength;

    /**
     * The height of each row in the property tree.
     */
    int rowHeight;

    /**
     * The currently selected item, nullptr, if none is selected.
     */
    MPropertyTree::Item *selected;

    /**
     * The current tree filter.
     */
    MPropertyTree::TreeFilter treeFilter;
};

/**
 * This class represents an item in the property tree
 * and all its sub items.
 * It is a widget and displays the property and its editor.
 * It provides a button to expand and collapse the property.
 * It can also be double clicked to expand or collapse.
 */
class MPropertyTree::Item : public QWidget
{
Q_OBJECT

public:
    /**
     * Creates a new property tree item at the given depth.
     * It is a child widget to the given @p parent and represents
     * the property @p property.
     * @param tree The parent tree widget.
     * @param property The property that is represented by this tree item.
     * @param depth The depth of this item in the tree. Default is 0 for top level items.
     */
    explicit Item(MPropertyTree *tree, MPropertyTree::Item *parent,
                  MProperty *property, int depth = 0);

    /**
     * Destroys this item and all its sub-items.
     * Does not destroy the underlying property, only its visual representation.
     */
    ~Item() override;

    /**
     * Expands this item and makes its sub property items visible.
     */
    void expand();

    /**
     * Collapses this item and hides its sub property items.
     */
    void collapse();

    /**
     * Expands this item and all children. This makes its sub property items visible.
     */
    void expandAll();

    /**
     * Collapses this item and all children.
     */
    void collapseAll();

    /**
     * Filters the properties in this sub-tree with the given filter.
     * This search also includes THIS property item.
     * @param filter The filter used to search for the property.
     * @return True, if the filter fits somewhere in this sub-tree.
     */
    bool filterProperties(const TreeFilter &filter);

    /**
     * Sets the split distance from the left start point of this widget.
     * This is where the editor widgets should be aligned at.
     * Sets @c spacer to extend to that split distance.
     * @param split The distance from the left of this widget to the split point.
     */
    void setSplit(int split);

    /**
     * Sets the level colors for this sub-tree, where each
     * entry in the given color vector corresponds to a level.
     * If a level has no entry in the vector, the last entry is taken
     * and the specified generation method is used to generate colors for
     * each successive level after it.
     * @param colors Level colors.
     * @param genMethod The method used to generate the colors for.
     * @param strength The strength of the used color generation method for
     * levels not defined by @p colors.
     */
    void setColors(const QVector<QColor> &colors, const ColorMethod &genMethod,
                   int strength);

    /**
     * Selects or deselects this property.
     * If another property in the tree is currently selected,
     * it will be deselected.
     * @param select Whether to select or not.
     */
    void toggleSelection(bool select);

    /**
     * Returns the item widget that only displays the underlying property and its editor
     * and not all its sub-properties. This can be used to focus a property in the tree.
     * @return The widget that solely represents this item, without its sub-properties.
     */
    QWidget *getItemWidget() const { return itemWidget; }

    /**
     * @return The required split of this item.
     */
    int getRequiredSplit() const { return maxRequiredSplit; }

    /**
     * Invalidates the item, so it no longer functions and can be deleted.
     * This should be done to avoid conditions, where the item is pending
     * deletion but might still be used. That way, it cannot be used and is ignored.
     */
    void invalidate();

    /**
     * Set the max label width that the property label is allowed to have,
     * to avoid pushing the editors off-screen.
     * @param width The max width.
     */
    void setMaxLabelWidth(int width);

signals:
    /**
     * Signal emitted, when the required split of this property item changes.
     * @param split The split width.
     * @param refreshAll Whether it is needed to refresh all parents.
     */
    void requiredSplitChanged(int split, bool refreshAll);

public slots:

    /**
     * Slot for toggling the sub property tree.
     * Will expand or collapse the sub tree.
     * Called by the @c expandButton.
     */
    void onExpandButtonPressed();

    /**
     * Slot for toggling the sub property tree and all properties within it.
     * Will expand or collapse the sub tree and all properties within it.
     * Called by the @c expandButton.
     */
    void onExpandButtonAltPressed();

    /**
     * Slot that is called when sub properties are added to the underlying property.
     * @param subProperty Sub property that was added to the underlying property.
     */
    void onSubPropertyAdded(Met3D::MProperty *subProperty);

    /**
     * Slot that is called when sub properties are inserted to the underlying property.
     * @param index The insertion index.
     * @param subProperty Sub property that was inserted to the underlying property.
     */
    void onSubPropertyInserted(int index, Met3D::MProperty *subProperty);

    /**
     * Slot that is called when sub properties are removed from the underlying property.
     * @param subProperty Sub property that was removed from the underlying property.
     */
    void onSubPropertyRemoved(Met3D::MProperty *subProperty);

    /**
     * Slot that is called when the underlying property was enabled or disabled.
     */
    void onToggleEnabled();

    /**
     * Slot that is called when the description of the property changes.
     */
    void onTooltipChanged();

    /**
     * Slot that is called, when the name of the property changes.
     */
    void onNameChanged();

    /**
     * Slot that is called, when the background color of the property changes.
     */
    void onColorChanged();

    /**
     * Slot that is called, when the property is programmatically collapsed or
     * expanded.
     * @param expanded Whether it is now expanded or not.
     */
    void onToggleExpand(bool expanded, Met3D::MPropertyTree *tree);

    /**
     * Slot that is called, when the property is set to be hidden / shown
     * in the tree. A hidden property is still part of the tree, just never
     * visible.
     */
    void onToggleHidden();

    /**
     * Slot called when the required split width of a sub item changed.
     * @param split The updated split width.
     * @param refreshAll Whether it might be needed to update all parents.
     */
    void onRequiredSplitChanged(int split, bool refreshAll);

protected:

    /**
     * Helper to update widget visibility.
     */
    void updateVisibility();

    /**
     * Update the expand button visibility.
     * It is visible if this property item has sub items,
     * which are not hidden. If all sub properties are hidden,
     * or none exist, the button will be hidden.
     */
    void updateExpandButtonVisibility();

    /**
     * Applies @c filterProperties() to this sub tree and refreshes
     * the filter state of all properties to the root of the tree.
     * This is faster then refreshing the whole tree.
     * @param filter The filter to apply.
     */
    void filterPropertiesPartial(const TreeFilter &filter);

    /**
     * This should be called when any of the sub properties fits the current filter,
     * so that this property can update its filter state based on that.
     * @param subFitsFilter Whether one or many sub properties fits the filter.
     * @param filter The applied filter.
     */
    void subPropertyFitsFilter(bool subFitsFilter, const TreeFilter &filter);

    /**
     * Find the next visible property in the sub-tree.
     * @param ignoreSubTree Whether the subtree is ignored when searching for the
     * next property.
     * @return The next property or nullptr if none can be found.
     */
    MPropertyTree::Item *findNext(bool ignoreSubTree = false);

    /**
     * Find the previous visible property in the sub-tree.
     * @return The previous property or nullptr if none can be found.
     */
    MPropertyTree::Item *findPrevious();

    /**
     * Find the last visible property in the sub-tree.
     * @return The last currently visible property in the sub-tree.
     */
    MPropertyTree::Item *findLast();

    /**
     * The maximum required split width for this property.
     * This is the distance from the left edge of the property tree
     * to the start of the editor widgets.
     * This is the max value of all the required split values in this properties
     * sub tree.
     * @return The maximum required split width for this property.
     */
    int calcMaxRequiredSplit() const;

    /**
     * The minimum required split width for this property.
     * This is the distance from the left edge of the property tree
     * to the start of the editor widgets.
     * @return The minimum required split width for this property.
     */
    int calcMinRequiredSplit() const;

    /**
     * Updates the max required split of this item and emits
     * @c requiredSplitChanged() when it changed.
     * If @p refreshAll is true, it will replace the max split even if
     * the value of @p split is smaller.
     * @param split The new possible new max split.
     * @param refreshAll Whether to refresh all parents and account for a smaller max split.
     */
    void updateMaxRequiredSplit(int split, bool refreshAll = false);

    void paintEvent(QPaintEvent *event) override;
    void mouseDoubleClickEvent(QMouseEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void contextMenuEvent(QContextMenuEvent *event) override;
    void keyPressEvent(QKeyEvent *event) override;

    /**
     * The vertical layout of this property tree item.
     * Contains the representation of the underlying property and all its sub properties.
     */
    QVBoxLayout *parentLayout;

    /**
     * The horizontal layout for the representation of the underlying property.
     * Contains at least the expand button and a label.
     */
    QHBoxLayout *itemLayout;

    /**
     * The widget to hold the @c itemLayout.
     */
    QWidget *itemWidget;

    /**
     * The spacer responsible to align all the editors.
     * It adds space between the property label and the
     * property editor.
     */
    QSpacerItem *spacer;

    /**
     * The widget that contains the property label
     * and editor.
     */
    QFrame *propertyWidget;

    /**
     * The horizontal layout for the property label
     * and editor.
     */
    QHBoxLayout *propertyLayout;

    /**
     * The button used to expand or collapse the sub properties.
     */
    MTreeExpandButton *expandButton;

    /**
     * The label displaying the underlying property name.
     */
    QLabel *propertyLabel;

    /**
     * An editor for the underlying property.
     * If the editor is @c nullptr, no editor is displayed.
     */
    MProperty::Editor *editor;

    /**
     * The depth of this item in the tree.
     */
    int treeDepth;

    /**
     * The margin around the property widget.
     */
    int margin = 4;

    /**
     * The underlying property that is represented by this item widget.
     */
    MProperty *property;

    /**
     * A map of all sub-properties to their representative sub-items.
     */
    QMap<MProperty *, MPropertyTree::Item *> subItems;

    /**
     * Whether the @c property fits the current filter of the tree.
     * This can also be true, if a parent property fits the filter.
     */
    bool fitsFilter;

    /**
     * The property tree that owns this item.
     */
    MPropertyTree *treeWidget;

    /**
     * The parent item in the tree or nullptr, if this is a top-level
     * item.
     */
    MPropertyTree::Item *parent;

    /**
     * Whether this item is currently enabled or not.
     */
    bool enabled;

    /**
     * The split currently set by the tree widget.
     */
    int currentSplit;

    /**
     * The min required split of this item.
     */
    int minRequiredSplit;

    /**
     * The max required split of this item. Includes all visible sub-items.
     */
    int maxRequiredSplit;
};

/**
 * A custom @c QVBoxLayout that is responsible
 * to correctly lay out the property tree items.
 * It calculates, where the split between property labels and
 * editors should be placed, and sets that for each property tree item.
 */
class MPropertyTree::ViewLayout : public QVBoxLayout
{
public:
    explicit ViewLayout(QWidget *parent = nullptr);

    void setGeometry(const QRect &rect) override;

    /**
     * Adds a new property tree item to the layout.
     * @param item The item to add.
     */
    void addTreeItem(MPropertyTree::Item *item);

    /**
     * Inserts a new property tree item to the layout at the given index.
     * If the index is negative, it will be added to the end.
     * @param index The index, where the item is inserted.
     * @param item The item to add.
     */
    void insertTreeItem(int index, MPropertyTree::Item *item);

    /**
     * Inserts a new property tree item to the layout after another item already
     * present in the layout.
     * If the other item is not present, the new item will be added to the end.
     * @param before The item before / above the newly added one.
     * @param item The new item.
     */
    void insertTreeItemAfter(MPropertyTree::Item *before, MPropertyTree::Item *item);

    /**
     * Removes a property tree item from the layout.
     * @param item The item to remove.
     */
    void removeTreeItem(MPropertyTree::Item *item);

    /**
     * @return All property tree items added to this layout.
     */
    const QVector<MPropertyTree::Item *> &getItems() const { return items; }

protected:
    /**
     * A list of all @c Item's currently in the layout.
     */
    QVector<MPropertyTree::Item *> items;

    int currentSplit;
};


/**
 * The container widget for the property tree,
 * that resides within the scroll area and holds
 * all the widgets for the different properties.
 */
class MPropertyTree::PropertyView : public QFrame
{
Q_OBJECT

public:
    explicit PropertyView(QWidget *parent = nullptr);

    /**
     * Adds a property tree item to the tree view.
     * @param item The item to add to the tree.
     */
    void addPropertyTreeItem(MPropertyTree::Item *item) const;

    /**
     * Inserts a property tree item to the tree view at the given index.
     * @param index The index to insert the property at.
     * @param item The item to add to the tree.
     */
    void insertPropertyTreeItem(int index, MPropertyTree::Item *item) const;

    /**
     * Inserts a property tree item to the tree view after another item already
     * present in the tree.
     * If the @p before item is not present in the tree, @p item will be added
     * to the end.
     * @param before The item already in the tree, that should be above / before
     * the new item.
     * @param item The item to add to the tree.
     */
    void insertPropertyTreeItemAfter(MPropertyTree::Item *before,
                                     MPropertyTree::Item *item) const;

    /**
     * Removes an item from the property tree.
     * @param item The item to remove.
     */
    void removePropertyTreeItem(MPropertyTree::Item *item) const;

    /**
     * Vertical layout of the tree view, containing all top level items.
     */
    QVBoxLayout *layout;

    /**
     * Vertical layout for the property label column.
     */
    MPropertyTree::ViewLayout *propertyLayout;
};

} // Met3D

#endif //MET_3D_MPROPERTYTREE_H
