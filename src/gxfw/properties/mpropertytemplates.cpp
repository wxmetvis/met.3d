/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mpropertytemplates.h"

// standard library imports

// related third party imports

// local application imports

namespace Met3D
{
MVector3DProperty MPropertyTemplates::GeographicPosition3D(const QString &name,
                                                           const QVector3D &defaultValue)
{
    MVector3DProperty p = MVector3DProperty(name, defaultValue);
    p.getXEditor()->setName("Longitude");
    p.getXEditor()->setSuffix(" °");
    p.getYEditor()->setName("Latitude");
    p.getYEditor()->setSuffix(" °");
    p.getZEditor()->setName("Elevation");
    p.getZEditor()->setSuffix(" hPa");
    p.getZEditor()->setMinMax(0.1f, 1050.0f);
    p.setShowSuffixInPreview(true);
    return p;
}


MPointFProperty MPropertyTemplates::GeographicPosition2D(const QString &name,
                                                         const QPointF &defaultValue)
{
    MPointFProperty p = MPointFProperty(name, defaultValue);
    p.setSuffix(" °");
    p.setXLabel("Longitude");
    p.setYLabel("Latitude");
    p.setShowSuffixInPreview(true);
    return p;
}


MPointFProperty MPropertyTemplates::ViewportPosition2D(const QString &name,
                                                       const QPointF &defaultValue)
{
    MPointFProperty p = MPointFProperty(name, defaultValue);
    p.setSuffix(" (-1..1)");
    p.setMinMax(-1, 1);
    p.setStep(0.1);
    p.setDecimals(3);
    return p;
}


MPointFProperty MPropertyTemplates::ViewportSize(const QString &name,
                                                 const QPointF &defaultValue)
{
    MPointFProperty p = MPointFProperty(name, defaultValue);
    p.setSuffix(" (-1..1)");
    p.setMinMax(-1, 1);
    p.setXLabel("Width");
    p.setYLabel("Height");

    return p;
}


MPropertyTemplates::VerticalExtent::VerticalExtent(float bottom, float top)
{
    groupProp = MProperty("Vertical extent");

    bottomProp = MFloatProperty("Bottom", bottom);
    groupProp.addSubProperty(bottomProp);

    topProp = MFloatProperty("Top", top);
    groupProp.addSubProperty(topProp);
}


MPropertyTemplates::VerticalExtentHPa::VerticalExtentHPa(float bottom, float top)
    : VerticalExtent(bottom, top)
{
    bottomProp.setMinMax(0.01f, 1050.f);
    bottomProp.setDecimals(2);
    bottomProp.setStep(10.);
    bottomProp.setSuffix(" hPa");

    topProp.setMinMax(0.01f, 1050.f);
    topProp.setDecimals(2);
    topProp.setStep(10.);
    topProp.setSuffix(" hPa");
}


MPropertyTemplates::VerticalExtentM::VerticalExtentM(float bottom, float top)
    : VerticalExtent(bottom, top)
{
    bottomProp.setDecimals(2);
    bottomProp.setStep(1000.);
    bottomProp.setSuffix(" m");

    topProp.setDecimals(2);
    topProp.setStep(1000.);
    topProp.setSuffix(" m");
}


MPropertyTemplates::Labels::Labels(bool enabled, int fontSize, const QColor& fontColour, bool enableBBox, const QColor& bboxColour)
{
    enabledProp = MBoolProperty("Labels", enabled);

    fontSizeProp = MIntProperty("Font size", fontSize);
    fontSizeProp.setMinimum(1);
    enabledProp.addSubProperty(fontSizeProp);

    fontColourProp = MColorProperty("Font colour", fontColour);
    enabledProp.addSubProperty(fontColourProp);

    enableBBoxProp = MBoolProperty("Background", enableBBox);
    enabledProp.addSubProperty(enableBBoxProp);

    bboxColourProp = MColorProperty("Background colour", bboxColour);
    enabledProp.addSubProperty(bboxColourProp);
}
} // Met3D