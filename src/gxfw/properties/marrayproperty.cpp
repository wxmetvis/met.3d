/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "marrayproperty.h"

// standard library imports

// related third party imports
#include <QLabel>

// local application imports

namespace Met3D
{
MArrayProperty::MArrayProperty(const QString &name)
        : MProperty(new MArrayProperty::Data(name))
{
}


MArrayProperty::MArrayProperty(const MArrayProperty &other)
    : MProperty(new MArrayProperty::Data(""))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MArrayProperty::append(MProperty *property)
{
    auto *ad = getData<MArrayProperty::Data>();
    ad->properties.append(property);
    connect(property, &MProperty::nameChanged,
            this, &MArrayProperty::onPropertyRenamed);

    property->setParent(this);

    for (MProperty::Editor *editor : d->editors)
    {
        dynamic_cast<MArrayProperty::Editor *>(editor)->propertyAdded(property);
    }
}


void MArrayProperty::remove(MProperty *property)
{
    auto *ad = getData<MArrayProperty::Data>();
    ad->properties.removeAll(property);
    disconnect(property, &MProperty::nameChanged,
               this, &MArrayProperty::onPropertyRenamed);

    for (MProperty::Editor *editor : d->editors)
    {
        dynamic_cast<MArrayProperty::Editor *>(editor)
                ->propertyRemoved(property);
    }
}


int MArrayProperty::length() const
{
    auto *ad = getData<MArrayProperty::Data>();
    return ad->properties.length();
}


MProperty *MArrayProperty::operator[](int idx)
{
    auto *ad = getData<MArrayProperty::Data>();
    return ad->properties[idx];
}


const MProperty *MArrayProperty::operator[](int idx) const
{
    auto *ad = getData<MArrayProperty::Data>();
    return ad->properties[idx];
}


MProperty *MArrayProperty::at(int idx) const
{
    auto *ad = getData<MArrayProperty::Data>();
    return ad->properties.at(idx);
}


const QVector<MProperty *> &MArrayProperty::getProperties()
{
    auto *ad = getData<MArrayProperty::Data>();
    return ad->properties;
}


void MArrayProperty::setShowPropertyLabels(bool show)
{
    auto *ad = getData<MArrayProperty::Data>();
    ad->showLabels = show;
    updateEditorSettings();
}


bool MArrayProperty::showPropertyLabels() const
{
    auto *ad = getData<MArrayProperty::Data>();
    return ad->showLabels;
}


MArrayProperty &MArrayProperty::operator=(const MArrayProperty &other)
{
    MProperty::operator=(other);

    auto *otherData = getData<MArrayProperty::Data>();
    setShowPropertyLabels(otherData->showLabels);

    for (MProperty *prop : otherData->properties)
    {
        append(prop);
    }

    return *this;
}


void MArrayProperty::onPropertyRenamed()
{
    updateEditorValues();
}


void MArrayProperty::saveToConfiguration(QSettings *settings) const
{
    auto *ad = getData<MArrayProperty::Data>();
    for (MProperty *prop : ad->properties)
    {
        prop->saveAllToConfiguration(settings);
    }
}


void MArrayProperty::loadFromConfiguration(QSettings *settings)
{
    auto ad = getData<MArrayProperty::Data>();
    for (MProperty *prop : ad->properties)
    {
        prop->loadAllFromConfiguration(settings);
    }
}


MProperty::Editor *MArrayProperty::createEditor(QWidget *parentWidget)
{
    return new MArrayProperty::Editor(parentWidget, this);
}


bool MArrayProperty::canSaveToConfig() const
{
    return true;
}


MArrayProperty::Editor::Editor(QWidget *parent,
                               MArrayProperty *property)
        : MProperty::Editor(parent, property)
{
    for (MProperty *prop : property->getProperties())
    {
        propertyAdded(prop);
    }
}


void MArrayProperty::Editor::updateSize(int height)
{
    for (MProperty::Editor *editor : editors)
    {
        editor->updateSize(height);
    }
}


void MArrayProperty::Editor::updateSettings()
{
    for (QLabel *label : labels)
    {
        label->setHidden(!dynamic_cast<MArrayProperty *>(property)
                ->showPropertyLabels());
    }
}


void MArrayProperty::Editor::updateValue()
{
    for (QLabel *label : labels)
    {
        label->setText(labels.key(label)->getName());
    }
}


void MArrayProperty::Editor::propertyAdded(MProperty *prop)
{
    MProperty::Editor *editor = prop->createEditorWidget(this);
    editor->setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Fixed);
    editors.insert(prop, editor);

    auto *label = new QLabel(prop->getName());
    labels.insert(prop, label);

    layout->addWidget(label);
    layout->addWidget(editor);

    if (!dynamic_cast<MArrayProperty *>(property)->showPropertyLabels())
    {
        label->setHidden(true);
    }
}


void MArrayProperty::Editor::propertyRemoved(MProperty *prop)
{
    layout->removeWidget(editors[prop]);
    layout->removeWidget(labels[prop]);

    editors[prop]->deleteLater();
    editors.remove(prop);

    delete labels[prop];
    labels.remove(prop);
}


} // Met3D