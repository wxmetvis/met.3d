/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MBUTTONPROPERTY_H
#define MET_3D_MBUTTONPROPERTY_H

// standard library imports
#include <utility>

// related third party imports

// local application imports
#include "mproperty.h"

// forward declarations
class QPushButton;

namespace Met3D
{

/**
 * A simple button property, that emits @c valueChanged() when the button is pressed.
 */
class MButtonProperty : public MProperty
{
public:
    MButtonProperty();
    MButtonProperty(const QString &name, QString buttonText);

    MButtonProperty(const MButtonProperty &other);

    void setButtonText(const QString &text);

    const QString &getButtonText() const;

    MButtonProperty &operator=(const MButtonProperty &other);

protected:
    /**
     * Declaration of editor class, so
     * that it is no longer visible to the
     * outside.
     */
    class Editor;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MProperty::Data
    {
        /**
         * The text on the button, default is "execute".
         */
        QString buttonText;


        Data(const QString &name, QString buttonText)
                : MProperty::Data(name),
                  buttonText(std::move(buttonText))
        {}
    };
};

/**
 * The corresponding editor widget for the @c MButtonProperty.
 */
class MButtonProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MButtonProperty *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateSettings() override;

private:
    QPushButton *button;
};

} // Met3D

#endif //MET_3D_MBUTTONPROPERTY_H
