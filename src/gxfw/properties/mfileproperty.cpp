/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mfileproperty.h"

// standard library imports

// related third party imports
#include <QLineEdit>
#include <QToolButton>

// local application imports
#include "gxfw/msystemcontrol.h"
#include "util/mutil.h"

namespace Met3D
{
MFileProperty::MFileProperty()
        : MValueProperty<QString>(new MFileProperty::Data("", ""))
{}


MFileProperty::MFileProperty(const QString &name, const QString &defaultPath)
        : MValueProperty<QString>(new MFileProperty::Data(name, defaultPath))
{}


MFileProperty::MFileProperty(const MFileProperty &other)
        : MValueProperty<QString>(
        new MFileProperty::Data(other.getName(), other.getDefaultValue()))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MFileProperty::setValue(const QString &value)
{
    auto *data = getData<MFileProperty::Data>();

    QFileInfo fInfo(value);
    if (!fInfo.exists() || !fInfo.isFile()) return;

    // Check if new value fits filter.
    for (const MFileType &t : data->fileTypes)
    {
        if (t.isType(value))
        {
            MValueProperty::setValue(value);
            return;
        }
    }
}


void MFileProperty::setFileFilters(const QVector<MFileType> &fileFilters)
{
    auto *data = getData<MFileProperty::Data>();
    data->fileTypes = fileFilters;
    // Set value again, so it is checked against the new filters.
    setValue(propertyValue);
    updateEditorSettings();
}


void MFileProperty::setFileFilter(const MFileType &fileFilter)
{
    setFileFilters({fileFilter});
}


const QVector<MFileType> &MFileProperty::getFileFilters() const
{
    auto *data = getData<MFileProperty::Data>();
    return data->fileTypes;
}


void MFileProperty::setDialogDirectory(const QString &directory)
{
    auto *data = getData<MFileProperty::Data>();
    data->defaultDirectory = directory;
    updateEditorSettings();
}


const QString &MFileProperty::getDialogDirectory() const
{
    auto *data = getData<MFileProperty::Data>();
    return data->defaultDirectory;
}


void MFileProperty::setDialogTitle(const QString &title)
{
    auto *data = getData<MFileProperty::Data>();
    data->fileDialogTitle = title;
    updateEditorSettings();
}


const QString &MFileProperty::getDialogTitle() const
{
    auto *data = getData<MFileProperty::Data>();
    return data->fileDialogTitle;
}


MFileProperty &MFileProperty::operator=(const MFileProperty &other)
{
    MValueProperty::operator=(other);

    setFileFilters(other.getFileFilters());

    return *this;
}


void MFileProperty::saveToConfiguration(QSettings *settings) const
{
    QDir baseDir = MSystemManagerAndControl::getInstance()->getMet3DBaseDir();

    QString relPath = baseDir.relativeFilePath(propertyValue);
    QString path = "$MET3D_BASE/" + relPath;

    settings->setValue(getConfigKey(), path);
}


void MFileProperty::loadFromConfiguration(QSettings *settings)
{
    QDir baseDir = MSystemManagerAndControl::getInstance()->getMet3DBaseDir();

    QString value = settings->value(getConfigKey(),
                                    propertyValue).toString();

    value = expandEnvironmentVariables(value);

    if (baseDir.exists(value))
    {
        value = baseDir.absoluteFilePath(value);
    }

    MValueProperty<QString>::setValue(value);
}


MProperty::Editor *MFileProperty::createEditor(QWidget *parentWidget)
{
    return new MFileProperty::Editor(parentWidget, this);
}


MFileProperty::Editor::Editor(QWidget *parent, MFileProperty *property)
        : MProperty::Editor(parent, property)
{
    lineEdit = new QLineEdit(this);
    lineEdit->setText(property->value());
    lineEdit->setReadOnly(true);
    lineEdit->setAlignment(Qt::AlignLeft);
    QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    sizePolicy.setHorizontalStretch(10);
    lineEdit->setSizePolicy(sizePolicy);
    lineEdit->setCursorPosition(0);

    QFont font = lineEdit->font();
    font.setCapitalization(QFont::Capitalization::MixedCase);
    lineEdit->setFont(font);

    toolButton = new QToolButton();
    toolButton->setText("...");

    layout->addWidget(lineEdit);
    layout->addWidget(toolButton);

    connect(lineEdit, &QLineEdit::editingFinished, this,
            &MFileProperty::Editor::onEditingFinished);
    connect(toolButton, &QToolButton::clicked, this,
            &MFileProperty::Editor::onOpenFileDialog);
}


MFileProperty::Editor::~Editor()
= default;


void MFileProperty::Editor::updateSize(int height)
{
    lineEdit->setFixedHeight(height);
    toolButton->setFixedHeight(height);
}


void MFileProperty::Editor::updateValue()
{
    auto prop = dynamic_cast<MFileProperty *>(property);
    bool b = lineEdit->blockSignals(true);
    lineEdit->setText(prop->value());
    lineEdit->setCursorPosition(0);
    lineEdit->blockSignals(b);
}


void MFileProperty::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding,
                               QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(10);
        lineEdit->setSizePolicy(sizePolicy);
    }
    else
    {
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(10);
        lineEdit->setSizePolicy(sizePolicy);
    }
}


void MFileProperty::Editor::onEditingFinished()
{
    QString text = lineEdit->text();
    auto *prop = dynamic_cast<MFileProperty *>(property);

    if (text != prop->value())
    {
        prop->setUndoableValue(text);
    }
}


void MFileProperty::Editor::onOpenFileDialog()
{
    auto prop = dynamic_cast<MFileProperty *>(property);
    QString file = MFileUtils::getOpenFileName(nullptr, prop->getDialogTitle(),
                                               prop->getFileFilters(),
                                               prop->getDialogDirectory());

    if (file.isEmpty()) return;

    prop->setValue(file);
}
} // Met3D