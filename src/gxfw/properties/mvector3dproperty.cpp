/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mvector3dproperty.h"

// standard library imports

// related third party imports
#include <QLabel>

// local application imports

namespace Met3D
{
MVector3DProperty::MVector3DProperty()
        : MValueProperty<QVector3D>(new MVector3DProperty::Data("", {}))
{
    auto *data = getData<MVector3DProperty::Data>();
    data->xProp = createSubProperty<MFloatProperty>("X", propertyValue.x());
    data->yProp = createSubProperty<MFloatProperty>("Y", propertyValue.y());
    data->zProp = createSubProperty<MFloatProperty>("Z", propertyValue.z());

    data->xProp
        ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
    data->yProp
        ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
    data->zProp
        ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
}


MVector3DProperty::MVector3DProperty(const QString &name,
                                     const QVector3D &defaultValue)
        : MValueProperty<QVector3D>(new MVector3DProperty::Data(name, defaultValue))
{
    auto *data = getData<MVector3DProperty::Data>();
    data->xProp = createSubProperty<MFloatProperty>("X", defaultValue.x());
    data->yProp = createSubProperty<MFloatProperty>("Y", defaultValue.y());
    data->zProp = createSubProperty<MFloatProperty>("Z", defaultValue.z());

    data->xProp
         ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
    data->yProp
         ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
    data->zProp
         ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
}


MVector3DProperty::MVector3DProperty(const MVector3DProperty &other)
    : MValueProperty<QVector3D>(new MVector3DProperty::Data("", {}))
{
    auto *otherData = other.getData<MVector3DProperty::Data>();
    auto *data = getData<MVector3DProperty::Data>();

    data->xProp = createSubProperty<MFloatProperty>("X", otherData->defaultValue.x());
    data->yProp = createSubProperty<MFloatProperty>("Y", otherData->defaultValue.y());
    data->zProp = createSubProperty<MFloatProperty>("Z", otherData->defaultValue.z());

    data->xProp
        ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
    data->yProp
        ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);
    data->zProp
        ->registerValueCallback(this, &MVector3DProperty::onVectorChanged);

    // Run assignment operator to copy the other property into this one.
    *this = other;
}


MVector3DProperty::~MVector3DProperty()
{
    auto *data = getData<MVector3DProperty::Data>();

    delete data->xProp;
    delete data->yProp;
    delete data->zProp;
}


void MVector3DProperty::setValue(const QVector3D &value)
{
    auto *data = getData<MVector3DProperty::Data>();

    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);
    data->zProp->suppressValueEvent(true);

    data->xProp->setValue(value.x());
    data->yProp->setValue(value.y());
    data->zProp->setValue(value.z());

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);
    data->zProp->suppressValueEvent(false);

    // Get corrected value
    QVector3D v;
    v.setX(data->xProp->value());
    v.setY(data->yProp->value());
    v.setZ(data->zProp->value());

    MValueProperty<QVector3D>::setValue(v);
}


void MVector3DProperty::setX(float x)
{
    auto *data = getData<MVector3DProperty::Data>();
    data->xProp->setValue(x);
}


void MVector3DProperty::setY(float y)
{
    auto *data = getData<MVector3DProperty::Data>();
    data->yProp->setValue(y);
}


void MVector3DProperty::setZ(float z)
{
    auto *data = getData<MVector3DProperty::Data>();
    data->zProp->setValue(z);
}


void MVector3DProperty::onVectorChanged()
{
    auto *data = getData<MVector3DProperty::Data>();

    QVector3D v;
    v.setX(data->xProp->value());
    v.setY(data->yProp->value());
    v.setZ(data->zProp->value());

    MValueProperty<QVector3D>::setValue(v);
}


void MVector3DProperty::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(getConfigKey(), propertyValue);
}


void MVector3DProperty::loadFromConfiguration(QSettings *settings)
{
    setValue(settings->value(getConfigKey(), getDefaultValue())
                     .value<QVector3D>());
}


MProperty::Editor *MVector3DProperty::createEditor(QWidget *parentWidget)
{
    return new MVector3DProperty::Editor(parentWidget, this);
}


MFloatProperty *MVector3DProperty::getXEditor()
{
    auto *data = getData<MVector3DProperty::Data>();

    return data->xProp;
}


MFloatProperty *MVector3DProperty::getYEditor()
{
    auto *data = getData<MVector3DProperty::Data>();
    return data->yProp;
}


MFloatProperty *MVector3DProperty::getZEditor()
{
    auto *data = getData<MVector3DProperty::Data>();
    return data->zProp;
}


void MVector3DProperty::setShowSuffixInPreview(bool show)
{
    auto *data = getData<MVector3DProperty::Data>();
    data->showSuffixInPreview = show;
    updateEditorSettings();
}


bool MVector3DProperty::showSuffixInPreview() const
{
    auto *data = getData<MVector3DProperty::Data>();
    return data->showSuffixInPreview;
}


MVector3DProperty &MVector3DProperty::operator=(const Met3D::MVector3DProperty &other)
{
    MValueProperty::operator=(other);

    auto *data = getData<MVector3DProperty::Data>();
    auto *otherData = other.getData<MVector3DProperty::Data>();

    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);
    data->zProp->suppressValueEvent(true);

    *data->xProp = *otherData->xProp;
    *data->yProp = *otherData->yProp;
    *data->zProp = *otherData->zProp;

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);
    data->zProp->suppressValueEvent(false);

    data->showSuffixInPreview = otherData->showSuffixInPreview;

    onVectorChanged();
    updateEditorSettings();

    return *this;
}


MVector3DProperty::Editor::Editor(QWidget *parent,
                                  MVector3DProperty *property)
        : MProperty::Editor(parent, property)
{
    valueLabel = new QLabel();
    updateText();

    layout->addWidget(valueLabel);
}


void MVector3DProperty::Editor::updateSize(int height)
{
    valueLabel->setFixedHeight(height);
}


void MVector3DProperty::Editor::updateValue()
{
    updateText();
}


void MVector3DProperty::Editor::updateSettings()
{
    updateText();
}


void MVector3DProperty::Editor::updateText()
{
    auto *prop = dynamic_cast<MVector3DProperty *>(property);
    bool s = prop->showSuffixInPreview();

    static QString format = "[%1%2,%3%4,%5%6]";

    valueLabel->setText(format.arg(prop->value().x()).arg(s ? prop->getXEditor()->getSuffix() : "")
                              .arg(prop->value().y()).arg(s ? prop->getYEditor()->getSuffix() : "")
                              .arg(prop->value().z()).arg(s ? prop->getZEditor()->getSuffix() : ""));
}

} // Met3D