/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mpointfproperty.h"

// standard library imports

// related third party imports
#include <QLabel>
#include <QDoubleSpinBox>

// local application imports

namespace Met3D
{
MPointFProperty::MPointFProperty()
        : MValueProperty<QPointF>(new MPointFProperty::Data("", {}))
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp = createSubProperty<MDoubleProperty>("X", propertyValue.x());
    data->yProp = createSubProperty<MDoubleProperty>("Y", propertyValue.y());

    data->xProp->registerValueCallback(this, &MPointFProperty::onPointChanged);
    data->yProp->registerValueCallback(this, &MPointFProperty::onPointChanged);
}

MPointFProperty::MPointFProperty(const QString &name, const QPointF &value)
        : MValueProperty<QPointF>(new MPointFProperty::Data(name, value))
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp = createSubProperty<MDoubleProperty>("X", propertyValue.x());
    data->yProp = createSubProperty<MDoubleProperty>("Y", propertyValue.y());

    data->xProp->registerValueCallback(this, &MPointFProperty::onPointChanged);
    data->yProp->registerValueCallback(this, &MPointFProperty::onPointChanged);
}


MPointFProperty::MPointFProperty(const MPointFProperty &other)
    : MValueProperty<QPointF>(new MPointFProperty::Data("", {}))
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp = createSubProperty<MDoubleProperty>("X", propertyValue.x());
    data->yProp = createSubProperty<MDoubleProperty>("Y", propertyValue.y());

    data->xProp->registerValueCallback(this, &MPointFProperty::onPointChanged);
    data->yProp->registerValueCallback(this, &MPointFProperty::onPointChanged);

    // Run assignment operator to copy the other property into this one.
    *this = other;
}


MPointFProperty::~MPointFProperty()
{
    auto *data = getData<MPointFProperty::Data>();

    delete data->xProp;
    delete data->yProp;
}


void MPointFProperty::setValue(const QPointF &value)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);

    data->xProp->setValue(value.x());
    data->yProp->setValue(value.y());

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);

    onPointChanged();
}


void MPointFProperty::setXLabel(const QString &label)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->setName(label);
}


const QString &MPointFProperty::getXLabel() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->xProp->getName();
}


void MPointFProperty::setYLabel(const QString &label)
{
    auto *data = getData<MPointFProperty::Data>();
    data->yProp->setName(label);
}


const QString &MPointFProperty::getYLabel() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->yProp->getName();
}


void MPointFProperty::setPrefix(const QString &label)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->setPrefix(label);
    data->yProp->setPrefix(label);
}


const QString &MPointFProperty::getPrefix() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->xProp->getPrefix();
}


void MPointFProperty::setSuffix(const QString &label)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->setSuffix(label);
    data->yProp->setSuffix(label);
}


const QString &MPointFProperty::getSuffix() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->xProp->getSuffix();
}


void MPointFProperty::setShowSuffixInPreview(bool show)
{
    auto *data = getData<MPointFProperty::Data>();
    data->showSuffixInPreview = show;
    updateEditorSettings();
}


bool MPointFProperty::showSuffixInPreview() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->showSuffixInPreview;
}


void MPointFProperty::setMinimum(const double &value)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);

    data->xProp->setMinimum(value);
    data->yProp->setMinimum(value);

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);

    onPointChanged();
}


double MPointFProperty::getMinimum() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->xProp->getMinimum();
}


void MPointFProperty::setMaximum(const double &value)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);

    data->xProp->setMaximum(value);
    data->yProp->setMaximum(value);

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);

    onPointChanged();
}


double MPointFProperty::getMaximum() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->xProp->getMaximum();
}


void MPointFProperty::setMinMax(const double &minValue, const double &maxValue)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);

    data->xProp->setMinMax(minValue, maxValue);
    data->yProp->setMinMax(minValue, maxValue);

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);

    onPointChanged();
}


void MPointFProperty::setStep(const double &value)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->setStep(value);
    data->yProp->setStep(value);
    updateEditorSettings();
}


double MPointFProperty::getStep() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->xProp->getStep();
}


void MPointFProperty::setDecimals(const int &value)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);

    data->xProp->setDecimals(value);
    data->yProp->setDecimals(value);

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);

    onPointChanged();
}


int MPointFProperty::getDecimals() const
{
    auto *data = getData<MPointFProperty::Data>();
    return data->xProp->getDecimals();
}


void MPointFProperty::setX(qreal x)
{
    auto *data = getData<MPointFProperty::Data>();
    data->xProp->setValue(x);
}


void MPointFProperty::setY(qreal y)
{
    auto *data = getData<MPointFProperty::Data>();
    data->yProp->setValue(y);
}


MPointFProperty &MPointFProperty::operator=(const Met3D::MPointFProperty &other)
{
    MValueProperty::operator=(other);

    auto *data = getData<MPointFProperty::Data>();
    auto *otherData = other.getData<MPointFProperty::Data>();

    data->xProp->suppressValueEvent(true);
    data->yProp->suppressValueEvent(true);

    *data->xProp = *otherData->xProp;
    *data->yProp = *otherData->yProp;

    data->xProp->suppressValueEvent(false);
    data->yProp->suppressValueEvent(false);

    data->showSuffixInPreview = otherData->showSuffixInPreview;

    onPointChanged();
    updateEditorSettings();

    return *this;
}


void MPointFProperty::onPointChanged()
{
    auto *data = getData<MPointFProperty::Data>();

    QPointF p;
    p.setX(data->xProp->value());
    p.setY(data->yProp->value());

    MValueProperty<QPointF>::setValue(p);
}


void MPointFProperty::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(getConfigKey(), propertyValue);
}


void MPointFProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MPointFProperty::Data>();
    setValue(settings->value(getConfigKey(), data->defaultValue).toPointF());
}


MProperty::Editor *MPointFProperty::createEditor(QWidget *parentWidget)
{
    return new MPointFProperty::Editor(parentWidget, this);
}


MPointFProperty::Editor::Editor(QWidget *parent, MPointFProperty *property)
        : MProperty::Editor(parent, property)
{
    valueLabel = new QLabel();
    updateText();

    layout->addWidget(valueLabel);
}


void MPointFProperty::Editor::updateSize(int height)
{
    valueLabel->setFixedHeight(height);
}


void MPointFProperty::Editor::updateValue()
{
    updateText();
}


void MPointFProperty::Editor::updateText()
{
    auto *prop = dynamic_cast<MPointFProperty *>(property);
    bool s = prop->showSuffixInPreview();

    static QString format = "[%1%2,%3%4]";

    valueLabel->setText(format.arg(prop->value().x()).arg(s ? prop->getSuffix() : "")
                              .arg(prop->value().y()).arg(s ? prop->getSuffix() : ""));
}

} // Met3D