/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPOINTFPROPERTY_H
#define MET_3D_MPOINTFPROPERTY_H

// standard library imports

// related third party imports
#include <QPointF>

// local application imports
#include "mproperty.h"
#include "mnumberproperty.h"

// forward declarations
class QLabel;
class QDoubleSpinBox;

namespace Met3D
{

/**
 * Property that holds a @c QPointF.
 */
class MPointFProperty : public MValueProperty<QPointF>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<QPointF>::operator QPointF;
    using MValueProperty<QPointF>::operator const QPointF &;
    using MValueProperty<QPointF>::operator=;

    MPointFProperty();
    MPointFProperty(const QString &name, const QPointF &value);
    MPointFProperty(const MPointFProperty &other);
    ~MPointFProperty();

    void setValue(const QPointF &value) override;

    /**
     * Set the label for the x coordinate of the point, which is displayed
     * in the editor of this property.
     * @param label The Label for the x coordinate.
     */
    void setXLabel(const QString &label);

    /**
     * @return The label for the x coordinate of this property.
     */
    const QString &getXLabel() const;

    /**
     * Set the label for the y coordinate of the point, which is displayed
     * in the editor of this property.
     * @param label The Label for the y coordinate.
     */
    void setYLabel(const QString &label);

    /**
     * @return The label for the y coordinate of this property.
     */
    const QString &getYLabel() const;

    /**
     * Set the prefix for both x and y coordinate of this property for its editor.
     * @param prefix Prefix.
     */
    void setPrefix(const QString &label);

    /**
     * @return The prefix of both coordinates.
     */
    const QString &getPrefix() const;

    /**
     * Set the suffix for both x and y coordinate of this property for its editor.
     * @param suffix The suffix to set for the editor.
     */
    void setSuffix(const QString &label);

    /**
     * @return The suffix of both coordinates.
     */
    const QString &getSuffix() const;

    /**
     * Toggles the suffix display in the preview string on and off.
     * @param show Whether to show or not. Default false.
     */
    void setShowSuffixInPreview(bool show);

    /**
     * @return Whether the suffix of the different axis is shown in the preview.
     */
    bool showSuffixInPreview() const;

    /**
     * Sets the minimum value of this property.
     * This is set for both x and y coordinate of the point.
     * If the minimum is larger than the maximum, the maximum will be adjusted
     * to fit the new minimum.
     * @param minimum The new minimum value.
     */
    void setMinimum(const double &value);

    /**
     * @return The minimum value for the coordinates.
     */
    double getMinimum() const;

    /**
     * Sets the maximum value of this property.
     * This is set for both x and y coordinate of the point.
     * If the maximum is smaller than the minimum, the minimum will be adjusted
     * to fit the new maximum.
     * @param maximum The new maximum value.
     */
    void setMaximum(const double &value);

    /**
     * @return The maximum value for the coordinates.
     */
    double getMaximum() const;

    /**
     * Sets both minimum and maximum.
     * This is set for both x and y coordinate of the point.
     * If they are reversed, the maximum and minimum will be swapped.
     * @param minimum The new minimum value of this property.
     * @param maximum The new maximum value of this property.
     */
    void setMinMax(const double &minValue, const double &maxValue);

    /**
     * Set the step for the coordinate editors.
     * @param value The step value.
     */
    void setStep(const double &value);

    /**
     * @return The step value for both x and y coordinate editors.
     */
    double getStep() const;

    /**
     * Set the amount of decimals that this property should have.
     * This is set for both x and y coordinate of the point.
     * Only used in its editor.
     * @param decimals Number of visible decimals.
     */
    void setDecimals(const int &value);

    /**
     * @return The decimals for both x and y coordinate.
     */
    int getDecimals() const;

    /**
     * Set the x value of this property.
     * @param x The new x value.
     */
    void setX(qreal x);

    /**
     * Set the y value of this property.
     * @param y The new y value.
     */
    void setY(qreal y);

    /**
     * @return the x value of the underlying @c QPointF.
     */
    constexpr inline qreal x() const { return propertyValue.x(); }

    /**
     * @return the y value of the underlying @c QPointF.
     */
    constexpr inline qreal y() const { return propertyValue.y(); }

    MPointFProperty &operator=(const MPointFProperty &other);

public slots:

    /**
     * Called by the sub properties, when their value changes.
     */
    void onPointChanged();

protected:
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty::Data
    {
        MDoubleProperty *xProp;
        MDoubleProperty *yProp;

        bool showSuffixInPreview;

        Data(const QString &name, const QPointF &defaultValue)
                : MValueProperty<QPointF>::Data(name, defaultValue),
                  xProp(nullptr),
                  yProp(nullptr),
                  showSuffixInPreview(false)
        {}
    };
};


/**
 * The editor for the @c QPointF property
 */
class MPointFProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MPointFProperty *property);

    void updateSize(int height) override;

    void updateValue() override;

    void updateText();

protected:
    QLabel *valueLabel;
};

} // Met3D

#endif //MET_3D_MPOINTFPROPERTY_H
