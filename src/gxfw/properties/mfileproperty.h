/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MFILEPROPERTY_H
#define MET_3D_MFILEPROPERTY_H

// standard library imports

// related third party imports
#include <QString>
#include <QVector>

// local application imports
#include "mproperty.h"
#include "util/mfileutils.h"

// forward declarations
class QLineEdit;

class QToolButton;

namespace Met3D
{

/**
 * A property, that holds the path to a file and allows the user
 * to open a file dialog to choose another file.
 * The property can only hold specific types of files, which need to be set
 * explicitly. If no file type is set, all files are accepted.
 */
class MFileProperty : public MValueProperty<QString>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<QString>::operator QString;
    using MValueProperty<QString>::operator const QString &;
    using MValueProperty<QString>::operator=;

    MFileProperty();
    MFileProperty(const QString &name, const QString &defaultPath);
    MFileProperty(const MFileProperty &other);

    void setValue(const QString &value) override;

    /**
     * Sets the allowed file types that this property can hold
     * and allows the user to select.
     * @param fileFilters The file types.
     */
    void setFileFilters(const QVector<MFileType> &fileFilters);

    /**
     * Sets the allowed file type that this property can hold
     * and allows the user to select.
     * @param fileFilters The file type.
     */
    void setFileFilter(const MFileType &fileFilter);

    /**
     * @return The allowed file types, that this property can hold.
     */
    const QVector<MFileType> &getFileFilters() const;

    /**
     * Set the opening directory of the file dialog, which the user can open
     * to choose a file.
     * @param directory The directory.
     */
    void setDialogDirectory(const QString &directory);

    /**
     * @return The directory that the file dialog opens by default.
     */
    const QString &getDialogDirectory() const;

    /**
     * Sets the window title of the file dialog the user can open.
     * @param title The title.
     */
    void setDialogTitle(const QString &title);

    /**
     * @return The file dialog title.
     */
    const QString &getDialogTitle() const;

    MFileProperty &operator=(const MFileProperty &other);

protected:
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<QString>::Data
    {
        /**
         * The allowed file types.
         */
        QVector<MFileType> fileTypes;

        /**
         * The default directory to open the file dialog in.
         */
        QString defaultDirectory;

        /**
         * The title of the file dialog.
         */
        QString fileDialogTitle;


        Data(const QString &name, const QString &defaultValue)
                : MValueProperty<QString>::Data(name, defaultValue),
                  fileTypes({MFileType::getWildcard()}),
                  defaultDirectory(),
                  fileDialogTitle("Open file")
        {}
    };
};


/**
 * The file property editor. It shows a line edit to preview the file path and a
 * button to choose a file.
 */
class MFileProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MFileProperty *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateValue() override;

    void setExpanding(bool expanding) override;

public slots:

    /**
     * Slot called from editor, when the string was edited.
     */
    void onEditingFinished();

    /**
     * Slot called from editor, when the file dialog should be opened.
     */
    void onOpenFileDialog();

protected:

    /**
     * The line edit used to show and edit the property value.
     */
    QLineEdit *lineEdit;

    /**
     * The tool button to open a file dialog.
     */
    QToolButton *toolButton;
};

} // Met3D

#endif //MET_3D_MFILEPROPERTY_H
