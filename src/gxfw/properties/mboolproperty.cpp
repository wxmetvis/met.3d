/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mboolproperty.h"

// standard library imports

// related third party imports
#include <QCheckBox>

// local application imports

namespace Met3D
{
MBoolProperty::MBoolProperty()
        : MValueProperty<bool>(new MBoolProperty::Data("", false))
{}


MBoolProperty::MBoolProperty(const QString &name, bool defaultValue)
        : MValueProperty<bool>(new MBoolProperty::Data(name, defaultValue))
{}


MBoolProperty::MBoolProperty(const MBoolProperty &other)
    : MValueProperty<bool>(new MBoolProperty::Data("",false))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MBoolProperty::setTrueLabel(const QString &text)
{
    auto *data = getData<MBoolProperty::Data>();
    data->trueText = text;
    updateEditorSettings();
}


void MBoolProperty::setFalseLabel(const QString &text)
{
    auto *data = getData<MBoolProperty::Data>();
    data->falseText = text;
    updateEditorSettings();
}


void MBoolProperty::setLabel(const QString &text)
{
    auto *data = getData<MBoolProperty::Data>();
    data->trueText = data->falseText = text;
    updateEditorSettings();
}


const QString &MBoolProperty::getLabel() const
{
    auto *data = getData<MBoolProperty::Data>();
    if (value())
    {
        return data->trueText;
    }
    else
    {
        return data->falseText;
    }
}


MBoolProperty &MBoolProperty::operator=(const MBoolProperty &other)
{
    MValueProperty::operator=(other);

    auto *otherData = other.getData<MBoolProperty::Data>();
    setTrueLabel(otherData->trueText);
    setFalseLabel(otherData->falseText);

    return *this;
}


void MBoolProperty::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(getConfigKey(), propertyValue);
}


void MBoolProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MBoolProperty::Data>();
    bool value = settings->value(getConfigKey(), data->defaultValue).toBool();

    MValueProperty<bool>::setValue(value);
}


MProperty::Editor *MBoolProperty::createEditor(QWidget *parentWidget)
{
    return new MBoolProperty::Editor(parentWidget, this);
}


MBoolProperty::Editor::Editor(QWidget *parent,
                              MBoolProperty *property)
        : MProperty::Editor(parent, property)
{
    box = new QCheckBox(property->getLabel(), this);
    box->setChecked(property->value());

    layout->addWidget(box);

    connect(box, &QCheckBox::stateChanged, this,
            &MBoolProperty::Editor::onStateChanged);
}


MBoolProperty::Editor::~Editor()
= default;


void MBoolProperty::Editor::updateSettings()
{
    auto boolProp = dynamic_cast<MBoolProperty *>(property);
    box->setText(boolProp->getLabel());
}


void MBoolProperty::Editor::updateValue()
{
    auto boolProp = dynamic_cast<MBoolProperty *>(property);
    box->setChecked(boolProp->value());
    box->setText(boolProp->getLabel());
}


void MBoolProperty::Editor::onStateChanged(int state)
{
    Q_UNUSED(state)

    bool value = box->isChecked();

    auto boolProp = dynamic_cast<MBoolProperty *>(property);

    if (value != boolProp->value())
    {
        boolProp->setUndoableValue(value);
    }

    box->setText(boolProp->getLabel());
}

} // Met3D