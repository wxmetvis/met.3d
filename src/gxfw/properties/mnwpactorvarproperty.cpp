/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mnwpactorvarproperty.h"

// standard library imports

// related third party imports
#include <QComboBox>
#include <QVector>

// local application imports

namespace Met3D
{
MNWPActorVarProperty::MNWPActorVarProperty(const QString &name)
        : MValueProperty<MNWPActorVariable *>(new MNWPActorVarProperty::Data(name))
{
}


MNWPActorVarProperty::MNWPActorVarProperty(const MNWPActorVarProperty &other)
        : MValueProperty<MNWPActorVariable *>(new MNWPActorVarProperty::Data(""))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MNWPActorVarProperty::setValue(MNWPActorVariable *const &value)
{
    auto *data = getData<MNWPActorVarProperty::Data>();
    if (!data->variables.contains(value) && !data->variables.isEmpty()) return;
    if (data->variables.isEmpty() && value != nullptr) return;
    MValueProperty::setValue(value);
}


void MNWPActorVarProperty::setIndex(int index)
{
    auto *data = getData<MNWPActorVarProperty::Data>();
    if (data->variables.length() > index && index >= 0)
    {
        setValue(data->variables[index]);
    }
}


void MNWPActorVarProperty::addVariable(MNWPActorVariable *variable)
{
    auto *data = getData<MNWPActorVarProperty::Data>();

    if (data->variables.contains(variable)) return;

    data->variables.append(variable);

    for (auto editor : d->editors)
    {
        dynamic_cast<MNWPActorVarProperty::Editor *>(editor)
                ->addVariable(variable);
    }

    // If this is the first variable being added, set the value to it.
    if (data->variables.size() == 1)
    {
        setValue(variable);
    }
}


void MNWPActorVarProperty::changeVariable(MNWPActorVariable *variable)
{
    auto *data = getData<MNWPActorVarProperty::Data>();

    if (!data->variables.contains(variable)) return;

    for (auto editor : d->editors)
    {
        dynamic_cast<MNWPActorVarProperty::Editor *>(editor)
                ->changeVariable(variable);
    }
}


void MNWPActorVarProperty::removeVariable(MNWPActorVariable *variable)
{
    auto *data = getData<MNWPActorVarProperty::Data>();

    if (!data->variables.contains(variable)) return;

    for (auto editor : d->editors)
    {
        dynamic_cast<MNWPActorVarProperty::Editor *>(editor)
                ->removeVariable(variable);
    }

    data->variables.removeOne(variable);

    if (variable == propertyValue)
    {
        if (data->variables.isEmpty())
        {
            setValue(nullptr);
        }
        else
        {
            setValue(data->variables.first());
        }
    }
}


const QVector<MNWPActorVariable*>& MNWPActorVarProperty::getVariables() const
{
    auto *data = getData<MNWPActorVarProperty::Data>();
    return data->variables;
}


MNWPActorVariable *MNWPActorVarProperty::operator->()
{
    return propertyValue;
}


const MNWPActorVariable *MNWPActorVarProperty::operator->() const
{
    return propertyValue;
}


MNWPActorVarProperty &MNWPActorVarProperty::operator=(const MNWPActorVarProperty &other)
{
    MValueProperty::operator=(other);

    for (MNWPActorVariable *var : getVariables())
    {
        removeVariable(var);
    }
    for (MNWPActorVariable *var : other.getVariables())
    {
        addVariable(var);
    }
    setValue(other.value());

    return *this;
}


void MNWPActorVarProperty::saveToConfiguration(QSettings *settings) const
{
    if (propertyValue == nullptr)
    {
        settings->setValue(getConfigKey(), QUuid());

    }
    else
    {
        settings->setValue(getConfigKey(), propertyValue->uuid);
    }
}


void MNWPActorVarProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MNWPActorVarProperty::Data>();

    QUuid selectedVarId = settings->value(getConfigKey()).toUuid();

    if (selectedVarId.isNull())
    {
        setValue(nullptr);
        return;
    }

    for (MNWPActorVariable *var : data->variables)
    {
        if (var != nullptr && var->uuid == selectedVarId)
        {
            setValue(var);
            return;
        }
    }

    setValue(nullptr);
}


MProperty::Editor *
MNWPActorVarProperty::createEditor(QWidget *parentWidget)
{
    return new MNWPActorVarProperty::Editor(parentWidget, this);
}


MNWPActorVarProperty::Editor::Editor(QWidget *parent,
                                     MNWPActorVarProperty *property)
        : MProperty::Editor(parent, property)
{
    box = new QComboBox(this);
    box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    box->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);

    QStringList names;
    for (MNWPActorVariable *var : property->getVariables())
    {
        if (var == nullptr)
        {
            names << "None";
            continue;
        }
        names << var->variableName;
    }
    box->clear();
    box->addItems(names);
    box->setCurrentIndex(property->getVariables().indexOf(property->value()));

    layout->addWidget(box);

    connect(box, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &MNWPActorVarProperty::Editor::currentIndexChanged);
}


MNWPActorVarProperty::Editor::~Editor()
= default;


void MNWPActorVarProperty::Editor::updateSize(int height)
{
    box->setFixedHeight(height);
}


void MNWPActorVarProperty::Editor::updateValue()
{
    auto *prop = dynamic_cast<MNWPActorVarProperty *>(property);

    MNWPActorVariable *currentValue = nullptr;
    if (box->currentIndex() > 0)
    {
        currentValue = prop->getVariables()[box->currentIndex()];
    }

    if (prop->value() != currentValue)
    {
        box->setCurrentIndex(prop->getVariables().indexOf(prop->value()));
    }
}


void MNWPActorVarProperty::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);
    if (expanding)
    {
        box->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    }
    else
    {
        box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}


void MNWPActorVarProperty::Editor::addVariable(MNWPActorVariable *variable)
{
    if (variable == nullptr) return;

    box->addItem(variable->variableName);
}


void MNWPActorVarProperty::Editor::changeVariable(MNWPActorVariable *variable)
{
    if (variable == nullptr) return;

    auto *prop = dynamic_cast<MNWPActorVarProperty *>(property);

    box->setItemText(prop->getVariables().indexOf(variable), variable->variableName);
}


void MNWPActorVarProperty::Editor::removeVariable(MNWPActorVariable *variable)
{
    if (variable == nullptr) return;

    auto *prop = dynamic_cast<MNWPActorVarProperty *>(property);
    int idx = prop->getVariables().indexOf(variable);

    if (idx == box->currentIndex())
    {
        box->setCurrentIndex(0);
    }

    box->removeItem(idx);
}


void MNWPActorVarProperty::Editor::currentIndexChanged(int index)
{
    auto *prop = dynamic_cast<MNWPActorVarProperty *>(property);

    if (index == -1)
    {
        prop->setValue(nullptr);
        return;
    }

    prop->setValue(prop->getVariables()[index]);
}


} // Met3D