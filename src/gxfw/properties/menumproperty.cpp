/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "menumproperty.h"

// standard library imports
#include <utility>

// related third party imports
#include <QComboBox>
#include <QStandardItemModel>

// local application imports

namespace Met3D
{

MEnumProperty::MEnumProperty(const QString &name)
        : MValueProperty<int>(new MEnumProperty::Data(name, 0))
{}


MEnumProperty::MEnumProperty(const QString &name, const QStringList &enumNames,
                             int index)
        : MValueProperty<int>(new MEnumProperty::Data(name, index))
{
    setEnumNames(enumNames, index);
}


MEnumProperty::MEnumProperty(const MEnumProperty &other)
        : MValueProperty<int>(new MEnumProperty::Data("", 0))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}



void MEnumProperty::setValue(const int &value)
{
    auto *data = getData<MEnumProperty::Data>();

    // Dont set value if it is invalid.
    if (data->enumNames.length() <= value) return;
    if (data->enumNames.isEmpty() && value != -1) return;
    if (value < 0) return;

    MValueProperty::setValue(value);
}


void MEnumProperty::saveAsEnumName(bool value)
{
    auto *data = getData<MEnumProperty::Data>();

    data->saveEnumName = value;
}


void MEnumProperty::setEnumNames(QStringList names, int index)
{
    auto *data = getData<MEnumProperty::Data>();

    data->enumNames = std::move(names);

    if (data->enumNames.isEmpty()) return;

    if (index >= 0 && index < data->enumNames.length())
    {
        setValue(index);
    }
    else
    {
        setValue(0);
    }

    for (auto editor : d->editors)
    {
        dynamic_cast<MEnumProperty::Editor *>(editor)->updateEnumNames();
    }
}


void MEnumProperty::setEnumItem(const QString &name)
{
    auto *data = getData<MEnumProperty::Data>();

    for (int i = 0; i < data->enumNames.length(); i++)
    {
        if (name.compare(data->enumNames[i], Qt::CaseInsensitive) == 0)
        {
            setValue(i);
            return;
        }
    }
}


const QStringList &MEnumProperty::getEnumNames() const
{
    auto *data = getData<MEnumProperty::Data>();
    return data->enumNames;
}


QString MEnumProperty::getSelectedEnumName() const
{
    auto *data = getData<MEnumProperty::Data>();

    if (propertyValue < 0 || propertyValue >= data->enumNames.length())
        return "";

    return data->enumNames[propertyValue];
}


void MEnumProperty::setSelectable(bool enabled) const
{
    auto *data = getData<MEnumProperty::Data>();
    data->selectable = enabled;

    for (auto editor : d->editors)
    {
        dynamic_cast<MEnumProperty::Editor *>(editor)->setSelectable(enabled);
    }
}


MEnumProperty &MEnumProperty::operator=(const Met3D::MEnumProperty &other)
{
    MValueProperty::operator=(other);

    setEnumNames(other.getEnumNames(), other.value());
    saveAsEnumName(other.getData<MEnumProperty::Data>()->saveEnumName);

    return *this;
}


void MEnumProperty::saveToConfiguration(QSettings *settings) const
{
    auto *data = getData<MEnumProperty::Data>();

    if (data->saveEnumName)
    {
        settings->setValue(getConfigKey(), getSelectedEnumName().toLower());
    }
    else
    {
        settings->setValue(getConfigKey(), propertyValue);
    }
}


void MEnumProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MEnumProperty::Data>();

    int idx;
    if (data->saveEnumName)
    {
        QString enumName = settings->value(getConfigKey()).toString();
        if (enumName.isEmpty())
        {
            idx = data->defaultValue;
        }
        else
        {
            for (int i = 0; i < data->enumNames.length(); i++)
            {
                if (enumName.compare(data->enumNames[i], Qt::CaseInsensitive) == 0)
                {
                    idx = i;
                    break;
                }
            }
        }
    }
    else
    {
        idx = settings->value(getConfigKey(), data->defaultValue).toInt();
    }

    setValue(idx);
}


MProperty::Editor *MEnumProperty::createEditor(QWidget *parentWidget)
{
    return new MEnumProperty::Editor(parentWidget, this);
}


MEnumProperty::Editor::Editor(QWidget *parent, MEnumProperty *property)
        : MProperty::Editor(parent, property)
{
    box = new QComboBox(this);
    box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);

    box->addItems(property->getEnumNames());
    box->setCurrentIndex(property->value());

    box->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);

    layout->addWidget(box);

    connect(box, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &MEnumProperty::Editor::currentIndexChanged);

    bool itemsSelectable = property->getData<MEnumProperty::Data>()->selectable;
    setSelectable(itemsSelectable);
}


MEnumProperty::Editor::~Editor()
= default;


void MEnumProperty::Editor::updateValue()
{
    auto *enumProp = dynamic_cast<MEnumProperty *>(property);

    // Index changed
    if (enumProp->value() != box->currentIndex())
    {
        box->setCurrentIndex(enumProp->value());
    }
}


void MEnumProperty::Editor::updateSize(int height)
{
    box->setFixedHeight(height);
}


void MEnumProperty::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        box->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    }
    else
    {
        box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}


void MEnumProperty::Editor::updateEnumNames()
{
    auto *enumProp = dynamic_cast<MEnumProperty *>(property);
    int previousValue = enumProp->value();

    bool b = box->blockSignals(true);
    box->clear();
    box->addItems(enumProp->getEnumNames());

    // Restore previous index.
    box->setCurrentIndex(previousValue);
    enumProp->setValue(box->currentIndex());
    box->blockSignals(b);

    bool itemsSelectable = enumProp->getData<MEnumProperty::Data>()->selectable;
    setSelectable(itemsSelectable);
}


void MEnumProperty::Editor::currentIndexChanged(int index)
{
    auto *enumProp = dynamic_cast<MEnumProperty *>(property);

    if (enumProp->value() == index) return;

    enumProp->setUndoableValue(index);
}


void MEnumProperty::Editor::setSelectable(bool enabled)
{
    auto *enumProp = dynamic_cast<MEnumProperty *>(property);
    auto *model = dynamic_cast<QStandardItemModel*>(box->model());

    int numEntries = enumProp->getEnumNames().size();
    for (int i = 0; i < numEntries; i++)
    {
        QStandardItem* item = model->item(i);
        item->setFlags(enabled ? item->flags() | Qt::ItemIsEnabled
                                               : item->flags() & ~Qt::ItemIsEnabled);
    }
}

} // Met3D