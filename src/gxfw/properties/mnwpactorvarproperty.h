/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MNWPACTORVARPROPERTY_H
#define MET_3D_MNWPACTORVARPROPERTY_H

// standard library imports

// related third party imports
#include <QtContainerFwd>

// local application imports
#include "mproperty.h"
#include "gxfw/nwpactorvariable.h"

// forward declarations
class QComboBox;

namespace Met3D
{

/**
 * A property that presents actor variables to the user to select from.
 * It holds a pointer to the selected actor variable as a value.
 * If none is selected, the value will be @c nullptr.
 */
class MNWPActorVarProperty : public MValueProperty<MNWPActorVariable *>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<MNWPActorVariable *>::operator MNWPActorVariable *;
    using MValueProperty<MNWPActorVariable *>::operator=;

    explicit MNWPActorVarProperty(const QString &name = "");

    MNWPActorVarProperty(const MNWPActorVarProperty &other);

    void setValue(MNWPActorVariable *const &value) override;

    /**
     * Set the selected actor variable via their index.
     * @param index Index of the actor variable.
     */
    void setIndex(int index);

    /**
     * Add a variable to the list of selectable variables.
     * If @p variable is @c nullptr, a "None" option is added.
     * @param variable The variable to add.
     */
    void addVariable(MNWPActorVariable *variable);

    /**
     * Signifies, that the given variable was changed.
     * This will update that variable in the property,
     * if it was added already.
     * @param variable The variable that was changed.
     */
    void changeVariable(MNWPActorVariable *variable);

    /**
     * Removes a variable from this property.
     * If the variable was the selected variable,
     * the value will be reset to the first entry.
     * @param variable The variable to remove.
     */
    void removeVariable(MNWPActorVariable *variable);

    const QVector<MNWPActorVariable*>& getVariables() const;

    /**
     * Direct pointer access to the @c MNWPActorVariable held by this
     * property.
     */
    MNWPActorVariable *operator->();

    /**
     * Direct pointer access to the @c MNWPActorVariable held by this
     * property.
     */
    const MNWPActorVariable *operator->() const;

    MNWPActorVarProperty &operator=(const MNWPActorVarProperty &other);

protected:
    // Editor class for this property type.
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<MNWPActorVariable *>::Data
    {
        QVector<MNWPActorVariable *> variables;

        explicit Data(const QString &name)
        : MValueProperty<MNWPActorVariable *>::Data(name, nullptr)
        {
        }
    };
};

class MNWPActorVarProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MNWPActorVarProperty *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateValue() override;

    void setExpanding(bool expanding) override;

    /**
     * Add a variable to the list of selectable variables.
     * @param variable The variable to add.
     */
    void addVariable(MNWPActorVariable *variable);

    /**
     * Signifies, that the given variable was changed.
     * This will update that variable in the property,
     * if it was added already.
     * @param variable The variable that was changed.
     */
    void changeVariable(MNWPActorVariable *variable);

    /**
     * Removes a variable from this property.
     * If the variable was the selected variable,
     * the value will be set to the first entry.
     * @param variable The variable to remove.
     */
    void removeVariable(MNWPActorVariable *variable);

public slots:

    /**
     * Slot called by the combobox of this editor,
     * when its selected index changes.
     * @param index The new selected index.
     */
    void currentIndexChanged(int index);

protected:
    /**
     * Combobox used to select the variables.
     */
    QComboBox *box;
};

} // Met3D

#endif //MET_3D_MNWPACTORVARPROPERTY_H
