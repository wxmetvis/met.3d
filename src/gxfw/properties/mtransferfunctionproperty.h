/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MTRANSFERFUNCTIONPROPERTY_H
#define MET_3D_MTRANSFERFUNCTIONPROPERTY_H

// standard library imports

// related third party imports

// local application imports
#include "mproperty.h"
#include "gxfw/transferfunction.h"
#include "gxfw/mglresourcesmanager.h"

namespace Met3D
{

class MSpatial1DTransferFunction;

class MTransferFunction1D;

/**
 * A property that can be used to select any transfer function.
 * It automatically updates its list of transfer functions,
 * to include any that is currently registered in the @¢ MGLResourceManager.
 * The value of this property holds a pointer to the selected transfer function,
 * or a @c nullptr, if none is selected.
 * @tparam TF Filter for the transfer function type that can be selected.
 */
template<class TF>
class MTransferFunctionProperty : public MValueProperty<MTransferFunction *>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<MTransferFunction *>::operator MTransferFunction *;
    using MValueProperty<MTransferFunction *>::operator=;

    explicit MTransferFunctionProperty(const QString &name = "");

    MTransferFunctionProperty(const MTransferFunctionProperty<TF> &other);

    void setValue(MTransferFunction *const &value) override;

    /**
     * Set the selected transfer function by its index in the list.
     * None is 0.
     * @param index The index to select.
     */
    void setIndex(int index);

    /**
     * Set the selected transfer function by name.
     * "None" is also valid and means no transfer function is selected.
     * @param name The name of the transfer function.
     */
    bool setByName(const QString &name);

    /**
     * @return A const reference to a vector containing all transfer functions
     * currently known to this property.
     */
    const QVector<MTransferFunction *> &getTransferFunctions();

    /**
     * Direct pointer access to the @c MTransferFunction held by this
     * property.
     */
    MTransferFunction *operator->() { return propertyValue; }

    /**
     * Direct pointer access to the @c MTransferFunction held by this
     * property.
     */
    const MTransferFunction *operator->() const { return propertyValue; }

public slots:

    /**
     * Slot called by the GL resource manager, when an actor is created.
     * @param actor The created actor.
     */
    void onActorCreated(Met3D::MActor *actor);

    /**
     * Slot called by the GL resource manager, when an actor is renamed.
     * @param actor The renamed actor.
     */
    void onActorRenamed(Met3D::MActor *actor);

    /**
     * Slot called by the GL resource manager, when an actor is deleted.
     * @param actor The deleted actor.
     */
    void onActorDeleted(Met3D::MActor *actor);

protected:
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<MTransferFunction *>::Data
    {
        /**
         * A vector containing all known transfer functions.
         */
        QVector<MTransferFunction *> transferFunctions;


        Data(const QString &name)
                : MValueProperty<MTransferFunction *>::Data(name, nullptr)
        {
        }
    };
};


template<class TF>
class MTransferFunctionProperty<TF>::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent,
           MTransferFunctionProperty *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateValue() override;

    void setExpanding(bool expanding) override;

    /**
     * Add a transfer function to the combo box of this editor.
     * @param tf The transfer function to add.
     */
    void addTransferFunction(MTransferFunction *tf);

    /**
     * Rename a transfer function in the combo box of this editor.
     * @param tf The transfer function to rename.
     */
    void renameTransferFunction(MTransferFunction *tf);

    /**
     * Remove a transfer function from the combo box of this editor.
     * @param tf The transfer function to remove.
     */
    void removeTransferFunction(MTransferFunction *tf);

public slots:

    /**
     * Slot called by the combobox of this editor,
     * when its selected index changes.
     * @param index The new selected index.
     */
    void currentIndexChanged(int index);

protected:
    /**
     * @return The transfer function currently selected by the combobox.
     */
    MTransferFunction *getCurrentValue();

    /**
     * Combobox used to select the transfer functions.
     */
    QComboBox *box;

    /**
     * List of transfer functions in the combobox.
     */
    QVector<MTransferFunction *> transferFunctions;
};


template<class TF>
MTransferFunctionProperty<TF>::MTransferFunctionProperty(const QString &name)
        : MValueProperty<MTransferFunction *>(
        new MTransferFunctionProperty<TF>::Data(name))
{
    static_assert(std::is_base_of<MTransferFunction, TF>::value,
                  "TF parameter has to be a child class of MTransferFunction.");

    auto *glRM = MGLResourcesManager::getInstance();
    auto *data = getData<MTransferFunctionProperty::Data>();

    for (MActor *actor : glRM->getActors())
    {
        auto *tf = dynamic_cast<TF *>(actor);
        if (tf)
        {
            data->transferFunctions.append(tf);
        }
    }

    connect(glRM, &MGLResourcesManager::actorCreated,
            this, &MTransferFunctionProperty::onActorCreated);

    connect(glRM, &MGLResourcesManager::actorRenamed,
            this, &MTransferFunctionProperty::onActorRenamed);

    connect(glRM, &MGLResourcesManager::actorDeleted,
            this, &MTransferFunctionProperty::onActorDeleted);
}


template<class TF>
MTransferFunctionProperty<TF>::MTransferFunctionProperty(
        const MTransferFunctionProperty<TF> &other)
        : MValueProperty<MTransferFunction *>(
        new MTransferFunctionProperty<TF>::Data("", ""))
{
    static_assert(std::is_base_of<MTransferFunction, TF>::value,
                  "TF parameter has to be a child class of MTransferFunction.");

    auto *glRM = MGLResourcesManager::getInstance();
    auto *data = getData<MTransferFunctionProperty::Data>();

    for (MActor *actor : glRM->getActors())
    {
        auto *tf = dynamic_cast<TF *>(actor);
        if (tf)
        {
            data->transferFunctions.append(tf);
        }
    }

    connect(glRM, &MGLResourcesManager::actorCreated,
            this, &MTransferFunctionProperty::onActorCreated);

    connect(glRM, &MGLResourcesManager::actorRenamed,
            this, &MTransferFunctionProperty::onActorRenamed);

    connect(glRM, &MGLResourcesManager::actorDeleted,
            this, &MTransferFunctionProperty::onActorDeleted);

    // Run assignment operator to copy the other property into this one.
    *this = other;
}


template<class TF>
void MTransferFunctionProperty<TF>::setValue(MTransferFunction *const &value)
{
    auto *data = getData<MTransferFunctionProperty::Data>();
    if (!data->transferFunctions.contains(value) && value != nullptr) return;
    MValueProperty::setValue(value);
}


template<class TF>
void MTransferFunctionProperty<TF>::setIndex(int index)
{
    auto *data = getData<MTransferFunctionProperty::Data>();
    if (data->transferFunctions.length() >= index && index > 0)
    {
        setValue(data->transferFunctions[index - 1]);
    }
    else
    {
        setValue(nullptr);
    }
}


template<class TF>
bool MTransferFunctionProperty<TF>::setByName(const QString &name)
{
    if (name == "None")
    {
        setValue(nullptr);
        return true;
    }

    auto *data = getData<MTransferFunctionProperty::Data>();

    for (MTransferFunction *tf : data->transferFunctions)
    {
        if (tf->getName() == name)
        {
            setValue(tf);
            return true;
        }
    }

    return false;
}


template<class TF>
const QVector<MTransferFunction *> &
MTransferFunctionProperty<TF>::getTransferFunctions()
{
    auto *data = getData<MTransferFunctionProperty::Data>();
    return data->transferFunctions;
}


template<class TF>
void MTransferFunctionProperty<TF>::onActorCreated(MActor *actor)
{
    auto *data = getData<MTransferFunctionProperty::Data>();
    auto *tf = dynamic_cast<TF *>(actor);
    if (tf)
    {
        data->transferFunctions.append(tf);
        for (auto editor : d->editors)
        {
            dynamic_cast<MTransferFunctionProperty::Editor *>(editor)
                    ->addTransferFunction(tf);
        }
    }
}


template<class TF>
void MTransferFunctionProperty<TF>::onActorRenamed(MActor *actor)
{
    auto *tf = dynamic_cast<TF *>(actor);
    if (tf)
    {
        for (auto editor : d->editors)
        {
            dynamic_cast<MTransferFunctionProperty::Editor *>(editor)
                    ->renameTransferFunction(tf);
        }
    }
}


template<class TF>
void MTransferFunctionProperty<TF>::onActorDeleted(MActor *actor)
{
    auto *data = getData<MTransferFunctionProperty::Data>();
    auto *tf = dynamic_cast<TF *>(actor);
    if (tf)
    {
        data->transferFunctions.removeAll(tf);
        for (auto editor : d->editors)
        {
            dynamic_cast<MTransferFunctionProperty::Editor *>(editor)
                    ->removeTransferFunction(tf);
        }
    }
}


template<class TF>
void MTransferFunctionProperty<TF>::saveToConfiguration(QSettings *settings) const
{
    QString name;
    if (propertyValue != nullptr)
    {
        name = propertyValue->getName();
    }
    settings->setValue(d->configKey, name);
}


template<class TF>
void MTransferFunctionProperty<TF>::loadFromConfiguration(QSettings *settings)
{
    QString tfName = settings->value(d->configKey, "").toString();
    if (tfName.isEmpty() || tfName == "None")
    {
        setValue(nullptr);
    }
    else
    {
        auto *data = getData<MTransferFunctionProperty::Data>();
        MTransferFunction *value = nullptr;
        for (MTransferFunction *tf : data->transferFunctions)
        {
            if (tf->getName() == tfName)
            {
                value = tf;
                break;
            }
        }

        if (value == nullptr)
        {
            if (MTransferFunction::loadMissingTransferFunction(
                    tfName, TF::staticActorType(),
                    "Property ", getName(), settings))
            {
                for (MTransferFunction *tf : data->transferFunctions)
                {
                    if (tf->getName() == tfName)
                    {
                        value = tf;
                        break;
                    }
                }
            }
        }

        setValue(value);
    }
}


template<class TF>
MProperty::Editor *
MTransferFunctionProperty<TF>::createEditor(QWidget *parentWidget)
{
    return new MTransferFunctionProperty::Editor(parentWidget, this);
}


template<class TF>
MTransferFunctionProperty<TF>::Editor::Editor(
        QWidget *parent, MTransferFunctionProperty<TF> *property)
        : MProperty::Editor(parent, property)
{
    box = new QComboBox(this);
    box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    box->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);

    QStringList names;
    names << "None";

    transferFunctions = property->getTransferFunctions();
    for (MTransferFunction *tf : transferFunctions)
    {
        names.append(tf->getName());
    }
    box->clear();
    box->addItems(names);
    box->setCurrentIndex(transferFunctions.indexOf(property->value()) + 1);

    layout->addWidget(box);

    connect(box, QOverload<int>::of(&QComboBox::currentIndexChanged),
            this, &MTransferFunctionProperty<TF>::Editor::currentIndexChanged);
}


template<class TF>
MTransferFunctionProperty<TF>::Editor::~Editor()
{
    delete box;
}


template<class TF>
void MTransferFunctionProperty<TF>::Editor::updateSize(int height)
{
    box->setFixedHeight(height);
}


template<class TF>
void MTransferFunctionProperty<TF>::Editor::updateValue()
{
    auto *prop = dynamic_cast<MTransferFunctionProperty *>(property);

    MTransferFunction *currentValue = nullptr;
    if (box->currentIndex() > 0)
    {
        currentValue = transferFunctions[box->currentIndex() - 1];
    }

    // Index changed
    if (prop->value() != currentValue)
    {
        bool b = box->blockSignals(true);
        box->setCurrentIndex(transferFunctions.indexOf(prop->value()) + 1);
        prop->setValue(getCurrentValue());
        box->blockSignals(b);
    }
}


template<class TF>
void MTransferFunctionProperty<TF>::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        box->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    }
    else
    {
        box->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}


template<class TF>
void
MTransferFunctionProperty<TF>::Editor::addTransferFunction(
        MTransferFunction *tf)
{
    if (tf == nullptr) return;

    transferFunctions.append(tf);

    auto *prop = dynamic_cast<MTransferFunctionProperty *>(property);

    bool b = box->blockSignals(true);
    box->addItem(tf->getName());
    prop->setValue(getCurrentValue());
    box->blockSignals(b);
}


template<class TF>
void
MTransferFunctionProperty<TF>::Editor::renameTransferFunction(
        MTransferFunction *tf)
{
    if (tf == nullptr) return;

    box->setItemText(transferFunctions.indexOf(tf) + 1, tf->getName());
}


template<class TF>
void
MTransferFunctionProperty<TF>::Editor::removeTransferFunction(
        MTransferFunction *tf)
{
    if (tf == nullptr || !transferFunctions.contains(tf)) return;

    auto *prop = dynamic_cast<MTransferFunctionProperty *>(property);
    bool b = box->blockSignals(true);

    // Remove the TF from the combobox and from the list of TFs
    int tfIndexToRemove = transferFunctions.indexOf(tf);
    box->removeItem(tfIndexToRemove + 1);
    transferFunctions.removeAt(tfIndexToRemove);

    prop->setValue(getCurrentValue());
    box->blockSignals(b);
}


template<class TF>
void MTransferFunctionProperty<TF>::Editor::currentIndexChanged(int index)
{
    if (index == -1) return; // Box was cleared, ignore.

    auto *prop = dynamic_cast<MTransferFunctionProperty *>(property);
    MTransferFunction *val = getCurrentValue();
    prop->setUndoableValue(val);
}


template<class TF>
MTransferFunction *MTransferFunctionProperty<TF>::Editor::getCurrentValue()
{
    int index = box->currentIndex();
    if (index == -1) return nullptr; // Box was cleared, ignore.
    if (index == 0)
    {
        return nullptr;
    }
    else
    {
        return transferFunctions[index - 1];
    }
}


typedef MTransferFunctionProperty<MTransferFunction1D> MTransferFunction1DProperty;
typedef MTransferFunctionProperty<MSpatial1DTransferFunction> MSpatial1DTransferFunctionProperty;

} // Met3D

#endif //MET_3D_MTRANSFERFUNCTIONPROPERTY_H
