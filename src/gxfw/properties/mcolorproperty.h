/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MCOLORPROPERTY_H
#define MET_3D_MCOLORPROPERTY_H

// standard library imports

// related third party imports

// local application imports
#include "mproperty.h"

// forward declarations
class QColor;
class QToolButton;
class QLabel;

namespace Met3D
{

/**
 * A color property.
 */
class MColorProperty : public MValueProperty<QColor>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<QColor>::operator QColor;
    using MValueProperty<QColor>::operator const QColor &;
    using MValueProperty<QColor>::operator=;

    MColorProperty();

    MColorProperty(const QString &name,
                            const QColor &defaultValue);

    MColorProperty(const MColorProperty &other);

    void setValue(const QColor &value) override;

    /**
     * Enables or disables the alpha channel in the color.
     * By default, this is true.
     * @param enableAlphaChannel Whether to enable or disable the alpha channel.
     */
    void toggleAlpha(bool enableAlphaChannel);

    /**
     * Checks, if this color property uses the alpha channel.
     * @return True if property uses the alpha channel.
     */
    bool hasAlpha() const;

    MColorProperty &operator=(const MColorProperty &other);

protected:
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<QColor>::Data
    {
        /**
         * Whether the alpha channel is used or not.
         */
        bool useAlpha;

        Data(const QString &name, const QColor &defaultValue)
                : MValueProperty<QColor>::Data(name, defaultValue),
                  useAlpha(true)
        {}
    };
};

class MColorProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MColorProperty *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateValue() override;

    void updateSettings() override;

public slots:

    void onClick();

protected:
    void mouseDoubleClickEvent(QMouseEvent *event) override;

    /**
     * Update the label and pixmap that shows the color value.
     */
    void updateLabel();

    /**
     * Create the small preview pixmap that shows the color value of the property.
     * @return The created pixmap.
     */
    QPixmap createCurrentPixmap() const;

    QLabel *pixmap;
    QLabel *label;
    QToolButton *button;
};

} // Met3D

#endif //MET_3D_MCOLORPROPERTY_H
