/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MVECTOR3DPROPERTY_H
#define MET_3D_MVECTOR3DPROPERTY_H

// standard library imports

// related third party imports
#include <QVector3D>

// local application imports
#include "mproperty.h"
#include "mnumberproperty.h"

// forward declarations
class QLabel;

namespace Met3D
{

/**
 * A property that holds a @c QVector3D
 */
class MVector3DProperty : public MValueProperty<QVector3D>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<QVector3D>::operator QVector3D;
    using MValueProperty<QVector3D>::operator const QVector3D &;
    using MValueProperty<QVector3D>::operator=;

    MVector3DProperty();
    MVector3DProperty(const QString &name, const QVector3D &defaultValue);

    MVector3DProperty(const MVector3DProperty &other);

    ~MVector3DProperty() override;

    void setValue(const QVector3D &value) override;

    /**
     * Set the x axis value.
     * @param x value for x axis.
     */
    void setX(float x);

    /**
     * Set the y axis value.
     * @param y value for y axis.
     */
    void setY(float y);

    /**
     * Set the z axis value.
     * @param z value for z axis.
     */
    void setZ(float z);

    /**
     * @return The x value of this property.
     */
    constexpr inline float x() const { return propertyValue.x(); };

    /**
     * @return The y value of this property.
     */
    constexpr inline float y() const { return propertyValue.y(); };

    /**
     * @return The z value of this property.
     */
    constexpr inline float z() const { return propertyValue.z(); };

    /**
     * Get the property that is responsible for the x value.
     * Can be used to set minima and maxima.
     * The returned property should never be removed from this property.
     * @return The property responsible to edit the x value of this property.
     */
    MFloatProperty *getXEditor();

    /**
     * Get the property that is responsible for the y value.
     * Can be used to set minima and maxima.
     * The returned property should never be removed from this property.
     * @return The property responsible to edit the y value of this property.
     */
    MFloatProperty *getYEditor();

    /**
     * Get the property that is responsible for the z value.
     * Can be used to set minima and maxima.
     * The returned property should never be removed from this property.
     * @return The property responsible to edit the z value of this property.
     */
    MFloatProperty *getZEditor();

    /**
     * Toggles the suffix display in the preview string on and off.
     * @param show Whether to show or not. Default false.
     */
    void setShowSuffixInPreview(bool show);

    /**
     * @return Whether the suffix of the different axis is shown in the preview.
     */
    bool showSuffixInPreview() const;

    MVector3DProperty &operator=(const MVector3DProperty &other);

public slots:

    /**
     * Called by the sub properties, when their value changes.
     */
    void onVectorChanged();

protected:
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty::Data
    {
        MFloatProperty *xProp;
        MFloatProperty *yProp;
        MFloatProperty *zProp;

        bool showSuffixInPreview;

        Data(const QString &name, const QVector3D &defaultValue)
                : MValueProperty<QVector3D>::Data(name, defaultValue),
                  xProp(nullptr),
                  yProp(nullptr),
                  zProp(nullptr),
                  showSuffixInPreview(false)
        {}
    };
};

class MVector3DProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MVector3DProperty *property);

    void updateSize(int height) override;

    void updateValue() override;

    void updateSettings() override;

    void updateText();

protected:
    QLabel *valueLabel;
};

} // Met3D

#endif //MET_3D_MVECTOR3DPROPERTY_H
