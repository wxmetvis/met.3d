/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MSTRINGPROPERTY_H
#define MET_3D_MSTRINGPROPERTY_H

// standard library imports

// related third party imports
#include <QString>

// local application imports
#include "mproperty.h"

// forward declarations
class QLabel;
class QLineEdit;

namespace Met3D
{

/**
 * The string property class. A property that displays a simple string,
 * that is editable.
 */
class MStringProperty : public MValueProperty<QString>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<QString>::operator QString;
    using MValueProperty<QString>::operator const QString &;
    using MValueProperty<QString>::operator=;

    MStringProperty();
    MStringProperty(const QString &name,
                             const QString &defaultValue);

    MStringProperty(const MStringProperty &other);

    /**
     * Set the property to be editable or not.
     * If it is not, the string is only displayed.
     * Does not disable the property.
     * @param canEdit Whether the string can be edited or not.
     */
    void setEditable(bool canEdit);

    /**
     * @return Whether the property is currently editable.
     */
    bool isEditable() const;

    MStringProperty &operator=(const MStringProperty &other);

protected:
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<QString>::Data
    {
        /**
         * Whether the string property is editable.
         * If it is not, it should only display the string.
         */
        bool editable;


        Data(const QString &name, const QString &defaultValue)
                : MValueProperty<QString>::Data(name, defaultValue),
                  editable(true)
        {}
    };
};

/**
 * The string property editor. It shows a line edit to edit the string.
 */
class MStringProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MStringProperty *property);

    ~Editor() override;

    void updateSize(int height) override;

    void updateValue() override;

    void updateSettings() override;

    void setExpanding(bool expanding) override;

public slots:

    /**
     * Slot called from editor, when the string was edited.
     */
    void onEditingFinished();

protected:

    /**
     * The line edit used to show and edit the property value.
     */
    QLineEdit *lineEdit;
};

} // Met3D

#endif //MET_3D_MSTRINGPROPERTY_H
