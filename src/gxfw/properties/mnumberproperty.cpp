/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mnumberproperty.h"

// standard library imports

// related third party imports

// local application imports

namespace Met3D
{
MIntProperty::MIntProperty()
        : MNumberProperty<int32_t>(new MNumberProperty::Data("", {}))
{}


MIntProperty::MIntProperty(const QString &name, int32_t defaultValue)
        : MNumberProperty<int32_t>(new MNumberProperty::Data(name, defaultValue))
{}


void MIntProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MIntProperty::Data>();
    MNumberProperty<int32_t>::setValue(
            settings->value(getConfigKey(), data->defaultValue).toInt());
}


MProperty::Editor *MIntProperty::createEditor(QWidget *parentWidget)
{
    return new MIntProperty::Editor(parentWidget, this);
}


MIntProperty::Editor::Editor(QWidget *parent, Met3D::MIntProperty *property)
        : MProperty::Editor(parent, property)
{
    spinBox = new QSpinBox(this);
    spinBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    spinBox->setMinimum(property->getMinimum());
    spinBox->setMaximum(property->getMaximum());
    spinBox->setSingleStep(property->getStep());
    spinBox->setPrefix(property->getPrefix());
    spinBox->setSuffix(property->getSuffix());
    spinBox->setValue(property->value());

    layout->addWidget(spinBox);

    connect(spinBox, &QSpinBox::editingFinished,
            this, &MIntProperty::Editor::onValueChanged);
}


MIntProperty::Editor::~Editor()
= default;


void MIntProperty::Editor::updateSize(int height)
{
    spinBox->setFixedHeight(height);
}


void MIntProperty::Editor::updateSettings()
{
    auto prop = dynamic_cast<MIntProperty *>(property);

    if (prop == nullptr) return;

    spinBox->setMinimum(prop->getMinimum());
    spinBox->setMaximum(prop->getMaximum());
    spinBox->setSingleStep(prop->getStep());
    spinBox->setPrefix(prop->getPrefix());
    spinBox->setSuffix(prop->getSuffix());
}


void MIntProperty::Editor::updateValue()
{
    auto prop = dynamic_cast<MIntProperty *>(property);

    if (prop == nullptr) return;

    bool b = spinBox->blockSignals(true);
    spinBox->setValue(prop->value());
    prop->setValue(spinBox->value());
    spinBox->blockSignals(b);
}


void MIntProperty::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        spinBox->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    }
    else
    {
        spinBox->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}


void MIntProperty::Editor::onValueChanged()
{
    auto prop = dynamic_cast<MNumberProperty<int32_t> *>(property);
    const int val = spinBox->value();

    if (val != prop->value())
    {
        prop->setUndoableValue(val);
    }
}


MDoubleProperty::MDoubleProperty()
        : MFloatingPointProperty(new MFloatingPointProperty::Data("", {}))
{}


MDoubleProperty::MDoubleProperty(const QString &name, double defaultValue)
        : MFloatingPointProperty(new MFloatingPointProperty::Data(name, defaultValue))
{}


void MDoubleProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MDoubleProperty::Data>();
    MFloatingPointProperty<double>::setValue(
            settings->value(getConfigKey(), data->defaultValue).toDouble());
}


MProperty::Editor *MDoubleProperty::createEditor(QWidget *parentWidget)
{
    return new MFloatingPointProperty<double>::Editor(parentWidget, this);
}


MFloatProperty::MFloatProperty()
        : MFloatingPointProperty<float>(new MFloatingPointProperty::Data("", {}))
{}


MFloatProperty::MFloatProperty(const QString &name, float defaultValue)
        : MFloatingPointProperty<float>(new MFloatingPointProperty::Data(name, defaultValue))
{}


void MFloatProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MFloatProperty::Data>();
    MFloatingPointProperty<float>::setValue(
            settings->value(getConfigKey(), data->defaultValue).toFloat());
}


MProperty::Editor *MFloatProperty::createEditor(QWidget *parentWidget)
{
    return new MFloatingPointProperty<float>::Editor(parentWidget, this);
}


MSciDoubleProperty::MSciDoubleProperty()
        : MSciFloatingPointProperty(new MSciFloatingPointProperty::Data("", {}))
{}


MSciDoubleProperty::MSciDoubleProperty(const QString &name, double defaultValue)
        : MSciFloatingPointProperty(new MSciFloatingPointProperty::Data(name, defaultValue))
{}


void MSciDoubleProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MSciDoubleProperty::Data>();
    MFloatingPointProperty<double>::setValue(
            settings->value(getConfigKey(), data->defaultValue).toDouble());
}


MProperty::Editor *MSciDoubleProperty::createEditor(QWidget *parentWidget)
{
    return new MSciFloatingPointProperty<double>::Editor(parentWidget, this);
}


MSciFloatProperty::MSciFloatProperty()
        : MSciFloatingPointProperty<float>(new MSciFloatingPointProperty::Data("", {}))
{}


MSciFloatProperty::MSciFloatProperty(const QString &name, float defaultValue)
        : MSciFloatingPointProperty<float>(new MSciFloatingPointProperty::Data(name, defaultValue))
{}


void MSciFloatProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MSciFloatProperty::Data>();
    MFloatingPointProperty<float>::setValue(
            settings->value(getConfigKey(), data->defaultValue).toFloat());
}


MProperty::Editor *MSciFloatProperty::createEditor(QWidget *parentWidget)
{
    return new MSciFloatingPointProperty<float>::Editor(parentWidget, this);
}
} // Met3D