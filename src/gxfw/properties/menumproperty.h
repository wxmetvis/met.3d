/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MENUMPROPERTY_H
#define MET_3D_MENUMPROPERTY_H

// standard library imports

// related third party imports
#include <QStringList>

// local application imports
#include "mproperty.h"

// forward declarations
class QComboBox;

namespace Met3D
{

/**
 * A property for enums. It receives a list of enum names as @c QStrings,
 * and provides a combobox to the user to select the requested enum.
 * The value this property holds is the selected enum index.
 * It is also possible to get the selected enum name string though.
 */
class MEnumProperty : public MValueProperty<int>
{
public:
    // Need to use these operators, otherwise implicit conversions
    // to the value are not possible.
    using MValueProperty<int>::operator int;
    using MValueProperty<int>::operator const int &;
    using MValueProperty<int>::operator=;

    explicit MEnumProperty(const QString &name = "");

    MEnumProperty(const QString &name, const QStringList &enumNames,
                  int index = 0);

    MEnumProperty(const MEnumProperty &other);

    void setValue(const int &value) override;

    /**
     * Whether to save this property as the selected enum name,
     * or the selected index.
     * The name will always be saved lower case.
     * @param value True, if the property should save the enum name instead of the index.
     */
    void saveAsEnumName(bool value);

    /**
     * Sets the enum names displayed by the property.
     * Resets the selected index to @c 0 or to the provided @p index.
     * @param names The enum names list.
     * @param index The index that will be selected, after setting
     * the enum names.
     */
    void setEnumNames(QStringList names, int index = 0);

    /**
     * Selects the given @p name in the enum list.
     * The selection ignores case.
     * @param name Name to select in enum list.
     */
    void setEnumItem(const QString &name);

    /**
     * @return A list of all enum names this property currently displays.
     */
    const QStringList &getEnumNames() const;

    /**
     * @return The selected enum name, or an empty string if there are no enum names set
     * or the selected index is invalid.
     */
    QString getSelectedEnumName() const;

    /**
     * Make the items in the enum property (not) selectable.
     * @param enabled If true, make items selectable, if false, make them not selectable.
     */
    void setSelectable(bool enabled) const;

    MEnumProperty &operator=(const MEnumProperty &other);

protected:
    /*
     * Declaration of editor class.
     */
    class Editor;

    void saveToConfiguration(QSettings *settings) const override;

    void loadFromConfiguration(QSettings *settings) override;

    MProperty::Editor *createEditor(QWidget *parentWidget) override;

    struct Data : MValueProperty<int>::Data
    {
        /**
         * List of strings as enum names.
         */
        QStringList enumNames;

        /**
         * Whether to save the enum name instead of index or not.
         */
        bool saveEnumName;

        /**
         * Whether entries in the enum are selectable.
         */
         bool selectable;


        explicit Data(const QString &name, int defaultIndex)
                : MValueProperty<int>::Data(name, defaultIndex),
                  saveEnumName(false),
                  selectable(true)
        {}
    };
};

/**
 * Editor for enum properties.
 */
class MEnumProperty::Editor : public MProperty::Editor
{
public:
    Editor(QWidget *parent, MEnumProperty *property);

    ~Editor() override;

    void updateValue() override;

    void updateSize(int height) override;

    void setExpanding(bool expanding) override;

    /**
     * Updates only editor names.
     * Since @c updateSettings() is called on value changes too,
     * we cannot differentiate between index changes and enum names
     * changes. That is why we use this method to update only
     * the enum names.
     */
    void updateEnumNames();

    /**
     * Make the items in the enum property (not) selectable.
     * @param enabled If true, make items selectable, if false, make them not selectable.
     */
    void setSelectable(bool enabled);

public slots:

    void currentIndexChanged(int index);

protected:
    QComboBox *box;
};

} // Met3D

#endif //MET_3D_MENUMPROPERTY_H
