/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mrectproperty.h"

// standard library imports

// related third party imports
#include <QLabel>

// local application imports

namespace Met3D
{
MRectProperty::MRectProperty()
        : MValueProperty<QRectF>(new MRectProperty::Data("", {}))
{
    auto *data = getData<MRectProperty::Data>();

    // Create sub properties, that control the value of the rect.
    data->xProp = new MDoubleProperty("X", propertyValue.x());
    data->yProp = new MDoubleProperty("Y", propertyValue.y());
    data->widthProp = new MDoubleProperty("Width", propertyValue.width());
    data->heightProp = new MDoubleProperty("Height", propertyValue.height());

    addSubProperty(data->xProp);
    addSubProperty(data->yProp);
    addSubProperty(data->widthProp);
    addSubProperty(data->heightProp);

    connect(data->xProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->yProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->widthProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->heightProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
}


MRectProperty::MRectProperty(const QString &name, const QRectF &defaultValue)
        : MValueProperty<QRectF>(new MRectProperty::Data(name, defaultValue))
{
    auto *data = getData<MRectProperty::Data>();

    // Create sub properties, that control the value of the rect.
    data->xProp = new MDoubleProperty("X", propertyValue.x());
    data->yProp = new MDoubleProperty("Y", propertyValue.y());
    data->widthProp = new MDoubleProperty("Width", propertyValue.width());
    data->heightProp = new MDoubleProperty("Height", propertyValue.height());

    addSubProperty(data->xProp);
    addSubProperty(data->yProp);
    addSubProperty(data->widthProp);
    addSubProperty(data->heightProp);

    connect(data->xProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->yProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->widthProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->heightProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
}


MRectProperty::MRectProperty(const MRectProperty &other)
        : MValueProperty<QRectF>(
        new MRectProperty::Data("", {}))
{
    auto *data = getData<MRectProperty::Data>();
    auto *otherData = other.getData<MRectProperty::Data>();

    // Create sub properties, that control the value of the rect.
    data->xProp = new MDoubleProperty("X", otherData->defaultValue.x());
    data->yProp = new MDoubleProperty("Y", otherData->defaultValue.y());
    data->widthProp = new MDoubleProperty("Width", otherData->defaultValue.width());
    data->heightProp = new MDoubleProperty("Height", otherData->defaultValue.height());

    addSubProperty(data->xProp);
    addSubProperty(data->yProp);
    addSubProperty(data->widthProp);
    addSubProperty(data->heightProp);

    connect(data->xProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->yProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->widthProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);
    connect(data->heightProp, &MProperty::valueChanged,
            this, &MRectProperty::onRectChanged);

    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MRectProperty::setValue(const QRectF &value)
{
    if (value == propertyValue) return;

    auto *data = getData<MRectProperty::Data>();

    data->xProp->setValue(value.x());
    data->yProp->setValue(value.y());
    data->widthProp->setValue(value.width());
    data->heightProp->setValue(value.height());

    MValueProperty::setValue(value);
}


void MRectProperty::setStep(double step)
{
    auto *data = getData<MRectProperty::Data>();
    data->xProp->setStep(step);
    data->yProp->setStep(step);
    data->widthProp->setStep(step);
    data->heightProp->setStep(step);
}


double MRectProperty::getStep() const
{
    auto *data = getData<MRectProperty::Data>();
    return data->xProp->getStep();
}


void MRectProperty::setDecimals(int decimals)
{
    auto *data = getData<MRectProperty::Data>();
    data->xProp->setDecimals(decimals);
    data->yProp->setDecimals(decimals);
    data->widthProp->setDecimals(decimals);
    data->heightProp->setDecimals(decimals);
    updateEditorSettings();
}


int MRectProperty::getDecimals() const
{
    auto *data = getData<MRectProperty::Data>();
    return data->xProp->getDecimals();
}


void MRectProperty::onRectChanged()
{
    auto *data = getData<MRectProperty::Data>();

    QRectF rect;
    rect.setX(data->xProp->value());
    rect.setY(data->yProp->value());
    rect.setWidth(data->widthProp->value());
    rect.setHeight(data->heightProp->value());

    if (rect != propertyValue)
    {
        MValueProperty::setValue(rect);
    }
}


void MRectProperty::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(getConfigKey(), propertyValue);
}


void MRectProperty::loadFromConfiguration(QSettings *settings)
{
    setValue(settings->value(getConfigKey(), getDefaultValue()).value<QRectF>());
}


MProperty::Editor *MRectProperty::createEditor(QWidget *parentWidget)
{
    return new MRectProperty::Editor(parentWidget, this);
}


const QString MRectProperty::Editor::LABEL_FORMAT = "X: %1, Y: %2, W: %3, H: %4";


MRectProperty::Editor::Editor(QWidget *parent, MRectProperty *property)
        : MProperty::Editor(parent, property)
{
    valueLabel = new QLabel();
    valueLabel->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Fixed);

    int decimals = property->getDecimals();
    const QRectF rect = property->value();
    QString text = LABEL_FORMAT.arg(rect.x(), decimals, 'f', decimals)
                               .arg(rect.y(), decimals, 'f', decimals)
                               .arg(rect.width(), decimals, 'f', decimals)
                               .arg(rect.height(), decimals, 'f', decimals);

    valueLabel->setText(text);

    layout->addWidget(valueLabel);
}


void MRectProperty::Editor::updateSize(int height)
{
    valueLabel->setFixedHeight(height);
}


void MRectProperty::Editor::updateValue()
{
    auto *prop = dynamic_cast<MRectProperty *>(property);

    int decimals = prop->getDecimals();
    const QRectF rect = prop->value();
    QString text = LABEL_FORMAT.arg(rect.x(), decimals, 'f', decimals)
                               .arg(rect.y(), decimals, 'f', decimals)
                               .arg(rect.width(), decimals, 'f', decimals)
                               .arg(rect.height(), decimals, 'f', decimals);

    valueLabel->setText(text);
}


void MRectProperty::Editor::setExpanding(bool expanding)
{
    MProperty::Editor::setExpanding(expanding);

    if (expanding)
    {
        valueLabel->setSizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
    }
    else
    {
        valueLabel->setSizePolicy(QSizePolicy::Preferred, QSizePolicy::Fixed);
    }
}
} // Met3D