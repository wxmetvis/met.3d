/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mbuttonproperty.h"

// standard library imports
#include <utility>

// related third party imports
#include <QPushButton>
#include "log4cplus/loggingmacros.h"

// local application imports
#include "util/mutil.h"

namespace Met3D
{
MButtonProperty::MButtonProperty()
        : MProperty(new MButtonProperty::Data("", ""))
{
}


MButtonProperty::MButtonProperty(const QString &name, QString buttonText)
        : MProperty(new MButtonProperty::Data(name, std::move(buttonText)))
{
}


MButtonProperty::MButtonProperty(const MButtonProperty &other)
    : MProperty(new MButtonProperty::Data("", ""))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MButtonProperty::setButtonText(const QString &text)
{
    auto *data = getData<MButtonProperty::Data>();
    data->buttonText = text;
    updateEditorSettings();
}


MButtonProperty &MButtonProperty::operator=(const Met3D::MButtonProperty &other)
{
    MProperty::operator=(other);

    auto *otherData = other.getData<MButtonProperty::Data>();
    setButtonText(otherData->buttonText);

    return *this;
}


const QString &MButtonProperty::getButtonText() const
{
    auto *data = getData<MButtonProperty::Data>();
    return data->buttonText;
}


MProperty::Editor *MButtonProperty::createEditor(QWidget *parentWidget)
{
    return new MButtonProperty::Editor(parentWidget, this);
}


MButtonProperty::Editor::Editor(QWidget *parent,
                                MButtonProperty *property)
        : MProperty::Editor(parent, property)
{
    button = new QPushButton(property->getButtonText());
    layout->addWidget(button);

    connect(button, &QPushButton::pressed, property, &MProperty::valueChanged);
}


MButtonProperty::Editor::~Editor()
= default;


void MButtonProperty::Editor::updateSize(int height)
{
    button->setFixedHeight(height);
}


void MButtonProperty::Editor::updateSettings()
{
    button->setText(dynamic_cast<MButtonProperty *>(property)->getButtonText());
}
} // Met3D