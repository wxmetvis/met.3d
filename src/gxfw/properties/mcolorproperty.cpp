/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mcolorproperty.h"

// standard library imports

// related third party imports
#include <QColorDialog>
#include <QPainter>
#include <QChar>
#include <QMouseEvent>
#include <QLabel>
#include <QToolButton>
#include <QColor>

// local application imports

namespace Met3D
{

const QString COLOR_LABEL_FORMAT_RGB = "[%1, %2, %3]";
const QString COLOR_LABEL_FORMAT_RGBA = "[%1, %2, %3, %4]";


MColorProperty::MColorProperty()
        : MValueProperty<QColor>(
        new MColorProperty::Data("", {}))
{}


MColorProperty::MColorProperty(const QString &name, const QColor &defaultValue)
        : MValueProperty<QColor>(new MColorProperty::Data(name, defaultValue))
{}


MColorProperty::MColorProperty(const MColorProperty &other)
    : MValueProperty<QColor>(new MColorProperty::Data("", {}))
{
    // Run assignment operator to copy the other property into this one.
    *this = other;
}


void MColorProperty::setValue(const QColor &value)
{
    QColor alphaAdjustedValue = value;
    if (! hasAlpha())
    {
        alphaAdjustedValue.setAlpha(255);
    }
    MValueProperty::setValue(alphaAdjustedValue);
}


void MColorProperty::toggleAlpha(bool enableAlphaChannel)
{
    auto *data = getData<MColorProperty::Data>();
    data->useAlpha = enableAlphaChannel;

    if (!data->useAlpha)
    {
        propertyValue.setAlpha(255);
    }

    updateEditorSettings();
}


bool MColorProperty::hasAlpha() const
{
    auto *data = getData<MColorProperty::Data>();
    return data->useAlpha;
}


MColorProperty &MColorProperty::operator=(const Met3D::MColorProperty &other)
{
    MValueProperty::operator=(other);

    toggleAlpha(other.hasAlpha());

    return *this;
}


void MColorProperty::saveToConfiguration(QSettings *settings) const
{
    settings->setValue(getConfigKey(), propertyValue);
}


void MColorProperty::loadFromConfiguration(QSettings *settings)
{
    auto *data = getData<MColorProperty::Data>();
    setValue(
            settings->value(getConfigKey(), data->defaultValue).value<QColor>());
}


MProperty::Editor *
MColorProperty::createEditor(QWidget *parentWidget)
{
    return new MColorProperty::Editor(parentWidget, this);
}


MColorProperty::Editor::Editor(QWidget *parent,
                               MColorProperty *property)
        : MProperty::Editor(parent, property)
{
    pixmap = new QLabel();

    label = new QLabel();

    updateLabel();

    button = new QToolButton();

    layout->addWidget(pixmap);
    layout->addWidget(label);

    button->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Ignored);
    button->setFixedWidth(20);
    button->setText("...");

    connect(button, &QToolButton::clicked, this,
            &MColorProperty::Editor::onClick);

    layout->addWidget(button);
}


MColorProperty::Editor::~Editor()
= default;


void MColorProperty::Editor::updateSize(int height)
{
    button->setFixedWidth(height);
}


void MColorProperty::Editor::updateValue()
{
    updateLabel();
}


void MColorProperty::Editor::updateSettings()
{
    updateLabel();
}


void MColorProperty::Editor::onClick()
{
    auto prop = dynamic_cast<MColorProperty *>(this->property);

    QFlags<QColorDialog::ColorDialogOption> options;

    if (prop->hasAlpha())
    {
        options |= QColorDialog::ShowAlphaChannel;
    }

    QColor newRgba = QColorDialog::getColor(prop->value(), nullptr,
                                            tr("Choose color"),
                                            options);

    if (newRgba != prop->value() && newRgba.isValid())
    {
        prop->setUndoableValue(newRgba);
    }
}


void MColorProperty::Editor::mouseDoubleClickEvent(QMouseEvent *event)
{
    QWidget::mouseDoubleClickEvent(event);

    if (label->geometry().contains(event->pos()) ||
            pixmap->geometry().contains(event->pos()))
    {
        event->accept();
        onClick();
    }
}


void MColorProperty::Editor::updateLabel()
{
    pixmap->setPixmap(createCurrentPixmap());

    auto *prop = dynamic_cast<MColorProperty *>(property);

    QColor color = prop->value();

    QString format = COLOR_LABEL_FORMAT_RGBA;

    if (!prop->hasAlpha())
    {
        format = COLOR_LABEL_FORMAT_RGB;
    }

    format = format.arg(color.red(), 3, 10, QChar('0'))
                   .arg(color.green(), 3, 10, QChar('0'))
                   .arg(color.blue(), 3, 10, QChar('0'));

    if (prop->hasAlpha())
    {
        format = format.arg(color.alpha(), 3, 10, QChar('0'));
    }

    label->setText(format);
}


QPixmap MColorProperty::Editor::createCurrentPixmap() const
{
    QImage img(16, 16, QImage::Format_ARGB32_Premultiplied);
    img.fill(0);

    auto painter = new QPainter(&img);
    QColor color = dynamic_cast<MColorProperty *>(this->property)->value();

    if (!dynamic_cast<MColorProperty *>(this->property)->hasAlpha())
    {
        color.setAlpha(255);
    }

    // Background checker pattern dark color.
    QColor bg = QColor(125, 125, 125);
    painter->setPen(bg);

    // Create background checker pattern to be able to show alpha values.
    painter->fillRect(0, 0, 4, 4, bg);
    painter->fillRect(8, 0, 4, 4, bg);
    painter->fillRect(4, 4, 4, 4, bg);
    painter->fillRect(12, 4, 4, 4, bg);
    painter->fillRect(0, 8, 4, 4, bg);
    painter->fillRect(8, 8, 4, 4, bg);
    painter->fillRect(4, 12, 4, 4, bg);
    painter->fillRect(12, 12, 4, 4, bg);

    // Draw the color rect over the background. This previews the value of this
    // property.
    painter->setPen(color);
    painter->fillRect(0, 0, 16, 16, color);

    // Draw a black outline around the preview.
    painter->setPen({0, 0, 0});
    painter->drawRect(0, 0, 16, 16);

    delete painter;

    return QPixmap::fromImage(img);
}

} // Met3D