/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MTREEEXPANDBUTTON_H
#define MET_3D_MTREEEXPANDBUTTON_H

// standard library imports

// related third party imports
#include <QAbstractButton>

// local application imports

namespace Met3D
{

/**
 * A triangular button that switches between right and down arrow
 * when clicked.
 * It is used in trees to collapse and expand the items.
 */
class MTreeExpandButton : public QAbstractButton
{
    Q_OBJECT

public:
    /**
     * Creates a new button.
     * @param parent The parent widget.
     * @param margin The margin around the arrow of the button.
     */
    explicit MTreeExpandButton(QWidget *parent, int margin = 0);
    QSize sizeHint() const override;

    /**
     * Checks, if the button is expanded (down arrow) or not.
     * @return True if expanded.
     */
    bool isExpanded() const;

    /**
     * Sets, whether or not the arrow is visible.
     * This can be used to hide the button, but still take up the layout space.
     */
    void setShowArrow(bool show);

    /**
     * Sets the button to the desired state.
     * Emits the @c pressed signal of this button, if the state changes.
     * @param expand Whether to expand the button or not.
     */
    void setExpanded(bool expand);

signals:
    /**
     * An alternative press action that is emitted, when the shift key
     * is held while pressing the button.
     */
    void alternativePressed();

protected:
    void mousePressEvent(QMouseEvent *e) override;

    void paintEvent(QPaintEvent *e) override;

    /**
     * Margin around the arrow.
     */
    int margin;

    /**
     * Whether to display the arrow or not.
     */
    bool displayArrow;

    /**
     * Whether the button is expanded (down arrow) or not (right arrow)
     */
    bool expanded;
};

} // Met3D

#endif //MET_3D_MTREEEXPANDBUTTON_H
