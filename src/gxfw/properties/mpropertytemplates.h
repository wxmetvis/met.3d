/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPROPERTYTEMPLATES_H
#define MET_3D_MPROPERTYTEMPLATES_H

// standard library imports

// related third party imports

// local application imports
#include "mvector3dproperty.h"
#include "mpointfproperty.h"
#include "mnumberproperty.h"
#include "mboolproperty.h"
#include "mcolorproperty.h"

// forward declarations

namespace Met3D
{
/**
 * This file contains multiple property templates.
 * Meaning pre-configured properties for different use-cases.
 * This is done by implementing creator method, that are a wrapper around the constructor
 * of the property.
 * Each of these creates the property and returns a copy with the preconfigured settings.
 * Another approach is to create pre-defined structs that contain the properties of a group.
 */
class MPropertyTemplates
{
public:
    /**
     * Creates a new @c QVector3D property for geographical position in lon-lat-hPa.
     * @param name The name of the property.
     * @param defaultValue The default value.
     * @return The pre-configured property.
     */
    static MVector3DProperty GeographicPosition3D(const QString &name, const QVector3D &defaultValue);

    /**
     * Creates a new @c QPontF property for geographical position in lon-lat.
     * @param name The name of the property.
     * @param defaultValue The default value.
     * @return The pre-configured property.
     */
    static MPointFProperty GeographicPosition2D(const QString &name, const QPointF &defaultValue);

    /**
     * Creates a new @c QPointF property for viewport coordinates.
     * @param name The name of the property.
     * @param defaultValue The default value.
     * @return The pre-configured property.
     */
    static MPointFProperty ViewportPosition2D(const QString &name, const QPointF &defaultValue);

    /**
     * Creates a new @c QPointF property for viewport sizes.
     * @param name The name of the property.
     * @param defaultValue The default value.
     * @return The pre-configured property.
     * TODO: (tv, 2024Nov11): A QSize property would be better than a QPointF
     */
    static MPointFProperty ViewportSize(const QString &name, const QPointF &defaultValue);

    /**
     * Pre-defined property group for vertical limits.
     * Provides a group property and a bottom / top property.
     * Does not provide configuration keys / groups or callbacks.
     */
    struct VerticalExtent
    {
        MProperty groupProp;
        MFloatProperty bottomProp;
        MFloatProperty topProp;

        explicit VerticalExtent(float bottom, float top);
    };

    /**
     * Pre-defined property group for vertical limits in hPa.
     * Provides a group property and a bottom / top property.
     * Does not provide configuration keys / groups or callbacks.
     */
    struct VerticalExtentHPa : public VerticalExtent
    {
        explicit VerticalExtentHPa(float bottom = 1050.f, float top = 100.f);
    };

    /**
     * Pre-defined property group for vertical limits in meters.
     * Provides a group property and a bottom / top property.
     * Does not provide configuration keys / groups or callbacks.
     */
    struct VerticalExtentM : public VerticalExtent
    {
        explicit VerticalExtentM(float bottom = 0, float top = 10000);
    };

    /**
     * Predefined group for label settings.
     * Does not provide configuration keys / groups or callbacks.
     */
    struct Labels
    {
        MBoolProperty enabledProp;
        MIntProperty fontSizeProp;
        MColorProperty fontColourProp;
        MBoolProperty enableBBoxProp;
        MColorProperty bboxColourProp;

        explicit Labels(bool enabled = false,
               int fontSize = 24,
               const QColor& fontColour = QColor(0, 0, 0),
               bool enableBBox = true,
               const QColor& bboxColour = QColor(255, 255, 255, 200));
    };
};

} // Met3D

#endif //MET_3D_MPROPERTYTEMPLATES_H
