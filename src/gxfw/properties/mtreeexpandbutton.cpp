/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mtreeexpandbutton.h"

// standard library imports

// related third party imports
#include <QPainter>
#include <QMouseEvent>
#include <QPaintEvent>
#include <QSizePolicy>

// local application imports

namespace Met3D
{

MTreeExpandButton::MTreeExpandButton(QWidget *parent, int margin)
        : QAbstractButton(parent),
          margin(margin),
          displayArrow(true),
          expanded(false)
{
    resize(20, 15);
    setMaximumSize(QSize(20, 15));
    setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    setFocusPolicy(Qt::NoFocus);
}


QSize MTreeExpandButton::sizeHint() const
{
    return {20, 15};
}


void MTreeExpandButton::paintEvent(QPaintEvent *e)
{
    if (!displayArrow) return;

    QSize currSize = size();
    auto width = static_cast<float>(currSize.width() - 5);
    auto height = static_cast<float>(currSize.height());
    auto fmargin = static_cast<float>(margin);
    float arrowWidth = width - fmargin * 2.0f;
    float arrowHeight = height - fmargin * 2.0f;

    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);

    static const QPointF expandedArrow[3] = {
            QPointF(fmargin, fmargin + arrowHeight / 3.0f),
            QPointF(fmargin + arrowWidth, fmargin + arrowHeight / 3.0f),
            QPointF(fmargin + arrowWidth / 2.0f, fmargin + arrowHeight)
    };

    static const QPointF collapsedArrow[3] = {
            QPointF(fmargin + arrowWidth / 3.0f, fmargin),
            QPointF(fmargin + arrowWidth, fmargin + arrowHeight / 2.0f),
            QPointF(fmargin + arrowWidth / 3.0f, fmargin + arrowHeight)
    };

    painter.setBrush(palette().dark());
    painter.setPen(palette().shadow().color());
    if (expanded)
    {
        painter.drawConvexPolygon(expandedArrow, 3);
    }
    else
    {
        painter.drawConvexPolygon(collapsedArrow, 3);
    }
}


bool MTreeExpandButton::isExpanded() const
{
    return expanded;
}


void MTreeExpandButton::setShowArrow(bool show)
{
    displayArrow = show;
    update();
}


void MTreeExpandButton::setExpanded(bool expand)
{
    if (expanded == expand) return;
    expanded = expand;
    emit pressed();
    update();
}



void MTreeExpandButton::mousePressEvent(QMouseEvent *e)
{
    if (e->button() != Qt::LeftButton) return;
    if (hitButton(e->pos()))
    {
        expanded = !expanded;
        repaint();

        if (e->modifiers() & Qt::ShiftModifier)
        {
            emit alternativePressed();
        }
        else
        {
            emit pressed();
        }
    }
}

} // Met3D