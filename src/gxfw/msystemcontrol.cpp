/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2022-2023 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "msystemcontrol.h"

// standard library imports
#include <iostream>
#include <stdexcept>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QApplication>

// local application imports
#include "mainwindow.h"
#include "gxfw/msceneviewglwidget.h"
#include "util/mutil.h"
#include "data/lrumemorymanager.h"
#include "system/mpyinterface.h"
#include "system/scheduling/scheduler.h"

namespace Met3D
{

MSystemManagerAndControl* MSystemManagerAndControl::instance = 0;

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSystemManagerAndControl::MSystemManagerAndControl(QWidget *parent) :
    QWidget(parent),
    met3dAppIsInitialized(false),
    connectedToMetview(false),
    uiReady(false),
    batchModeIsActive(false),
    batchModeTimeRange_sec(0),
    batchModeQuitWhenCompleted(false),
    batchModeOverwriteImages(true),
    lastFocusedSceneView(nullptr),
    mainWindow(nullptr),
    colourmapPool(new MColourmapPool())
{
    LOG4CPLUS_INFO(mlog, "Initialising system manager...");

    sysManagerLayout = new QVBoxLayout(this);

    // Properties of the scene view are displayed in a tree property browser
    // widget.
    systemPropertiesBrowser = new MPropertyTree(this);
    systemPropertiesBrowser->setBaseLevelColor(QColor(191, 255, 191));
    systemPropertiesBrowser->setColorGenerationMethod(MPropertyTree::ColorMethod::LIGHTEN);

    sysManagerLayout->addWidget(systemPropertiesBrowser);
    setLayout(sysManagerLayout);

    // Insert a dummy "None" entry into the list of waypoints models.
    waypointsTableModelPool.insert("None", nullptr);
    syncControlPool.insert("None", nullptr);
    boundingBoxPool.insert(QString("None"), nullptr);

    // Add group containing general application configurations.
    appConfigProp = MProperty("Application-wide settings");
    systemPropertiesBrowser->addProperty(&appConfigProp);

    imageExportGroupProp = MProperty("Image export");
    appConfigProp.addSubProperty(imageExportGroupProp);

    screenshotQualityProp = MIntProperty("Quality of saved images", 70);
    screenshotQualityProp.setConfigKey("quality_of_saved_images");
    screenshotQualityProp.setSuffix(" %");
    screenshotQualityProp.setMinMax(0, 100);
    screenshotQualityProp.setTooltip("Specify image quality when saving visualizations to file. "
                                     "The effect of this value depends on the image file format. "
                                     "A value of 0 corresponds to highest compression, 100 is least compressed.");
    imageExportGroupProp.addSubProperty(screenshotQualityProp);

    // Add group containing .
    allSceneViewsGroupProp = MProperty("Scene-view specific settings");
    allSceneViewsGroupProp.expand();
    systemPropertiesBrowser->addProperty(&allSceneViewsGroupProp);

    memoryManagersGroupProp = MProperty("System information");
    systemPropertiesBrowser->addProperty(&memoryManagersGroupProp);

    debugSchedulerProp = MButtonProperty("Scheduler state", "Dump");
    debugSchedulerProp.setTooltip("Dump the current state of the scheduler.");
    debugSchedulerProp.registerValueCallback([=]()
    {
        MScheduler::getInstance()->printCurrentState();
    });
    memoryManagersGroupProp.addSubProperty(debugSchedulerProp);

    // Determine the Met.3D home directory (the base directory to find
    // shader files and data files that do not change).
    QString met3DHomeDirString =
        QProcessEnvironment::systemEnvironment().value("MET3D_HOME");
    makePathAbsolute(met3DHomeDirString);
    met3DHomeDir = QDir(met3DHomeDirString);
    LOG4CPLUS_INFO(mlog, "  > MET3D_HOME set to "
                    << met3DHomeDir.absolutePath());

    // Determine the Met.3D base directory (useful to find third-party files).
    QString met3DBaseDirString =
        QProcessEnvironment::systemEnvironment().value("MET3D_BASE");
    makePathAbsolute(met3DBaseDirString);
    met3DBaseDir = QDir(met3DBaseDirString);
    LOG4CPLUS_INFO(mlog, "  > MET3D_BASE set to "
                             << met3DBaseDir.absolutePath());

    defaultMemoryManagers.insert("NWP", "");
    defaultMemoryManagers.insert("Analysis", "");
    defaultMemoryManagers.insert("Trajectories", "");

    // Register meta types for signal-slot functionality.
    qRegisterMetaType<Met3D::MSyncEvent>();
    qRegisterMetaType<MDataRequest>("MDataRequest");
}


MSystemManagerAndControl::~MSystemManagerAndControl()
{
    // release all registered resources
    LOG4CPLUS_INFO(mlog, "Freeing system resources...");

    LOG4CPLUS_DEBUG(mlog, "\tscheduler");
    MScheduler::deleteInstance();

    // currently not working because of data request failure

    /*LOG4CPLUS_DEBUG(mlog, "\tmemory manager pool" << flush);
    for (auto it = memoryManagerPool.begin();
               it != memoryManagerPool.end(); ++it)
    {
        std::string key = it.key().toStdString();
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleted ''" << key << "''" << flush);

        delete it.value();
    }*/

    LOG4CPLUS_DEBUG(mlog, "\tdata source pool");
    for (MAbstractDataSource *dataSource : dataSourcePool)
    {
        std::string key = dataSourcePool.key(dataSource).toStdString();
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting \''" << key << "''");
        delete dataSource;
    }

    LOG4CPLUS_DEBUG(mlog, "\tsynchronization control pool");
    for (MSyncControl *syncControl : syncControlPool)//auto it = syncControlPool.begin(); it != syncControlPool.end(); ++it)
    {
        std::string key = syncControlPool.key(syncControl).toStdString();
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting \''" << key << "''");
        delete syncControl;
    }

    LOG4CPLUS_DEBUG(mlog, "\twaypoints model pool");
    for (MWaypointsTableModel *waypointsTableModel : waypointsTableModelPool)
    {
        std::string key =
                waypointsTableModelPool.key(waypointsTableModel).toStdString();
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting \''" << key << "''");
        delete waypointsTableModel;
    }

    LOG4CPLUS_DEBUG(mlog, "\tbounding box pool");
    for (auto it = boundingBoxPool.begin(); it != boundingBoxPool.end(); ++it)
    {
        std::string key = it.key().toStdString();
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting \''" << key << "''");
        delete it.value();
    }

    LOG4CPLUS_DEBUG(mlog, "\tcamera sequence model pool");
    for (int i = 0; i < cameraSequencePool.length(); i++)
    {
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting \''" << i << "''");

        delete cameraSequencePool[i];
    }

    delete systemPropertiesBrowser;
    delete sysManagerLayout;
    delete colourmapPool;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MSystemManagerAndControl* MSystemManagerAndControl::getInstance(QWidget *parent)
{
    if (MSystemManagerAndControl::instance == nullptr)
    {
        MSystemManagerAndControl::instance = new MSystemManagerAndControl(parent);
    }

    return MSystemManagerAndControl::instance;
}


void MSystemManagerAndControl::storeApplicationCommandLineArguments(
    QCommandLineParser *arguments)
{
    commandLineArguments = arguments;

    // Parse command line arguments to check if application has been started
    // from Metview.
    if (arguments->isSet("metview"))
    {
        connectedToMetview = true;
        QString msg = QString("Starting in Metview mode. ");
        LOG4CPLUS_INFO(mlog, msg);
    }
}


bool MSystemManagerAndControl::isCommandLineArgumentSet(const QString& optionName) const
{
    return commandLineArguments->isSet(optionName);
}


QString MSystemManagerAndControl::getCommandLineArgumentValue(const QString& optionName) const
{
    return commandLineArguments->value(optionName);
}


QStringList MSystemManagerAndControl::getCommandLineArgumentValues(const QString& optionName) const
{
    return commandLineArguments->values(optionName);
}


const QDir& MSystemManagerAndControl::getMet3DHomeDir() const
{
    return met3DHomeDir;
}


const QDir& MSystemManagerAndControl::getMet3DBaseDir() const
{
    return met3DBaseDir;
}


void MSystemManagerAndControl::setMet3DWorkingDirectory(QString workingDir)
{
    this->met3DWorkingDirectory = QDir(workingDir);
}


void MSystemManagerAndControl::registerSceneView(MSceneViewGLWidget *view)
{
    // Add the view's properties to the property browser.
    allSceneViewsGroupProp.addSubProperty(view->getPropertyGroup());

    registeredViews.append(view);
}

MSceneViewGLWidget *MSystemManagerAndControl::getLastInteractedSceneView()
{
    if (!lastFocusedSceneView)
    {
        lastFocusedSceneView = registeredViews.first();
    }
    return lastFocusedSceneView;
}


void MSystemManagerAndControl::setMainWindow(MMainWindow *window)
{
    mainWindow = window;
}


MMainWindow* MSystemManagerAndControl::getMainWindow()
{
    return mainWindow;
}


bool MSystemManagerAndControl::isUiReady() const
{
    return uiReady;
}


void MSystemManagerAndControl::uiIsReady()
{
    uiReady = true;

    auto sessionMgr = mainWindow->getSessionManagerDialog();
    // Load the session if requested by frontend config.
    if (sessionMgr->getLoadSessionOnStart())
    {
        sessionMgr->loadSessionOnStart();
    }

    // Add actors to be loaded from file on start.
    if (isCommandLineArgumentSet("actors"))
    {
        QString actorsValue = getCommandLineArgumentValue("actors");
        QStringList actorConfigs = actorsValue.split(";", Qt::SkipEmptyParts);

        auto glRM = MGLResourcesManager::getInstance();
        for (const QString &actor : actorConfigs)
        {
            QVector<MSceneControl *> scenes = glRM->getScenes().toVector();
            MAbstractActorFactory::createAndRegisterActorFromFile(
                    expandEnvironmentVariables(actor), scenes);
        }
    }

    // If batch mode has been enabled in the configuration stage, start
    // batch mode execution.
    //NOTE: See documentation of batch mode behaviour in Doxygen doc of
    // MSystemManagerAndControl::executeBatchMode().
    if (isInBatchMode())
    {
        LOG4CPLUS_INFO(mlog, "Batch mode is enabled. Invoking batch execution.");
        executeBatchMode();
    }
}


void MSystemManagerAndControl::registerMemoryManager(
        const QString& id, MAbstractMemoryManager* memoryManager)
{
    memoryManagerPool.insert(id, memoryManager);
}


void MSystemManagerAndControl::registerDataSource(
        const QString &id, MAbstractDataSource* dataSource)
{
    dataSourcePool.insert(id, dataSource);
}


QStringList MSystemManagerAndControl::getDataSourceIdentifiers() const
{
    return dataSourcePool.keys();
}


MAbstractMemoryManager* MSystemManagerAndControl::getMemoryManager(
        const QString& id) const
{
    return memoryManagerPool.value(id);
}


QStringList MSystemManagerAndControl::getMemoryManagerIdentifiers() const
{
    return memoryManagerPool.keys();
}


MAbstractDataSource* MSystemManagerAndControl::getDataSource(
        const QString& id) const
{
    if ( !dataSourcePool.contains(id) )
        return nullptr;
    else
        return dataSourcePool.value(id);
}


void MSystemManagerAndControl::registerSyncControl(MSyncControl *syncControl)
{
    syncControlPool.insert(syncControl->getID(), syncControl);

    for (MSceneViewGLWidget *sceneview : registeredViews)
    {
        sceneview->updateSyncControlProperty();
    }
}


MSyncControl *MSystemManagerAndControl::getSyncControl(
        const QString &id) const
{
    if ( !syncControlPool.contains(id) )
    {
        LOG4CPLUS_WARN(mlog, "Synchronization control with ID "
                        << id << " is not available!");
        return nullptr;
    }
    return syncControlPool.value(id);
}


QStringList MSystemManagerAndControl::getSyncControlIdentifiers() const
{
    return syncControlPool.keys();
}


void MSystemManagerAndControl::removeSyncControl(MSyncControl *syncControl)
{
    syncControlPool.remove(syncControl->getID());
    delete syncControl;

    for (MSceneViewGLWidget *sceneview : registeredViews)
    {
        sceneview->updateSyncControlProperty();
    }
}


void MSystemManagerAndControl::registerWaypointsModel(MWaypointsTableModel *wps)
{
    waypointsTableModelPool.insert(wps->getID(), wps);
}


MWaypointsTableModel *MSystemManagerAndControl::getWaypointsModel(
        const QString &id) const
{
    if ( !waypointsTableModelPool.contains(id) )
    {
        LOG4CPLUS_WARN(mlog, "Waypoints model with ID " << id
                        << " is not available!");
        return nullptr;
    }
    return waypointsTableModelPool.value(id);
}


QStringList MSystemManagerAndControl::getWaypointsModelsIdentifiers() const
{
    return waypointsTableModelPool.keys();
}

int MSystemManagerAndControl::registerCameraSequence(MCameraSequence *sequence)
{
    cameraSequencePool.append(sequence);

    emit cameraSequenceRegistered();

    return cameraSequencePool.length() - 1;
}

void MSystemManagerAndControl::removeAllCameraSequences()
{
    emit cameraSequencesCleared(); // Need to do that first, before deleting the sequence objects

    for (int i = 0; i < cameraSequencePool.length(); i++)
    {
        delete cameraSequencePool[i];
    }
    cameraSequencePool.clear();
}

void MSystemManagerAndControl::removeCameraSequence(int index)
{
    if (index < 0 || index >= cameraSequencePool.length())
    {
        LOG4CPLUS_WARN(mlog, "Camera sequence " << index
                        << " cannot be removed! It does not exist.");
    }
    MCameraSequence* sequence = cameraSequencePool[index];
    cameraSequencePool.removeAt(index);

    emit cameraSequenceRemoved(index);

    delete sequence;
}

void MSystemManagerAndControl::renameCameraSequence(int index, QString name)
{
    if (index < 0 || index >= cameraSequencePool.length())
    {
        LOG4CPLUS_WARN(mlog, "Camera sequence " << index
                        << " cannot be renamed! It does not exist.");
    }
    MCameraSequence* sequence = cameraSequencePool[index];
    sequence->sequenceName = name;

    emit cameraSequenceRenamed(index, name);
}

MCameraSequence *MSystemManagerAndControl::getCameraSequence(const int index) const
{
    if (index < 0 || index >= cameraSequencePool.length())
    {
        LOG4CPLUS_WARN(mlog, "Camera sequence " << index
                        << " is not available!");

        return nullptr;
    }

    return cameraSequencePool[index];
}

QList<MCameraSequence *> MSystemManagerAndControl::getCameraSequences() const
{
    return QList<MCameraSequence *>(cameraSequencePool);
}


void MSystemManagerAndControl::registerBoundingBox(MBoundingBox *bbox)
{
    boundingBoxPool.insert(bbox->getID(), bbox);
    emit boundingBoxCreated();
}


void MSystemManagerAndControl::deleteBoundingBox(const QString& id)
{
    delete boundingBoxPool.value(id);
    boundingBoxPool.remove(id);
    emit boundingBoxDeleted(id);
}


void MSystemManagerAndControl::renameBoundingBox(const QString& oldId,
                                                 MBoundingBox *bbox)
{
    boundingBoxPool.remove(oldId);
    boundingBoxPool.insert(bbox->getID(), bbox);
    emit boundingBoxRenamed();
}


MBoundingBox *MSystemManagerAndControl::getBoundingBox(const QString &id) const
{
    if ( !boundingBoxPool.contains(id) )
    {
        LOG4CPLUS_WARN(mlog, "Bounding box with ID " << id
                        << " is not available!");
        return nullptr;
    }
    return boundingBoxPool.value(id);
}


QStringList MSystemManagerAndControl::getBoundingBoxesIdentifiers() const
{
    return boundingBoxPool.keys();
}


MBoundingBoxDockWidget *MSystemManagerAndControl::getBoundingBoxDock() const
{
    return mainWindow->getBoundingBoxDock();
}


double MSystemManagerAndControl::elapsedTimeSinceSystemStart(
        const MStopwatch::TimeUnits units)
{
    systemStopwatch.split();
    return systemStopwatch.getElapsedTime(units);
}


void MSystemManagerAndControl::setBatchMode(bool isActive, QString animType, QString syncName,
        QString dataSourceIDForStartTime, int timeRange_sec,
        bool quitWhenCompleted, bool overwriteImages)
{
    batchModeIsActive = isActive;
    batchModeAnimationType = animType;
    syncControlForBatchModeAnimation = syncName;
    batchModeDataSourceIDToGetStartTime = dataSourceIDForStartTime;
    batchModeTimeRange_sec = timeRange_sec;
    batchModeQuitWhenCompleted = quitWhenCompleted;
    batchModeOverwriteImages = overwriteImages;
}


bool MSystemManagerAndControl::isInBatchMode()
{
    return batchModeIsActive;
}


void MSystemManagerAndControl::initializeColourmapPool(QStringList directories)
{
    colourmapPool->initializePool();

    for (const QString& dir : directories)
    {
        QDirIterator iter(dir, QDir::Files | QDir::NoDotAndDotDot, QDirIterator::Subdirectories);

        QString filename;

        while (iter.hasNext())
        {
            filename = iter.next();

            // Vapor XML
            if (filename.endsWith(".vtf", Qt::CaseInsensitive))
            {
                if (colourmapPool->loadVaporXmlColourmap(filename))
                {
                    LOG4CPLUS_INFO(mlog, "Vapor XML " << filename << " loaded.");
                }
            }
            // XML files (ex. Paraview colourmap XML)
            else if (filename.endsWith(".xml", Qt::CaseInsensitive))
            {
                if (colourmapPool->loadParaviewXmlColourmap(filename))
                {
                    LOG4CPLUS_INFO(mlog, "ParaView colourmap XML "
                        << filename << " loaded.");
                }
                else
                {
                    LOG4CPLUS_ERROR(mlog, "File " << filename
                        << " is not a known colourmap format to Met.3D.");
                }
            }
            // Unsupported files
            else
            {
                QString ext = filename.split(".").last();
                LOG4CPLUS_WARN(mlog, "Met.3D currently does not support or "
                    "recognize colourmaps with the " << ext << " extension.");
            }
        }
    }
}


MColourmapPool *MSystemManagerAndControl::getColourmapPool() const
{
    return colourmapPool;
}


void MSystemManagerAndControl::executeBatchMode()
{
    //NOTE: See documentation of batch mode behaviour in Doxygen doc of
    // MSystemManagerAndControl::executeBatchMode() (in the header file).

    LOG4CPLUS_INFO(mlog, "");
    LOG4CPLUS_INFO(mlog, "Starting BATCH MODE execution.");

    // Check if sync control and data source configured for batch mode exist.
    MSyncControl *syncControl = getSyncControl(syncControlForBatchModeAnimation);
    if (syncControl == nullptr)
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Batch mode execution: Synchronization control'"
                       + syncControlForBatchModeAnimation +
                       "' specified in frontend configuration is not available. "
                       "Batch mode will NOT be executed.");
        msgBox.exec();
        return;
    }

    if (getDataSource(batchModeDataSourceIDToGetStartTime) == nullptr)
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Batch mode execution: Data source '"
                       + batchModeDataSourceIDToGetStartTime +
                       "' specified in frontend configuration is not available. "
                       "Batch mode will NOT be executed.");
        msgBox.exec();
        return;
    }

    // Restrict the sync control's allowed init/valid time to those available
    // from the data source. Also set the init/valid time GUI elements,
    // and set the animation time range.
    QStringList dataSources;
    dataSources.append(batchModeDataSourceIDToGetStartTime);
    syncControl->restrictControlToDataSources(dataSources);
    // Select the most recent (denoted by "-1") available init time from the
    // data source.
    //NOTE (mr, 27Sept2021) -- this can later be changed to allow selection
    // of an arbitrary init time from the config file.
    LOG4CPLUS_INFO(mlog, "BATCH MODE: Selecting most recent init time.");
    syncControl->selectInitTimeAsListPosition(-1);
    syncControl->setAnimationTimeRange(
                syncControl->initDateTime(),
                syncControl->initDateTime().addSecs(batchModeTimeRange_sec));

    // Force to overwrite image files that already exist?
    syncControl->setOverwriteAnimationImageSequence(batchModeOverwriteImages);

    // W.r.t. animation type, currently only the 'timeAnimation' and 'cameraSequenceTimeAnimation'
    // options are implemented.
    if (batchModeAnimationType == "timeAnimation" ||
            batchModeAnimationType == "cameraSequenceTimeAnimation")
    {
        // If configured so, connect the sync control's "timeAnimationEnds"
        // signal to the "closeMainWindow" slot to automatically quit the
        // application after the batch animation has finished. Use
        // "Qt::QueuedConnection" as described here:
        // https://doc.qt.io/qt-5/qcoreapplication.html#quit
        if (batchModeQuitWhenCompleted)
        {
            connect(syncControl, SIGNAL(timeAnimationEnds()), this,
                    SLOT(closeMainWindow()), Qt::QueuedConnection);
        }
        // When using cameraSequenceTimeAnimation as the animation type,
        // setup synccontrol to use the camera sequence specified in
        // the scene settings to animate the camera during the time animation.
        if (batchModeAnimationType == "cameraSequenceTimeAnimation")
        {
            syncControl->setupCameraSequenceBatchMode(true);
        }
        else
        {
            syncControl->setupCameraSequenceBatchMode(false);
        }

        // Batch mode: Connect a signal so we know then the processing of the
        // first frame has finished. After the first frame has finished
        // rendering, we start the animation via this slot and take the first
        // screenshot.
        auto scheduler = MScheduler::getInstance();
        if (scheduler->isProcessing())
        {
            batchModeSchedulerSignalSlot =
                connect(scheduler,
                   &MScheduler::schedulerIsProcessing,
                   this, [=](bool b)
                   {
                       // Check if scheduler is finished.
                       if (scheduler->isProcessing()) return;

                       // Disconnect all this created connection.
                       disconnect(batchModeSchedulerSignalSlot);

                       // Start time animation.
                       syncControl->startTimeAnimationProgrammatically();

                   }, Qt::QueuedConnection);
        }
        else
        {
            syncControl->startTimeAnimationProgrammatically();
        }
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("Batch mode execution: Animation type '"
                       + batchModeAnimationType +
                       "' is not supported ('timeAnimation' is supported.). "
                       "Batch mode will NOT be executed.");
        msgBox.exec();
        return;
    }
}


void MSystemManagerAndControl::setApplicationConfigurationValue(
        QString key, QVariant item)
{
    applicationConfigurationValues.insert(key, item);
}


QVariant MSystemManagerAndControl::getApplicationConfigurationValue(QString key)
{
    return applicationConfigurationValues.value(key);
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


void MSystemManagerAndControl::closeMainWindow()
{
    LOG4CPLUS_INFO(mlog, "System manager received command to quit the "
                    " application. Closing main window... ");

    mainWindow->close();
}

void MSystemManagerAndControl::setLastFocus(MSceneViewGLWidget *widget)
{
    lastFocusedSceneView = widget;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MSystemManagerAndControl::setApplicationIsInitialized()
{
    met3dAppIsInitialized = true;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

} // namespace Met3D
