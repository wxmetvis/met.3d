/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef ADDTRAJECTORYFILTERDIALOG_H
#define ADDTRAJECTORYFILTERDIALOG_H

// standard library imports

// related third party imports
#include <QDialog>

// local application imports


namespace Ui {
class MAddTrajectoryFilterDialog;
}

namespace Met3D
{

/**
 * Dialog class used to select a trajectory filter from the list of currently
 * registered trajectory filter types. Registration takes place in the
 * @c MTrajectoryFilterFactory.
 */
class MAddTrajectoryFilterDialog : public QDialog
{
    Q_OBJECT
    
public:
    /**
     * Creates a new dialog and fills the table with the registered
     * trajectory filters.
     */
    explicit MAddTrajectoryFilterDialog(QWidget *parent = nullptr);

    ~MAddTrajectoryFilterDialog() override;

    /**
     * @return The currently selected entry in the table, or an empty string if
     * no selection is made.
     */
    QString getSelectedFilterType();

private:
    /**
     * Helper method that fills the table with the trajectory filters.
     */
    void createEntries();

    Ui::MAddTrajectoryFilterDialog *ui;
};

} // namespace Met3D

#endif // ADDTRAJECTORYFILTERDIALOG_H
