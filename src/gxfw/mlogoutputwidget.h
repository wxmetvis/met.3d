/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MLOGOUTPUTWIDGET_H
#define MLOGOUTPUTWIDGET_H

// standard library imports

// related third party imports
#include <QWidget>
#include <QKeyEvent>
#include <QTextEdit>
#include <QCheckBox>

// local application imports
#include "util/mlogger.h"

namespace Ui
{
class MLogOutputWidget;
}

namespace Met3D
{
class MLogOutputWidget : public QWidget
{
Q_OBJECT

public:
    explicit MLogOutputWidget(QWidget *parent = nullptr);

    ~MLogOutputWidget() override;

protected:
    void keyPressEvent(QKeyEvent *event) override;

private slots:
    void onLogMessageReceived(MLogger::MLogMessage *message);
    void onSave();
    void onClear();
    void onSearchButtonPressed();
    void onSearchTextChanged(const QString& text);
    void onLogTextChanged();
    void onSearchPrevious();
    void onSearchNext();
    void onLogLevelFilterChanged(int state);

private:
    /**
     * Print @p message to the logging window.
     * @param message Log message to print.
     */
    void printLogMessage(MLogger::MLogMessage* message);

    /**
     * Highlight all results for @c searchText in the log.
     */
    void highlightSearchResults();

    /**
     * Highlight new results for @c searchText in the log.
     */
    void highlightNewSearchResults();

    Ui::MLogOutputWidget *ui;
    bool searchMode;
    QString searchText;

    QList<QTextEdit::ExtraSelection> searchHighlights;
    QMap<log4cplus::LogLevel, QCheckBox*> logLevelCheckboxes;
};
}

#endif // MLOGOUTPUTWIDGET_H
