/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2023 Christoph Fischer [*]
**  Copyright 2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MACTOR_H
#define MACTOR_H

// standard library imports
#include <memory>

// related third party imports
#include <QtCore>
#include <log4cplus/loggingmacros.h>
#include <QGuiApplication>

// local application imports
#include "gxfw/mtypes.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/vertexbuffer.h"
#include "gxfw/camera.h"
#include "mactorcomponent.h"
#include "util/mutil.h"
#include "gxfw/progressbar.h"
#include "gxfw/properties/mproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/marrayproperty.h"
#include "gxfw/properties/mcolorproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mpropertytemplates.h"
#include "util/mfileutils.h"

namespace Met3D
{

class MGLResourcesManager;
class MSceneViewGLWidget;
class MSceneControl;


/**
  @brief MActor is the abstract base class for all actors.

  It inherits from QObject to use Qt's signal/slot mechanism.
  */
class MActor : public QObject
{
    Q_OBJECT

public:
    /**
      Constructs a new MActor with a unique identification number (@ref
      getID()) and initialises a property group for the new actor. The MActor
      property group handles properties common for all actors, derived actor
      classes should add their own properties in their constructor.

      @note This constructor NEEDS to be called from derived constructors.
      @param supportsShadows Whether this actor supports scene shadows or not.
      */
    MActor(QObject *parent = 0, bool supportsShadows = true);

    /**
      */
    ~MActor();

    /**

     */
    void initialize();

    /**
      Initialise OpenGL resources of the actor that cannot be shared between
      OpenGL contexts. According to the OpenGL 4.2 core specification, Appendix
      D, this includes:

      "Objects which contain references to other objects include framebuffer,
      program pipeline, query, and vertex array objects. Such objects are
      called container objects and are not shared."

      @see initializeActorResources()
      */
    virtual void initializePerGLContextResources(MSceneViewGLWidget *sceneView)
    {
        Q_UNUSED(sceneView);
    }

    /**
      If the actor is enabled (property @ref enabled is set to true), render
      the actor in the current OpenGL context. Calls the virtual method @ref
      renderToCurrentContext() to perform the actual rendering in the derived
      classes. Do not overwrite this method, but reimplement
      @ref renderToCurrentContext() instead.
      */
    void render(MSceneViewGLWidget *sceneView);

    /**
      If the actor is enabled (property @ref enabled is set to true), render
      the actor in the current OpenGL context in full-screen mode. Calls the
      virtual method @ref renderToCurrentContextFullScreen() to perform the
      actual rendering in the derived classes. Do not overwrite this method,
      but reimplement @ref renderToCurrentContextFullScreen() instead.
      */
    void renderToFullScreen(MSceneViewGLWidget *sceneView);

    /**
      If the actor is enabled (property enabled is set to true), render
      the actors 2D UI elements in the current OpenGL context' UI layer.
      Calls the virtual method renderToCurrentContextUiLayer() to perform the actual rendering in the derived
      classes. Do not overwrite this method, but reimplement
      renderToCurrentContextUiLayer() instead.
      */
    void renderToUiLayer(MSceneViewGLWidget *sceneView);

    /**
      Returns whether the actor has been initialised. Usually true after @ref
      initialize() has been called.
      */
    bool isInitialized();

    /**
      Returns true if the actor is enabled.
     */
    bool isEnabled() const;

    /**
      Recompiles the actor's GLSL shaders. Needs to call
      @ref beginCompileShaders() and @ref endCompileShaders().
      Does not compile actor's components shaders.
      */
    virtual void reloadShaderEffects() = 0;

    /**
      Calls the actors @ref reloadShaderEffects() implementation,
      and also compiles the shaders of all currently added components.
      @brief reloadShaderEffects
     */
    void reloadAllShaderEffects();

    /**
      Returns actor type.
    */
    static QString staticActorType() { return "Default actor"; }

    /**
      Returns actor type.

      Note: Actor type needs to be set in the constructor.
    */
    QString getActorType() { return actorType; }

    /**
      Returns the file name of the actor's icon for the toolbar. The icon is
      expected to be location in the images/actors/ folder.
      Using the fallback icon if actor does not implement an icon.
      */
    static QString staticIconFileName() { return "missing.png"; }

    /**
      Returns a unique number that identifies the actor.
      */
    unsigned int getID() const;

    /**
      Sets the name of the actor.
      */
    void setName(const QString& name);

    /**
      Returns the name of the actor (set by @ref setName()).
      */
    QString getName() const;

    /**
      Returns a @c MProperty instance acting as parent to all the properties
      of this actor (i.e. representing the properties "group").
      */
    MProperty* getPropertyGroup() { return &actorIsEnabledProp; }

    /**
      Informs this actor that it has been added to the @ref MSceneControl @p
      scene. The scene is added to the protected property @ref scenes. If the
      scene is already contained in @ref scenes, it is not added a second time.

      <em> Do not call this function directly. </em> It is implicitely called
      from @ref MSceneControl::addActor(), which should be used to add an actor
      to a scene.

      @see MSceneControl::addActor()
      */
    virtual void registerScene(MSceneControl *scene);

    virtual void deregisterScene(MSceneControl *scene);

    /**
     * Slot called whenever this actor has been added to a scene view.
     */
    virtual void onSceneViewAdded() {}

    /**
     Provides information of how many elements (e.g. NWP variables) of this
     actor are connected to a MSyncControl. Needs to be reimplemented in
     derived classes and MSceneControl::variableSynchronizesWith() needs to be
     called accordingly.
     */
    virtual void provideSynchronizationInfoToScene(MSceneControl *scene)
    {
        Q_UNUSED(scene);
    }

    /**
      Returns a list of the scenes in which this actor has been registered.
     */
    const QList<MSceneControl*>& getScenes() { return scenes; }

    /**
      Clears list of the scenes in which this actor has been registered.
     */
    void clearScenes() { scenes.clear(); }

    /**
      Returns a list of the views in which this actor appears.
     */
    const QList<MSceneViewGLWidget*> getViews();

    /**
      Returns a list with the labels that shall be rendered for this actor.
      */
    QList<MLabel*> getLabelsToRender();

    /**
      Returns the position label in a list if it shall be rendered for this actor.
      */
    virtual QList<MLabel*> getPositionLabelToRender();

    /**
      Returns @p true if the actor can be picked and dragged in modify mode.
      */
    bool isPickable() { return actorIsPickable; }

    /**
      Returns a handle ID if the clip space coordinates of a handle of this
      actor are within @p clipRadius of (@p clipX, @p clipY); or -1 otherwise.

      Called by a @ref MSceneViewGLWidget in modification mode.
      */
    int checkIntersectionWithHandleAndAllComponents(MSceneViewGLWidget *sceneView,
                                                    float clipX, float clipY);

    /**
      Returns a handle ID if the clip space coordinates of a handle of this
      actor are within @p clipRadius of (@p clipX, @p clipY); or -1 otherwise.

      Called by the actor before calling component intersections.
      */
    virtual int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                            float clipX, float clipY)
    {
        Q_UNUSED(sceneView);
        Q_UNUSED(clipX);
        Q_UNUSED(clipY);
        return -1;
    }

    /**
      Creates a label representing the position of handle with ID @p handleID
      near the handle.

      Called by a @ref MSceneViewGLWidget in modification mode.
      */
    virtual void addPositionLabel(MSceneViewGLWidget *sceneView, int handleID,
                                  float clipX, float clipY)
    {
        Q_UNUSED(sceneView);
        Q_UNUSED(handleID);
        Q_UNUSED(clipX);
        Q_UNUSED(clipY);
    }

    /**
      Removes label displaying the position if present.

      Called by a @ref MSceneViewGLWidget in modification mode.
      */
    virtual void removePositionLabel();

    /**
      Drags the handle with ID @p handleID to the clip space point (@p clipX,
      @p clipY).

      Called by @ref dragEventWithAllComponents after actor components.
      */
    virtual void dragEvent(MSceneViewGLWidget *sceneView,
                           int handleID, float clipX, float clipY)
    {
        Q_UNUSED(sceneView);
        Q_UNUSED(clipX);
        Q_UNUSED(clipY);
        Q_UNUSED(handleID);
    }

    /**
      Drags the handle with ID @p handleID to the clip space point (@p clipX,
      @p clipY).

      Called by a @ref MSceneViewGLWidget in modification mode.
      */
    void dragEventWithAllComponents(MSceneViewGLWidget *sceneView,
                                    int handleID, float clipX, float clipY);

    /**
      Releases the handle with ID @p handleID.

      Called by the actor in @ref releaseEventWithAllComponents to call actor specific implementation.
      */
    virtual void releaseEvent(MSceneViewGLWidget *sceneView, int handleID)
    {
        Q_UNUSED(sceneView);
    }

    /**
      Releases the handle with ID @p handleID.

      Called by a @ref MSceneViewGLWidget in modification mode.
      */
    void releaseEventWithAllComponents(MSceneViewGLWidget *sceneView, int handleID);

    /**
      Triggers the analysis of a potential object at the clip space position @p
      clipX, @p clipY. If the trigger is successful, true shall be returned. If
      no object can be identified at the given position, the method should
      return false.

      Called by a @ref MSceneViewGLWidget in analysis mode.
     */
    virtual bool triggerAnalysisOfObjectAtPos(MSceneViewGLWidget *sceneView,
                                              float clipX, float clipY,
                                              float clipRadius)
    {
        Q_UNUSED(sceneView);
        Q_UNUSED(clipX);
        Q_UNUSED(clipY);
        Q_UNUSED(clipRadius);
        return false;
    }

    void setEnabled(bool enabled);

    void setLabelsEnabled(bool enabled);

    /**
      Returns an ID unique to the actor type. For example, this ID is used to
      ifentify the actor type in configuration files and for runtime actor
      creation.

      @note This function NEEDS to be overridden in derived classes.
     */
    virtual QString getSettingsID() = 0;

    static QString staticSettingsID() { return "MActor"; }

    /**
     Save the actor configuration to the file @p filename. Calls @ref
     saveConfiguration() to store the actor-specific settings of derived
     classes.
     */
    void saveConfigurationToFile(QString filename = "");

    void loadConfigurationFromFile(QString filename = "");

    /**
     Save actor-specific configuration to the @ref QSettings object @p
     settings.

     @note Override this function if your derived class needs to store
     configuration.
     */
    void saveActorConfiguration(QSettings *settings);

    /**
     Load actor-specific configuration from the @ref QSettings object @p
     settings.
     */
    void loadActorConfiguration(QSettings *settings);

    /**
     Save actor-specific settings that are not part of the actor properties
     to the @ref QSettings object @p settings.

     @note Override this function if your derived class needs to store such
     configuration.
     */
    virtual void saveConfiguration(QSettings *settings) { Q_UNUSED(settings); }

    /**
     * Save the actor specific configuration header to the @p settings object.
     * The header should contain all information to recreate the actor and its property tree.
     * For example, save the number of variables in the actor, so that they can be recreated,
     * but dont save the exact settings from the variable properties. They are saved automatically
     * afterwards.
     * @param settings The settings object to write the header into.
     */
    virtual void saveConfigurationHeader(QSettings *settings) { Q_UNUSED(settings); }

    /**
     Load actor-specific settings that are not part of the actor properties
     from the @ref QSettings object @p settings.

     @note Override this function if your derived class needs to load such
     configuration.
     */
    virtual void loadConfiguration(QSettings *settings) { Q_UNUSED(settings); }


    /**
     * Load actor specific settings that are part of config files created before
     * the property framework implementation done in 1.14.
     * The property framework largely handles the saving and loading of the properties
     * by itself.
     * @param settings The settings object to load from.
     */
    virtual void loadConfigurationPrior_V_1_14(QSettings *settings);

    /**
     * Load the actor specific configuration header from the @p settings object.
     * The header should contain all information to recreate the actor and its property tree.
     * For example, create all variables that were saved, but not load their settings yet.
     * Or create all mssing properties from the tree, so that they can be loaded afterwards.
     * @param settings The settings object to write the header into.
     */
    virtual void loadConfigurationHeader(QSettings *settings) { Q_UNUSED(settings); }

    void collapseActorPropertyTree();

    /**
      Set to @p false if the actor should not be deleted by the user.
      Useful for actors that exist only as a single instance in the system,
      e.g. text labels.
     */
    void setActorIsUserDeletable(bool b);

    bool getActorIsUserDeletable() const { return actorIsUserDeletable; }

    /**
     * Will prompt a request to the user, whether he wants to delete this actor,
     * and if he accepts, it will be deleted, as long as it is deletable.
     */
    void deleteActor();

    /**
     * Ask the user, whether to delete this actor.
     * @return Result of the question.
     */
    bool getConfirmationForDeletion();

    /**
     * Ask the user to rename the actor.
     * @return The new actor name or an empty string if it failed to rename the actor.
     */
    QString renameActor();

    /**
      Returns @p true if this actor is in some way connected to the argument
      @p actor.

      @note Override this method in your derived class.
     */
    virtual bool isConnectedTo(MActor *actor) { Q_UNUSED(actor); return false; }

    /**
      Returns @p true if this actor supports visualisation of multiple ensemble
      members (e.g. spaghetti plots).

      @note Override this method in your derived class.
     */
    virtual bool supportsMultipleEnsembleMemberVisualization()
    { return actorSupportsMultipleEnsembleMemberVisualization; }

    /**
      Returns @p true if this actor supports visualisation in full-screen mode.
     */
    virtual bool supportsFullScreenVisualisation()
    { return actorSupportsFullScreenVisualisation; }

    /**
      Called if full-screen mode is switched.

      @p fullScreenEnabled is true if full-screen mode is switched on and to
      false if switched off.

     Needs to be reimplemented in derived classes which support full-screen
     visualisation and need to react when full-screen mode is changed.
     */
    virtual void onFullScreenModeSwitch(MSceneViewGLWidget *sceneView,
                                        bool fullScreenEnabled)
    { Q_UNUSED(sceneView); Q_UNUSED(fullScreenEnabled); }

    /**
      Adds a new MActorComponent of type @c T. Also initializes components resources
      if the actor is already initialized, otherwise it is initialized with the actor.
      Will also call @c MActorComponent::addProperties() to initialize the components properties.
      @param __args Constructor arguments for component, excludes first mandatory argument which must be MActor.
      @return A reference to the added component.
     */
    template<class T, typename... _Args>
    std::shared_ptr<T> addComponentOfType(_Args&&... __args)
    {
        static_assert(std::is_base_of<MActorComponent, T>::value,
                "type parameter must derive from MActorComponent");
        std::shared_ptr<T> comp = std::make_shared<T>(this, std::forward<_Args>(__args)...);

        if (!comp)
        {
            LOG4CPLUS_ERROR(mlog, "Could not create actor component.");
            return comp;
        }

        components.insert(comp->getID(), comp);

        connect(comp.get(), SIGNAL(componentChanged()),
                this, SLOT(emitActorChangedSignal()));

        // When a component is added after actor was initialized, initialize component instantly.
        if (actorIsInitialized)
        {
            comp->initializeResources();

            beginCompileShaders(1);

            comp->reloadShaderEffects(false);

            MProgressBar::getInstance()->taskCompleted(progressBarHandles.takeFirst());

            endCompileShaders();
        }

        comp->addProperties(&actorPropertiesSupGroup);

        return comp;
    }


    /**
     * Adds a new MActorComponent of type @c T. Also initializes components resources
     * if the actor is already initialized, otherwise it is initialized with the actor.
     * Will also call @c MActorComponent::addProperties() to initialize the components properties.
     * IMPORTANT: The component has to have been removed from any previous actor it might have been
     * added to, otherwise the call will fail.
     * @tparam T Component type.
     * @param comp The component to add.
     * @return True, if added, otherwise false.
     */
    template<class T>
    bool addComponentOfType(const std::shared_ptr<T> &comp)
    {
        static_assert(std::is_base_of<MActorComponent, T>::value,
                      "type parameter must derive from MActorComponent");

        if (!comp)
        {
            LOG4CPLUS_ERROR(mlog, "The added actor component is invalid.");
            return false;
        }

        if (components.contains(comp->getID()))
        {
            LOG4CPLUS_ERROR(mlog, "The actor component was already added to this actor.");
            return false;
        }

        if (comp->parent != nullptr && comp->parent != this)
        {
            LOG4CPLUS_ERROR(mlog, "The actor component is already part of another actor.");
            return false;
        }

        components.insert(comp->getID(), comp);

        connect(comp.get(), SIGNAL(componentChanged()),
                this, SLOT(emitActorChangedSignal()));

        // When a component is added after actor was initialized, initialize component instantly.
        if (actorIsInitialized)
        {
            comp->initializeResources();

            beginCompileShaders(1);

            comp->reloadShaderEffects(false);

            MProgressBar::getInstance()->taskCompleted(progressBarHandles.takeFirst());

            endCompileShaders();
        }

        comp->addProperties(&actorPropertiesSupGroup);

        return true;
    }

    /**
      @brief getComponent Returns the component assigned to id @ref componentId, if it was added to this actor.
      @param componentId ID of the component.
      @return The pointer to the component or an empty shared pointer if it doesnt exist.
     */
    std::shared_ptr<MActorComponent> getComponent(const unsigned int componentId) const;

    /**
      @brief removeComponent Removes the actor component with the given ID @p componentId.
      @param componentId Id of the actor component to remove.
      @return True when removed, otherwise false.
     */
    bool removeComponentByID(const unsigned int componentId);

    bool renderingAsWireframe() const
    { return renderAsWireFrameProp; }

    /**
     * Whether this actor casts shadows or not.
     * @return True, if this actor casts shadows, false if it does not.
     */
    bool castsShadows() const;

    /**
     * Whether this actor receives shadows through the scene lighting or not.
     * @return True, if it is shadowed or not.
     */
    bool receivesShadows() const;

public slots:
    /**
      Called when another actor (not this one) in the global actor pool has been
      created. Calls @ref onOtherActorCreated(), in which derived classes can
      handle actor deletion events.
     */
    void actOnOtherActorCreated(MActor *actor);

    /**
      Called when another actor (not this one) in the global actor pool has been
      deleted. Allows this actor to take actions if any connection exists
      between this actor and the deleted actor. Calls @ref onOtherActorDeleted(),
      in which derived classes can handle actor deletion events.
     */
    void actOnOtherActorDeleted(MActor *actor);

    /**
      Called when another actor (not this one) in the global actor pool has been
      renamed. Calls @ref onOtherActorRenamed(), in which derived classes can
      handle actor renaming events.
     */
    void actOnOtherActorRenamed(MActor *actor, QString oldName);

    /**
      Emit the @ref actorChanged() signal, but only if the signal is enabled,
      the actor is enabled and the actor is initialized.

      @see enableEmissionOfActorChangedSignal()
     */
    void emitActorChangedSignal();

signals:
    /**
      Emitted when a property of the actor has changed. Should also be emitted
      in derived classes when a property added in the derived class has
      changed.

      @note Never emit directly, always use @ref signalActorChanged()!
      */
    void actorChanged();

    /**
      Emitted when the name of the actor has been changed via @ref setName().
     */
    void actorNameChanged(MActor *actor, QString oldName);

protected:
    void setActorType(QString actorType) { this->actorType = actorType; }

    /**
      Initialise the actor, e.g. load GLSL shader programs, create GPU OpenGL
      resources that can be shared between OpenGL contexts.

      @note Please check the OpenGL 4.2 core specification, Appendix D, to make
      sure the created resources can be shared.

      @see initializePerGLContextResources()
      */
    virtual void initializeActorResources() = 0;

    /**
      Implement this function in derived classes to handle enabling or disabling of this actor.
     */
    virtual void onActorEnabledToggle(){}

    /**
      Implement this function in derived classes to handle actor creation
      events. @see actOnOtherActorCreated().
     */
    virtual void onOtherActorCreated(MActor *actor)
    { Q_UNUSED(actor); }

    /**
      Implement this function in derived classes to handle actor deletion
      events. @see actOnOtherActorDeleted().
     */
    virtual void onOtherActorDeleted(MActor *actor)
    { Q_UNUSED(actor); }

    /**
      Implement this function in derived classes to handle actor renaming
      events. @see actOnOtherActorRenamed().
     */
    virtual void onOtherActorRenamed(MActor *actor, QString oldName)
    { Q_UNUSED(actor); Q_UNUSED(oldName); }

    // Define friends for suppressActorUpdates().
    friend class MVerticalRegridProperties;
    friend class MSessionManagerDialog;

    /**
      Query this method to know whether updates to any part of the actor (e.g.
      loading of new data, upload of resources to the GPU) should be omitted.

      Returns @p false if either the actor has not been initialized yet
      (@see initialize()) or if updated are suppressed by a call to
      @ref enableActorUpdates()).
     */
    bool suppressActorUpdates();

    // Define friends for enableEmissionOfActorChangedSignal().
    friend class MNWPHorizontalSectionActor;

    /**
      Enables/disables the emission of the @ref actorChanged() signal. Use this
      to change actor properties without triggering a redraw of the scene.
      */
    void enableEmissionOfActorChangedSignal(bool enabled);

    /**
      Render the actor in the current OpenGL context.

      Needs to be implemented in derived classes if they render 3D elements.
      */
    virtual void renderToCurrentContext(MSceneViewGLWidget *sceneView) { Q_UNUSED(sceneView); }

    /**
      Render the actors 2D UI elements in the current OpenGL context' UI layer.

      Needs to be implemented in derived classes if they render 2D UI elements.
      */
    virtual void renderToCurrentContextUiLayer(MSceneViewGLWidget *sceneView){ Q_UNUSED(sceneView); }

    /**
      Render the actor in the current OpenGL context in full screen mode.

      Needs to be implemented in derived classes supporting full-screen mode.
      */
    virtual void renderToCurrentFullScreenContext(
            MSceneViewGLWidget *sceneView)
    { Q_UNUSED(sceneView); }

    void enablePicking(bool p) { actorIsPickable = p; }

    /**
      This method needs to be called at the beginning of each actor's
      @ref reloadShaderEffects() method, indicating the number of shaders
      that will be compiled. It needs to be followed by a call to
      @ref endCompileShaders() after all shaders have been compiled.

      The method sets up the "compile shaders" progress dialog. It resets
      @ref shaderCompilationProgress to @p 0.
      */
    void beginCompileShaders(int numberOfShaders);

    /**
      See @ref beginCompileShaders().
     */
    void endCompileShaders();

    /**
      Compiles a GLSL shader from @p filename and updates the "compile shaders"
      progress dialog. Uses @ref shaderCompilationProgress to update number of
      already compiled shaders.
      */
    void compileShadersFromFileWithProgressDialog(
            std::shared_ptr<GL::MShaderEffect> shader,
            const QString filename);

    /**
      Disables/enables actor updates. Usage:

      enableActorUpdates(false);
      [code that doesn't allow actor updates, e.g. load-from-file]
      enableActorUpdates(true);

      Uses a counter to allow nested calls.

      @see suppressActorUpdates()
     */
    void enableActorUpdates(bool enable);

    // Define friends for request/release texture and image units.
    friend class MNWP2DSectionActorVariable;
    friend class MNWP2DHorizontalActorVariable;
    friend class MNWP3DVolumeActorVariable;

    GLint assignTextureUnit();

    void releaseTextureUnit(GLint unit);

    GLint assignImageUnit();

    void releaseImageUnit(GLint unit);

    double computePositionLabelDistanceWeight(MCamera *camera,
                                              QVector3D mousePosWorldSpace);

    /**
      Call this method from a derived actor's constructor to enable full-screen
      rendering support for this actor. If set to @p true, the actor will be
      listed as available in a scene view's full-screen rendering property.

      Compare to @ref MSceneViewGLWidget::onActorCreated(MActor *actor) .
     */
    void setActorSupportsFullScreenVisualisation(bool b)
    { actorSupportsFullScreenVisualisation = b; }

    /**
      Call this method from a derived actor's constructor to enable multiple
      ensemble member visualisation support for this actor. If set to @p true,
      actor variables will list the "multiple members" option in their ensemble
      mode list and will be connected to a grid aggregation data source.

      Compare to @ref MNWPActorVariable .
     */
    void setActorSupportsMultipleEnsembleMemberVisualization(bool b)
    { actorSupportsMultipleEnsembleMemberVisualization = b; }

    /**
      Override this method in derived actors to print debug output upon user
      request.
     */
    virtual void printDebugOutputOnUserRequest();


    // Properties common to all actors.
    //=================================

    MBoolProperty actorIsEnabledProp;           /* Property group for this actor. */
    MArrayProperty actorPropertiesSupGroup;     /* Subgroup for actor properties;
                                                   derived actor classes may define
                                                   additional subgroups for, e.g.,
                                                   variables. */
    QAction *deleteActorContextAction;
    QAction *renameActorContextAction;

    MProperty actorDevelopmentSupGroupProp;

    bool          actorIsEnabled;           /* Whether actor will be rendered. */
    MBoolProperty renderAsWireFrameProp;    /* True if the actor should be
                                               rendered in wireframe mode. */

    MButtonProperty saveConfigProp;
    MButtonProperty loadConfigProp;

    MButtonProperty reloadShaderProp;
    MButtonProperty printDebugOutputProp;

    MPropertyTemplates::Labels labelProps;

    MArrayProperty actorShadowGroupProp;
    MBoolProperty actorCastShadowProp;
    MBoolProperty actorReceiveShadowProp;

    bool actorIsPickable;

    QList<MProgressBarTaskHandle*> progressBarHandles;

    /**
     * Whether this actor casts shadows or not.
     * This needs to be set during construction, as it adds
     * a user property.
     */
    const bool actorSupportsShadows;

    /** List of scenes to which this actor has been added. */
    QList<MSceneControl*> scenes;

    /** List of labels that belong to this actor. */
    QList<MLabel*> labels;

    MLabel* positionLabel;

    /**
     Removes all labels of this actor. Calls @ref MTextManager::removeText()
     for each label in @ref labels and deletes the label from the list.
     */
    void removeAllLabels();

    /**
     @deprecated Uploads a QVector<QVector3D> data field to the vertex buffer
     @p vbo.

     @note Use @ref MTypedVertexBuffer functionality instead.
     */
    void uploadVec3ToVertexBuffer(const QVector<QVector3D> *data, GLuint *vbo);

    void uploadVec3ToVertexBuffer(const QVector<QVector3D>& data,
                                  const QString requestKey,
                                  GL::MVertexBuffer** vbo,
                                  QGLWidget* currentGLContext = nullptr);

    void uploadVec4ToVertexBuffer(const QVector<QVector4D>& data,
                                  const QString requestKey,
                                  GL::MVertexBuffer** vbo,
                                  QGLWidget* currentGLContext = nullptr);

    void uploadVec2ToVertexBuffer(const QVector<QVector2D>& data,
                                  const QString requestKey,
                                  GL::MVertexBuffer** vbo,
                                  QGLWidget* currentGLContext = nullptr);

    void uploadFloatToVertexBuffer(const QVector<float>& data,
                                  const QString requestKey,
                                  GL::MVertexBuffer** vbo,
                                  QGLWidget* currentGLContext = nullptr);

    /** Unique integer identifying this actor, assigned in the constructor. */
    unsigned int myID;

    bool multipleEnsembleMembersEnabled;

    /**
     * Drag event counter to be able to undo drag events.
     * Can be used in sub-classes.
     */
    int dragEventID = 0;

private:
    static unsigned int idCounter;

    QString actorName;
    QString actorType;

    // All currently added actor components
    QMap<unsigned int, std::shared_ptr<MActorComponent>> components;

    bool actorIsInitialized;
    int actorChangedSignalDisabledCounter;
    int actorUpdatesDisabledCounter;

    // "true" (default) if the actor can be deleted from the GUI.
    bool actorIsUserDeletable;
    // "true" if the actor supports full-screen visualisation.
    // (default = "false")
    bool actorSupportsFullScreenVisualisation;
    // "true" if the actor supports multiple ensemble member visualisation.
    // (default = "false")
    bool actorSupportsMultipleEnsembleMemberVisualization;

    QList<GLint> availableTextureUnits;
    QList<GLint> assignedTextureUnits;
    QList<GLint> availableImageUnits;
    QList<GLint> assignedImageUnits;

public:
    /**
     * An undoable action to rename an actor.
     */
    class RenameCommand : public QUndoCommand
    {
    public:
        RenameCommand(MActor *actor, QString newName);

        void undo() override;
        void redo() override;

    private:
        /**
         * The renamed actor.
         */
        MActor *actor;

        /**
         * The new name of the actor.
         */
        QString newName;

        /**
         * The old name of the actor.
         */
        QString oldName;

        /**
         * A reference to the connection which is called when the actor is
         * destroyed. Saved here, so we can disconnect it when this command
         * is deleted.
         */
        QMetaObject::Connection destroyCallback;
    };


    /**
     * Undo command for actor deletion.
     */
    class DeleteCommand : public QUndoCommand
    {
    public:
        explicit DeleteCommand(MActor *actor);

        void undo() override;
        void redo() override;

    private:
        /**
         * Actor config used to recreate the actor.
         */
        MTempConfigFile actorConfig;
        /**
         * The actor that should be deleted by this command on redo().
         */
        MActor *actor;
        /**
         * The actor type used to recreate it.
         */
        QString actorType;
        /**
         * The actor name used to recreate it.
         */
        QString actorName;
        /**
         * The scenes the actor was part of.
         */
        QList<MSceneControl*> scenes;
        /**
         * The render queue ids of the actor for the scenes it was part of.
         */
        QList<int> sceneRenderIDs;
    };


    /**
     * Undo command used for actor creation.
     */
    class CreateCommand : public QUndoCommand
    {
    public:
        CreateCommand(QString actorType, QString actorName, const QVector<MSceneControl *>& scenes = {});
        CreateCommand(QString actorConfig,
                      QVector<MSceneControl *> scenes,
                      Qt::KeyboardModifiers mods = {});

        void undo() override;
        void redo() override;

        MActor *getCreatedActor() const { return createdActor; }

    private:
        /**
         * The type of actor to create.
         */
        QString actorType;
        /**
         * The name of the actor to create.
         */
        QString actorName;
        /**
         * The path to the actor config that is used to create the actor from.
         */
        QString actorConfig;
        /**
         * The scenes to add the actor to.
         */
        QVector<MSceneControl *> scenes;
        /**
         * Keyboard modifiers used when creating the actor.
         */
        Qt::KeyboardModifiers mods;
        /**
         * The created actor.
         */
        MActor *createdActor;
    };

    /**
     * Undo command used for actor duplication.
     */
    class DuplicateCommand : public QUndoCommand
    {
    public:
        DuplicateCommand(MActor *actor, const QString &newActorName);

        void undo() override;
        void redo() override;

        MActor *getCreatedActor() const { return createdActor; }

    private:
        /**
         * The actor that was duplicated.
         */
        MActor *originalActor;
        /**
         * The name of the original actor that was duplicated.
         */
        QString originalActorName;
        /**
         * The name of the duplicated actor.
         */
        QString createdActorName;
        /**
         * The created actor.
         */
        MActor *createdActor;
        /**
         * The scenes the actor was added to.
         */
        QVector<MSceneControl *> scenes;
    };
};


/**
 @brief Base class for actor factories. Actors that can be instantiated during
 runtime require an actor factory. Derive from this class and override the @ref
 createInstance() method to create an instance of the derived actor class.

 Actor factories need to be registered with the @ref MGLResourcesManager,
 @see MApplicationConfigurationManager::registerActorFactories()
 */
class MAbstractActorFactory
{
public:
    MAbstractActorFactory();

    virtual ~MAbstractActorFactory() = default;

    QString getName() const { return name; }

    /**
     * Get the file name of the icon associated with actors of this factory.
     * @return the file path
     */
    QString getIconFileName() const { return iconFilename; };


    /**
     * Create a new actor. Note that this method does not register the
     * actor to any resource manager or scene, use the static methods for that
     * instead.
     *
     * @param configfile If provided, create the actor from this config file.
     * @param actorName The actor name to use. If a config file is used, the
     * actor name from the config file will *not* be used.
     * @return The newly created actor.
     */
    MActor *create(QString &actorName, const QString &configfile = QString());
    MActor *create();
    MActor *createFromConfig(const QString &configfile = QString());


    /**
     * Creates a new actor based on the given type and registers
     * it to the managers.
     * @param actorName The name of the actor. The name might be replaced via UI
     * if the name already exists. If a config file is loaded this actor name is
     * used if it is not empty.
     * @param scenesToAdd Vector of scenes to which the actor should be added.
     * @param configfile (optional) The config file to load the actor from.
     * @param mods (optional) The keyboard modifiers active for actor creation.
     * @param skipInit (optional) Whether to skip actor initialization or not. Has to be
     * done while loading a session, so that all actors can be initialized together
     * after loading.
     * @return The newly created actor.
     */
    MActor *createAndRegisterActor(QString &actorName,
                                   QVector<MSceneControl *> &scenesToAdd,
                                   const QString &configfile = QString(),
                                   Qt::KeyboardModifiers mods =
                                       QGuiApplication::keyboardModifiers(),
                                   bool skipInit = false);


    /******************************************************************************
    ***                  STATIC METHODS FOR ACTOR CREATION                      ***
    *******************************************************************************/

    /**
     * Determines the actor factory from the given actor type, and
     * call the dynamic @c createAndRegisterActor method.
     *
     * @param actorType The actor type.
     * @param actorName The actor to use if provided, otherwise generate one.
     * If an invalid name is provided, the user will be prompted to replace it.
     * @param scenes List of scenes the actor should be added to. If not
     * provided, don't add to any scene by default.
     * @param skipInit Whether to skip actor initialization or not. Has to be
     * done while loading a session, so that all actors can be initialized together
     * after loading.
     * @return The new actor or nullptr if something went wrong.
     */
    static MActor *createAndRegisterActor(const QString &actorType,
                                          QString &actorName,
                                          QVector<MSceneControl *> &scenes,
                                          bool skipInit = false);
    static MActor *createAndRegisterActor(const QString &actorType,
                                          QString &actorName,
                                          bool skipInit = false);
    static MActor *createAndRegisterActor(const QString &actorType);


    /**
     * Opens a window where the user can select an actor file to load.
     * Calls @c createAndRegisterActorFromFile if an actor file is selected.
     * @param scenes Vector of scenes to which the actor should be added.
     * @return The new actor or nullptr if something went wrong.
     */
    static MActor *createAndRegisterActorFromFileDialog(
        QVector<MSceneControl *> scenes = QVector<MSceneControl *>());


    /**
     * Creates and registers an actor from the given config file. This
     * method determines the actor type and whether the file is valid, and
     * calls @c createAndRegisterActor to create the actor.
     *
     * @param configfile Path to the config file.
     * @param scenes Vector of scenes to which the actor should be added.
     * @param mods (optional) The keyboard modifiers active for actor creation
     * @return The new actor or nullptr if something went wrong.
     */
    static MActor *createAndRegisterActorFromFile(
        const QString &configfile, QVector<MSceneControl *> &scenes,
        Qt::KeyboardModifiers mods = QGuiApplication::keyboardModifiers());


    /**
     * Check if an actor with the given name exists.
     * @param name The name to check.
     * @return True if actor with given name exists.
     */
    static bool actorExists(const QString &name);


    /**
     * Checks whether the given actor name is available and valid
     * (not empty, and a valid object name).
     * @param actorName The name to check.
     * @return True if name is available and valid.
     */
    static bool isActorNameAvailableAndValid(const QString &actorName);


    /**
     * Renames the @p actor to @p newName. If @var newName cannot be
     * used to name the actor, return false and do not rename.
     * @param actor The actor to rename.
     * @param newName The new name to rename the actor to.
     * @return true if successfully renamed.
     */
    static bool renameActor(MActor *actor, const QString& newName);


    /**
     * Delete the @var actor from the program and from memory. This does no
     * sanity checks and forces the deletion.
     * @param actor Actor to delete.
     */
    static void deleteActor(MActor *actor);


    /**
     * Generate an available actor name.
     * @param actorType Type of actor to generate an actor name.
     * @return An unused actor name.
     */
    static QString generateActorName(const QString &actorType);


    bool acceptSettings(QSettings *settings);

    bool acceptSettings(const QString& configfile);

protected:
    virtual void initialize() = 0;
    virtual MActor* createInstance() = 0;

    /**
      Call this function in the derived @ref createInstance() method to display
      a warning that the actor is still in an "experimental" state -- useful
      for development versions.
     */
    void displayWarningExperimentalStatus();

    QString name;
    QString iconFilename;

    QString settingsID;
    bool isInitialized;
};


template <typename ActorType>
class MGenericActorFactory : public MAbstractActorFactory
{
public:
    virtual MActor* createInstance() override
    {
        return new ActorType();
    }

    virtual void initialize() override
    {
        name = ActorType::staticActorType();
        iconFilename = ActorType::staticIconFileName();
        settingsID = ActorType::staticSettingsID();

        isInitialized = true;
    }
};


} // namespace Met3D

#endif // MACTOR_H
