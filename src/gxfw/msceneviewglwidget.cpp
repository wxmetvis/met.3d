/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2017 Bianca Tost [+]
**  Copyright 2022-2024 Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "msceneviewglwidget.h"

// standard library imports
#include <sstream>
#include <fstream>
#include <string>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QFileInfo>
#include <QLabel>
#include <QApplication>

// local application imports
#include "util/metroutines.h"
#include "util/mutil.h"
#include "gxfw/mtypes.h"
#include "gxfw/textmanager.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "mainwindow.h"
#include "data/mimedata.h"
#include "toolbar/selectactornamescenedialog.h"
#include "actors/lightactor.h"
#include "system/pipelineconfiguration.h"
#include "util/mfiletypes.h"
#include "gxfw/properties/mpropertytemplates.h"


namespace Met3D
{

// Static counter for all scene views.
unsigned int MSceneViewGLWidget::idCounter = 0;

const int defaultRenderResolutionX = 1920;
const int defaultRenderResolutionY = 1080;

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSceneViewGLWidget::MSceneViewGLWidget()
    : QGLWidget(MGLResourcesManager::getInstance()->format(), 0,
                MGLResourcesManager::getInstance()),
      scene(nullptr),
      sceneFbo(0),
      currentFbo(0),
      tex2DSceneColor(nullptr),
      tex2DDepthBuffer(nullptr),
      tex2DFSRResult(nullptr),
      tex2DFinalColor(nullptr),
      tex2DFinalDepthBuffer(nullptr),
      tex2DShadowMap(nullptr),
      sceneLightsBuffer(nullptr),
      lightUniformBuffer(nullptr),
      vboViewportQuad(nullptr),
      lastPoint(QVector3D(0,0,0)),
      currentCamera(&camera),
      sceneDirectionalLight(nullptr),
      sceneNavigationMode(MOVE_CAMERA),
      freezeMode(0),
      isLoadingFromConfig(false),
      fullScreenActor(nullptr),
      synchronizationControl(nullptr),
      displayDateTimeLabel(nullptr),
      sceneNameLabel(nullptr),
      measureFPS(false),
      measureFPSFrameCount(0),
      visualizationParameterChange(false),
      cameraSynchronizedWith(nullptr),
      singleInteractionActor(nullptr),
      resizeViewDialog(new MResizeWindowDialog),
      overwriteImageSequence(false)
{
    viewIsInitialised = false;
    focusShader = nullptr;
    renderToViewportShader = nullptr;
    fsrPassShader = nullptr;

    // Obtain a "personal" identification number.
    this->myID = MSceneViewGLWidget::idCounter++;

    pbot    = 1050.; // hPa
    ptop    = 20.;
    logpbot = log(pbot);
    zbot    = 0.;
    ztop    = 36.;
    slopePtoZ = (ztop - zbot) / (log(ptop) - log(pbot));

    userIsInteracting    = false;
    userIsScrolling      = false;
    viewportResized      = false;

    pickedActor.actor    = 0;
    pickedActor.handleID = -1;

    // Initial camera position.
//    camera.setOrigin(QVector3D(180., 0., 0.));
    camera.rotate(20., 0., 0., 1.);
    camera.rotate(40., 1., 0., 0.);
    camera.moveForward(-160.);
    camera.moveUp(-30.);
    camera.moveRight(-20);
    camera.setVerticalFov(45.);
    sceneRotationMatrix.setToIdentity();

    fsrQuality = OFF;
    shadowMapResProp = ShadowResolution::NormalRes;

    // Focus policy: Accept focus by both tab and click.
    setFocusPolicy(Qt::StrongFocus);

    fpsStopwatch = new MStopwatch();
    frameCount   = 0;

    if (myID == 0)
    {
        // Scene view with ID 0 measures system frame rate performace.
        fpsTimer = new QTimer(this);
        connect(fpsTimer, SIGNAL(timeout()), SLOT(updateFPSTimer()));
        fpsTimer->start(1000); // update fps display every 1000ms
        splitNextFrame = false;
        fpsTimeseriesSize = 60;
        fpsTimeseriesIndex = 0;
        fpsTimeseries = new float[fpsTimeseriesSize];
        for (int i = 0; i < fpsTimeseriesSize; i++) fpsTimeseries[i] = -1;
    }

    checkScrollTimer.setInterval(250);
    connect(&checkScrollTimer, SIGNAL(timeout()),this, SLOT(checkUserScrolling()));
    checkScrollTimer.start();

    MSystemManagerAndControl *systemControl = MSystemManagerAndControl::getInstance();

    // Create a property group for this scene view's properties. The group will
    // be displayed in the properties browser in the scene view control.
    groupProp = MArrayProperty(QString("Scene view #%1 configuration").arg(myID+1));
    groupProp.setShowPropertyLabels(false);

    // Configuration properties.
    loadConfigProp = MButtonProperty("Load", "Load");
    loadConfigProp.registerValueCallback([=]()
                                              { loadConfigurationFromFile(); });
    groupProp.append(&loadConfigProp);

    saveConfigProp = MButtonProperty("Save", "Save");
    saveConfigProp.registerValueCallback([=]()
                                              { saveConfigurationToFile(); });
    groupProp.append(&saveConfigProp);

    sceneSaveToImageProp = MButtonProperty("Save as image", "Save");
    sceneSaveToImageProp.registerValueCallback(this,
                                               &MSceneViewGLWidget::saveToImageFile);
    groupProp.addSubProperty(sceneSaveToImageProp);

    resizeProp = MButtonProperty("Screen size of view", "Resize");
    resizeProp.registerValueCallback(this, &MSceneViewGLWidget::resizeView);
    groupProp.addSubProperty(resizeProp);

    // Camera group.
    cameraAndNavGroupProp = MArrayProperty("Camera and navigation");
    cameraAndNavGroupProp.setShowPropertyLabels(false);
    groupProp.addSubProperty(cameraAndNavGroupProp);

    cameraLoadFromFileProp = MButtonProperty("Load", "Load");
    cameraLoadFromFileProp.registerValueCallback([=]()
    {
        executeCameraAction(CAMERA_LOADFROMFILE, false);
    });
    cameraAndNavGroupProp.append(&cameraLoadFromFileProp);

    cameraSaveToFileProp = MButtonProperty("Save", "Save");
    cameraSaveToFileProp.registerValueCallback([=]()
    {
        executeCameraAction(CAMERA_SAVETOFILE, false);
    });
    cameraAndNavGroupProp.append(&cameraSaveToFileProp);

    // Camera position.
    cameraPositionProp = MStringProperty("Camera position", "");
    cameraPositionProp.setEditable(false);
    cameraAndNavGroupProp.addSubProperty(cameraPositionProp);

    sceneNavigationModeProp = MEnumProperty("View and navigation mode");
    sceneNavigationModeProp.setEnumNames({"Move camera", "Rotate scene",
                                          "2D top view", "Single full screen actor"});
    sceneNavigationModeProp.setConfigKey("view_and_navigation_mode");
    sceneNavigationModeProp.saveAsEnumName(true);
    sceneNavigationModeProp.registerValueCallback([=]()
    {
        // Disable auto-rotation when scene navigation is changed.
        setAutoRotationMode(false);

        sceneNavigationMode = (SceneNavigationMode) sceneNavigationModeProp
                .value();

        // Disconnect full-screen actor to directly update connected scene view.
        if (fullScreenActor && sceneNavigationMode != SINGLE_FULLSCREEN_ACTOR)
        {
            fullScreenActor->onFullScreenModeSwitch(this, false);
            disconnect(fullScreenActor, SIGNAL(actorChanged()),
                       this, SLOT(onFullScreenActorUpdate()));
        }

        if (sceneNavigationMode == MOVE_CAMERA)
        {
            sceneRotationCenterProp.setEnabled(false);
            selectSceneRotationCentreProp.setEnabled(false);
            cameraAutorotationModeProp.setEnabled(false);
            fullScreenActorProp.setEnabled(false);
        }
        else if (sceneNavigationMode == ROTATE_SCENE)
        {
            sceneRotationCenterProp.setEnabled(true);
            selectSceneRotationCentreProp.setEnabled(true);
            cameraAutorotationModeProp.setEnabled(true);
            fullScreenActorProp.setEnabled(false);
        }
        else if (sceneNavigationMode == TOPVIEW_2D)
        {
            sceneRotationCenterProp.setEnabled(false);
            selectSceneRotationCentreProp.setEnabled(false);
            cameraAutorotationModeProp.setEnabled(false);
            fullScreenActorProp.setEnabled(false);
            // !TODO: disable camera zooming

            QVector3D eyePos = QVector3D(-10, 50, 100);//camera.getOrigin();

            camera.setOrigin(eyePos);
            camera.setZAxis(QVector3D(0, 0, -1));
            camera.setYAxis(QVector3D(0, 1, 0));
        }
        else if (sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR)
        {
            fullScreenActorProp.setEnabled(true);
            sceneRotationCenterProp.setEnabled(false);
            selectSceneRotationCentreProp.setEnabled(false);
            cameraAutorotationModeProp.setEnabled(false);

            // Connect full-screen actor to directly update connected scene view.
            if (fullScreenActor)
            {
                fullScreenActor->onFullScreenModeSwitch(this, true);
                connect(fullScreenActor, SIGNAL(actorChanged()),
                        this, SLOT(onFullScreenActorUpdate()));
            }
        }
        camera.setOrthographic(sceneNavigationMode == TOPVIEW_2D);

        updateSceneLabel();
        updateGL();
    });
    cameraAndNavGroupProp.addSubProperty(sceneNavigationModeProp);

    sceneNavigationSensitivityProp = MFloatProperty("Navigation sensitivity", 1.0);
    sceneNavigationSensitivityProp.setMinimum(1.0);
    sceneNavigationSensitivityProp.setMaximum(100.0);
    sceneNavigationSensitivityProp.setConfigKey("navigation_sensitivity");
    cameraAndNavGroupProp.addSubProperty(sceneNavigationSensitivityProp);

    setCameraProp = MArrayProperty("Set camera");
    setCameraProp.setShowPropertyLabels(false);
    cameraAndNavGroupProp.addSubProperty(setCameraProp);

    cameraSetNorthUpProp = MButtonProperty("Set north up", "North up");
    cameraSetNorthUpProp.registerValueCallback([=]()
    {
        executeCameraAction(CAMERA_NORTHUP, false);
    });
    setCameraProp.append(&cameraSetNorthUpProp);

    cameraSetUprightProp = MButtonProperty("Set upright", "Upright");
    cameraSetUprightProp.registerValueCallback([=]()
    {
        executeCameraAction(CAMERA_UPRIGHT, false);
    });
    setCameraProp.append(&cameraSetUprightProp);

    cameraSetTopViewProp = MButtonProperty("Set top view", "Top view");
    cameraSetTopViewProp.registerValueCallback([=]()
    {
        executeCameraAction(CAMERA_TOPVIEW, false);
    });
    setCameraProp.append(&cameraSetTopViewProp);

    cameraRecoverSceneProp = MButtonProperty("Recover lost scene", "Recover");
    cameraRecoverSceneProp.registerValueCallback([=]()
    {
       recoverSceneInViewPort();

       updateGL();
    });
    cameraAndNavGroupProp.addSubProperty(cameraRecoverSceneProp);

    sceneRotationCenterProp = MPropertyTemplates::GeographicPosition3D("Scene rotation centre", {0, 0, 1020});
    sceneRotationCenterProp.setConfigKey("scene_rotation_centre");
    cameraAndNavGroupProp.addSubProperty(sceneRotationCenterProp);

    selectSceneRotationCentreProp = MButtonProperty("Interactively select rotation centre", "Select");
    selectSceneRotationCentreProp.setEnabled(false);
    selectSceneRotationCentreProp.registerValueCallback([=]()
    {
        selectSceneRotationCentreProp.setEnabled(false);
        sceneNavigationModeProp.setEnabled(false);
        MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
        MLabel *pickText = glRM->getSceneRotationCentreSelectionLabel();
        MMovablePoleActor *pickActor = glRM
                ->getSceneRotationCentreSelectionPoleActor();

        pickActor->removeAllPoles();
        pickActor->addPole(sceneRotationCenterProp.value().toPointF());

        sceneNavigationMode = MOVE_CAMERA;

        scene->addActor(pickActor);
        scene->setSingleInteractionActor(pickActor);

        for (MSceneViewGLWidget *sceneView : scene->getRegisteredSceneViews())
        {
            sceneView->staticLabels.append(pickText);
            sceneView->updateSceneLabel();
        }
    });
    cameraAndNavGroupProp.addSubProperty(selectSceneRotationCentreProp);

    cameraAutorotationModeProp = MBoolProperty("Auto-rotate camera", false);
    cameraAutorotationModeProp.setEnabled(false);
    cameraAutorotationModeProp.setConfigKey("auto_rotate_camera");
    cameraAutorotationModeProp.registerValueCallback([=]()
    {
        if (!cameraAutorotationModeProp.value()) cameraAutoRotationTimer->stop();

        updateSceneLabel();
        updateGL();
    });
    cameraAndNavGroupProp.addSubProperty(cameraAutorotationModeProp);

    fullScreenActorProp = MEnumProperty("Actor in fullscreen mode");

    // Scan currently available actors supporting full-screen mode. Add Actors
    // to the list displayed in the combo box of the fullScreenActorProperty.
    QStringList availableFullScreenActors;

    availableFullScreenActors << "None";
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    for (MActor *mactor : glRM->getActors())
    {
        if (mactor->supportsFullScreenVisualisation())
        {
            availableFullScreenActors << mactor->getName();
        }
    }

    fullScreenActorProp.setEnumNames(availableFullScreenActors);
    fullScreenActorProp.setEnabled(false);
    fullScreenActorProp.setConfigKey("actor_in_fullscreen_mode");
    fullScreenActorProp.saveAsEnumName(true);
    fullScreenActorProp.registerValueCallback([=]()
    {
        QString actorName = fullScreenActorProp.getSelectedEnumName();
        // Disconnect previous full-screen actor.
        if (fullScreenActor)
        {
            fullScreenActor->onFullScreenModeSwitch(this, false);
            disconnect(fullScreenActor, SIGNAL(actorChanged()),
                       this, SLOT(onFullScreenActorUpdate()));
        }
        fullScreenActor = MGLResourcesManager::getInstance()->getActorByName(
                actorName);
        // Connect current full-screen actor.
        if (fullScreenActor)
        {
            fullScreenActor->onFullScreenModeSwitch(this, true);
            connect(fullScreenActor, SIGNAL(actorChanged()),
                    this, SLOT(onFullScreenActorUpdate()));
        }

        if (!updatesEnabled())
        {
            return;
        }
        updateGL();
    });
    cameraAndNavGroupProp.addSubProperty(fullScreenActorProp);

    QList<MSceneViewGLWidget*> otherViews = systemControl->getRegisteredViews();
    QStringList otherViewLabels;
    otherViewLabels << "None";
    for (auto& otherView : otherViews)
        otherViewLabels << QString("view #%1").arg(otherView->getID()+1);

    syncCameraWithViewProp =  MEnumProperty("Sync camera with view");
    syncCameraWithViewProp.saveAsEnumName(true);
    syncCameraWithViewProp.setConfigKey("sync_camera_with");
    syncCameraWithViewProp.setEnumNames(otherViewLabels, 0);
    syncCameraWithViewProp.registerValueCallback([=]()
    {
        if (cameraSynchronizedWith != nullptr)
        {
            cameraSynchronizedWith->removeCameraSync(this);
            cameraSynchronizedWith = nullptr;
        }

        int index = syncCameraWithViewProp;

        if (index > 0)
        {
            MSceneViewGLWidget *otherView =
                    MSystemManagerAndControl::getInstance()
                            ->getRegisteredViews()[index - 1];
            otherView->addCameraSync(this);
            cameraSynchronizedWith = otherView;
        }

        if (viewIsInitialised) updateGL();
    });
    cameraAndNavGroupProp.addSubProperty(syncCameraWithViewProp);

    cameraSequenceAnimationProp = MEnumProperty("Camera sequence");
    cameraSequenceAnimationProp.setConfigKey("camera_sequence_index");
    cameraAndNavGroupProp.addSubProperty(cameraSequenceAnimationProp);
    refreshCameraSequenceProperty();

    connect(MSystemManagerAndControl::getInstance(), &MSystemManagerAndControl::cameraSequenceRegistered,
            this, &MSceneViewGLWidget::onSequenceRegistered);
    connect(MSystemManagerAndControl::getInstance(), &MSystemManagerAndControl::cameraSequenceRemoved,
            this, &MSceneViewGLWidget::onSequenceRemoved);
    connect(MSystemManagerAndControl::getInstance(), &MSystemManagerAndControl::cameraSequenceRenamed,
            this, &MSceneViewGLWidget::onSequenceRenamed);


    // Interaction group.
    interactionGroupProp = MProperty("Interaction");
    groupProp.addSubProperty(interactionGroupProp);

    // Register modify mode property.
    actorInteractionModeProp = MBoolProperty("Actor interaction mode", false);
    actorInteractionModeProp.setConfigKey("actor_interaction_mode");
    actorInteractionModeProp.registerValueCallback([=]()
    {
        // Analysis mode cannot be active at the same time.
        if (actorInteractionModeProp && analysisModeProp)
        {
            analysisModeProp = false;
        }

        // In actor interaction mode, mouse tracking is enabled to have
        // mouseMoveEvent() executed on every mouse move, not only when a
        // button is pressed.
        setMouseTracking(actorInteractionModeProp);
        updateSceneLabel();
        updateGL();
    });
    interactionGroupProp.addSubProperty(actorInteractionModeProp);

    analysisModeProp = MBoolProperty("Analysis mode", false);
    analysisModeProp.setConfigKey("analysis_mode");
    analysisModeProp.registerValueCallback([=]()
    {
        // Interaction mode cannot be active at the same time.
        if (analysisModeProp && actorInteractionModeProp)
        {
            actorInteractionModeProp = false;
        }
        updateSceneLabel();
        updateGL();
    });
    interactionGroupProp.addSubProperty(analysisModeProp);

    handleGroupProp = MProperty("Handles");
    interactionGroupProp.addSubProperty(handleGroupProp);

    handleSizeProp = MFloatProperty("Size", 0.5);
    handleSizeProp.setConfigKey("handle_size");
    handleSizeProp.setMinimum(0.01);
    handleSizeProp.setStep(0.1);
    handleSizeProp.registerValueCallback([=]()
    {
        onHandleSizeChanged();
    });
    handleGroupProp.addSubProperty(handleSizeProp);

    // Position label properties.
    posLabelIsEnabledProp = MBoolProperty("Position labels", true);
    posLabelIsEnabledProp.setConfigKey("show_position_labels");
    handleGroupProp.addSubProperty(posLabelIsEnabledProp);


    // Appearance group.
    appearanceGroupProp = MProperty("Appearance");
    groupProp.addSubProperty(appearanceGroupProp);

    verticalScalingProp = MDoubleProperty("Vertical scaling", ztop);
    verticalScalingProp.setConfigKey("vertical_scaling");
    verticalScalingProp.setMinimum(1.);
    verticalScalingProp.setMaximum(999.);
    verticalScalingProp.registerValueCallback([=]()
    {
        // Recompute pressure-to-worldZ slope.
        ztop = verticalScalingProp;
        slopePtoZ = (ztop - zbot) / (log(ptop) - log(pbot));
        // This will be set to false at the end of the next render cycle.
        visualizationParameterChange = true;

        // Update scene light.
        needSceneLightUpdate = true;

        if (viewIsInitialised) updateGL();
    });
    appearanceGroupProp.addSubProperty(verticalScalingProp);

    backgroundColourProp = MColorProperty("Background colour", QColor(255, 255, 255));
    backgroundColourProp.setConfigKey("background_colour");
    backgroundColourProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    appearanceGroupProp.addSubProperty(backgroundColourProp);

    lightingGroupProp = MProperty("Lighting");
    appearanceGroupProp.addSubProperty(lightingGroupProp);

    ambientLightColorProp = MColorProperty("Ambient light colour", {255, 255, 255, 255});
    ambientLightColorProp.setConfigKey("ambient_light_colour");
    ambientLightColorProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    lightingGroupProp.addSubProperty(ambientLightColorProp);

    ambientLightProp = MFloatProperty("Ambient light strength", 0.2f);
    ambientLightProp.setConfigKey("ambient_light_strength");
    ambientLightProp.setMinimum(0.0f);
    ambientLightProp.setMaximum(1.0f);
    ambientLightProp.setStep(0.1f);
    ambientLightProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    lightingGroupProp.addSubProperty(ambientLightProp);

    diffuseLightProp = MFloatProperty("Diffuse light strength", 0.6f);
    diffuseLightProp.setConfigKey("diffuse_light_strength");
    diffuseLightProp.setMinimum(0.0f);
    diffuseLightProp.setMaximum(1.0f);
    diffuseLightProp.setStep(0.1f);
    diffuseLightProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    lightingGroupProp.addSubProperty(diffuseLightProp);

    specularStrengthProp = MFloatProperty("Specular light strength", 0.2f);
    specularStrengthProp.setConfigKey("specular_light_strength");
    specularStrengthProp.setMinimum(0.0f);
    specularStrengthProp.setMaximum(1.0f);
    specularStrengthProp.setStep(0.1f);
    specularStrengthProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    lightingGroupProp.addSubProperty(specularStrengthProp);

    // Scene view label.
    sceneViewLabelEnableProp = MBoolProperty("Scene view label", true);
    sceneViewLabelEnableProp.setConfigKey("scene_view_label_enabled");
    sceneViewLabelEnableProp.registerValueCallback([=]()
    {
        updateSceneLabel();
        updateGL();
    });
    appearanceGroupProp.addSubProperty(sceneViewLabelEnableProp);

    // Display date time.
    displayDateTimeLabelProps.enabledProp.setName("Show time annotation");
    displayDateTimeLabelProps.enabledProp.setConfigKey("show_time_annotation");
    displayDateTimeLabelProps.enabledProp.registerValueCallback([=]()
    {
        if (!updatesEnabled())
        {
            return;
        }
        updateDisplayTime();
    });
    appearanceGroupProp.addSubProperty(displayDateTimeLabelProps.enabledProp);

    auto fontCallback = [=]()
    {
        if (!updatesEnabled())
        {
            return;
        }

        updateDisplayTime();
    };

    displayDateTimeLabelProps.fontSizeProp.setConfigKey("font_size");
    displayDateTimeLabelProps.fontSizeProp.registerValueCallback(fontCallback);

    displayDateTimeLabelProps.fontColourProp.setConfigKey("font_colour");
    displayDateTimeLabelProps.fontColourProp.registerValueCallback(fontCallback);

    displayDateTimeLabelProps.enableBBoxProp.setConfigKey("label_bbox");
    displayDateTimeLabelProps.enableBBoxProp.registerValueCallback(fontCallback);

    displayDateTimeLabelProps.bboxColourProp.setConfigKey("bounding_box_color");
    displayDateTimeLabelProps.bboxColourProp.registerValueCallback(fontCallback);

    displayDateTimeSyncControlProp = MEnumProperty("Synchronize with");
    QStringList syncControls = systemControl->getSyncControlIdentifiers();
    displayDateTimeSyncControlProp.setEnumNames(syncControls);
    displayDateTimeSyncControlProp.saveAsEnumName(true);
    displayDateTimeSyncControlProp.setConfigKey("synchronize_with");
    displayDateTimeSyncControlProp.setHidden(true);
    displayDateTimeSyncControlProp.registerValueCallback([=]()
    {
        if (!updatesEnabled())
        {
            return;
        }

        MSystemManagerAndControl *sysMC =
                MSystemManagerAndControl::getInstance();
        int index = displayDateTimeSyncControlProp;
        synchronizeWith(sysMC->getSyncControl(
                sysMC->getSyncControlIdentifiers().at(index)));

        updateDisplayTime();
    });
    displayDateTimeLabelProps.enabledProp.addSubProperty(displayDateTimeSyncControlProp);

    displayDateTimePositionProp = MPropertyTemplates::ViewportPosition2D("Viewport position", {-0.99, 0.99});
    displayDateTimePositionProp.setConfigKey("viewport_position");
    displayDateTimePositionProp.registerValueCallback(fontCallback);
    displayDateTimeLabelProps.enabledProp.addSubProperty(displayDateTimePositionProp);


    // North arrow.
    northArrow.enabledProp = MBoolProperty("Northward arrow", false);
    northArrow.enabledProp.setConfigKey("northward_arrow_enabled");
    northArrow.enabledProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    appearanceGroupProp.addSubProperty(northArrow.enabledProp);

    northArrow.scaleProp = MArrayProperty("Scale");
    northArrow.enabledProp.addSubProperty(northArrow.scaleProp);

    northArrow.horizontalScaleProp = MFloatProperty("Width", 5.f);
    northArrow.horizontalScaleProp.setStep(.1);
    northArrow.horizontalScaleProp.setDecimals(2);
    northArrow.horizontalScaleProp.setMinimum(0.01);
    northArrow.horizontalScaleProp.setMaximum(100.);
    northArrow.horizontalScaleProp.setConfigKey("north_arrow_horizontal_scale");
    northArrow.horizontalScaleProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    northArrow.scaleProp.append(&northArrow.horizontalScaleProp);

    northArrow.verticalScaleProp = MFloatProperty("Height", 5.f);
    northArrow.verticalScaleProp.setStep(.1);
    northArrow.verticalScaleProp.setDecimals(2);
    northArrow.verticalScaleProp.setMinimum(0.01);
    northArrow.verticalScaleProp.setMaximum(100.);
    northArrow.verticalScaleProp.setConfigKey("north_arrow_height");
    northArrow.verticalScaleProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    northArrow.scaleProp.append(&northArrow.verticalScaleProp);

    northArrow.positionProp = MPropertyTemplates::GeographicPosition3D(
            "Geographic position", {0.f, 0.0f, 1050.0f});
    northArrow.positionProp.setConfigKey("north_arrow_geographic_position");
    northArrow.positionProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    northArrow.enabledProp.addSubProperty(northArrow.positionProp);

    northArrow.colourProp = MColorProperty("Colour", {222, 46, 30, 255});
    northArrow.colourProp.setConfigKey("north_arrow_colour");
    northArrow.colourProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    northArrow.enabledProp.addSubProperty(northArrow.colourProp);


    // Rendering group.
    renderingGroupProp = MProperty("Rendering");
    groupProp.addSubProperty(renderingGroupProp);

    multisamplingEnabledProp = MBoolProperty("Multisampling", true);
    multisamplingEnabledProp.setConfigKey("multisampling");
    multisamplingEnabledProp.registerValueCallback([=]()
    {
        enableOITProp.setEnabled(multisamplingEnabledProp);
        updateGL();
    });
    renderingGroupProp.addSubProperty(multisamplingEnabledProp);

    multiSamplingSamplesProp = MIntProperty("Samples", 8);
    multiSamplingSamplesProp.setMinMax(0, 64);
    multiSamplingSamplesProp.registerValueCallback([=]()
    {
        resizeGL(viewPortWidth, viewPortHeight);
        updateGL();
    });
    multisamplingEnabledProp.addSubProperty(multiSamplingSamplesProp);

    enableOITProp = MBoolProperty("Order independent transparency", true);
    enableOITProp.setConfigKey("order_independent_transparency");
    enableOITProp.setTooltip("Requires multisampling. "
                             "Impacts performance slightly, depending on sample count. "
                             "When using a DVR raycaster, performance impact is heavier.");
    enableOITProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    renderingGroupProp.addSubProperty(enableOITProp);

    antialiasingEnabledProp = MBoolProperty("Antialiasing", false);
    antialiasingEnabledProp.setConfigKey("antialiasing");
    antialiasingEnabledProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    renderingGroupProp.addSubProperty(antialiasingEnabledProp);

    renderLabelsWithDepthTestProp = MBoolProperty("Depth test for labels", true);
    renderLabelsWithDepthTestProp.setConfigKey("depth_test_for_labels");
    renderLabelsWithDepthTestProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    renderingGroupProp.addSubProperty(renderLabelsWithDepthTestProp);

    farPlaneDistanceProp = MFloatProperty("Far plane distance", 500.0);
    farPlaneDistanceProp.setMinimum(0.01);
    farPlaneDistanceProp.setConfigKey("far_plane_distance");
    farPlaneDistanceProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    renderingGroupProp.addSubProperty(farPlaneDistanceProp);

    shadowDistanceProp = MFloatProperty("Shadow distance", 400);
    shadowDistanceProp.setConfigKey("shadow_distance");
    shadowDistanceProp.setTooltip("The maximum distance at which a shadow is drawn. Increasing it will decrease shadow quality.");
    shadowDistanceProp.setMinimum(0);
    shadowDistanceProp.registerValueCallback(this, &MSceneViewGLWidget::updateGL);
    renderingGroupProp.addSubProperty(shadowDistanceProp);

    QStringList modesLst;
    modesLst << "Disabled" << "2K" << "4K" << "8K";
    shadowMapResolutionProp = MEnumProperty("Shadow map resolution", modesLst, shadowMapResProp);
    shadowMapResolutionProp.setConfigKey("shadow_map_resolution");
    shadowMapResolutionProp.registerValueCallback([=]()
    {
        shadowMapResProp = static_cast<ShadowResolution>(shadowMapResolutionProp.value());

        if (tex2DShadowMap != nullptr && shadowMapResProp != Disabled)
        {
            int res = getShadowMapResolution();

            tex2DShadowMap->bindToTextureUnit(texUnitShadowMap);
            tex2DShadowMap->updateSize(res, res);
            glTexImage3DMultisample(GL_TEXTURE_2D_MULTISAMPLE_ARRAY, multiSamplingSamplesProp, GL_DEPTH_COMPONENT32F, res, res, 3,false); CHECK_GL_ERROR;
            tex2DShadowMap->unbindFromTextureUnit(texUnitShadowMap);
        }

        updateGL();
    });
    renderingGroupProp.addSubProperty(shadowMapResolutionProp);

    useCustomResolutionProp = MBoolProperty("Adjust view resolution", false);
    useCustomResolutionProp.setConfigKey("adjust_view_resolution");
    useCustomResolutionProp.registerValueCallback([=]()
    {
        customResolutionWidthProp.setEnabled(useCustomResolutionProp);
        customResolutionHeightProp.setEnabled(useCustomResolutionProp);
        resizeGL(viewPortWidth, viewPortHeight);

        updateGL();
    });
    renderingGroupProp.addSubProperty(useCustomResolutionProp);

    customResolutionProp = MArrayProperty("Resolution");
    useCustomResolutionProp.addSubProperty(customResolutionProp);

    customResolutionWidthProp = MIntProperty("Width", defaultRenderResolutionX);
    customResolutionWidthProp.setEnabled(useCustomResolutionProp);
    customResolutionWidthProp.setMinimum(240);
    customResolutionWidthProp.setMaximum(1024 * 32);
    customResolutionWidthProp.setConfigKey("custom_resolution_width");
    customResolutionWidthProp.registerValueCallback([=]()
    {
        resizeGL(viewPortWidth, viewPortHeight);

        updateGL();
    });
    customResolutionProp.append(&customResolutionWidthProp);

    customResolutionHeightProp = MIntProperty("Height", defaultRenderResolutionY);
    customResolutionHeightProp.setEnabled(useCustomResolutionProp);
    customResolutionHeightProp.setMinimum(240);
    customResolutionHeightProp.setMaximum(1024 * 32);
    customResolutionHeightProp.setConfigKey("custom_resolution_height");
    customResolutionHeightProp.registerValueCallback([=]()
    {
        resizeGL(viewPortWidth, viewPortHeight);

        updateGL();
    });
    customResolutionProp.append(&customResolutionHeightProp);

    fsrQualityProp = MEnumProperty("AMD FSR 1");
    fsrQualityProp.setTooltip("AMD FidelityFX Super Resolution 1.0 is "
                                   "an advanced spatial upscaling technology optimized"
                                   " for high-quality image rendering at high framerates.");
    QStringList fsrQualityOptions;
    fsrQualityOptions << "Off (Native resolution)"
                      << "Ultra Quality"
                      << "Quality"
                      << "Balanced"
                      << "Performance";
    fsrQualityProp.setEnumNames(fsrQualityOptions);
    fsrQualityProp.setValue(fsrQuality);
    fsrQualityProp.setConfigKey("fsr_quality");
    fsrQualityProp.registerValueCallback([=]()
    {
        fsrQuality = FSRQuality(fsrQualityProp.value());

        resizeGL(viewPortWidth, viewPortHeight);

        updateGL();
    });
    renderingGroupProp.addSubProperty(fsrQualityProp);

#ifndef CONTINUOUS_GL_UPDATE
    measureFPSProp = MButtonProperty("30s FPS measurement", "Start");
    measureFPSProp.registerValueCallback([=]()
    {
        LOG4CPLUS_INFO(mlog, "Measuring FPS for 30 seconds...");
        // Enable FPS measurement.
        measureFPS = true;
        measureFPSFrameCount = 0;
        // Record measurements starting at the front of "fpsTimeseries".
        fpsTimeseriesIndex = 0;
        QTimer::singleShot(30000, this, SLOT(stopFPSMeasurement()));
        updateGL();
    });
    renderingGroupProp.addSubProperty(measureFPSProp);
#endif

    reloadShadersProp = MButtonProperty("Reload shaders", "Reload");
    reloadShadersProp.registerValueCallback([=]()
    {
        reloadShaders();
        updateGL();
    });
    renderingGroupProp.addSubProperty(reloadShadersProp);


    // Inform the scene view control about this scene view and connect to its
    // propertyChanged() signal.
    systemControl->registerSceneView(this);
    connect(this, &MSceneViewGLWidget::sceneInteractedWith,
            systemControl, &MSystemManagerAndControl::setLastFocus);

    // Observe the creation/deletion of actors -- if these support full-screen
    // mode, add to the list displayed in the full-screen actor property.
    connect(glRM, SIGNAL(actorCreated(MActor*)), SLOT(onActorCreated(MActor*)));
    connect(glRM, SIGNAL(actorDeleted(MActor*)), SLOT(onActorDeleted(MActor*)));
    connect(glRM, SIGNAL(actorRenamed(MActor*, QString)),
            SLOT(onActorRenamed(MActor*, QString)));

    // Set up a timer for camera auto-rotation.
    cameraAutoRotationTimer = new QTimer();
    cameraAutoRotationTimer->setInterval(20);
    connect(cameraAutoRotationTimer, SIGNAL(timeout()),
            this, SLOT(autoRotateCamera()));

    // We want to accept drag/drop events.
    setAcceptDrops(true);
}


MSceneViewGLWidget::~MSceneViewGLWidget()
{
    delete fpsStopwatch;
    if (focusShader) delete focusShader;

    if (myID == 0) {
        LOG4CPLUS_DEBUG(mlog, " ====== FPS timeseries ======");
        QString s;
        for (int i = 0; i < fpsTimeseriesSize; i++)
            s += QString("%1 ").arg(fpsTimeseries[i]);
        LOG4CPLUS_DEBUG(mlog, s);
        delete[] fpsTimeseries;
    }

    if (synchronizationControl != nullptr)
    {
        synchronizeWith(nullptr);
    }

    // Cleanup textures and shaders
    // For scene rendering and upscaling
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (tex2DSceneColor) glRM->releaseGPUItem(tex2DSceneColor);
    if (tex2DDepthBuffer) glRM->releaseGPUItem(tex2DDepthBuffer);
    if (tex2DFSRResult) glRM->releaseGPUItem(tex2DFSRResult);
    if (tex2DFinalColor) glRM->releaseGPUItem(tex2DFinalColor);
    if (tex2DFinalDepthBuffer) glRM->releaseGPUItem(tex2DFinalDepthBuffer);
    if (tex2DShadowMap) glRM->releaseGPUItem(tex2DShadowMap);
    if (vboViewportQuad) glRM->releaseGPUItem(vboViewportQuad);

    glDeleteFramebuffers(1, &sceneFbo);
    glDeleteFramebuffers(1, &sceneFboMsaa);
    glDeleteFramebuffers(1, &finalResultFbo);
    glDeleteFramebuffers(1, &shadowMappingFbo);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MSceneViewGLWidget::setScene(MSceneControl *scene)
{
    removeCurrentScene();

    LOG4CPLUS_INFO(mlog, "scene view " << myID+1 << " connects to scene "
                    << scene->getName());
    this->scene = scene;
    this->scene->registerSceneView(this);

#ifndef CONTINUOUS_GL_UPDATE
    connect(this->scene,
            SIGNAL(sceneChanged()),
            SLOT(updateGL()));
#endif

    connect(this->scene, SIGNAL(sceneLightingChanged()), SLOT(onLightChanged()));

    if (!viewIsInitialised) return;

    onLightChanged();
    updateSceneLabel();

#ifndef CONTINUOUS_GL_UPDATE
    updateGL();
#endif
}


void MSceneViewGLWidget::removeCurrentScene()
{
    // If this view is currently connected to a scene, disconnect from this
    // scene.
    if (scene)
    {
        LOG4CPLUS_INFO(mlog, "scene view " << myID+1
                        << " disconnects from scene "
                        << scene->getName());

        this->scene->unregisterSceneView(this);

#ifndef CONTINUOUS_GL_UPDATE
        disconnect(scene,
                   SIGNAL(sceneChanged()),
                   this,
                   SLOT(updateGL()));
#endif

        disconnect(scene,
                   SIGNAL(sceneLightingChanged()),
                   this,
                   SLOT(onLightChanged()));
        onLightChanged();
    }

    scene = nullptr;
}


QSize MSceneViewGLWidget::minimumSizeHint() const
{
    return QSize(80, 60);
}


QSize MSceneViewGLWidget::sizeHint() const
{
    return QSize(80, 60);
}


void MSceneViewGLWidget::setBackgroundColour(const QColor &color)
{
    backgroundColourProp = color;
#ifndef CONTINUOUS_GL_UPDATE
    updateGL();
#endif
}


double MSceneViewGLWidget::worldZfromPressure(double p_hPa)
{
    return worldZfromPressure(p_hPa, logpbot, slopePtoZ);
}


double MSceneViewGLWidget::worldZfromPressure(
        double p_hPa, double log_pBottom_hPa, double deltaZ_deltaLogP)
{
    return (log(p_hPa)-log_pBottom_hPa) * deltaZ_deltaLogP;
}


double MSceneViewGLWidget::pressureFromWorldZ(double z)
{
    return exp(z / slopePtoZ + logpbot);
}


double MSceneViewGLWidget::worldZMeterScaling()
{
    double hmBottom = pressure2metre_standardICAO(pbot * 100);
    double hmTop = pressure2metre_standardICAO(ptop * 100);
    double r = (hmTop - hmBottom) / (ztop - zbot);

    return r;
}

QVector2D MSceneViewGLWidget::pressureToWorldZParameters()
{
    return QVector2D(logpbot, slopePtoZ);
}


QVector3D MSceneViewGLWidget::lonLatPToClipSpace(const QVector3D& lonlatp)
{
    QVector3D worldspace = QVector3D(lonlatp.x(),
                                     lonlatp.y(),
                                     worldZfromPressure(lonlatp.z()));
    return modelViewProjectionMatrix * worldspace;
}


QVector3D MSceneViewGLWidget::worldToClipSpace(const QVector3D &worldSpace)
{
    return modelViewProjectionMatrix * worldSpace;
}


QVector3D MSceneViewGLWidget::clipSpaceToLonLatWorldZ(const QVector3D &clipPos)
{
    QVector3D worldSpacePos = modelViewProjectionMatrixInverted * clipPos;
    return worldSpacePos;
}


QVector3D MSceneViewGLWidget::clipSpaceToLonLatP(const QVector3D &clipPos)
{
    QVector3D lonLatPPos = clipSpaceToLonLatWorldZ(clipPos);
    lonLatPPos.setZ(pressureFromWorldZ(lonLatPPos.z()));
    return lonLatPPos;
}


QVector3D MSceneViewGLWidget::projectClipSpaceToWorld(const QPointF &clip,
                                                      const QVector3D &ref,
                                                      const QVector3D &n)
{
    float clipX = static_cast<float>(clip.x());
    float clipY = static_cast<float>(clip.y());
    // Select an arbitrary z-value to construct a point in clip space that,
    // transformed to world space, lies on the ray passing through the camera
    // and the location on the worldZ==0 plane "picked" by the mouse.
    // (See notes 22-23Feb2012).
    QVector3D mousePosClipSpace = QVector3D(clipX, clipY, 0.);

    // The point p at which the ray intersects the worldZ==0 plane is found by
    // computing the value d in p=d*l+l0, where l0 is a point on the ray and l
    // is a vector in the direction of the ray. d can be found with
    //        (p0 - l0) * n
    //   d = ----------------
    //            l * n
    // where p0 (the ref parameter) is a point on the worldZ==0 plane and n is the normal vector
    // of the plane.
    //       http://en.wikipedia.org/wiki/Line-plane_intersection

    // To compute l0, the MVP matrix has to be inverted.
    QMatrix4x4 *mvpMatrixInverted = getModelViewProjectionMatrixInverted();
    QVector3D l0 = *mvpMatrixInverted * mousePosClipSpace;

    // Compute l as the vector from l0 to the camera origin.
    QVector3D cameraPosWorldSpace = camera.getOrigin();
    QVector3D l = (l0 - cameraPosWorldSpace);

    // Compute the mouse position in world space.
    float d = QVector3D::dotProduct(ref - l0, n) / QVector3D::dotProduct(l, n);
    return l0 + d * l;
}


QVector3D MSceneViewGLWidget::getLightDirection()
{
    if (sceneDirectionalLight != nullptr)
    {
        return sceneDirectionalLight->direction();
    }
    else
    {
        return {0, 0, -1};
    }
}


void MSceneViewGLWidget::bindShadowMap(
        std::shared_ptr<GL::MShaderEffect> shader, GLuint texUnit)
{
    tex2DShadowMap->bindToTextureUnit(texUnit);
    shader->setUniformValue("sceneShadowMap", texUnit);
}


void MSceneViewGLWidget::setVerticalScaling(float scaling)
{
    verticalScalingProp = scaling;
}

float MSceneViewGLWidget::getVerticalScaling()
{
    return ztop;
}


void MSceneViewGLWidget::setInteractionMode(bool enabled)
{
    // Don't change interaction mode if property is disabled.
    if (!actorInteractionModeProp.isEnabled())
    {
        return;
    }

    // Analysis mode cannot be active at the same time.
    if (enabled && analysisModeProp)
        analysisModeProp = false;

    actorInteractionModeProp = enabled;
}


void MSceneViewGLWidget::setAnalysisMode(bool enabled)
{
    // Don't change analysis mode if property is disabled.
    if (!analysisModeProp.isEnabled())
    {
        return;
    }

    // Interaction mode cannot be active at the same time.
    if (enabled && actorInteractionModeProp)
        actorInteractionModeProp = false;

    analysisModeProp = enabled;
}


void MSceneViewGLWidget::setAutoRotationMode(bool enabled)
{
    // Auto-rotation can only be set in ROTATE_SCENE mode.
    if (sceneNavigationMode != ROTATE_SCENE) return;

    cameraAutorotationModeProp = enabled;
}


void MSceneViewGLWidget::setFreeze(bool enabled)
{
    if (enabled)
    {
        freezeMode++;
    }
    else
    {
        freezeMode--;
        if (freezeMode < 0) freezeMode = 0;
    }

#ifndef CONTINUOUS_GL_UPDATE
    if ( viewIsInitialised && (!freezeMode) )
    {
        updateGL();
    }
#endif
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MSceneViewGLWidget::executeCameraAction(int action,
                                             bool ignoreWithoutFocus)
{
    // Only act on this signal if we have input focus.
    if ( ignoreWithoutFocus && (!hasFocus()) )
    {
        return;
    }

    // If in full-screen mode, don't move camera.
    if (sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR)
    {
        return;
    }

    // Get current camera axes.
    QVector3D xAxis = camera.getXAxis();
    QVector3D yAxis = camera.getYAxis();
    QVector3D zAxis = camera.getZAxis();
    QVector3D origin = camera.getOrigin();

    // Modify axes.
    switch (action)
    {
    case CAMERA_NORTHUP:
        yAxis.setX(0);
        zAxis.setX(0);
        camera.setYAxis(yAxis);
        camera.setZAxis(zAxis);
        break;
    case CAMERA_TOPVIEW:
        origin.setZ(250.);
        camera.setOrigin(origin);
        camera.setYAxis(QVector3D(0, 1., 0));
        camera.setZAxis(QVector3D(0, 0, -1.));
        break;
    case CAMERA_UPRIGHT:
        camera.setYAxis(QVector3D(0, 0, 1.));
        camera.setZAxis(QVector3D::crossProduct(
                            QVector3D(0, 0, 1.),xAxis));
        break;
    case CAMERA_SAVETOFILE:
        camera.saveConfigurationToFile();
        break;
    case CAMERA_LOADFROMFILE:
        camera.loadConfigurationFromFile();
        break;
    }

    updateCameraPositionDisplay();

#ifndef CONTINUOUS_GL_UPDATE
    if (viewIsInitialised && (!freezeMode)) updateGL();
#endif
}


void MSceneViewGLWidget::updateDisplayTime()
{
    MTextManager* tm = MGLResourcesManager::getInstance()->getTextManager();

    int i = staticLabels.indexOf(displayDateTimeLabel);
    if (i >= 0)
    {
        staticLabels.removeAt(i);
        tm->removeText(displayDateTimeLabel);
    }

    if (synchronizationControl == nullptr || !MSystemManagerAndControl::getInstance()->applicationIsInitialized())
    {
        return;
    }

    if (!displayDateTimeLabelProps.enabledProp)
    {
        if (viewIsInitialised)
        {
            updateGL();
        }
        return;
    }

    QDateTime validTime = synchronizationControl->validDateTime();
    QDateTime initTime  = synchronizationControl->initDateTime();

    QString str = QString("Valid: %1 (step %2 hrs from %3)")
            .arg(validTime.toString("ddd yyyy-MM-dd hh:mm UTC"))
            .arg(int(initTime.secsTo(validTime) / 3600.))
            .arg(initTime.toString("ddd yyyy-MM-dd hh:mm UTC"));

    QPointF position = displayDateTimePositionProp;

    displayDateTimeLabel = tm->addText(
            str,
            MTextManager::CLIPSPACE,
            position.x(), position.y(), -0.99,
            displayDateTimeLabelProps.fontSizeProp,
            displayDateTimeLabelProps.fontColourProp,
            MTextManager::UPPERLEFT,
            displayDateTimeLabelProps.enableBBoxProp,
            displayDateTimeLabelProps.bboxColourProp);

    staticLabels.append(displayDateTimeLabel);

#ifndef CONTINUOUS_GL_UPDATE
    if (viewIsInitialised)
    {
        updateGL();
    }
#endif
}


void MSceneViewGLWidget::updateFPSTimer()
{
    // Perform a stopwatch split next frame.
    splitNextFrame = true;
}


void MSceneViewGLWidget::stopFPSMeasurement()
{
    measureFPS = false;

    float avgRenderTimeMeasurementPeriod = 30000. / float(measureFPSFrameCount);
    float avgFPSMeasurementPeriod = float(measureFPSFrameCount) / 30.;

    LOG4CPLUS_INFO(mlog, "fps measurement is stopped"
                    << "; number of frames in 30s: " << measureFPSFrameCount
                    << "; average render time over 30s: "
                    << avgRenderTimeMeasurementPeriod << " ms ("
                    << avgFPSMeasurementPeriod << " fps)");
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MSceneViewGLWidget::initializeGL()
{
    LOG4CPLUS_INFO(mlog, "Initialising OpenGL context of scene view " << myID);
    LOG4CPLUS_INFO(mlog, "\tOpenGL context is "
                    << (context()->isValid() ? "" : "NOT ") << "valid.");
    LOG4CPLUS_INFO(mlog, "\tOpenGL context is "
                    << (context()->isSharing() ? "" : "NOT ") << "sharing.");

    glGenVertexArrays(1, &defaultVao);
    glBindVertexArray(defaultVao);

    int maxMSAASamples = 0;
    glGetIntegerv(GL_MAX_SAMPLES, &maxMSAASamples);
    multiSamplingSamplesProp.setMaximum(maxMSAASamples);
    if (maxMSAASamples <= 0)
    {
        multisamplingEnabledProp = false;
        multisamplingEnabledProp.setHidden(true);
    }

    // Create the widget's only shader: To draw the focus rectangle.
    QGLShader *vshader = new QGLShader(QGLShader::Vertex, this);
    const char *vsrc =
        "#version 130\n"
        "in vec2 vertex;\n"
        "void main(void)\n"
        "{\n"
        "    gl_Position = vec4(vertex.xy, -1, 1);\n"
        "}\n";
    vshader->compileSourceCode(vsrc);

    QGLShader *fshader = new QGLShader(QGLShader::Fragment, this);
    const char *fsrc =
        "#version 130\n"
        "uniform vec4 colourValue;\n"
        "out vec4 fragColour;\n"
        "void main(void)\n"
        "{\n"
        "    fragColour = colourValue;\n"
        "}\n";
    fshader->compileSourceCode(fsrc);

    focusShader = new QGLShaderProgram(this);
    focusShader->addShader(vshader);
    focusShader->addShader(fshader);
#define FOCUSSHADER_VERTEX_ATTRIBUTE 0
    focusShader->bindAttributeLocation("vertex", FOCUSSHADER_VERTEX_ATTRIBUTE);
    focusShader->link();

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    bool loadShaders = false;
    loadShaders |= glRM->generateEffectProgram("north_arrow", northArrowShader);
    loadShaders |= glRM->generateEffectProgram("scene_final_render", renderToViewportShader);
    loadShaders |= glRM->generateEffectProgram("fsr_pass", fsrPassShader);

    if (loadShaders)
    {
        reloadShaders();
    }

    if (!vboViewportQuad)
    {
        const QString vboID = QString("vbo_viewport_quad_scene_#%1").arg(myID);
        GL::MFloat2VertexBuffer* newVB =
                new GL::MFloat2VertexBuffer(vboID, 4);
        if (glRM->tryStoreGPUItem(newVB))
        {
            float vertexData[8] = {-1, -1, 1, -1, 1, 1, -1, 1};
            newVB->upload(vertexData, 8, this); CHECK_GL_ERROR;
            vboViewportQuad = static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(vboID));
        }
        else
        {
            LOG4CPLUS_ERROR(mlog, "Cannot store vertex buffer for viewport"
                           " in GPU memory.");
            delete newVB;
            return;
        }
    }

    bool existed;

    glBindBuffer(GL_ARRAY_BUFFER, 0);  CHECK_GL_ERROR;

    // Create scene color framebuffer for MSAA
    glGenFramebuffers(1, &sceneFboMsaa); CHECK_GL_ERROR;
    switchFramebuffer(sceneFboMsaa); CHECK_GL_ERROR;

    // Create scene color renderbuffers for MSAA
    glGenRenderbuffers(1, &sceneColorRbo); CHECK_GL_ERROR;
    glBindRenderbuffer(GL_RENDERBUFFER, sceneColorRbo); CHECK_GL_ERROR;
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, multiSamplingSamplesProp, GL_RGB, defaultRenderResolutionX, defaultRenderResolutionY); CHECK_GL_ERROR;
    glBindRenderbuffer(GL_RENDERBUFFER, 0); CHECK_GL_ERROR;
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_RENDERBUFFER, sceneColorRbo); CHECK_GL_ERROR;

    glGenRenderbuffers(1, &sceneDepthRbo); CHECK_GL_ERROR;
    glBindRenderbuffer(GL_RENDERBUFFER, sceneDepthRbo); CHECK_GL_ERROR;
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, multiSamplingSamplesProp, GL_DEPTH_COMPONENT32, defaultRenderResolutionX, defaultRenderResolutionY); CHECK_GL_ERROR;
    glBindRenderbuffer(GL_RENDERBUFFER, 0); CHECK_GL_ERROR;
    glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, sceneDepthRbo); CHECK_GL_ERROR;

    GLenum state = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (state != GL_FRAMEBUFFER_COMPLETE)
    {
        LOG4CPLUS_ERROR(mlog, "Scene color msaa FBO is not completed.");
        LOG4CPLUS_ERROR(mlog, "Framebuffer status is " << state);
    }

    switchFramebuffer(0); CHECK_GL_ERROR;

    // Create scene color framebuffer. This contains the 3D scene color information.
    // Renders the scene at the lower or native resolution, which might be upscaled later.
    glGenFramebuffers(1, &sceneFbo); CHECK_GL_ERROR;
    switchFramebuffer(sceneFbo); CHECK_GL_ERROR;

    // Create color buffer texture for the scene color fbo.
    if (!tex2DSceneColor)
    {
        const QString sceneColorTextureID = QString("scene_color_scene_#%1").arg(myID);

        texUnitSceneColor = 0;
        tex2DSceneColor = glRM->createTexture(sceneColorTextureID, &existed, GL_TEXTURE_2D, GL_RGB8, defaultRenderResolutionX, defaultRenderResolutionY); CHECK_GL_ERROR;

        if (tex2DSceneColor)
        {
            tex2DSceneColor->bindToTextureUnit(texUnitSceneColor); CHECK_GL_ERROR;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, defaultRenderResolutionX, defaultRenderResolutionY, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
            glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2DSceneColor->getTextureObject(), 0); CHECK_GL_ERROR;
        }
    }

    // Create depth buffer texture for the scene color fbo.
    if (!tex2DDepthBuffer)
    {
        const QString depthBufferTextureID = QString("scene_depth_buffer_scene_#%1").arg(myID);

        texUnitDepthBuffer = 1;
        tex2DDepthBuffer = glRM->createTexture(depthBufferTextureID, &existed, GL_TEXTURE_2D, GL_DEPTH_COMPONENT32, defaultRenderResolutionX, defaultRenderResolutionY); CHECK_GL_ERROR;

        if (tex2DDepthBuffer)
        {
            tex2DDepthBuffer->bindToTextureUnit(texUnitDepthBuffer); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_GL_ERROR;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, defaultRenderResolutionX, defaultRenderResolutionY, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr); CHECK_GL_ERROR;
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex2DDepthBuffer->getTextureObject(), 0); CHECK_GL_ERROR;
            glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
        }
    }

    state = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (state != GL_FRAMEBUFFER_COMPLETE)
    {
        LOG4CPLUS_ERROR(mlog, "Scene color FBO is not completed.");
        LOG4CPLUS_ERROR(mlog, "Framebuffer status is " << state);
    }

    switchFramebuffer(0); CHECK_GL_ERROR;

    // Create final color framebuffer.
    // Renders the final scene result at native resolution aswell as 2D elements such as text.
    glGenFramebuffers(1, &finalResultFbo); CHECK_GL_ERROR;
    switchFramebuffer(finalResultFbo); CHECK_GL_ERROR;

    // Create color buffer texture for the final color fbo.
    if (!tex2DFinalColor)
    {
        const QString sceneColorTextureID = QString("final_color_scene_#%1").arg(myID);

        texUnitFinalColor = 2;
        tex2DFinalColor = glRM->createTexture(sceneColorTextureID, &existed, GL_TEXTURE_2D, GL_RGB8, defaultRenderResolutionX, defaultRenderResolutionY); CHECK_GL_ERROR;

        if (tex2DFinalColor)
        {
            tex2DFinalColor->bindToTextureUnit(texUnitFinalColor); CHECK_GL_ERROR;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, defaultRenderResolutionX, defaultRenderResolutionY, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
            glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, tex2DFinalColor->getTextureObject(), 0); CHECK_GL_ERROR;
        }
    }

    // Create depth buffer texture for the final color fbo.
    if (!tex2DFinalDepthBuffer)
    {
        const QString depthBufferTextureID = QString("final_depth_buffer_scene_#%1").arg(myID);

        texUnitFinalDepthBuffer = 3;
        tex2DFinalDepthBuffer = glRM->createTexture(depthBufferTextureID, &existed, GL_TEXTURE_2D, GL_DEPTH_COMPONENT32, defaultRenderResolutionX, defaultRenderResolutionY); CHECK_GL_ERROR;

        if (tex2DFinalDepthBuffer)
        {
            tex2DFinalDepthBuffer->bindToTextureUnit(texUnitFinalDepthBuffer); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR); CHECK_GL_ERROR;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, defaultRenderResolutionX, defaultRenderResolutionY, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr); CHECK_GL_ERROR;
            glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, tex2DFinalDepthBuffer->getTextureObject(), 0); CHECK_GL_ERROR;
            glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
        }
    }

    state = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (state != GL_FRAMEBUFFER_COMPLETE)
    {
        LOG4CPLUS_ERROR(mlog, "Final render FBO is not completed.");
        LOG4CPLUS_ERROR(mlog, "Framebuffer status is " << state);
    }

    switchFramebuffer(0); CHECK_GL_ERROR;

    // Create FSR result texture. This texture holds the resulting upscaled scene color after the FSR pass.
    if (!tex2DFSRResult)
    {
        const QString fsrResultTextureID = QString("fsr_result_scene_#%1").arg(myID);

        texUnitFSRResult = 4;

        tex2DFSRResult = glRM->createTexture(fsrResultTextureID, &existed, GL_TEXTURE_2D, GL_RGBA32F, defaultRenderResolutionX, defaultRenderResolutionY); CHECK_GL_ERROR;

        if (tex2DFSRResult)
        {
            tex2DFSRResult->bindToTextureUnit(texUnitFSRResult); CHECK_GL_ERROR;
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, defaultRenderResolutionX, defaultRenderResolutionY, 0, GL_RGBA, GL_FLOAT, nullptr); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_NEAREST); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_NEAREST); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
            glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
            glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
        }
    }

    // Create shadow mapping framebuffer.
    glGenFramebuffers(1, &shadowMappingFbo); CHECK_GL_ERROR;
    switchFramebuffer(shadowMappingFbo); CHECK_GL_ERROR;

    // Create depth buffer texture for the final color fbo.
    if (!tex2DShadowMap)
    {
        const QString depthBufferTextureID = QString("shadow_map_scene_#%1").arg(myID);

        int res = getShadowMapResolution();

        texUnitShadowMap = 5;
        tex2DShadowMap = glRM->createTexture(depthBufferTextureID, &existed, GL_TEXTURE_2D_MULTISAMPLE_ARRAY, GL_DEPTH_COMPONENT32F, res, res, 3); CHECK_GL_ERROR;

        if (tex2DShadowMap)
        {
            tex2DShadowMap->bindToTextureUnit(texUnitShadowMap); CHECK_GL_ERROR;
            glTexImage3DMultisample(GL_TEXTURE_2D_MULTISAMPLE_ARRAY, multiSamplingSamplesProp, GL_DEPTH_COMPONENT32F, res, res, 3, false); CHECK_GL_ERROR;
            glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex2DShadowMap->getTextureObject(), 0, 0); CHECK_GL_ERROR;
            glBindTexture(GL_TEXTURE_2D_MULTISAMPLE_ARRAY, 0); CHECK_GL_ERROR;
        }
    }

    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);

    state = glCheckFramebufferStatus(GL_FRAMEBUFFER);

    if (state != GL_FRAMEBUFFER_COMPLETE)
    {
        LOG4CPLUS_ERROR(mlog, "Shadow map FBO is not completed.");
        LOG4CPLUS_ERROR(mlog, "Framebuffer status is " << state);
    }

    switchFramebuffer(0); CHECK_GL_ERROR;

    QString ssboSceneLightsID = QString("scene_lights_#%1").arg(myID);

    if (sceneLightsBuffer == nullptr)
    {
        sceneLightsBuffer = new GL::MShaderStorageBufferObject(ssboSceneLightsID, sizeof(MLight), 0);

        if (glRM->tryStoreGPUItem(sceneLightsBuffer))
        {
            sceneLightsBuffer = dynamic_cast<GL::MShaderStorageBufferObject*>(glRM->getGPUItem(ssboSceneLightsID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store scene lights buffer in GPU memory.");
            delete sceneLightsBuffer;
            sceneLightsBuffer = nullptr;
        }
    }

    if (lightUniformBuffer == nullptr)
    {
        QString uboSceneLightingID = QString("scene_light_uniform_buffer_#%1").arg(myID);
        lightUniformBuffer = new GL::MUniformBufferObject<LightingUniforms>(uboSceneLightingID);

        if (glRM->tryStoreGPUItem(lightUniformBuffer))
        {
            lightUniformBuffer = dynamic_cast<GL::MUniformBufferObject<LightingUniforms>*>(glRM->getGPUItem(uboSceneLightingID));
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Cannot store scene light uniform buffer in GPU memory.");
            delete lightUniformBuffer;
            lightUniformBuffer = nullptr;
        }
    }

    // Initial OpenGL settings.
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);

    // Initialise the not shared OpenGL resources of the scene's actors.
    if (scene)
    {
        LOG4CPLUS_INFO(mlog, "Initialising not-shared OpenGL resources of "
                        << "the scene's actors...");
        const QVector<MActor*>& renderQueue = scene->getRenderQueue();
        for (int i = 0; i < renderQueue.size(); i++)
        {
            renderQueue[i]->initializePerGLContextResources(this);
        }

        updateSceneLabel();
    }

    // Show scene time.
    updateDisplayTime();
    updateCameraPositionDisplay();

    // Refresh light.
    onLightChanged();

    viewIsInitialised = true;
    LOG4CPLUS_INFO(mlog, "Initialisation done\n");
}


void MSceneViewGLWidget::updateGL()
{
    if (isLoadingFromConfig) return;
    // Don't update GL if no scene is attached to the scene view.
    if (scene != nullptr)
    {
        makeCurrent();
        // Call update instead of QGLWidget::updateGL, since it waits to the next update cycle.
        update();
    }
}


void MSceneViewGLWidget::paintGL()
{
    // Only render this widget if it is visible.
    if (!isVisible()) return;
    if (freezeMode) return;
    if (!getScene()) return;

    currentCamera = &camera;

    // Status information: The "main" scene view instance measures frame rate.
    if (myID == 0)
    {
        if (splitNextFrame)
        {
            fpsStopwatch->split();

            double frameTime = fpsStopwatch->getLastSplitTime(MStopwatch::SECONDS);
            QString fpsString = QString("%1 ms (%2 fps)")
                    .arg(frameTime/frameCount*1000., 0, 'f', 1)
                    .arg(frameCount/frameTime, 0, 'f', 1);

            MGLResourcesManager::getInstance()->getRenderTimeProp()->setValue(fpsString);
            if (measureFPS) LOG4CPLUS_INFO(mlog, fpsString);

            fpsTimeseries[fpsTimeseriesIndex++] = frameCount/frameTime;
            if (fpsTimeseriesIndex == fpsTimeseriesSize) fpsTimeseriesIndex = 0;

            frameCount = 0;
            splitNextFrame = false;
        }

        frameCount++;
    }

    // In ROTATE_SCENE mode, rotate the camera around the current scene centre.
    // Compute the new camera position.
    if (sceneNavigationMode == ROTATE_SCENE)
    {
        // Compute worldZ coordinate from sceneRotationCentre pressure
        // coordinate.
        QVector3D sceneRotationCentre = sceneRotationCenterProp;
        double z = worldZfromPressure(sceneRotationCentre.z());

        // Compute a matrix to translate the camera position in world space to
        // the sceneRotationCentre.
        QMatrix4x4 updateCameraMatrix;
        updateCameraMatrix.setToIdentity();
        updateCameraMatrix.translate(
                sceneRotationCentre.x(), sceneRotationCentre.y(), z);

        // Rotate the camera around the current centre by the rotation defined
        // by the sceneRotationMatrix.
        updateCameraMatrix *= sceneRotationMatrix;

        // Translate the camera position back to its origin.
        updateCameraMatrix.translate(
                -sceneRotationCentre.x(), -sceneRotationCentre.y(), -z);

        updateCameraMatrix = updateCameraMatrix.inverted();
        sceneRotationMatrix = sceneRotationMatrix.inverted();

        // Update camera position.
        camera.setOrigin(updateCameraMatrix * camera.getOrigin());
        camera.setYAxis(sceneRotationMatrix * camera.getYAxis());
        camera.setZAxis(sceneRotationMatrix * camera.getZAxis());

        // Reset current rotation.
        sceneRotationMatrix.setToIdentity();
    }

    // Update camera FOV.
    const float ratio = static_cast<float>(renderResultWidth)
            / static_cast<float>(renderResultHeight);

    camera.setFovRatio(ratio);

    // Set render step to shadow mapping.
    currentRenderStep = SHADOW_MAPPING;

    bool updatePackedLights = false;
    for (MLightActor *light : getScene()->getSceneLights())
    {
        if (!light->isEnabled() || !light->isHeadlight()) continue;
        if (light->updateHeadlight(this))
        {
            updatePackedLights = true;
        }
    }

    if (updatePackedLights)
    {
        updateSceneLightsBuffer();
    }

    setMultisampleGLState();

    // Render shadow map.
    sceneLightsBuffer->bindToIndex(3);

    if (sceneDirectionalLight != nullptr &&
        sceneNavigationMode != SINGLE_FULLSCREEN_ACTOR &&
        shadowMapResProp != Disabled)
    {
        float originalNearPlane = camera.getNearPlane();
        float originalFarPlane = camera.getFarPlane();
        float shadowsFarPlane = std::min(shadowDistanceProp.value(), originalFarPlane);
        float delta = shadowsFarPlane - originalNearPlane;
        QVector3D originalDir = camera.getZAxis();
        QVector3D originalPos = camera.getOrigin();

        // Calculate split planes.
        splits[0] = originalNearPlane;
        splits[1] = delta / 6.0f;
        splits[2] = (delta - splits[0]) / 3.0f + splits[1];
        splits[3] = shadowsFarPlane;

        MCamera shadowCam = camera;
        currentCamera = &shadowCam;

        for (int split = 1; split <= 3; split++)
        {
            shadowCam.setNearPlane(splits[split - 1]);
            shadowCam.setFarPlane(splits[split]);

            glViewport(0, 0, tex2DShadowMap->getWidth(), tex2DShadowMap->getHeight());
            switchFramebuffer(shadowMappingFbo);
            glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex2DShadowMap->getTextureObject(), 0, split - 1); CHECK_GL_ERROR;
            glClear(GL_DEPTH_BUFFER_BIT);
            glEnable(GL_DEPTH_TEST);
            glDepthFunc(GL_LESS);
            glBindVertexArray(defaultVao);

            projectionMatrix = sceneDirectionalLight->getProjectionMatrix(this);
            viewMatrix = sceneDirectionalLight->getViewMatrix(this);
            modelViewProjectionMatrix = projectionMatrix * viewMatrix;
            modelViewProjectionMatrixInverted = modelViewProjectionMatrix.inverted();
            shadowCam.setOrigin(viewMatrix.inverted() * QVector3D(0.0, 0.0, 0.0));
            shadowCam.setZAxis(sceneDirectionalLight->direction());

            if (scene)
            {
                // Use the render queue of the scene to
                // render all actors in the scene.

                // Get a reference to the scene's render queue and render the actors.
                // Collect the actor's labels -- they are rendered in the next step.
                for (MActor* actor : scene->getRenderQueue())
                {
                    if (actor->castsShadows() && actor->isEnabled())
                    {
                        actor->render(this);
                    }
                }
            }

            lightViewProjMatrix[split - 1] = modelViewProjectionMatrix;

            shadowCam.setOrigin(originalPos);
            shadowCam.setZAxis(originalDir);
        }

        currentCamera = &camera;
    }
    else
    {
        glViewport(0, 0, tex2DShadowMap->getWidth(), tex2DShadowMap->getHeight());
        switchFramebuffer(shadowMappingFbo);
        for (int split = 1; split <= 3; split++)
        {
            glFramebufferTextureLayer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, tex2DShadowMap->getTextureObject(), 0, split - 1); CHECK_GL_ERROR;
            glClear(GL_DEPTH_BUFFER_BIT);
        }
    }

    // Setup framebuffer
    switchFramebuffer(sceneFboMsaa); CHECK_GL_ERROR;
    glViewport(0, 0, fsrViewPortWidth, fsrViewPortHeight); CHECK_GL_ERROR;
    glEnable(GL_DEPTH_TEST); CHECK_GL_ERROR;
    glDepthFunc(GL_LESS);
    glBindVertexArray(defaultVao);

    // Set render step to scene.
    currentRenderStep = RENDER_SCENE;

    setMultisampleGLState();

    if (antialiasingEnabledProp)
    {
        glEnable(GL_LINE_SMOOTH);
        glHint(GL_LINE_SMOOTH_HINT, GL_NICEST);
        glEnable(GL_POLYGON_SMOOTH);
        glHint(GL_POLYGON_SMOOTH_HINT, GL_NICEST);
    }
    else
    {
        glDisable(GL_LINE_SMOOTH);
        glDisable(GL_POLYGON_SMOOTH);
    }

    qglClearColor(backgroundColourProp);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // Compute model-view-projection matrix.
    modelViewProjectionMatrix.setToIdentity();

    projectionMatrix = camera.getProjectionMatrix();
    viewMatrix = camera.getViewMatrix();
    modelViewProjectionMatrix = projectionMatrix * viewMatrix;
    modelViewProjectionMatrixInverted = modelViewProjectionMatrix.inverted();

    // Update uniform buffer for lighting.glsl
    updateLightingUniformBuffer();

    QList<MLabel*> labelList;
    labelList.append(staticLabels);

    // 01. Render 3D scene with depth testing.
    if (scene)
    {
        bool _interactionMode = actorInteractionModeProp;

        if (sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR)
        {
            // If full-screen mode is active, only the selected full-screen
            // actor should be drawn.
            if (fullScreenActor != nullptr)
            {
                if (singleInteractionActor != nullptr &&
                        (singleInteractionActor->getName()
                         == fullScreenActor->getName()))
                {
                    actorInteractionModeProp = true;
                }

                fullScreenActor->renderToFullScreen(this);

                if (fullScreenActor == pickedActor.actor)
                {
                    labelList.append(fullScreenActor->getPositionLabelToRender());
                }
                labelList.append(fullScreenActor->getLabelsToRender());

                if (singleInteractionActor != nullptr &&
                        (singleInteractionActor->getName()
                         == fullScreenActor->getName()))
                {
                    actorInteractionModeProp = false;
                }
            }
        }
        else
        {
            // If NOT in full-screen mode, use the render queue of the scene to
            // render all actors in the scene.

            // Temporary ubo representation for the light uniforms, so that
            // we can update the "receiveShadows" uniform for each actor.
            LightingUniforms tmpLightingUniforms{};

            // Get a reference to the scene's render queue and render the actors.
            // Collect the actor's labels -- they are rendered in the next step.
            for (MActor* actor : scene->getRenderQueue())
            {
                if (singleInteractionActor != nullptr &&
                        singleInteractionActor->getName() == actor->getName())
                {
                    actorInteractionModeProp = true;
                }

                // Update the light uniform buffer receiveShadows member for each
                // actor. This is not really optimal, since we update a part of a
                // global UBO for each actor instead of setting a uniform in
                // each actor.
                tmpLightingUniforms.receiveShadows = actor->receivesShadows();
                lightUniformBuffer->updateSubData(&tmpLightingUniforms, &tmpLightingUniforms.receiveShadows);

                actor->render(this);

                if (actor == pickedActor.actor)
                {
                    labelList.append(actor->getPositionLabelToRender());
                }
                labelList.append(actor->getLabelsToRender());

                if (singleInteractionActor != nullptr &&
                        singleInteractionActor->getName() == actor->getName())
                {
                    actorInteractionModeProp = false;
                }
            }
        }

        actorInteractionModeProp = _interactionMode;
    }

    needSceneLightUpdate = false;

    if (northArrow.enabledProp)
    {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        northArrowShader->bind();
        northArrowShader->setUniformValue("colour", northArrow.colourProp);
        northArrowShader->setUniformValue("lightDirection", getLightDirection());
        northArrowShader->setUniformValue("horizontalScale",
                                          northArrow.horizontalScaleProp);
        northArrowShader->setUniformValue("verticalScale",
                                          northArrow.verticalScaleProp);
        northArrowShader->setUniformValue("lon", northArrow.positionProp.value().x());
        northArrowShader->setUniformValue("lat", northArrow.positionProp.value().y());
        northArrowShader->setUniformValue("worldZ", worldZfromPressure(northArrow.positionProp.value().z()));
        northArrowShader->setUniformValue("rotationMatrix", sceneRotationMatrix);
        northArrowShader->setUniformValue("mvpMatrix",
                                          modelViewProjectionMatrix);
        glDrawArrays(GL_POINTS, 0, 1);
    }

    // Copy main frame buffer to scene frame buffer, which contains the texture to upscale.
    glBlitNamedFramebuffer(sceneFboMsaa, sceneFbo, 0, 0, fsrViewPortWidth, fsrViewPortHeight, 0, 0, fsrViewPortWidth, fsrViewPortHeight, GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT, GL_NEAREST); CHECK_GL_ERROR;

    // 02. Switch to final color framebuffer, as we now want to render full resolution for UI and up scaled image
    switchFramebuffer(finalResultFbo); CHECK_GL_ERROR;
    glViewport(0, 0, renderResultWidth, renderResultHeight); CHECK_GL_ERROR;
    qglClearColor(backgroundColourProp); CHECK_GL_ERROR;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); CHECK_GL_ERROR;
    glDisable(GL_CULL_FACE); CHECK_GL_ERROR;

    // ... Upscale using FSR, then render the final image to a quad on the screen.
    if (fsrQuality != FSRQuality::OFF)
    {
        // Run FSR in compute shader and render it to tex2DFSRResult
        fsrPassShader->bind(); CHECK_GL_ERROR;
        fsrPassShader->bindProgram("EASU_PASS"); CHECK_GL_ERROR;
        fsrPassShader->setUniformValue("sourceViewport", QVector4D((float)fsrViewPortWidth, (float)fsrViewPortHeight, 0, 0)); CHECK_GL_ERROR;
        fsrPassShader->setUniformValue("sourceSize", QVector4D((float)fsrViewPortWidth, (float)fsrViewPortHeight, 0, 0)); CHECK_GL_ERROR;
        fsrPassShader->setUniformValue("outputSize", QVector4D((float)renderResultWidth, (float)renderResultHeight, 0, 0)); CHECK_GL_ERROR;
        tex2DSceneColor->bindToTextureUnit(texUnitSceneColor); CHECK_GL_ERROR;
        fsrPassShader->setUniformValue("inputSampler",  GLint(texUnitSceneColor)); CHECK_GL_ERROR;
        tex2DFSRResult->bindToTextureUnit(texUnitFSRResult);
        glBindImageTexture(0, tex2DFSRResult->getTextureObject(), 0, GL_TRUE, 0, GL_WRITE_ONLY, GL_RGBA32F); CHECK_GL_ERROR;

        glDispatchCompute((renderResultWidth + 15) / 16, (renderResultHeight + 15) / 16, 1); CHECK_GL_ERROR;
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

        // Run RCAS sharpening afterwards (only really useful when using anti-aliasing)
        /*fsrPassShader->bindProgram("RCAS_PASS"); CHECK_GL_ERROR;
        fsrPassShader->setUniformValue("sharpness", 2.0); CHECK_GL_ERROR;
        tex2DSceneColor->bindToTextureUnit(texUnitSceneColor); CHECK_GL_ERROR;
        fsrPassShader->setUniformValue("inputSampler",  GLint(texUnitFSRResult)); CHECK_GL_ERROR;
        tex2DFSRResult->bindToTextureUnit(texUnitFSRResult);
        glBindImageTexture(0, tex2DFSRResult->getTextureObject(), 0, GL_TRUE, 0, GL_READ_WRITE, GL_RGBA32F); CHECK_GL_ERROR; // Add RCAS buffer here

        glDispatchCompute((viewPortWidth + 15) / 16, (viewPortHeight + 15) / 16, 1); CHECK_GL_ERROR;
        glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);*/

        glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;

        // Draw sceneColor result onto screen quad
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
        // Disable antialiasing
        glDisable(GL_LINE_SMOOTH); CHECK_GL_ERROR;
        glDisable(GL_POLYGON_SMOOTH); CHECK_GL_ERROR;
        // Disable multisampling
        glDisable(GL_MULTISAMPLE); CHECK_GL_ERROR;
        glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE); CHECK_GL_ERROR;
        // Disable depth test, otherwise parts of the frame wont be rendered with transparency.
        glDepthFunc(GL_ALWAYS); CHECK_GL_ERROR;
        // Render sceneColor
        renderToViewportShader->bind(); CHECK_GL_ERROR;
        tex2DFSRResult->bindToTextureUnit(texUnitFSRResult); CHECK_GL_ERROR;
        renderToViewportShader->setUniformValue("sceneColor", GLint(texUnitFSRResult)); CHECK_GL_ERROR;
        tex2DDepthBuffer->bindToTextureUnit(texUnitDepthBuffer); CHECK_GL_ERROR;
        renderToViewportShader->setUniformValue("depthBuffer", texUnitDepthBuffer); CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
        vboViewportQuad->attachToVertexAttribute(0, 2, false, 2 * sizeof(float), 0); CHECK_GL_ERROR;
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4); CHECK_GL_ERROR;

        glDepthFunc(GL_LESS); CHECK_GL_ERROR;
    }
    // ... Or only render the scene to a quad on the screen.
    else
    {
        // Draw sceneColor result onto screen quad
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
        // Disable antialiasing
        glDisable(GL_LINE_SMOOTH); CHECK_GL_ERROR;
        glDisable(GL_POLYGON_SMOOTH); CHECK_GL_ERROR;
        // Disable multisampling
        glDisable(GL_MULTISAMPLE); CHECK_GL_ERROR;
        glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE); CHECK_GL_ERROR;
        // Disable depth test, otherwise parts of the frame wont be rendered with transparency.
        glDepthFunc(GL_ALWAYS); CHECK_GL_ERROR;
        // Render sceneColor
        renderToViewportShader->bind(); CHECK_GL_ERROR;
        tex2DSceneColor->bindToTextureUnit(texUnitSceneColor); CHECK_GL_ERROR;
        renderToViewportShader->setUniformValue("sceneColor", GLint(texUnitSceneColor)); CHECK_GL_ERROR;
        tex2DDepthBuffer->bindToTextureUnit(texUnitDepthBuffer); CHECK_GL_ERROR;
        renderToViewportShader->setUniformValue("depthBuffer", texUnitDepthBuffer); CHECK_GL_ERROR;
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
        vboViewportQuad->attachToVertexAttribute(0, 2, false, 2 * sizeof(float), 0); CHECK_GL_ERROR;
        glDrawArrays(GL_TRIANGLE_FAN, 0, 4); CHECK_GL_ERROR;

        glDepthFunc(GL_LESS); CHECK_GL_ERROR;
    }

    // 03 Render 2D UI elements, such as Transfer functions
    glEnable(GL_BLEND);
    // Set render step to scene.
    currentRenderStep = RENDER_UI;

    if (scene)
    {
        glDepthFunc(GL_ALWAYS);
        if (sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR)
        {
            // If full-screen mode is active, only the selected full-screen
            // actor should be drawn.
            if (fullScreenActor != nullptr)
            {
                // Render UI elements of fullscreen actor
                fullScreenActor->renderToUiLayer(this);
            }
        }
        else
        {
            for (MActor* actor : scene->getRenderQueue())
            {
                actor->renderToUiLayer(this);
            }
        }
        glDepthFunc(GL_LESS);
    }

    // 04 Draw labels
    if (!renderLabelsWithDepthTestProp)
    {
        glDisable(GL_DEPTH_TEST);
        glDepthFunc(GL_ALWAYS);
        MGLResourcesManager::getInstance()->getTextManager()
                ->renderLabelList(this, labelList);
        glDepthFunc(GL_LESS);
        glEnable(GL_DEPTH_TEST);
    }
    else
    {
        glDepthFunc(GL_LEQUAL);
        MGLResourcesManager::getInstance()->getTextManager()
                ->renderLabelList(this, labelList);
        glDepthFunc(GL_LESS);
    }

    // 05 Draw focus rectangle.
    if (hasFocus())
    {
        glDepthFunc(GL_ALWAYS);
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE); CHECK_GL_ERROR;
        focusShader->bind(); CHECK_GL_ERROR;
        focusShader->enableAttributeArray(FOCUSSHADER_VERTEX_ATTRIBUTE); CHECK_GL_ERROR;
        focusShader->setUniformValue("colourValue", QColor(Qt::red)); CHECK_GL_ERROR;
        vboViewportQuad->bindToArrayBuffer();
        vboViewportQuad->attachToVertexAttribute(FOCUSSHADER_VERTEX_ATTRIBUTE, 2, false, 2 * sizeof(float), 0);
        setLineWidth(2); CHECK_GL_ERROR;
        glDrawArrays(GL_LINE_LOOP, 0, 4); CHECK_GL_ERROR;
        glDepthFunc(GL_LESS);
    }

    // 06 Render scene onto final quad again, so that custom resolution fits the viewport.
    switchFramebuffer(0);
    glViewport(0, 0, viewPortWidth, viewPortHeight);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); CHECK_GL_ERROR;
    glDisable(GL_CULL_FACE); CHECK_GL_ERROR;

    // Draw finalColor result onto screen quad
    glDepthFunc(GL_ALWAYS);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL); CHECK_GL_ERROR;
    renderToViewportShader->bind(); CHECK_GL_ERROR;
    tex2DFinalColor->bindToTextureUnit(texUnitFinalColor); CHECK_GL_ERROR;
    renderToViewportShader->setUniformValue("sceneColor", GLint(texUnitFinalColor)); CHECK_GL_ERROR;
    tex2DFinalDepthBuffer->bindToTextureUnit(texUnitFinalDepthBuffer); CHECK_GL_ERROR;
    renderToViewportShader->setUniformValue("depthBuffer", texUnitFinalDepthBuffer); CHECK_GL_ERROR;
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
    vboViewportQuad->attachToVertexAttribute(0, 2, false, 2 * sizeof(float), 0); CHECK_GL_ERROR;
    glDrawArrays(GL_TRIANGLE_FAN, 0, 4); CHECK_GL_ERROR;

    // All actors have been rendered; they won't query this variable until the
    // next render cylce.
    visualizationParameterChange = false;

    // Set render step to scene.
    currentRenderStep = INACTIVE;

    // This update is triggered by a click on "measureFPSProperty". The scene
    // is updated for 30 seconds for fps measurements.
    if (measureFPS)
    {
        measureFPSFrameCount++;
#ifndef CONTINUOUS_GL_UPDATE
        update();
#endif
    }

#ifdef CONTINUOUS_GL_UPDATE
    // For framerate measurements, graphics rendering is performed in a
    // continous loop. Similar to glutPostRedisplay(), update() sets a flag
    // that tells Qt's main loop to redraw this widget in the next loop cycle.
    // See http://lists.trolltech.com/qt-interest/2005-03/thread00136-0.html
    update();
#endif

#ifdef LOG_EVENT_TIMES
    LOG4CPLUS_DEBUG(mlog, "scene rendering completed at "
                    << MSystemManagerAndControl::getInstance()
                    ->elapsedTimeSinceSystemStart(MStopwatch::MILLISECONDS)
                    << " ms");
#endif

    // Save the time animation screenshot if one was requested before rendering.
    if (!nextFrameScreenshotFilename.isEmpty())
    {
        saveToImageFileName(nextFrameScreenshotFilename);
        nextFrameScreenshotFilename = "";
    }
}


void MSceneViewGLWidget::resizeGL(int width, int height)
{
    if (isLoadingFromConfig) return;
    viewPortWidth = width;
    viewPortHeight = height;

    if (useCustomResolutionProp)
    {
        renderResultWidth = customResolutionWidthProp;
        renderResultHeight = customResolutionHeightProp;
    }
    else
    {
        renderResultWidth = viewPortWidth;
        renderResultHeight = viewPortHeight;
    }

    fsrViewPortHeight = renderResultHeight / getViewportUpscaleFactor();
    fsrViewPortWidth = renderResultWidth / getViewportUpscaleFactor();

    if(renderResultWidth % 2 == 1)
    {
        fsrViewPortWidth++;
    }
    if(renderResultHeight % 2 == 1)
    {
        fsrViewPortHeight++;
    }

    glViewport(0, 0, fsrViewPortWidth, fsrViewPortHeight);

    // viewport was resized, set timer and variable
    resizeTimer.restart();
    viewportResized = true;
    QVector3D co = camera.getOrigin();
    modelViewProjectionMatrix.setToIdentity();
    const double ratio = static_cast<double>(renderResultWidth) / renderResultHeight;

    if (sceneNavigationMode == TOPVIEW_2D)
    {
        const float zBack = co.z();
        float dyHalf = std::tan(M_PI / 8.0) * zBack;
        float dxHalf = ratio * dyHalf;

        float minX = - dxHalf;
        float maxX = + dxHalf;
        float minY = - dyHalf;
        float maxY = + dyHalf;

        modelViewProjectionMatrix.ortho(minX, maxX, minY, maxY, 0., 500.);
    }
    else
    {
        modelViewProjectionMatrix.perspective(
                45.,
                ratio,
                co.z()/10.,
                500.);
    }

    projectionMatrix = modelViewProjectionMatrix;
    viewMatrix = camera.getViewMatrix();
    modelViewProjectionMatrix *= viewMatrix;
    modelViewProjectionMatrixInverted = modelViewProjectionMatrix.inverted();

    // If we are not yet initialized, the buffers don't exist.
    if (!viewIsInitialised) return;

    // Resize Scene color render target
    if (tex2DSceneColor)
    {
        tex2DSceneColor->updateSize(fsrViewPortWidth, fsrViewPortHeight); CHECK_GL_ERROR;
        tex2DSceneColor->bindToTextureUnit(texUnitSceneColor); CHECK_GL_ERROR;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, fsrViewPortWidth, fsrViewPortHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr); CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
    }
    // Resize depth buffer render target
    if (tex2DDepthBuffer)
    {
        tex2DDepthBuffer->updateSize(fsrViewPortWidth, fsrViewPortHeight); CHECK_GL_ERROR;
        tex2DDepthBuffer->bindToTextureUnit(texUnitDepthBuffer); CHECK_GL_ERROR;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, fsrViewPortWidth, fsrViewPortHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr); CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
    }

    glBindRenderbuffer(GL_RENDERBUFFER, sceneColorRbo); CHECK_GL_ERROR;
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, multiSamplingSamplesProp, GL_RGB8, fsrViewPortWidth, fsrViewPortHeight); CHECK_GL_ERROR;
    glBindRenderbuffer(GL_RENDERBUFFER, 0); CHECK_GL_ERROR;

    glBindRenderbuffer(GL_RENDERBUFFER, sceneDepthRbo); CHECK_GL_ERROR;
    glRenderbufferStorageMultisample(GL_RENDERBUFFER, multiSamplingSamplesProp, GL_DEPTH_COMPONENT32, fsrViewPortWidth, fsrViewPortHeight); CHECK_GL_ERROR;
    glBindRenderbuffer(GL_RENDERBUFFER, 0); CHECK_GL_ERROR;

    // Resize final color render target
    if (tex2DFinalColor)
    {
        tex2DFinalColor->updateSize(renderResultWidth, renderResultHeight); CHECK_GL_ERROR;
        tex2DFinalColor->bindToTextureUnit(texUnitFinalColor); CHECK_GL_ERROR;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, renderResultWidth, renderResultHeight, 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
    }
    // Resize final depth buffer render target
    if (tex2DFinalDepthBuffer)
    {
        tex2DFinalDepthBuffer->updateSize(renderResultWidth, renderResultHeight); CHECK_GL_ERROR;
        tex2DFinalDepthBuffer->bindToTextureUnit(texUnitFinalDepthBuffer); CHECK_GL_ERROR;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, renderResultWidth, renderResultHeight, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr); CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
    }

    // Resize FSR render result
    if (tex2DFSRResult)
    {
        tex2DFSRResult->updateSize(renderResultWidth, renderResultHeight); CHECK_GL_ERROR;
        tex2DFSRResult->bindToTextureUnit(texUnitFSRResult); CHECK_GL_ERROR;
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, renderResultWidth, renderResultHeight, 0, GL_RGBA, GL_FLOAT, nullptr); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T,GL_CLAMP_TO_EDGE); CHECK_GL_ERROR;
        glBindTexture(GL_TEXTURE_2D, 0); CHECK_GL_ERROR;
    }
}


bool MSceneViewGLWidget::isViewPortResized()
{
    float elapsedTime = resizeTimer.elapsed() / 1000.0f;

    if(elapsedTime > 0.1f)
    {
        viewportResized = false;
    }

    return viewportResized;
}


void MSceneViewGLWidget::mouseDoubleClickEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    // Toggle interaction mode.
    setInteractionMode(!actorInteractionModeProp);

    if (posLabelIsEnabledProp && actorInteractionModeProp
            && pickedActor.actor != nullptr
            && (event->buttons() & Qt::LeftButton))
    {
        float clipX = -1. + 2.*(float(event->x()) / float(viewPortWidth));
        float clipY =  1. - 2.*(float(event->y()) / float(viewPortHeight));
        pickedActor.actor->addPositionLabel(this, pickedActor.handleID,
                                            clipX, clipY);
    }
}


void MSceneViewGLWidget::mousePressEvent(QMouseEvent *event)
{
    lastPos = event->pos();
    float clipX = -1. + 2.*(float(event->x()) / float(viewPortWidth));
    float clipY =  1. - 2.*(float(event->y()) / float(viewPortHeight));
    lastPoint = QVector3D(clipX, clipY, 0);
    float length = sqrt(lastPoint.x() * lastPoint.x()
                        + lastPoint.y() * lastPoint.y());
    length = (length < 1.0) ? length : 1.0;
    lastPoint.setZ(cos((M_PI/2.0) * length));
    userIsInteracting = true;

    if (posLabelIsEnabledProp && actorInteractionModeProp
            && pickedActor.actor != nullptr
            && (event->buttons() & Qt::LeftButton))
    {
        pickedActor.actor->addPositionLabel(this, pickedActor.handleID,
                                            clipX, clipY);
    }
}

void MSceneViewGLWidget::refreshCameraSequenceProperty()
{
    MSystemManagerAndControl *sysMC =
            MSystemManagerAndControl::getInstance();

    auto sequences = sysMC->getCameraSequences();

    int index = cameraSequenceAnimationProp;

    QStringList names;
    names.append("None");

    for (int i = 0; i < sequences.length(); i++)
    {
        QString label  = sequences.at(i)->sequenceName;
        names.append(label);
    }

    cameraSequenceAnimationProp.setEnumNames(names);

    if (sequences.length() <= index)
        index = -1;

    cameraSequenceAnimationProp.setValue(index);
}


void MSceneViewGLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (freezeMode) return;

    // A) INTERACTION MODE.
    // ========================================================================

    // NOTE: If the interaction mode is enabled, Qt mouse tracking is enabled
    // (in propertyChange()). Hence, mouseMoveEvent() is called always when the
    // mouse is moved on the OpenGL canvas. If mouse tracking is disabled,
    // mouseMoveEvent() is only called when a button has been pressed.

    if (actorInteractionModeProp)
    {
        // No scene registered? Return.
        if (!scene) return;

        // Transform the mouse cursor position to 2D clip space.
        float clipX = -1. + 2.*(float(event->x()) / float(viewPortWidth));
        float clipY =  1. - 2.*(float(event->y()) / float(viewPortHeight));

        // The left mouse button is pressed: This is a drag event.
        if (event->buttons() & Qt::LeftButton)
        {
            // No actor has been picked to be dragged: Return.
            if (!pickedActor.actor) return;

            pickedActor.actor->dragEventWithAllComponents(
                        this, pickedActor.handleID, clipX, clipY);
        }
        // No mouse button is pressed. Track the mouse to find pickable
        // elements.
        else
        {
            // Reset the currently picked actor.
            pickedActor.actor = 0;
            pickedActor.handleID = -1;

            // If in full-screen mode, only interact with selected full-screen
            // actor.
            if (sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR)
            {
                // Only check actors that are pickable.
                if (fullScreenActor != nullptr && fullScreenActor->isPickable())
                {
                    pickedActor.handleID =
                            fullScreenActor->checkIntersectionWithHandleAndAllComponents(
                                    this, clipX, clipY);
                    // If the test returned a valid handle ID, store the actor
                    // and its handle as the currently picked actor.
                    if (pickedActor.handleID >= 0)
                    {
                        pickedActor.actor = fullScreenActor;
                    }
                }
            }
            else
            {

                // Loop over all actors in the scene and let them check whether
                // the mouse cursor coincides with one of their handles.
                for (MActor* actor : scene->getRenderQueue())
                {
                    // Only check actors that are pickable.
                    if (actor->isPickable() && actor->isEnabled())
                    {
                        if (singleInteractionActor == nullptr ||
                                singleInteractionActor->getName()
                                == actor->getName())
                        {
                            pickedActor.handleID = actor->checkIntersectionWithHandleAndAllComponents(
                                    this, clipX, clipY);
                            // If the test returned a valid handle ID, store the actor
                            // and its handle as the currently picked actor.
                            if (pickedActor.handleID >= 0)
                            {
                                pickedActor.actor = actor;
                                break;
                            }
                        }
                    }
                }
            }

            // Redraw (the actors might draw any highlighted handles).
#ifndef CONTINUOUS_GL_UPDATE
            updateGL();
#endif
        }
        emit sceneInteractedWith(this);
        return;
    }


    // B) ANALYSIS MODE.
    // ========================================================================
    if (analysisModeProp)
    {
        return;
    }


    // C) FULL-SCREEN MODE.
    // ========================================================================
    if (sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR)
    {
        // If in full-screen mode, don't move camera.
        return;
    }


    // D) CAMERA MOVEMENTS.
    // ========================================================================

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    int dx = event->x() - lastPos.x();
    int dy = event->y() - lastPos.y();

    float sensitivity = sceneNavigationSensitivityProp;
    if (event->modifiers() == Qt::ShiftModifier) sensitivity *= 10.;

    if (event->buttons() & glRM->globalMouseButtonRotate)
    {
        if (sceneNavigationMode == MOVE_CAMERA)
        {
            // The left mouse button rotates the camera, the camera position
            // remains unchanged.
            // dx maps to a rotation around the world space z-axis
            camera.rotateWorldSpace(-dx/10., 0, 0, 1);
            // dy maps to a rotation around the camera x-axis
            camera.rotate(-dy/10., 1, 0, 0);
        }
        else if (sceneNavigationMode == ROTATE_SCENE)
        {
            sceneRotationMatrix.setToIdentity();
            // Rotation is implemented with a rotation matrix and is combined
            // with a translation in "paintGL()". The calculation uses an
            // arcball rotation; see:
            // https://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Arcball
            float clipX = -1. + 2.*(float(event->x()) / float(viewPortWidth));
            float clipY =  1. - 2.*(float(event->y()) / float(viewPortHeight));
            QVector3D curPoint = QVector3D(clipX, clipY,0);
            float length = sqrt(curPoint.x() * curPoint.x() +
                                curPoint.y() * curPoint.y());
            length = (length < 1.0) ? length : 1.0;
            curPoint.setZ(cos((M_PI/2.0) * length));
            QVector3D difPosition = (lastPoint - curPoint);
            float angle = difPosition.length() * 45.0;

            // The rotation vector is also rotated by the worldMatrix
            // to perform the rotation regarding the actual world rotation.
            QVector3D rotAxis = QVector3D::crossProduct(lastPoint, curPoint);
            sceneRotationMatrix.rotate(angle,rotAxis);
            lastPoint = curPoint;

            if (cameraAutorotationModeProp)
            {
                cameraAutoRotationAxis = rotAxis;
                cameraAutoRotationAngle = angle / 10.;
            }
        }
    }

    else if (event->buttons() & glRM->globalMouseButtonPan)
    {
        // The right mouse button moves the camera around in the scene.

        if (glRM->isReverseCameraPan)
        {
            // Move camera with respect to the mouse movement.
            camera.moveUp(dy/10./sensitivity, 1.);
            camera.moveRight(-dx/10./sensitivity);
        }
        else
        {
            // Move scene with respect to the mouse movement.
            camera.moveUp(-dy/10./sensitivity, 1.);
            camera.moveRight(dx/10./sensitivity);
        }
    }

    else if (event->buttons() & glRM->globalMouseButtonZoom)
    {
        // "Pure" mouse wheel: zoom (move camera forward/backward)
        float zoomFactor = -1./sensitivity;

        if (sceneNavigationMode == ROTATE_SCENE) zoomFactor = -zoomFactor;
        if (glRM->isReverseCameraZoom) zoomFactor = -zoomFactor;

        camera.moveForward(dy * zoomFactor);
    }

    lastPos = event->pos();
    emit sceneInteractedWith(this);
    updateCameraPositionDisplay();
    updateSynchronizedCameras();

#ifndef CONTINUOUS_GL_UPDATE
    updateGL();
#endif
}


void MSceneViewGLWidget::mouseReleaseEvent(QMouseEvent *event)
{
    if (freezeMode) return;

    if (actorInteractionModeProp && pickedActor.actor != nullptr)
    {
        pickedActor.actor->removePositionLabel();
        pickedActor.actor->releaseEventWithAllComponents(this, pickedActor.handleID);
    }

    userIsInteracting = false;
    emit clicked();

    // ANALYSIS MODE.
    // ==============

    if (analysisModeProp)
    {
        // No scene registered? Return.
        if (!scene) return;

        // Transform the mouse cursor position to 2D clip space.
        float clipX = -1. + 2.*(float(event->x()) / float(viewPortWidth));
        float clipY =  1. - 2.*(float(event->y()) / float(viewPortHeight));

        // The left mouse button is released: Trigger analysis.
        if (event->buttons() ^ Qt::LeftButton)
        {
            // Loop over all actors in the scene and let them check whether
            // the mouse cursor coincides with one of their objects.
            const QVector<MActor*>& actors = scene->getRenderQueue();
            for (int i = 0; i < actors.size(); i++)
                // Only check actors that are pickable.
                if (actors[i]->isPickable())
                    if (actors[i]->triggerAnalysisOfObjectAtPos(
                                this, clipX, clipY, 10. / float(viewPortWidth)))
                        break;
        }
    }

    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // AUTO-ROTATION MODE.
    // ===================

    // If we are in auto-rotation mode, start the auto-rotation timer to
    // rotate the scene around its rotation centre.
    if (event->button() == glRM->globalMouseButtonRotate &&
        cameraAutorotationModeProp && sceneNavigationMode == ROTATE_SCENE)
    {
      cameraAutoRotationTimer->start();
      userIsInteracting = true;
    }

#ifndef CONTINUOUS_GL_UPDATE
    updateGL();
#endif
}


void MSceneViewGLWidget::wheelEvent(QWheelEvent *event)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    if (actorInteractionModeProp || analysisModeProp
            || sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR)
    {
        return;
    }
    if (freezeMode)
    {
        return;
    }
    if (glRM->globalMouseButtonZoom != Qt::MiddleButton)
    {
        return;
    }
    if (event->modifiers() == Qt::ControlModifier)
    {
        // Ctrl + mouse wheel: -- none --
    }
    else if (event->modifiers() == Qt::AltModifier)
    {
//        // Alt + mouse wheel: time navigation for scene.
//        if (event->delta() > 0) {
//            scene->timeForward();
//        } else {
//            scene->timeBackward();
//        }
    }
    else
    {
        // starts scroll timer and sets scrolling to true
        userIsScrolling = true;
        scrollTimer.restart();

        // "Pure" mouse wheel: zoom (move camera forward/backward)
        float zoomFactor = 10.f / sceneNavigationSensitivityProp;
        if (event->modifiers() == Qt::ShiftModifier) zoomFactor /= 10.;

        if (sceneNavigationMode == ROTATE_SCENE) zoomFactor = -zoomFactor;
        if (glRM->isReverseCameraZoom) zoomFactor = -zoomFactor;

        if (event->angleDelta().y() > 0)
        {
            camera.moveForward(0.5 * zoomFactor);
        }
        else
        {
            camera.moveForward(-0.5 * zoomFactor);
        }
        updateCameraPositionDisplay();
        updateSynchronizedCameras();
    }
#ifndef CONTINUOUS_GL_UPDATE
    updateGL();
#endif
}


void MSceneViewGLWidget::checkUserScrolling()
{
    float elapsedTime = scrollTimer.elapsed() / 1000.0f;

    bool oldUserScrolling = userIsScrolling;

    if (elapsedTime > 0.5f) { userIsScrolling = false; }

    if (oldUserScrolling != userIsScrolling) { updateGL(); }
}


void MSceneViewGLWidget::autoRotateCamera()
{
    sceneRotationMatrix.rotate(cameraAutoRotationAngle, cameraAutoRotationAxis);
#ifndef CONTINUOUS_GL_UPDATE
    updateGL();
#endif
}

void MSceneViewGLWidget::onSequenceRegistered()
{
    refreshCameraSequenceProperty();
}

void MSceneViewGLWidget::onSequenceRemoved(int index)
{
    if (index < cameraSequenceAnimationProp - 1)
    {
        cameraSequenceAnimationProp = cameraSequenceAnimationProp - 1;
    }
    refreshCameraSequenceProperty();
}

void MSceneViewGLWidget::onSequenceRenamed(int index, QString name)
{
    refreshCameraSequenceProperty();
}


void MSceneViewGLWidget::focusInEvent(QFocusEvent *event)
{
    MSystemManagerAndControl::getInstance()->setLastFocus(this);
    QGLWidget::focusInEvent(event);
}


bool MSceneViewGLWidget::userIsScrollingWithMouse()
{
    return userIsScrolling;
}


void MSceneViewGLWidget::keyPressEvent(QKeyEvent *event)
{
    // Special case: The user interactively selects a new scene rotation
    // centre (see "selectSceneRotationCentreProperty"). The selection is
    // finished upon hitting the "ENTER" key.
    if (singleInteractionActor != nullptr)
    {
        if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
        {
            MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
            MMovablePoleActor *pickActor =
                    glRM->getSceneRotationCentreSelectionPoleActor();
            MLabel *pickText = glRM->getSceneRotationCentreSelectionLabel();
            QVector3D rotationCenter = pickActor->getPoleVertices().at(0);

            sceneRotationCenterProp.setX(rotationCenter.x());
            sceneRotationCenterProp.setY(rotationCenter.y());

            setSceneRotationCentre(sceneRotationCenterProp);

            scene->setSingleInteractionActor(nullptr);
            scene->removeActorByName(pickActor->getName());
            for (MSceneViewGLWidget *sceneView : scene->getRegisteredSceneViews())
            {
                sceneView->staticLabels.removeOne(pickText);
                sceneView->updateSceneLabel();
            }

            sceneNavigationMode = ROTATE_SCENE;
            sceneNavigationModeProp.setValue(sceneNavigationMode);
            selectSceneRotationCentreProp.setEnabled(true);
            sceneNavigationModeProp.setEnabled(true);
        }
    }

    if (freezeMode) return;

    switch (event->key())
    {
    case Qt::Key_L:
        // Shader reload.
        MGLResourcesManager::getInstance()->reloadActorShaders();
#ifndef CONTINUOUS_GL_UPDATE
        updateGL();
#endif
        break;
    case Qt::Key_I:
        // Toggle interaction mode.
        setInteractionMode(!actorInteractionModeProp);
        break;
    case Qt::Key_A:
        // Toggle analysis mode.
        setAnalysisMode(!analysisModeProp);
        break;
    case Qt::Key_R:
        if (event->modifiers() & Qt::AltModifier)
        {
            resizeView();
            break;
        }
        // Toggle auto-rotation mode.
        setAutoRotationMode(!cameraAutorotationModeProp);
        break;
    case Qt::Key_N:
        if (event->modifiers() & Qt::AltModifier)
        {
            executeCameraAction(CAMERA_NORTHUP);
        }
        break;
    case Qt::Key_U:
        if (event->modifiers() & Qt::AltModifier)
        {
            executeCameraAction(CAMERA_UPRIGHT);
        }
        break;
    case Qt::Key_T:
        if (event->modifiers() & Qt::AltModifier)
        {
            executeCameraAction(CAMERA_TOPVIEW);
        }
        break;
    case Qt::Key_S:
        if (QApplication::keyboardModifiers() == Qt::NoModifier)
        {
            saveToImageFile();
        }
        break;
    case Qt::Key_Up:
    {
        if (analysisModeProp || sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR || freezeMode || actorInteractionModeProp) break;

        float sensitivity = sceneNavigationSensitivityProp;
        if (event->modifiers() & Qt::ShiftModifier) sensitivity *= 10.;

        if (event->modifiers() & Qt::ControlModifier) // Rotate up
        {
            camera.rotate(1.f / sensitivity, 1, 0, 0);
        }
        else if (event->modifiers() & Qt::AltModifier) // Move up
        {
            camera.moveUp(10.f / sensitivity, 1.);
        }
        else // Zoom in
        {
            camera.moveForward(1.f / sensitivity);
        }

        updateCameraPositionDisplay();
        updateSynchronizedCameras();

#ifndef CONTINUOUS_GL_UPDATE
        updateGL();
#endif
        break;
    }
    case Qt::Key_Down:
    {
        if (analysisModeProp || sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR || freezeMode || actorInteractionModeProp) break;

        float sensitivity = sceneNavigationSensitivityProp;
        if (event->modifiers() & Qt::ShiftModifier) sensitivity *= 10.;

        if (event->modifiers() & Qt::ControlModifier) // Rotate down
        {
            camera.rotate(-1.f / sensitivity, 1, 0, 0);
        }
        else if (event->modifiers() & Qt::AltModifier) // Move down
        {
            camera.moveUp(-10.f / sensitivity, 1.);
        }
        else // Zoom out
        {
            camera.moveForward(-1.f / sensitivity);
        }

        updateCameraPositionDisplay();
        updateSynchronizedCameras();

#ifndef CONTINUOUS_GL_UPDATE
        updateGL();
#endif
        break;
    }
    case Qt::Key_Right:
    {
        if (analysisModeProp || sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR || freezeMode || actorInteractionModeProp) break;

        float sensitivity = sceneNavigationSensitivityProp;
        if (event->modifiers() & Qt::ShiftModifier) sensitivity *= 10.;

        if (event->modifiers() & Qt::ControlModifier) // Rotate right
        {
            camera.rotateWorldSpace(-1.f / sensitivity, 0, 0, 1);
        }
        else // Move right
        {
            camera.moveRight(-10.f / sensitivity);
        }

        updateCameraPositionDisplay();
        updateSynchronizedCameras();

#ifndef CONTINUOUS_GL_UPDATE
        updateGL();
#endif
        break;
    }
    case Qt::Key_Left:
    {
        if (analysisModeProp || sceneNavigationMode == SINGLE_FULLSCREEN_ACTOR || freezeMode || actorInteractionModeProp) break;

        float sensitivity = sceneNavigationSensitivityProp;
        if (event->modifiers() & Qt::ShiftModifier) sensitivity *= 10.;

        if (event->modifiers() & Qt::ControlModifier) // Rotate left
        {
            camera.rotateWorldSpace(1.f / sensitivity, 0, 0, 1);
        }
        else // Move left
        {
            camera.moveRight(10.f / sensitivity);
        }

        updateCameraPositionDisplay();
        updateSynchronizedCameras();

#ifndef CONTINUOUS_GL_UPDATE
        updateGL();
#endif
        break;
    }
    default:
        // If we do not act upon the key, pass event to base class
        // implementation.
        QGLWidget::keyPressEvent(event);
    }
}


void MSceneViewGLWidget::updateSynchronizedCameras()
{
    // Synchronize cameras of connected scene views.
    QSetIterator<MSceneViewGLWidget*> i(syncCameras);
    while (i.hasNext())
    {
        MSceneViewGLWidget* otherView = i.next();
        MCamera *otherCamera = otherView->getCamera();
        otherCamera->setOrigin(camera.getOrigin());
        otherCamera->setYAxis(camera.getYAxis());
        otherCamera->setZAxis(camera.getZAxis());
        otherView->updateCameraPositionDisplay();
        otherView->updateGL();
    }
}

void MSceneViewGLWidget::switchFramebuffer(GLuint fbo)
{
    this->makeCurrent();
    this->currentFbo = fbo;
    glBindFramebuffer(GL_FRAMEBUFFER, fbo); CHECK_GL_ERROR;
}


void MSceneViewGLWidget::reloadShaders()
{
    // If any of the shaders is not initialized, none are.
    if (!northArrowShader) return;

    northArrowShader->compileFromFile_Met3DHome(
            "src/glsl/north_arrow.fx.glsl");
    renderToViewportShader->compileFromFile_Met3DHome("src/glsl/scene_final_colour.fx.glsl");
    fsrPassShader->compileFromFile_Met3DHome("src/glsl/fsr_pass.fx.glsl");
}


void MSceneViewGLWidget::updateSceneLightsBuffer()
{
    if (sceneLightsBuffer == nullptr) return;

    // Render shadow map.
    std::vector<MLight> packedLights;
    for (MLightActor *light : getScene()->getSceneLights())
    {
        if (!light->isEnabled()) continue;
        packedLights.push_back(light->packLight());
    }
    // If we have no lights, push an empty one back.
    if (packedLights.empty())
    {
        packedLights.emplace_back();
    }

    // Sort lights by their type.
    std::sort(packedLights.begin(), packedLights.end(),
              [](MLight a, MLight b)
              {
                  return a.type < b.type;
              });

    // Update light buffer and bind it to buffer binding point 3.
    sceneLightsBuffer->updateSize(packedLights.size());
    sceneLightsBuffer->upload(packedLights.data(), GL_DYNAMIC_DRAW);
}


void MSceneViewGLWidget::updateLightingUniformBuffer()
{
    LightingUniforms lightingUniforms{};
    lightingUniforms.ambientColor[0] = static_cast<float>(ambientLightColorProp.value().redF());
    lightingUniforms.ambientColor[1] = static_cast<float>(ambientLightColorProp.value().greenF());
    lightingUniforms.ambientColor[2] = static_cast<float>(ambientLightColorProp.value().blueF());

    QVector3D origin = camera.getOrigin();

    lightingUniforms.cameraPositionWS[0] = origin.x();
    lightingUniforms.cameraPositionWS[1] = origin.y();
    lightingUniforms.cameraPositionWS[2] = origin.z();

    for (int i = 0; i < 3; i++)
    {
        QMatrix4x4 tmp = lightViewProjMatrix[i].transposed();
        tmp.copyDataTo(&lightingUniforms.shadowLightMatrix[i * 16]);
    }

    QMatrix4x4 tmp = camera.getViewMatrix().transposed();
    tmp.copyDataTo(lightingUniforms.camViewMatix);

    QVector3D dir = getLightDirection();
    lightingUniforms.shadowLightDirection[0] = dir.x();
    lightingUniforms.shadowLightDirection[1] = dir.y();
    lightingUniforms.shadowLightDirection[2] = dir.z();

    lightingUniforms.shadowMapTextMapScale[0] = 1.0f / static_cast<float>(tex2DShadowMap->getWidth());
    lightingUniforms.shadowMapTextMapScale[1] = 1.0f / static_cast<float>(tex2DShadowMap->getHeight());

    lightingUniforms.shadowMapAvailable = static_cast<GLuint>(sceneDirectionalLight != nullptr && shadowMapResProp != Disabled);

    for (int i = 0; i < 3; i++)
    {
        lightingUniforms.splitDistances[i] = splits[i + 1];
    }

    lightingUniforms.ambientLight = ambientLightProp;
    lightingUniforms.diffuseLight = diffuseLightProp;
    lightingUniforms.specularStrength = specularStrengthProp;

    int activeLights = 0;
    for (MLightActor *light : scene->getSceneLights())
    {
        if (light->isEnabled()) activeLights++;
    }

    // Lighting disabled
    if (activeLights == 0)
    {
        lightingUniforms.ambientColor[0] = 1.0;
        lightingUniforms.ambientColor[1] = 1.0;
        lightingUniforms.ambientColor[2] = 1.0;

        lightingUniforms.ambientLight = 1.0;
        lightingUniforms.diffuseLight = 0.0;
        lightingUniforms.specularStrength = 0.0;
    }

    lightingUniforms.receiveShadows = true;

    lightingUniforms.shadowMapSampleCount = getSampleCount();

    lightUniformBuffer->uploadData(&lightingUniforms, GL_DYNAMIC_DRAW);
    lightUniformBuffer->bind(4);
}


void MSceneViewGLWidget::setMultisampleGLState()
{
    if (multisamplingEnabledProp)
    {
        glEnable(GL_MULTISAMPLE);

        if (enableOITProp)
        {
            glEnable(GL_SAMPLE_ALPHA_TO_COVERAGE);
            glDisable(GL_BLEND);
        }
        else
        {
            glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
            glEnable(GL_BLEND);
        }
    }
    else
    {
        glDisable(GL_MULTISAMPLE);
        glDisable(GL_SAMPLE_ALPHA_TO_COVERAGE);
        glEnable(GL_BLEND);
    }
}


void MSceneViewGLWidget::updateSceneLabel()
{
    // Don't update scene label if no scene is attached to the scene view.
    if (scene == nullptr)
    {
        return;
    }

    // Remove the old scene description label from the list of static labels.
    for (int i = 0; i < staticLabels.size(); i++)
    {
        if (staticLabels[i] == sceneNameLabel)
        {
            staticLabels.removeAt(i);
            MGLResourcesManager::getInstance()->getTextManager()->removeText(
                        sceneNameLabel);
        }
    }

    bool labelEnabled = sceneViewLabelEnableProp;

    if (!labelEnabled)
    {
        return;
    }

    // Create a new scene description label (view number and scene name in
    // lower left corner of the view).
    QString label = QString("view %1 (%2)").arg(myID+1).arg(scene->getName());
    if (actorInteractionModeProp) label += " - actor interaction mode";
    if (analysisModeProp) label += " - analysis mode";
    if (cameraAutorotationModeProp) label += " - auto-rotate camera";

    sceneNameLabel = MGLResourcesManager::getInstance()->getTextManager()->addText(
                label, MTextManager::CLIPSPACE, -0.99, -0.99, -0.99, 20,
                QColor(0, 0, 255, 150));

    staticLabels.append(sceneNameLabel);
}


void MSceneViewGLWidget::saveAnimationImage(QString path, QString filename)
{
    filename = QDir(path).absoluteFilePath(filename);
    QFileInfo checkFile(filename);
    if (!overwriteImageSequence && checkFile.exists())
    {
// TODO (bt, 17Oct2016) Use operating system dependend filename splitting.
        QMessageBox::StandardButton reply = QMessageBox::question(
                    MGLResourcesManager::getInstance(),
                    "Save screenshot",
                    filename.split("/").last()
                    + " already exits.\n"
                    + "Do you want to replace it?",
                    QMessageBox::Yes|QMessageBox::YesAll|QMessageBox::No,
                    QMessageBox::No);
        // Don't save image if user rejects to overwrite the file it.
        if (reply == QMessageBox::No)
        {
            return;
        }
        if (reply == QMessageBox::YesAll)
        {
            overwriteImageSequence = true;
        }
    }
    // Instead of directly saving the screenshot here,
    // save the filename and wait for rendering to finish.
    nextFrameScreenshotFilename = filename;
    updateGL();
}


void MSceneViewGLWidget::updateSyncControlProperty()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    QString syncControlName = "None";
    int currentIndex = displayDateTimeSyncControlProp;

    if (currentIndex > 0)
    {
        syncControlName = displayDateTimeSyncControlProp.getSelectedEnumName();
    }

    QStringList syncControls = sysMC->getSyncControlIdentifiers();
    displayDateTimeSyncControlProp.setEnumNames(sysMC->getSyncControlIdentifiers());

    //currentIndex = max(0, sysMC->getSyncControlIdentifiers().indexOf(syncControlName));
    // TODO (tv, 24Oct2024): Replace this line with the line above when re-enabling the feature.
    currentIndex = std::max(1, sysMC->getSyncControlIdentifiers().indexOf(syncControlName));

    displayDateTimeSyncControlProp = currentIndex;
}


void MSceneViewGLWidget::onActorCreated(MActor *actor)
{
    // If the new actor supports full-screen visualisation, add it to the list
    // of available full-screen actors.
    if (actor->supportsFullScreenVisualisation())
    {
        // Don't render while the properties are being updated.
        setUpdatesEnabled(false);

        int currentIndex = fullScreenActorProp;

        QStringList availableFullScreenActors = fullScreenActorProp.getEnumNames();
        availableFullScreenActors << actor->getName();

        fullScreenActorProp.setEnumNames(availableFullScreenActors, currentIndex);

        setUpdatesEnabled(true);
    }
}


void MSceneViewGLWidget::onActorDeleted(MActor *actor)
{
    // If the deleted actor supports full-screen visualisation, remove it from
    // the list of available full-screen actors.
    if (actor->supportsFullScreenVisualisation())
    {
        setUpdatesEnabled(false);

        int currentIndex = fullScreenActorProp;
        QString fullScreenActorName = fullScreenActorProp.getSelectedEnumName();

        QStringList availableFullScreenActors = fullScreenActorProp.getEnumNames();
        availableFullScreenActors.removeOne(actor->getName());

        fullScreenActorProp.setEnumNames(availableFullScreenActors);

        currentIndex = std::max(0, availableFullScreenActors.indexOf(fullScreenActorName));

        setUpdatesEnabled(true);

        fullScreenActorProp.setValue(currentIndex);
    }
}


void MSceneViewGLWidget::onActorRenamed(MActor *actor, QString oldName)
{
    // If the renamed actor supports full-screen visualisation, change its name
    // in the list of available full-screen actors.
    if (actor->supportsFullScreenVisualisation())
    {
        // Don't render while the properties are being updated.
        setUpdatesEnabled(false);

        int currentIndex = fullScreenActorProp;

        QStringList availableFullScreenActors = fullScreenActorProp.getEnumNames();
        availableFullScreenActors[availableFullScreenActors.indexOf(oldName)]
                = actor->getName();

        fullScreenActorProp.setEnumNames(availableFullScreenActors, currentIndex);

        setUpdatesEnabled(true);
    }
}


void MSceneViewGLWidget::onFullScreenActorUpdate()
{
    updateGL();
}

void MSceneViewGLWidget::onLightChanged()
{
    needSceneLightUpdate = true;

    sceneDirectionalLight = nullptr;

    // Update light actor responsible for scene light.
    for (MLightActor *light : getScene()->getSceneLights())
    {
        if (!light->isEnabled()) continue;
        if (light->getLightType() == MLightActor::Directional && light->isEnabled())
        {
            sceneDirectionalLight = light;
            break;
        }
    }

    // Update light buffer.
    updateSceneLightsBuffer();
}


void MSceneViewGLWidget::updateCameraPositionDisplay()
{
    QVector3D co = camera.getOrigin();
    cameraPositionProp.setValue(
                QString("%1/%2/%3").arg(co.x(), 0, 'f', 1)
                .arg(co.y(), 0, 'f', 1).arg(co.z(), 0, 'f', 1));
}

void MSceneViewGLWidget::updateCamera()
{
    updateCameraPositionDisplay();

#ifndef CONTINUOUS_GL_UPDATE
    if (viewIsInitialised && (!freezeMode)) updateGL();
#endif
}


void MSceneViewGLWidget::setSingleInteractionActor(MActor *actor)
{
    singleInteractionActor = actor;
    if (singleInteractionActor != nullptr)
    {
        setInteractionMode(true);
    }
    else
    {
        setInteractionMode(false);
    }
}


void MSceneViewGLWidget::setSceneNavigationMode(SceneNavigationMode mode)
{
    sceneNavigationMode = mode;
    sceneNavigationModeProp.setValue((int) mode);
}


void MSceneViewGLWidget::setSceneRotationCentre(QVector3D centre)
{
    sceneRotationCenterProp = centre;
}


void MSceneViewGLWidget::dragEnterEvent(QDragEnterEvent *event)
{
    // Get drop type and filter for ones which we can handle
    const MMimeType dropType = MMimeData::parseMimeData(event->mimeData());

    QVector<MMimeType> acceptedTypes(
    {MMimeType::ACTOR, MMimeType::ACTOR_FILE,
     MMimeType::LOAD_ACTOR_BUTTON, MMimeType::DATASET_CONFIG_FILE});

    if (acceptedTypes.contains(dropType))
    {
        event->acceptProposedAction();
    }
}


void MSceneViewGLWidget::dropEvent(QDropEvent *event)
{
    auto thisSceneList = QVector<MSceneControl *>({scene});

    // Get the type of the drop event.
    QString data;
    MMimeType dropType = MMimeData::parseMimeData(event->mimeData(), data);

    if (dropType == MMimeType::ACTOR)
    {
        // Create actor in this scene.
        QVector<MSceneControl *> scenes({this->scene});

        QString actorName;
        auto *cmd = new MActor::CreateCommand(data, actorName, scenes);
        MUndoStack::run(cmd);
    }
    else if (dropType == MMimeType::LOAD_ACTOR_BUTTON)
    {
        MAbstractActorFactory::createAndRegisterActorFromFileDialog(
            thisSceneList);
    }
    else if (dropType == MMimeType::ACTOR_FILE)
    {
        // Create actor from file via file dropped here.
        const QString dropFilePath = event->mimeData()->urls()[0].
            toLocalFile();
        auto *cmd = new MActor::CreateCommand(dropFilePath, thisSceneList);
        MUndoStack::run(cmd);
    }
    else if (dropType == MMimeType::DATASET_CONFIG_FILE)
    {
        // Load the data set configuration file.
        const QString dropFilePath = event->mimeData()->urls()[0].
            toLocalFile();

        if (QGuiApplication::keyboardModifiers() & Qt::AltModifier)
        {
            // Quick drop without dataset dialog.
            MPipelineConfiguration pipelineConfig;
            pipelineConfig.loadDatasetFromFile(dropFilePath);
        }
        else
        {
            auto *settings = new QSettings(dropFilePath, QSettings::IniFormat);
            MAddDatasetDialog::createAndOpen(settings);
        }
    }
}


void MSceneViewGLWidget::saveConfigurationToFile(QString filename)
{
    if (filename.isEmpty())
    {
        QString directory =
                MSystemManagerAndControl::getInstance()
                ->getMet3DWorkingDirectory().absoluteFilePath("config/sceneviews");
        QDir().mkpath(directory);
        filename = MFileUtils::getSaveFileName(
                    nullptr,
                    "Save scene view configuration",
                    FileTypes::M_SCENE_VIEW_CONFIG,
                    directory,
                    QString("sceneview%1").arg(myID + 1));

        if (filename.isEmpty())
        {
            return;
        }
    }

    LOG4CPLUS_INFO(mlog, "Saving configuration to " << filename);

    QSettings *settings = new QSettings(filename, QSettings::IniFormat);

    // Overwrite if the file exists.
    if (QFile::exists(filename))
    {
        QStringList groups = settings->childGroups();
        // Only overwrite file if it contains already configuration for a
        // scene view.
        if ( !groups.contains("MSceneView") )
        {
            QMessageBox msg;
            msg.setWindowTitle("Error");
            msg.setText("The selected file contains a configuration other than "
                        "MSceneView.\n"
                        "This file will NOT be overwritten -- have you selected"
                        " the correct file?");
            msg.setIcon(QMessageBox::Warning);
            msg.exec();
            delete settings;
            return;
        }
        QFile::remove(filename);
    }

    settings->beginGroup("FileFormat");
    // Save version id of Met.3D.
    settings->setValue("met3dVersion", met3dVersionString);
    settings->endGroup();

    // Save scene view configuration.
    settings->beginGroup("MSceneView");
    saveConfiguration(settings);
    settings->endGroup();

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been saved.");
}


void MSceneViewGLWidget::loadConfigurationFromFile(QString filename)
{
    if (filename.isEmpty())
    {
        filename = MFileUtils::getOpenFileName(
                    nullptr,
                    "Load scene view configuration",
                    FileTypes::M_SCENE_VIEW_CONFIG,
                    MSystemManagerAndControl::getInstance()
                    ->getMet3DWorkingDirectory().absoluteFilePath("config/sceneviews"));

        if (filename.isEmpty())
        {
            return;
        }
    }

    QSettings *settings = new QSettings(filename, QSettings::IniFormat);

    QStringList groups = settings->childGroups();
    if ( !groups.contains("MSceneView") )
    {
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText("The selected file does not contain configuration data "
                    "for scene views.");
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
        delete settings;
        return;
    }

    LOG4CPLUS_INFO(mlog, "Loading configuration from " << filename);

    settings->beginGroup("MSceneView");
    loadConfiguration(settings);
    settings->endGroup();

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been loaded.");
}


void MSceneViewGLWidget::saveConfiguration(QSettings *settings)
{
    camera.saveConfiguration(settings);

    groupProp.saveAllToConfiguration(settings);

    // Properties are saved automatically, by calling save on the whole tree.
}


void MSceneViewGLWidget::loadConfiguration(QSettings *settings)
{
    if (readConfigVersion(settings) < ACTOR_PROPERTY_REWORK_COMP_VER)
    {
        loadConfigurationPrior_1_14(settings);
    }
    else
    {
        groupProp.loadAllFromConfiguration(settings);
    }
    camera.loadConfiguration(settings);
}


void MSceneViewGLWidget::loadConfigurationPrior_1_14(QSettings *settings)
{
    isLoadingFromConfig = true;
    MSystemManagerAndControl* sysMC = MSystemManagerAndControl::getInstance();

    settings->beginGroup("CameraSettings");
    cameraSequenceAnimationProp = settings->value("cameraSequenceIndex", -1).toInt();
    refreshCameraSequenceProperty();
    settings->endGroup();

    // Load interaction properties.
    settings->beginGroup("Interaction");
    // Since scene navigation mode is stored as a string, get the corresponding
    // index.
    QString enumName = settings->value("sceneNavigation", "move camera").toString();
    sceneNavigationModeProp.setEnumItem(enumName);

    QVector3D rotCenter;
    rotCenter.setX(settings->value("sceneRotationCentreLongitude", 0.).toFloat());
    rotCenter.setY(settings->value("sceneRotationCentreLatitude", 45.).toFloat());
    rotCenter.setZ(settings->value("sceneRotationCentreElevation", 1020.).toFloat());

    enumName = settings->value("fullScreenActor", "None").toString();
    fullScreenActorProp.setEnumItem(enumName);

    sceneNavigationSensitivityProp =
            settings->value("NavigationSensitivity", 1.).toDouble();

    cameraAutorotationModeProp =
            settings->value("autoRotateCamera", false).toBool();

    // Since scene sync camera with view is stored as a string, get the
    // corresponding index.
    enumName = settings->value("SyncCameraWithView", "None").toString();
    syncCameraWithViewProp.setEnumItem(enumName);

    actorInteractionModeProp = settings->value("actorInteractionMode", false).toBool();
    analysisModeProp = settings->value("analysisMode", false).toBool();
    settings->endGroup(); // interaction

    // Load rendering properties.
    settings->beginGroup("Rendering");
    backgroundColourProp =
            settings->value("backgroundColour", QColor(255, 255, 255))
                    .value<QColor>();
    farPlaneDistanceProp = settings->value("farPlaneDistance", 500.f).toDouble();
    multisamplingEnabledProp = settings->value("multisampling", true).toBool();
    antialiasingEnabledProp = settings->value("antialiasing", false).toBool();
    renderLabelsWithDepthTestProp = settings->value("depthTestForLabels", true).toBool();

    shadowMapResProp = static_cast<ShadowResolution>(settings
            ->value("shadowMapRes", static_cast<int>(ShadowResolution::NormalRes)).toInt());

    shadowDistanceProp = settings->value("shadowDistance", 400).toFloat();

    ambientLightColorProp = settings->value("ambientLightColor", QColor(255, 255, 255, 255)).value<QColor>();

    ambientLightProp = settings->value("ambientLight", 0.2f).toFloat();

    diffuseLightProp = settings->value("diffuseLight", 0.6f).toFloat();

    specularStrengthProp = settings->value("specularStrength", 0.2f).toFloat();

    verticalScalingProp = settings->value("verticalScaling", 36.).toDouble();
    fsrQualityProp = settings->value("fsrQuality", 0).toInt();

    useCustomResolutionProp =
            settings->value("useCustomResolution", false).toBool();

    customResolutionWidthProp =
            settings->value("customResolutionWidth", defaultRenderResolutionX).toDouble();

    customResolutionHeightProp =
            settings->value("customResolutionHeight", defaultRenderResolutionY).toDouble();

    settings->endGroup(); // rendering

    // Load display date time properties.
    settings->beginGroup("DisplayDateTime");
    QString syncControlName = settings->value("syncControl", "None").toString();
    displayDateTimeLabelProps.fontSizeProp = settings->value("fontSize", 24.).toDouble();
    displayDateTimeLabelProps.fontColourProp = settings->value("fontColour", QColor(0, 0, 0)).value<QColor>();
    displayDateTimeLabelProps.enableBBoxProp =
            settings->value("bBox", true).toBool();
    displayDateTimeLabelProps.bboxColourProp =
            settings->value("bBoxColour", QColor(255, 255, 255, 200))
                    .value<QColor>();
    displayDateTimePositionProp =
            settings->value("position", QPointF(-0.99, 0.99)).toPointF();
    settings->endGroup(); // display date time

    synchronizeWith(sysMC->getSyncControl(syncControlName));

    // Update display time when loading configuration only if the view is
    // initialised since otherwise adding the text will lead to an
    // initialisation error when the text is added.
    if (viewIsInitialised)
    {
        updateDisplayTime();
    }

    // Load arrow pointing north properties.
    settings->beginGroup("ArrowPointingNorth");
    northArrow.enabledProp = settings->value("enabled", false).toBool();
    northArrow.horizontalScaleProp =
            settings->value("horizontalScale", 5.).toFloat();
    northArrow.verticalScaleProp =
            settings->value("verticalScale", 5.).toFloat();
    northArrow.positionProp.setX(settings->value("lon", 0.).toFloat());
    northArrow.positionProp.setY(settings->value("lat", 80.).toFloat());
    northArrow.positionProp.setZ(pressureFromWorldZ(settings->value("worldZPos", 1.).toFloat()));
    northArrow.colourProp =
            settings->value("colour", QColor(222, 46, 30)).value<QColor>();
    settings->endGroup(); // arrow pointing north

    // Load scene view label properties.
    settings->beginGroup("SceneViewLabel");
    sceneViewLabelEnableProp =
            settings->value("enabled", true).toBool();
    settings->endGroup(); // scene view label

    isLoadingFromConfig = false;
}


void MSceneViewGLWidget::onHandleSizeChanged()
{
#ifndef CONTINUOUS_GL_UPDATE
    updateGL();
#endif
}


void MSceneViewGLWidget::synchronizeWith(
        MSyncControl *sync, bool updateGUIProperties)
{
    Q_UNUSED(updateGUIProperties);
    if (synchronizationControl == sync)
    {
        return;
    }

    // Reset connection to current synchronization control.
    // ====================================================

    // If the view is currently connected to a sync control, disconnect the
    // signals.
    if (synchronizationControl != nullptr)
    {
#ifdef DIRECT_SYNCHRONIZATION
        synchronizationControl->deregisterSynchronizedClass(this);
#else
        disconnect(synchronizationControl, SIGNAL(initDateTimeChanged(QDateTime)),
                   this, SLOT(updateDisplayTime()));
        disconnect(synchronizationControl, SIGNAL(validDateTimeChanged(QDateTime)),
                   this, SLOT(updateDisplayTime()));
#endif
    }

    // Connect to new sync control and try to switch to its current times.
    synchronizationControl = sync;

    // Update "synchronizationProperty".
    // =================================
    setUpdatesEnabled(false);
    QString newSyncID =
            (sync == nullptr) ? "None" : synchronizationControl->getID();
    int index = displayDateTimeSyncControlProp.getEnumNames().indexOf(newSyncID);
    displayDateTimeSyncControlProp.setValue(index);
    setUpdatesEnabled(true);

    // Connect to new sync control and synchronize.
    // ============================================
    if (sync != nullptr)
    {
#ifdef DIRECT_SYNCHRONIZATION
        synchronizationControl->registerSynchronizedClass(this);
#else
        connect(synchronizationControl, SIGNAL(initDateTimeChanged(QDateTime)),
                this, SLOT(updateDisplayTime()));
        connect(synchronizationControl, SIGNAL(validDateTimeChanged(QDateTime)),
                this, SLOT(updateDisplayTime()));
#endif
    }
}


SceneBounds MSceneViewGLWidget::getSceneBounds()
{
    SceneBounds sceneBounds;
    sceneBounds.topPressure_hPa = pbot;
    sceneBounds.bottomPressure_hPa = ptop;

    for(MActor *actor : scene->getRenderQueue())
    {
        if (!actor->isEnabled()) continue;
        if (auto *bboxInteface =
                dynamic_cast<MBoundingBoxInterface*>(actor))
        {
            MBoundingBox *bbox = bboxInteface->getBBoxConnection()->getBoundingBox();

            if (bbox == nullptr) continue;

            if (bbox->getWestLon() < sceneBounds.westLon)
                sceneBounds.westLon = bbox->getWestLon();
            if (bbox->getSouthLat() < sceneBounds.southLat)
                sceneBounds.southLat = bbox->getSouthLat();
            if (bbox->getEastLon() > sceneBounds.eastLon)
                sceneBounds.eastLon = bbox->getEastLon();
            if (bbox->getNorthLat() > sceneBounds.northLat)
                sceneBounds.northLat = bbox->getNorthLat();
            if (bbox->getBottomPressure_hPa() > sceneBounds.bottomPressure_hPa)
                sceneBounds.bottomPressure_hPa = bbox->getBottomPressure_hPa();
            if (bbox->getTopPressure_hPa() < sceneBounds.topPressure_hPa)
                sceneBounds.topPressure_hPa = bbox->getTopPressure_hPa();
        }
    }

    sceneBounds.center = QVector3D((sceneBounds.eastLon + sceneBounds.westLon) / 2.0,
                                    (sceneBounds.northLat + sceneBounds.southLat) / 2.0,
                                    (worldZfromPressure(sceneBounds.topPressure_hPa) + worldZfromPressure(sceneBounds.bottomPressure_hPa)) / 2.0);

    sceneBounds.extent = QVector3D(sceneBounds.eastLon - sceneBounds.center.x(),
                                   sceneBounds.northLat - sceneBounds.center.y(),
                                   worldZfromPressure(sceneBounds.topPressure_hPa))
                                           - sceneBounds.center;

    sceneBounds.radius = sceneBounds.extent.length();

    return sceneBounds;
}


int MSceneViewGLWidget::getShadowMapResolution() const
{
    return 1 << (11 + static_cast<int>(shadowMapResProp) - 1);
}


bool MSceneViewGLWidget::synchronizationEvent(
        MSynchronizationType, QVector<QVariant>)
{
    updateDisplayTime();

    return false;
}


bool MSceneViewGLWidget::setLineWidth(GLfloat lineWidth)
{
    if (MGLResourcesManager::getInstance()->supportsGlLineWidth())
    {
        glLineWidth(lineWidth);
        return true;
    }

    return false;
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

void MSceneViewGLWidget::saveToImageFile()
{
    QVector<MFileType> fileFilter = {FileTypes::M_IMAGE_PNG,
                                     FileTypes::M_IMAGE_JPG,
                                     FileTypes::M_IMAGE_WEBP,
                                     FileTypes::M_IMAGE_BMP,
                                     FileTypes::M_IMAGE_PPM,
                                     FileTypes::M_IMAGE_TIFF,
                                     FileTypes::M_IMAGE_XBM,
                                     FileTypes::M_IMAGE_XPM};

    // Variable filename stores filename and path but for some operating systems
    // not the selected extension.
    QString filename = MFileUtils::getSaveFileName(
                nullptr,
                "Save screenshot",
                fileFilter,
                QDir::home().absoluteFilePath("met3d/screenshots"),
                "screenshot");

    if (!filename.isEmpty())
    {
        saveToImageFileName(filename);
    }
}


void MSceneViewGLWidget::saveToImageFileName(QString filename)
{
    // Take Screenshot of current scene.
    QImage screenshot = QImage(renderResultWidth, renderResultHeight, QImage::Format_RGBA8888);

    switchFramebuffer(finalResultFbo);
    glReadPixels(0, 0, renderResultWidth, renderResultHeight, GL_RGBA, GL_UNSIGNED_BYTE, screenshot.bits());

    switchFramebuffer(0);

    // Mirror screenshot
    screenshot = screenshot.mirrored();

    // Chop red frame. (Only visible if view has focus.)
    if (this->hasFocus())
    {
        screenshot = screenshot.copy(1, 1, screenshot.width() - 2,
                                     screenshot.height() - 2);
    }

    if (!filename.isEmpty())
    {
        if (screenshot.save(filename, nullptr,
                            MSystemManagerAndControl::getInstance()
                                    ->getImageFileQuality()))
        {
            QString str = "Saved screenshot of current view to " + filename
                    + "\n";
            LOG4CPLUS_INFO(mlog, str);
        }
        else
        {
            QMessageBox::critical(MGLResourcesManager::getInstance(),
                                 "Error",
                                 "Could not save " + filename,
                                 QMessageBox::Ok,
                                 QMessageBox::NoButton);
            QString str = "Could not save " + filename + "\n";
            LOG4CPLUS_ERROR(mlog, str);
        }
    }
}


void MSceneViewGLWidget::resizeView()
{
    resizeViewDialog->setWindowTitle("Resize View");
    // Initialise input boxes and ratio with current window size
    resizeViewDialog->setup(this->width(),this->height());

    if (resizeViewDialog->exec() == QDialog::Rejected)
    {
        return;
    }

    int newWidth = resizeViewDialog->getWidth();
    int newHeight = resizeViewDialog->getHeight();

    MMainWindow *mainWindow =
            MSystemManagerAndControl::getInstance()->getMainWindow();
    mainWindow->resizeSceneView(newWidth, newHeight, this);
}


void MSceneViewGLWidget::recoverSceneInViewPort()
{
    SceneBounds bounds = getSceneBounds();

    // Need 2 different methods to determine camera position based on render mode.
    if (sceneNavigationMode == TOPVIEW_2D)
    {
        // Get ratio of viewport
        double ratio = static_cast<double>(renderResultWidth) / renderResultHeight;

        // Get ratio of scene extents
        double sceneRatio = bounds.extent.x() / bounds.extent.y();

        // Calculate dxHalf and dyHalf that we would need to get the camera to frame everything
        float dxHalf;
        float dyHalf;

        if (ratio < sceneRatio)
        {
            dxHalf = bounds.extent.x();
            dyHalf = dxHalf / ratio;
        }
        else
        {
            dyHalf = bounds.extent.y();
        }

        // Calculate a z coordinate from dyHalf to move the camera to.
        // This z coordinate is far enough away to frame the scene.
        float z = dyHalf / std::tan(M_PI / 8.0);

        // Set position to the scene bounds center and set z to move the camera back to where it frames the whole scene.
        QVector3D pos = bounds.center;
        pos.setZ(z);

        // Update camera location
        camera.setOrigin(pos);
    }
    else
    {
        // Get ratio of viewport
        double ratio = static_cast<double>(renderResultWidth) / renderResultHeight;
        if (ratio > 1.0) // The width is larger than height
        {
            ratio = 1.;
        }

        // Maybe save fov in degrees in a variable or constant?
        // It is used several times throughout the code.
        // Multiply fov by viewport ratio, if it is smaller than 1
        // To account for situations, where the horizontal view would be too
        // narrow for the scene to be displayed in.
        double fov = 45. * ratio * (M_PI / 180.0);

        QVector3D forward = camera.getZAxis().normalized() * -1;

        // Calculate position of the camera based on distance from the center of the scene bounds.
        // Since we know the size of the bounding sphere of the scene, we can simply divide it by the
        // tangent of half the fov, to get the distance to the center of the bounding sphere.
        double distance = fabs(bounds.radius / tan( fov / 2 ));
        QVector3D pos = bounds.center + forward * distance;

        // Update camera location
        camera.setOrigin(pos);
    }
}


float MSceneViewGLWidget::getViewportUpscaleFactor() const
{
    switch (fsrQuality)
    {
    case FSRQuality::OFF:
        return 1.0f;
    case FSRQuality::ULTRA_QUALITY:
        return 1.3f;
    case FSRQuality::QUALITY:
        return 1.5f;
    case FSRQuality::BALANCED:
        return 1.7f;
    case FSRQuality::PERFORMANCE:
        return 2.0f;
    }

    return 1.0f;
}


float MSceneViewGLWidget::getFSRlodBias() const
{
    switch(fsrQuality)
    {
        case FSRQuality::OFF:
            return 1.0f;
            break;
        case FSRQuality::ULTRA_QUALITY:
            return -0.38f;
            break;
        case FSRQuality::QUALITY:
            return -0.58f;
            break;
        case FSRQuality::BALANCED:
            return -0.79f;
            break;
        case FSRQuality::PERFORMANCE:
            return -1.0f;
            break;
    }

    return 1.0f;
}

} // namespace Met3D
