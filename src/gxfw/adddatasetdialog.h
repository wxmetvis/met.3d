/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2016      Christoph Heidelmann [+]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2023      Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**  Copyright 2024      Susanne Fuchs [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MADDDATASETDIALOG_H
#define MADDDATASETDIALOG_H

// standard library imports

// related third party imports
#include <QDialog>
#include <QAbstractButton>
#include <QSettings>

// local application imports
#include "system/mpipelineconfigurationinfo.h"


namespace Ui {
class MAddDatasetDialog;
}


namespace Met3D
{

/**

 */
class MAddDatasetDialog : public QDialog
{
    Q_OBJECT

public:
    explicit MAddDatasetDialog(QWidget *parent = 0);
    ~MAddDatasetDialog();

    PipelineType getSelectedPipelineType();

    MNWPPipelineConfigurationInfo getNWPPipelineConfigurationInfo();

    MTrajectoriesPipelineConfigurationInfo
    getTrajectoriesPipelineConfigurationInfo();

    MRadarPipelineConfigurationInfo
    getRadarPipelineConfigurationInfo();

    void resetAddDatasetGUI();

    void saveConfiguration(QSettings *settings);
    void loadConfiguration(QSettings *settings);

    /**
     * Creates a new add dataset dialog. If @p settings is provided, the dialog
     * is populated with the information from the settings.
     * @param settings Optional: settings to populate the dataset dialog with
     */
    static void createAndOpen(QSettings* settings = nullptr);

private:
    void openDialog();

public slots:
    void saveConfigurationToFile(QString filename = "");
    bool loadConfigurationFromFile(QString filename = "");

    /**
     * Slot which opens the table to edit the input variable string for derived
     * computations in a table for easier edit.
     */
    void editInputVarsInTable();

private slots:
    void browsePath();
    void inputFieldChanged();
    void setDefaultMemoryManager();

protected:

private:
    Ui::MAddDatasetDialog *ui;
    QAbstractButton *okButton;

    // Variables which are not shown in UI and only used when loading pipeline configs on program start

    /**
     * Whether pipeline is ensemble or deterministic pipeline
     */
    bool isEnsemble;
};

} // namespace Met3D

#endif // MADDDATASETDIALOG_H
