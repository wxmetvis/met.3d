/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "addtrajectoryfilterdialog.h"
#include "ui_addtrajectoryfilterdialog.h"

// standard library imports

// related third party imports
#include <QTableWidget>

// local application imports
#include "trajectories/filter/trajectoryfilterfactory.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MAddTrajectoryFilterDialog::MAddTrajectoryFilterDialog(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::MAddTrajectoryFilterDialog)
{
    ui->setupUi(this);
    createEntries();
}


MAddTrajectoryFilterDialog::~MAddTrajectoryFilterDialog()
{
    delete ui;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QString MAddTrajectoryFilterDialog::getSelectedFilterType()
{
    int row = ui->trajectoryFilterTable->currentRow();

    QString type = ui->trajectoryFilterTable->item(row, 0)->text();
    return type;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MAddTrajectoryFilterDialog::createEntries()
{
    QTableWidget *table = ui->trajectoryFilterTable;

    // Set the data field table's header.
    table->setColumnCount(1);
    table->setHorizontalHeaderLabels(QStringList() << "Filter type");

    auto factory = MTrajectoryFilterFactory::getInstance();
    QStringList filterNames = factory->getRegisteredFilterNames();

    for (const QString& filterName : filterNames)
    {
        int row = table->rowCount();
        table->setRowCount(row + 1);
        // Insert the element.
        table->setItem(
            row, 0, new QTableWidgetItem(filterName));
    }

    // Resize the table's columns to fit actor names.
    table->resizeColumnsToContents();
    // Set table width to always fit window size.
    table->horizontalHeader()->setStretchLastSection(true);
    // Disable resize of column by user.
    table->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeMode::Fixed);
    // Resize widget to fit table size.
    this->resize(table->width(), table->height());
}


} // namespace Met3D
