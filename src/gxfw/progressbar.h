/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_PROGRESSBAR_H
#define MET_3D_PROGRESSBAR_H

// standard library imports

// related third party imports
#include <QWidget>
#include <QLabel>
#include <QProgressBar>
#include <QMutex>
#include <QMenuBar>

// local application imports


namespace Met3D
{
class MTask;

/**
 * This class represents a task handle issued by the @c MProgressBar.
 * When a caller adds a task to the progress bar, it returns a handle, which
 * can be used to notify completeness of this specific task. It holds the
 * display name used to display the task besides the progress bar.
 */
class MProgressBarTaskHandle
{
public:
    explicit MProgressBarTaskHandle(QString name) : displayName(std::move(name))
    {}


    QString displayName;
};


/**
 * The global progress bar used in Met.3D. It is anchored to the right of the
 * menu bar. "Worker classes" can add tasks to this progress bar, and inform
 * it once the task is finished. This is just the progress bar and does NOT
 * schedule any tasks.
 */
class MProgressBar : public QWidget
{
Q_OBJECT

private:
    /**
     * Creates the singleton progress bar.
     * @param parent THe parent menu bar.
     */
    explicit MProgressBar(QWidget *parent = nullptr);

    ~MProgressBar() override;

public:

    /**
     * Initializes the progress bar to the menu bar. This has to be done
     * before calling @c getInstance().
     * @param menubar The QMenuBar to add the progress bar to.
     */
    static void initialize(QMenuBar *menubar);

    /**
     * @return The singleton instance, or nullptr if it has not been
     * initialized. Call @c initialize() beforehand.
     */
    static MProgressBar *getInstance();

    /**
     * Adds a new task to the progress bar given a name to display for the task.
     * @param displayName The name to display. This updates the progress bar by
     * adding one to its maximum.
     * @return The task handle.
     */
    MProgressBarTaskHandle *addTask(const QString &displayName = QString("Processing..."));

    /**
     * Convenience method to add a @c MTask to the progress bar. Does not return
     * a handle but writes the handle to MTask::progressBarTaskHandle.
     * @param task The task to add.
     */
    void addTask(MTask* task);

    /**
     * Add multiple tasks to the progress bar, given a display name for the task.
     * This adds @p numTasks to the maximum possible value of the underlying
     * progress bar. Therefore, the progress bar can "jump back", as tasks can be
     * added more quickly then completing them.
     * @param numTasks THe amount of tasks to add.
     * @param displayName The display name for the tasks.
     * @return Task handles for each task added.
     */
    QList<MProgressBarTaskHandle *> addTasks(int numTasks, const QString &displayName);

    /**
     * Notifies the progress bar that a task has been completed. This updates
     * the progress bar and the label.
     * @param handle The handle that has been completed.
     */
    void taskCompleted(MProgressBarTaskHandle *handle);

    /**
     * Convenience function that marks the first handle in the
     * list as completed and removes the handle from the list.
     * Does not mark any task as completed if the list is empty.
     *
     * @param handles The list of handles to mark the first task
     * as completed.
     * @return True if a task has been completed and removed from
     * the provided list.
     */
    bool firstTaskCompleted(QList<MProgressBarTaskHandle*>& handles);

    /**
     * Notifies the progress bar that a list of tasks have been completed. This
     * updates the progress bar and the label. This will invalidate and delete
     * the handles from the provided list.
     * @param handles The handles that have been completed.
     */
    void tasksCompleted(QList<MProgressBarTaskHandle*>& handles);

    /**
     * Convenience function to mark the given @p task in the progress bar
     * as completed. Accesses its underlying handle.
     * @param task The task to mark completed.
     */
    void taskCompleted(MTask* task);

private:
    /**
     * Update the tool tip which is shown on hover.
     */
    void updateToolTip();

    /**
     * Resets the progress bar. This deletes all remaining task handles.
     */
    void resetProgressBar();

    /**
     * Toggle the frontend progress bar.
     * @param on Whether to toggle it on or off.
     */
    void toggleProgressBar(bool on);

    /**
     * Triggers a UI update cycle. Since some tasks are executed on the main thread,
     * not forcing a redraw of the progress bar would delay its visual update
     * until all main thread tasks are completed. This method ensures the progress
     * bar is updated promptly to reflect ongoing progress. This method is called
     * on a timer to make sure it does not clog the event handler.
     */
    void redrawIfRequested();

    // The instance of the progress bar.
    static MProgressBar *instance;

    // Label and QProgressBar used in this widget.
    QLabel *progressBarLabel;
    QProgressBar *progressBar;

    int progress;
    int maximum;

    // Whether a redraw of the progress bar has been requested.
    bool redrawRequested;
    QTimer *updateTimer;

    // Currently active task handles.
    QList<MProgressBarTaskHandle *> activeTasks;
    // Mutex to avoid concurrent updates of progress or maximum.
    QMutex m_mutex;

signals:
    /**
     * Emit this signal to force update the display of the progress bar.
     */
    void updateProgressBar();

private slots:
    /**
     * Called from the main thread to update the progress bar. Never call this
     * slot directly and use the signal above instead. This makes sure that the
     * progress bar is only updated from the main (UI) thread.
     */
    void onUpdateProgressBar();

};

} // Met3D


#endif //MET_3D_PROGRESSBAR_H
