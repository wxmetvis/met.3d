/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2017      Michael Kern [+]
**  Copyright 2015-2016 Christoph Heidelmann [+]
**  Copyright 2024      Susanne Fuchs [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "nwpactorvariable.h"

// standard library imports

// related third party imports
#include "GL/glew.h"
#include <QtCore>
#include <utility>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/mscenecontrol.h"
#include "gxfw/nwpmultivaractor.h"
#include "gxfw/memberselectiondialog.h"
#include "actors/nwpvolumeraycasteractor.h"
#include "actors/nwphorizontalsectionactor.h"
#include "mainwindow.h"
#include "data/netcdfwriter.h"
#include "data/structuredgridstatisticsanalysis.h"
#include "datasource/selectdatavariabledialog.h"
#include "util/propertyutils.h"


#ifdef WIN32

unsigned int abs(unsigned int a)
{
    return a;
}

#endif


namespace Met3D
{

/******************************************************************************
***                            NWPActorVariable                             ***
*******************************************************************************/

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWPActorVariable::MNWPActorVariable(MNWPMultiVarActor *actor)
    : dataSource(nullptr),
      uuid(),
      grid(nullptr),
      radarGrid(nullptr),
      sectionHeight(1000),
      textureUnitRadarDataMap(-1),
      textureUnitHSectionRadarLonLats(-1),
      textureUnitRadarRayAngles(-1),
      aggregationDataSource(nullptr),
      gridAggregation(nullptr),
      textureDataField(nullptr),
      textureUnitDataField(-1),
      textureLonLatLevAxes(nullptr),
      textureUnitLonLatLevAxes(-1),
      textureSurfacePressure(nullptr),
      textureUnitSurfacePressure(-1),
      textureHybridCoefficients(nullptr),
      textureUnitHybridCoefficients(-1),
      textureDataFlags(nullptr),
      textureUnitDataFlags(-1),
      texturePressureTexCoordTable(nullptr),
      textureUnitPressureTexCoordTable(-1),
      textureAuxiliaryPressure(nullptr),
      textureUnitAuxiliaryPressure(-1),
      textureDummy1D(nullptr),
      textureDummy2D(nullptr),
      textureDummy3D(nullptr),
      textureUnitDummy1D(-1),
      textureUnitDummy2D(-1),
      textureUnitDummy3D(-1),
      transferFunction(nullptr),
      textureUnitTransferFunction(-1),
      actor(actor),
      singleVariableAnalysisControl(nullptr),
      ensembleFilterOperation(""),
      ensembleMemberLoadedFromConfiguration(-1),
      useFlagsIfAvailable(false),
      gridTopologyMayHaveChanged(true),
      requestPropertiesFactory(new MRequestPropertiesFactory(this)),
      suppressUpdate(false),
      initialized(false)
{
    // Create and initialise properties.
    // ===============================================
    QString groupName = QString("%1 (%2)").arg(variableName).arg(
            MStructuredGrid::verticalLevelTypeToString(levelType));
    varGroupProp = MProperty(groupName);
    varGroupProp.setConfigGroup(groupName);

    datasourceNameProp = MStringProperty("Data source ID", "");
    datasourceNameProp.setConfigKey("data_source_id");
    datasourceNameProp.setEditable(false);
    varGroupProp.addSubProperty(datasourceNameProp);

    variableLongNameProp = MStringProperty("Long name", "");
    variableLongNameProp.setConfigKey("long_name");
    variableLongNameProp.setEditable(false);
    varGroupProp.addSubProperty(variableLongNameProp);

    changeVariableGroupProp = MArrayProperty("Change / Remove");
    changeVariableGroupProp.setShowPropertyLabels(false);
    varGroupProp.addSubProperty(changeVariableGroupProp);

    changeVariableProp = MButtonProperty("Change variable", "Change");
    changeVariableProp.registerValueCallback([=]()
    {
        if (actor->suppressActorUpdates())
        {
            return;
        }

        if(changeVariable())
        {
            actor->onChangeActorVariable(this);
            actor->emitActorChangedSignal();
        }
    });
    changeVariableGroupProp.append(&changeVariableProp);

    removeVariableProp = MButtonProperty("Remove", "Remove");
    changeVariableGroupProp.append(&removeVariableProp);

    dataStatisticsGroupProp = MProperty("Data statistics of the entire grid");
    varGroupProp.addSubProperty(dataStatisticsGroupProp);

    showDataStatisticsProp = MButtonProperty("Show statistics", "Compute");
    showDataStatisticsProp.registerValueCallback([=]()
    {
        if (actor->suppressActorUpdates())
        {
            return;
        }
        runStatisticalAnalysis(significantDigitsProp,
                               histogramDisplayModeProp,
                               histogramDisplayColourProp);
    });
    dataStatisticsGroupProp.addSubProperty(showDataStatisticsProp);

    significantDigitsProp = MIntProperty("Significant digits", 0);
    significantDigitsProp.setConfigKey("significant_digits");
    significantDigitsProp.setTooltip(
                "Digits considered for computation of the data value"
                " distribution."
                "\nIf this number is negative, the corresponding digits in"
                " front of the decimal point will be neglected.");
    dataStatisticsGroupProp.addSubProperty(significantDigitsProp);

    QStringList histogramDisplayModes = {"Relative frequencies",
                                         "Absolute grid point count"};

    histogramDisplayModeProp = MEnumProperty("Histogram displays", histogramDisplayModes);
    histogramDisplayModeProp.setConfigKey("historgram_display");
    dataStatisticsGroupProp.addSubProperty(histogramDisplayModeProp);

    histogramDisplayColourProp = MColorProperty("Histogram colour", QColor(Qt::blue));
    histogramDisplayColourProp.setConfigKey("histogram_colour");
    dataStatisticsGroupProp.addSubProperty(histogramDisplayColourProp);

    // Property: Synchronize time and ensemble with an MSyncControl instance?
    synchronizationGroupProp = MProperty("Synchronization");
    varGroupProp.addSubProperty(synchronizationGroupProp);

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    QStringList syncControls = sysMC->getSyncControlIdentifiers();
    synchronizationProp = MEnumProperty("Synchronize with", syncControls, 0);
    synchronizationProp.setConfigKey("synchronize_with");
    synchronizationProp.registerValueCallback([=]()
    {
        MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
        QString syncID = synchronizationProp.getSelectedEnumName();
        synchronizeWith(sysMC->getSyncControl(syncID));
    });
    synchronizationProp.saveAsEnumName(true);
    synchronizationGroupProp.addSubProperty(synchronizationProp);

    synchronizeInitTimeProp = MBoolProperty("Sync. init time", true);
    synchronizeInitTimeProp.setConfigKey("sync_init_time");
    synchronizeInitTimeProp.registerValueCallback([=]()
    {
        updateTimeProperties();

        if (actor->suppressActorUpdates()) return;

        if (synchronizeInitTimeProp)
        {
            if (setInitDateTime(synchronizationControl->initDateTime()))
            {
                asynchronousDataRequest();
            }
        }
    });
    synchronizationGroupProp.addSubProperty(synchronizeInitTimeProp);
    synchronizeValidTimeProp = MBoolProperty("Sync. valid time", true);
    synchronizeValidTimeProp.setConfigKey("sync_valid_time");
    synchronizeValidTimeProp.registerValueCallback([=]()
    {
        updateTimeProperties();

        if (actor->suppressActorUpdates()) return;

        if (synchronizeValidTimeProp)
        {
            if (setValidDateTime(synchronizationControl->validDateTime()))
            {
                asynchronousDataRequest();
            }
        }
    });
    synchronizationGroupProp.addSubProperty(synchronizeValidTimeProp);
    synchronizeEnsembleProp = MBoolProperty("Sync. ensemble", true);
    synchronizeEnsembleProp.setConfigKey("sync_ensemble");
    synchronizeEnsembleProp.registerValueCallback([=]()
    {
        updateEnsembleProperties();
        updateSyncPropertyColourHints();

        if (actor->suppressActorUpdates()) return;

        if (synchronizeEnsembleProp)
        {
            if (setEnsembleMember(synchronizationControl->ensembleMember()))
            {
                asynchronousDataRequest();
            }
        }
    });
    synchronizationGroupProp.addSubProperty(synchronizeEnsembleProp);

    // Properties for init and valid time.
    initTimeProp = MEnumProperty("Initialisation");
    initTimeProp.setConfigKey("init_time");
    initTimeProp.registerValueCallback([=]()
    {
        updateValidTimeProperty();

        if (actor->suppressActorUpdates()) return;

        asynchronousDataRequest();
    });
    varGroupProp.addSubProperty(initTimeProp);
    validTimeProp = MEnumProperty("Valid");
    validTimeProp.setConfigKey("valid_time");
    validTimeProp.registerValueCallback([=]()
    {
        if (suppressUpdate) return; // ignore if init times are being updated
        if (actor->suppressActorUpdates()) return;

        asynchronousDataRequest();
    });
    varGroupProp.addSubProperty(validTimeProp);

    ensembleMultiMemberListProp = MArrayProperty("Selected members");
    ensembleMultiMemberListProp.setShowPropertyLabels(false);
    varGroupProp.addSubProperty(ensembleMultiMemberListProp);

    // Properties for ensemble control.
    ensembleMultiMemberSelectionProp = MButtonProperty("Select members", "...");
    ensembleMultiMemberSelectionProp.setTooltip(
                "Select which ensemble members this variable should utilize");
    ensembleMultiMemberSelectionProp.registerValueCallback([=]()
    {
        if (actor->suppressActorUpdates()) return;

        MMemberSelectionDialog dlg;
        dlg.setAvailableEnsembleMembers(dataSource->availableEnsembleMembers(
                                        levelType, variableName));
        dlg.setSelectedMembers(selectedEnsembleMembers);

        if ( dlg.exec() == QDialog::Accepted )
        {
            // Get set of selected members from dialog, update
            // ensembleMultiMemberProperty to display set to user and, if
            // necessary, request new data field.
            QSet<unsigned int> selMembers = dlg.getSelectedMembers();
            if (!selMembers.isEmpty())
            {
                selectedEnsembleMembers = selMembers;

                // Update the current data field if either the currently
                // selected member has changed (because the previously selected
                // one is not available anymore) or the ensemble more is set to
                // mean, std.dev, etc (in this case the computed field needs to
                // be recomputed based on the new member set).

                // Selected member has changed?
                bool updateDataField = updateEnsembleSingleMemberProperty();
                // ..or ens mode is != member.
                updateDataField |= !ensembleFilterOperation.isEmpty();

                if (updateDataField) asynchronousDataRequest();
                    return;
            }
            else
            {
                // The user has selected an emtpy set of members. Display a
                // warning and do NOT accept the empty set.
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setText("You need to select at least one member.");
                msgBox.exec();
            }
        }
    });

    ensembleMultiMemberProp = MStringProperty("Selected members", "");
    ensembleMultiMemberProp.setConfigKey("selected_members");
    ensembleMultiMemberProp.setEditable(false);

    ensembleMultiMemberListProp.append(&ensembleMultiMemberProp);
    ensembleMultiMemberListProp.append(&ensembleMultiMemberSelectionProp);

    QStringList ensembleModeNames;
    ensembleModeNames << "member" << "mean" << "standard deviation"
                      << "p(> threshold)" << "p(< threshold)"
                      << "min" << "max" << "max-min";
    multipleEnsembleMembersEnabled =
            actor->supportsMultipleEnsembleMemberVisualization();
    // Check if the actor supports simultaneous visualization of multiple
    // ensemble members. If yes, the corresponding ensemble mode will trigger
    // the request of a "grid aggregation" containing all requested members.
    // Search the code for "if (multipleEnsembleMembersEnabled)...".
    if (multipleEnsembleMembersEnabled)
    {
        ensembleModeNames << "Multiple members";
    }
    ensembleModeProp = MEnumProperty("Ensemble mode", ensembleModeNames);
    ensembleModeProp.setConfigKey("ensemble_mode");
    ensembleModeProp.registerValueCallback([=]()
    {
        updateEnsembleProperties();

        if (actor->suppressActorUpdates()) return;

        // Reload data.
        asynchronousDataRequest();
    });
    varGroupProp.addSubProperty(ensembleModeProp);

    ensembleSingleMemberProp = MEnumProperty("Ensemble member");
    ensembleSingleMemberProp.setConfigKey("ensemble_member");
    ensembleSingleMemberProp.registerValueCallback([=]()
    {
        if (actor->suppressActorUpdates()) return;

        if (ensembleSingleMemberProp.isEnabled() )
        {
            asynchronousDataRequest();
            return;
        }
    });
    varGroupProp.addSubProperty(ensembleSingleMemberProp);

    ensembleThresholdProp = MFloatProperty("Ensemble threshold", 0);
    ensembleThresholdProp.setConfigKey("ensemble_threshold");
    ensembleThresholdProp.setDecimals(6);
    ensembleThresholdProp.setStep(0.1);
    ensembleThresholdProp.registerValueCallback([=]()
    {
        updateEnsembleProperties();

        if (actor->suppressActorUpdates()) return;

        // Reload data.
        asynchronousDataRequest();
        return;
    });
    varGroupProp.addSubProperty(ensembleThresholdProp);

    // Rendering properties.
    varRenderingGroupProp = MProperty("Rendering");
    varGroupProp.addSubProperty(varRenderingGroupProp);

    transferFunctionProp = MTransferFunction1DProperty("Transfer function");
    transferFunctionProp.setConfigKey("transfer_function");
    transferFunctionProp.registerValueCallback([=]()
    {
       setTransferFunctionFromProperty();
       actor->onChangeActorVariableTransferFunction(this);
       actor->emitActorChangedSignal();
    });
    varRenderingGroupProp.addSubProperty(transferFunctionProp);

    // Debug properties.
    debugGroupProp = MProperty("Debug");
    varGroupProp.addSubProperty(debugGroupProp);
    dumpGridDataProp = MButtonProperty("Dump grid data to log", "Dump");
    dumpGridDataProp.registerValueCallback([=]()
    {
        if (grid)
        {
            // Dump raw grid data to console, printing first 200 data values.
            grid->dumpGridData(200);
        }
        return;
    });
    debugGroupProp.addSubProperty(dumpGridDataProp);

    if (multipleEnsembleMembersEnabled)
    {
        // Create an aggregation source that belongs to this actor variable.
        aggregationDataSource = new MGridAggregationDataSource();
        connect(aggregationDataSource, &MGridAggregationDataSource::dataRequestCompleted,
                this, &MNWPActorVariable::asynchronousDataAvailable);
    }
}


MNWPActorVariable::~MNWPActorVariable()
{
    // Release data fields.
    releaseDataItems();
    releaseAggregatedDataItems();

    // Delete synchronization links (don't update the already deleted GUI
    // properties anymore...).
    synchronizeWith(nullptr, false);

    if (multipleEnsembleMembersEnabled)
    {
        disconnect(aggregationDataSource,
                   &MWeatherPredictionDataSource::dataRequestCompleted,
                   this, &MNWPActorVariable::asynchronousDataAvailable);
        delete aggregationDataSource;
    }

    if (textureUnitDataField >=0)
        actor->releaseTextureUnit(textureUnitDataField);
    if (textureUnitLonLatLevAxes >=0)
        actor->releaseTextureUnit(textureUnitLonLatLevAxes);
    if (textureUnitTransferFunction >=0)
        actor->releaseTextureUnit(textureUnitTransferFunction);
    if (textureUnitSurfacePressure >=0)
        actor->releaseTextureUnit(textureUnitSurfacePressure);
    if (textureUnitHybridCoefficients >=0)
        actor->releaseTextureUnit(textureUnitHybridCoefficients);
    if (textureUnitDataFlags >=0)
        actor->releaseTextureUnit(textureUnitDataFlags);
    if (textureUnitPressureTexCoordTable >=0)
        actor->releaseTextureUnit(textureUnitPressureTexCoordTable);
    if (textureUnitAuxiliaryPressure >= 0)
        actor->releaseTextureUnit(textureUnitAuxiliaryPressure);
    if (textureUnitDummy1D >= 0)
        actor->releaseTextureUnit(textureUnitDummy1D);
    if (textureUnitDummy2D >= 0)
        actor->releaseTextureUnit(textureUnitDummy2D);
    if (textureUnitDummy3D >= 0)
        actor->releaseTextureUnit(textureUnitDummy3D);
    if (textureUnitRadarRayAngles >= 0)
        actor->releaseTextureUnit(textureUnitRadarRayAngles);
    if (textureUnitRadarDataMap >= 0)
        actor->releaseTextureUnit(textureUnitRadarDataMap);
    if (textureUnitHSectionRadarLonLats >= 0)
        actor->releaseTextureUnit(textureUnitHSectionRadarLonLats);

    for (MRequestProperties *requestProperty : propertiesList)
    {
        delete requestProperty;
    }

    delete requestPropertiesFactory;
    if (textureDummy1D) delete textureDummy1D;
    if (textureDummy2D) delete textureDummy2D;
    if (textureDummy3D) delete textureDummy3D;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWPActorVariable::initialize()
{
    actor->enableActorUpdates(false);

    if (uuid.isNull())
    {
        uuid = QUuid::createUuid();
    }

    initializeDataSource();

    QString groupName = QString("%1 (%2)").arg(variableName).arg(
                MStructuredGrid::verticalLevelTypeToString(levelType));
    varGroupProp.setName(groupName);
    varGroupProp.setConfigGroup(groupName);

    // Obtain new texture units.
    if (textureUnitDataField >=0)
        actor->releaseTextureUnit(textureUnitDataField);
    if (textureUnitLonLatLevAxes >=0)
        actor->releaseTextureUnit(textureUnitLonLatLevAxes);
    if (textureUnitTransferFunction >=0)
        actor->releaseTextureUnit(textureUnitTransferFunction);
    if (textureUnitSurfacePressure >=0)
        actor->releaseTextureUnit(textureUnitSurfacePressure);
    if (textureUnitHybridCoefficients >=0)
        actor->releaseTextureUnit(textureUnitHybridCoefficients);
    if (textureUnitDataFlags >=0)
        actor->releaseTextureUnit(textureUnitDataFlags);
    if (textureUnitPressureTexCoordTable >=0)
        actor->releaseTextureUnit(textureUnitPressureTexCoordTable);
    if (textureUnitAuxiliaryPressure >= 0)
        actor->releaseTextureUnit(textureUnitAuxiliaryPressure);
    if (textureUnitDummy1D >= 0)
        actor->releaseTextureUnit(textureUnitDummy1D);
    if (textureUnitDummy2D >= 0)
        actor->releaseTextureUnit(textureUnitDummy2D);
    if (textureUnitDummy3D >= 0)
        actor->releaseTextureUnit(textureUnitDummy3D);
    if (textureUnitRadarRayAngles >= 0)
        actor->releaseTextureUnit(textureUnitRadarRayAngles);
    if (textureUnitRadarDataMap >= 0)
        actor->releaseTextureUnit(textureUnitRadarDataMap);
    if (textureUnitHSectionRadarLonLats >= 0)
        actor->releaseTextureUnit(textureUnitHSectionRadarLonLats);

    textureUnitDataField = actor->assignTextureUnit();
    textureUnitTransferFunction = actor->assignTextureUnit();
    textureUnitDummy1D = actor->assignTextureUnit();
    textureUnitDummy2D = actor->assignTextureUnit();
    textureUnitDummy3D = actor->assignTextureUnit();
    textureUnitDataFlags = actor->assignTextureUnit();
    textureUnitLonLatLevAxes = actor->assignTextureUnit();
    textureUnitSurfacePressure = -1;
    textureUnitHybridCoefficients = -1;
    textureUnitPressureTexCoordTable = -1;
    textureUnitAuxiliaryPressure = -1;
    textureUnitRadarRayAngles = -1;
    textureUnitRadarDataMap = -1;
    textureUnitHSectionRadarLonLats = -1;

    if (levelType == HYBRID_SIGMA_PRESSURE_3D)
    {
        textureUnitSurfacePressure = actor->assignTextureUnit();
        textureUnitHybridCoefficients = actor->assignTextureUnit();
        textureUnitPressureTexCoordTable = actor->assignTextureUnit();
    }
    else if (levelType == PRESSURE_LEVELS_3D)
    {
        textureUnitPressureTexCoordTable = actor->assignTextureUnit();
    }
    else if (levelType == AUXILIARY_PRESSURE_3D)
    {
        textureUnitAuxiliaryPressure = actor->assignTextureUnit();
    }
    else if (levelType == RADAR_LEVELS_3D)
    {
        textureUnitRadarRayAngles = actor->assignTextureUnit();
        textureUnitRadarDataMap = actor->assignTextureUnit();
        textureUnitHSectionRadarLonLats = actor->assignTextureUnit();
    }

    textureDataField = textureHybridCoefficients =
            textureLonLatLevAxes = textureSurfacePressure =
            textureDataFlags = texturePressureTexCoordTable =
            textureAuxiliaryPressure = nullptr;

    textureHSectionRadarData = textureHSectionRadarLonLats = textureRadarRayAngles = nullptr;
    for (GL::MTexture *&radarTexture : textureRadarDataMap)
    {
        radarTexture = nullptr;
    }

    gridTopologyMayHaveChanged = true;

    datasourceNameProp = dataSourceID;
    variableLongNameProp = dataSource->variableLongName(levelType, variableName);

    requestPropertiesFactory->updateProperties(&propertiesList,
                                               dataSource->requiredKeys());

    updateInitTimeProperty();
    updateValidTimeProperty();
    initEnsembleProperties();

    // Get values from sync control, if connected to one.
    if (synchronizationControl == nullptr)
    {
        synchronizeInitTimeProp.setEnabled(false);
        synchronizeInitTimeProp.setEnabled(false);
        synchronizeEnsembleProp.setEnabled(false);
    }
    else
    {
        if (synchronizeInitTimeProp)
        {
            setInitDateTime(synchronizationControl->initDateTime());
        }
        updateValidTimeProperty();
        if (synchronizeValidTimeProp)
        {
            setValidDateTime(synchronizationControl->validDateTime());
        }
        if (synchronizeEnsembleProp)
        {
            setEnsembleMember(synchronizationControl->ensembleMember());
        }
    }

    updateTimeProperties();
    updateEnsembleProperties();
    updateSyncPropertyColourHints();

    setTransferFunctionFromProperty();

    if (textureDummy1D == nullptr)
        textureDummy1D = new GL::MTexture(GL_TEXTURE_1D, GL_R32F, 1);
    if (textureDummy2D == nullptr)
        textureDummy2D = new GL::MTexture(GL_TEXTURE_2D, GL_R32F, 1, 1);
    if (textureDummy3D == nullptr)
        textureDummy3D = new GL::MTexture(GL_TEXTURE_3D, GL_R32F, 1, 1, 1);

    textureDummy1D->bindToTextureUnit(0);
    glTexImage1D(GL_TEXTURE_1D, 0, GL_R32F, 1, 0, GL_RED, GL_FLOAT, nullptr); CHECK_GL_ERROR;
    textureDummy2D->bindToTextureUnit(0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_R32F, 1, 1, 0, GL_RED, GL_FLOAT, nullptr); CHECK_GL_ERROR;
    textureDummy3D->bindToTextureUnit(0);
    glTexImage3D(GL_TEXTURE_3D, 0, GL_R32F, 1, 1, 1, 0, GL_RED, GL_FLOAT, nullptr); CHECK_GL_ERROR;
    textureDummy3D->unbindFromTextureUnit(0);

    // Load data field.
    asynchronousDataRequest();

    actor->enableActorUpdates(true);
    initialized = true;
}


void MNWPActorVariable::initializeDataSource()
{
    // This method is called on variable creation and when the datafield it
    // represents is changed (see changeVariables()). In the latter case,
    // the old data source needs to be disconnected.
    if (dataSource != nullptr)
    {
        disconnect(dataSource, &MWeatherPredictionDataSource::dataRequestCompleted,
                   this, &MNWPActorVariable::asynchronousDataAvailable);
    }

    // Get a pointer to the new source and connect to its request completed
    // signal.
    MAbstractDataSource *source =
            MSystemManagerAndControl::getInstance()->getDataSource(dataSourceID);
    dataSource = dynamic_cast<MWeatherPredictionDataSource*>(source);

    // Set the grid type depending on if the data source provides a radar or structured grid.
    gridType = dataSource->returnsRadarGrid() ? RADAR_GRID : STRUCTURED_GRID;

    if (dataSource == nullptr)
    {
        LOG4CPLUS_ERROR(mlog, "No data source with ID " << dataSourceID << " available.");
    }
    else
    {
        connect(dataSource, &MWeatherPredictionDataSource::dataRequestCompleted,
                this, &MNWPActorVariable::asynchronousDataAvailable);

        if (multipleEnsembleMembersEnabled)
        {
            // Connect the new data source to this variable's aggregation source.
            // Memory manager may be set only once.
            if (aggregationDataSource->getMemoryManager() == nullptr)
            {
                aggregationDataSource->setMemoryManager(
                        dataSource->getMemoryManager());
            }
            aggregationDataSource->setInputSource(dataSource);
        }
    }
}


void MNWPActorVariable::updateGroupProperty()
{
    QString groupName = QString("%1 (%2)").arg(variableName).arg(
            MStructuredGrid::verticalLevelTypeToString(levelType));
    varGroupProp.setName(groupName);
    varGroupProp.setConfigGroup(groupName);
}


void MNWPActorVariable::synchronizeWith(
        MSyncControl *sync, bool updateGUIProperties)

{
    if (synchronizationControl == sync)
    {
        return;
    }

    // Reset connection to current synchronization control.
    // ====================================================

    // If the variable is currently connected to a sync control, reset the
    // background colours of the valid and init time properties (they have
    // been set to red/green from this class to indicate time sync status,
    // see setValidDateTime()) and disconnect the signals.
    if (synchronizationControl != nullptr)
    {
        for (MSceneControl* scene : actor->getScenes())
        {
            scene->variableDeletesSynchronizationWith(synchronizationControl);
        }

#ifdef DIRECT_SYNCHRONIZATION
        synchronizationControl->deregisterSynchronizedClass(this);
#else
        disconnect(synchronizationControl, SIGNAL(initDateTimeChanged(QDateTime)),
                   this, SLOT(setInitDateTime(QDateTime)));
        disconnect(synchronizationControl, SIGNAL(validDateTimeChanged(QDateTime)),
                   this, SLOT(setValidDateTime(QDateTime)));
        disconnect(synchronizationControl, SIGNAL(ensembleMemberChanged(int)),
                   this, SLOT(setEnsembleMember(int)));
#endif
    }
    // Connect to new sync control and try to switch to its current times.
    synchronizationControl = sync;

    // Update "synchronizationProperty".
    // =================================
    if (updateGUIProperties)
    {
        QString displayedSyncID = synchronizationProp.getSelectedEnumName();
        QString newSyncID =
                (sync == nullptr) ? "None" : synchronizationControl->getID();
        if (displayedSyncID != newSyncID)
        {
            actor->enableActorUpdates(false);
            synchronizationProp.setEnumItem(newSyncID);
            actor->enableActorUpdates(true);
        }
    }

    // Connect to new sync control and synchronize.
    // ============================================
    if (sync != nullptr)
    {
        // Tell the actor's scenes that this variable synchronized with this
        // sync control.
        for (MSceneControl* scene : actor->getScenes())
        {
            scene->variableSynchronizesWith(sync);
        }

#ifdef DIRECT_SYNCHRONIZATION
        synchronizationControl->registerSynchronizedClass(this);
#else
//TODO (mr, 01Dec2015) -- add checks for synchronizeInitTime etc..
        connect(sync, SIGNAL(initDateTimeChanged(QDateTime)),
                this, SLOT(setInitDateTime(QDateTime)));
        connect(sync, SIGNAL(validDateTimeChanged(QDateTime)),
                this, SLOT(setValidDateTime(QDateTime)));
        connect(sync, SIGNAL(ensembleMemberChanged(int)),
                this, SLOT(setEnsembleMember(int)));
#endif
        if (updateGUIProperties)
        {
            actor->enableActorUpdates(false);
            synchronizeInitTimeProp.setEnabled(true);
            synchronizeValidTimeProp.setEnabled(true);
            synchronizeEnsembleProp.setEnabled(true);
            actor->enableActorUpdates(true);
        }

        // Disable actor updates to avoid asynchonous data requests being
        // triggered from the individual time/ensemble updates before all
        // properties have been updated.
        actor->enableActorUpdates(false);
        if (synchronizeInitTimeProp)
        {
            setInitDateTime(sync->initDateTime());
        }
        if (synchronizeValidTimeProp)
        {
            setValidDateTime(sync->validDateTime());
        }
        if (synchronizeEnsembleProp)
        {
            setEnsembleMember(sync->ensembleMember());
        }
        actor->enableActorUpdates(true);

        // Trigger data request manually after all properties have been
        // synchronised.
        if (actor->isInitialized())
        {
            asynchronousDataRequest();
        }
    }
    else
    {
        // No synchronization. Reset property colours and disable sync
        // checkboxes.
        initTimeProp.resetBackgroundColor();
        validTimeProp.resetBackgroundColor();
        ensembleSingleMemberProp.resetBackgroundColor();

        if (updateGUIProperties)
        {
            actor->enableActorUpdates(false);
            synchronizeInitTimeProp.setEnabled(false);
            synchronizeValidTimeProp.setEnabled(false);
            synchronizeEnsembleProp.setEnabled(false);
            actor->enableActorUpdates(true);
        }
    }

    // Update "synchronize xyz" GUI properties.
    // ========================================
    if (updateGUIProperties && actor->isInitialized())
    {
        updateTimeProperties();
        updateEnsembleProperties();
    }
}


bool MNWPActorVariable::synchronizationEvent(
        MSynchronizationType syncType, QVector<QVariant> data)
{
    switch (syncType)
    {
    case SYNC_INIT_TIME:
    {
        if (!synchronizeInitTimeProp)
        {
            return false;
        }
        actor->enableActorUpdates(false);
        bool newInitTimeSet = setInitDateTime(data.at(0).toDateTime());
        actor->enableActorUpdates(true);
        if (newInitTimeSet)
        {
            asynchronousDataRequest(true);
        }
        return newInitTimeSet;
    }
    case SYNC_VALID_TIME:
    {
        if (!synchronizeValidTimeProp)
        {
            return false;
        }
        actor->enableActorUpdates(false);
        bool newValidTimeSet = setValidDateTime(data.at(0).toDateTime());
        actor->enableActorUpdates(true);
        if (newValidTimeSet)
        {
            asynchronousDataRequest(true);
        }
        return newValidTimeSet;
    }
    case SYNC_INIT_VALID_TIME:
    {
        actor->enableActorUpdates(false);
        bool newInitTimeSet = false;
        bool newValidTimeSet = false;
        if (synchronizeInitTimeProp)
        {
            newInitTimeSet = setInitDateTime(data.at(0).toDateTime());
        }
        if (synchronizeValidTimeProp)
        {
            newValidTimeSet = setValidDateTime(data.at(1).toDateTime());
        }
        actor->enableActorUpdates(true);
        if (newInitTimeSet || newValidTimeSet)
        {
            asynchronousDataRequest(true);
        }
        return (newInitTimeSet || newValidTimeSet);
    }
    case SYNC_ENSEMBLE_MEMBER:
    {
        if (!synchronizeEnsembleProp)
        {
            return false;
        }
        actor->enableActorUpdates(false);
        bool newEnsembleMemberSet = setEnsembleMember(data.at(0).toInt());
        actor->enableActorUpdates(true);
        if (newEnsembleMemberSet)
        {
            asynchronousDataRequest(true);
        }
        return newEnsembleMemberSet;
    }
    default:
        break;
    }

    return false;
}


void MNWPActorVariable::updateSyncPropertyColourHints(MSceneControl *scene)
{
    if (synchronizationControl == nullptr)
    {
        // No synchronization -- reset all property colours.
        initTimeProp.resetBackgroundColor();
        validTimeProp.resetBackgroundColor();
        ensembleSingleMemberProp.resetBackgroundColor();
    }
    else
    {
        // ( Also see internalSetDateTime() ).

        // Init time.
        // ==========
        bool match = (getPropertyTime(initTimeProp)
                      == synchronizationControl->initDateTime());
        QColor colour = match ? QColor(0, 255, 0) : QColor(255, 0, 0);
        if (synchronizeInitTimeProp)
        {
            initTimeProp.setBackgroundColor(colour);
        }
        else
        {
            initTimeProp.resetBackgroundColor();
        }

        // Valid time.
        // ===========
        match = (getPropertyTime(validTimeProp)
                 == synchronizationControl->validDateTime());
        colour = match ? QColor(0, 255, 0) : QColor(255, 0, 0);
        if (synchronizeValidTimeProp)
        {
            validTimeProp.setBackgroundColor(colour);
        }
        else
        {
            validTimeProp.resetBackgroundColor();
        }

        // Ensemble.
        // =========
        match = (getEnsembleMember()
                 == synchronizationControl->ensembleMember());
        colour = match ? QColor(0, 255, 0) : QColor(255, 0, 0);
        if (synchronizeEnsembleProp)
        {
            ensembleSingleMemberProp.setBackgroundColor(colour);
        }
        else
        {
            ensembleSingleMemberProp.resetBackgroundColor();
        }
    }
}


MDataRequest MNWPActorVariable::buildDataRequest() const
{
    QDateTime initTime  = getPropertyTime(initTimeProp);
    QDateTime validTime = getPropertyTime(validTimeProp);
    unsigned int member = getEnsembleMember();

    MDataRequestHelper rh;

    if (gridType == RADAR_GRID)
    {
        rh.insert("VARIABLE", variableName);
        rh.insert("VALID_TIME", validTime);
    }
    else
    {
        rh.insert("LEVELTYPE", levelType);
        rh.insert("VARIABLE", variableName);
        rh.insert("INIT_TIME", initTime);
        rh.insert("VALID_TIME", validTime);

        if ((ensembleFilterOperation == "")
                || (ensembleFilterOperation == "MULTIPLE_MEMBERS"))
        {
            rh.insert("MEMBER", member);
        }
        else
        {
            rh.insert("ENS_OPERATION", ensembleFilterOperation);
            rh.insert("SELECTED_MEMBERS", selectedEnsembleMembers);
        }
    }

    // Add request keys from the property subgroups.
    for (MRequestProperties* props : propertiesList)
    {
        props->addToRequest(&rh);
    }

    MDataRequest r = rh.request();

    return r;
}


void MNWPActorVariable::asynchronousDataRequest(bool synchronizationRequest)
{
#ifndef DIRECT_SYNCHRONIZATION
    Q_UNUSED(synchronizationRequest);
#endif
    // Request grid.
    // ===================================================================
    MDataRequest r = buildDataRequest();
    MDataRequestHelper rh = MDataRequestHelper(r);

    LOG4CPLUS_TRACE(mlog, "Emitting data request " << r << " ...");

    // Place the requests into the QSet pendingRequests to decide in O(1)
    // in asynchronousDataAvailable() whether to accept an incoming request.
    pendingRequests.insert(r);
    // Place the request into the request queue to ensure correct order
    // when incoming requests are handled.
    MRequestQueueInfo rqi;
    rqi.request = r;
    rqi.available = false;
#ifdef DIRECT_SYNCHRONIZATION
    rqi.syncchronizationRequest = synchronizationRequest;
#endif
    pendingRequestsQueue.enqueue(rqi);
#ifdef MSTOPWATCH_ENABLED
    if (!stopwatches.contains(r)) stopwatches[r] = new MStopwatch();
#endif

    dataSource->requestData(r);

    // Special case: If ensemble mode is set to "multiple members". A grid
    // aggregation containing the required members is requested IN ADDITION to
    // the single member requested before. The single member request before is
    // REDUNDANT and should be suppressed in the future.

    //TODO (mr, 18Apr2018) -- Revise this architecture.
    if (gridType == STRUCTURED_GRID
        && ensembleFilterOperation == "MULTIPLE_MEMBERS")
    {
        if (!multipleEnsembleMembersEnabled)
        {
            return;
        }
        rh.remove("MEMBER");
        rh.insert("ENS_OPERATION", ensembleFilterOperation);
        rh.insert("SELECTED_MEMBERS", selectedEnsembleMembers);
        r = rh.request();

        LOG4CPLUS_TRACE(mlog, "Emitting additional 'multiple ensemble member'"
                              " request " << r << " ...");

        pendingRequests.insert(r);
        MRequestQueueInfo rqi;
        rqi.request = r;
        rqi.available = false;
#ifdef DIRECT_SYNCHRONIZATION
        rqi.syncchronizationRequest = synchronizationRequest;
#endif
        pendingRequestsQueue.enqueue(rqi);
#ifdef MSTOPWATCH_ENABLED
        if (!stopwatches.contains(r)) stopwatches[r] = new MStopwatch();
#endif

        aggregationDataSource->requestData(r);
    }
}


void MNWPActorVariable::saveConfiguration(QSettings *settings)
{
    settings->setValue("dataLoaderID", dataSourceID);
    settings->setValue("levelType", levelType);
    settings->setValue("variableName", variableName);
    settings->setValue("uuid", uuid);

    // Save ensemble members.
    // ==============================
    settings->setValue("ensembleUtilizedMembers",
                       MDataRequestHelper::uintSetToString(
                           selectedEnsembleMembers));
}


void MNWPActorVariable::loadConfiguration(QSettings *settings)
{
    // This method is only called from MNWPMultiVarActor::loadConfigurationHeader().
    // Data source is set in MNWPMultiVarActor::loadConfigurationHeader() to be
    // able to handle the case in which the stored data source is not
    // available and the user is asked for an alternative source. The
    // remaining configuration should nevertheless be loaded.

    // Load ensemble members.
    // ==============================
    selectedEnsembleMembers = MDataRequestHelper::uintSetFromString(
            settings->value("ensembleUtilizedMembers").toString());

    // We need the properties list filled at this point to be able to load the
    // configurations of properties at creation of actor.
    if (propertiesList.isEmpty())
    {
        // Get a pointer to the new source and connect to its request completed
        // signal.
        MAbstractDataSource *source =
                MSystemManagerAndControl::getInstance()->getDataSource(dataSourceID);
        dataSource = dynamic_cast<MWeatherPredictionDataSource*>(source);
        // Set the grid type depending on if the data source provides a radar or structured grid.
        gridType = dataSource->returnsRadarGrid() ? RADAR_GRID : STRUCTURED_GRID;

        if (dataSource != nullptr)
        {
            requestPropertiesFactory->updateProperties(
                    &propertiesList, dataSource->requiredKeys());
        }
    }
}


void MNWPActorVariable::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    // Load ensemble members.
    // ==============================
    selectedEnsembleMembers = MDataRequestHelper::uintSetFromString(
            settings->value("ensembleUtilizedMembers").toString());

    // Load data statistics properties.
    // ================================
    significantDigitsProp = settings->value("statisticsSignificantDigits", 0.).toInt();
    histogramDisplayModeProp = settings->value(
            "statisticsHistogramDisplayMode",
            MStructuredGridStatisticsAnalysisControl
            ::RELATIVE_FREQUENCY_DISTRIBUTION).toInt();

    // Load ensemble mode properties.
    // ==============================
    // At this time, the variables hasn't been initialized yet .. store the
    // loaded ensemble member temporarily; updateEnsembleSingleMemberProperty()
    // will make use of this variable.
    ensembleMemberLoadedFromConfiguration =
            settings->value("ensembleSingleMember", -1).toInt();

    ensembleThresholdProp = settings->value("ensembleThreshold", 0.).toFloat();

    QString emName = settings->value("ensembleMode").toString();
    if (!setEnsembleMode(emName))
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(QString("Variable '%1':\n"
                               "Ensemble mode '%2' does not exist.\n"
                               "Setting ensemble mode to 'member'.")
                               .arg(variableName, emName));
        msgBox.exec();
    }

    // Load synchronization properties (AFTER the ensemble mode properties
    // have been loaded; sync may overwrite some settings).
    // ===================================================================
    synchronizeInitTimeProp = settings->value("synchronizeInitTime", true).toBool();
    synchronizeValidTimeProp = settings->value("synchronizeValidTime", true).toBool();
    synchronizeEnsembleProp = settings->value("synchronizeEnsemble", true).toBool();

    QString syncID = settings->value("synchronizationID").toString();
    if ( !syncID.isEmpty() )
    {
        MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
        if (sysMC->getSyncControlIdentifiers().contains(syncID))
        {
            synchronizeWith(sysMC->getSyncControl(syncID));
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setIcon(QMessageBox::Warning);
            msgBox.setText(QString("Variable '%1':\n"
                                   "Synchronization control '%2' does not exist.\n"
                                   "Setting synchronization control to 'None'.")
                                   .arg(variableName, syncID));
            msgBox.exec();

            synchronizeWith(nullptr);
        }
    }

    // Load rendering properties.
    // ==========================
    QString tfName = settings->value("transferFunction", "None").toString();
    while (!setTransferFunction(tfName))
    {
        if (!MTransferFunction::loadMissingTransferFunction(
                tfName, MTransferFunction1D::staticActorType(),
                "Variable ", variableName, settings))
        {
            break;
        }
    }

    // We need the properties list filled at this point to be able to load the
    // configurations of properties at creation of actor.
    if (propertiesList.empty())
    {
        // Get a pointer to the new source and connect to its request completed
        // signal.
        MAbstractDataSource *source =
                MSystemManagerAndControl::getInstance()->getDataSource(dataSourceID);
        dataSource = dynamic_cast<MWeatherPredictionDataSource*>(source);
        // Set the grid type depending on if the data source provides a radar or structured grid.
        gridType = dataSource->returnsRadarGrid() ? RADAR_GRID : STRUCTURED_GRID;

        if (dataSource != nullptr)
        {
            requestPropertiesFactory->updateProperties(
                    &propertiesList, dataSource->requiredKeys());
        }
    }

    // Load properties of connected request property subgroups.
    for (MRequestProperties* props : propertiesList)
        props->loadConfigurationPrior_V_1_14(settings);
}


bool MNWPActorVariable::setEnsembleMode(const QString& emName)
{
    QStringList emNames = ensembleModeProp.getEnumNames();
    int emIndex = emNames.indexOf(emName);

    if (emIndex >= 0)
    {
        ensembleModeProp = emIndex;
        return true;
    }

    // Set ensemble mode property to "None".
    ensembleModeProp = 0;

    return false; // the given ensemble mode name could not be found
}


bool MNWPActorVariable::setTransferFunction(const QString& tfName)
{
    return transferFunctionProp.setByName(tfName);
}


void MNWPActorVariable::setTransferFunctionToFirstAvailable()
{
    for (auto tf : transferFunctionProp.getTransferFunctions())
    {
        if (tf != nullptr)
        {
            transferFunctionProp = tf;
            return;
        }
    }
}


void MNWPActorVariable::useFlags(bool b)
{
    useFlagsIfAvailable = b;

    if (grid != nullptr)
    {
        if (useFlagsIfAvailable)
        {
            if (textureDataFlags == nullptr)
                textureDataFlags = grid->getFlagsTexture();
            // else: if textureDataFlags points to a valid texture there's
            // nothing to be done.
        }
        else
        {
            // Flags are disabled -- if there was a texture bound release it.
            if (textureDataFlags != nullptr)
            {
                grid->releaseFlagsTexture();
                textureDataFlags = nullptr;
            }
        }
    }
}


int MNWPActorVariable::getEnsembleMember() const
{
    QString memberString = ensembleSingleMemberProp.getSelectedEnumName();

    bool ok = true;
    int member = memberString.toInt(&ok);

    if (ok) return member; else return -99999;
}


void MNWPActorVariable::triggerAsynchronousDataRequest(
        bool gridTopologyMayChange)
{
    if ( !actor->isInitialized() ) return;

    if (gridTopologyMayChange) gridTopologyMayHaveChanged = true;
    asynchronousDataRequest();
}


void MNWPActorVariable::actorPropertyChangeEvent(
        MPropertyType::ChangeNotification ptype, void *value)
{
    for (auto & props : propertiesList)
        props->actorPropertyChangeEvent(ptype, value);
}


bool MNWPActorVariable::hasData() const
{
    if (gridType == RADAR_GRID)
    {
        // The raycaster uses "textureDataField", the radar actor "textureRadarDataMap".
        return (!textureRadarDataMap.isEmpty() || textureDataField != nullptr) && radarGrid != nullptr;
    }
    else if (gridType == STRUCTURED_GRID)
    {
        return textureDataField != nullptr && grid != nullptr;
    }
    return false;

}


QString MNWPActorVariable::debugOutputAsString()
{
    QString str = QString("==================\nNWPActorVariable :: "
                          "%1 of %2:\n").arg(variableName, dataSourceID);

    str += QString("Number of pending requests: %1\n").arg(pendingRequests.size());
    str += QString("Entries in the queue of pending requests:\n");
    for (int i = 0; i < pendingRequestsQueue.size(); i++)
    {
        str += QString("  ++ entry #%1: available=%2, request=%3\n").arg(i)
                .arg(pendingRequestsQueue[i].available)
                .arg(pendingRequestsQueue[i].request);
    }

    str += QString("==================\n");

    return str;
}


QString MNWPActorVariable::getIdentifier() const
{
    return dataSourceID + "/" + QString::number(levelType) + "/" + variableName;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

bool MNWPActorVariable::setValidDateTime(const QDateTime& datetime)
{
    return internalSetDateTime(availableValidTimes, datetime, validTimeProp);
}


bool MNWPActorVariable::setInitDateTime(const QDateTime& datetime)
{
    return internalSetDateTime(availableInitTimes, datetime, initTimeProp);
}


bool MNWPActorVariable::setEnsembleMember(int member)
{
    // Ensemble mean: member == -1.
    // ============================
    if (member < 0)
    {
        // Set ensemble filter operation to MEAN.
#ifdef DIRECT_SYNCHRONIZATION
        if (ensembleFilterOperation == "MEAN")
        {
            // The ensemble mode was already set to MEAN? Nothing needs to
            // be done.
            return false;
        }
        else
        {
            setEnsembleMode("mean");
            return true;
        }
#else
        ensembleFilterOperation = "MEAN";
        asynchronousDataRequest();
#endif
    }

    // Change to the specified member.
    // ===============================
    else
    {
        // Change member via ensemble member property.

#ifdef DIRECT_SYNCHRONIZATION
        QString prevEnsembleFilterOperation = ensembleFilterOperation;
        int prevEnsembleMember = getEnsembleMember();
#endif

        if (ensembleFilterOperation != "MEMBER") setEnsembleMode("member");

        setEnumPropertyClosest<unsigned int>(
                selectedEnsembleMembersAsSortedList, (unsigned int)member,
                ensembleSingleMemberProp, synchronizeEnsembleProp,
                actor->getScenes());

#ifdef DIRECT_SYNCHRONIZATION
        // Does a new data request need to be emitted?
        if (prevEnsembleFilterOperation != ensembleFilterOperation) return true;
        if (prevEnsembleMember != member) return true;
        return false;
#endif
    }

    return false;
}


void MNWPActorVariable::asynchronousDataAvailable(MDataRequest request)
{
    // Decide in O(1) based on the QSet whether to accept the incoming request.
    if (!pendingRequests.contains(request)) return;
    pendingRequests.remove(request);

    LOG4CPLUS_TRACE(mlog, "Accepting received data for request <"
                    << request << ">.");
    LOG4CPLUS_TRACE(mlog, "Number of additionally remaining pending requests: "
                    << pendingRequests.size());

#ifdef MSTOPWATCH_ENABLED
    if (stopwatches.contains(request))
    {
        stopwatches[request]->split();
        LOG4CPLUS_TRACE(mlog, "request processed in "
                        << stopwatches[request]->getLastSplitTime(
                            MStopwatch::SECONDS)
                        << " seconds.");
        delete stopwatches[request];
        stopwatches.remove(request);
    }
#endif

    // Mark the incoming request as "available" in the request queue. Usually
    // the requests are received in correct order, i.e. this loop on average
    // will only compare the first entry.
    for (int i = 0; i < pendingRequestsQueue.size(); i++)
    {
        if (pendingRequestsQueue[i].request == request)
        {
            pendingRequestsQueue[i].available = true;
            // Don't break; the incoming request might be relevant for multiple
            // entries in the queue.
        }
    }

    // Debug: Output content of request queue.
//    for (int i = 0; i < pendingRequestsQueue.size(); i++)
//    {
//        LOG4CPLUS_TRACE(mlog, "RQI[" << i << "]["
//                        << pendingRequestsQueue[i].available << "] <"
//                        << pendingRequestsQueue[i].request << ">");
//    }

    // Prepare datafields for rendering as long as they are available in
    // the order in which they were requested.
    while ( ( !pendingRequestsQueue.isEmpty() ) &&
            pendingRequestsQueue.head().available )
    {
        MRequestQueueInfo rqi = pendingRequestsQueue.dequeue();
        MDataRequest processRequest = rqi.request;
        LOG4CPLUS_TRACE(mlog, "Preparing for rendering: request <"
                        << processRequest << ">.");

        if (processRequest.contains("MULTIPLE_MEMBERS") && gridType == STRUCTURED_GRID)
        {
            if (multipleEnsembleMembersEnabled)
            {
                // Handle incoming grid aggregation.
                releaseAggregatedDataItems();
                gridAggregation = aggregationDataSource->getData(processRequest);
            }
        }
        else if (gridType == RADAR_GRID)
        {
            releaseDataItems();
            // Add an empy structured grid and a radarGrid containing the data.
            grid = new MStructuredGrid(MVerticalLevelType::RADAR_LEVELS_3D, 0, 0, 0);
            radarGrid = dataSource->getRadarData(processRequest);
            generateHorizontalSectionGeometry();
            textureDataField = radarGrid->getTexture();
            textureRadarRayAngles = radarGrid->getRayTexture();
            radarMetadataBuffer = radarGrid->getMetadataBuffer();

            for (const uint &elIdx : radarGrid->getElevationIndexList())
            {
                textureRadarDataMap[elIdx] = radarGrid->getRadarElevationDataTexture(elIdx);
            }

            asynchronousDataAvailableEvent(grid);
        }
        else // Structured grid without multiple members.
        {
            // Release currently used data items.
            releaseDataItems();

            // If the ensemble mode was "multiple members" before but is not
            // anymore, the aggregated data grids have not yet been released.
            // Hence, if the current ensemble more is NOT "multiple members",
            // release them here.
            if (ensembleFilterOperation != "MULTIPLE_MEMBERS")
            {
                releaseAggregatedDataItems();
            }

            // Acquire the new ones.
            grid = dataSource->getData(processRequest);
            if (grid == nullptr)
            {
//TODO (29Aug2018, mr) -- how should we handle errors in the pipeline that
// lead to a nullptr instead of a valid grid being returned?
                LOG4CPLUS_ERROR(mlog, "Something went wrong "
                                "in the data pipeline -- no grid data available "
                                "... don't know what to do.");
            }

            textureDataField  = grid->getTexture();
            textureLonLatLevAxes = grid->getLonLatLevTexture();

            if (useFlagsIfAvailable && grid->flagsEnabled())
            {
                textureDataFlags = grid->getFlagsTexture();
            }
            else
            {
                textureDataFlags = nullptr;
            }

            if (MLonLatHybridSigmaPressureGrid *hgrid =
                    dynamic_cast<MLonLatHybridSigmaPressureGrid*>(grid))
            {
                textureHybridCoefficients = hgrid->getHybridCoeffTexture();
                textureSurfacePressure = hgrid->getSurfacePressureGrid()->getTexture();
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
                texturePressureTexCoordTable = hgrid->getPressureTexCoordTexture2D();
#endif
            }

            if (MLonLatAuxiliaryPressureGrid *apgrid =
                    dynamic_cast<MLonLatAuxiliaryPressureGrid*>(grid))
            {
                textureAuxiliaryPressure =
                        apgrid->getAuxiliaryPressureFieldGrid()->getTexture();
            }

            if (MRegularLonLatStructuredPressureGrid *pgrid =
                    dynamic_cast<MRegularLonLatStructuredPressureGrid*>(grid))
            {
                texturePressureTexCoordTable = pgrid->getPressureTexCoordTexture1D();
            }

            asynchronousDataAvailableEvent(grid);
        }

        // If the last requested data item has been processed, notify dependent
        // modules that data fields have changed and the sync request has been
        // completed.
        if (pendingRequestsQueue.isEmpty())
        {
            dataFieldChangedEvent();
            actor->dataFieldChangedEvent();

#ifdef DIRECT_SYNCHRONIZATION
            // If this was a synchronization request signal to the sync control
            // that it has been completed.
            if (rqi.syncchronizationRequest)
            {
                synchronizationControl->synchronizationCompleted(this);
            }
#endif
        }
    }
}

void MNWPActorVariable::generateHorizontalSectionGeometry()
{
    if (gridType == STRUCTURED_GRID)
    {
        LOG4CPLUS_ERROR(mlog, "Cannot generate horizontal radar section geometry "
                              "for a structured grid, only radar grids are "
                              "supported." );
        return;
    }
    QVector<float> lonlats;
    QVector<float> data;
    radarGrid->calculateHorizontalRadarSection(sectionHeight, lonlats, data);
    textureHSectionRadarLonLats = radarGrid->getHSecLonLatTexture(lonlats);
    textureHSectionRadarData = radarGrid->getHSecDataTexture(data);
}


/******************************************************************************
***                           PROTECTED METHODS                             ***
*******************************************************************************/

void MNWPActorVariable::releaseDataItems()
{
    // Release currently used data items.
    if (gridType == STRUCTURED_GRID && grid)
    {
        if (MLonLatHybridSigmaPressureGrid *hgrid =
                dynamic_cast<MLonLatHybridSigmaPressureGrid*>(grid))
        {
            hgrid->releaseHybridCoeffTexture();
            textureHybridCoefficients = nullptr;
            hgrid->getSurfacePressureGrid()->releaseTexture();
            textureSurfacePressure = nullptr;
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
            hgrid->releasePressureTexCoordTexture2D();
            texturePressureTexCoordTable = nullptr;
#endif
        }

        if (MLonLatAuxiliaryPressureGrid *apgrid =
                dynamic_cast<MLonLatAuxiliaryPressureGrid*>(grid))
        {
            apgrid->getAuxiliaryPressureFieldGrid()->releaseTexture();
            textureAuxiliaryPressure = nullptr;
        }

        if (MRegularLonLatStructuredPressureGrid *pgrid =
                dynamic_cast<MRegularLonLatStructuredPressureGrid*>(grid))
        {
            pgrid->releasePressureTexCoordTexture1D();
            texturePressureTexCoordTable = nullptr;
        }

        if (textureDataFlags != nullptr)
        {
            grid->releaseFlagsTexture();
            textureDataFlags = nullptr;
        }

        grid->releaseTexture();
        textureDataField = nullptr;
        grid->releaseLonLatLevTexture();
        textureLonLatLevAxes = nullptr;
        dataSource->releaseData(grid);
        grid = nullptr;
    }

    if (gridType == RADAR_GRID && radarGrid)
    {
        radarGrid->releaseGPUItems();
        textureRadarRayAngles = nullptr;
        for (GL::MTexture *&radarTexture : textureRadarDataMap.values())
        {
            radarTexture = nullptr;
        }
        radarMetadataBuffer = nullptr;
        dataSource->releaseData(radarGrid); // Deletes radarGrid.
        radarGrid = nullptr;
        delete grid;
        grid = nullptr;
    }

    // Remove data statistics analysis control if present.
    if (singleVariableAnalysisControl != nullptr)
    {
        delete singleVariableAnalysisControl;
        singleVariableAnalysisControl = nullptr;
    }
}


void MNWPActorVariable::releaseAggregatedDataItems()
{
    if (gridAggregation)
    {
        aggregationDataSource->releaseData(gridAggregation);
        gridAggregation = nullptr;
    }
}


QDateTime MNWPActorVariable::getPropertyTime(const MEnumProperty &enumProperty)
{
    const QStringList& dateStrings = enumProperty.getEnumNames();

    // If the list of date strings is empty return an invalid null time.
    if (dateStrings.empty()) return {};

    int index = enumProperty.value();
    QDateTime dt = QDateTime::fromString(dateStrings.at(index), Qt::ISODate);
    dt.setTimeSpec(Qt::UTC);
    return dt;
}


void MNWPActorVariable::updateInitTimeProperty()
{
    suppressUpdate = true;

    // Get the current init time value.
    QDateTime initTime  = getPropertyTime(initTimeProp);

    // Get available init times from the data loader. Convert the QDateTime
    // objects to strings for the enum manager.
    if (dataSource != nullptr)
    {
        availableInitTimes = dataSource->availableInitTimes(
                    levelType, variableName);
    }
    else
    {
        availableInitTimes = QList<QDateTime>();
        LOG4CPLUS_WARN(mlog, "Update of available init times failed; "
                             "no datasource available.");
    }

    QStringList timeStrings;
    for (const QDateTime &time : availableInitTimes)
    {
        timeStrings << time.toString(Qt::ISODate);
    }

    initTimeProp.setEnumNames(timeStrings);

    setInitDateTime(initTime);

    suppressUpdate = false;
}


void MNWPActorVariable::updateValidTimeProperty()
{
    suppressUpdate = true;

    // Get the current time values.
    QDateTime initTime  = getPropertyTime(initTimeProp);
    QDateTime validTime = getPropertyTime(validTimeProp);

    // Get a list of the available valid times for the new init time,
    // convert the QDateTime objects to strings for the enum manager.
    if (dataSource != nullptr)
    {
        availableValidTimes = dataSource->availableValidTimes(
                    levelType, variableName, initTime);
    }
    else
    {
        availableValidTimes = QList<QDateTime>();
        LOG4CPLUS_WARN(mlog, "Update of available valid times failed; "
                             "no datasource available.");
    }

    QStringList validTimeStrings;
    for (const QDateTime &time : availableValidTimes)
    {
        validTimeStrings << time.toString(Qt::ISODate);
    }

    validTimeProp.setEnumNames(validTimeStrings);

    // Try to re-set the old valid time.
    setValidDateTime(validTime);

    suppressUpdate = false;
}


void MNWPActorVariable::updateTimeProperties()
{
    actor->enableActorUpdates(false);

    initTimeProp.setEnabled(!synchronizeInitTimeProp || synchronizationControl == nullptr);
    validTimeProp.setEnabled(!synchronizeValidTimeProp || synchronizationControl == nullptr);

    updateSyncPropertyColourHints();

    actor->enableActorUpdates(true);
}


void MNWPActorVariable::initEnsembleProperties()
{
    // Initially all ensemble members are selected to be used for ensemble
    // operations. Exception: loadConfiguration() has loaded a set of
    // selected members from the configuration file. In this case, test
    // whether all those members are actually available.
    if (selectedEnsembleMembers.empty())
    {
        selectedEnsembleMembers = dataSource->availableEnsembleMembers(
                    levelType, variableName);
    }
    else
    {
        selectedEnsembleMembers = selectedEnsembleMembers.intersect(
                    dataSource->availableEnsembleMembers(
                        levelType, variableName));

    }
    updateEnsembleSingleMemberProperty();
}


void MNWPActorVariable::updateEnsembleProperties()
{
    int mode = ensembleModeProp.value();

    actor->enableActorUpdates(false);

    // Ensemble properties are only enabled if not synchronized.
    ensembleModeProp.setEnabled(!synchronizeEnsembleProp || synchronizationControl == nullptr);

    switch ( mode )
    {
    case (0):
        // single ensemble member
        ensembleSingleMemberProp.setEnabled(true);
        ensembleThresholdProp.setEnabled(false);
        ensembleFilterOperation = "";
        break;
    case (1):
        // mean
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(false);
        ensembleFilterOperation = "MEAN";
        break;
    case (2):
        // stddev
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(false);
        ensembleFilterOperation = "STDDEV";
        break;
    case (3):
        // > threshold
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(true);
        ensembleFilterOperation = QString("P>%1").arg(ensembleThresholdProp.value());
        break;
    case (4):
        // < threshold
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(true);
        ensembleFilterOperation = QString("P<%1").arg(ensembleThresholdProp.value());
        break;
    case (5):
        // min
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(false);
        ensembleFilterOperation = "MIN";
        break;
    case (6):
        // max
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(false);
        ensembleFilterOperation = "MAX";
        break;
    case (7):
        // max-min
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(false);
        ensembleFilterOperation = "MAX-MIN";
        break;
    case (8):
        // multiple members
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(false);
        ensembleFilterOperation = "MULTIPLE_MEMBERS";
    }

    // If the ensemble is synchronized, disable all properties (they are set
    // via the synchronization control).
    if (synchronizeEnsembleProp && synchronizationControl != nullptr)
    {
        ensembleSingleMemberProp.setEnabled(false);
        ensembleThresholdProp.setEnabled(false);
    }

    actor->enableActorUpdates(true);
}


bool MNWPActorVariable::updateEnsembleSingleMemberProperty()
{
    // Remember currently set ensemble member in order to restore it below
    // (if getEnsembleMember() returns a value < 0 the list is currently
    // empty; however, since the ensemble members are represented by
    // unsigned ints below we cast this case to 0).
    int prevEnsembleMember = std::max(0, getEnsembleMember());

    if (ensembleMemberLoadedFromConfiguration > 0)
    {
        // If an ensemble member has been loaded from a config file, try
        // to restore this member.
        prevEnsembleMember = ensembleMemberLoadedFromConfiguration;
        ensembleMemberLoadedFromConfiguration = -1;
    }

    // Update ensembleMultiMemberProperty to display the currently selected
    // list of ensemble members.
    QString s = MDataRequestHelper::uintSetToString(selectedEnsembleMembers);
    ensembleMultiMemberProp = s;
    ensembleMultiMemberProp.setTooltip(s);

    // Update ensembleSingleMemberProperty so that the user can choose only
    // from the list of selected members. (Requires first sorting the set of
    // members as a list, which can then be converted  to a string list).
    selectedEnsembleMembersAsSortedList = selectedEnsembleMembers.values();
    std::sort(selectedEnsembleMembersAsSortedList.begin(),
              selectedEnsembleMembersAsSortedList.end());

    QStringList selectedMembersAsStringList;
    for (unsigned int member : selectedEnsembleMembersAsSortedList)
        selectedMembersAsStringList << QString("%1").arg(member);

    actor->enableActorUpdates(false);
    ensembleSingleMemberProp.setEnumNames(selectedMembersAsStringList);
    setEnumPropertyClosest<unsigned int>(
            selectedEnsembleMembersAsSortedList,
            (unsigned int)prevEnsembleMember,
            ensembleSingleMemberProp, synchronizeEnsembleProp,
            actor->getScenes());
    actor->enableActorUpdates(true);

    bool displayedMemberHasChanged = (getEnsembleMember() != prevEnsembleMember);
    return displayedMemberHasChanged;
}


bool MNWPActorVariable::setTransferFunctionFromProperty()
{
    transferFunction = dynamic_cast<MTransferFunction1D*>(transferFunctionProp.value());
    if (transferFunction == nullptr)
    {
        return false;
    }
    return true;
}


/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

bool MNWPActorVariable::internalSetDateTime(
        const QList<QDateTime>& availableTimes,
        const QDateTime& datetime,
        MEnumProperty& timeProperty)
{
    // Find the time closest to "datetime" in the list of available valid
    // times.
    int i = -1; // use of "++i" below
    bool exactMatch = false;
    while (i < availableTimes.size()-1)
    {
        // Loop as long as datetime is larger that the currently inspected
        // element (use "++i" to have the same i available for the remaining
        // statements in this block).
        if (datetime > availableTimes.at(++i)) continue;

        // We'll only get here if datetime <= availableTimes.at(i). If we
        // have an exact match, break the loop. This is our time.
        if (availableTimes.at(i) == datetime)
        {
            exactMatch = true;
            break;
        }

        // If datetime cannot be exactly matched it lies between indices i-1
        // and i in availableTimes. Determine which is closer.
        if (i == 0) break; // if there's no i-1 we're done
        if ( abs(datetime.secsTo(availableTimes.at(i-1)))
             <= abs(datetime.secsTo(availableTimes.at(i))) ) i--;
        // "i" now contains the index of the closest available valid time.
        break;
    }

    if (i > -1)
    {
        // ( Also see updateSyncPropertyColourHints() ).

        // Update background colour of the valid time property in the connected
        // scene's property browser: green if the scene's valid time is an
        // exact match with one of the available valid time, red otherwise.
        if (synchronizationControl != nullptr)
        {
            QColor colour = exactMatch ? QColor(0, 255, 0) : QColor(255, 0, 0);
            timeProperty.setBackgroundColor(colour);
        }

        // Get the currently selected index.
        int currentIndex = timeProperty.value();

        if (i == currentIndex)
        {
            // Index i is already the current one. Nothing needs to be done.
            return false;
        }
        else
        {
            // Set the new valid time.
            timeProperty.setValue(i);
            // A new index was set. Return true.
            return true;
        }
    }

    return false;
}


void MNWPActorVariable::runStatisticalAnalysis(double significantDigits,
                                               int histogramDisplayMode,
                                               const QColor& histogramDisplayColour)
{
    if (singleVariableAnalysisControl == nullptr)
    {
        // Create new analysis control. (Constructor of analysis control
        // sets analysis control of variable.)
        new MStructuredGridStatisticsAnalysisControl(this);
        if (gridType == RADAR_GRID)
        {
            singleVariableAnalysisControl->setMemoryManager(
                radarGrid->getMemoryManager());
        }
        else
        {
            singleVariableAnalysisControl->setMemoryManager(
                grid->getMemoryManager());
        }
    }
    MDataRequestHelper rh;
    rh.insert("HISTOGRAM_SIGNIFICANT_DIGITS",
              QString::number(significantDigits));
    rh.insert("HISTOGRAM_DISPLAYMODE", histogramDisplayMode);
    // MKM QVector3D::QVector3D(float xpos, float ypos, float zpos)
    // convert QColor to QVector3D and pass to DataRequestHelper
    rh.insert("HISTOGRAM_DISPLAYCOLOUR", QVector3D(histogramDisplayColour.redF(),
                                                  histogramDisplayColour.greenF(),
                                                  histogramDisplayColour.blueF()));
    singleVariableAnalysisControl->run(rh.request());
}


bool MNWPActorVariable::changeVariable()
{
    // Open an MSelectDataVariableDialog and re-initialize the variable with
    // information returned from the dialog.
    MSelectDataVariableDialog dialog(actor->supportedLevelTypes());

    if (dialog.exec() == QDialog::Rejected)
    {
        return false;
    }

    MSelectableDataVariable dataVariable = dialog.getSelectedDataVariable();

    LOG4CPLUS_INFO(mlog, "New variable has been selected: "
                    << dataVariable.variableName);

    dataSourceID = dataVariable.dataSourceID;
    levelType    = dataVariable.levelType;
    variableName = dataVariable.variableName;

    releaseDataItems();
    releaseAggregatedDataItems();
    actor->enableActorUpdates(false);
    initialize();
    actor->enableActorUpdates(true);

    return true;
}


/******************************************************************************
*******************************************************************************/
/******************************************************************************
*******************************************************************************/


/******************************************************************************
***                    MNWP2DSectionActorVariable                           ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWP2DSectionActorVariable::MNWP2DSectionActorVariable(
        MNWPMultiVarActor *actor)
    : MNWPActorVariable(actor),
      targetGrid2D(nullptr),
      textureTargetGrid(nullptr),
      textureUnitTargetGrid(-1),
      imageUnitTargetGrid(-1)
{
    assert(actor != nullptr);

    // Create and initialise Properties.
    // ===============================================

    // Property to save the current 2D section grid to a file.
    saveXSecGridProp = MButtonProperty("Save xsec grid", "Save");
    saveXSecGridProp.registerValueCallback([=]()
    {
        // Click on button "save" --> trigger a saveAsNetCDF() call for the
        // target grid.
        LOG4CPLUS_INFO(mlog, "Saving cross-section grid...");

        QString filename = QString("cross_section_grid_%1_hPa.met3d.nc")
                .arg(variableName);
        if (targetGrid2D)
        {
            netCDF::MNetCDFWriter::write(targetGrid2D, filename);
            LOG4CPLUS_INFO(mlog, "done.");
        }
        else
        {
            LOG4CPLUS_ERROR(mlog, "No cross-section grid defined.");
        }
    });
    debugGroupProp.addSubProperty(saveXSecGridProp);

    // 2D render settings.
    renderSettings.renderMode = RenderMode::Disabled;
    QStringList renderModeNames;
    renderModeNames << "disabled" << "filled contours" << "pseudo colour"
                    << "line contours" << "filled and line contours"
                    << "pcolour and line contours";
    renderSettings.renderModeProp = MEnumProperty("Render mode", renderModeNames);
    renderSettings.renderModeProp.setConfigKey("render_mode");
    renderSettings.renderModeProp.saveAsEnumName(true);
    renderSettings.renderModeProp.registerValueCallback([=]()
    {
        renderSettings.renderMode = static_cast<RenderMode::Type>(
                        renderSettings.renderModeProp.value());
        actor->emitActorChangedSignal();
    });

    // Place render mode property at the top of all entries in the "rendering"
    // group.
    varRenderingGroupProp.insertSubProperty(0, renderSettings.renderModeProp);

    renderSettings.contourSetGroupProp = MProperty("Contour sets");
    varRenderingGroupProp.addSubProperty(renderSettings.contourSetGroupProp);

    renderSettings.addContourSetProp = MButtonProperty("Add contour set", "Add");
    renderSettings.addContourSetProp.registerValueCallback([=]()
    {
        actor->enableEmissionOfActorChangedSignal(false);
        addContourSet();
        actor->enableEmissionOfActorChangedSignal(true);
    });
    renderSettings.contourSetGroupProp.addSubProperty(renderSettings.addContourSetProp);

    renderSettings.contoursUseTFProp = MBoolProperty("Use transfer function", false);
    renderSettings.contoursUseTFProp.setConfigKey("use_transfer_function");
    renderSettings.contoursUseTFProp.setTooltip(
                "Use transfer function for all contour sets");
    renderSettings.contoursUseTFProp.registerValueCallback(actor, &MActor::emitActorChangedSignal);
    renderSettings.contourSetGroupProp.addSubProperty(renderSettings.contoursUseTFProp);

    addContourSet();
}


MNWP2DSectionActorVariable::~MNWP2DSectionActorVariable()
{
    if (imageUnitTargetGrid >= 0)
        actor->releaseImageUnit(imageUnitTargetGrid);
    if (textureUnitTargetGrid >= 0)
        actor->releaseTextureUnit(textureUnitTargetGrid);

    for (ContourSettings *contourSet : contourSetList)
    {
        delete contourSet;
    }
}


MNWP2DSectionActorVariable::ContourSettings::ContourSettings(
        MNWP2DSectionActorVariable *actorVar, MActor *actor, const uint8_t index,
        bool enabled, double thickness, bool useTransferFunction,
        const QColor &colour, bool labelsEnabled, const QString &levelsString)
        : levels(QVector<double>()),
          startIndex(0),
          stopIndex(0),
          parent(actorVar)
{
    QString propertyTitle = QString("contour set #%1").arg(index  + 1);
    this->enabledProp = MBoolProperty(propertyTitle, enabled);
    this->enabledProp.setConfigKey("enabled");
    this->enabledProp.setConfigGroup(propertyTitle);
    this->enabledProp.registerValueCallback([=]()
    {
        parent->updateContourLabels();
        actor->emitActorChangedSignal();
    });

    levelsProp = MStringProperty("Levels", levelsString);
    levelsProp.setConfigKey("levels");
    levelsProp.registerValueCallback([=]()
    {
        QString cLevelStr = levelsProp.value();
        parent->parseContourLevelString(cLevelStr, this);

        if (actor->suppressActorUpdates()) return;

        parent->contourValuesUpdateEvent(this);
        actor->emitActorChangedSignal();
    });
    this->enabledProp.addSubProperty(levelsProp);

    this->thicknessProp = MDoubleProperty("Thickness", thickness);
    this->thicknessProp.setConfigKey("thickness");
    this->thicknessProp.setMinMax(0.1, 10.0);
    this->thicknessProp.setDecimals(2);
    this->thicknessProp.setStep(0.1);
    this->thicknessProp.registerValueCallback(actor, &MActor::emitActorChangedSignal);
    this->enabledProp.addSubProperty(this->thicknessProp);

    this->useTFProp = MBoolProperty("Use transfer function", useTransferFunction);
    this->useTFProp.setConfigKey("use_transfer_function");
    this->useTFProp.registerValueCallback(actor, &MActor::emitActorChangedSignal);
    this->enabledProp.addSubProperty(this->useTFProp);

    this->colourProp = MColorProperty("Colour", colour);
    this->colourProp.setConfigKey("colour");
    this->colourProp.registerValueCallback(actor, &MActor::emitActorChangedSignal);
    this->enabledProp.addSubProperty(this->colourProp);

    // Contour labels are only implemented for horizontal cross-section actors.
    if (dynamic_cast<MNWPHorizontalSectionActor*>(actor))
    {
        this->labelsEnabledProp = MBoolProperty("Labels", labelsEnabled);
        this->labelsEnabledProp.setConfigKey("labels_enabled");
        this->labelsEnabledProp.registerValueCallback([=](){

            parent->updateContourLabels();
            actor->emitActorChangedSignal();
        });
        this->enabledProp.addSubProperty(this->labelsEnabledProp);
    }

    removeProp = MButtonProperty("Remove", "Remove");
    removeProp.registerValueCallback([=]()
    {
        if (parent->removeContourSet(this))
        {
            actor->emitActorChangedSignal();
        }
    });
    this->enabledProp.addSubProperty(this->removeProp);

    auto *removeAction = new QAction("Remove");

    connect(removeAction, &QAction::triggered, [=](bool checked)
    {
        if (parent->removeContourSet(this))
        {
            actor->emitActorChangedSignal();
        }
    });

    this->enabledProp.addContextMenuAction(removeAction);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWP2DSectionActorVariable::initialize()
{
    if (imageUnitTargetGrid >= 0)
        actor->releaseImageUnit(imageUnitTargetGrid);
    if (textureUnitTargetGrid >= 0)
        actor->releaseTextureUnit(textureUnitTargetGrid);

    imageUnitTargetGrid = actor->assignImageUnit();
    textureUnitTargetGrid = actor->assignTextureUnit();

    MNWPActorVariable::initialize();
}


void MNWP2DSectionActorVariable::saveConfiguration(QSettings *settings)
{
    MNWPActorVariable::saveConfiguration(settings);

    // Save number of contour sets to restore before loading properties.
    settings->setValue("numContourSets", contourSetList.size());
}


void MNWP2DSectionActorVariable::loadConfiguration(QSettings *settings)
{
    MNWPActorVariable::loadConfiguration(settings);

    // Delete current contour sets.
    for (ContourSettings *contourSet : contourSetList)
    {
        delete contourSet;
    }
    contourSetList.clear();

    // Add all contour sets so that they can be loaded by the property tree.
    int numContourSet = settings->value("numContourSets").toInt();
    for (int i = 0; i < numContourSet; i++)
    {
        // Adds an empty contour set.
        addContourSet();
    }

    // If configuration file has no contour set setting defined, add one so
    // that at least one contour sets setting is available.
    if (numContourSet == 0)
    {
        addContourSet();
    }
}


void MNWP2DSectionActorVariable::loadConfigurationPrior_V_1_14(
        QSettings *settings)
{
    MNWPActorVariable::loadConfigurationPrior_V_1_14(settings);

    // Delete current contour sets.
    for (ContourSettings *contourSet : contourSetList)
    {
        delete contourSet;
    }
    contourSetList.clear();

    // Add all contour sets so that they can be loaded by the property tree.
    int numContourSet = settings->value("numContourSets").toInt();
    for (int i = 0; i < numContourSet; i++)
    {
        // Adds an empty contour set.
        addContourSet();
    }

    QString renderModeName =
            settings->value("renderMode", "disabled").toString();
    RenderMode::Type renderMode = stringToRenderMode(renderModeName);

    // Print message if render mode name is no defined and set render mode to
    // disabled.
    if (renderMode == RenderMode::Invalid)
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(QString("Error reading configuration file: "
                               "Could not find render mode '%1'.\n"
                               "Setting render mode to 'disabled'.").arg(renderModeName));
        msgBox.exec();

        renderMode = stringToRenderMode(QString("disabled"));
    }

    renderSettings.renderModeProp.setValue(renderMode);

    // Contour Sets.
    //================
    numContourSet = settings->beginReadArray("contourSet");
    renderSettings.contoursUseTFProp = settings->value("useTransferFunction",
                                                       false).toBool();
    for (int i = 0; i < numContourSet; i++)
    {
        settings->setArrayIndex(i);
        bool enabled = settings->value("enabled", true).toBool();
        QString levels = settings->value("levels", "").toString();
        double thickness = settings->value("thickness", 1.5).toDouble();
        bool useTF = settings->value("useTF", false).toBool();
        QColor colour = settings->value("colour",
                                        QColor(0, 0, 0, 255)).value<QColor>();
        bool labelsEnabled = settings->value("labelsEnabled", false).toBool();
        addContourSet(enabled, thickness, useTF, colour, labelsEnabled, levels);
    }
    settings->endArray();

    // Thin and thick contour lines. (For compatibility with version < 1.2)
    //=====================================================================
    QVersionNumber configVersion = readConfigVersion(settings);
    if (configVersion < QVersionNumber(1, 2))
    {
        QString levels =
                settings->value("thinContourLevels").toString();

        double thickness =
                settings->value("thinContourThickness", 1.2).toDouble();
        QColor colour = settings->value("thinContourColour",
                                        QColor(0, 0, 0, 255)).value<QColor>();

        addContourSet(true, thickness, false, colour, false, levels);


        levels = settings->value("thickContourLevels").toString();
        thickness = settings->value("thickContourThickness", 2.).toDouble();
        colour = settings->value("thickContourColour",
                                 QColor(0, 0, 0, 255)).value<QColor>();

        addContourSet(true, thickness, false, colour, false, levels);

        numContourSet = 2;
    }

    // If configuration file has no contour set setting defined, add one so
    // that at least one contour sets setting is available.
    if (numContourSet == 0)
    {
        addContourSet();
    }
}


bool MNWP2DSectionActorVariable::hasData() const
{
    // Check textureTagetGrid too because it is not set at the same time as
    // textureDataField. Otherwise, hasData might return true even if
    // textureTargetGrid is still null, resulting in a SegFault when rendered.
    return MNWPActorVariable::hasData() && textureTargetGrid != nullptr;
}


QString MNWP2DSectionActorVariable::renderModeToString(
        RenderMode::Type renderMode)
{
    switch (renderMode)
    {
    case RenderMode::Disabled:
        return QString("disabled");
    case RenderMode::FilledContours:
        return QString("filled contours");
    case RenderMode::PseudoColour:
        return QString("pseudo colour");
    case RenderMode::LineContours:
        return QString("line contours");
    case RenderMode::FilledAndLineContours:
        return QString("filled and line contours");
    case RenderMode::PseudoColourAndLineContours:
        return QString("pcolour and line contours");
    case RenderMode::TexturedContours:
        return QString("textured contours");
    case RenderMode::FilledAndTexturedContours:
        return QString("filled and textured contours");
    case RenderMode::LineAndTexturedContours:
        return QString("line and textured contours");
    case RenderMode::PseudoColourAndTexturedContours:
        return QString("pcolour and textured contours");
    case RenderMode::FilledAndLineAndTexturedContours:
        return QString("filled, line and textured contours");
    case RenderMode::PseudoColourAndLineAndTexturedContours:
        return QString("pcolour and line and textured contours");
    default:
        return QString("");
    }
}


MNWP2DSectionActorVariable::RenderMode::Type
MNWP2DSectionActorVariable::stringToRenderMode(QString renderModeName)
{
    // NOTE: Render mode identification was changed in Met.3D version 1.1. For
    // compatibility with version 1.0, the old numeric identifiers are
    // considered here as well.
    if (renderModeName == QString("disabled")
            || renderModeName == QString("0")) // compatibility with Met.3D 1.0
    {
        return RenderMode::Disabled;
    }
    else if (renderModeName == QString("filled contours")
             || renderModeName == QString("1"))
    {
        return RenderMode::FilledContours;
    }
    else if (renderModeName == QString("pseudo colour")
             || renderModeName == QString("2"))
    {
        return RenderMode::PseudoColour;
    }
    else if (renderModeName == QString("line contours")
             || renderModeName == QString("3"))
    {
        return RenderMode::LineContours;
    }
    else if (renderModeName == QString("filled and line contours")
             || renderModeName == QString("4"))
    {
        return RenderMode::FilledAndLineContours;
    }
    else if (renderModeName == QString("pcolour and line contours")
             || renderModeName == QString("5"))
    {
        return RenderMode::PseudoColourAndLineContours;
    }
    else
    {
        return RenderMode::Invalid;
    }
}


void MNWP2DSectionActorVariable::addContourSet(
        const bool enabled, const double thickness, const bool useTF,
        const QColor& colour, const bool labelsEnabled, const QString& levelString)
{
    // TODO (tv, 15May2024): IMPORTANT, what should happen if an object holding
    //  properties is copied. Should the properties be copied too? How should they be copied?
    auto *contourSet = new ContourSettings(this, actor, contourSetList.size(),
                                           enabled,thickness, useTF, colour,
                                           labelsEnabled, levelString);
    contourSetList.append(contourSet);
    renderSettings.contourSetGroupProp.addSubProperty(
                contourSetList.back()->enabledProp);
    parseContourLevelString(levelString, contourSetList.back());

}


bool MNWP2DSectionActorVariable::removeContourSet(ContourSettings *settings)
{
    if (!contourSetList.contains(settings)) return false;
    return removeContourSet(contourSetList.indexOf(settings));
}


bool MNWP2DSectionActorVariable::removeContourSet(int index)
{
    // Avoid removing all contour sets.
    if (contourSetList.size() > 1)
    {
        // Don't ask for confirmation during application start.
        if (MSystemManagerAndControl::getInstance()->applicationIsInitialized())
        {
            QMessageBox yesNoBox;
            yesNoBox.setWindowTitle("Delete contour set");
            yesNoBox.setText(QString("Do you really want to delete "
                                     "contour set #%1").arg(index + 1));
            yesNoBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
            yesNoBox.setDefaultButton(QMessageBox::No);

            if (yesNoBox.exec() != QMessageBox::Yes)
            {
                return false;
            }
        }

        ContourSettings *contourSet = contourSetList[index];
        // Redraw is only needed if contour sets to delete are enabled.
        bool needsRedraw = contourSet->enabledProp;
        contourSet->enabledProp = false;
        renderSettings.contourSetGroupProp.removeSubProperty(
                    contourSet->enabledProp);
        contourSetList.remove(index);

        QString text = contourSet->enabledProp.getName();
        text = text.mid(0, text.indexOf("#") + 1);
        // Rename contour sets after deleted contour sets to
        // close gap left by deleted contour sets.
        for (; index < contourSetList.size(); index++)
        {
            contourSetList[index]->enabledProp.setName(
                        text + QString::number(index + 1));
            contourSetList[index]->enabledProp.setConfigGroup(contourSetList[index]->enabledProp.getName());
        }
        delete contourSet;
        return needsRedraw;
    }
    else
    {
        // Display error message if user wants to delete last contour set.
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText("At least one contour set needs to be present.");
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
        return false;
    }
}


void MNWP2DSectionActorVariable::setRenderMode(
        MNWP2DSectionActorVariable::RenderMode::Type mode)
{
    renderSettings.renderMode = mode;
    renderSettings.renderModeProp = int(mode);
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

bool MNWP2DSectionActorVariable::parseContourLevelString(
        const QString& cLevelStr, ContourSettings *contours)
{
    QVector<double> *contourSet = &(contours->levels);
    contours->startIndex = 0;
    contours->stopIndex = 0;
    // Clear the current list of contour sets; if cLevelStr does not match
    // any accepted format no contours are drawn.
    contourSet->clear();

    // Empty strings, i.e. no contour lines, are accepted.
    if (cLevelStr.isEmpty()) return true;

    // Match strings of format "[0,100,10]" or "[0.5,10,0.5]".
    QRegExp rxRange("^\\[([\\-|\\+]*\\d+\\.*\\d*),([\\-|\\+]*\\d+\\.*\\d*),"
                    "([\\-|\\+]*\\d+\\.*\\d*)\\]$");
    // Match strings of format "1,2,3,4,5" or "0,0.5,1,1.5,5,10" (number of
    // values is arbitrary).
    QRegExp rxList("^([\\-|\\+]*\\d+\\.*\\d*,*)+$");

    if (rxRange.indexIn(cLevelStr) == 0)
    {
        QStringList rangeValues = rxRange.capturedTexts();

        bool ok;
        double from = rangeValues.value(1).toDouble(&ok);
        double to   = rangeValues.value(2).toDouble(&ok);
        double step = rangeValues.value(3).toDouble(&ok);

        if (step > 0)
            for (double d = from; d <= to; d += step) *contourSet << d;
        else if (step < 0)
            // Sort iso values by ascending order (reason see below).
            for (double d = to; d <= from; d -= step) *contourSet << d;

        contours->stopIndex = contourSet->size();
        return true;
    }
    else if (rxList.indexIn(cLevelStr) == 0)
    {
        QStringList listValues = cLevelStr.split(",");

        bool ok;
        for (int i = 0; i < listValues.size(); i++)
            *contourSet << listValues.value(i).toDouble(&ok);

        // Sort contour levels to avoid all levels disappearing if the user
        // ends the sequence with a iso value smaller than the minimum value of
        // the data field or starts the sequence with a level greater than the
        // maximum.
        std::sort(contourSet->begin(), contourSet->end());
        contours->stopIndex = contourSet->size();
        return true;
    }

    // No RegExp could be matched.
    return false;
}


/******************************************************************************
*******************************************************************************/
/******************************************************************************
*******************************************************************************/

/******************************************************************************
***                  MNWP2DHorizontalActorVariable                          ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWP2DHorizontalActorVariable::MNWP2DHorizontalActorVariable(
        MNWPMultiVarActor *actor)
    : MNWP2DSectionActorVariable(actor),
      spatialTransferFunction(nullptr),
      textureUnitSpatialTransferFunction(-1),
      llcrnrlon(0.),
      llcrnrlat(0.),
      urcrnrlon(0.),
      urcrnrlat(0.),
      shiftForWesternLon(0.f)
{
    assert(actor != nullptr);

    QStringList renderModeNames = renderSettings.renderModeProp.getEnumNames();
    renderModeNames << "textured contours"
                    << "filled and textured contours"
                    << "line and textured contours"
                    << "pcolour and textured contours"
                    << "filled, line and textured contours"
                    << "pcolour and line and textured contours";
    renderSettings.renderModeProp.setEnumNames(renderModeNames);

    spatialTransferFunctionProp = MSpatial1DTransferFunctionProperty(
            "Textured transfer function");
    spatialTransferFunctionProp.setConfigKey("textured_transfer_function");
    spatialTransferFunctionProp.registerValueCallback([=]()
    {
        if (setSpatialTransferFunctionFromProperty())
        {
            actor->emitActorChangedSignal();
        }
    });
    int index = varRenderingGroupProp.subPropertyCount() - 1;
    varRenderingGroupProp.insertSubProperty(index, spatialTransferFunctionProp);

    contourLabelSuffixProp = MStringProperty("Contour label suffix", "");
    contourLabelSuffixProp.setConfigKey("contour_label_suffix");
    contourLabelSuffixProp.registerValueCallback([=]()
    {
        updateContourLabels();
        actor->emitActorChangedSignal();
    });
    varRenderingGroupProp.addSubProperty(contourLabelSuffixProp);
}


MNWP2DHorizontalActorVariable::~MNWP2DHorizontalActorVariable()
{
    if (textureUnitSpatialTransferFunction >=0)
        actor->releaseTextureUnit(textureUnitSpatialTransferFunction);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWP2DHorizontalActorVariable::initialize()
{
    MNWP2DSectionActorVariable::initialize();

    if (textureUnitSpatialTransferFunction >=0)
        actor->releaseTextureUnit(textureUnitSpatialTransferFunction);

    textureUnitSpatialTransferFunction = actor->assignTextureUnit();

    setSpatialTransferFunctionFromProperty();
}


void MNWP2DHorizontalActorVariable::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MNWP2DSectionActorVariable::loadConfigurationPrior_V_1_14(settings);

    // Load rendering properties.
    // ==========================
    QString stfName = settings->value("spatialTransferFunction", "None").toString();
    while (!setSpatialTransferFunction(stfName))
    {
        if (!MTransferFunction::loadMissingTransferFunction(
                stfName, MSpatial1DTransferFunction::staticActorType(),
                "Variable ", variableName, settings))
        {
            break;
        }
    }

    contourLabelSuffixProp = settings->value("contourLabelSuffix").toString();
}


void MNWP2DHorizontalActorVariable::computeRenderRegionParameters(
        double llcrnrlon, double llcrnrlat,
        double urcrnrlon, double urcrnrlat)
{
    // Copy bbox coordinates to member variables.
    this->llcrnrlon = llcrnrlon;
    this->llcrnrlat = llcrnrlat;
    this->urcrnrlon = urcrnrlon;
    this->urcrnrlat = urcrnrlat;

    bool gridIsCyclic = grid->gridIsCyclicInLongitude();

    // 1A) Find longitudinal index "i0" in data grid that corresponds to
    //     the WESTmost rendered grid point.
    // ================================================================

    // Longitudes stored in ascending order.
    double shiftLon = grid->lons[0];
    float westBBoxBorderRelativeToWestDataBorder = llcrnrlon - shiftLon;
    float eastBBoxBorderRelativeToWestDataBorder = urcrnrlon - shiftLon;

//WORKAROUND -- Usage of M_LONLAT_RESOLUTION defined in mutil.h
    // NOTE (mr, Dec2013): Workaround to fix a float accuracy problem
    // occuring with some NetCDF data files converted from GRIB with
    // netcdf-java): For example, such longitude arrays can occur:
    // -18, -17, -16, -15, -14, -13, -12, -11, -10, -9.000004, -8.000004,
    // The latter should be equal to -9.0, -8.0 etc. The inaccuracy causes
    // wrong indices below, hence we compare to this absolute epsilon to
    // determine equality of two float values.
    // THIS WORKAROUND NEEDS TO BE REMOVED WHEN HIGHER RESOLUTIONS THAN 0.00001
    // ARE HANDLED BY MET.3D.
    // Cf. http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
    // for potentially better solutions.

    // Find the first lon index larger than llcrnrlon.
    for (i0 = 0; i0 < grid->nlons; i0++)
    {
//        LOG4CPLUS_DEBUG_FMT(mlog, "i0=%i lons=%.10f %.10f NMOD1=%.10f NMOD2=%.10f",
//                            i0, grid->lons[i0], float(grid->lons[i0]),
//                            NMOD(grid->lons[i0]-shiftLon, 360.),
//                            NMOD(llcrnrlon-shiftLon, 360.));
        if (MMOD(grid->lons[i0] - shiftLon, 360.) + M_LONLAT_RESOLUTION
                >= MMOD(westBBoxBorderRelativeToWestDataBorder, 360.)) break;
    }
    // Handle overshooting for non-cyclic grids (otherwise i0 = grid->nlons
    // if the bounding box is east of the grid domain).
    if (!gridIsCyclic) i0 = std::min(i0, grid->nlons-1);

    // 1B) Find longitudinal index "i1" in data grid that corresponds to
    //     the EASTmost rendered grid point.
    // ================================================================

    // Find the last lon index smaller than urcrnrlon.
    unsigned int i1;
    for (i1 = grid->nlons - 1; i1 > 0; i1--)
    {
        if (MMOD(grid->lons[i1] - shiftLon, 360.)
                <= MMOD(eastBBoxBorderRelativeToWestDataBorder, 360.)) break;
    }

    // 2) Find lat indices "j0"/"j1" that corresponds to SOUTHmost/NORTHmost
    //    grid points.
    // =====================================================================

    // Latitude is never cyclic, hence no modulo is required here.
    for (j0 = 0; j0 < grid->nlats; j0++)
    {
        if (grid->lats[j0] <= urcrnrlat) break;
    }
    int j1;
    for (j1 = grid->nlats - 1; j1 > 0; j1--)
    {
        if (grid->lats[j1] >= llcrnrlat) break;
    }

    // 3) Determine the number "nlons" of longitudes to be rendered. Special
    // care has to be taken as data grid may need to be rendered multiple
    // times or can fall apart into disjoint regions, depending on bbox.
    // =====================================================================

    // Possible cases:
    //  a) data region is completely contained in bbox
    //     --> nlons = num. grid points of data grid
    //  b) bbox cuts out part of data region
    //     --> nlons = num. grid points in displayed region
    //  c) bbox is large enough in lon so that data region falls apart into
    //     disjoint regions
    //     --> nlons = num. grid points in western region + num. grid points
    //         in eastern region
    //  d) bbox is >360deg in lon and (parts) of the data region are repeated
    //     --> nlons = sum of grid points in all regions

    // Mapped to longitudes in the range [0..360), eastern bbox border is
    // east of western bbox border...
    if (MMOD(westBBoxBorderRelativeToWestDataBorder, 360.)
            <= MMOD(eastBBoxBorderRelativeToWestDataBorder, 360.))
    {
        // One region, no disjoint parts.
        nlons = i1 - i0 + 1;
    }
    // ... or eastern bbox border is WEST of western bbox border.
    else
    {
        // Treat west and east subregions separately. Eastern subregion goes
        // from i0 to nlons (nlons = grid->nlons - i0), western subregion from
        // 0 to i1 (nlons = i1 + 1).
        nlons = i1 + grid->nlons - i0 + 1;
    }

    // Since the modulo operation maps to a domain of width 360, it is necessary
    // to add the missing width if the domain is larger than 360.
    nlons += floor((urcrnrlon - llcrnrlon) / 360.) * grid->nlons;

    // Compute initial shift to place the grid at the correct position during
    // rendering. It contains a multiple of 360.
    shiftForWesternLon = float(floor((llcrnrlon - grid->lons[0]) / 360.) * 360.f);


    // Bianca's explanation for the above equations (sorry, only in German...):
    // ------------------------------------------------------------------------
    // Dies funktioniert auch, falls die westlichste und oestlichste Teilregionen
    // fuer sich genommen jeweils nur ein Ausschnitt der Gitterregion sind
    // (eine Teilregion ergibt sich hierbei aus der Begrenzung durch die
    // Boundingbox und einer Aufteilung des gesamten Raums in disjunkte
    // Teilregionen mit einer Breite von 360deg und einer westlichen Grenze
    // entsprechend der westlichen Grenze plus ein (positives/negatives)
    // Vielfaches von 360deg). Grundsaetzlich laesst berechnet sich nlons in diesem
    // Fall nach folgender Formel:
    //    nlonsWestPart + nlonsEastPart + nlonsGrid * numSubregions
    //  mit (nlonsWestPart = grid->nlons - i0) und (nlonsEastPart = i1 + 1) folgt:
    //   (i1 + 1 + grid->nlons - i0) + (grid->nlons * numSubregions)
    // Wobei numSubregions die Anzahl der disjunkten Teilregionen angibt, die
    // vollstaendig in der Boundingbox enthalten sind.
    // Wird die oestliche Grenze westlich der westlichen Grenze abgebildet, so
    // gilt:
    //   (i1 < i0) -> (i1 - i0 <= -1) -> (i1 - i0 + grid->nlons) < grid->nlons
    // Da wiederum gilt, dass in einer Boundingboxregion von 360deg Breite, die
    // Gitterregion ein Mal enthalten ist, muss der Abstand zwischen der
    // westlichen und oestlichen Boundingboxgrenze (abzueglich 360deg fuer jede
    // vollstaendig enthaltenen Teilregion) in diesem Fall kleiner als 360deg sein.
    // Wiederrum gilt:
    //   (i1 >= i0) -> (i1 - i0 >= 0) -> (i1 - i0 + grid->nlons) >= grid->nlons
    // Der Umkehrschluss zeigt, dass, wenn die oestliche Boundingboxgrenze
    // oestlich der westlichen abgebildet wird, der Abstand zwischen den
    // Boundingboxgrenzen (abzueglich der vollstaendig enthaltenen Teilregion)
    // groesser als 360deg sein muss (ausgenommen Boundingbox mit einer
    // Gesammtbreite unter 360deg). Da die Boundingbox abzueglich der Teilregionen
    // groesser als 360deg ist, ergibt sich:
    //    factor = floor((urcrnrlon - llcrnrlon) / 360.) = numSubregions + 1
    // Ausserdem laesst sich die Formel fuer nlons folgendermassen umstellen:
    //    (i1 - i0 + 1) + nlonsGrid * (numSubregions + 1)
    // Da wir factor fuer die Abschaetzung von numSubregions nutzen, koennen wir
    // also fuer den Fall, dass die oestliche Boundingboxgrenze oestlich der
    // westlichen Boundingboxgrenze abgebildet wird, stets (i1 - i0 + 1) zur
    // Berechnung von nlons in einem ersten Schritt nutzen.
    // Fuer Boundingboxbreiten unter 360deg koennen nur dann seperate Teilregionen
    // entstehen, wenn die oestliche Boundingboxgrenze westlich der westlichen
    // Boundingboxgrenze abgebildet wird.

    // 4) Determine the number "nlats" of latitudes to be rendered.
    // ============================================================

    nlats = j1 - j0 + 1;
    if (nlats < 0) nlats = 0;


    // DEBUG output.
    // =============

    LOG4CPLUS_DEBUG(mlog, "Boundingbox/grid-index mapping of "
                    << variableName.toStdString()
                    << ": grid is " << (gridIsCyclic ? "" : "not")
                    << " cyclic; shiftLon = " << shiftLon << "; "
                    << "bbox = (" << llcrnrlon << "/" << llcrnrlat << " -> "
                    << urcrnrlon << "/" << urcrnrlat << "); "
                    << "i = (" << i0 << "--" << i1 << "); j = (" << j0 << "--"
                    << j1 << "); n = (" << nlons << "/" << nlats << ")");
}


void MNWP2DHorizontalActorVariable::updateContourIndicesFromTargetGrid(
        float slicePosition_hPa, ContourSettings *contourSet)
{
    // Download generated grid from GPU via imageStore() in vertex shader
    // and glGetTexImage() here.
    textureTargetGrid->bindToLastTextureUnit();
    glGetTexImage(GL_TEXTURE_2D, 0, GL_RED, GL_FLOAT,
                  targetGrid2D->data); CHECK_GL_ERROR;

    // Number of longitudinal grid points required for rendering.
    int numRequiredDataLons = nlons - 1;
    // Use the sub grid but don't overshoot the grid's nlons (nlons can contain
    // a number larger than grid->nlons). maskRectangularRegion() does not
    // mark the outside region correctly if the provided nlons is larger than
    // the grid's nlons.
    numRequiredDataLons = std::min(numRequiredDataLons, int(grid->nlons - 1));

    // Set the target grid points outside the render domain to
    // MISSING_VALUE, so that min() and max() work correctly.
    targetGrid2D->maskRectangularRegion(
                i0, j0, 0, numRequiredDataLons, nlats-1, 1);

    float tgmin = targetGrid2D->min();
    float tgmax = targetGrid2D->max();

    // Update start and stop indices of contour sets.

    QVector<ContourSettings*> localContourSetList;
    localContourSetList.clear();

    // No contour sets given results in updating all levels available.
    if (contourSet == nullptr)
    {
        for (int i = 0; i < contourSetList.size(); i++)
        {
            localContourSetList.append(contourSetList[i]);
        }
    }
    else
    {
        localContourSetList.append(contourSet);
    }

    for (ContourSettings *contourSet : localContourSetList)
    {
        contourSet->startIndex = 0;
        contourSet->stopIndex  = contourSet->levels.size();

        int i = 0;
        if (!contourSet->levels.isEmpty()
                && tgmin > contourSet->levels.last())
        {
            contourSet->startIndex = contourSet->levels.size();
        }
        else
        {
            for (; i < contourSet->levels.size(); i++)
            {
                if (contourSet->levels[i] >= tgmin)
                {
                    contourSet->startIndex = i;
                    break;
                }
            }
            for (; i < contourSet->levels.size(); i++)
            {
                if (contourSet->levels[i] > tgmax)
                {
                    contourSet->stopIndex = i;
                    break;
                }
            }
        }
    }
    // Update contour labels.
    updateContourLabels();
}


QList<MLabel*> MNWP2DHorizontalActorVariable::getContourLabels(
        bool noOverlapping, MSceneViewGLWidget* sceneView)
{
    // If labels may overlap return the whole list.
    if (!noOverlapping) return contourLabels;

    assert(sceneView != nullptr);

    // Create new empty list of to-render labels.
    QList<MLabel*> renderList;
    renderList.reserve(contourLabels.size());

    // Collect pixel coordinates and pixel size of each selected label.
    QList<QVector3D> contourPixelCoords;
    contourPixelCoords.reserve(contourLabels.size());

    // Loop through all labels and add only those
    // which do not overlap with currently selected labels.
    for (MLabel* label : contourLabels)
    {
        // Get label position in clip space.
        QVector3D pixelPos = sceneView->lonLatPToClipSpace(label->anchor);
        // Viewport transformation.
        const int screenHeight = sceneView->getSceneResolutionHeight();
        const int screenWidth = sceneView->getSceneResolutionWidth();

        pixelPos.setX(pixelPos.x() * (screenWidth / 2.0f) + (screenWidth / 2.0f));
        pixelPos.setY(pixelPos.y() * (screenHeight / 2.0f) + (screenHeight / 2.0f));

        // Compute label width in number of pixels.
        const int labelWidth = label->width / 2;

        // Check if label intersects with other labels.
        bool labelIsOverlapping = false;
        for (QVector3D pixelCoord : contourPixelCoords)
        {
            // Distance between centers should be more than the sum of their half widths
            // Or greater than the label height.
            if (   std::abs(pixelPos.x() - pixelCoord.x()) <= labelWidth + pixelCoord.z()
                && std::abs(pixelPos.y() - pixelCoord.y()) <= label->size)
            {
                labelIsOverlapping = true;
                break;
            }
        }

        // If current label does not overlap, add it to the render list.
        if (!labelIsOverlapping)
        {
            renderList.append(label);
            contourPixelCoords.append(
                        QVector3D(pixelPos.x(), pixelPos.y(), label->width / 2));
        }
    }

    return renderList;
}


MNWP2DSectionActorVariable::RenderMode::Type
MNWP2DHorizontalActorVariable::stringToRenderMode(QString renderModeName)
{
    RenderMode::Type renderMode;

    renderMode = MNWP2DSectionActorVariable::stringToRenderMode(renderModeName);

    // Check if string matched 2D section render mode.
    if (renderMode != RenderMode::Invalid)
    {
        return renderMode;
    }

    // Check if string matches horizontal section render mode and return invalid
    // if not.
    if (renderModeName == QString("textured contours"))
    {
        return RenderMode::TexturedContours;
    }
    else if (renderModeName == QString("filled and textured contours"))
    {
        return RenderMode::FilledAndTexturedContours;
    }
    else if (renderModeName == QString("line and textured contours"))
    {
        return RenderMode::LineAndTexturedContours;
    }
    else if (renderModeName == QString("pcolour and textured contours"))
    {
        return RenderMode::PseudoColourAndTexturedContours;
    }
    else if (renderModeName == QString("filled, line and textured contours"))
    {
        return RenderMode::FilledAndLineAndTexturedContours;
    }
    else if (renderModeName == QString("pcolour and line and textured contours"))
    {
        return RenderMode::PseudoColourAndLineAndTexturedContours;
    }
    else
    {
        return RenderMode::Invalid;
    }
}


bool MNWP2DHorizontalActorVariable::setSpatialTransferFunction(const QString& stfName)
{
    return spatialTransferFunctionProp.setByName(stfName);
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MNWP2DHorizontalActorVariable::dataFieldChangedEvent()
{
    if (gridTopologyMayHaveChanged)
    {
        if (targetGrid2D) delete targetGrid2D;

        // Initialize data object for downloading the GPU-interpolated section grid
        // to the CPU; copy lat/lon fields.
        targetGrid2D = new MRegularLonLatGrid(grid->nlats, grid->nlons);
        targetGrid2D->setLongitudesFrom(grid);
        targetGrid2D->setLatitudesFrom(grid);
        // Set level to slice position if horizontal section.

        // Initialize target grid with slice level if parent actor is HSec Actor.
        auto* hsecActor = dynamic_cast<MNWPHorizontalSectionActor*>(actor);
        if (hsecActor)
        {
            targetGrid2D->levels[0] = hsecActor->getSectionElevation_hPa();
        }

        targetGrid2D->setTextureParameters(GL_R32F, GL_RED, GL_CLAMP_TO_EDGE, GL_NEAREST);
        textureTargetGrid = targetGrid2D->getTexture();

        computeRenderRegionParameters(llcrnrlon, llcrnrlat, urcrnrlon, urcrnrlat);

        gridTopologyMayHaveChanged = false;
    }
}


void MNWP2DHorizontalActorVariable::contourValuesUpdateEvent(
        ContourSettings *levels)
{
    if (targetGrid2D == nullptr) return;

    updateContourIndicesFromTargetGrid(targetGrid2D->levels[0], levels);
}


void MNWP2DHorizontalActorVariable::updateContourLabels()
{
    if (!hasData()) return;

    // Remove all labels from text manager.
    const int contourLabelSize = contourLabels.size();

    MTextManager* tm = MGLResourcesManager::getInstance()->getTextManager();

    for (int i = 0; i < contourLabelSize; ++i)
    {
        tm->removeText(contourLabels.takeLast());
    }

    // Delta between two grid cells.
    const float deltaGrid = grid->lons[1] - grid->lons[0];

    // Collect infos of data.
    const int widthX = std::ceil(std::abs(urcrnrlon - llcrnrlon) / deltaGrid);
    const int widthY = std::ceil(std::abs(urcrnrlat - llcrnrlat) / deltaGrid);

    const int resLon = grid->nlons;
    const int resLat = grid->nlats;

    // Compute current boundary indices in grid.
    int minX = (llcrnrlon - grid->lons[0]) / deltaGrid;
    int maxX = minX + widthX;

    int minY = (grid->lats[0] - urcrnrlat) / deltaGrid;
    int maxY = minY + widthY;

    minX = std::min(std::max(0, minX), resLon - 1);
    minY = std::min(std::max(0, minY), resLat - 1);
    maxX = std::max(std::min(maxX, resLon - 1), minX);
    maxY = std::max(std::min(maxY, resLat - 1), minY);

    // Step incrementation.
    int step = 1;
    int contourSetIndex = 0;

    for (ContourSettings *contourSet : contourSetList)
    {
        // Skip labels of contour sets, which are not enabled or which
        // labels are not enabled.
        if (!(contourSet->enabledProp && contourSet->labelsEnabledProp))
        {
            contourSetIndex++;
            continue;
        }
        // Step along all borders and search for potential iso-contours.
        for (int i = contourSet->startIndex; i < contourSet->stopIndex; ++i)
        {
            float isoValue = contourSet->levels.at(i);

            // Traverse grid downwards.
            for (int j = minY + 1; j <= maxY; j += step)
            {
                checkGridForContourLabel(targetGrid2D, j, minX, 1, 0, isoValue,
                                         contourSetIndex);
                checkGridForContourLabel(targetGrid2D, j, maxX, 1, 0, isoValue,
                                         contourSetIndex);
            }

            // Traverse grid rightwards.
            for (int j = minX + 1; j <= maxX; j += step)
            {
                checkGridForContourLabel(targetGrid2D, minY, j, 0, 1, isoValue,
                                         contourSetIndex);
                checkGridForContourLabel(targetGrid2D, maxY, j, 0, 1, isoValue,
                                         contourSetIndex);
            }
        }
        contourSetIndex++;
    }
}


void MNWP2DHorizontalActorVariable::checkGridForContourLabel(
        const MRegularLonLatGrid* grid,
        const int lat, const int lon,
        const int deltaLat, const int deltaLon,
        const float isoValue, const int index)
{
    // Check if there is any possible isovalue between two grid cells
    if (!isoLineInGridCell(targetGrid2D, lat - deltaLat, lon - deltaLon,
                           lat, lon, isoValue))
    {
        return;
    }

    QVector3D posPrev(grid->lons[lon],
                      grid->lats[lat],
                      targetGrid2D->levels[0]);

    QVector3D posNext(grid->lons[lon-deltaLon],
                      grid->lats[lat-deltaLat],
                      targetGrid2D->levels[0]);

    float valuePrev = targetGrid2D->getValue(lat, lon);
    float valueNext = targetGrid2D->getValue(lat - deltaLat, lon - deltaLon);

    addNewContourLabel(posPrev, posNext, valuePrev, valueNext, isoValue, index);
}


bool MNWP2DHorizontalActorVariable::isoLineInGridCell(
        const MRegularLonLatGrid* grid,
        const int jl, const int il,
        const int jr, const int ir,
        const float isoValue)
{
    bool signPrev = grid->getValue(jl, il) >= isoValue;
    bool signNext = grid->getValue(jr, ir) >= isoValue;

    return signPrev != signNext;
}


void MNWP2DHorizontalActorVariable::addNewContourLabel(
        const QVector3D& posPrev, const QVector3D& posNext,
        const float isoPrev, const float isoNext, const float isoValue,
        const int index)

{
    // Compute interpolant.
    float t = std::abs(isoValue - isoPrev) / std::abs(isoNext - isoPrev);

    // Compute world position of label by linear interpolation.
    QVector3D pos = posPrev * (1 - t) + posNext * t;

    MTextManager* tm = MGLResourcesManager::getInstance()->getTextManager();

    // Create text label and add it to the contour label list with text size 16
    // Add the user-defined suffix to the isoValue text.
    contourLabels.append(
                tm->addText(
                    QString("%1 %2").arg(isoValue).arg(contourLabelSuffixProp),
                    MTextManager::LONLATP, pos.x(), pos.y(), pos.z(),
                    16, contourSetList[index]->colourProp,
                    MTextManager::BASELINECENTRE, true,
                    QColor(255, 255, 255, 200), 0.3)
                );
}


/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

bool MNWP2DHorizontalActorVariable::setSpatialTransferFunctionFromProperty()
{
    spatialTransferFunction = dynamic_cast<MSpatial1DTransferFunction*>(spatialTransferFunctionProp.value());
    if (spatialTransferFunction == nullptr)
    {
        return false;
    }
    return true;
}


/******************************************************************************
*******************************************************************************/
/******************************************************************************
*******************************************************************************/

/******************************************************************************
***                NWPActor_VerticalXSec2D_Variable                         ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWP2DVerticalActorVariable::MNWP2DVerticalActorVariable(
        MNWPMultiVarActor *actor)
    : MNWP2DSectionActorVariable(actor)
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWP2DVerticalActorVariable::dataFieldChangedEvent()
{
    if (gridTopologyMayHaveChanged)
    {
        if (targetGrid2D) delete targetGrid2D;

        targetGrid2D = new MRegularLonLatGrid(1024, grid->nlevs);

        targetGrid2D->setTextureParameters(GL_R32F, GL_RED, GL_CLAMP_TO_EDGE, GL_NEAREST);
        textureTargetGrid = targetGrid2D->getTexture();

        gridTopologyMayHaveChanged = false;
    }

    MNWP2DSectionActorVariable::dataFieldChangedEvent();
    updateVerticalLevelRange(p_bot_hPa, p_top_hPa);
}


void MNWP2DVerticalActorVariable::updateVerticalLevelRange(
        double p_bot_hPa, double p_top_hPa)
{
    // Determine the upper/lower model levels that enclose the range
    // pbot..ptop.
    this->p_bot_hPa = p_bot_hPa;
    this->p_top_hPa = p_top_hPa;

    if (MLonLatHybridSigmaPressureGrid *hgrid =
            dynamic_cast<MLonLatHybridSigmaPressureGrid*>(grid))
    {
        // Min and max surface pressure.
        double psfc_hPa_min = hgrid->getSurfacePressureGrid()->min() / 100.;
        double psfc_hPa_max = hgrid->getSurfacePressureGrid()->max() / 100.;

        // Find the psfc_hPa_max model levels that enclose p_top_hPa..
        int p_top_kLowerPressure, p_top_kUpperPressure;
        hgrid->findEnclosingModelLevels(psfc_hPa_max, p_top_hPa,
                                        &p_top_kLowerPressure,    // use this
                                        &p_top_kUpperPressure);

        // ..and the psfc_hPa_min model levels that enclose p_bot_hPa.
        int p_bot_kLowerPressure, p_bot_kUpperPressure;
        hgrid->findEnclosingModelLevels(psfc_hPa_min, p_bot_hPa,
                                        &p_bot_kLowerPressure,
                                        &p_bot_kUpperPressure);   // use this

        gridVerticalLevelStart =
                std::min(p_top_kLowerPressure, p_bot_kUpperPressure);
        gridVerticalLevelCount =
                abs(p_top_kLowerPressure - p_bot_kUpperPressure);

        LOG4CPLUS_DEBUG(mlog, "\tVariable: " << variableName
                        << ": psfc_min = " << psfc_hPa_min
                        << " hPa, psfc_max = " << psfc_hPa_max
                        << " hPa; vertical levels from " << gridVerticalLevelStart
                        << ", count " << gridVerticalLevelCount);
    }
    else  if (MLonLatAuxiliaryPressureGrid *apgrid =
              dynamic_cast<MLonLatAuxiliaryPressureGrid*>(grid))
    {
        // Min and max surface pressure.
        double pfield_hPa_min = apgrid->getAuxiliaryPressureFieldGrid()->min() / 100.;
        double pfield_hPa_max = apgrid->getAuxiliaryPressureFieldGrid()->max() / 100.;

        gridVerticalLevelStart = 0;
        gridVerticalLevelCount = grid->nlevs;

        LOG4CPLUS_DEBUG(mlog, "\tVariable: " << variableName
                        << ": auxPressureField_min = " << pfield_hPa_min
                        << " hPa, auxPressureField_max = " << pfield_hPa_max
                        << " hPa; vertical levels from " << gridVerticalLevelStart
                        << ", count " << gridVerticalLevelCount);
    }
    else
    {
        // All other leveltypes do not have terrain following vertical
        // coordinates --> vertical levels are the same at every location
        // --> we can use horizontal index 0,0.
        int k_bot = grid->findLevel(0, 0, p_bot_hPa);
        int k_top = grid->findLevel(0, 0, p_top_hPa);
        gridVerticalLevelStart = std::min(k_bot, k_top);
        gridVerticalLevelCount = abs(k_bot - k_top) + 1;
    }
}


void MNWP2DVerticalActorVariable::contourValuesUpdateEvent(
        ContourSettings *levels)
{
    // Update start and stop indices of contour sets.

    QVector<ContourSettings*> localContourSetList;
    localContourSetList.clear();

    // If no contour sets are given, update all levels available.
    if (levels == nullptr)
    {
        for (ContourSettings *i : contourSetList)
        {
            localContourSetList.append(i);
        }
    }
    else
    {
        localContourSetList.append(levels);
    }

    for (ContourSettings *contourSet : localContourSetList)
    {
        contourSet->startIndex = 0;
        contourSet->stopIndex  = contourSet->levels.size();
    }
}


/******************************************************************************
*******************************************************************************/
/******************************************************************************
*******************************************************************************/

/******************************************************************************
***                     MNWP3DVolumeActorVariable                           ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWP3DVolumeActorVariable::MNWP3DVolumeActorVariable(MNWPMultiVarActor *actor)
    : MNWPActorVariable(actor),
      textureMinMaxAccelStructure(nullptr),
      textureUnitMinMaxAccelStructure(-1)
{
}


MNWP3DVolumeActorVariable::~MNWP3DVolumeActorVariable()
{
    if (textureUnitMinMaxAccelStructure >= 0)
        actor->releaseTextureUnit(textureUnitMinMaxAccelStructure);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWP3DVolumeActorVariable::initialize()
{
    textureMinMaxAccelStructure = nullptr;

    if (textureUnitMinMaxAccelStructure >= 0)
        actor->releaseTextureUnit(textureUnitMinMaxAccelStructure);

    textureUnitMinMaxAccelStructure = actor->assignTextureUnit();

    MNWPActorVariable::initialize();
}


void MNWP3DVolumeActorVariable::asynchronousDataAvailableEvent(
        MStructuredGrid *grid)
{
#ifdef ENABLE_RAYCASTER_ACCELERATION
    if (gridType == STRUCTURED_GRID)
    {
        textureMinMaxAccelStructure = grid->getMinMaxAccelTexture3D();
    }
    else
    {
        textureMinMaxAccelStructure = radarGrid->getMinMaxAccelTexture3D();
    }

#endif
}

bool MNWP3DVolumeActorVariable::hasData() const
{
#ifdef ENABLE_RAYCASTER_ACCELERATION
    // Check textureMinMaxAccelStructure too because it is not set at the same
    // time as textureDataField. Otherwise, hasData might return true even if
    // textureMinMaxAccelStructure is still null.
    return MNWPActorVariable::hasData() && textureMinMaxAccelStructure != nullptr;
#else
    return MNWPActorVariable::hasData();
#endif
}


void MNWP3DVolumeActorVariable::releaseDataItems()
{
    // Release currently used data items.
    if (grid)
    {
#ifdef ENABLE_RAYCASTER_ACCELERATION
        grid->releaseMinMaxAccelTexture3D();
        textureMinMaxAccelStructure = nullptr;
#endif
    }

    if (radarGrid)
    {
#ifdef ENABLE_RAYCASTER_ACCELERATION
        radarGrid->releaseMinMaxAccelTexture3D();
        textureMinMaxAccelStructure = nullptr;
#endif
    }

    MNWPActorVariable::releaseDataItems();
}


bool MNWP3DVolumeActorVariable::setTransferFunctionFromProperty()
{
    // Since the shadow of volume raycaster actor depends on the transfer
    // function, it is necessary to trigger an update if the transfer function
    // changes otherwise the shadow won't adapt to the changes.
    if (transferFunction != nullptr
        && actor->getActorType() == MNWPVolumeRaycasterActor::staticActorType())
    {
        disconnect(transferFunction, SIGNAL(actorChanged()),
                   static_cast<MNWPVolumeRaycasterActor*>(actor),
                   SLOT(updateShadow()));
    }

    bool returnValue = MNWPActorVariable::setTransferFunctionFromProperty();

    if (returnValue == true && transferFunction != nullptr
        && actor->getActorType() == MNWPVolumeRaycasterActor::staticActorType())
    {
        connect(transferFunction, SIGNAL(actorChanged()),
                static_cast<MNWPVolumeRaycasterActor*>(actor),
                SLOT(updateShadow()));
    }

    return returnValue;
}

/******************************************************************************
*******************************************************************************/
/******************************************************************************
*******************************************************************************/

/******************************************************************************
***                         MNWPSkewTActorVariable                          ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWPSkewTActorVariable::MNWPSkewTActorVariable(MNWPMultiVarActor *actor)
    : MNWPActorVariable(actor),
      profileVertexBuffer(nullptr)
{
    assert(actor != nullptr);

    // Create and initialise properties.
    // ===============================================

    profileColourProp = MColorProperty("Line colour", QColor(0, 0, 0, 255));
    profileColourProp.setConfigKey("line_colour");
    profileColourProp.registerValueCallback(actor, &MActor::emitActorChangedSignal);
    varRenderingGroupProp.addSubProperty(profileColourProp);

    lineThicknessProp = MDoubleProperty("Line thickness", 2.0);
    lineThicknessProp.setConfigKey("line_thickness");
    lineThicknessProp.setMinMax(0.0, 10.0);
    lineThicknessProp.setDecimals(2);
    lineThicknessProp.setStep(0.1);
    lineThicknessProp.registerValueCallback(actor, &MActor::emitActorChangedSignal);
    varRenderingGroupProp.addSubProperty(lineThicknessProp);
}


MNWPSkewTActorVariable::~MNWPSkewTActorVariable()
{
    if (profileVertexBuffer != nullptr)
    {
        profile.releaseVertexBuffer();
    }

    for (int m : profileVertexBufferAggregation.keys())
    {
        profileAggregation[m].releaseVertexBuffer();
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWPSkewTActorVariable::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MNWPActorVariable::loadConfigurationPrior_V_1_14(settings);
    profileColourProp = settings->value("lineColour").value<QColor>();
    lineThicknessProp = settings->value("lineThickness", 2.).toDouble();
}


void MNWPSkewTActorVariable::dataFieldChangedEvent()
{
    // Remove all shortcut pointers to vertex buffers in case the set of
    // selected members has changed (otherwise removed members would remain
    // in the set).
    for (int m : profileVertexBufferAggregation.keys())
    {
        profileAggregation[m].releaseVertexBuffer();
        profileVertexBufferAggregation.remove(m);
    }

    updateProfile(profile.getLonLatLocation());
}


void MNWPSkewTActorVariable::updateProfile(QVector2D lonLatLocation)
{
    QVector<QVector2D> profileData;
    if (grid != nullptr)
    {
        profileData = grid->extractVerticalProfile(
                    lonLatLocation.x(), lonLatLocation.y());
    }
    profile.updateData(lonLatLocation, profileData);

    if (profileVertexBuffer == nullptr)
    {
        profileVertexBuffer = profile.getVertexBuffer();
    }

    // If multiple member mode is active (ensemble displays, e.g. spaghettis):
    // =======================================================================
    if (ensembleFilterOperation == "MULTIPLE_MEMBERS"
            && gridAggregation != nullptr)
    {
        // For each member grid in the grid aggregation, extract the vertical
        // profile and obtain a vertex buffer.
        for (MStructuredGrid* g : gridAggregation->getGrids())
        {
            profileData = g->extractVerticalProfile(
                        lonLatLocation.x(), lonLatLocation.y());

//NOTE (mr, 2019Apr02): This implementation could be improved by combining
// all profiles into a single vertex buffer. Also see comment in
// MSkewTActor::drawProfiles().

            int m = g->getEnsembleMember();
            profileAggregation[m].updateData(lonLatLocation, profileData);

            if (!profileVertexBufferAggregation.contains(m))
            {
                profileVertexBufferAggregation.insert(
                            m, profileAggregation[m].getVertexBuffer());
            }
        }
    }
}


} // namespace Met3D
