/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPROPERTYLOADINGERRORDIALOG_H
#define MET_3D_MPROPERTYLOADINGERRORDIALOG_H

// standard library imports

// related third party imports
#include <QMessageBox>
#include <QVector>

// local application imports
#include "gxfw/properties/mproperty.h"

// forward declarations

namespace Met3D
{

/**
 * A dialog to display information regarding @c MProperty instances that could not be loaded.
 */
class MPropertyLoadingErrorDialog : QMessageBox
{
public:
    using QMessageBox::setText;

    /**
     * Creates a new property loading error dialog
     * @param properties A list of properties that could not be loaded.
     * @param parent The parent widget.
     */
    explicit MPropertyLoadingErrorDialog(const QVector<MProperty *>& properties, QWidget *parent = nullptr);

    int exec() override;
};

} // Met3D

#endif //MET_3D_MPROPERTYLOADINGERRORDIALOG_H
