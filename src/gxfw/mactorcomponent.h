/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MACTORCOMPONENT_H
#define MACTORCOMPONENT_H

// standard library imports

// related third party imports
#include <QObject>
#include <QSettings>

// local application imports
#include "gxfw/properties/mproperty.h"

namespace Met3D
{

// Forward declaration for linker.
class MActor;
class MSceneViewGLWidget;


/**
 * The MActorComponent class implements a modular actor component.
 * Actor components should be modular parts of actors, that can be added or removed from
 * actors and provide additional functionality for the actor it was added to.
 * Classes that implement this component must have an constructor that takes as its
 * first argument an MActor*.
 */
class MActorComponent : public QObject
{
    Q_OBJECT

// Define MActor as friend, as it needs access to some protected members.
friend class MActor;

public:
    MActorComponent(MActor *parent, QString& name);
    ~MActorComponent() override;

    /**
     * @return The unique ID of this actor component.
     */
    unsigned int getID() const { return id; }

    /**
     * @return The name of this component
     */
    QString getName() const { return name; }

    /**
     * Sets the name of the component.
     * The name does not have to be unique.
     * Sub-classes might need to react to the change via @c onNameChanged().
     * @param newName The new name of the component.
     */
    void setName(const QString &name);

    /**
     * @return The actor that this component is added to.
     */
    MActor *getParentActor() const { return parent; }

    /**
     * @return If the component is enabled or not.
     */
    bool isEnabled() const { return enabled; }

    /**
     * @brief setEnabled Enables or disables this component.
     * @param enabled Whether to enable or disable the component.
     */
    void setEnabled(bool enabled);

    /**
     * @brief onEnabled Called when component enables.
     */
    virtual void onEnabled()
    {
    };

    /**
     * @brief onDisabled Called when component disables.
     */
    virtual void onDisabled()
    {
    };


    /**
     * @brief onNameChanged Called when a component name changes.
     */
    virtual void onNameChanged()
    {
    };


    /**
     * Initialise the component, e.g.create GPU OpenGL resources
     * that can be shared between OpenGL contexts.
     */
    virtual void initializeResources()
    {
    };

    /**
     * Reloads the components glsl shader programs, and generates the needed
     * effect programs, if needed.
     * Will automatically be called from parent after component initialization.
     * The parameter @ref recompile should be used to check, whether the shaders
     * have to be recompiled, or compilation can be skipped, if the effect
     * program already exists.
     * @param recompile Whether the shaders should be recompiled or not.
     */
    virtual void reloadShaderEffects(bool recompile)
    {
        Q_UNUSED(recompile);
    };

    /**
     * @brief renderToCurrentContext Render the component to the current context.
     * @param sceneView Scene view that we currently render to.
     */
    virtual void renderToCurrentContext(MSceneViewGLWidget *sceneView)
    {
        Q_UNUSED(sceneView);
    };

    /**
     * @brief renderToUiLayer Render the component to the current context' UI layer.
     * @param sceneView Scene view that we currently render to.
     */
    virtual void renderToUiLayer(MSceneViewGLWidget *sceneView)
    {
        Q_UNUSED(sceneView);
    };

    /**
     * @brief checkIntersectionWithHandle Returns a handle ID if the clip space coordinate of a handle
     * of this component is within an area around @p clipX and @p clipY.
     * @param sceneView Scene view that called this check.
     * @param clipX Clipspace x coordinate.
     * @param clipY Clipspace y coordinate.
     * @return The handle id or -1.
     */
    virtual int checkIntersectionWithHandle(MSceneViewGLWidget *sceneView,
                                                float clipX, float clipY)
    {
        Q_UNUSED(sceneView);
        Q_UNUSED(clipX);
        Q_UNUSED(clipY);
        return -1;
    }

    /**
     * @brief dragEvent Drags the hande with ID @p handleID to the clip space point (@p clipX,
     * @p clipY).
     * @param sceneView Scene view that called this.
     * @param handleID Dragged handle id
     * @param clipX Clipspace x coordinate
     * @param clipY Clipspace y coordinate
     */
    virtual void dragEvent(MSceneViewGLWidget *sceneView,
                   int handleID, float clipX, float clipY)
    {
        Q_UNUSED(sceneView);
        Q_UNUSED(handleID);
        Q_UNUSED(clipX);
        Q_UNUSED(clipY);
    }

    /**
     * @brief releaseEvent Releases the handle with ID @p handleID.
     * When overriding, also call this.
     * @param sceneView Scene view that called this.
     * @param handleID Released handle id.
     */
    virtual void releaseEvent(MSceneViewGLWidget *sceneView, int handleID)
    {
        dragEventID++;
        Q_UNUSED(sceneView);
        Q_UNUSED(handleID);
    }

    /**
     * @brief addProperties Adds the properties of this component to a property group.
     * @param groupProp Group property from parent actor under which the properties of this component are inserted.
     */
    virtual void addProperties(MProperty *groupProp)
    {
        Q_UNUSED(groupProp);
    }

    /**
     * @brief toggleProperties Enables or disables the properties of this component as requested by the parent actor.
     * @param enable Whether to enable or disable all properties of this component in the parent actor.
     */
    virtual void toggleProperties(bool enable)
    {
        Q_UNUSED(enable);
    }

    /**
     * @brief removeProperties Removes the properties of this component.
     * @param groupProp Group property from parent actor under which the properties of this component are inserted.
     */
    virtual void removeProperties(MProperty *groupProp)
    {
        Q_UNUSED(groupProp);
    }

    /**
     * @brief saveConfiguration Save component specific settings to the @ref QSettings object @p settings.
     * @param settings Settings object to save to.
     */
    virtual void saveConfiguration(QSettings *settings)
    {
        Q_UNUSED(settings);
    }

    /**
     * @brief loadConfiguration Load component specific settings from the @ref QSettings object @p settings.
     * @param settings Settings object to load from.
     */
    virtual void loadConfiguration(QSettings *settings)
    {
        Q_UNUSED(settings);
    }

    /**
     * Load component specific settings that are part of config files created before
     * the property framework implementation done in 1.14.
     * The property framework largely handles the saving and loading of the properties
     * by itself.
     * @param settings The settings object to load from.
     */
    virtual void loadConfigurationPrior_V_1_14(QSettings *settings)
    {
        Q_UNUSED(settings);
    }
public slots:
    /**
     * @brief emitComponentChangedSignal Emits a signal to notify parent actor that a component updated.
     */
    void emitComponentChangedSignal();

signals:
    /**
     * @brief componentChanged Emitted when this component changed and requires a render update.
     */
    void componentChanged();

protected:
    int dragEventID = 0;

private:
    static unsigned int idCounter;

    bool enabled;
    MActor* parent;
    QString name;
    const unsigned int id;
};

}

#endif // MACTORCOMPONENT_H
