/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mscenecontrol.h"

// standard library imports
#include <iostream>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "actors/trajectoryactor.h"
#include "gxfw/properties/mproperty.h"
#include "actors/lightactor.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

QMutex MSceneControl::staticSceneControlAccessMutex;

MSceneControl::MSceneControl(QString name, QWidget *parent)
    : QWidget(parent),
      name(name),
      actorPropertiesBrowser(nullptr),
      blockRedrawRequests(0),
      actorChangeDuringBlock(false)
{
    QMutexLocker scAccessMutexLocker(&staticSceneControlAccessMutex);

    sceneControlLayout = new QVBoxLayout(this);

    actorPropertiesBrowser = new MPropertyTree(this);
    // Set a light yellow as background colour (the colour is taken from the
    // QtDesigner property editor (screenshot and GIMP..)).
    //actorPropertiesBrowser->setBaseLevelColor(QColor(255, 255, 191));

    // Add the actor properties browser to the GUI.
    sceneControlLayout->addWidget(actorPropertiesBrowser);
    setLayout(sceneControlLayout);

    scAccessMutexLocker.unlock();
}


MSceneControl::~MSceneControl()
{
    QMutexLocker scAccessMutexLocker(&staticSceneControlAccessMutex);
    delete actorPropertiesBrowser;
    actorPropertiesBrowser = nullptr;
    delete sceneControlLayout;

    scAccessMutexLocker.unlock();
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

int MSceneControl::addActor(MActor *actor, int index)
{
    // Actor already exists, move to the end of the render queue.
    if (renderQueue.contains(actor))
    {
        removeActor(actor);
    }

//    int insertAtIndex = max(renderQueue.size() - 1, 0);
    int insertAtIndex = renderQueue.size();
    if (index != -1 && renderQueue.size() > index) insertAtIndex = index;

    LOG4CPLUS_INFO(mlog, "Adding actor '" << actor->getName()
                                          << "' to scene '" << name << "'...");

    // Keep track of the connected actors by storing them in a render queue.
    renderQueue.insert(insertAtIndex, actor);

    // Add the item's properties to the property browser.
    MProperty *actorGroupProp = actor->getPropertyGroup();
    actorPropertiesBrowser->insertProperty(insertAtIndex, actorGroupProp);

    // Collect the "actorChanged()" signals of all registered actors in the
    // "actOnActorChange()" slot of this class.
    connect(actor, SIGNAL(actorChanged()), SLOT(onActorChanged()));

    // Tell the actor that it has been added to this scene.
    actor->registerScene(this);

    // Tell the actor to inform this scene about its synchronized elements.
    actor->provideSynchronizationInfoToScene(this);

    if (auto *light = dynamic_cast<MLightActor*>(actor))
    {
        if (!sceneLights.contains(light))
        {
            sceneLights.append(light);
            connect(light, &MLightActor::lightChanged,
                    this, &MSceneControl::sceneLightingChanged);
            emit sceneLightingChanged();
        }
    }

    emit sceneChanged();

    return insertAtIndex; // return index of actor in queue
}

void MSceneControl::removeActor(int id)
{
    MActor* actor = renderQueue.at(id);
    removeActor(actor);
}


void MSceneControl::removeActorByName(const QString& actorName)
{
    MActor* actor = nullptr;

    for (MActor *queuedActor : renderQueue)
    {
        if (queuedActor->getName() == actorName)
        {
            actor = queuedActor;
            break;
        }
    }

    if (!actor) return;

    removeActor(actor);
}


bool MSceneControl::removeActor(MActor *actor)
{
    if (!actor) return false;
    LOG4CPLUS_INFO(mlog, "Removing actor ''" << actor->getName()
                    << "'' from scene " << name);

    actorPropertiesBrowser->removeProperty(actor->getPropertyGroup());

    actor->deregisterScene(this);

    if (auto *light = dynamic_cast<MLightActor*>(actor))
    {
        sceneLights.removeAll(light);
        disconnect(light, &MLightActor::lightChanged,
                this, &MSceneControl::sceneLightingChanged);
        emit sceneLightingChanged();
    }

    renderQueue.removeOne(actor);

    emit sceneChanged();
    return true;
}


int MSceneControl::getActorRenderID(const QString& actorName) const
{
    for (int i = 0; i < renderQueue.size(); ++i)
    {
        if (renderQueue[i]->getName() == actorName)
        {
            return i;
        }
    }

    return -1;
}


void MSceneControl::variableSynchronizesWith(MSyncControl *sync)
{
    syncControlCounter[sync]++;

    connect(sync, SIGNAL(beginSynchronization()),
            this, SLOT(startBlockingRedraws()));
    connect(sync, SIGNAL(endSynchronization()),
            this, SLOT(endBlockingRedraws()));
}


void MSceneControl::variableDeletesSynchronizationWith(MSyncControl *sync)
{
    syncControlCounter[sync]--;

    if (syncControlCounter[sync] == 0)
    {
        disconnect(sync, SIGNAL(beginSynchronization()),
                   this, SLOT(startBlockingRedraws()));
        disconnect(sync, SIGNAL(endSynchronization()),
                   this, SLOT(endBlockingRedraws()));
    }
}


void MSceneControl::registerSceneView(MSceneViewGLWidget *view)
{
    registeredSceneViews.insert(view);
    emit sceneViewAdded();
}


void MSceneControl::unregisterSceneView(MSceneViewGLWidget *view)
{
    registeredSceneViews.remove(view);
}


void MSceneControl::setSingleInteractionActor(MActor *actor)
{
    for (MSceneViewGLWidget* view : registeredSceneViews)
        view->setSingleInteractionActor(actor);
}


void MSceneControl::collapseActorPropertyTree(MActor *actor)
{
    actor->getPropertyGroup()->collapse(actorPropertiesBrowser);
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MSceneControl::onActorChanged()
{
    if (blockRedrawRequests == 0)
    {
        // If no blocking requests are currently registered, emit a signal to
        // redraw the scene.
        emit sceneChanged();
    }
    else
        // Remember that something has changed while redrawing was blocked.
        actorChangeDuringBlock = true;
}


void MSceneControl::reloadActorShaders()
{
    for (MActor* &actor : renderQueue)
    {
        actor->reloadAllShaderEffects();
    }

    emit sceneChanged(); // trigger redraw of the scene
}


void MSceneControl::startBlockingRedraws()
{
    // Increase the number of blocking requests. See notes 23Jan2013.
    blockRedrawRequests++;
}


void MSceneControl::endBlockingRedraws()
{
    // Decrease the number of blocking requests.
    if (blockRedrawRequests > 0) blockRedrawRequests--;

    // If something has changed since the last call to start/end blocking,
    // redraw the scene.
    if (actorChangeDuringBlock)
    {
        actorChangeDuringBlock = false;
        emit sceneChanged();
    }
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

} // namespace Met3D
