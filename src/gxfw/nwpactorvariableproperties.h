/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2023 Marc Rautenhaus [*, previously +]
**  Copyright 2020-2023 Andreas Beckert [*]
**  Copyright 2024      Susanne Fuchs [*]
**  Copyright 2024      Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef NWPACTORVARIABLEPROPERTIES_H
#define NWPACTORVARIABLEPROPERTIES_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/datarequest.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mbuttonproperty.h"
#include "gxfw/properties/mstringproperty.h"


namespace Met3D
{

class MNWPActorVariable;

struct MPropertyType
{
    enum ChangeNotification { IsoValue = 0, VerticalRegrid = 1 };
};


/**
  @brief Abstract base class for group properties for pipeline parameters. This
  is the "GUI" part of pipeline modules (for a pipeline module that requires
  specific parameters, implement corresponding GUI properties in a derived
  class).

  Derived classes have to be registered in the @ref MRequestPropertiesFactory
  @ref updateProperties() method. A @ref MNWPActorVariable can pass its
  properties and the keys required by its current data source to this method.
  Properties corresponding to the required keys are then added to the actor
  variable's property list.
 */
class MRequestProperties
{
public:
    explicit MRequestProperties(MNWPActorVariable *actorVar);
    virtual ~MRequestProperties() = default;

    /**
     Adds key/value pairs corresponding to the properties of this class.
     */
    virtual void addToRequest(MDataRequestHelper *rh) = 0;

    /**
      Implement this method to react to changes of actor properties.
      Compare to @ref MNWPActorVariable::actorPropertyChangeEvent().
     */
    virtual void actorPropertyChangeEvent(
            MPropertyType::ChangeNotification ptype, void *value)
    { Q_UNUSED(ptype); Q_UNUSED(value); }

    /**
     * Load request property specific that are part of config files created before
     * the property framework implementation done in 1.14.
     * The property framework largely handles the saving and loading of the properties
     * by itself.
     * @param settings The settings object to load from.
     */
    virtual void loadConfigurationPrior_V_1_14(QSettings *settings) { Q_UNUSED(settings); }

protected:
    MNWPActorVariable *actorVariable;

};


/**
  Factory for @ref MRequestProperties. Each @ref MNWPActorVariable owns an
  instance of this factory to update its properties.

  @note New classes derived from @ref MRequestProperties need to be added
  to @ref updateProperties().
 */
class MRequestPropertiesFactory
{
public:
    explicit MRequestPropertiesFactory(MNWPActorVariable *actorVar);

    void updateProperties(QList<MRequestProperties*> *propertiesList,
                          const QStringList& keysRequiredByPipeline);

protected:
    template<typename T> void updateTypedProperties(
            const QStringList& keysHandledByType,
            QList<MRequestProperties*> *propertiesList,
            const QStringList& keysRequiredByPipeline);

    MNWPActorVariable *actorVariable;
};


/******************************************************************************
***                      SPECIFIC IMPLEMENTATIONS                           ***
*******************************************************************************/

/**
  Vertical regridding.
 */
class MVerticalRegridProperties : public MRequestProperties
{
public:
    explicit MVerticalRegridProperties(MNWPActorVariable *actorVar);
    ~MVerticalRegridProperties() override;

    void addToRequest(MDataRequestHelper *rh) override;

    void actorPropertyChangeEvent(
            MPropertyType::ChangeNotification ptype, void *value) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

protected:
    MProperty     groupProp;
    MEnumProperty regridModeProp;
    QString       regridMode;
    MBoolProperty enableBroadcastProp;

    bool ignorePropertyChangeEvents;
};


/**
  Grid specification for BL trajectories.
 */
class MTrajectoryGriddingProperties : public MRequestProperties
{
public:
    explicit MTrajectoryGriddingProperties(MNWPActorVariable *actorVar);
    ~MTrajectoryGriddingProperties() override;
    void addToRequest(MDataRequestHelper *rh) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

protected:
    MProperty groupProp;
    MButtonProperty applySettingsProp;
    MBoolProperty scaleParcelThicknessProp;

    MFloatProperty westernLonProp;
    MFloatProperty deltaLonProp;
    MIntProperty numLonProp;

    MFloatProperty northerLatProp;
    MFloatProperty deltaLatProp;
    MIntProperty numLatProp;

    MEnumProperty verticalGridTypeProp;
    MFloatProperty bottomPressureProp;
    MFloatProperty topPressureProp;
    MIntProperty numPressureProp;
};


/**
  Probability region contribution.
 */
class MTrajectoryThinOutProperties : public MRequestProperties
{
public:
    explicit MTrajectoryThinOutProperties(MNWPActorVariable *actorVar);
    ~MTrajectoryThinOutProperties() override;

    void addToRequest(MDataRequestHelper *rh) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

protected:
    MProperty groupProp;
    MButtonProperty applySettingsProp;
    MBoolProperty enableThinOutProp;
    MIntProperty strideLonProp;
    MIntProperty strideLatProp;
    MIntProperty strideLevProp;
};


/**
  Probability region contribution.
 */
class MProbabilityRegionProperties : public MRequestProperties
{
public:
    explicit MProbabilityRegionProperties(MNWPActorVariable *actorVar);
    ~MProbabilityRegionProperties() override;

    void addToRequest(MDataRequestHelper *rh) override;

    void actorPropertyChangeEvent(
            MPropertyType::ChangeNotification ptype, void *value) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

protected:
    MProperty groupProp;
    MFloatProperty probabilityRegionIsovalueProp;
};


/**
  Smooth filter.
  */
class MSmoothProperties : public MRequestProperties
{
public:
    explicit MSmoothProperties(MNWPActorVariable *actorVar);
    ~MSmoothProperties() override;

    void addToRequest(MDataRequestHelper *rh) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    // Smooth modes as enum item. Add here your own implemented smooth modes.
    typedef enum {
        DISABLE_FILTER = 0,
        GAUSS_DISTANCE = 1,
        BOX_BLUR_DISTANCE_FAST = 2,
        UNIFORM_WEIGHTED_GRIDPOINTS = 3,
        GAUSS_GRIDPOINTS = 4,
        BOX_BLUR_GRIDPOINTS_SLOW = 5,
        BOX_BLUR_GRIDPOINTS_FAST = 6,
    } SmoothModeTypes;

    // Types of boundary handleing in smooth filter.
    typedef enum {
        CONSTANT = 0,
        NANPADDING = 1,
        SYMMETRIC = 2,
    } BoundaryModeTypes;


    /**
     * @brief smoothModeToString this method converts the smooth mode from
     * SmoothModeType to the real smooth mode name. If you want to add an
     * smooth mode, you have to add in this method a translation from
     * SmoothModeType to the smooth mode name (string).
     * @param smoothMode the smooth mode from the enum item.
     * @return smooth mode name as string.
     */
    static QString smoothModeToString(SmoothModeTypes smoothMode);

    /**
     * @brief stringToSmoothMode This method converts the smooth mode as string
     * to the enum item SmoothModeTypes. If you add a smooth mode, you need to
     * add in this method the conversion from SmoothModeType to string
     * @param smoothMode the real name of the smooth mode as string
     * @return the smooth mode as enum item.
     */
    MSmoothProperties::SmoothModeTypes stringToSmoothMode(const QString& smoothMode);


    /**
     * @brief boundaryModeToString this method converts the boundary mode from
     * BoundaryModeType to the real boundary mode name. If you want to add an
     * boundary mode, you have to add in this method a translation from
     * BoundaryModeType to the boundary mode name (string).
     * @param smoothMode the smooth mode from the enum item.
     * @return boundary mode name as string.
     */
    static QString boundaryModeToString(BoundaryModeTypes boundaryMode);

    /**
     * @brief stringToBoundaryMode This method converts the boundary mode as string
     * to the enum item BoundaryModeTypes. If you add a boundary mode, you need to
     * add in this method the conversion from BoundaryModeType to string
     * @param boundaryMode the real name of the smooth mode as string
     * @return the boundary mode as enum item.
     */
    MSmoothProperties::BoundaryModeTypes stringToBoundaryMode(QString boundaryMode);

protected:
    MBoolProperty recomputeOnPropertyChangeProp;
    MFloatProperty smoothStDevKmProp;
    MIntProperty smoothStDevGridboxProp;
    MButtonProperty applySettingsProp;
    MEnumProperty smoothModeProp;
    MEnumProperty boundaryModeProp;

private:
    SmoothModeTypes smoothMode;
    BoundaryModeTypes boundaryMode;
    MProperty groupProp;
};


/**
  Actor for partial derivative filter
  */
class MPartialDerivativeProperties : public MRequestProperties
{
public:
    explicit MPartialDerivativeProperties(MNWPActorVariable *actorVar);
    ~MPartialDerivativeProperties() override;

    void addToRequest(MDataRequestHelper *rh) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    // Partial derivative modes as enum item. Add here your own implemented partial derivative modes.
    typedef enum {
        DISABLE_FILTER = 0,
        DLON = 1,
        DLAT = 2,
        DP = 3,
        MAG_OF_HORIZ_GRAD = 4,
        DLON_OF_MAG_OF_HORIZ_GRAD = 5,
        DLAT_OF_MAG_OF_HORIZ_GRAD = 6,
        MAG_OF_HORIZ_GRAD_OF_MAG_OF_HORIZ_GRAD = 7,
        DLON_LAGRANTO = 8,
        DLAT_LAGRANTO = 9,
    } PartialDerivativeModeTypes;
    /**
     * @brief partialDerivativeModeToString this method converts the partial derivative mode from
     * PartialDerivativeModeTypes to the real smooth mode name. If you want to add an
     * partial derivative mode, you have to add in this method a translation from
     * PartialDerivativeModeTypes to the partial derivative mode name (string).
     * @param partialModeType the partial derivative mode from the enum item.
     * @return partial derivative mode name as string.
     */
    static QString partialDerivativeModeToString(PartialDerivativeModeTypes partialModeType);

    /**
     * @brief PartialDerivativeModeTypes This method converts the partial derivative
     * mode as string to the enum item PartialDerivativeModeTypes.
     * If you add a partial derivative mode, you need to
     * add in this method the conversion from PartialDerivativeModeTypes to string
     * @param partialModeName the real name of the partial derivative mode as string
     * @return the partial derivative mode as enum item.
     */
    static MPartialDerivativeProperties::PartialDerivativeModeTypes stringToPartialDerivativeMode(
            QString partialModeName);
protected:
    MBoolProperty recomputeOnPropertyChangeProp;
    MButtonProperty applySettingsProp;
    MEnumProperty partialDerivativeModeProp;
private:
    PartialDerivativeModeTypes partialDerivativeMode;
    MProperty groupProp;
};

class MRadarProperties : public MRequestProperties
{
public:
    explicit MRadarProperties(MNWPActorVariable *actorVar);
    ~MRadarProperties() override;
    void addToRequest(MDataRequestHelper *rh) override;

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

protected:
    MProperty radarGroupProp;
    MButtonProperty applySettingsProp;
    MBoolProperty undetectTransparencyProp;
};

class MRadarRegridProperties : public MRequestProperties
{
  public:
    MRadarRegridProperties(MNWPActorVariable *actorVar);
    ~MRadarRegridProperties();
    void addToRequest(MDataRequestHelper *rh);

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;


  protected:
    MProperty groupProp;
    MEnumProperty verticalTargetGridModeProp;

    typedef enum {
        NONE = -1,
        EDPL = 0, // equidistant pressure levels
        EDHL = 1, // equidistant height levels
        UD = 2,   // user defined pressure levels
    } RegridLevelMode;

    RegridLevelMode regridLevelMode;

    MIntProperty numLevProp;
    MStringProperty userLevelStringProp;

    MEnumProperty horizontalTargetGridModeProp;

    MDoubleProperty deltaLonRadarProp;
    MIntProperty numLonProp;

    MDoubleProperty deltaLatRadarProp;
    MIntProperty numLatProp;

    MButtonProperty applySettingsProp;

    bool gridTopologyChanged;

    RegridLevelMode stringToRegridLevelMode(QString regridLevelModeName);
    QString regridLevelModeToString(RegridLevelMode regridLevelMode);
};

} // namespace Met3D

#endif // NWPACTORVARIABLEPROPERTIES_H
