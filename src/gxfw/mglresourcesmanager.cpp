/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017      Bianca Tost [+]
**  Copyright 2023      Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mglresourcesmanager.h"

// standard library imports
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <limits>

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <gdal.h>

// local application imports
#include "gxfw/textmanager.h"
#include "actors/lightactor.h"
#include "util/mexception.h"
#include "util/mutil.h"

namespace Met3D
{

// Nvidia video memory info extension, see
// http://developer.download.nvidia.com/opengl/specs/GL_NVX_gpu_memory_info.txt
#define GPU_MEMORY_INFO_DEDICATED_VIDMEM_NVX       0x9047
#define GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX    0x9048
#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX  0x9049
#define GPU_MEMORY_INFO_EVICTION_COUNT_NVX         0x904A
#define GPU_MEMORY_INFO_EVICTED_MEMORY_NVX         0x904B

// Initially set the single GL resources manager pointer to null.
MGLResourcesManager* MGLResourcesManager::instance = 0;

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

// Constructor is private.
MGLResourcesManager::MGLResourcesManager(const QGLFormat &format,
                                         QWidget *parent,
                                         QGLWidget *shareWidget)
    : QGLWidget(format, parent, shareWidget),
      globalMouseButtonRotate(Qt::LeftButton),
      globalMouseButtonPan(Qt::RightButton),
      globalMouseButtonZoom(Qt::MiddleButton),
      isReverseCameraZoom(false),
      isReverseCameraPan(false),
      videoMemoryUsage_kb(0),
      sceneLight(nullptr),
      selectSceneCentreActor(nullptr),
      selectSceneCentreText(nullptr)
{
    // Get the system control instance.
    systemControl = MSystemManagerAndControl::getInstance();
    MProperty *memoryGroup = systemControl->getMemoryManagersPropertyGroup();

    // Properties that are displayed in the system control.
    groupProp = MProperty("OpenGL resources");

    fpsMeasureProp = MStringProperty("Render time per frame", "");
    fpsMeasureProp.setEditable(false);
    groupProp.addSubProperty(fpsMeasureProp);

    totalVideoMemoryProp = MStringProperty("Available video memory", "");
    totalVideoMemoryProp.setEditable(false);
    groupProp.addSubProperty(totalVideoMemoryProp);

    met3DVideoMemoryProp = MStringProperty("NWP video memory usage", "");
    met3DVideoMemoryProp.setEditable(false);
    groupProp.addSubProperty(met3DVideoMemoryProp);

    totalSystemMemoryProp = MStringProperty("Available system memory", "");
    totalSystemMemoryProp.setEditable(false);
    groupProp.addSubProperty(totalSystemMemoryProp);

    dumpMemoryContentProp = MButtonProperty("Dump memory content", "Dump");
    dumpMemoryContentProp.registerValueCallback(this, &MGLResourcesManager::dumpMemoryContent);
    groupProp.addSubProperty(dumpMemoryContentProp);

    memoryGroup->insertSubProperty(0, &groupProp);

    // Register GDAL.
    GDALAllRegister();
}


MGLResourcesManager::~MGLResourcesManager()
{
    LOG4CPLUS_INFO(mlog, "Freeing graphics resources...");

    // Free memory of the various pools.

    LOG4CPLUS_DEBUG(mlog, "\tactor pool");
    for (int i = 0; i < actorPool.size(); i++)
    {
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting actor " << actorPool[i]->getID()
                        << " (" << actorPool[i]->getName() << ")");
        delete actorPool[i];
        if (actorPool[i] == textManager)
        {
            textManager = nullptr;
        }
    }

    LOG4CPLUS_DEBUG(mlog, "\tactor factory pool");
    for(MAbstractActorFactory* factory : actorFactoryPool)
    {
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting actor factory "
                        << factory->getName());
        delete factory;
    }

    LOG4CPLUS_DEBUG(mlog, "\tscene pool");
    for (int i = 0; i < scenePool.size(); i++)
    {
        LOG4CPLUS_DEBUG(mlog, "\t\t -> deleting scene "
                        << scenePool[i]->getName());
        delete scenePool[i];
    }

    MGLResourcesManager::instance = 0;
    LOG4CPLUS_INFO(mlog, "Done freeing graphics resources.");
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MGLResourcesManager::initialize(const QGLFormat &format,
                                     QWidget *parent,
                                     QGLWidget *shareWidget)
{
    // If the instance is already initialized, this method won't do anything.
    if (MGLResourcesManager::instance == 0)
    {
        MGLResourcesManager::instance = new MGLResourcesManager(format, parent,
                                                                shareWidget);
        MGLResourcesManager::instance->initializeTextManager();
        MGLResourcesManager::instance->initializeMainSceneLight();
    }
}


MGLResourcesManager* MGLResourcesManager::getInstance()
{
    if (MGLResourcesManager::instance == 0)
        throw MInitialisationError(
                "MGLResourcesManager has not been initialized.",
                __FILE__, __LINE__);

    return MGLResourcesManager::instance;
}


bool MGLResourcesManager::hasInstance()
{
    return MGLResourcesManager::instance != 0;
}


void MGLResourcesManager::registerScene(MSceneControl *scene)
{
    LOG4CPLUS_INFO(mlog, "Registering scene "
                    << scene->getName());
    scenePool.append(scene);

    if (sceneLight != nullptr)
    {
        scene->addActor(sceneLight);
    }
}


MSceneControl *MGLResourcesManager::getScene(QString name)
{
    for (int i = 0; i < scenePool.size(); i++)
    {
        if (scenePool[i]->getName() == name) { return scenePool[i]; }
    }

    return nullptr;
}


QList<MSceneControl *> &MGLResourcesManager::getScenes()
{
    return scenePool;
}


int MGLResourcesManager::getSceneID(MSceneControl *scene) const
{
    return scenePool.indexOf(scene);
}


void MGLResourcesManager::deleteScene(QString name)
{
    for (int i = 0; i < scenePool.size(); i++)
    {
        if (scenePool[i]->getName() == name)
        {
            MSceneControl* scene = scenePool.takeAt(i);
            sceneLight->deregisterScene(scene);
            delete scene;
        }
    }
}


void MGLResourcesManager::registerActor(MActor *actor)
{
    LOG4CPLUS_INFO(mlog, "Registering actor " << actor->getName()
                    << " (ID " << actor->getID()
                    << ") with graphics resources manager");
    actorPool.append(actor);

    emit actorCreated(actor);

    // Listen to actorHasChangedItsName() signals of this actor.
    connect(actor, SIGNAL(actorNameChanged(MActor*, QString)),
            this, SLOT(actorHasChangedItsName(MActor*, QString)));

    // Notify the actor when other actors change.
    connect(this, SIGNAL(actorCreated(MActor*)),
            actor, SLOT(actOnOtherActorCreated(MActor*)));
    connect(this, SIGNAL(actorDeleted(MActor*)),
            actor, SLOT(actOnOtherActorDeleted(MActor*)));
    connect(this, SIGNAL(actorRenamed(MActor*, QString)),
            actor, SLOT(actOnOtherActorRenamed(MActor*, QString)));
}


bool MGLResourcesManager::generateEffectProgram(
        QString name, std::shared_ptr<GL::MShaderEffect>& effectProgram)
{
    bool newShaderHasBeenCreated = false;

    // Only create a new shader program if the shader pool does not alreay
    // contain a program with the specified name.
    if (!effectPool.contains(name))
    {
        effectPool.insert(
                    name,
                    std::shared_ptr<GL::MShaderEffect>(new GL::MShaderEffect()));
        newShaderHasBeenCreated = true;
    }

    effectProgram = effectPool.value(name);
    return newShaderHasBeenCreated;
}


GL::MTexture* MGLResourcesManager::createTexture( QString  key,
                                                 bool    *existed,
                                                 GLenum   target,
                                                 GLint    internalFormat,
                                                 GLsizei  width,
                                                 GLsizei  height,
                                                 GLsizei  depth )
{
    GL::MTexture *tex = new GL::MTexture(key, target, internalFormat, width, height, depth);

    if (tryStoreGPUItem(tex))
    {
        *existed = false;
        return dynamic_cast<GL::MTexture*>(getGPUItem(key));
    }
    else if(activeGPUItems.contains(key)
            || releasedGPUItems.contains(key))
    {
        *existed = true;
        delete tex;
        return dynamic_cast<GL::MTexture*>(getGPUItem(key));
    }

    *existed = false;
    delete tex;
    return nullptr;
}


void MGLResourcesManager::deleteActor(const QString name)
{
    LOG4CPLUS_INFO(mlog, "Deleting OpenGL actor '" << name << "' from actor pool");

    for (int i = 0; i < actorPool.size(); ++i)
    {
        MActor* actor = actorPool.at(i);
        if (actor->getName() == name)
        {
            actorPool.remove(i);

            // Disconnect actor from signals.
            disconnect(actor, SIGNAL(actorNameChanged(MActor*, QString)),
                       this, SLOT(actorHasChangedItsName(MActor*, QString)));

            disconnect(this, SIGNAL(actorCreated(MActor*)),
                       actor, SLOT(actOnOtherActorCreated(MActor*)));
            disconnect(this, SIGNAL(actorDeleted(MActor*)),
                       actor, SLOT(actOnOtherActorDeleted(MActor*)));
            disconnect(this, SIGNAL(actorRenamed(MActor*, QString)),
                       actor, SLOT(actOnOtherActorRenamed(MActor*, QString)));

            // Notify other actors that this actor will be deleted.
            emit actorDeleted(actor);

            delete actor;
            break;
        }
    }
}


void MGLResourcesManager::deleteActor(MActor* actor)
{
    deleteActor(actor->getName());
}


bool MGLResourcesManager::tryStoreGPUItem(GL::MAbstractGPUDataItem *item)
{
    MDataRequest key = item->getRequestKey();

    LOG4CPLUS_TRACE(mlog, "tryStoreGPUItem() for key " << key);

    // Items that are already stored in the cache cannot be stored again.
    if (contains(key))
    {
        LOG4CPLUS_TRACE(mlog, "declined, request key already exists.");
        return false;
    }

    // Test if the system memory limit will be exceeded by adding the new data
    // item. If so, remove some of the released data items.
    unsigned int itemMemoryUsage_kb = item->getGPUMemorySize_kb();
    while ( (videoMemoryUsage_kb + itemMemoryUsage_kb >= videoMemoryLimit_kb)
            && !releasedGPUItemsQueue.empty())
    {
        MDataRequest removeKey = releasedGPUItemsQueue.takeFirst();
        gpuItemsReferenceCounter.take(removeKey);
        GL::MAbstractGPUDataItem* removeItem = releasedGPUItems.take(removeKey);
        videoMemoryUsage_kb -= removeItem->getGPUMemorySize_kb();
        delete removeItem;
    }

    // If not enough memory could be freed throw an exception.
    if ( videoMemoryUsage_kb + itemMemoryUsage_kb >= videoMemoryLimit_kb )
    {
        LOG4CPLUS_FATAL(mlog, "Current GPU memory usage: "
                        << videoMemoryUsage_kb << " kb; new item requires: "
                        << itemMemoryUsage_kb << " kb; GPU memory limit: "
                        << videoMemoryLimit_kb << " kb");
        throw MMemoryError("GPU memory limit exceeded, cannot release"
                           " any further data items", __FILE__, __LINE__);
    }

    // Memory is fine, so insert the new grid into pool of data items.
    activeGPUItems.insert(key, item);
    // Block item until first call of getGPUItem() (see getData()).
    gpuItemsReferenceCounter.insert(key, -1);
    gpuItemsMemoryUsage_kb.insert(key, itemMemoryUsage_kb);
    videoMemoryUsage_kb += itemMemoryUsage_kb;
    displayMemoryUsage();
    return true;
}


bool MGLResourcesManager::contains(const Met3D::MDataRequest &key)
{
    return (activeGPUItems.contains(key)
            || releasedGPUItems.contains(key));
}


bool MGLResourcesManager::contains(GL::MAbstractGPUDataItem *item)
{
    MDataRequest key = item->getRequestKey();

    return (activeGPUItems.contains(key) || releasedGPUItems.contains(key));
}


GL::MAbstractGPUDataItem* MGLResourcesManager::getGPUItem(MDataRequest key)
{
    // Requested item exists in pool of active textures? Return it.
    if ( activeGPUItems.contains(key) )
    {
        // The data item is available and currently active. If the reference
        // contour is negative, the item has recently be inserted into the
        // cache by tryStoreGPUItem() and is blocked until this (=first) call
        // to getGPUItem(). Hence, set the reference counter to 1. Otherwise
        // increase the reference counter and return the item.
        if (gpuItemsReferenceCounter[key] < 0)
            gpuItemsReferenceCounter[key] = 1;
        else
            gpuItemsReferenceCounter[key] += 1;
        return activeGPUItems.value(key);
    }

    // Requested item exists in pool of released textures, i.e. is still in
    // video memory? Move it to active items and return.
    if ( releasedGPUItems.contains(key) )
    {
        GL::MAbstractGPUDataItem* item = releasedGPUItems.take(key);
        releasedGPUItemsQueue.removeOne(key);
        activeGPUItems.insert(key, item);
        gpuItemsReferenceCounter[key] += 1;
        return item;
    }

    // Key is not in video memory, return NULL pointer.
    return nullptr;
}


void MGLResourcesManager::releaseGPUItem(GL::MAbstractGPUDataItem *item)
{
    releaseGPUItem(item->getRequestKey());
}


void MGLResourcesManager::releaseGPUItem(Met3D::MDataRequest key)
{
    if ( !gpuItemsReferenceCounter.contains(key) ) return;

    // Decrement reference counter. If it is zero afterwards, we can safely
    // release the data field -- no actor requires it at the moment.
    gpuItemsReferenceCounter[key] -= 1;
    if ( (gpuItemsReferenceCounter[key] == 0) && activeGPUItems.contains(key) )
    {
        releasedGPUItems.insert(key, activeGPUItems.take(key));
        releasedGPUItemsQueue.push_back(key);
    }
}


void MGLResourcesManager::releaseAllGPUItemReferences(MDataRequest key)
{
    if (gpuItemsReferenceCounter.empty()
            || !gpuItemsReferenceCounter.contains(key)) return;

    if (gpuItemsReferenceCounter[key] > 1) gpuItemsReferenceCounter[key] = 1;
    releaseGPUItem(key);
}


void MGLResourcesManager::updateGPUItemSize(GL::MAbstractGPUDataItem *item)
{
    Met3D::MDataRequest key = item->getRequestKey();
    if ( !gpuItemsReferenceCounter.contains(key) ) return;

    unsigned int itemMemoryUsage_kb = item->getGPUMemorySize_kb();
    unsigned int oldMemoryUsage_kb = gpuItemsMemoryUsage_kb[key];
    gpuItemsMemoryUsage_kb[key] = itemMemoryUsage_kb;
    videoMemoryUsage_kb += (itemMemoryUsage_kb - oldMemoryUsage_kb);
}


bool MGLResourcesManager::isManagedGPUItem(GL::MAbstractGPUDataItem *item)
{
    Met3D::MDataRequest key = item->getRequestKey();
    return gpuItemsReferenceCounter.contains(key);
}


void MGLResourcesManager::reloadActorShaders()
{
    LOG4CPLUS_INFO(mlog, "Reloading actor shaders...");
    for (int i = 0; i < actorPool.size(); i++)
        if (actorPool[i]->isInitialized()) actorPool[i]->reloadAllShaderEffects();
}


MActor* MGLResourcesManager::getActorByName(const QString& name) const
{
    for (const auto& actor : actorPool)
    {
        if (actor->getName() == name) return actor;
    }

    return nullptr;
}


void MGLResourcesManager::deleteActorFactory(const QString& name)
{
    // If no key with given name exists return.
    if (!actorFactoryPool.contains(name)) return;

    // Erase instantitation method with given key "name".
    MAbstractActorFactory *factory = actorFactoryPool.take(name);
    delete factory;
}


const QList<QString> MGLResourcesManager::getActorFactoryNames() const
{
    return actorFactoryPool.keys();
}


MAbstractActorFactory* MGLResourcesManager::getActorFactory(const QString &name)
{
    if (actorFactoryPool.contains(name)) return actorFactoryPool[name];

    return nullptr;
}


QList<MAbstractActorFactory*> MGLResourcesManager::getActorFactories()
{
    return actorFactoryPool.values();
}


QList<MActor*> MGLResourcesManager::getActorsConnectedTo(MActor *actor)
{
    QList<MActor*> connectedActors;

    for (MActor* a : actorPool)
    {
        if (a->isConnectedTo(actor))
        {
            connectedActors << a;
        }
    }

    return connectedActors;
}


QList<MActor*> MGLResourcesManager::getActorsConnectedToBBox(QString bBoxName)
{
    QList<MActor*> connectedActors;

    for (MActor* actor : actorPool)
    {
        // Only actors inheriting from MBoundingBoxInterface can be connected to
        // a bounding box.
        if (MBoundingBoxInterface *bBoxActor =
                dynamic_cast<MBoundingBoxInterface*>(actor))
        {
            if (bBoxActor->getBoundingBoxName() == bBoxName)
            {
                connectedActors << actor;
            }
        }
    }

    return connectedActors;
}


void MGLResourcesManager::gpuMemoryInfo_kb(uint& total, uint& available)
{
    // Get CPU memory usage via NVIDIA extension.

    // See http://www.geeks3d.com/20100531/programming-tips-how-to-know-the-graphics-memory-size-and-usage-in-opengl/
    // and http://developer.download.nvidia.com/opengl/specs/GL_NVX_gpu_memory_info.txt

    GLint totalGPUMemory = 0;
    GLint currentlyAvailableGPUMemory = 0;

    glGetIntegerv(GL_GPU_MEM_INFO_TOTAL_AVAILABLE_MEM_NVX,
                  &totalGPUMemory);
    glGetIntegerv(GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX,
                  &currentlyAvailableGPUMemory);

    if (glGetError() == GL_INVALID_ENUM)
    {
        totalGPUMemory = 0;
        currentlyAvailableGPUMemory = 0;
    }

    // On AMD or virtual GPU this method won't work, so we use a user-configurable fallback value.
    // This is not desirable, but for the way memory management currently works, it is needed.
    if (totalGPUMemory == 0)
    {
        totalGPUMemory = fallbackVideoMemoryLimit_kb;
        currentlyAvailableGPUMemory = fallbackVideoMemoryLimit_kb - videoMemoryUsage_kb;
    }

    total = totalGPUMemory;
    available = currentlyAvailableGPUMemory;
}


void MGLResourcesManager::systemMemoryInfo_kb(uint& total, uint& available)
{
    // Get system memory information on Linux systems by opening /proc/meminfo
    // and parsing the "MemFree: x kB" and "MemTotal:" lines.

    total = 0;      // in kB
    available = 0;

    FILE* meminfo = fopen("/proc/meminfo", "rt");
    // Silently ignore ary errors if the file cannot be found.
    if(!meminfo) return;

#define LINELENGTH 64
    char cLine[LINELENGTH];
    char cMemFreeText[] = "MemFree:";
    char cMemTotalText[] = "MemTotal:";

    while(fgets(cLine, LINELENGTH, meminfo) != 0) {
        if(strncmp(cMemFreeText, cLine, strlen(cMemFreeText)) == 0) {
            if(sscanf(cLine, "%*s %u kB", &available) != 1)
            {
                LOG4CPLUS_ERROR(mlog, "Couldn't parse \"MemFree\" line in "
                                "/proc/meminfo");
            }
        }
        if(strncmp(cMemTotalText, cLine, strlen(cMemTotalText)) == 0) {
            if(sscanf(cLine, "%*s %u kB", &total) != 1) {
                LOG4CPLUS_ERROR(mlog, "Couldn't parse \"MemTotal\" line in "
                                "/proc/meminfo");
            }
        }
        if ((total > 0) && (available > 0)) break;
    }

    fclose(meminfo);
}


void MGLResourcesManager::initializeTextManager()
{
    textManager = new MTextManager();

    // Prevent the text manager actor to be deleted by the user at runtime.
    textManager->setActorIsUserDeletable(false);

    registerActor(textManager);
}


void MGLResourcesManager::initializeMainSceneLight()
{
    sceneLight = new MLightActor();
    sceneLight->setName("Scene light");
    sceneLight->setActorIsUserDeletable(false);

    registerActor(sceneLight);

    for (MSceneControl *scene : getScenes())
    {
        scene->addActor(sceneLight);
    }
}


MTextManager* MGLResourcesManager::getTextManager()
{
    return textManager;
}


MLightActor *MGLResourcesManager::getMainSceneLight()
{
    return sceneLight;
}


MLabel* MGLResourcesManager::getSceneRotationCentreSelectionLabel()
{
    // Initialize an MLabel for the interactive camera selection.
    if (selectSceneCentreText == nullptr)
    {
        MTextManager* tm = getTextManager();
        selectSceneCentreText = tm->addText(
                    "Drag the pole to the rotation centre you would like to "
                    "use, then hit enter.",
                     MTextManager::CLIPSPACE,-0.5,0.9,0,16,
                     QColor(0,0,0),MTextManager::BASELINELEFT,true);
    }

    return selectSceneCentreText;
}


MMovablePoleActor* MGLResourcesManager::getSceneRotationCentreSelectionPoleActor()
{
    // Initialize an MMovablePoleActor for the interactive camera selection.
    if (selectSceneCentreActor == nullptr)
    {
        MAbstractActorFactory *factory = getActorFactory("Movable poles");
        selectSceneCentreActor =
                dynamic_cast<MMovablePoleActor*>(factory->create());
        selectSceneCentreActor->setName("SelectSceneRotationCentreActor");
        selectSceneCentreActor->setEnabled(true);
        selectSceneCentreActor->initialize();
        registerActor(selectSceneCentreActor);
        selectSceneCentreActor->addPole(QPointF(0,0));
    }
    return selectSceneCentreActor;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MGLResourcesManager::displayMemoryUsage()
{
    uint total = 0;
    uint available = 0;

    gpuMemoryInfo_kb(total, available);
    totalVideoMemoryProp.setValue(
            QString("%1 / %2 MiB").arg(available / 1024).arg(total / 1024));

    met3DVideoMemoryProp.setValue(QString("%1 / %2 MiB")
                                               .arg(videoMemoryUsage_kb / 1024)
                                               .arg(videoMemoryLimit_kb
                                                            / 1024));

    systemMemoryInfo_kb(total, available);
    totalSystemMemoryProp.setValue(
            QString("%1 / %2 MiB").arg(available / 1024).arg(total / 1024));
}


void MGLResourcesManager::dumpMemoryContent()
{
    QString s = "\n\nOPENGL MEMORY CACHE CONTENT\n"
            "===========================\n"
            "Active items:\n";

    std::vector<GL::MAbstractGPUDataItem*> sortedActiveItems;

    for (GL::MAbstractGPUDataItem *item : activeGPUItems)
    {
        sortedActiveItems.push_back(item);
    }

    std::sort(sortedActiveItems.begin(), sortedActiveItems.end(),
              [](GL::MAbstractGPUDataItem * a, GL::MAbstractGPUDataItem * b)
    {
        return a->getGPUMemorySize_kb() > b->getGPUMemorySize_kb();
    });

    for (GL::MAbstractGPUDataItem *item : sortedActiveItems)
    {
        s += QString("REQUEST: %1, SIZE: %2 kb, REFERENCES: %3\n")
                .arg(item->getRequestKey())
                .arg(item->getGPUMemorySize_kb())
                .arg(gpuItemsReferenceCounter[item->getRequestKey()]);
    }

    s += "\nReleased items (in queued order):\n";

    for (const MDataRequest& request : releasedGPUItemsQueue)
    {
        s += QString("REQUEST: %1, SIZE: %2 kb, REFERENCES: %3\n")
                .arg(request)
                .arg(releasedGPUItems[request]->getGPUMemorySize_kb())
                .arg(gpuItemsReferenceCounter[request]);
    }

    s += "\n\n===========================\n";

    LOG4CPLUS_INFO(mlog, s);
}


void MGLResourcesManager::actorHasChangedItsName(MActor *actor, QString oldName)
{
    // Simply pass on the received signal to all registered actors.
    emit actorRenamed(actor, oldName);
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MGLResourcesManager::initializeGL()
{
    LOG4CPLUS_INFO(mlog, "*** MGLResourcesManager: Initialising OpenGL "
                    << "context. ***");

    // Print information on the OpenGL context and version.
    LOG4CPLUS_INFO(mlog, "*** OpenGL information:");
    LOG4CPLUS_INFO(mlog, "OpenGL context is "
                    << (context()->isValid() ? "" : "NOT ") << "valid.");
    LOG4CPLUS_INFO(mlog, "\tRequested version: "
                    << context()->requestedFormat().majorVersion()
                    << "." << context()->requestedFormat().minorVersion());
    LOG4CPLUS_INFO(mlog, "\tObtained version: "
                    << context()->format().majorVersion() << "."
                    << context()->format().minorVersion());
    LOG4CPLUS_INFO(mlog, "\tObtained profile: "
                    << context()->format().profile());
    LOG4CPLUS_INFO(mlog, "\tShaders are "
                    << (QGLShaderProgram::hasOpenGLShaderPrograms(
                            context()) ? "" : "NOT ") << "supported.");
    LOG4CPLUS_INFO(mlog, "\tMultisampling is "
                    << (context()->format().sampleBuffers() ? "" : "NOT ")
                    << "enabled.");
    LOG4CPLUS_INFO(mlog, "\tNumber of samples per pixel: "
                    << context()->format().samples());


    LOG4CPLUS_INFO(mlog, "Initialising GLEW...");
    GLenum err = glewInit();
    if (err != GLEW_OK)
    {
        LOG4CPLUS_ERROR(mlog, "\tError: " << glewGetErrorString(err));
    }
    LOG4CPLUS_INFO(mlog, "Using GLEW " << glewGetString(GLEW_VERSION));
    LOG4CPLUS_INFO(mlog, "OpenGL version supported by this platform "
                    << "(glGetString): " << glGetString(GL_VERSION));

    LOG4CPLUS_INFO(mlog, "Core extensions of OpenGL 4.0 are supported: "
                    << (GLEW_VERSION_4_0 ? "Yes" : "No"));
    LOG4CPLUS_INFO(mlog, "Core extensions of OpenGL 4.1 are supported: "
                    << (GLEW_VERSION_4_1 ? "Yes" : "No"));
    LOG4CPLUS_INFO(mlog, "Core extensions of OpenGL 4.2 are supported: "
                    << (GLEW_VERSION_4_2 ? "Yes" : "No"));
    LOG4CPLUS_INFO(mlog, "Core extensions of OpenGL 4.3 are supported: "
                    << (GLEW_VERSION_4_3 ? "Yes" : "No"));

    QString glVersion = QString((char*)glGetString(GL_VERSION));

    groupProp.setName(QString("OpenGL resources (%1)")
                                   .arg(glVersion));

    if (!glVersion.contains("NVIDIA", Qt::CaseInsensitive))
    {
        LOG4CPLUS_WARN(mlog, "************************** IMPORTANT **************************");
        LOG4CPLUS_WARN(mlog, "Met.3D has only been tested with the proprietary Nvidia driver.");
        LOG4CPLUS_WARN(mlog, "It seems you are using a different driver. Please let us know if");
        LOG4CPLUS_WARN(mlog, "you run into problems.");
        LOG4CPLUS_WARN(mlog, "************************** IMPORTANT **************************");
    }

    QString value = "";
    if (QGLFormat::hasOpenGL())
    {
        int flags = QGLFormat::openGLVersionFlags();
        if (flags & QGLFormat::OpenGL_Version_4_0) value += "OpenGL 4.0";
        else if (flags & QGLFormat::OpenGL_Version_3_3) value += "OpenGL 3.3";
        else if (flags & QGLFormat::OpenGL_Version_3_2) value += "OpenGL 3.2";
        else if (flags & QGLFormat::OpenGL_Version_3_1) value += "OpenGL 3.1";
        else if (flags & QGLFormat::OpenGL_Version_3_0) value += "OpenGL 3.0";
        else if (flags & QGLFormat::OpenGL_Version_2_1) value += "OpenGL 2.1";
        else if (flags & QGLFormat::OpenGL_Version_2_0) value += "OpenGL 2.0";
        else if (flags & QGLFormat::OpenGL_Version_1_5) value += "OpenGL 1.5";
        else if (flags & QGLFormat::OpenGL_Version_1_4) value += "OpenGL 1.4";
        else if (flags & QGLFormat::OpenGL_Version_1_3) value += "OpenGL 1.3";
        else if (flags & QGLFormat::OpenGL_Version_1_2) value += "OpenGL 1.2";
        else if (flags & QGLFormat::OpenGL_Version_1_1) value += "OpenGL 1.1";
        if (flags & QGLFormat::OpenGL_ES_CommonLite_Version_1_0) value += " OpenGL ES 1.0 Common Lite";
        else if (flags & QGLFormat::OpenGL_ES_Common_Version_1_0) value += " OpenGL ES 1.0 Common";
        else if (flags & QGLFormat::OpenGL_ES_CommonLite_Version_1_1) value += " OpenGL ES 1.1 Common Lite";
        else if (flags & QGLFormat::OpenGL_ES_Common_Version_1_1) value += " OpenGL ES 1.1 Common";
        else if (flags & QGLFormat::OpenGL_ES_Version_2_0) value += " OpenGL ES 2.0";
    }
    else
    {
        value = "None";
    }
    LOG4CPLUS_DEBUG(mlog, "QGLFormat::openGLVersionFlags() returns minimum "
                    << "supported version is " << value);

    LOG4CPLUS_INFO(mlog, "*** END OpenGL information.\n");

    glLineWidth(2);
    if (glGetError() == GL_INVALID_VALUE)
    {
        LOG4CPLUS_WARN(mlog, "Line rendering with widths larger than 1 is not supported on this GPU driver. "
                             "Some features such as graticule lines might not work correctly.");
    }
    else
    {
        supportVariableLineWidths = true;
    }

    uint gpuTotalMemory;
    uint gpuAvailableMemory;
    gpuMemoryInfo_kb(gpuTotalMemory, gpuAvailableMemory);
    videoMemoryLimit_kb = gpuTotalMemory;

    LOG4CPLUS_INFO(mlog, "Maximum GPU video memory to be used: "
                    << videoMemoryLimit_kb / 1024. << " MB.");

    // Initialise currently registered actors.
    LOG4CPLUS_INFO(mlog, "Initialising actors...");
    LOG4CPLUS_INFO(mlog, "===================================================="
                    << "====================================================");

    for (int i = 0; i < actorPool.size(); i++)
    {
        LOG4CPLUS_INFO(mlog, "======== ACTOR #" << i << " ========");
        if (!actorPool[i]->isInitialized()) actorPool[i]->initialize();
    }

    LOG4CPLUS_INFO(mlog, "===================================================="
                    << "====================================================");
    LOG4CPLUS_INFO(mlog, "Actors are initialised.");

    displayMemoryUsage();

    // Start a time to update the displayed memory information every 2 seconds.
    QTimer *timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(displayMemoryUsage()));
    timer->start(5000);

    // Prevent this widget from being shown. It is meant as a hidden widget
    // that manages the OpenGL resources, not for displaying anything.
    setVisible(false);

    LOG4CPLUS_INFO(mlog, "GL resources manager initialisation done\n*****\n");

    // Inform the system manager that the application has been initialized.
    systemControl->setApplicationIsInitialized();
}


void MGLResourcesManager::setFallbackGpuMemoryLimit(float limit)
{
    fallbackVideoMemoryLimit_kb = limit * 1024 * 1024;
}


bool MGLResourcesManager::supportsGlLineWidth() const
{
    return supportVariableLineWidths;
}

} // namespace Met3D
