/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2023 Marc Rautenhaus [*, previously +]
**  Copyright 2015-2017 Bianca Tost [+]
**  Copyright 2020-2023 Andreas Beckert [*]
**  Copyright 2024      Christoph Fischer [*]
**  Copyright 2024      Susanne Fuchs [*]
**  Copyright 2024      Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "nwpactorvariableproperties.h"

// standard library imports

// related third party imports
#include <QtCore>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/nwpactorvariable.h"
#include "gxfw/nwpmultivaractor.h"

namespace Met3D
{

/******************************************************************************
***                       MRequestPropertiesGroup                           ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MRequestProperties::MRequestProperties(MNWPActorVariable *actorVar)
    : actorVariable(actorVar)
{
}


/******************************************************************************
***                      MRequestPropertiesFactory                          ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MRequestPropertiesFactory::MRequestPropertiesFactory(MNWPActorVariable *actorVar)
    : actorVariable(actorVar)
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MRequestPropertiesFactory::updateProperties(
        QList<MRequestProperties*> *propertiesList,
        const QStringList &keysRequiredByPipeline)
{
    // MVerticalRegridProperties
    updateTypedProperties<MVerticalRegridProperties>(
                QStringList() << "REGRID",
                propertiesList, keysRequiredByPipeline);

    // MTrajectoryGriddingProperties
    updateTypedProperties<MTrajectoryGriddingProperties>(
                QStringList() << "GRID_GEOMETRY",
                propertiesList, keysRequiredByPipeline);

    // MProbabilityRegionProperties
    updateTypedProperties<MProbabilityRegionProperties>(
                QStringList() << "PROBABILITY",
                propertiesList, keysRequiredByPipeline);

    // MPartialDerivativeProperties
    updateTypedProperties<MPartialDerivativeProperties>(
                QStringList() << "PARTIALDERIVATIVE",
                propertiesList, keysRequiredByPipeline);

    // MSmoothProperties
    updateTypedProperties<MSmoothProperties>(
                QStringList() << "SMOOTH",
                propertiesList, keysRequiredByPipeline);

    // MRRadarProperties
    updateTypedProperties<MRadarProperties>(
                QStringList() << "USE_UNDETECT",
                propertiesList, keysRequiredByPipeline);


    // MRadarRegridProperties
    updateTypedProperties<MRadarRegridProperties>(
                QStringList() << "RADAR_REGRID_LEVELS"
                      << "RADAR_REGRID_LONLAT_DISTANCE"
                      << "RADAR_REGRIDDER_TOPOLOGY_HAS_CHANGED",
                propertiesList, keysRequiredByPipeline);
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

template<typename T> void MRequestPropertiesFactory::updateTypedProperties(
        const QStringList& keysHandledByType,
        QList<MRequestProperties*> *propertiesList,
        const QStringList& keysRequiredByPipeline)
{
    // Does the list of keys required by the pipeline contain all keys
    // provided by property type T?
    for (int i = 0; i < keysHandledByType.size(); i++)
    {
        if (!keysRequiredByPipeline.contains(keysHandledByType[i]))
        {

            // Keys required by pipeline are not present. Check if properties
            // exist from earlier pipeline connection. If yes, delete.
            for (int i = 0; i < propertiesList->size(); i++)
            {
                if (dynamic_cast<T*>(propertiesList->at(i)))
                {
                    delete propertiesList->takeAt(i);
                    return;
                }
            }
            return;
        }
    }

    // All provided keys are contained. Check if an instance of T is already
    // contained in the actor variables' properties list.
    for (int i = 0; i < propertiesList->size(); i++)
    {
        if (dynamic_cast<T*>(propertiesList->at(i)))
        {
            return;
        }
    }

    // This is not the case, hence add a new one.
    propertiesList->append(new T(actorVariable));
}


/******************************************************************************
***                      SPECIFIC IMPLEMENTATIONS                           ***
*******************************************************************************/

/******************************************************************************
***                      MVerticalRegridProperties                          ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MVerticalRegridProperties::MVerticalRegridProperties(
        MNWPActorVariable *actorVar)
    : MRequestProperties(actorVar),
      regridMode(""),
      ignorePropertyChangeEvents(false)
{
    // Create and initialise properties.
    // ===============================================
    groupProp = MProperty("Vertical regrid");
    groupProp.setConfigGroup("VerticalRegrid");
    actorVar->varGroupProp.addSubProperty(groupProp);

    QStringList regridModeNames;
    regridModeNames << "No regrid"
                    << "To hybrid/mean psfc"
                    << "To hybrid/min psfc"
                    << "To hybrid/const 1013.25 hPa"
                    << "To pressure levels/ECMWF standard"
                    << "To pressure levels/const 1013.25 hPa";
    regridModeProp = MEnumProperty("Regrid mode", regridModeNames);
    regridModeProp.setConfigKey("regrid_mode");
    regridModeProp.saveAsEnumName(true);
    regridModeProp.registerValueCallback([=]()
    {
        int index = regridModeProp.value();
        switch (index)
        {
        default:
        case 0: // no regrid
            regridMode = "";
            break;
        case 1: // mean sfc pressure
            regridMode = "ML/MEAN";
            break;
        case 2: // min sfc pressure
            regridMode = "ML/MIN";
            break;
        case 3: // const sfc pressure
            regridMode = "ML/CONST_STANDARD_PSFC";
            break;
        case 4: // PL grid
            regridMode = "PL/HPA/10/50/100/200/250/300/400/500/700/850/925/1000";
            break;
        case 5: // PL grid, const sfc pressure
            regridMode = "PL/CONST_STANDARD_PSFC";
            break;
        }

        if (actorVariable->getActor()->suppressActorUpdates()) return;
        actorVariable->triggerAsynchronousDataRequest(true);

        // If enabled, broadcast change to other actor variables.
        if (enableBroadcastProp)
        {
            ignorePropertyChangeEvents = true;
            actorVariable->getActor()->broadcastPropertyChangedEvent(
                    MPropertyType::VerticalRegrid, &index);
            ignorePropertyChangeEvents = false;
        }
    });
    groupProp.addSubProperty(regridModeProp);

//TODO (mr, 01Feb2015) -- find a more elegant way to sync properties;
//                        possibly with Qt signals?
    enableBroadcastProp = MBoolProperty("Broadcast to all variables", false);
    enableBroadcastProp.setConfigGroup("broadcast_to_all_variables");
    groupProp.addSubProperty(enableBroadcastProp);
}


MVerticalRegridProperties::~MVerticalRegridProperties()
= default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MVerticalRegridProperties::addToRequest(MDataRequestHelper *rh)
{
    if (regridMode != "")
    {
        if (regridMode == "ML/MEAN")
            rh->insert("REGRID", QString("ML/MEAN/%1").arg(
                           MDataRequestHelper::uintSetToString(
                               actorVariable->selectedEnsembleMembers)));
        else if (regridMode == "ML/MIN")
            rh->insert("REGRID", QString("ML/MIN/%1").arg(
                           MDataRequestHelper::uintSetToString(
                               actorVariable->selectedEnsembleMembers)));
        else
            rh->insert("REGRID", regridMode);
    }
}


void MVerticalRegridProperties::actorPropertyChangeEvent(
        MPropertyType::ChangeNotification ptype, void *value)
{
    if (ignorePropertyChangeEvents) return;

    if (ptype == MPropertyType::VerticalRegrid)
    {
        // Prevent further broadcasts.
        enableBroadcastProp = false;

        int index = *(static_cast<int*>(value));
        regridModeProp.setValue(index);
    }
}


void MVerticalRegridProperties::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    settings->beginGroup("VerticalRegrid");
    regridModeProp.setEnumItem(settings->value("regridMode", "no regrid").toString());
    enableBroadcastProp = settings->value("enableBroadcast", false).toBool();
    settings->endGroup();
}


/******************************************************************************
***                    MTrajectoryGriddingProperties                        ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryGriddingProperties::MTrajectoryGriddingProperties(
        MNWPActorVariable *actorVar)
    : MRequestProperties(actorVar)
{
    // Create and initialise properties.
    // ===============================================
    groupProp = MProperty("Trajectory gridding settings");
    groupProp.setConfigGroup("TrajectoryGriddingSettings");
    actorVar->varGroupProp.addSubProperty(groupProp);

    applySettingsProp = MButtonProperty("Apply settings", "Apply");
    applySettingsProp.registerValueCallback([=]()
    {
        actorVariable->triggerAsynchronousDataRequest(true);
    });
    groupProp.addSubProperty(applySettingsProp);

    scaleParcelThicknessProp = MBoolProperty("Scale air parcel thickness", false);
    scaleParcelThicknessProp.setConfigKey("scale_air_parcel_thickness");
    groupProp.addSubProperty(scaleParcelThicknessProp);

    westernLonProp = MFloatProperty("Western lon", -100.);
    westernLonProp.setConfigKey("western_lon");
    westernLonProp.setMinMax(-360, 360);
    westernLonProp.setDecimals(2);
    westernLonProp.setStep(1);
    westernLonProp.setSuffix(" degrees");
    groupProp.addSubProperty(westernLonProp);

    deltaLonProp = MFloatProperty("Delta lon", 1);
    deltaLonProp.setConfigKey("delta_lon");
    deltaLonProp.setMinMax(0.01, 90);
    deltaLonProp.setDecimals(2);
    deltaLonProp.setStep(1);
    deltaLonProp.setSuffix(" degrees");
    groupProp.addSubProperty(deltaLonProp);

    numLonProp = MIntProperty("num. longitudes", 131);
    numLonProp.setConfigKey("num_lons");
    numLonProp.setMinimum(1);
    groupProp.addSubProperty(numLonProp);

    northerLatProp = MFloatProperty("Northern lat", 85);
    northerLatProp.setConfigKey("northern_lat");
    northerLatProp.setMinMax(-90, 90);
    northerLatProp.setDecimals(2);
    northerLatProp.setStep(1);
    northerLatProp.setSuffix(" degrees");
    groupProp.addSubProperty(northerLatProp);

    deltaLatProp = MFloatProperty("Delta lat", 1);
    deltaLatProp.setConfigKey("delta_lat");
    deltaLatProp.setMinMax(0.01, 90);
    deltaLatProp.setDecimals(2);
    deltaLatProp.setStep(1);
    deltaLatProp.setSuffix(" degrees");
    groupProp.addSubProperty(deltaLatProp);

    numLatProp = MIntProperty("Num. latitudes", 66);
    numLatProp.setConfigKey("num_lats");
    numLatProp.setMinimum(1);
    groupProp.addSubProperty(numLatProp);

    QStringList gridType; gridType << "regular";
    verticalGridTypeProp = MEnumProperty("Vertical grid type", gridType);
    verticalGridTypeProp.setConfigKey("vertical_grid_type");
    groupProp.addSubProperty(verticalGridTypeProp);

    bottomPressureProp = MFloatProperty("Bottom pressure", 1050.);
    bottomPressureProp.setConfigKey("bottom_pressure");
    bottomPressureProp.setMinMax(20., 1050.);
    bottomPressureProp.setStep(5.);
    bottomPressureProp.setDecimals(2);
    bottomPressureProp.setSuffix(" hPa");
    groupProp.addSubProperty(bottomPressureProp);

    topPressureProp = MFloatProperty("Top pressure", 100.);
    topPressureProp.setConfigKey("top_pressure");
    topPressureProp.setMinMax(20., 1050.);
    topPressureProp.setDecimals(2);
    topPressureProp.setStep(5.);
    topPressureProp.setSuffix(" hPa");
    groupProp.addSubProperty(topPressureProp);

    numPressureProp = MIntProperty("Num. vertical levels", 20);
    numPressureProp.setConfigKey("num_vertical_levels");
    numPressureProp.setMinimum(1);
    groupProp.addSubProperty(numPressureProp);
}


MTrajectoryGriddingProperties::~MTrajectoryGriddingProperties()
= default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MTrajectoryGriddingProperties::addToRequest(MDataRequestHelper *rh)
{
    // Gridding settings.
    int vertGridTypeIndex = verticalGridTypeProp;
    QString vertGridType = (vertGridTypeIndex == 0) ? "REGULAR" : "STRETCHED";

    float westernlon = westernLonProp;
    float dlon       = deltaLonProp;
    int   nlon       = numLonProp;

    float northernlat = northerLatProp;
    float dlat        = deltaLatProp;
    int   nlat        = numLatProp;

    float pbot = bottomPressureProp;
    float ptop = topPressureProp;
    int   np   = numPressureProp;

    bool scaleParcelThickness = scaleParcelThicknessProp;
    rh->insert("GRID_GEOMETRY", QString("%1/%2/%3/%4/%5/%6/%7/%8/%9/%10/%11")
               .arg(vertGridType)
               .arg(westernlon).arg(dlon).arg(nlon).arg(northernlat)
               .arg(dlat).arg(nlat).arg(pbot).arg(ptop).arg(np)
               .arg(scaleParcelThickness ? "1" : "0"));
}


void MTrajectoryGriddingProperties::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    scaleParcelThicknessProp = settings->value("scaleParcelThickness", true).toBool();

    westernLonProp = settings->value("westernlon", -90.).toDouble();
    deltaLonProp = settings->value("dlon", 1.).toDouble();
    numLonProp = settings->value("nlon", 180).toInt();

    northerLatProp = settings->value("northernlat", -90.).toDouble();
    deltaLatProp = settings->value("dlat", 1.).toDouble();
    numLatProp = settings->value("nlat", 180).toInt();

    bottomPressureProp = settings->value("pbot", 1000.).toDouble();
    topPressureProp = settings->value("ptop", 100.).toDouble();
    numPressureProp = settings->value("np", 20).toInt();
}


/******************************************************************************
***                     MProbabilityRegionProperties                        ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MProbabilityRegionProperties::MProbabilityRegionProperties(
        MNWPActorVariable *actorVar)
    : MRequestProperties(actorVar)
{
    // Create and initialise properties.
    // ===============================================
    groupProp = MProperty("Region contribution");
    actorVar->varGroupProp.addSubProperty(groupProp);

    probabilityRegionIsovalueProp = MFloatProperty("Prob. region. isoval", 0.3);
    probabilityRegionIsovalueProp.setConfigKey("prop_region_isoval");
    probabilityRegionIsovalueProp.setMinMax(0, 1);
    probabilityRegionIsovalueProp.setDecimals(3);
    probabilityRegionIsovalueProp.setStep(0.1);
    probabilityRegionIsovalueProp.setSuffix(" (0-1)");
    probabilityRegionIsovalueProp.registerValueCallback([=]()
    {
        actorVariable->triggerAsynchronousDataRequest(false);
    });
    groupProp.addSubProperty(probabilityRegionIsovalueProp);
}


MProbabilityRegionProperties::~MProbabilityRegionProperties()
= default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MProbabilityRegionProperties::addToRequest(MDataRequestHelper *rh)
{
    if (rh->contains("ENS_OPERATION"))
        if ( ! rh->value("ENS_OPERATION").startsWith("P")) return;

    float probabilityRegionDetectionIsovalue = probabilityRegionIsovalueProp;
    rh->insert("PROBABILITY",
               QString("%1").arg(probabilityRegionDetectionIsovalue));
}


void MProbabilityRegionProperties::actorPropertyChangeEvent(
        MPropertyType::ChangeNotification ptype, void *value)
{
    if (ptype == MPropertyType::IsoValue)
    {
        float v = *(static_cast<float*>(value));
        probabilityRegionIsovalueProp = v;
    }
}


void MProbabilityRegionProperties::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    probabilityRegionIsovalueProp = settings
            ->value("probabilityRegionDetectionIsovalue", 0.).toFloat();
}


/******************************************************************************
***                     MSmoothingProperties                        ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSmoothProperties::MSmoothProperties(
        MNWPActorVariable *actorVar)
    : MRequestProperties(actorVar),
      smoothMode(DISABLE_FILTER),
      boundaryMode(CONSTANT)
{
    // Create and initialize properties.
    // ===============================================
    groupProp = MProperty("Horizontal smoothing");
    groupProp.setConfigGroup("SmoothFilter");
    actorVar->varGroupProp.addSubProperty(groupProp);

    recomputeOnPropertyChangeProp = MBoolProperty("Recompute on property change", false);
    recomputeOnPropertyChangeProp.setConfigKey("recompute_on_property_change");
    groupProp.addSubProperty(recomputeOnPropertyChangeProp);

    applySettingsProp = MButtonProperty("Compute", "Compute");
    applySettingsProp.registerValueCallback([=]()
    {
        actorVariable->triggerAsynchronousDataRequest(true);
    });
    groupProp.addSubProperty(applySettingsProp);

    QStringList smoothModeNames;
    // Comment the smoothModeName if you want that it does not show up in the
    // drop down menu of the GUI.
    smoothModeNames << smoothModeToString(DISABLE_FILTER)
                    << smoothModeToString(GAUSS_DISTANCE)
                    << smoothModeToString(BOX_BLUR_DISTANCE_FAST)
                    << smoothModeToString(UNIFORM_WEIGHTED_GRIDPOINTS)
                    << smoothModeToString(GAUSS_GRIDPOINTS)
                    << smoothModeToString(BOX_BLUR_GRIDPOINTS_SLOW)
                    << smoothModeToString(BOX_BLUR_GRIDPOINTS_FAST);
    smoothModeProp = MEnumProperty("Smooth mode", smoothModeNames);
    smoothModeProp.setConfigKey("smooth_mode");
    smoothModeProp.setValue(smoothMode);
    smoothModeProp.registerValueCallback([=]()
    {
        SmoothModeTypes index = stringToSmoothMode(smoothModeProp.getSelectedEnumName());

        switch (index)
        {
        case DISABLE_FILTER: // no smoothing
            smoothMode = DISABLE_FILTER;
            smoothStDevKmProp.setEnabled(false);
            smoothStDevGridboxProp.setEnabled(false);
            boundaryModeProp.setEnabled(false);
            break;
        case GAUSS_DISTANCE:
            smoothMode = GAUSS_DISTANCE;
            smoothStDevKmProp.setEnabled(true);
            smoothStDevGridboxProp.setEnabled(false);
            boundaryMode = NANPADDING;
            boundaryModeProp = boundaryMode;
            boundaryModeProp.setEnabled(false);
            break;
        case BOX_BLUR_DISTANCE_FAST:
            smoothMode = BOX_BLUR_DISTANCE_FAST;
            smoothStDevKmProp.setEnabled(true);
            smoothStDevGridboxProp.setEnabled(false);
            boundaryModeProp.setEnabled(true);
            break;
        case UNIFORM_WEIGHTED_GRIDPOINTS:
            smoothMode = UNIFORM_WEIGHTED_GRIDPOINTS;
            smoothStDevKmProp.setEnabled(false);
            smoothStDevGridboxProp.setEnabled(true);
            boundaryMode = NANPADDING;
            boundaryModeProp = boundaryMode;
            boundaryModeProp.setEnabled(false);
            break;
        case GAUSS_GRIDPOINTS:
            smoothMode =  GAUSS_GRIDPOINTS;
            smoothStDevKmProp.setEnabled(false);
            smoothStDevGridboxProp.setEnabled(true);
            boundaryMode = NANPADDING;
            boundaryModeProp = boundaryMode;
            boundaryModeProp.setEnabled(false);
            break;
        case BOX_BLUR_GRIDPOINTS_SLOW:
            smoothMode = BOX_BLUR_GRIDPOINTS_SLOW;
            smoothStDevKmProp.setEnabled(false);
            smoothStDevGridboxProp.setEnabled(true);
            boundaryMode = CONSTANT;
            boundaryModeProp = boundaryMode;
            boundaryModeProp.setEnabled(false);
            break;
        case BOX_BLUR_GRIDPOINTS_FAST:
            smoothMode = BOX_BLUR_GRIDPOINTS_FAST;
            smoothStDevKmProp.setEnabled(false);
            smoothStDevGridboxProp.setEnabled(true);
            boundaryModeProp.setEnabled(true);
            break;
        }

        if (recomputeOnPropertyChangeProp)
        {
            actorVariable->triggerAsynchronousDataRequest(true);
        }
    });
    groupProp.addSubProperty(smoothModeProp);

    QStringList boundaryModeNames;
    // Comment the boundaryModeName if you want that it does not show up in the
    // drop down menu of the GUI.
    boundaryModeNames << boundaryModeToString(CONSTANT)
                      << boundaryModeToString(NANPADDING)
                      << boundaryModeToString(SYMMETRIC);
    boundaryModeProp = MEnumProperty("Boundary conditions", boundaryModeNames);
    boundaryModeProp.setConfigKey("boundary_conditions");
    boundaryModeProp.setValue(boundaryMode);
    boundaryModeProp.registerValueCallback([=]()
    {
        boundaryMode = stringToBoundaryMode(boundaryModeProp.getSelectedEnumName());

        if (recomputeOnPropertyChangeProp)
        {
            actorVariable->triggerAsynchronousDataRequest(true);
        }
    });
    groupProp.addSubProperty(boundaryModeProp);

    smoothStDevKmProp = MFloatProperty("Standard deviation (km)", 10);
    smoothStDevKmProp.setConfigKey("standard_deviation_km");
    smoothStDevKmProp.setMinMax(0.5, 1000.0);
    smoothStDevKmProp.setDecimals(1);
    smoothStDevKmProp.setStep(0.5);
    smoothStDevKmProp.setEnabled(false);
    smoothStDevKmProp.registerValueCallback([=]()
    {
       if (recomputeOnPropertyChangeProp)
       {
           actorVariable->triggerAsynchronousDataRequest(true);
       }
    });
    groupProp.addSubProperty(smoothStDevKmProp);

    smoothStDevGridboxProp = MIntProperty("Standard deviation (grid cells)", 3);
    smoothStDevGridboxProp.setConfigKey("standard_deviation_grid_cells");
    smoothStDevGridboxProp.setMinMax(1, 500);
    smoothStDevGridboxProp.setEnabled(false);
    smoothStDevGridboxProp.registerValueCallback([=]()
    {
        if (recomputeOnPropertyChangeProp)
        {
            actorVariable->triggerAsynchronousDataRequest(true);
        }
    });
    groupProp.addSubProperty(smoothStDevGridboxProp);
}


MSmoothProperties::~MSmoothProperties()
= default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MSmoothProperties::addToRequest(MDataRequestHelper *rh)
{
    if (smoothMode != DISABLE_FILTER)
    {
        float smoothStDev_km = smoothStDevKmProp;
        float smoothStdGridbox = smoothStDevGridboxProp;
        // request is as follows:
        // 1. Smooth mode (e.g. BOX_BLUR_DISTANCE_FAST)
        // 2. Standard deviation of smooth mode in km (e.g. 50)
        // 3. Standard deviation of smooth radius in gridpoints (e.g. 5)
        // 4. Boundary handling (e.g. SYMMETRIC)
        rh->insert("SMOOTH", QString("%1/%2/%3/%4")
                   .arg(smoothMode).arg(smoothStDev_km)
                   .arg(smoothStdGridbox).arg(boundaryMode));
    }
}


void MSmoothProperties::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    settings->beginGroup("SmoothFilter");
    smoothModeProp = stringToSmoothMode(
            (settings->value("smoothMode",
                             smoothModeToString(DISABLE_FILTER))
            ).toString());
    smoothStDevKmProp =
            settings->value("standardDeviation_km", 10.0).toFloat();
    smoothStDevGridboxProp =
            settings->value("standardDeviation_gridcells", 3).toInt();
    boundaryModeProp = stringToBoundaryMode(
            (settings->value("boundaryMode",
                             boundaryModeToString(CONSTANT))
            ).toString());
    settings->endGroup();
}


MSmoothProperties::SmoothModeTypes MSmoothProperties::stringToSmoothMode(
        const QString& smoothModeName)
{
    if (smoothModeName == "disabled")
    {
        return DISABLE_FILTER;
    }
    else if (smoothModeName == "horizontalGauss_distance")
    {
        return GAUSS_DISTANCE;
    }
    else if (smoothModeName == "horizontalBoxBlur_distance")
    {
        return BOX_BLUR_DISTANCE_FAST;
    }
    else if (smoothModeName == "horizontalUniformWeights_gridcells")
    {
        return UNIFORM_WEIGHTED_GRIDPOINTS;
    }
    else if (smoothModeName == "horizontalGauss_gridcells")
    {
        return GAUSS_GRIDPOINTS;
    }
    else if (smoothModeName == "horizontalBoxBlurSlow_gridcells")
    {
        return BOX_BLUR_GRIDPOINTS_SLOW;
    }
    else if (smoothModeName == "horizontalBoxBlur_gridcells")
    {
        return BOX_BLUR_GRIDPOINTS_FAST;
    }
    else
    {
        return DISABLE_FILTER;
    }
}


QString MSmoothProperties::smoothModeToString(
        SmoothModeTypes smoothModeType)
{
    switch (smoothModeType)
    {
        case DISABLE_FILTER: return "disabled";
        case GAUSS_DISTANCE: return "horizontalGauss_distance";
        case BOX_BLUR_DISTANCE_FAST: return "horizontalBoxBlur_distance";
        case UNIFORM_WEIGHTED_GRIDPOINTS: return
                "horizontalUniformWeights_gridcells";
        case GAUSS_GRIDPOINTS: return "horizontalGauss_gridcells";
        case BOX_BLUR_GRIDPOINTS_SLOW: return "horizontalBoxBlurSlow_gridcells";
        case BOX_BLUR_GRIDPOINTS_FAST: return "horizontalBoxBlur_gridcells";
    }
    return "disable filter";
}


MSmoothProperties::BoundaryModeTypes MSmoothProperties::stringToBoundaryMode(
        QString boundaryModeName)
{
    if (boundaryModeName == "constant")
    {
        return CONSTANT;
    }
    else if (boundaryModeName == "nan-padding")
    {
        return NANPADDING;
    }
    else if (boundaryModeName == "symmetric")
    {
        return SYMMETRIC;
    }
    else
    {
        return CONSTANT;
    }
}


QString MSmoothProperties::boundaryModeToString(
        BoundaryModeTypes boundaryModeType)
{
    switch (boundaryModeType)
    {
        case CONSTANT: return "constant";
        case NANPADDING: return "nan-padding";
        case SYMMETRIC: return "symmetric";
    }
    return "constant";
}


/******************************************************************************
***                     MPartialDerivativeProperties                        ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPartialDerivativeProperties::MPartialDerivativeProperties(
        MNWPActorVariable *actorVar)
    : MRequestProperties(actorVar),
      partialDerivativeMode(DISABLE_FILTER)
{
    // Create and initialize properties.
    // ===============================================
    groupProp = MProperty("Partial derivative");
    groupProp.setConfigGroup("partialDerivative");
    actorVar->varGroupProp.addSubProperty(groupProp);

    recomputeOnPropertyChangeProp = MBoolProperty("Recompute on property change", false);
    recomputeOnPropertyChangeProp.setConfigKey("recompute_on_property_change");
    groupProp.addSubProperty(recomputeOnPropertyChangeProp);

    applySettingsProp = MButtonProperty("Compute", "Compute");
    applySettingsProp.registerValueCallback([=]()
    {
        actorVariable->triggerAsynchronousDataRequest(true);
    });
    groupProp.addSubProperty(applySettingsProp);

    QStringList partialDerivativeModeNames;
    // Comment the smooth if you want it don't show up in the drop down menu of
    // the GUI.
    partialDerivativeModeNames << partialDerivativeModeToString(DISABLE_FILTER)
                               << partialDerivativeModeToString(DLON)
                               << partialDerivativeModeToString(DLAT)
                               << partialDerivativeModeToString(DP)
                               << partialDerivativeModeToString(MAG_OF_HORIZ_GRAD)
                               << partialDerivativeModeToString(DLON_OF_MAG_OF_HORIZ_GRAD)
                               << partialDerivativeModeToString(DLAT_OF_MAG_OF_HORIZ_GRAD)
                               << partialDerivativeModeToString(MAG_OF_HORIZ_GRAD_OF_MAG_OF_HORIZ_GRAD);

    // Enable option for LAGRANTO partial derivative routine.
    // ============================================
#ifdef ENABLE_LAGRANTO_DERIVATIVES
    partialDerivativeModeNames << partialDerivativeModeToString(DLON_LAGRANTO)
                               << partialDerivativeModeToString(DLAT_LAGRANTO);
#endif

    partialDerivativeModeProp = MEnumProperty("Partial derivative mode", partialDerivativeModeNames);
    partialDerivativeModeProp.setConfigKey("partial_derivative_mode");
    partialDerivativeModeProp.setValue(partialDerivativeMode);
    partialDerivativeModeProp.registerValueCallback([=]()
    {
        partialDerivativeMode = stringToPartialDerivativeMode(
                partialDerivativeModeProp.getSelectedEnumName());

        if (recomputeOnPropertyChangeProp)
        {
            actorVariable->triggerAsynchronousDataRequest(true);
        }
    });
    groupProp.addSubProperty(partialDerivativeModeProp);
}


MPartialDerivativeProperties::~MPartialDerivativeProperties()
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MPartialDerivativeProperties::addToRequest(MDataRequestHelper *rh)
{
    if (partialDerivativeMode != DISABLE_FILTER)
    {
        rh->insert("PARTIALDERIVATIVE", QString("%1")
                   .arg(partialDerivativeMode));
    }
}


void MPartialDerivativeProperties::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    settings->beginGroup("partialDerivative");
    partialDerivativeModeProp = stringToPartialDerivativeMode(
                    (settings->value("partialDerivativeMode",
                                     "disabled")).toString());
    settings->endGroup();
}


MPartialDerivativeProperties::PartialDerivativeModeTypes MPartialDerivativeProperties::stringToPartialDerivativeMode(
        QString partialModeName)
{
    if (partialModeName == "disabled")
    {
        return DISABLE_FILTER;
    }
    else if (partialModeName == "\u2202\u03C8/\u2202lon")
    {
        return DLON;
    }
    else if (partialModeName == "\u2202\u03C8/\u2202lat")
    {
        return DLAT;
    }
    else if (partialModeName == "\u2202\u03C8/\u2202p")
    {
        return DP;
    }
    else if (partialModeName == "\u2202\u03C8/\u2202lon_lagrant")
    {
        return DLON_LAGRANTO;
    }
    else if (partialModeName == "\u2202\u03C8/\u2202lat_lagrant")
    {
        return DLAT_LAGRANTO;
    }
    else if (partialModeName == "|\u2207\u2095\u03C8|")
    {
        return MAG_OF_HORIZ_GRAD;
    }
    else if (partialModeName == "\u2202/\u2202lon |\u2207\u2095\u03C8|")
    {
        return DLON_OF_MAG_OF_HORIZ_GRAD;
    }
    else if (partialModeName == "\u2202/\u2202lat |\u2207\u2095\u03C8|")
    {
        return DLAT_OF_MAG_OF_HORIZ_GRAD;
    }
    else if (partialModeName == "|\u2207\u2095|\u2207\u2095\u03C8||")
    {
        return MAG_OF_HORIZ_GRAD_OF_MAG_OF_HORIZ_GRAD;
    }
    else
    {
        return DISABLE_FILTER;
    }
}


QString MPartialDerivativeProperties::partialDerivativeModeToString(
        PartialDerivativeModeTypes partialModeType)
{
    switch (partialModeType)
    {
        case DISABLE_FILTER: return "disabled";
        case DLON: return "\u2202\u03C8/\u2202lon";
        case DLAT: return "\u2202\u03C8/\u2202lat";
        case DP: return "\u2202\u03C8/\u2202p";
        case DLON_LAGRANTO: return "\u2202\u03C8/\u2202lon_lagrant";
        case DLAT_LAGRANTO: return "\u2202\u03C8/\u2202lat_lagrant";
        case MAG_OF_HORIZ_GRAD: return "|\u2207\u2095\u03C8|";
        case DLON_OF_MAG_OF_HORIZ_GRAD: return "\u2202/\u2202lon |\u2207\u2095\u03C8|";
        case DLAT_OF_MAG_OF_HORIZ_GRAD: return "\u2202/\u2202lat |\u2207\u2095\u03C8|";
        case MAG_OF_HORIZ_GRAD_OF_MAG_OF_HORIZ_GRAD: return "|\u2207\u2095|\u2207\u2095\u03C8||";
    }
    return "disabled";
}


/******************************************************************************
***                      MRadarProperties                          ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MRadarProperties::MRadarProperties(
    MNWPActorVariable *actorVar)
    : MRequestProperties(actorVar)
{
    // Create and initialise QtProperties for the GUI.
    // ===============================================

    radarGroupProp = MProperty("Radar settings");
    radarGroupProp.setConfigGroup("RadarSettings");
    actorVar->varGroupProp.addSubProperty(radarGroupProp);

    applySettingsProp = MButtonProperty("Apply settings", "Apply");
    applySettingsProp.registerValueCallback([=]()
    {
        actorVariable->triggerAsynchronousDataRequest(true);
    });
    radarGroupProp.addSubProperty(applySettingsProp);

    undetectTransparencyProp = MBoolProperty("Cells without signal: transparent", false);
    undetectTransparencyProp.setConfigKey("cells_without_signal_transparent");
    undetectTransparencyProp.setTooltip("Set value that indicates no signal was measured to 'missing value'.");
    radarGroupProp.addSubProperty(undetectTransparencyProp);
}


MRadarProperties::~MRadarProperties()
= default;


void MRadarProperties::addToRequest(MDataRequestHelper *rh)
{
    rh->insert("USE_UNDETECT", !undetectTransparencyProp.value());
}


void MRadarProperties::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    undetectTransparencyProp = settings->value("undetectTransparency", false).toBool();
}


/******************************************************************************
***                      MRadarRegridProperties                          ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MRadarRegridProperties::MRadarRegridProperties(
    MNWPActorVariable *actorVar)
    : MRequestProperties(actorVar),
      regridLevelMode(EDHL),
      gridTopologyChanged(false)
{
   // Create and initialise properties.
   // ===============================================

    groupProp = MProperty("regrid settings");

    auto gridTopoChanged = [=](){ gridTopologyChanged = true; };

    applySettingsProp = MButtonProperty("Apply settings", "Regrid");
    applySettingsProp.registerValueCallback([=]()
    {
       if (userLevelStringProp.isEnabled())
       {
           QVector<float> levels = parseFloatRangeString(userLevelStringProp);
           if (levels.empty())
           {
               LOG4CPLUS_ERROR(mlog, "User defined levels has to be a commma-separated list of float values, with at least two values. For example: 100, 500, 700, 1000");
           }
       }

        actorVariable->triggerAsynchronousDataRequest(true);
    });
    groupProp.addSubProperty(applySettingsProp);

    QStringList regridLevelNames;
    regridLevelNames << "equidistant pressure levels"
                    << "equidistant height levels"
                    << "user defined pressure levels";
    verticalTargetGridModeProp = MEnumProperty("Vertical target grid",
                                               regridLevelNames,
                                               regridLevelMode);
    verticalTargetGridModeProp.setConfigKey("vertical_target_grid");
    verticalTargetGridModeProp.registerValueCallback([=]()
    {
        regridLevelMode = RegridLevelMode(verticalTargetGridModeProp.value());

        switch (regridLevelMode)
        {
        case EDPL: // equidistant pressure
        case EDHL: // equidistant height
            numLevProp.setEnabled(true);
            userLevelStringProp.setEnabled(false);
            break;
        case UD: // user defined pressure levels
            numLevProp.setEnabled(false);
            userLevelStringProp.setEnabled(true);
            break;
        case NONE:
            break;
        }

        gridTopologyChanged = true;
    });
    groupProp.addSubProperty(verticalTargetGridModeProp);

    numLevProp = MIntProperty("Number of pressure/height levels", 100);
    numLevProp.setConfigKey("number_of_pressure_or_height_levels");
    numLevProp.setMinimum(1);
    numLevProp.registerValueCallback(gridTopoChanged);
    groupProp.addSubProperty(numLevProp);

    userLevelStringProp = MStringProperty("Levels", "");
    userLevelStringProp.setConfigKey("levels");
    userLevelStringProp.setEditable(false);
    userLevelStringProp.setTooltip("Enter commma-separated list of at least two values, e.g.: 550, 710.25, 1000.");
    userLevelStringProp.registerValueCallback(gridTopoChanged);
    groupProp.addSubProperty(userLevelStringProp);

    numLevProp.setEnabled(true);

    QStringList lonLatSelectionModes;
    lonLatSelectionModes << "number of grid points" << "distance between grid points";
    horizontalTargetGridModeProp = MEnumProperty("Horizontal target grid",
                                                 lonLatSelectionModes);
    horizontalTargetGridModeProp.setConfigKey("horizontal_target_grid");
    horizontalTargetGridModeProp.setTooltip("Specify the size of the target grid by either defining the "
                                                "number of grid points in both lon/lat direction, or the grid point "
                                                "spacing in metres");
    horizontalTargetGridModeProp.registerValueCallback([=]()
    {
        int index = horizontalTargetGridModeProp;

        switch (index)
        {
        case 0: // number of points
            deltaLatRadarProp.setEnabled(false);
            deltaLonRadarProp.setEnabled(false);
            numLatProp.setEnabled(true);
            numLonProp.setEnabled(true);
            break;
        case 1: // delta between points
            deltaLatRadarProp.setEnabled(true);
            deltaLonRadarProp.setEnabled(true);
            numLatProp.setEnabled(false);
            numLonProp.setEnabled(false);
            break;
        default:
            break;
        }
        gridTopologyChanged = true;
    });
    groupProp.addSubProperty(horizontalTargetGridModeProp);

    numLatProp = MIntProperty("Num. latitudinal points", 100);
    numLatProp.setConfigKey("num_lat_points");
    numLatProp.setMinimum(1);
    numLatProp.registerValueCallback(gridTopoChanged);
    groupProp.addSubProperty(numLatProp);

    numLonProp = MIntProperty("Num. longitudinal points", 100);
    numLonProp.setConfigKey("num_lon_points");
    numLonProp.setMinimum(1);
    numLonProp.registerValueCallback(gridTopoChanged);
    groupProp.addSubProperty(numLonProp);

    deltaLonRadarProp = MDoubleProperty("Longitudinal grid point spacing [m]", 4000.0);
    deltaLonRadarProp.setConfigKey("lon_grid_point_spacing_m");
    deltaLonRadarProp.setMinimum(1.0);
    deltaLonRadarProp.registerValueCallback(gridTopoChanged);
    groupProp.addSubProperty(deltaLonRadarProp);

    deltaLatRadarProp = MDoubleProperty("Latitudinal grid point spacing [m]", 4000.0);
    deltaLatRadarProp.setConfigKey("lat_grid_point_spacing_m");
    deltaLatRadarProp.setMinimum(1.0);
    deltaLatRadarProp.registerValueCallback(gridTopoChanged);
    groupProp.addSubProperty(deltaLatRadarProp);

    deltaLatRadarProp.setEnabled(false);
    deltaLonRadarProp.setEnabled(false);
    numLatProp.setEnabled(true);
    numLonProp.setEnabled(true);
}


MRadarRegridProperties::~MRadarRegridProperties()
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MRadarRegridProperties::addToRequest(MDataRequestHelper *rh)
{
    // Insert number of levels into the request for equidistant levels, or a '/'
    // list of levels if user defined.
    int numLevs = numLevProp;
    if (regridLevelMode == EDPL || regridLevelMode == NONE) // default option.
    {
        rh->insert("RADAR_REGRID_LEVELS", QString("EDPL/%1").arg(numLevs));
    }
    else if (regridLevelMode == EDHL)
    {
        rh->insert("RADAR_REGRID_LEVELS", QString("EDHL/%1").arg(numLevs));
    }
    else
    {
        QVector<float> levels = parseFloatRangeString(userLevelStringProp.value());
        QString levelString("UD");
        for (float level : levels)
        {
            levelString.append(QString("/%1").arg(level));
        }
        rh->insert("RADAR_REGRID_LEVELS", levelString);
    }

    // Insert numbers of longitude and latitude grid points into the request.
    if (numLonProp.isEnabled() && numLatProp.isEnabled()) {
        int numLonPoints = numLonProp;
        int numLatPoints = numLatProp;
        rh->insert("RADAR_REGRID_LONLAT_DISTANCE", QString("NUM_POINTS/%1/%2")
                                                .arg(numLonPoints)
                                                .arg(numLatPoints));
    }
    else // Or step size between grid points in lon. and lat. directions.
    {
        double deltaLonPoints = deltaLonRadarProp;
        double deltaLatPoints = deltaLatRadarProp;
        rh->insert("RADAR_REGRID_LONLAT_DISTANCE", QString("DELTA/%1/%2")
                                                .arg(deltaLonPoints)
                                                .arg(deltaLatPoints));
    }

    rh->insert("RADAR_REGRIDDER_TOPOLOGY_HAS_CHANGED", gridTopologyChanged);
    gridTopologyChanged = false;
}


void MRadarRegridProperties::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    // Pressure level mode (equidistant pressure, eq. height, user defined).
    int readLevel = settings->value("regridLevelMode", regridLevelMode).toInt();
    regridLevelMode = static_cast<MRadarRegridProperties::RegridLevelMode>(readLevel);
    verticalTargetGridModeProp = regridLevelMode;

    // Number of pressure/height levels.
    int defaultLevs = numLevProp;
    int numLevs = settings->value("numLevels", defaultLevs).toInt();
    numLevProp = numLevs;

    // User defined pressure levels.
    QString userLevels = settings->value("userLevels", "").toString();
    userLevelStringProp = userLevels;

    // Switch deciding if lon/lat distance given in number of points or distance.
    int numOrDeltaSwitch = settings->value("numOrDeltaSwitch", 0).toInt();
    horizontalTargetGridModeProp = numOrDeltaSwitch;

    // Number of points in longitude direction.
    int defaultNumLons = numLonProp;
    int numLons = settings->value("numLons", defaultNumLons).toInt();
    numLonProp = numLons;

    // Number of points in latitude direction.
    int defaultNumLats = numLatProp;
    int numLats = settings->value("numLats", defaultNumLats).toInt();
    numLatProp = numLats;

    // Distance between points (in m) in longitude direction.
    double defaultDeltaLon = deltaLonRadarProp;
    double deltaLon = settings->value("deltaLon", defaultDeltaLon).toDouble();
    deltaLonRadarProp = deltaLon;

    // Distance between points (in m) in latitude direction.
    double defaultDeltaLat = deltaLatRadarProp;
    double deltaLat = settings->value("deltaLat", defaultDeltaLat).toDouble();
    deltaLatRadarProp = deltaLat;
}


MRadarRegridProperties::RegridLevelMode
MRadarRegridProperties::stringToRegridLevelMode(QString regridLevelModeName)
{
    if (regridLevelModeName == "equidistant pressure levels")
    {
        return RegridLevelMode::EDPL;
    }
    else if (regridLevelModeName == "equidistant height levels")
    {
        return RegridLevelMode::EDHL;
    }
    else if (regridLevelModeName == "user defined pressure levels")
    {
        return RegridLevelMode::UD;
    }
    else
    {
        return RegridLevelMode::NONE;
    }
}


QString MRadarRegridProperties::regridLevelModeToString(RegridLevelMode regridLevelMode)
{
    switch (regridLevelMode)
    {
    case RegridLevelMode::EDPL:
    {
        return "equidistant pressure levels";
    }
    case RegridLevelMode::EDHL:
    {
        return "equidistant height levels";
    }
    case RegridLevelMode::UD:
    {
        return "user defined pressure levels";
    }
    default:
        return "";
    }
}


} // namespace Met3D
