/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MTOOLBARADDACTORDIALOG_H
#define MTOOLBARADDACTORDIALOG_H

// standard library imports

// related third party imports
#include <QDialog>
#include <QCheckBox>
#include <QDialogButtonBox>
#include <QGridLayout>
#include <QGroupBox>
#include <QLabel>
#include <QLineEdit>

// local application imports

namespace Met3D
{
class MSceneControl;

/**
 * This class encapsulates the dialog shown the actor creation requests
 * a different actor name or a custom set of scenes which the actor should be
 * added to.
 */
class MSelectActorNameSceneDialog : public QDialog
{
    Q_OBJECT
public:
    /**
     * Creates the dialog with the preselected scenes to mark.
     */
    MSelectActorNameSceneDialog(const QString &preselectedName,
                                QVector<MSceneControl *> &preselectedScenes);
    ~MSelectActorNameSceneDialog() override;

    /**
     * Show the dialog where the user can choose a name and scenes for
     * the new actor.
     * @param actorName Reference to the name to be populated.
     * @param scenes Reference to the scenes to be populated with an actor.
     *
     * @return true if user wants to create an actor and the references have
     * been filled.
     */
    static bool handleActorCreation(QString &actorName,
                                    QVector<MSceneControl *> &scenes);

private:
    /**
     * Get the currently selected name from the text box.
     * @return The name currently written in the field.
     */
    QString getSelectedName() const;

    /**
     * Get the currently selected scenes to add the actor to.
     * @return QVector of SceneControls
     */
    QVector<MSceneControl *> getSelectedScenes() const;

    // The UI elements and objects.
    QLabel *actorNameLabel;
    QLineEdit *actorNameEdit;
    QDialogButtonBox *buttonBox;
    QGroupBox *scenesGroupBox;
    QVBoxLayout *vbox;
    QVector<QCheckBox *> sceneBoxes;
    QGridLayout *mainLayout;
};
} // Met3D

#endif //MTOOLBARADDACTORDIALOG_H
