/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef TOOLBAR_H
#define TOOLBAR_H

// standard library imports

// related third party imports
#include <QToolBar>

// local application imports

namespace Met3D
{
/**
  MToolbar implements the Met3D toolbar.
  Currently, it contains icons for the actors for quickly creating them.
  Functionality support is implemented by @c MToolbarElement s.
 */
class MToolbar : public QToolBar
{
    Q_OBJECT
public:
    explicit MToolbar(QWidget *parent);
    ~MToolbar() override;

private:
    /**
     * Register the load button to the toolbar.
     */
    void registerLoadButton();

    /**
     * Register the actor types to the toolbar.
     */
    void registerActorTypes();
};
} // Met3D

#endif //TOOLBAR_H
