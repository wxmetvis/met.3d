/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MTOOLBARELEMENT_H
#define MTOOLBARELEMENT_H

// standard library imports

// related third party imports
#include <QAction>

// local application imports

namespace Met3D
{
/**
  MToolbarElement implements the actions associated with the icons in
  the toolbar. Elements are currently: Load actor button, and actor creation
  buttons.
 */
class MToolbarElement : public QAction
{
    Q_OBJECT

public:
    enum MToolbarElementType
    {
        LOAD_ACTOR_FROM_FILE,
        CREATE_ACTOR,
        SIZE
    };

    /**
     * Creates a button with text as element for the toolbar.
     * @param text The text to display.
     * @param parent Reference to the toolbar.
     */
    MToolbarElement(const QString &text, QObject *parent = nullptr);

    /**
     * Creates a button with icon as element for the toolbar.
     * @param icon The icon to display.
     * @param text The description text along with it.
     * @param parent Reference to the toolbar.
     */
    MToolbarElement(const QIcon &icon, const QString &text,
                    QObject *parent = nullptr);

    /**
     * The overwritten event filter for delegating mouse events we are
     * interested in. Might be a drag or a click.
     */
    bool eventFilter(QObject *watched, QEvent *event) override;

    /**
     * Set the widget associated to this QAction.
     * @param widget The widget.
     */
    void setWidget(QWidget *widget);

public slots:
    /**
     * Slot for finishing a drag event. Resets the element.
     */
    void dragFinished() const;

    /**
     * Called when a button on the toolbar is clicked.
     */
    void clicked() const;

private:
    /**
     * Private methods for handling mouse events
     */
    void mousePressEvent(QMouseEvent *e);
    void mouseMoveEvent(QMouseEvent *e);

    QPoint dragStartPosition; // for drag detection
    MToolbarElementType elementType;
    QWidget *widget;
};
} // Met3D

#endif //MTOOLBARELEMENT_H
