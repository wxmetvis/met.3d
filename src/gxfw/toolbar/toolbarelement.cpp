/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "toolbarelement.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QDrag>
#include <QToolButton>
#include <QApplication>

// local application imports
#include "gxfw/mactor.h"
#include "data/mimedata.h"
#include "gxfw/msceneviewglwidget.h"
#include "selectactornamescenedialog.h"


namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MToolbarElement::MToolbarElement(const QIcon &icon, const QString &text,
                                 QObject *parent)
    : QAction(icon, text, parent),
      elementType(CREATE_ACTOR),
      widget(nullptr)
{
    connect(this, &QAction::triggered, this,
            &MToolbarElement::clicked);
}


MToolbarElement::MToolbarElement(const QString &text, QObject *parent)
    : QAction(text, parent),
      elementType(LOAD_ACTOR_FROM_FILE),
      widget(nullptr)
{
    connect(this, &QAction::triggered, this,
            &MToolbarElement::clicked);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

bool MToolbarElement::eventFilter(QObject *watched, QEvent *event)
{
    QMouseEvent *mouseEvent = dynamic_cast<QMouseEvent *>(event);
    if (event->type() == QEvent::MouseButtonPress)
    {
        mousePressEvent(mouseEvent);
    }
    else if (event->type() == QEvent::MouseMove)
    {
        mouseMoveEvent(mouseEvent);
    }

    return QAction::eventFilter(watched, event);
}


void MToolbarElement::setWidget(QWidget *widget)
{
    this->widget = widget;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MToolbarElement::dragFinished() const
{
    const auto toolButton = dynamic_cast<QToolButton *>(widget);
    // Need to set setDown to false manually, otherwise the icon will stay
    // "down" (pressed).
    if (toolButton)
    {
        toolButton->setDown(false);
    }
}

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

void MToolbarElement::mousePressEvent(QMouseEvent *e)
{
    // If we click, we do not know if this will be a drag event yet.
    // Record starting position so we can create a drag event later.
    if (e->button() == Qt::LeftButton) dragStartPosition = e->pos();
}


void MToolbarElement::mouseMoveEvent(QMouseEvent *e)
{
    if (! (e->buttons() & Qt::LeftButton)) return;
    // Check whether to start a drag event.
    // Decide based on distance traveled since mouseDown.
    if ((e->pos() - dragStartPosition).manhattanLength()
        < QApplication::startDragDistance())
        return;

    // Create a new drag event to drag actors.
    QDrag *drag = new QDrag(this);
    MMimeData *mimeData = nullptr;

    // Check type of this element, and what MimeType it is associated with.
    const QString actorTypeName = text();
    if (elementType == CREATE_ACTOR)
    {
        // Actor drag.
        mimeData = new MMimeData(MMimeType::ACTOR, actorTypeName.toUtf8());
    }
    else if (elementType == LOAD_ACTOR_FROM_FILE)
    {
        // "Load actor" button drag.
        mimeData = new MMimeData(MMimeType::LOAD_ACTOR_BUTTON, QByteArray());
    }

    if (! mimeData)
    {
        delete drag;
        return;
    }

    drag->setMimeData(mimeData);

    // Connect end of life of drag object to a local slot.
    connect(drag, SIGNAL(destroyed()), this, SLOT(dragFinished()));
    drag->exec(Qt::CopyAction);
}


void MToolbarElement::clicked() const
{
    // Get last selected scene.
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MSceneControl *lastSelectedSceneControl = sysMC->
                                              getLastInteractedSceneView()->
                                              getScene();

    auto selectedScenes = QVector<MSceneControl *>({lastSelectedSceneControl});

    // Handle click depending on the type of button.
    if (elementType == CREATE_ACTOR)
    {
        const auto toolButton = dynamic_cast<QToolButton *>(widget);
        if (! toolButton) return;

        QString actorType = toolButton->text();

        // Create and register the actor.
        QString actorName;
        auto *cmd = new MActor::CreateCommand(actorType, actorName, selectedScenes);
        MUndoStack::run(cmd);
    }
    else if (elementType == LOAD_ACTOR_FROM_FILE)
    {
        MAbstractActorFactory::createAndRegisterActorFromFileDialog(
            selectedScenes);
    }
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D
