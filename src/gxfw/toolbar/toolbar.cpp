/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "toolbar.h"

// standard library imports

// related third party imports
#include <QMouseEvent>

// local application imports
#include "toolbarelement.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msystemcontrol.h"

constexpr float TOOLBAR_SCALE = 1.0;
constexpr float TOOLBAR_FONT_SIZE = 20.0 * TOOLBAR_SCALE;
constexpr float TOOLBAR_ICON_SIZE = 40.0 * TOOLBAR_SCALE;

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MToolbar::MToolbar(QWidget* parent)
    : QToolBar("Toolbar", parent)
{
    installEventFilter(this);

    setObjectName("Main Toolbar");
    setIconSize(QSize(TOOLBAR_ICON_SIZE, TOOLBAR_ICON_SIZE));

    QLabel* actorLabel = new QLabel("<b>Actors</b>");
    actorLabel->setStyleSheet(
        QString("font-size: %1px;").arg(TOOLBAR_FONT_SIZE));
    this->addWidget(actorLabel);

    registerLoadButton();
    registerActorTypes();
}


MToolbar::~MToolbar() = default;


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MToolbar::registerActorTypes()
{
    // Populate toolbar.
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    auto mglManager = MGLResourcesManager::getInstance();
    QList<MAbstractActorFactory *> actorFactories = mglManager->
        getActorFactories();

    // For each actor, get its icon (or default fallback) and display it.
    for (MAbstractActorFactory *actorFactory : actorFactories)
    {
        QString actorTypeName = actorFactory->getName();
        QString actorIconName = actorFactory->getIconFileName();

        QString iconFilepath = sysMC->getMet3DHomeDir().absoluteFilePath(
            "resources/icons/actors/" + actorIconName);

        const QIcon icon(iconFilepath);

        // Create a toolbar element.
        auto *tbElement = new MToolbarElement(icon, actorTypeName, this);
        this->addAction(tbElement);
        tbElement->setStatusTip("Create a new actor of type " + actorTypeName);

        // Connect it to the associated widget to receive events.
        QWidget* associatedWidget = widgetForAction(tbElement);
        associatedWidget->installEventFilter(tbElement);
        tbElement->setWidget(associatedWidget);
    }
}


void MToolbar::registerLoadButton()
{
    // Create a toolbar element.
    auto *loadElement = new MToolbarElement("Load...", this);
    this->addAction(loadElement);
    loadElement->setStatusTip("Load an actor from a file.");

    // Connect it to the associated widget to receive events.
    QWidget* associatedWidget = widgetForAction(loadElement);

    associatedWidget->setStyleSheet(QString("font-size: %1px;").arg(TOOLBAR_FONT_SIZE));

    associatedWidget->installEventFilter(loadElement);
    loadElement->setWidget(associatedWidget);
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D
