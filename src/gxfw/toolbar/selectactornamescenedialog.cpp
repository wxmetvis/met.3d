/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "selectactornamescenedialog.h"

// standard library imports

// related third party imports
#include <QLineEdit>

// local application imports
#include "gxfw/mactor.h"
#include "gxfw/mglresourcesmanager.h"


namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectActorNameSceneDialog::MSelectActorNameSceneDialog(
    const QString &preselectedName,
    QVector<MSceneControl *> &preselectedScenes)
    : QDialog(nullptr)
{
    auto allScenes = MGLResourcesManager::getInstance()->getScenes();

    if (preselectedScenes.isEmpty())
    {
        // Caller did not provide any info on scenes. Preselect the first by default.
        preselectedScenes.append(allScenes[0]);
    }

    actorNameEdit = new QLineEdit(this);
    actorNameEdit->setPlaceholderText("Choose name...");
    if (! preselectedName.isEmpty())
    {
        actorNameEdit->setText(preselectedName);
    }
    actorNameEdit->setFocus();
    actorNameLabel = new QLabel("&Name:", this);
    actorNameLabel->setBuddy(actorNameEdit);

    scenesGroupBox = new QGroupBox(tr("Exclusive Radio Buttons"));
    vbox = new QVBoxLayout;

    for (int i = 0; i < allScenes.size(); i++)
    {
        QCheckBox *box = new QCheckBox("Scene &" + QString::number(i + 1));
        box->setChecked(preselectedScenes.contains(allScenes[i]));
        sceneBoxes.append(box);
        vbox->addWidget(box);
    }

    vbox->addStretch(1);
    scenesGroupBox->setLayout(vbox);

    buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok
                                     | QDialogButtonBox::Cancel, this);

    connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
    connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);

    mainLayout = new QGridLayout;
    mainLayout->addWidget(actorNameLabel, 0, 0);
    mainLayout->addWidget(actorNameEdit, 0, 1);
    mainLayout->addWidget(scenesGroupBox, 1, 0, 1, 2);
    mainLayout->addWidget(buttonBox, 2, 0, 1, 2);

    this->setWindowTitle("New actor...");
    this->setLayout(mainLayout);
}


MSelectActorNameSceneDialog::~MSelectActorNameSceneDialog()
{
    // Clean up the UI.
    delete actorNameLabel;
    delete actorNameEdit;
    delete buttonBox;
    delete scenesGroupBox; // Also deletes the vbox.

    delete mainLayout;
    sceneBoxes.clear();
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QString MSelectActorNameSceneDialog::getSelectedName() const
{
    return actorNameEdit->text();
}


QVector<MSceneControl *> MSelectActorNameSceneDialog::getSelectedScenes() const
{
    QVector<MSceneControl *> checkedScenes;
    auto allScenes = MGLResourcesManager::getInstance()->getScenes();

    for (int i = 0; i < sceneBoxes.size(); i++)
    {
        auto &box = sceneBoxes[i];
        if (box->isChecked())
        {
            checkedScenes.append(allScenes[i]);
        }
    }
    return checkedScenes;
}


bool MSelectActorNameSceneDialog::handleActorCreation(
    QString &actorName, QVector<MSceneControl *> &scenes)
{
    MSelectActorNameSceneDialog dialog(actorName, scenes);

    bool actorNameValid = false;
    QString selectedActorName;
    QVector<MSceneControl *> selectedScenes;

    do
    {
        dialog.exec();
        if (dialog.result() == QDialog::Rejected)
        {
            break;
        }
        if (dialog.result() == QDialog::Accepted)
        {
            selectedActorName = dialog.getSelectedName();
            selectedScenes = dialog.getSelectedScenes();

            actorNameValid =
                MAbstractActorFactory::isActorNameAvailableAndValid(
                    selectedActorName);

            if (! actorNameValid)
            {
                // Invalid actor, try again.
                QMessageBox msgBox;
                msgBox.setIcon(QMessageBox::Warning);
                msgBox.setText("Actor name '" + selectedActorName
                               + "' already exists or is invalid."
                               "\nPlease enter a different name.");
                msgBox.exec();
            }
        }
    } while (! actorNameValid);

    scenes = selectedScenes;

    actorName = selectedActorName;
    return actorNameValid;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D
