/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**  Copyright 2024 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mactor.h"

// standard library imports

// related third party imports
#include <QInputDialog>
#include <utility>

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msystemcontrol.h"
#include "mainwindow.h"
#include "toolbar/selectactornamescenedialog.h"
#include "actors/trajectoryactor.h"
#include "gxfw/mscenecontrol.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "util/mfiletypes.h"
#include "util/mfileutils.h"

namespace Met3D
{

// Static counter for all actors that derive from MActor.
unsigned int MActor::idCounter = 0;


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MActor::MActor(QObject *parent, bool supportsShadows)
    : QObject(parent),
      actorIsEnabled(true),
      actorIsPickable(false), // by default actors are not pickable
      actorSupportsShadows(supportsShadows),
      positionLabel(nullptr),
      actorName("Default actor"),
      actorType(staticActorType()),
      actorIsInitialized(false),
      actorChangedSignalDisabledCounter(0),
      actorUpdatesDisabledCounter(0),
      actorIsUserDeletable(true),
      actorSupportsFullScreenVisualisation(false),
      actorSupportsMultipleEnsembleMemberVisualization(false)
{
    // Obtain a "personal" identification number from the static ID counter.
    myID = MActor::idCounter++;

    // Create and initialise QtProperties for the GUI.
    // ===============================================
    actorIsEnabledProp = MBoolProperty(actorName, true);
    actorIsEnabledProp.setConfigKey("actor_enabled");
    actorIsEnabledProp.registerValueCallback([=]()
    {
        actorIsEnabled = actorIsEnabledProp.value();

        onActorEnabledToggle();

        if ((actorChangedSignalDisabledCounter == 0) && isInitialized())
        {
            emit actorChanged();
        }
    });

    deleteActorContextAction = new QAction("Delete");
    connect(deleteActorContextAction, &QAction::triggered, [=]()
    {
        this->deleteActor();
    });
    renameActorContextAction = new QAction("Rename");
    connect(renameActorContextAction, &QAction::triggered, [=]()
    {
        this->renameActor();
    });
    actorIsEnabledProp.addContextMenuAction(renameActorContextAction);
    actorIsEnabledProp.addContextMenuAction(deleteActorContextAction);

    // Development properties.
    actorDevelopmentSupGroupProp = MProperty("Development tools");
    actorIsEnabledProp.addSubProperty(actorDevelopmentSupGroupProp);

    // Actor configuration properties.
    actorPropertiesSupGroup = MArrayProperty("Configuration");
    actorPropertiesSupGroup.setShowPropertyLabels(false);
    actorIsEnabledProp.addSubProperty(actorPropertiesSupGroup);

    loadConfigProp = MButtonProperty("Load", "Load");
    loadConfigProp.registerValueCallback([=]()
    {
        loadConfigurationFromFile();
    });
    actorPropertiesSupGroup.append(&loadConfigProp);
    saveConfigProp = MButtonProperty("Save", "Save");
    saveConfigProp.registerValueCallback([=]()
    {
        saveConfigurationToFile();
    });
    actorPropertiesSupGroup.append(&saveConfigProp);

    renderAsWireFrameProp = MBoolProperty("Wire frame", false);
    renderAsWireFrameProp.setConfigKey("wire_frame_enabled");
    renderAsWireFrameProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorDevelopmentSupGroupProp.addSubProperty(renderAsWireFrameProp);

    reloadShaderProp = MButtonProperty("Reload shaders", "Reload");
    reloadShaderProp.registerValueCallback([=]()
    {
        LOG4CPLUS_INFO(mlog, "Reloading actor shaders...");
        if (isInitialized())
        {
            reloadAllShaderEffects();
            emitActorChangedSignal();
        }
    });
    actorDevelopmentSupGroupProp.addSubProperty(reloadShaderProp);

    printDebugOutputProp = MButtonProperty("Print debug output", "Print");
    printDebugOutputProp.registerValueCallback([=](){ printDebugOutputOnUserRequest(); });
    actorDevelopmentSupGroupProp.addSubProperty(printDebugOutputProp);

    // Label properties.
    labelProps = MPropertyTemplates::Labels(true, 16, {0, 0, 100}, true);
    labelProps.enabledProp.setConfigKey("labels_enabled");
    labelProps.enabledProp.setConfigGroup("label_properties");
    labelProps.enabledProp.registerValueCallback(this, &MActor::emitActorChangedSignal);
    actorPropertiesSupGroup.addSubProperty(labelProps.enabledProp);

    labelProps.fontSizeProp.setConfigKey("label_size");
    labelProps.fontColourProp.setConfigKey("label_colour");
    labelProps.enableBBoxProp.setConfigKey("bounding_box");
    labelProps.bboxColourProp.setConfigKey("bbox_colour");

    if (actorSupportsShadows)
    {
        actorShadowGroupProp = MArrayProperty("Shadows");
        actorShadowGroupProp.setShowPropertyLabels(false);
        actorPropertiesSupGroup.addSubProperty(actorShadowGroupProp);

        actorCastShadowProp = MBoolProperty("Cast shadows", true);
        actorCastShadowProp.setLabel("Cast");
        actorCastShadowProp.setConfigKey("cast_shadows");
        actorCastShadowProp.registerValueCallback([=](){emitActorChangedSignal();});
        actorShadowGroupProp.append(&actorCastShadowProp);

        actorReceiveShadowProp = MBoolProperty("Receive shadows", true);
        actorReceiveShadowProp.setLabel("Receive");
        actorReceiveShadowProp.setConfigKey("receive_shadows");
        actorReceiveShadowProp.registerValueCallback([=](){emitActorChangedSignal();});
        actorShadowGroupProp.append(&actorReceiveShadowProp);
    }

    // Register the actor with the specified resources manager. Note that the
    // resources manager will take care of deleting the actor on program exit.
    // MGLResourcesManager::getInstance()->registerActor(this);
    // NOTE: When registerActor() is called from here, MGLResourcesManager gets
    // only the type information of MActor, not of the derived class (whose
    // constructor has not been executed at this point). Hence, for methods
    // reacting to the actorCreated() signal, there is no way for the actors to
    // determine the correct actor type. The only solution I see at the moment
    // is to call registerActor() externally...
}


MActor::~MActor()
{
    if (positionLabel != nullptr)
    {
        delete positionLabel;
    }

    QList<uint32_t> ids = components.keys();
    for (auto id : ids)
    {
        removeComponentByID(id);
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MActor::initialize()
{
    LOG4CPLUS_INFO(mlog, "Initialising actor ["
                    << getSettingsID() << "] ...");
    if (actorIsInitialized)
    {
        LOG4CPLUS_WARN(mlog, "\tactor has already been initialised, skipping.");
        return;
    }

    // Determine number of available texture/image units. Required for
    // assignTextureUnit() and assignImageUnit().
    GLint numUnits = 0;
    glGetIntegerv(GL_MAX_COMBINED_TEXTURE_IMAGE_UNITS_ARB, &numUnits);
    for (GLint u = 0; u < numUnits; u++)
    {
        availableTextureUnits << u;
        availableImageUnits << u;
    }

    // Get the values of the bounding box connected initially to the actor if
    // the actor is using bounding boxes.
    if (MBoundingBoxInterface *bboxInteface =
            dynamic_cast<MBoundingBoxInterface*>(this))
    {
        enableActorUpdates(false);
        bboxInteface->onBoundingBoxChanged();
        enableActorUpdates(true);
    }

    initializeActorResources();

    for (const auto &comp : components)
    {
        LOG4CPLUS_INFO(mlog, "Initializing actor component "
                        << comp->getName());
        comp->initializeResources();

        // Only 1 since we dont know the number of shader programs in the component.
        beginCompileShaders(1);

        comp->reloadShaderEffects(false);

        MProgressBar::getInstance()->taskCompleted(progressBarHandles.takeFirst());

        endCompileShaders();
    }

    actorIsInitialized = true;
    LOG4CPLUS_INFO(mlog, "... finished initialisation of actor ["
                    << getSettingsID() << "].");
}


void MActor::render(MSceneViewGLWidget *sceneView)
{
    if (!actorIsEnabled) return;
    renderToCurrentContext(sceneView);

    for (const auto& comp : components)
    {
        if (!comp->isEnabled()) continue;
        comp->renderToCurrentContext(sceneView);
    }
}


void MActor::renderToFullScreen(MSceneViewGLWidget *sceneView)
{
    if (!actorIsEnabled) return;
    renderToCurrentFullScreenContext(sceneView);

    for (const auto& comp : components)
    {
        if (!comp->isEnabled()) continue;
        comp->renderToCurrentContext(sceneView);
    }
}

void MActor::renderToUiLayer(MSceneViewGLWidget *sceneView)
{
    if (!actorIsEnabled) return;
    renderToCurrentContextUiLayer(sceneView);

    for (const auto& comp : components)
    {
        if (!comp->isEnabled()) continue;
        comp->renderToUiLayer(sceneView);
    }
}


bool MActor::isInitialized()
{
    return actorIsInitialized;
}


bool MActor::isEnabled() const
{
    return actorIsEnabled;
}

void MActor::reloadAllShaderEffects()
{
    reloadShaderEffects();

    if (components.size() <= 0) return;

    beginCompileShaders(components.size());

    for (const auto& comp : components)
    {
        comp->reloadShaderEffects(true);

        MProgressBar::getInstance()->taskCompleted(progressBarHandles.takeFirst());
    }

    endCompileShaders();
}


unsigned int MActor::getID() const
{
    return myID;
}


void MActor::setName(const QString& name)
{
    QString oldName = actorName;
    actorName = name;
    actorIsEnabledProp.setName(actorName);

    emit actorNameChanged(this, oldName);
}


QString MActor::getName() const
{
    return actorName;
}


void MActor::registerScene(MSceneControl *scene)
{
    if (!scenes.contains(scene))
    {
        scenes.append(scene);
        onSceneViewAdded();
    }
}

void MActor::deregisterScene(MSceneControl *scene)
{
    for (int i = 0; i < scenes.size(); ++i)
    {
        if (scene == scenes[i])
        {
            scenes.removeAt(i);
            break;
        }
    }
}


const QList<MSceneViewGLWidget*> MActor::getViews()
{
    QSet<MSceneViewGLWidget*> views;
    QListIterator<MSceneControl*> sceneIterator(scenes);
    while (sceneIterator.hasNext())
        views.unite(sceneIterator.next()->getRegisteredSceneViews());
    return views.values();
}


QList<MLabel*> MActor::getLabelsToRender()
{
    // If either labels or the actor itself are disabled return an empty list.
    if (actorIsEnabled && labelProps.enabledProp)
    {
        return labels;
    }
    else
    {
        return QList<MLabel*>();
    }
}


QList<MLabel*> MActor::getPositionLabelToRender()
{
    QList<MLabel*> labelList = QList<MLabel*>();
    // Add position label to empty list if the lable is present and the actor
    // and the lable are enabled.
    if (positionLabel != nullptr && actorIsEnabled)
    {
        labelList.append(positionLabel);
    }
    return labelList;
}

int MActor::checkIntersectionWithHandleAndAllComponents(MSceneViewGLWidget *sceneView, float clipX, float clipY)
{
    int handle = this->checkIntersectionWithHandle(sceneView, clipX, clipY);

    if (handle < 0)
    {
        for (const auto& comp : components)
        {
            if (!comp->isEnabled()) continue;
            handle = comp->checkIntersectionWithHandle(sceneView, clipX, clipY);
            if (handle >= 0) break;
        }
    }

    return handle;
}


void MActor::removePositionLabel()
{
    if (positionLabel != nullptr)
    {
        MGLResourcesManager* glRM = MGLResourcesManager::getInstance();
        MTextManager* tm = glRM->getTextManager();
        tm->removeText(positionLabel);
        positionLabel = nullptr;
    }
    emitActorChangedSignal();
}

void MActor::dragEventWithAllComponents(MSceneViewGLWidget *sceneView, int handleID, float clipX, float clipY)
{
    for (const auto &comp : components)
    {
        if (!comp->isEnabled()) continue;
        comp->dragEvent(sceneView, handleID, clipX, clipY);
    }

    this->dragEvent(sceneView, handleID, clipX, clipY);
}

void MActor::releaseEventWithAllComponents(MSceneViewGLWidget *sceneView, int handleID)
{
    for (const auto &comp : components)
    {
        if (!comp->isEnabled()) continue;
        comp->releaseEvent(sceneView, handleID);
    }

    this->releaseEvent(sceneView, handleID);
    dragEventID++;
}


void MActor::setEnabled(bool enabled)
{
    actorIsEnabledProp.setValue(enabled);
}


void MActor::setLabelsEnabled(bool enabled)
{
    labelProps.enabledProp = enabled;
}


QString MActor::getSettingsID()
{
    return "MActor";
}


void MActor::saveConfigurationToFile(QString filename)
{
    if (filename.isEmpty())
    {
        QString directory =
                MSystemManagerAndControl::getInstance()
                ->getMet3DWorkingDirectory().absoluteFilePath("config/actors");
        QDir().mkpath(directory);
        filename = MFileUtils::getSaveFileName(
                    nullptr,
                    "Save actor configuration",
                    FileTypes::M_ACTOR_CONFIG,
                    directory);

        if (filename.isEmpty())
        {
            return;
        }
    }

    LOG4CPLUS_INFO(mlog, "Saving actor configuration to " << filename);

    // Overwrite if the file exists.
    if (QFile::exists(filename))
    {
        auto settings = new QSettings(filename, QSettings::IniFormat);
        bool validConfig;
        if (readConfigVersion(settings) >= ACTOR_PROPERTY_REWORK_COMP_VER)
        {
            settings->beginGroup("Metadata");
            QString configType = settings->value("actorConfigType").toString();
            settings->endGroup();

            validConfig = configType == getActorType();
        }
        else
        {
            QStringList groups = settings->childGroups();
            validConfig = groups.contains(getSettingsID());
        }

        // Only overwrite file if it contains already configuration for the
        // actor to save.
        if (!validConfig)
        {
            QMessageBox msg;
            msg.setWindowTitle("Error");
            msg.setText(QString("The selected file contains a configuration"
                                " other than %1.\n"
                                "This file will NOT be overwritten -- have you"
                                " selected the correct file?")
                        .arg(getSettingsID()));
            msg.setIcon(QMessageBox::Warning);
            msg.exec();
            delete settings;
            return;
        }

        QFile::remove(filename);
    }
    auto* settings = new QSettings(filename, QSettings::IniFormat);

    settings->beginGroup("FileFormat");
    // Save version id of Met.3D.
    settings->setValue("met3dVersion", met3dVersionString);
    settings->endGroup();

    // Save actor settings.
    saveActorConfiguration(settings);

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been saved.");
}


void MActor::loadConfigurationFromFile(QString filename)
{
    if (filename.isEmpty())
    {
        filename = MFileUtils::getOpenFileName(
                    nullptr,
                    "Load actor configuration",
                    FileTypes::M_ACTOR_CONFIG,
                    MSystemManagerAndControl::getInstance()
                    ->getMet3DWorkingDirectory().absoluteFilePath("config/actors"));

        if (filename.isEmpty())
        {
            return;
        }
    }

    LOG4CPLUS_INFO(mlog, "Loading actor configuration from "
                    << filename);

    // While settings are loaded actor updates are disabled.
    enableActorUpdates(false);

    auto* settings = new QSettings(filename, QSettings::IniFormat);

    // Check if config is this actor type.
    bool validConfig;
    if (readConfigVersion(settings) >= ACTOR_PROPERTY_REWORK_COMP_VER)
    {
        settings->beginGroup("Metadata");
        QString configType = settings->value("actorConfigType").toString();
        settings->endGroup();

        validConfig = configType == getActorType();
    }
    else
    {
        QStringList groups = settings->childGroups();
        validConfig = groups.contains(getSettingsID());
    }
    if (!validConfig)
    {
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText("The selected file does not contain configuration data "
                    "for this actor.");
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
        delete settings;
        return;
    }

    // Ask the user to update the actor variables if they are currently not
    // loaded or known to the system manager.
    MSessionManagerDialog::updateActorVariablesForActor(settings, {this});
    // Load actor settings.
    loadActorConfiguration(settings);

    delete settings;

    // Re-enable actor updates.
    enableActorUpdates(true);

    LOG4CPLUS_INFO(mlog, "... configuration has been loaded.");

    // Signal that the actor properties have changed.
    emitActorChangedSignal();
}


void MActor::saveActorConfiguration(QSettings *settings)
{
    // Explicitly save the type of this actor into a Metadata group,
    // so that we can check it while loading or overwriting.
    settings->beginGroup("Metadata");
    settings->setValue("actorConfigType", getActorType());
    settings->endGroup();

    settings->beginGroup(MActor::getSettingsID());

    settings->setValue("actorName", actorName);

    // Save configuration to restore actor state without the specific actor properties.
    settings->beginGroup("header");
    saveConfigurationHeader(settings);
    settings->endGroup(); // Actor settings header.

    // Save properties. This saves all properties of the actor that are specified
    // to be saved.
    settings->beginGroup("properties");
    actorIsEnabledProp.saveAllToConfiguration(settings);
    settings->endGroup(); // Property settings group

    settings->endGroup(); // Actor settings group

    // Save derived classes settings that are not part of the properties.
    saveConfiguration(settings);

    // Save component settings that are not saved as part of the properties.
    settings->beginGroup("components");

    for (const auto &comp : components)
    {
        comp->saveConfiguration(settings);
    }

    settings->endGroup(); // Actor components settings group
}


void MActor::loadActorConfiguration(QSettings *settings)
{
    settings->beginGroup(MActor::getSettingsID());

    const QString name = settings->value("actorName").toString();

    // Reject new actor name if it is invalid or the name does already exist.
    // (If the name of the actor stays the same, the name also already exists
    // but it also does not need to be changed).
    if (!MGLResourcesManager::getInstance()->getActorByName(name)
            && isValidObjectName(name))
    {
        setName(name);
    }
    else if (name != actorName)
    {
        setName(MAbstractActorFactory::generateActorName(name));
    }

    // Compatibility code to load actor configurations from before the property rework.
    if (readConfigVersion(settings) < ACTOR_PROPERTY_REWORK_COMP_VER)
    {
        settings->endGroup(); // Actor settings group

        loadConfigurationPrior_V_1_14(settings);

        // Load components last, as the child actor might create them while loading.
        settings->beginGroup("components");

        for (const auto &comp : components)
        {
            comp->loadConfigurationPrior_V_1_14(settings);
        }

        settings->endGroup();
        return;
    }


    // Load configuration to restore actor state without the specific actor properties.
    settings->beginGroup("header");
    loadConfigurationHeader(settings);
    settings->endGroup(); // Actor settings header.

    // Load properties. This loads all properties of the actor that are specified
    // to be saved. This REQUIRES all properties that were part of the property
    // tree when the actor was saved to be present while loading.
    settings->beginGroup("properties");
    actorIsEnabledProp.loadAllFromConfiguration(settings);
    settings->endGroup(); // Property settings group

    settings->endGroup(); // Actor settings group

    // Load derived classes settings that are not part of the properties.
    loadConfiguration(settings);

    // Load component settings that are not saved as part of the properties.
    settings->beginGroup("components");

    for (const auto &comp : components)
    {
        comp->loadConfiguration(settings);
    }

    settings->endGroup(); // Actor components settings group
}


void MActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    settings->beginGroup(MActor::getSettingsID());

    actorIsEnabledProp = settings->value("actorIsEnabled", true).toBool();

    labelProps.enabledProp = settings->value("labelsAreEnabled", true).toBool();

    renderAsWireFrameProp = settings->value("renderAsWireFrame", false).toBool();

    labelProps.fontColourProp = settings->value("labelColour", QColor(0, 0, 100)).value<QColor>();

    labelProps.fontSizeProp = settings->value("labelSize", 16).toInt();

    labelProps.enableBBoxProp = settings->value("labelBBox", true).toBool();

    labelProps.bboxColourProp = settings->value("labelBBoxColour",
                                          QColor(255, 255, 255, 200)).value<QColor>();

    if (actorSupportsShadows)
    {
        actorCastShadowProp = settings->value("castShadow", true).toBool();

        actorReceiveShadowProp = settings->value("receiveShadow", true).toBool();
    }

    settings->endGroup(); // Actor settings group
}


void MActor::collapseActorPropertyTree()
{
    actorIsEnabledProp.collapse();
}


void MActor::setActorIsUserDeletable(bool b)
{
    actorIsUserDeletable = b;

    if (!actorIsUserDeletable)
    {
        actorIsEnabledProp.removeContextMenuAction(deleteActorContextAction);
    }
    else
    {
        actorIsEnabledProp.addContextMenuAction(deleteActorContextAction);
    }
}


void MActor::deleteActor()
{
    // Is this actor allowed to be deleted by a user?
    if (!getActorIsUserDeletable())
    {
        QMessageBox msgBox;
        msgBox.setText("This actor has a special role and cannot be deleted.");
        msgBox.exec();
        return;
    }

    // Ask the user if he wants to delete this actor, showing connected actors.
    if (!getConfirmationForDeletion())
    {
        return;
    }

    DeleteCommand *cmd = new DeleteCommand(this);
    MUndoStack::run(cmd);
}


bool MActor::getConfirmationForDeletion()
{
    auto* glRM = MGLResourcesManager::getInstance();

    // Check if the user really wants to delete the actor.
    QString msg = "Do you really want to remove actor ''" + actorName +
            "''?";

    // If the actor to be deleted is connected to other actors (e.g. transfer
    // functions), print a warning.
    QList<MActor *> connectedActors = glRM->getActorsConnectedTo(this);
    if (! connectedActors.empty())
    {
        msg += "\n\n\rWARNING: ''" + actorName
                + "'' is connected to the following actors:\n\n\r";

        for (const MActor *a : connectedActors) msg += a->getName() + "\n\r";
    }

    QMessageBox yesNoBox;
    yesNoBox.setWindowTitle("Delete actor");
    yesNoBox.setText(msg);
    yesNoBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    yesNoBox.setDefaultButton(QMessageBox::No);

    // Return the yes/no selection
    return (yesNoBox.exec() == QMessageBox::Yes);
}


QString MActor::renameActor()
{
    QString newName(getName());
    while (!MAbstractActorFactory::isActorNameAvailableAndValid(newName))
    {
        bool ok = false;
        newName = QInputDialog::getText(
                nullptr, "Rename actor",
                "Please enter a new name for the actor:",
                QLineEdit::Normal,
                newName, &ok);

        if (!ok) return "";
    }

    if (newName == getName()) return "";

    // Check if name is valid, if yes, we can rename.
    if (!MAbstractActorFactory::isActorNameAvailableAndValid(newName))
    {
        return "";
    }

    auto cmd = new RenameCommand(this, newName);
    MUndoStack::run(cmd);

    return newName;
}


std::shared_ptr<MActorComponent> MActor::getComponent(const unsigned int componentId) const
{
    if (components.contains(componentId))
    {
        return components[componentId];
    }

    return std::shared_ptr<MActorComponent>(nullptr);
}


bool MActor::removeComponentByID(unsigned int componentId)
{
    if (components.contains(componentId))
    {
        // Disconnect signals
        components[componentId]->disconnect(this);
        this->disconnect(components[componentId].get());
        // Remove properties.
        components[componentId]->removeProperties(&actorPropertiesSupGroup);
        // Reset parent.
        components[componentId]->parent = nullptr;
        // Remove component.
        components.remove(componentId);
        return true;
    }

    return false;
}


bool MActor::castsShadows() const
{
    return actorSupportsShadows && actorCastShadowProp;
}


bool MActor::receivesShadows() const
{
    return actorSupportsShadows && actorReceiveShadowProp;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


void MActor::actOnOtherActorCreated(MActor *actor)
{
    onOtherActorCreated(actor);
}


void MActor::actOnOtherActorDeleted(MActor *actor)
{
    onOtherActorDeleted(actor);
}


void MActor::actOnOtherActorRenamed(MActor *actor, QString oldName)
{
    onOtherActorRenamed(actor, std::move(oldName));
}


void MActor::emitActorChangedSignal()
{
    if ((actorChangedSignalDisabledCounter == 0) && actorIsEnabled
            && isInitialized() && (actorUpdatesDisabledCounter == 0))
    {
        emit actorChanged();
    }
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

bool MActor::suppressActorUpdates()
{
    return ( !((actorUpdatesDisabledCounter == 0) && isInitialized()) );
}


void MActor::enableEmissionOfActorChangedSignal(bool enabled)
{
    if (enabled)
        actorChangedSignalDisabledCounter = std::min(0, actorChangedSignalDisabledCounter + 1);
    else
        actorChangedSignalDisabledCounter--;
}


void MActor::beginCompileShaders(int numberOfShaders)
{
    // Register tasks for each shader to the progress bar.
    auto taskHandles = MProgressBar::getInstance()->addTasks(numberOfShaders, "Compiling OpenGL GLSL shaders...");
    progressBarHandles.append(taskHandles);
}


void MActor::endCompileShaders()
{
}


void MActor::compileShadersFromFileWithProgressDialog(
        std::shared_ptr<GL::MShaderEffect> shader,
        const QString filename)
{
    shader->compileFromFile_Met3DHome(filename);
    MProgressBar::getInstance()->taskCompleted(progressBarHandles.takeFirst());
}


void MActor::enableActorUpdates(bool enable)
{
    if (enable)
        actorUpdatesDisabledCounter = std::min(0, actorUpdatesDisabledCounter + 1);
    else
        actorUpdatesDisabledCounter--;
}


GLint MActor::assignTextureUnit()
{
    if (availableTextureUnits.size() > 0)
    {
        GLint unit = availableTextureUnits.takeFirst();
        assignedTextureUnits << unit;

        LOG4CPLUS_DEBUG(mlog, "Assigning texture unit " << unit);

        return unit;
    }
    else
    {
        LOG4CPLUS_ERROR(mlog, "No texture units available anymore!");
        return -1;
    }
}


void MActor::releaseTextureUnit(GLint unit)
{
    LOG4CPLUS_DEBUG(mlog, "Releasing texture unit " << unit);

    if (assignedTextureUnits.contains(unit))
    {
        assignedTextureUnits.removeOne(unit);
        availableTextureUnits << unit;
        // Sort the available units so that the lowest value is chosen next
        // time requestTextureUnit() is called.
        std::sort(availableTextureUnits.begin(), availableTextureUnits.end());
    }
    else
    {
        LOG4CPLUS_ERROR(mlog, "Failure at attempt to release not assigned "
                        "texture unit!");
    }
}


GLint MActor::assignImageUnit()
{
    if (availableImageUnits.size() > 0)
    {
        GLint unit = availableImageUnits.takeFirst();
        assignedImageUnits << unit;

        LOG4CPLUS_DEBUG(mlog, "Assigning image unit " << unit);

        return unit;
    }
    else
    {
        LOG4CPLUS_ERROR(mlog, "No image units available anymore!");
        return -1;
    }
}


void MActor::releaseImageUnit(GLint unit)
{
    LOG4CPLUS_DEBUG(mlog, "Releasing image unit " << unit);

    if (assignedImageUnits.contains(unit))
    {
        assignedImageUnits.removeOne(unit);
        availableImageUnits << unit;
        // Sort the available units so that the lowest value is chosen next
        // time requestImageUnit() is called.
        std::sort(availableImageUnits.begin(), availableImageUnits.end());
    }
    else
    {
        LOG4CPLUS_ERROR(mlog, "Failure at attempt to release not assigned "
                        "image unit!");
    }
}


void MActor::removeAllLabels()
{
    // Remove all text labels of the old geometry.
    while ( !labels.isEmpty() )
        MGLResourcesManager::getInstance()->getTextManager()
                ->removeText(labels.takeLast());
}


void MActor::uploadVec3ToVertexBuffer(const QVector<QVector3D>& data,
                              const QString requestKey,
                              GL::MVertexBuffer** vbo,
                              QGLWidget* currentGLContext)
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    GL::MVertexBuffer* vb = static_cast<GL::MVertexBuffer*>(
                                            glRM->getGPUItem(requestKey));

    if (vb)
    {
        *vbo = vb;
        GL::MVector3DVertexBuffer* buf = dynamic_cast<GL::MVector3DVertexBuffer*>(vb);
        // reallocate buffer if size has changed
        buf->reallocate(nullptr, data.size(), 0, false, currentGLContext);
        buf->update(data, 0, 0, currentGLContext);

    } else {

        GL::MVector3DVertexBuffer* newVB = nullptr;
        newVB = new GL::MVector3DVertexBuffer(requestKey, data.size());
        if (glRM->tryStoreGPUItem(newVB)) { newVB->upload(data, currentGLContext); }
        else { delete newVB; }
        *vbo = static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));
    }
}

void MActor::uploadVec4ToVertexBuffer(const QVector<QVector4D>& data,
                              const QString requestKey,
                              GL::MVertexBuffer** vbo,
                              QGLWidget* currentGLContext)
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    GL::MVertexBuffer* vb = static_cast<GL::MVertexBuffer*>(
                                            glRM->getGPUItem(requestKey));

    if (vb)
    {
        *vbo = vb;
        GL::MVector4DVertexBuffer* buf = dynamic_cast<GL::MVector4DVertexBuffer*>(vb);
        // reallocate buffer if size has changed
        buf->reallocate(nullptr, data.size(), 0, false, currentGLContext);
        buf->update(data, 0, 0, currentGLContext);

    }
    else
    {

        GL::MVector4DVertexBuffer* newVB = nullptr;
        newVB = new GL::MVector4DVertexBuffer(requestKey, data.size());
        if (glRM->tryStoreGPUItem(newVB)) { newVB->upload(data, currentGLContext); }
        else { delete newVB; }
        *vbo = static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));
    }
}

void MActor::uploadVec2ToVertexBuffer(const QVector<QVector2D>& data,
                              const QString requestKey,
                              GL::MVertexBuffer** vbo,
                              QGLWidget* currentGLContext)
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    GL::MVertexBuffer* vb = static_cast<GL::MVertexBuffer*>(
                                            glRM->getGPUItem(requestKey));

    if (vb)
    {
        *vbo = vb;
        GL::MVector2DVertexBuffer* buf = dynamic_cast<GL::MVector2DVertexBuffer*>(vb);
        // reallocate buffer if size has changed
        buf->reallocate(nullptr, data.size(), 0, false, currentGLContext);
        buf->update(data, 0, 0, currentGLContext);

    } else {

        GL::MVector2DVertexBuffer* newVB = nullptr;
        newVB = new GL::MVector2DVertexBuffer(requestKey, data.size());
        if (glRM->tryStoreGPUItem(newVB)) { newVB->upload(data, currentGLContext); }
        else { delete newVB; }
        *vbo = static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));
    }
}


void MActor::uploadFloatToVertexBuffer(const QVector<float>& data,
                                      const QString requestKey,
                                      GL::MVertexBuffer** vbo,
                                      QGLWidget* currentGLContext)
{
    MGLResourcesManager* glRM = MGLResourcesManager::getInstance();

    GL::MVertexBuffer* vb = static_cast<GL::MVertexBuffer*>(
            glRM->getGPUItem(requestKey));

    if (vb)
    {
        *vbo = vb;
        GL::MFloatVertexBuffer* buf = dynamic_cast<GL::MFloatVertexBuffer*>(vb);
        // reallocate buffer if size has changed
        buf->reallocate(nullptr, data.size(), 0, false, currentGLContext);
        buf->update(data, 0, 0, currentGLContext);

    } else {

        GL::MFloatVertexBuffer* newVB = nullptr;
        newVB = new GL::MFloatVertexBuffer(requestKey, data.size());
        if (glRM->tryStoreGPUItem(newVB)) { newVB->upload(data, currentGLContext); }
        else { delete newVB; }
        *vbo = static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(requestKey));
    }
}


void MActor::uploadVec3ToVertexBuffer(const QVector<QVector3D> *data, GLuint *vbo)
{
    // Delete the old VBO. If this is the first time the method has been called
    // (i.e. no VBO exists), "vbo" is set to 0 (constructor!); glDeleteBuffers
    // ignores 0 buffers.
    glDeleteBuffers(1, vbo); CHECK_GL_ERROR;

    // Generate new VBO and upload geometry data to GPU.
    glGenBuffers(1, vbo); CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, *vbo); CHECK_GL_ERROR;
    glBufferData(GL_ARRAY_BUFFER,
                 sizeof(float) * data->size() * 3,
                 data->constData(),
                 GL_STATIC_DRAW); CHECK_GL_ERROR;
    glBindBuffer(GL_ARRAY_BUFFER, 0); CHECK_GL_ERROR;
}


double MActor::computePositionLabelDistanceWeight(
        MCamera* camera, QVector3D mousePosWorldSpace)
{

    // Calculate distance of label to handle with repsect to distance of
    // handle and camera. (Avoid too large distance for camera near handle
    // and too small distance for camera far away from handle.)
    QVector3D zAxis = camera->getZAxis();
    zAxis.normalize();
    QVector3D camPos = camera->getOrigin();
    double dist = -(QVector3D::dotProduct(zAxis, camPos)
                    - sqrt(QVector3D::dotProduct(mousePosWorldSpace,
                                                 mousePosWorldSpace)));
    dist *= dist * 0.00003;

    return dist;
}


void MActor::printDebugOutputOnUserRequest()
{
    LOG4CPLUS_DEBUG(mlog, "No debug output has been provided for this actor.");
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/




/******************************************************************************
***                         MAbstractActorFactory                           ***
*******************************************************************************/

MAbstractActorFactory::MAbstractActorFactory()
    : isInitialized(false)
{
}


MActor *MAbstractActorFactory::create(QString& actorName, const QString &configfile)
{
    LOG4CPLUS_INFO(mlog, "Creating new default instance of <"
                    << getName() << "> ...");

    MActor* actor = createInstance();

    if (!configfile.isEmpty()) actor->loadConfigurationFromFile(configfile);

    // Update the actor name if provided.
    if (!actorName.isEmpty())
    {
        actor->setName(actorName);
    }
    actorName = actor->getName();

    LOG4CPLUS_INFO(mlog, "... instance of <"
                    << getName() << "> has been created.");

    return actor;
}


MActor *MAbstractActorFactory::create()
{
    QString tempName = "";
    return create(tempName);
}


MActor *MAbstractActorFactory::createFromConfig(const QString &configfile)
{
    QString tempName = "";
    return create(tempName, configfile);
}


MActor *MAbstractActorFactory::createAndRegisterActor(QString &actorName,
    QVector<MSceneControl *> &scenesToAdd, const QString &configfile,
    Qt::KeyboardModifiers mods, bool skipInit)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    QVector<MActor*> actorsBeforeCreation = glRM->getActors();

    // Create actor based on optional config file and name.
    MActor *actor = this->create(actorName, configfile);

    QVector<MActor*> actorsAfterCreation = glRM->getActors();
    // Check for actors that got created along with this actor, e.g., actors
    // depending on other actors. If there are such actors, already add these
    // to the same scenes this actor should be added to.
    for (MActor* actorAfterCreation : actorsAfterCreation)
    {
        if (! actorsBeforeCreation.contains(actorAfterCreation)
            && actorAfterCreation != actor)
        {
            for (MSceneControl *scene : scenesToAdd)
            {
                scene->addActor(actorAfterCreation, -1);
            }
        }
    }

    // If Alt pressed or name already exists -> window to handle new actor names.
    if ((mods & Qt::AltModifier))
    {
        // Alt-Modifier: Create from dialog info.
        if (! MSelectActorNameSceneDialog::handleActorCreation(
            actorName, scenesToAdd))
        {
            return nullptr;
        }
    }
    else if (glRM->getActorByName(actorName))
    {
        actorName = generateActorName(actorName);

        if (! MSelectActorNameSceneDialog::handleActorCreation(
                actorName, scenesToAdd))
        {
            return nullptr;
        }
    }
    else if (actorName.isEmpty())
    {
        actorName = generateActorName(this->name);
    }

    actor->setName(actorName);

    actor->setEnabled(true);

    if (!skipInit)
    {
        // Initialize all shaders and graphical resources of actor.
        actor->initialize();
    }

    // Register actor in resource manager.
    glRM->registerActor(actor);

    // If scenes provided to add: add the actor to the end of their list.
    for (MSceneControl *scene : scenesToAdd)
    {
        scene->addActor(actor, -1);
    }

    return actor;
}


/******************************************************************************
***                           Static interface                              ***
*******************************************************************************/

MActor *MAbstractActorFactory::createAndRegisterActor(const QString &actorType,
    QString &actorName, QVector<MSceneControl *> &scenes, bool skipInit)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    for (MAbstractActorFactory *factory : glRM->getActorFactories())
    {
        if (factory->getName() == actorType)
        {
            return factory->createAndRegisterActor(actorName, scenes, "", {}, skipInit);
        }
    }

    LOG4CPLUS_WARN(
        mlog, "Could not infer actor type from given actor type string '"
        << actorType << "'. Actor creation failed.");
    return nullptr;
}


MActor *MAbstractActorFactory::createAndRegisterActor(const QString &actorType,
    QString &actorName, bool skipInit)
{
    auto scenes = QVector<MSceneControl *>();
    return createAndRegisterActor(actorType, actorName, scenes, skipInit);
}


MActor *MAbstractActorFactory::createAndRegisterActor(const QString &actorType)
{
    QString actorName = generateActorName(actorType);
    return createAndRegisterActor(actorType, actorName);
}


MActor *MAbstractActorFactory::createAndRegisterActorFromFile(
    const QString &configfile, QVector<MSceneControl *> &scenes,
    Qt::KeyboardModifiers mods)
{
    LOG4CPLUS_INFO(mlog, "Loading configuration file "
                    << configfile << " ...");
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Find actor that can be created with the specified config file and
    // create a new instance.
    for (MAbstractActorFactory *factory : glRM->getActorFactories())
    {
        if (factory->acceptSettings(configfile))
        {
            QString actorName = "";
            MActor *actor = factory->createAndRegisterActor(
                actorName, scenes, configfile, mods);

            if (! actor)
            {
                LOG4CPLUS_WARN(
                    mlog, "Could not create actor from configuration file '"
                    << configfile << "'.");
                return nullptr;
            }
            return actor;
        }
    }
    LOG4CPLUS_WARN(
        mlog, "Could not determine actor type from configuration file '"
        << configfile << "'");
    return nullptr;
}


MActor *MAbstractActorFactory::createAndRegisterActorFromFileDialog(
    QVector<MSceneControl *> scenes)
{
    // Save the current keyboard modifiers. User probably released modifier
    // after he chose the file in the dialog.
    Qt::KeyboardModifiers mods = QGuiApplication::keyboardModifiers();

    QString configfile = MFileUtils::getOpenFileName(nullptr,
        "Load actor configuration", FileTypes::M_ACTOR_CONFIG,
        MSystemManagerAndControl::getInstance()
        ->getMet3DWorkingDirectory().absoluteFilePath("config/actors"));

    if (configfile.isEmpty()) return nullptr;

    auto *cmd = new MActor::CreateCommand(configfile, std::move(scenes), mods);
    MUndoStack::run(cmd);

    return cmd->getCreatedActor();
}


bool MAbstractActorFactory::actorExists(const QString &name)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    return glRM->getActorByName(name) != nullptr;
}


bool MAbstractActorFactory::isActorNameAvailableAndValid(
    const QString &actorName)
{
    return isValidObjectName(actorName) && actorName != "" && ! actorExists(
               actorName);
}


bool MAbstractActorFactory::renameActor(MActor *actor, const QString& newName)
{
    if (! actor) return false;
    if (! isActorNameAvailableAndValid(newName)) return false;

    // Change name of actor (also changes name of property browser).
    actor->setName(newName);
    return true;
}


void MAbstractActorFactory::deleteActor(MActor *actor)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    QString actorName = actor->getName();

    QList<MSceneControl *> actorScenes = actor->getScenes();

    // Remove actor from its current scenes.
    for (MSceneControl *scene : actorScenes)
    {
        scene->removeActorByName(actorName);
    }

    // Delete the resources of the actor.
    glRM->deleteActor(actor);
}


QString MAbstractActorFactory::generateActorName(const QString &actorType)
{
    QString actorName = actorType;
    uint copies = 0;

    // Count up until we find an unused actor name
    // e.g. Graticule -> Graticule (1) -> Graticule (2) ...
    while (actorExists(actorName))
    {
        copies++;
        actorName = actorType + " (%1)";
        actorName = actorName.arg(copies);
    }

    return actorName;
}


bool MAbstractActorFactory::acceptSettings(QSettings *settings)
{
    if (readConfigVersion(settings) < ACTOR_PROPERTY_REWORK_COMP_VER)
    {
        QStringList groups = settings->childGroups();
        return groups.contains(settingsID);
    }
    else
    {
        settings->beginGroup("Metadata");
        QString fName = settings->value("actorConfigType").toString();
        settings->endGroup();

        return fName == name;
    }
}


bool MAbstractActorFactory::acceptSettings(const QString& configfile)
{
    QSettings* settings = new QSettings(configfile, QSettings::IniFormat);
    bool accept = acceptSettings(settings);
    delete settings;
    return accept;
}


void MAbstractActorFactory::displayWarningExperimentalStatus()
{
    if (isInitialized)
    {
        QString text = QString(
                    "Actors of type '%1' are still in experimental "
                    "status. Some options may not yet be fully functional, "
                    "there also may be bugs. Please double-check "
                    "when interpreting the displayed data.").arg(getName());
        QMessageBox msg;
        msg.setWindowTitle("Warning");
        msg.setText(text);
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
    }
}


MActor::RenameCommand::RenameCommand(MActor *actor, QString newName)
        : QUndoCommand("Rename actor"),
          actor(actor),
          newName(std::move(newName)),
          oldName(actor->getName())
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    destroyCallback = connect(glRM, &MGLResourcesManager::actorDeleted,
    [=](MActor *actor)
    {
        if (this->actor == actor)
        {
            setObsolete(true);
            disconnect(destroyCallback);
        }
    });
}


void MActor::RenameCommand::undo()
{
    if (!MAbstractActorFactory::renameActor(actor, oldName))
    {
        LOG4CPLUS_WARN(mlog, "Failed to rename the actor.");
    }
}


void MActor::RenameCommand::redo()
{
    if (!MAbstractActorFactory::renameActor(actor, newName))
    {
        LOG4CPLUS_WARN(mlog, "Failed to rename the actor.");
    }
}


MActor::DeleteCommand::DeleteCommand(MActor *actor)
        : QUndoCommand("Delete actor"),
          actor(actor)
{
    actorType = actor->getActorType();
    actorName = actor->getName();

    scenes = actor->getScenes();

    for (auto scene : scenes)
    {
        sceneRenderIDs.append(scene->getActorRenderID(actorName));
    }

    actor->saveActorConfiguration(actorConfig.settings);
}


void MActor::DeleteCommand::undo()
{
    actor = MAbstractActorFactory::createAndRegisterActor(actorType, actorName);

    if (actor == nullptr)
    {
        setObsolete(true);
        return;
    }

    actor->loadActorConfiguration(actorConfig.settings);

    int i = 0;
    for (auto scene : scenes)
    {
        scene->addActor(actor, sceneRenderIDs[i]);
        i++;
    }
}


void MActor::DeleteCommand::redo()
{
    if (actor == nullptr)
    {
        setObsolete(true);
        return;
    }

    // delete the actor
    MAbstractActorFactory::deleteActor(actor);
    actor = nullptr;
}


MActor::CreateCommand::CreateCommand(QString actorType, QString actorName,
                                     const QVector<MSceneControl *> &scenes)
        : QUndoCommand("Create Actor"),
          actorType(std::move(actorType)),
          actorName(std::move(actorName)),
          actorConfig(""),
          scenes(scenes)
{
}


MActor::CreateCommand::CreateCommand(QString actorConfig,
                                     QVector<MSceneControl *> scenes,
                                     Qt::KeyboardModifiers mods)
        : QUndoCommand("Create Actor"),
          actorType(),
          actorName(),
          actorConfig(std::move(actorConfig)),
          scenes(std::move(scenes)),
          mods(mods)
{}


void MActor::CreateCommand::undo()
{
    if (actorName.isEmpty())
    {
        setObsolete(true);
        return;
    }

    createdActor = MGLResourcesManager::getInstance()->getActorByName(actorName);

    if (createdActor == nullptr)
    {
        setObsolete(true);
        return;
    }

    MAbstractActorFactory::deleteActor(createdActor);
}


void MActor::CreateCommand::redo()
{
    if (actorConfig.isEmpty())
    {
        createdActor = MAbstractActorFactory::createAndRegisterActor(actorType,
                                                                     actorName,
                                                                     scenes);

        if (actorName.isEmpty() && createdActor != nullptr)
        {
            actorName = createdActor->getName();
            scenes = createdActor->getScenes().toVector();
        }
    }
    else
    {
        createdActor = MAbstractActorFactory::createAndRegisterActorFromFile(
                actorConfig, scenes, mods);
        mods = {};
        scenes = createdActor->scenes.toVector();
        actorName = createdActor->getName();
    }

    if (createdActor == nullptr)
    {
        setObsolete(true);
    }
}


MActor::DuplicateCommand::DuplicateCommand(MActor *actor, const QString &newActorName)
: QUndoCommand("Duplicate Actor"),
  originalActor(actor),
  originalActorName(actor->getName()),
  createdActorName(newActorName),
  createdActor(nullptr)
{}


void MActor::DuplicateCommand::undo()
{
    if (createdActorName.isEmpty())
    {
        setObsolete(true);
        return;
    }

    createdActor = MGLResourcesManager::getInstance()->getActorByName(createdActorName);

    if (createdActor == nullptr)
    {
        setObsolete(true);
        return;
    }

    MAbstractActorFactory::deleteActor(createdActor);
}


void MActor::DuplicateCommand::redo()
{
    // Failsafe if the original pointer was deleted.
    originalActor = MGLResourcesManager::getInstance()->getActorByName(originalActorName);

    // Create a new actor instance.
    MActor* copyActor = MAbstractActorFactory::createAndRegisterActor(originalActor->getActorType(), createdActorName, scenes);

    // Use QSettings object to copy the actor settings over.
    MTempConfigFile config;
    QSettings *settings = config.settings;

    // Copy the settings from the original actor to the copy actor.
    originalActor->saveActorConfiguration(settings);
    copyActor->loadActorConfiguration(settings);

    // Set name of the actor to the copy name, as it is currently the original actors name, and re-enable it.
    copyActor->setName(createdActorName);
    copyActor->setEnabled(true);

    scenes = copyActor->getScenes().toVector();

    createdActor = copyActor;
}
} // namespace Met3D

