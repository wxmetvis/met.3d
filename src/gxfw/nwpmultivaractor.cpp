/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2018 Bianca Tost [+]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "nwpmultivaractor.h"

// standard library imports
#include <limits>

// related third party imports
#include <QtCore>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "gxfw/mglresourcesmanager.h"
#include "gxfw/msceneviewglwidget.h"
#include "gxfw/mscenecontrol.h"
#include "actors/nwpverticalsectionactor.h"
#include "data/netcdfwriter.h"
#include "datasource/selectdatavariabledialog.h"
#include "mainwindow.h"
#include "util/mfiletypes.h"
#include "util/mfileutils.h"
#include "system/mpyinterface.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MNWPMultiVarActor::MNWPMultiVarActor()
    : MActor(),
      analysisControl(nullptr)
{
    // Create and initialise properties for the GUI.
    // ===============================================

    // Property group for the variable properties.
    variablesGroupProp = MProperty("Variables");
    variablesGroupProp.setConfigGroup("actor_variables");
    actorIsEnabledProp.addSubProperty(variablesGroupProp);

    addVariableProp = MButtonProperty("Add new variable", "Add");
    addVariableProp.registerValueCallback([=]()
    {
        addActorVariable(true);
        emitActorChangedSignal();
    });
    variablesGroupProp.addSubProperty(addVariableProp);

    // Export variables to NetCDF file
    exportVariablesToFileProp = MButtonProperty("Export variables to file", "Export");
    exportVariablesToFileProp.setTooltip(
        "Exports the currently loaded actor variables to a NetCDF file.");

    exportVariablesToFileProp.registerValueCallback([=]()
    {
        QString filename = MFileUtils::getSaveFileName(
                nullptr, "Export to NetCDF file", FileTypes::M_NETCDF);
        if (!filename.isEmpty())
            netCDF::MNetCDFWriter::write(getNWPVariables(), filename);
    });
    variablesGroupProp.addSubProperty(exportVariablesToFileProp);

    // Export variables to Python
    exportVariablesToPythonGroupProp = MButtonProperty("Export variables to python", "Export");
    exportVariablesToPythonGroupProp.registerValueCallback([=]()
    {
        QString file = exportToPythonFilenameProp;
        QString funcName = exportToPythonFunctionProp;

        MPyInterface::exportVariables(getNWPVariables(), file, funcName);
    });
    variablesGroupProp.addSubProperty(exportVariablesToPythonGroupProp);


    exportToPythonFilenameProp = MFileProperty("Python file", "");
    exportToPythonFilenameProp.setConfigKey("export_python_file");
    exportToPythonFilenameProp.setTooltip("The file containing the method to be called.");
    exportToPythonFilenameProp.setFileFilter(FileTypes::M_PYTHON);
    exportVariablesToPythonGroupProp.addSubProperty(exportToPythonFilenameProp);

    exportToPythonFunctionProp = MStringProperty("Target function", "");
    exportToPythonFunctionProp.setConfigKey("export_python_function");
    exportToPythonFunctionProp.setTooltip("The function name to be called. This function will get invoked "
                                               "with a xarray.Dataset as argument containing the current variables.");
    exportVariablesToPythonGroupProp.addSubProperty(exportToPythonFunctionProp);

    exportToPythonReloadProp = MButtonProperty("Reload python file", "Click");
    exportVariablesToPythonGroupProp.registerValueCallback(MPyInterface::reload);
    exportVariablesToPythonGroupProp.addSubProperty(exportToPythonReloadProp);
}


MNWPMultiVarActor::~MNWPMultiVarActor()
{
    // Delete all MNWPActorVariable instances.
    for (MNWPActorVariable* var : variables) delete var;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MNWPMultiVarActor::provideSynchronizationInfoToScene(
        MSceneControl *scene)
{
    for (MNWPActorVariable* var : variables)
    {
        if (var->getSynchronizationControl() != nullptr)
        {
            scene->variableSynchronizesWith(var->getSynchronizationControl());
            var->updateSyncPropertyColourHints(scene);
        }
    }
}


MNWPActorVariable *MNWPMultiVarActor::createActorVariable2(
        const QString &dataSourceID,
        const MVerticalLevelType levelType,
        const QString &variableName)
{
    MSelectableDataVariable dataVariable;
    dataVariable.dataSourceID = dataSourceID;
    dataVariable.levelType = levelType;
    dataVariable.variableName = variableName;

    return createActorVariable(dataVariable);
}


MNWPActorVariable* MNWPMultiVarActor::addActorVariable(bool allowMultiple)
{
    // Ask the user for a data source to be connected to the new actor variable.
    MSelectDataVariableDialog dialog(this->supportedLevelTypes());
    dialog.getTable()->allowMultiSelect(allowMultiple);

    if (dialog.exec() == QDialog::Rejected) return nullptr;

    QStringList sysControlIdentifiers = MSystemManagerAndControl::getInstance()
            ->getSyncControlIdentifiers();

    bool accepted = false;
    // ask user which sync control should be synchronized with variable
    QString syncName = QInputDialog::getItem(
                nullptr, "Choose Sync Control",
                "Please select a sync control to synchronize with: ",
                sysControlIdentifiers,
                std::min(1, sysControlIdentifiers.size() - 1), false,
                &accepted);
    // if user has aborted do not add any variable
    if (!accepted) return nullptr;

    QList<MSelectableDataVariable> selDataVariables = dialog.getSelectedDataVariables();
    // add the selection and return the first successful one
    MNWPActorVariable* firstVar = nullptr;
    for (const MSelectableDataVariable& selDataVariable : selDataVariables)
    {
        MNWPActorVariable* var = createActorVariable(selDataVariable);
        if (var)
        {
            addActorVariable(var, syncName);
            if (! firstVar) firstVar = var;
        }
    }
    return firstVar;
}


MNWPActorVariable* MNWPMultiVarActor::addActorVariable(
        MNWPActorVariable *var, const QString &syncName)
{
    LOG4CPLUS_INFO(mlog, "Adding new variable '" << var->variableName
                    << "' to actor '" << getName() << "'");

    enableEmissionOfActorChangedSignal(false);

    // Add variable to list of variables.
    variables << var;

    // Add variable property group to this actor's properties.
    variablesGroupProp.addSubProperty(var->varGroupProp);

    // Register callback to the remove property of the variable.
    auto varRemoveCallback = [=]()
    {
        // Ask the user if the variable should really be deleted.
        QMessageBox yesNoBox;
        yesNoBox.setWindowTitle("Delete actor variable");
        yesNoBox.setText("Do you really want to delete actor variable ''"
                                 + var->variableName + "''?");
        yesNoBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
        yesNoBox.setDefaultButton(QMessageBox::No);
        if (yesNoBox.exec() != QMessageBox::Yes) return;

        // Tell derived classes that the variable will be deleted.
        enableEmissionOfActorChangedSignal(false);
        onDeleteActorVariable(var);
        enableEmissionOfActorChangedSignal(true);

        variablesGroupProp.removeSubProperty(var->varGroupProp);
        variables.removeOne(var);

        LOG4CPLUS_INFO(mlog, "Removed variable <" << var->variableName
                                                  << "> from actor ''" << getName() << "''.");

        delete var;

        this->emitActorChangedSignal();
    };

    QAction *varRemoveAction = new QAction("Remove");
    connect(varRemoveAction, &QAction::triggered, varRemoveCallback);

    var->removeVariableProp.registerValueCallback(varRemoveCallback);
    var->varGroupProp.addContextMenuAction(varRemoveAction);

    // Initialize variable.
    if (isInitialized())
    {
        LOG4CPLUS_INFO(mlog, "Initializing variable:");
        var->initialize();
    }
    else
    {
        // If actor is not initialized, at least set the data source as the actor
        // should have access to a variable's data source when onAddActorVariable()
        // is called.
        var->initializeDataSource();
        var->updateGroupProperty();
    }

    if (!syncName.isEmpty())
    {
        var->synchronizeWith(MSystemManagerAndControl::getInstance()
                             ->getSyncControl(syncName));
    }

    // Tell derived classes that this variable has been added.
    onAddActorVariable(var);

    enableEmissionOfActorChangedSignal(true);

    LOG4CPLUS_INFO(mlog, "... variable has been added.");

    return var;
}


void MNWPMultiVarActor::saveConfigurationHeader(QSettings *settings)
{
    MActor::saveConfigurationHeader(settings);

    settings->beginGroup(MNWPMultiVarActor::getSettingsID());
    settings->setValue("numVariables", variables.size());
    settings->endGroup();

    for (int vi = 0; vi < variables.size(); vi++)
    {
        settings->beginGroup(QString("Variable_%1").arg(vi));
        variables.at(vi)->saveConfiguration(settings);
        settings->endGroup();
    }
}


void MNWPMultiVarActor::loadConfigurationHeader(QSettings *settings)
{
    MActor::loadConfigurationHeader(settings);

    // Delete current actor variables and their property groups.
    // =========================================================

    LOG4CPLUS_INFO(mlog, "Removing current actor variables:");

    for (MNWPActorVariable* var : variables)
    {
        // Tell derived classes that the variable will be deleted.
        enableEmissionOfActorChangedSignal(false);
        onDeleteActorVariable(var);
        enableEmissionOfActorChangedSignal(true);

        variablesGroupProp.removeSubProperty(var->varGroupProp);
        variables.removeOne(var);

        LOG4CPLUS_INFO(mlog, "Removed variable <" << var->variableName
                        << "> from actor ''" << getName() << "''.");

        delete var;
    }

    // Read MNWPMultiVarActor specific properties.
    // ===========================================

    settings->beginGroup(MNWPMultiVarActor::getSettingsID());

    int numVariables = settings->value("numVariables").toInt();

    settings->endGroup();

    // Create new actor variables from file info.
    // ==========================================

    LOG4CPLUS_INFO(mlog, "Creating new actor variables:");

    // Index of variable with respect to variable list not containing variables
    // which were not loaded.
    int varIndex = 0;

    for (int vi = 0; vi < numVariables; vi++)
    {
        settings->beginGroup(QString("Variable_%1").arg(vi));

        MSelectableDataVariable dataVariable(settings);
        // Apply mapping if we have a data source mapping. This is used when
        // a session is loaded for a different data set it has been created
        // with. This mapping has been set by the user during session loading.
        if (dataVariableMapping.contains(dataVariable))
        {
            QUuid varId = dataVariable.uuid;
            dataVariable = dataVariableMapping.value(dataVariable);
            dataVariable.uuid = varId;
        }

        bool available = dataVariable.isInSystemManagerAvailable();

        LOG4CPLUS_DEBUG(mlog, "  > Variable " << vi << ": data source = "
                        << dataVariable.dataSourceID << ", level type = "
                        << MStructuredGrid::verticalLevelTypeToString(dataVariable.levelType)
                        << ", variable = " << dataVariable.variableName);

        if (available)
        {
            // Yes, data source and variable are available.

            // Create new actor variable.
            MNWPActorVariable* var = createActorVariable(dataVariable);
            // And set the uuid of the variable to the uuid loaded from file.
            var->uuid = dataVariable.uuid;

            // Add the variable to the actor.
            addActorVariable(var, QString());

            // Load the variable configuration.
            var->loadConfiguration(settings);

            ++varIndex;
        }
        else
        {
            // No, either data source or variable is not available.
            LOG4CPLUS_WARN(mlog, "Actor variable '" << dataVariable.variableName
                << "' is not available for actor '" << getName() << "'. "
                << "Variable will not be loaded.");
            onLoadActorVariableFailure(varIndex);
        }
        settings->endGroup();
    }
}


void MNWPMultiVarActor::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    MActor::loadConfigurationPrior_V_1_14(settings);

    // Delete current actor variables and their property groups.
    // =========================================================

    LOG4CPLUS_INFO(mlog, "Removing current actor variables:");

    for (MNWPActorVariable* var : variables)
    {
        // Tell derived classes that the variable will be deleted.
        enableEmissionOfActorChangedSignal(false);
        onDeleteActorVariable(var);
        enableEmissionOfActorChangedSignal(true);

        variablesGroupProp.removeSubProperty(var->varGroupProp);
        variables.removeOne(var);

        LOG4CPLUS_INFO(mlog, "Removed variable <" << var->variableName
                                                  << "> from actor ''" << getName() << "''.");

        delete var;
    }

    // Read MNWPMultiVarActor specific properties.
    // ===========================================

    settings->beginGroup(MNWPMultiVarActor::getSettingsID());

    int numVariables = settings->value("numVariables").toInt();

    settings->endGroup();

    // Create new actor variables from file info.
    // ==========================================

    LOG4CPLUS_INFO(mlog, "Creating new actor variables:");

    // Index of variable with respect to variable list not containing variables
    // which were not loaded.
    int varIndex = 0;

    for (int vi = 0; vi < numVariables; vi++)
    {
        settings->beginGroup(QString("Variable_%1").arg(vi));

        MSelectableDataVariable dataVariable(settings);
        // Apply mapping if we have a data source mapping. This is used when
        // a session is loaded for a different data set it has been created
        // with. This mapping has been set by the user during session loading.
        if (dataVariableMapping.contains(dataVariable))
        {
            QUuid varId = dataVariable.uuid;
            dataVariable = dataVariableMapping.value(dataVariable);
            dataVariable.uuid = varId;
        }

        bool available = dataVariable.isInSystemManagerAvailable();

        LOG4CPLUS_DEBUG(mlog, "  > Variable " << vi << ": data source = "
                                              << dataVariable.dataSourceID << ", level type = "
                                              << MStructuredGrid::verticalLevelTypeToString(dataVariable.levelType)
                                              << ", variable = " << dataVariable.variableName);

        if (available)
        {
            // Yes, data source and variable are available.

            // Create new actor variable.
            MNWPActorVariable* var = createActorVariable(dataVariable);
            // And set the uuid of the variable to the uuid loaded from file.
            var->uuid = dataVariable.uuid;

            // Add the variable to the actor.
            addActorVariable(var, QString());

            // Load the variable configuration.
            var->loadConfigurationPrior_V_1_14(settings);

//TODO (mr, Feb2015) -- The call to var->initialize() in addActorVariable()
//                      already triggers a data request, but BEFORE the sync
//                      time/member is set. Hence trigger another request
//                      here -- this should be fixed (only trigger here!).
//                      Also see below.
            var->triggerAsynchronousDataRequest(false);
            ++varIndex;
        }
        else
        {
            // No, either data source or variable is not available.
            LOG4CPLUS_WARN(mlog, "Actor variable '" << dataVariable.variableName
                                                    << "' is not available for actor '" << getName() << "'. "
                                                    << "Variable will not be loaded.");
            onLoadActorVariableFailure(varIndex);
        }
        settings->endGroup();
    }
}


void MNWPMultiVarActor::broadcastPropertyChangedEvent(
        MPropertyType::ChangeNotification ptype, void *value)
{
    for (MNWPActorVariable* var : variables)
        var->actorPropertyChangeEvent(ptype, value);
}


bool MNWPMultiVarActor::isConnectedTo(MActor *actor)
{
    if (MActor::isConnectedTo(actor))
    {
        return true;
    }

    // This actor is connected to the argument actor if the actor is the
    // transfer function of any variable.
    for (MNWPActorVariable* var : variables)
    {
        if (var->transferFunction == actor)
        {
            return true;
        }
    }

    return false;
}


void MNWPMultiVarActor::printDebugOutputOnUserRequest()
{
    QString str = QString("\n==================\nNWPMultiVarActor :: "
                          "%1\n\n").arg(getName());

    for (MNWPActorVariable* var : variables)
    {
        str += var->debugOutputAsString();
    }

    str += QString("==================\n");

    LOG4CPLUS_DEBUG(mlog, str);
}


QMap<MSelectableDataVariable, MSelectableDataVariable>
MNWPMultiVarActor::getDataVariableMapping() const
{
    return dataVariableMapping;
}


void MNWPMultiVarActor::setDataVariableMapping(
    const QMap<MSelectableDataVariable, MSelectableDataVariable> &mapping)
{
    dataVariableMapping = mapping;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MNWPMultiVarActor::initializeActorResources()
{
    for (MNWPActorVariable* var : variables)
    {
        var->initialize();
    }

    collapseActorPropertyTree();
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/


} // namespace Met3D
