/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mactorcomponent.h"

// Static counter for all components that derive from MActor.
unsigned int Met3D::MActorComponent::idCounter = 0;

Met3D::MActorComponent::MActorComponent(MActor *parent, QString& name) :
    QObject(nullptr),
    enabled(true),
    parent(parent),
    name(name),
    id(idCounter++)
{

}

Met3D::MActorComponent::~MActorComponent()
= default;


void Met3D::MActorComponent::setName(const QString &newName)
{
    name = newName;

    onNameChanged();
}


void Met3D::MActorComponent::setEnabled(const bool enabled)
{
    this->enabled = enabled;

    if (enabled)
    {
        onEnabled();
    }
    else
    {
        onDisabled();
    }
}

void Met3D::MActorComponent::emitComponentChangedSignal()
{
    if (enabled)
    {
        emit componentChanged();
    }
}
