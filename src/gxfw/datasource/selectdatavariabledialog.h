/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017 Bianca Tost [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef SELECTDATAVARIABLEDIALOG_H
#define SELECTDATAVARIABLEDIALOG_H

// standard library imports

// related third party imports

// local application imports
#include "selectdatavariabletable.h"
#include "data/structuredgrid.h"


namespace Ui {
class MSelectDataVariableDialog;
}


namespace Met3D
{

/**
 * MSelectDataVariablesDialog implements a dialog from which the user can
 * select one or multiple forecast variables, along with the default
 * "Accept" and "Reject" buttons.
 */
class MSelectDataVariableDialog : public QDialog
{
    Q_OBJECT
    
public:
    /**
     * Constructs a new dialog. The table is populated with data variables which
     * are registered to the @c MGLResourcesManager. All vertical level types
     * are considered.
     * @param parent The parent widget.
     */
    explicit MSelectDataVariableDialog(QWidget *parent = 0);

    /**
     * Constructs a new dialog. The table is populated with data variables which
     * are registered to the @c MGLResourcesManager. Only vertical level types
     * passed by @p supportedTypes are considered.
     * @param supportedTypes Vertical level types.
     * @param parent The parent widget.
     */
    explicit MSelectDataVariableDialog(const QList<MVerticalLevelType>& supportedTypes,
                                     QWidget *parent = 0);

    ~MSelectDataVariableDialog();

    /**
     * @return The variable table in this dialog.
     */
    MSelectDataVariableTable* getTable();

    /**
     * Convenience methods to get the currently selected data variable(s) from
     * the table in this dialog.
     */
    MSelectableDataVariable getSelectedDataVariable();
    QList<MSelectableDataVariable> getSelectedDataVariables();

public Q_SLOTS:
    /**
      @brief Reimplemented exec() to avoid execusion of dialog if no variables
      or data sources respectively are available to select.

      Prints warning corresponding to the selection dialog (variables or data
      sources) executed.
      */
    int exec();

private:
    Ui::MSelectDataVariableDialog* ui;

};

} // namespace Met3D

#endif // SELECTDATAVARIABLEDIALOG_H
