/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSELECTDATAVARIABLETABLE_H
#define MSELECTDATAVARIABLETABLE_H

// standard library imports

// related third party imports

// local application imports
#include "data/structuredgrid.h"
#include "data/selectabledatavariable.h"

namespace Met3D
{

/**
 * This class represents the table which can list the available variables. The
 * user is able to select one or multiple variables.
 */
class MSelectDataVariableTable : public QTableWidget
{
public:
    MSelectDataVariableTable(QWidget *parent = nullptr);

    /**
     * Fill the table with variables of the given level types.
     * @param levelTypes List of level types to be allowed.
     */
    void fill(const QList<MVerticalLevelType> &levelTypes);

    /**
     * Fill the table with variables of all level types.
     */
    void fillWithAllLevelTypes();

    /**
     * @return The currently selected data variable. If multiple items are
     * selected, the first selected one is returned.
     */
    MSelectableDataVariable getSelectedDataVariable() const;

    /**
     * @return The currently selected data variables.
     */
    QList<MSelectableDataVariable> getSelectedDataVariables() const;

    /**
     * Allow multiple items to be selected in the table.
     * @param multiSelect If True, multi select is allowed, if False, force
     * single select.
     */
    void allowMultiSelect(bool multiSelect);

    /**
      checkIfSingleDataSourceWithSingleVariableIsPresent returns the data source
      containing the only variable if just one variable of the supported level
      Types is present.

      @param ok is set to true if only one variable is present and to false
      otherwise.
     */
    MSelectableDataVariable checkIfSingleDataSourceWithSingleVariableIsPresent(
        bool *ok) const;

    /**
     * @return True if the table has any entries.
     */
    bool hasEntriesAvailable() const;

private:
    /**
      Creates table entries for variable selection dialog restricted to
      @p supportedTypes.
      */
    void createDataSourceEntries(
        const QList<MVerticalLevelType> &supportedTypes);

    /**
     * Get the selectable variable from a given @p row.
     * @param row The row.
     * @return The data source or a default object if the row is not populated
     * or no variables are displayed.
     */
    MSelectableDataVariable getVariableFromRow(int row) const;

    void mousePressEvent(QMouseEvent *event) override;
    void mouseMoveEvent(QMouseEvent *event) override;

    /**
      Indicator variable used for selection dialog to decide if at least one
      entry is available.
      */
    bool entriesAvailable;
    QPoint dragStartPosition; // To detect drag events.
};

} // Met3D

#endif //MSELECTDATAVARIABLETABLE_H
