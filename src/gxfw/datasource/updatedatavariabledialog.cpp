/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "updatedatavariabledialog.h"
#include "ui_updatedatavariabledialog.h"

// standard library imports

// related third party imports

// local application imports
#include "mainwindow.h"
#include "gxfw/adddatasetdialog.h"
#include "system/pipelineconfiguration.h"


namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MUpdateDataVariableDialog::MUpdateDataVariableDialog(
    const QList<MSelectableDataVariable> &sessionDataVariables, QWidget *parent)
    : QDialog(parent),
      ui(new Ui::MUpdateDataVariableDialog)
{
    ui->setupUi(this);

    // Fill the table with session and updated data sources.
    ui->updateDataSourceTable->fillFrom(sessionDataVariables);

    // Fill the bottom table with available variables.
    ui->loadedDataSources->fillWithAllLevelTypes();
    ui->loadedDataSources->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->loadedDataSources->selectRow(0);

    // Create slots for the buttons to load/create data sets.
    connect(ui->newDatasetButton, &QPushButton::clicked, this,
            &MUpdateDataVariableDialog::newDatasetButtonClicked);
    connect(ui->openDatasetConfigurationButton, &QPushButton::clicked, this,
            &MUpdateDataVariableDialog::openDatasetButtonClicked);

    // Add connection: When clicking on an arrow in the updateDataSourcesTable,
    // fill the row with the current selection from the loadedDataSources table.
    connect(ui->updateDataSourceTable, &MUpdateDataVariableTable::setButtonClickedSignal,
        this, &MUpdateDataVariableDialog::setButtonClicked);
}


MUpdateDataVariableDialog::~MUpdateDataVariableDialog()
{
    delete ui;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MUpdateDataVariableTable *MUpdateDataVariableDialog::getUpdateTable() const
{
    return ui->updateDataSourceTable;
}


/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MUpdateDataVariableDialog::setButtonClicked(int row) const
{
    MSelectableDataVariable selection = ui->loadedDataSources->getSelectedDataVariable();
    if (! selection.isValid())
        return;
    ui->updateDataSourceTable->setReplacementVariableInTable(row, selection);
}

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

void MUpdateDataVariableDialog::newDatasetButtonClicked()
{
    MAddDatasetDialog::createAndOpen();
    // Update the table.
    ui->loadedDataSources->fillWithAllLevelTypes();
    ui->updateDataSourceTable->updateLoadedFields();
}


void MUpdateDataVariableDialog::openDatasetButtonClicked()
{
    QVector<QSettings*> pipelines =
        MPipelineConfiguration::readPipelineConfigsFromFiles();

    for (auto pipeline : pipelines)
    {
        MAddDatasetDialog::createAndOpen(pipeline);
    }
    // Update the table.
    ui->loadedDataSources->fillWithAllLevelTypes();
    ui->updateDataSourceTable->updateLoadedFields();
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MUpdateDataVariableDialog::done(int result)
{
    // User pressed accept, check the updated data sources.
    if (getUpdateTable()->allVariablesUpdated())
    {
        QDialog::done(result);
        return;
    }

    // Some are missing. Ask the user if he still wants to add them.
    QString msg = "For some data sources in the session file no corresponding "
        "loaded data sources have been specified. Do you want to continue "
        "without these data sources?";
    QMessageBox yesNoBox;
    yesNoBox.setWindowTitle("Update data sources");
    yesNoBox.setText(msg);
    yesNoBox.setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    yesNoBox.setDefaultButton(QMessageBox::Yes);

    if (yesNoBox.exec() == QMessageBox::Yes)
    {
        QDialog::done(result);
    }
}
}