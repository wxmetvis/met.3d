/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017 Bianca Tost [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MSELECTDATASOURCETABLE_H
#define MSELECTDATASOURCETABLE_H

// standard library imports

// related third party imports

// local application imports
#include "data/selectabledatavariable.h"

namespace Met3D
{
class MWeatherPredictionDataSource;

enum MSelectDataSourceType
{
    WEATHER_PREDICTION_SOURCE = 0,
    TRAJECTORY_SOURCE = 1
};

/**
 * This class represents the table which can list the available NWP data
 * sources, or the trajectory data sources.
 */
class MSelectDataSourceTable : public QTableWidget
{
public:
    MSelectDataSourceTable(QWidget *parent = nullptr);

    /**
     * Populate the table based on the given data source @p type.
     * @param type The type to fill the table.
     */
    void fillByType(MSelectDataSourceType type);

    /**
     * Allow multiple items to be selected in the table.
     * @param multiSelect If True, multi select is allowed, if False, force
     * single select.
     */
    void allowMultiSelect(bool multiSelect);

    /**
      Checks whether the @p source contains init times, valid times and
      ensemble members informations.

      Returns @return true if it contains all necessary data and @return false
      if not.
      Also used by @ref MSyncControl.
      */
    static bool checkDataSourceForData(MWeatherPredictionDataSource *source);

    /**
      Checks whether @p dataSourceID describes a data source for
      trajectories by checking for the data sources needed (reader, normals,
      timestepFilter).

      checkForTrajectoryDataSource returns @return true if the check was
      positive, @return false otherwise.
     */
    static bool checkForTrajectoryDataSource(QString dataSourceID);

    /**
     * @return True if the table has any entries.
     */
    bool hasEntriesAvailable() const;

    /**
     * @return The current selection in the table.
     */
    QString getSelectedDataSourceID() const;
    QList<QString> getSelectedDataSourceIDs() const;

private:

    /**
      Creates table entries for data source selection dialog.
      */
    void createWeatherPredictionDataSourceEntries();
    /**
      Creates table entries for data source selection dialog restricted to
      trajectory data sources.
      */
    void createTrajectoryDataSourceEntries();

    QString getDataSourceIDFromRow(int row) const;

    /**
      Indicator variable used for selection dialog to decide if at least one
      entry is available.
      */
    bool entriesAvailable;
    MSelectDataSourceType fillType;
};
}

#endif //MSELECTDATASOURCETABLE_H
