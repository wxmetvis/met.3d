/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017 Bianca Tost [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef SELECTDATASOURCEDIALOG_H
#define SELECTDATASOURCEDIALOG_H

// standard library imports

// related third party imports

// local application imports
#include "selectdatasourcetable.h"
#include "data/structuredgrid.h"


namespace Ui {
class MSelectDataSourceDialog;
}


namespace Met3D
{

class MWeatherPredictionDataSource;


/**
  @brief MSelectDataSourceDialog implements a dialog from which the user can
  select data sources to restrict a synchronisation control to
  their times and ensemble members, or a trajectory data source.

  Which dialog is created depends on the constructor used to construct the
  dialog.
  */
class MSelectDataSourceDialog : public QDialog
{
    Q_OBJECT

public:
    /**
      Constructs a new dialog. The dialog's data field table is filled with a
      list of the data source registered with @ref MGLResourcesManager.

      Which dialog should be created is defined by @p type. If this
      constructor is called to create a variable selection dialog, it uses all
      vertical level types available.

      The overloaded constructor without given @p type adds both NWP sources
      and trajectory sources to the selection.
      */
    explicit MSelectDataSourceDialog(MSelectDataSourceType type,
                                     QWidget *parent = 0);
    explicit MSelectDataSourceDialog(QWidget *parent = 0);

    ~MSelectDataSourceDialog();

    /**
     * @return The table containing the data sources.
     */
    MSelectDataSourceTable* getTable();


public Q_SLOTS:
    /**
      @brief Reimplemented exec() to avoid execusion of dialog if no variables
      or data sources respectively are available to select.

      Prints warning corresponding to the selection dialog (variables or data
      sources) executed.
      */
    int exec();

private:
    Ui::MSelectDataSourceDialog* ui;

};

} // namespace Met3D

#endif // SELECTDATASOURCEDIALOG_H
