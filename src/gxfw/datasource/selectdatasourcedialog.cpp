/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2017      Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "selectdatasourcedialog.h"
#include "ui_selectdatasourcedialog.h"

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "actors/geometryactor.h"
#include "gxfw/msystemcontrol.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectDataSourceDialog::MSelectDataSourceDialog(
        MSelectDataSourceType type, QWidget *parent)
    : QDialog(parent),
      ui(new Ui::MSelectDataSourceDialog)
{
    ui->setupUi(this);
    ui->dataFieldTable->fillByType(type);
    ui->dataFieldTable->setDragDropMode(QAbstractItemView::NoDragDrop);

    switch (type)
    {
    case MSelectDataSourceType::WEATHER_PREDICTION_SOURCE:
    {
        ui->label->setText("Please select data sources and confirm with \"OK\".");
        ui->dataFieldTable->allowMultiSelect(true);
        break;
    }
    case MSelectDataSourceType::TRAJECTORY_SOURCE:
    {
        ui->label->setText("Please select a data source and confirm with \"OK\".");
        ui->dataFieldTable->allowMultiSelect(false);
        break;
    }
    default:
        break;
    }
}


MSelectDataSourceDialog::MSelectDataSourceDialog(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::MSelectDataSourceDialog)
{
    ui->setupUi(this);

    ui->dataFieldTable->fillByType(MSelectDataSourceType::WEATHER_PREDICTION_SOURCE);
    ui->dataFieldTable->fillByType(MSelectDataSourceType::TRAJECTORY_SOURCE);
    ui->dataFieldTable->setDragDropMode(QAbstractItemView::NoDragDrop);

    ui->label->setText("Please select data sources and confirm with \"OK\".");
    ui->dataFieldTable->allowMultiSelect(true);
}


MSelectDataSourceDialog::~MSelectDataSourceDialog()
{
    delete ui;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MSelectDataSourceTable* MSelectDataSourceDialog::getTable()
{
    return ui->dataFieldTable;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

int MSelectDataSourceDialog::exec()
{
    // Test if variables or data sources to select are available. If not, inform
    // user and return QDialog::Rejected without executing the dialog.
    if (ui->dataFieldTable->hasEntriesAvailable())
    {
        return QDialog::exec();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("No data sources available to select.");
        msgBox.exec();
        return QDialog::Rejected;
    }
}

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

} // namespace Met3D
