/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "selectdatavariabletable.h"

// standard library imports

// related third party imports
#include <QDrag>
#include <QHeaderView>
#include <QApplication>

// local application imports
#include "data/mimedata.h"
#include "data/weatherpredictiondatasource.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectDataVariableTable::MSelectDataVariableTable(QWidget *parent)
    : QTableWidget(parent),
      entriesAvailable(false)
{
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MSelectDataVariableTable::fill(const QList<MVerticalLevelType> &types)
{
    createDataSourceEntries(types);
}


void MSelectDataVariableTable::fillWithAllLevelTypes()
{
    fill(QList<MVerticalLevelType>()
            << AUXILIARY_PRESSURE_3D << HYBRID_SIGMA_PRESSURE_3D
            << LOG_PRESSURE_LEVELS_3D << PRESSURE_LEVELS_3D
            << SINGLE_LEVEL << POTENTIAL_VORTICITY_2D);
}


MSelectableDataVariable MSelectDataVariableTable::getSelectedDataVariable() const
{
    return getVariableFromRow(currentRow());
}


QList<MSelectableDataVariable>
MSelectDataVariableTable::getSelectedDataVariables() const
{
    QList<QTableWidgetItem *> items = selectedItems();
    QList<MSelectableDataVariable> dataVariables;
    QList<int> visitedRows;

    for (const QTableWidgetItem *item : items)
    {
        const int row = item->row();
        if (visitedRows.contains(row)) { continue; }
        visitedRows.push_back(row);
        dataVariables.push_back(getVariableFromRow(row));
    }

    return dataVariables;
}


void MSelectDataVariableTable::allowMultiSelect(bool multiSelect)
{
    if (multiSelect)
    {
        setSelectionMode(MultiSelection);
    }
    else
    {
        setSelectionMode(SingleSelection);
    }
}


void MSelectDataVariableTable::mousePressEvent(QMouseEvent *e)
{
    // If we click, we do not know if this will be a drag event yet.
    // Record starting position so we can create a drag event later.
    if (e->button() == Qt::LeftButton) dragStartPosition = e->pos();
    QTableWidget::mousePressEvent(e);
}


void MSelectDataVariableTable::mouseMoveEvent(QMouseEvent *event)
{
    if (! (event->buttons() & Qt::LeftButton)) return;
    // Check whether to start a drag event.
    // Decide based on distance traveled since mouseDown.
    if ((event->pos() - dragStartPosition).manhattanLength()
        < QApplication::startDragDistance())
        return;

    auto *drag = new QDrag(this);

    // Current selection, serialize to MimeData.
    MSelectableDataVariable dataVariable = getSelectedDataVariable();

    auto *mimeData = new MMimeData(MMimeType::SELECTABLE_DATA_VARIABLE,
                                   dataVariable.serialize());

    drag->setMimeData(mimeData);
    drag->exec(Qt::CopyAction);
}


MSelectableDataVariable MSelectDataVariableTable::
checkIfSingleDataSourceWithSingleVariableIsPresent(bool *ok) const
{
    if (rowCount() != 1)
    {
        *ok = false;
        return MSelectableDataVariable();
    }
    *ok = true;
    return getVariableFromRow(0);
}


bool MSelectDataVariableTable::hasEntriesAvailable() const
{
    return entriesAvailable;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MSelectDataVariableTable::createDataSourceEntries(
    const QList<MVerticalLevelType> &supportedTypes)
{
    // Clear the table if there were previous entries.
    clear();
    setRowCount(0);

    // Set the data field table's header.
    setColumnCount(6);
    QStringList headerLabels;
    headerLabels << "Dataset" << "Vertical Dimension" << "Variable Name"
        << "Standard Name" << "Long Name" << "Units";
    setHorizontalHeaderLabels(headerLabels);

    // Loop over all data loaders registered with the resource manager.
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    entriesAvailable = false;

    QStringList dataSources = sysMC->getDataSourceIdentifiers();
    for (const QString& dataSourceId : dataSources)
    {
        auto *source = dynamic_cast<MWeatherPredictionDataSource *>
            (sysMC->getDataSource(dataSourceId));

        if (source == nullptr) continue;

        // Fill the data field table with elements that are available from the
        // current data loader.

        // Loop over all level types.
        QList<MVerticalLevelType> levelTypes = source->availableLevelTypes();
        for (MVerticalLevelType lvl : levelTypes)
        {
            // Do not list data sources of not supported level types.
            if (! supportedTypes.contains(lvl)) continue;

            // .. and all variables for the current level type ..
            QStringList variables = source->availableVariables(lvl);
            entriesAvailable = entriesAvailable || ! variables.empty();
            for (const QString& var : variables)
            {
                // Add a row to the table...
                int row = rowCount();
                setRowCount(row + 1);
                // ... and insert the element.
                setItem(row, 0, new QTableWidgetItem(dataSourceId));
                setItem(row, 1, new QTableWidgetItem(
                            MStructuredGrid::verticalLevelTypeToString(lvl)));
                setItem(row, 2, new QTableWidgetItem(var));
                setItem(row, 3, new QTableWidgetItem(
                            source->variableStandardName(lvl, var)));
                setItem(row, 4, new QTableWidgetItem(
                            source->variableLongName(lvl, var)));
                setItem(row, 5, new QTableWidgetItem(
                            source->variableUnits(lvl, var)));
            } // for (variables)
        }     // for (level types)
    }         // for (data loaders)

    // Resize the table's columns.
    resizeColumnsToContents();
}


MSelectableDataVariable MSelectDataVariableTable::getVariableFromRow(
    int row) const
{
    MSelectableDataVariable dataSource;
    if (row < 0 || row >= rowCount()) return dataSource;

    dataSource.dataSourceID = item(row, 0)->text();
    dataSource.levelType = MStructuredGrid::verticalLevelTypeFromString(
        item(row, 1)->text());

    //TODO (mr, Feb2015) -- make this function return the std.name by default.
    //                      This requires changes in MWeatherPredictionDataSource->
    //                      availableVariables(), which needs to return std.names as well
    // Use the CF standard name as variable name, as long as a standard name
    // exists. Otherwise use the NetCDF/Grib variable name.
    //    QString varName = ui->dataFieldTable->item(row, 3)->text();
    //    if (varName.isEmpty())
    //        varName = ui->dataFieldTable->item(row, 2)->text();
    //    dataSource.variableName = varName;

    QString varName = item(row, 2)->text();
    dataSource.variableName = varName;

    return dataSource;
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D