/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MUPDATEDATAVARIABLEDIALOG_H
#define MUPDATEDATAVARIABLEDIALOG_H

// standard library imports

// related third party imports

// local application imports
#include "updatedatavariabletable.h"

namespace Ui
{
class MUpdateDataVariableDialog;
}


namespace Met3D
{
/**
 * This class represents the dialog where the user can map variables on
 * loading a session. This is particularly useful when a complex session is
 * loaded for a different dataset, thus all variables in the session file
 * cannot be assigned to the loaded data set. This dialog allows for quick
 * mapping of old variables to new variables.
 */
class MUpdateDataVariableDialog : public QDialog
{
    Q_OBJECT

public:
    /**
     * Creates a new dialog. This also creates both relevant tables, the
     * @c MUpdateDataVariableTable and @c MSelectDataVariableTable.
     * @param sessionDataSources The data sources in the session configuration.
     * @param parent The parent widget.
     */
    explicit MUpdateDataVariableDialog(
        const QList<MSelectableDataVariable> &sessionDataVariables,
        QWidget *parent = nullptr);
    ~MUpdateDataVariableDialog();

    /**
     * @return The table with found and updated variables.
     */
    MUpdateDataVariableTable *getUpdateTable() const;

public slots:
    /**
     * Slot whenever a "set" button is clicked in the @c MUpdatedDataSourceTable.
     * @param row The row of the button.
     */
    void setButtonClicked(int row) const;

private slots:
    /**
     * Called when the new dataset button is clicked. Opens the new dataset
     * dialog. After closing the new dataset dialog, update this dialog.
     */
    void newDatasetButtonClicked();

    /**
     * Called when the open dataset configuration button is clicked. Opens the
     * file explorer to select a data set to load. After closing that dialog,
     * update this dialog.
     */
    void openDatasetButtonClicked();

private:
    /**
     * Overwritten call to when "Accept" is pressed. We check before accepting
     * if all sources have been updated. If not, display a warning, but still
     * allow the user to continue.
     */
    void done(int) override;

    Ui::MUpdateDataVariableDialog *ui;
};
}


#endif //MUPDATEDATAVARIABLEDIALOG_H
