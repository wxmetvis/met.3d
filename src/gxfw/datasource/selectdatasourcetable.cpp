/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2017 Bianca Tost [+]
**  Copyright 2024 Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "selectdatasourcetable.h"

// standard library imports

// related third party imports
#include <QDrag>
#include <QHeaderView>

// local application imports
#include "data/mimedata.h"
#include "trajectories/source/trajectorydatasource.h"
#include "data/weatherpredictiondatasource.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectDataSourceTable::MSelectDataSourceTable(QWidget *parent)
    : QTableWidget(parent),
      entriesAvailable(false)
{
    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QString MSelectDataSourceTable::getSelectedDataSourceID() const
{
    return getDataSourceIDFromRow(currentRow());
}


QList<QString> MSelectDataSourceTable::getSelectedDataSourceIDs() const
{
    QList<QTableWidgetItem *> items = selectedItems();
    QList<QString> dataSources;
    QList<int> visitedRows;

    for (const QTableWidgetItem *item : items)
    {
        const int row = item->row();
        if (visitedRows.contains(row)) { continue; }
        visitedRows.push_back(row);
        dataSources.push_back(getDataSourceIDFromRow(row));
    }

    return dataSources;
}


void MSelectDataSourceTable::fillByType(MSelectDataSourceType type)
{
    switch (type)
    {
    case MSelectDataSourceType::WEATHER_PREDICTION_SOURCE:
        createWeatherPredictionDataSourceEntries();
        break;
    case MSelectDataSourceType::TRAJECTORY_SOURCE:
        createTrajectoryDataSourceEntries();
        break;
    default:
        break;
    }
}


void MSelectDataSourceTable::allowMultiSelect(bool multiSelect)
{
    if (multiSelect)
    {
        setSelectionMode(MultiSelection);
    }
    else
    {
        setSelectionMode(SingleSelection);
    }
}


bool MSelectDataSourceTable::checkDataSourceForData(
    MWeatherPredictionDataSource *source)
{
    if (source == nullptr)
    {
        return false;
    }

    QStringList variables;
    QList<QDateTime> currentInitTimes;
    QList<QDateTime> currentValidTimes;
    bool validTimesMissing = true;

    // Check if data source contains init times, valid times and
    // ensemble members.
    QList<MVerticalLevelType> levelTypes = source->availableLevelTypes();
    for (int ilvl = 0; ilvl < levelTypes.size(); ilvl++)
    {
        MVerticalLevelType levelType = levelTypes.at(ilvl);

        variables = source->availableVariables(levelType);

        for (int ivar = 0; ivar < variables.size(); ivar++)
        {
            QString var = variables.at(ivar);
            currentInitTimes =
                source->availableInitTimes(levelType, var);
            if (currentInitTimes.size() == 0)
            {
                continue;
            }

            validTimesMissing = true;
            for (int iInitTime = 0; iInitTime < currentInitTimes.size();
                 iInitTime++)
            {
                QDateTime initTime = currentInitTimes.at(iInitTime);
                currentValidTimes = source->availableValidTimes(levelType, var,
                    initTime);
                if (currentValidTimes.size() == 0)
                {
                    continue;
                }
                validTimesMissing = false;
            } // initTimes

            if (validTimesMissing)
            {
                continue;
            }

            if (source->availableEnsembleMembers(levelType, var).size() == 0)
            {
                continue;
            }

            return true;
        } // variables
    }     // levelTypes

    return false;
}


bool MSelectDataSourceTable::checkForTrajectoryDataSource(QString dataSourceID)
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    if (! dynamic_cast<MTrajectoryDataSource *>(
        sysMC->getDataSource(dataSourceID + QString(" Reader"))))
    {
        return false;
    }
    return true;
}


bool MSelectDataSourceTable::hasEntriesAvailable() const
{
    return entriesAvailable;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MSelectDataSourceTable::createWeatherPredictionDataSourceEntries()
{
    QStringList variables;
    QList<QDateTime> currentInitTimes;
    QList<QDateTime> currentValidTimes;
    variables.clear();
    currentInitTimes.clear();
    currentValidTimes.clear();

    // Set the data field table's header.
    setColumnCount(1);
    setHorizontalHeaderLabels(QStringList("Dataset"));

    // Loop over all data loaders registered with the resource manager.
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    QStringList dataSources = sysMC->getDataSourceIdentifiers();
    for (int idl = 0; idl < dataSources.size(); idl++)
    {
        MWeatherPredictionDataSource *source =
            dynamic_cast<MWeatherPredictionDataSource *>
            (sysMC->getDataSource(dataSources[idl]));

        if (source == nullptr)
        {
            continue;
        }

        // Only add data source to table if it contains init times, valid times
        // and ensemble member informations.
        if (checkDataSourceForData(source))
        {
            // Add a row to the table..
            int row = rowCount();
            setRowCount(row + 1);
            // .. and insert the element.
            setItem(row, 0, new QTableWidgetItem(dataSources[idl]));

            entriesAvailable = true;
        }
    } // for (data loaders)

    // Resize the table's columns to fit data source names.
    resizeColumnsToContents();
    // Set table width to always fit window size.
    horizontalHeader()->setStretchLastSection(true);
    // Disable resize of column by user.
    horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Fixed);
    // Resize widget to fit table size.
    this->resize(width(), height());
}


void MSelectDataSourceTable::createTrajectoryDataSourceEntries()
{
    // Set the data field table's header.
    setColumnCount(1);
    setHorizontalHeaderLabels(QStringList("Dataset"));
    setSelectionMode(SingleSelection);

    // Loop over all data loaders registered with the resource manager.
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    QStringList dataSources = sysMC->getDataSourceIdentifiers();
    for (int idl = 0; idl < dataSources.size(); idl++)
    {
        QString dataSourceID = dataSources[idl];

        // Use Reader to identify trajectory data sources.
        if (dataSourceID.endsWith(" Reader"))
        {
            dataSourceID.chop(7);

            if (checkForTrajectoryDataSource(dataSourceID))
            {
                // Add a row to the table..
                int row = rowCount();
                setRowCount(row + 1);
                // .. and insert the element.
                setItem(row, 0, new QTableWidgetItem(dataSourceID));

                entriesAvailable = true;
            }
        }
    } // for (data loaders)

    // Resize the table's columns to fit data source names.
    resizeColumnsToContents();
    // Set table width to always fit window size.
    horizontalHeader()->setStretchLastSection(true);
    // Disable resize of column by user.
    horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeMode::Fixed);
    setEditTriggers(NoEditTriggers);
    // Resize widget to fit table size.
    this->resize(width(), height());
}


QString MSelectDataSourceTable::getDataSourceIDFromRow(int row) const
{
    if (row < 0 || row >= rowCount()) return "";
    return item(row, 0)->text();
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
}