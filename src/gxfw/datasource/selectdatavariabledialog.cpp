/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2017 Marc Rautenhaus
**  Copyright 2017      Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "selectdatavariabledialog.h"
#include "ui_selectdatavariabledialog.h"

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "gxfw/msystemcontrol.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectDataVariableDialog::MSelectDataVariableDialog(QWidget *parent)
    : MSelectDataVariableDialog(QList<MVerticalLevelType>()
            << AUXILIARY_PRESSURE_3D << HYBRID_SIGMA_PRESSURE_3D
            << LOG_PRESSURE_LEVELS_3D << PRESSURE_LEVELS_3D
            << SINGLE_LEVEL << POTENTIAL_VORTICITY_2D, parent)
{
}


MSelectDataVariableDialog::MSelectDataVariableDialog(
        const QList<MVerticalLevelType>& supportList, QWidget *parent)
    : QDialog(parent),
      ui(new Ui::MSelectDataVariableDialog)
{
    ui->setupUi(this);

    ui->label->setText("Please select a variable and confirm with \"OK\".");
    ui->dataFieldTable->fill(supportList);
}


MSelectDataVariableDialog::~MSelectDataVariableDialog()
{
    delete ui;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MSelectDataVariableTable* MSelectDataVariableDialog::getTable()
{
    return ui->dataFieldTable;
}


MSelectableDataVariable MSelectDataVariableDialog::getSelectedDataVariable()
{
    return getTable()->getSelectedDataVariable();
}


QList<MSelectableDataVariable> MSelectDataVariableDialog::getSelectedDataVariables()
{
    return getTable()->getSelectedDataVariables();
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

int MSelectDataVariableDialog::exec()
{
    // Test if variables or data sources to select are available. If not, inform
    // user and return QDialog::Rejected without executing the dialog.
    if (ui->dataFieldTable->hasEntriesAvailable())
    {
        return QDialog::exec();
    }
    else
    {
        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText("No variables available to select.");
        msgBox.exec();

        return QDialog::Rejected;
    }
}

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

} // namespace Met3D
