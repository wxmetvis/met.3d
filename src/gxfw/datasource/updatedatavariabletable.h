/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MUPDATEDATAVARIABLETABLE_H
#define MUPDATEDATAVARIABLETABLE_H

// standard library imports

// related third party imports

// local application imports
#include "data/selectabledatavariable.h"

namespace Met3D {
/**
 * This class represents the upper table in the update data variables dialog.
 * It contains two parts: On the left, the variables found in the session
 * configuration (filled by @c fillFrom), and on the right side the
 * replacement variables.
 */
class MUpdateDataVariableTable : public QTableWidget {
    Q_OBJECT
public:
    /**
     * Creates a new empty table.
     */
    MUpdateDataVariableTable(QWidget* parent = nullptr);

    /**
     * Fill the left part of the table from the given @p sessionDataSources,
     * and also the right part of the table if these are also present in the
     * current system manager.
     * @param sessionDataSources The list of data sources in the session file.
     */
    void fillFrom(const QList<MSelectableDataVariable>& sessionDataVariables);

    /**
     * Drag events to support drag and drop from the @c MSelectDataVariableTable
     * onto this table.
     */
    void dragEnterEvent(QDragEnterEvent *event) override;
    void dropEvent(QDropEvent *event) override;
    void dragMoveEvent(QDragMoveEvent *event) override;

    /**
     * @return True if all variables have an updated variable.
     */
    bool allVariablesUpdated() const;

    /**
     * @return The current mapping of session data sources (left) to selected
     * updated data sources (right).
     */
    QMap<MSelectableDataVariable, MSelectableDataVariable> getDataVariableMapping() const;

    /**
     * Set the session variable @p dataVariable in the table at row @p row.
     */
    void setSessionVariableInTable(int row,
                                   const MSelectableDataVariable &dataVariable);

    /**
     * Set the replacement variable @p dataVariable in the table at row @p row.
     */
    void setReplacementVariableInTable(int row,
                                  const MSelectableDataVariable &dataVariable);

    /**
     * Updates the right table with replaced variables by scanning the loaded
     * data variables for ones matching the variables on the left side.
     */
    void updateLoadedFields();

signals:
    void setButtonClickedSignal(int row);

private:
    /**
     * Update the color of the given @p row.
     */
    void updateColor(int row) const;

    /**
     * Initialize the arrow in the middle for the given @p row.
     */
    void initializeArrow(int row);

    /**
     * Initialize the "set" button on the right end of the given @p row.
     */
    void initializeSetButton(int row);

    /**
     * Set the given table @p item in the given @p row and @p column. Overwrites
     * the @c QTableWidget method to also make items non-editable.
     */
    void setItem(int row, int column, QTableWidgetItem *item);

    /**
     * @return True if given @p row has an updated variable selected.
     */
    bool isRowUpdated(int row) const;

    /**
     * Get the @c MSelectableDataVariable on the left side of the given @p row.
     * @param row The row to get the data variable.
     * @return The selectable data variable.
     */
    MSelectableDataVariable getSessionDataVariable(int row) const;
};

} // Met3D

#endif //MUPDATEDATAVARIABLETABLE_H
