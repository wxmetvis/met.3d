/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "updatedatavariabletable.h"

// standard library imports

// related third party imports
#include <QHeaderView>

// local application imports
#include "data/mimedata.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MUpdateDataVariableTable::MUpdateDataVariableTable(QWidget *parent)
    : QTableWidget(parent)
{
    // Set the data field table's header.
    setColumnCount(8);
    QStringList headerLabels;
    headerLabels << "Dataset" << "Vertical Dimension" << "Variable Name"
        << " " << "Dataset" << "Vertical Dimension" << "Variable Name" << " ";
    setHorizontalHeaderLabels(headerLabels);

    // Disable manual changes, only drag/drop allowed.
    setSelectionMode(NoSelection);
    setSelectionBehavior(SelectRows);
    setEditTriggers(NoEditTriggers);
    setFocusPolicy(Qt::NoFocus);

    horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    horizontalHeader()->setSectionResizeMode(7, QHeaderView::ResizeToContents);
    setAcceptDrops(true);
}


void MUpdateDataVariableTable::fillFrom(
    const QList<MSelectableDataVariable>& sessionDataVariables)
{
    for (int ivar = 0; ivar < sessionDataVariables.size(); ivar++)
    {
        MSelectableDataVariable dataVariable = sessionDataVariables.at(ivar);
        // Add a row to the table.
        int row = rowCount();
        setRowCount(row + 1);
        // .. and insert the element.
        setSessionVariableInTable(row, dataVariable);

        initializeArrow(row);
        // If data source in system manager, add it also to the right side.
        if (dataVariable.isInSystemManagerAvailable())
        {
            setReplacementVariableInTable(row, dataVariable);
        }
        else
        {
            // Initialize empty so we can set a background color.
            setItem(row, 4, new QTableWidgetItem(""));
            setItem(row, 5, new QTableWidgetItem(""));
            setItem(row, 6, new QTableWidgetItem(""));
        }
        initializeSetButton(row);

        setRowHeight(row, 8);
        updateColor(row);
    }
    resizeColumnsToContents();
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MUpdateDataVariableTable::dragEnterEvent(QDragEnterEvent *event)
{
    const MMimeType dropType = MMimeData::parseMimeData(event->mimeData());
    // Accept drop of selectable data variables.
    if (dropType == MMimeType::SELECTABLE_DATA_VARIABLE)
    {
        event->acceptProposedAction();
    }
}


void MUpdateDataVariableTable::dropEvent(QDropEvent *event)
{
    // Get the type of the drop event.
    QString data;
    MMimeType dropType = MMimeData::parseMimeData(event->mimeData(), data);

    MSelectableDataVariable selectedDataVariable =
        MSelectableDataVariable::deserialize(data.toLocal8Bit());

    // Get the row we currently are to drop.
    int dropRow = rowAt(event->pos().y());
    if (dropRow < 0 || dropRow >= rowCount()) return;

    setReplacementVariableInTable(dropRow, selectedDataVariable);
}


void MUpdateDataVariableTable::dragMoveEvent(QDragMoveEvent *event)
{
    event->accept();
}


bool MUpdateDataVariableTable::allVariablesUpdated() const
{
    // A data source is updated if the right half of the table is populated.
    for (int row = 0; row < rowCount(); row++)
    {
        if (! isRowUpdated(row))
        {
            return false;
        }
    }
    return true;
}


QMap<MSelectableDataVariable, MSelectableDataVariable> MUpdateDataVariableTable::
getDataVariableMapping() const
{
    QMap<MSelectableDataVariable, MSelectableDataVariable> map;

    for (int row = 0; row < rowCount(); row++)
    {
        MSelectableDataVariable sourceVar(item(row, 0)->text(),
                                       item(row, 1)->text(),
                                       item(row, 2)->text());

        MSelectableDataVariable destVar(item(row, 4)->text(),
                                     item(row, 5)->text(),
                                     item(row, 6)->text());

        map.insert(sourceVar, destVar);
    }
    return map;
}


void MUpdateDataVariableTable::setSessionVariableInTable(
    int row, const MSelectableDataVariable& dataVariable)
{
    // Create non-editable table entries.
    auto *dataSourceIdItem = new QTableWidgetItem(dataVariable.dataSourceID);
    setItem(row, 0, dataSourceIdItem);

    auto *verticalLevelTypeItem = new QTableWidgetItem(
        MStructuredGrid::verticalLevelTypeToString(dataVariable.levelType));
    setItem(row, 1, verticalLevelTypeItem);

    auto *varNameItem = new QTableWidgetItem(dataVariable.variableName);
    setItem(row, 2, varNameItem);

    updateColor(row);
}


void MUpdateDataVariableTable::setReplacementVariableInTable(
    int row, const MSelectableDataVariable& dataVariable)
{
    // If we want to set a target variable, check whether the source variable
    // and target variable have same number of dimensions. Only replace 2-D
    // with 2-D and 3-D with 3-D fields.
    MSelectableDataVariable sessionDataVariable = getSessionDataVariable(row);

    if ((sessionDataVariable.levelType != dataVariable.levelType)
        && (sessionDataVariable.levelType == SINGLE_LEVEL
            || dataVariable.levelType == SINGLE_LEVEL))
    {

        QMessageBox msgBox;
        msgBox.setIcon(QMessageBox::Warning);
        msgBox.setText(QString("Session and replaced data variable have a "
                               "mismatching number of dimensions. Replacement "
                               "variables are required to have the same number "
                               "of dimensions as the variable in the session file. "
                               "Please select a fitting variable."));
        msgBox.exec();
        return;
    }

    // Create non-editable table entries.
    auto *dataSourceIdItem = new QTableWidgetItem(dataVariable.dataSourceID);
    setItem(row, 4, dataSourceIdItem);

    auto *verticalLevelTypeItem = new QTableWidgetItem(
        MStructuredGrid::verticalLevelTypeToString(dataVariable.levelType));
    setItem(row, 5, verticalLevelTypeItem);

    auto *varNameItem = new QTableWidgetItem(dataVariable.variableName);
    setItem(row, 6, varNameItem);

    updateColor(row);
}


void MUpdateDataVariableTable::updateLoadedFields()
{
    for (int row = 0; row < rowCount(); row++)
    {
        MSelectableDataVariable dataVariable(item(row, 0)->text(),
                                           item(row, 1)->text(),
                                           item(row, 2)->text());

        if (dataVariable.isInSystemManagerAvailable() && ! isRowUpdated(row))
        {
            // Populate right side if the user did not already select a
            // replacement and the data source is loaded.
            setReplacementVariableInTable(row, dataVariable);
        }
        updateColor(row);
    }
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MUpdateDataVariableTable::updateColor(int row) const
{
    QColor backgroundColor;
    // Red if right half of table is not populated.
    if (isRowUpdated(row))
    {
        backgroundColor = QColor(150, 255, 150);
    }
    else
    {
        backgroundColor = QColor(255, 150, 150);
    }

    for (int i = 0; i < 7; i++)
    {
        // Skip arrow cells and uninitialized cells.
        if (i == 3 || ! item(row, i)) continue;
        item(row, i)->setBackground(backgroundColor);
    }
}


void MUpdateDataVariableTable::initializeArrow(int row)
{
    // Initialize the arrow centered int the widget.
    auto arrowItem = new QTableWidgetItem(QChar(0x27F6));
    arrowItem->setData(Qt::FontRole, QFont("", 14));
    arrowItem->setTextAlignment(Qt::AlignCenter);

    setItem(row, 3, arrowItem);
}


void MUpdateDataVariableTable::initializeSetButton(int row)
{
    // Create the button.
    auto* rowButton = new QPushButton();
    rowButton->setText("set");

    auto* cellWidget = new QWidget();
    // Create a layout to put the button in, so it can be added as a cell widget.
    auto* cellLayout = new QHBoxLayout(cellWidget);
    cellLayout->addWidget(rowButton);
    cellLayout->setAlignment(Qt::AlignCenter);
    cellLayout->setContentsMargins(0, 0, 0, 0);
    cellWidget->setLayout(cellLayout);

    // Connect a click on the button to the signal emitting the row clicked.
    connect(rowButton, &QPushButton::clicked, this,
            [this, row] { setButtonClickedSignal(row); });

    setCellWidget(row, 7, cellWidget);
}


void MUpdateDataVariableTable::setItem(int row, int column,
                                      QTableWidgetItem *item)
{
    item->setFlags(item->flags() ^ Qt::ItemIsEditable);
    QTableWidget::setItem(row, column, item);
}


bool MUpdateDataVariableTable::isRowUpdated(int row) const
{
    // Check if the vertical level type column is not empty.
    return ! item(row, 5) || item(row, 5)->text() != "";
}


MSelectableDataVariable
MUpdateDataVariableTable::getSessionDataVariable(int row) const
{
    MSelectableDataVariable var(item(row, 0)->text(),
                                      item(row, 1)->text(),
                                      item(row, 2)->text());
    return var;
}

} // Met3D