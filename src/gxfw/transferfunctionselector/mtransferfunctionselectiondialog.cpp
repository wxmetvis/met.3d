/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "mtransferfunctionselectiondialog.h"
#include "ui_mtransferfunctionselectiondialog.h"
#include "mtransferfunctionitemwidget.h"
#include "gxfw/mglresourcesmanager.h"
#include "util/mfiletypes.h"

namespace Met3D
{

MTransferFunctionSelectionDialog::MTransferFunctionSelectionDialog(int numSteps,
                                                                   QWidget *parent) :
        QDialog(parent),
        ui(new Ui::MTransferFunctionSelectionDialog),
        steps(numSteps)
{
    auto sysMC = MSystemManagerAndControl::getInstance();
    colourmaps = sysMC->getColourmapPool();

    ui->setupUi(this);

    ui->tfList->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    importButton = ui->buttonBox->addButton("Import", QDialogButtonBox::ActionRole);

    refreshColourmapList();
}


MTransferFunctionSelectionDialog::~MTransferFunctionSelectionDialog()
{
    delete ui;

    for (auto item : items)
    {
        delete item;
    }

    for (auto widget : itemWidgets)
    {
        delete widget;
    }
}


void MTransferFunctionSelectionDialog::onFilterEdited(const QString &text)
{
    QStringList names = items.keys();
    QStringList filteredNames = names.filter(text, Qt::CaseInsensitive);

    for (auto item : items)
    {
        item->setHidden(true);
    }

    for (const QString &name : filteredNames)
    {
        items[name]->setHidden(false);
    }
}


void MTransferFunctionSelectionDialog::onSelected(QListWidgetItem *to, QListWidgetItem *from)
{
    Q_UNUSED(from)

    if (to == nullptr)
    {
        return;
    }

    selectedColourmap = to->data(Qt::UserRole).toString();
}


void MTransferFunctionSelectionDialog::onItemConfirmed(QListWidgetItem *item)
{
    selectedColourmap = item->data(Qt::UserRole).toString();

    QDialog::accept();
}


void MTransferFunctionSelectionDialog::onButtonClicked(QAbstractButton *button)
{
    if (button == importButton)
    {
        QString filename = MFileUtils::getOpenFileName(
                nullptr,
                "Import colour map",
                {FileTypes::M_VAPOR_XML, FileTypes::M_PARA_VIEW_COLORMAP_XML},
                "/");

        if (filename.isEmpty()) return;

        // Vapor XML
        if (FileTypes::M_VAPOR_XML.isType(filename))
        {
            if (colourmaps->loadVaporXmlColourmap(filename))
            {
                LOG4CPLUS_INFO(mlog, "Vapor XML " << filename << " loaded.");

                refreshColourmapList();
            }
        }
        // XML files (ex. Paraview colourmap XML)
        else if (FileTypes::M_PARA_VIEW_COLORMAP_XML.isType(filename))
        {
            if (colourmaps->loadParaviewXmlColourmap(filename))
            {
                LOG4CPLUS_INFO(mlog, "ParaView colourmap XML " << filename << " loaded.");

                refreshColourmapList();
            }
        }
        // Unsupported files
        else
        {
            QString ext = filename.split(".").last();
            LOG4CPLUS_WARN(mlog,
                           "Met.3D currently does not support or recognize colourmaps with the "
                           << ext << " extension.");
        }
    }
}


void MTransferFunctionSelectionDialog::addColourmap(const QString &name, MColourmap *map)
{
    if (items.contains(name))
    {
        return;
    }

    QImage tf(steps, 1, QImage::Format_ARGB32);

    for (int i = 0; i < steps; i++)
    {
        float scalar = ((float) i) / ((float) (steps - 1));

        QColor colour = map->scalarToColour(scalar);

        tf.setPixelColor(i, 0, colour);
    }

    auto itemWidget = new MTransferFunctionItemWidget();
    itemWidget->setData(name, tf);

    auto item = new QListWidgetItem();
    item->setData(Qt::UserRole, name);

    item->setSizeHint(itemWidget->sizeHint());

    ui->tfList->addItem(item);
    ui->tfList->setItemWidget(item, itemWidget);

    items[name] = item;
    itemWidgets[name] = itemWidget;
}


void MTransferFunctionSelectionDialog::refreshColourmapList()
{
    QString selected = selectedColourmap;

    ui->tfList->clear();

    items.clear();

    for (auto widget : itemWidgets)
    {
        delete widget;
    }

    itemWidgets.clear();

    for (const QString &map : this->colourmaps->availableColourmaps())
    {
        addColourmap(map, this->colourmaps->colourmap(map));
    }

    ui->tfList->clearSelection();

    if (items.contains(selected))
    {
        items[selected]->setSelected(true);
        ui->tfList->scrollToItem(items[selected]);
    }
}

}