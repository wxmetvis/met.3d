/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef SELECTPOINTSOURCEDIALOG_H
#define SELECTPOINTSOURCEDIALOG_H

// standard library imports

// related third party imports
#include <QDialog>

// local application imports


namespace Ui {
class MSelectPointSourceDialog;
}

namespace Met3D
{
class MPointGeneratorInterface;

/**
 * @c MSelectPointSourceDialog implements a dialog from which the user can
 * select a registered point generating interface.
 */
class MSelectPointSourceDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit MSelectPointSourceDialog(QWidget *parent = nullptr);

    ~MSelectPointSourceDialog() override;

    /**
     * @return The point generator interface currently selected from the table.
     * Returns a nullptr if no selection is made.
     */
    MPointGeneratorInterface* getSelectedInterface();

private:
    /**
     * Creates table entries for point source selection dialog.
     */
    void createSourceEntries();

    Ui::MSelectPointSourceDialog *ui;
};

} // namespace Met3D

#endif // SELECTPOINTSOURCEDIALOG_H
