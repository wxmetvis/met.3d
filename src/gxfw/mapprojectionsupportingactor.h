/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020 Kameswarro Modali [*]
**  Copyright 2017 Bianca Tost [+]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef ROTATEDGRIDSUPPORTINGACTOR_H
#define ROTATEDGRIDSUPPORTINGACTOR_H

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mactor.h"
#include "gxfw/gl/texture.h"
#include "gxfw/properties/menumproperty.h"
#include "gxfw/properties/mpointfproperty.h"
#include "gxfw/properties/mstringproperty.h"
#include "gxfw/properties/mbuttonproperty.h"


class MGLResourcesManager;
class MSceneViewGLWidget;

namespace Met3D
{

/**
  @brief MMapProjectionSupportingActor is a abstract base class for actors
  supporting map projections.

  It adds properties for (a) proj.org supported projections and (b) for rotated
  pole projections. For the latter, properties are included to define rotated
  north pole coordinates and to choose between whether to use rotation or not
  and if rotation is used whether to rotate the bounding box or to treat the
  bounding box coordinates as regular coordinates.

  It is used by @ref MBaseMapActor and @ref MGraticuleActor .
  */
class MMapProjectionSupportingActor : public MActor
{
public:
    typedef enum {
        MAPPROJECTION_CYLINDRICAL = 0,
        MAPPROJECTION_ROTATEDLATLON = 1,
        MAPPROJECTION_PROJ_LIBRARY = 2,
    } MapProjectionType;

    MMapProjectionSupportingActor(QList<MapProjectionType> supportedProjections);
    ~MMapProjectionSupportingActor();

    QString getSettingsID() override { return "MapProjectionEnablingActor"; }

    void loadConfigurationPrior_V_1_14(QSettings *settings) override;

    /**
      Call this method from onQtPropertyChanged() implementations to update
      the map projection parameters when the projection type has been changed.
     */
    virtual void updateMapProjectionProperties();

    static QString mapProjectionToString(MapProjectionType mapProjection);

    static MapProjectionType stringToMapProjection(const QString& mapProjection);

    MapProjectionType getMapProjection() { return mapProjection; }

    QPointF getRotatedNorthPole() { return rotatedNorthPoleProp; }

    QString getProjLibraryString() { return projLibraryString; }

    /**
     * Generates a unique identifier for the current map projection settings
     * that can be used as a variable for data requests.
     * @return The unique identifier.
     */
    QString getProjectionIdentifier() const;

    /**
     * @return True if the projection is currently a Proj4 projection.
     */
    bool isMapProjectionProj4() const;

    /**
     * @return True if the projection is currently a rotated latlon projection.
     */
    bool isMapProjectionRotated() const;

protected:
    MProperty mapProjectionPropertiesSubGroup;

    MEnumProperty mapProjectionTypesProp;
    MapProjectionType mapProjection;
    MapProjectionType previousMapProjection;

    // Rotated lat-lon grid.
    MPointFProperty rotatedNorthPoleProp;

    // Proj-library based projection.
    QString projLibraryDefaultString;
    QString projLibraryString;
    MStringProperty projLibraryStringProp;
    MButtonProperty projLibraryApplyProp;
};

} // namespace Met3D

#endif // ROTATEDGRIDSUPPORTINGACTOR_H
