/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mselectpointfilterdialog.h"
#include "ui_mselectpointfilterdialog.h"

// standard library imports

// related third party imports
#include <QTableWidget>

// local application imports
#include "trajectories/pointfilter/pointfilterfactory.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSelectPointFilterDialog::MSelectPointFilterDialog(QWidget *parent)
    : QDialog(parent),
      ui(new Ui::MSelectPointFilterDialog)
{
    ui->setupUi(this);
    createFilterEntries();
}


MSelectPointFilterDialog::~MSelectPointFilterDialog()
{
    delete ui;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

QString MSelectPointFilterDialog::getSelectedFilterName()
{
    int row = ui->pointFilterTable->currentRow();
    return ui->pointFilterTable->item(row, 0)->text();
}


void MSelectPointFilterDialog::createFilterEntries()
{
    QTableWidget *table = ui->pointFilterTable;

    // Set the data field table's header.
    table->setColumnCount(1);
    table->setHorizontalHeaderLabels(QStringList() << "Filter name");

    MPointFilterFactory *filterFactory = MPointFilterFactory::getInstance();
    QStringList createdFilters = filterFactory->getRegisteredFilterNames();

    for (const QString& registeredFilter : createdFilters)
    {
        int row = table->rowCount();
        table->setRowCount(row + 1);
        // Insert the element.
        table->setItem(row, 0, new QTableWidgetItem(registeredFilter));
    }

    // Resize the table's columns to fit actor names.
    table->resizeColumnsToContents();
    // Set table width to always fit window size.
    table->horizontalHeader()->setStretchLastSection(true);
    // Disable resize of column by user.
    table->horizontalHeader()->setSectionResizeMode(
        QHeaderView::ResizeMode::Fixed);
    // Resize widget to fit table size.
    this->resize(table->width(), table->height());
}

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // namespace Met3D