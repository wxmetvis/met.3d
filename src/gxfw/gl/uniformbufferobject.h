/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_UNIFORMBUFFEROBJECT_H
#define MET_3D_UNIFORMBUFFEROBJECT_H

// standard library imports

// related third party imports

// local application imports
#include "gxfw/gl/abstractgpudataitem.h"

namespace GL
{

/**
 * This class encapsulates an OpenGL uniform buffer object.
 * Uniform buffers are shared between all shaders using the buffer bound to the same
 * binding point. This means, the data is only uploaded once.
 * The binding points are shared between all buffer objects such as
 * shader storage buffers and uniform buffers.
 * @tparam T The c++ type that mirrors the OpenGL buffer definition.
 */
template<typename T>
class MUniformBufferObject : public MAbstractGPUDataItem
{
public:
    /**
     * Create a new uniform buffer object.
     * @param requestID Request id for this data item.
     */
    explicit MUniformBufferObject(const QString &requestID);

    ~MUniformBufferObject() override;

    unsigned int getGPUMemorySize_kb() override;

    /**
     * Upload data of instance of type @c T to the uniform buffer object.
     * @param data Pointer to the data to upload.
     * @param usage Usage of the data. Example: @c GL_STATIC_DRAW
     */
    void uploadData(const T *data, GLenum usage);

    /**
     * Update the uniform buffer for a single member @p member of the struct
     * @c buffer.
     * @tparam M The type of the member that should be updated in the UBO.
     * @param buffer The buffer that contains the member.
     * @param member The member that contains the data that should be updated.
     */
    template<typename M>
    void updateSubData(const T *buffer, const M *member)
    {
        glBindBuffer(GL_UNIFORM_BUFFER, ubo);
        glBufferSubData(GL_UNIFORM_BUFFER,
                        reinterpret_cast<GLintptr>(member) - reinterpret_cast<GLintptr>(buffer),
                        sizeof(M), member);
        glBindBuffer(GL_UNIFORM_BUFFER, 0);
    };

    /**
     * Bind the uniform buffer object to a uniform buffer binding point.
     * @param bindingPoint The specified binding point.
     */
    void bind(GLuint bindingPoint);

    /**
     * @return The underlying OpenGL object.
     */
    GLuint getBufferObject();

protected:

    /**
     * The OpenGL uniform buffer object.
     */
    GLuint ubo;
};


template<typename T>
MUniformBufferObject<T>::MUniformBufferObject(const QString &requestID)
        : MAbstractGPUDataItem(requestID), ubo(0)
{
    glGenBuffers(1, &ubo);
}


template<typename T>
MUniformBufferObject<T>::~MUniformBufferObject()
{
    glDeleteBuffers(1, &ubo);
}


template<typename T>
unsigned int MUniformBufferObject<T>::getGPUMemorySize_kb()
{
    return sizeof(T) / 1024.;
}


template<typename T>
void MUniformBufferObject<T>::uploadData(const T *data, GLenum usage)
{
    glBindBuffer(GL_UNIFORM_BUFFER, ubo);
    glBufferData(GL_UNIFORM_BUFFER, sizeof(T), data, usage);
    glBindBuffer(GL_UNIFORM_BUFFER, 0);
}


template<typename T>
void MUniformBufferObject<T>::bind(GLuint bindingPoint)
{
    glBindBufferBase(GL_UNIFORM_BUFFER, bindingPoint, ubo);
}


template<typename T>
GLuint MUniformBufferObject<T>::getBufferObject()
{
    return ubo;
}

} // Met3D

#endif //MET_3D_UNIFORMBUFFEROBJECT_H
