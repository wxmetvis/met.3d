/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2023-2025 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef TEXTMANAGER_H
#define TEXTMANAGER_H

// standard library imports
#include <memory>

// related third party imports
#include <GL/glew.h>
#include <QRawFont>
#include <QMap>
#include <QChar>

// local application imports
#include "gxfw/mtypes.h"
#include "gxfw/mactor.h"
#include "gxfw/gl/shadereffect.h"
#include "gxfw/gl/texture.h"

namespace Met3D
{

// forward declarations
class MGLResourcesManager;
class MSceneViewGLWidget;


/**
  @brief TextureAtlasCharacterInfo contains information about the bitmap of a
  specific character (glyph) in the texture atlas.
  */
struct MTextureAtlasCharacterInfo {
    // Advance cursor by these values after the character has been drawn.
    float advanceX;
    float advanceY;
    // "bitmap" refers to the character bitmap stored in the texture atlas.
    // Values are given in pixels of the character bitmap; left and top refer
    // to the indention of the bitmap relative to the cursor position.
    float bitmapWidth;
    float bitmapHeight;
    float bitmapLeft;
    float bitmapTop;
    // Texture coordinates of where the character starts in the texture.
    float xOffset_TexCoords;
    float yOffset_TexCoords;
};


/**
  @brief MTextManager manages all labels (i.e. text strings) that appear in a
  scene.

  Labels can be added to the render queue with the addXY() methods. These
  methods create the geometry for a label and upload it to the GPU (using
  VBOs). All registered labels are rendered when renderToCurrentContext() is
  called by a @ref MSceneViewGLWidget.

  Label rendering is implemented using a texture atlas generated with FreeType
  2. Compare this tutorial
  (http://en.wikibooks.org/wiki/OpenGL_Programming/Modern_OpenGL_Tutorial_Text_Rendering_02)
  and this StackOverflow post
  (http://stackoverflow.com/questions/2071621/opengl-live-text-rendering) for
  more information.

  @todo Refactor to use GL::MVertexBuffer
  */
class MTextManager : public MActor
{
public:
    /**
     * Available text anchors for text.
     */
    enum TextAnchor {
        BASELINELEFT   =  0,
        BASELINERIGHT  =  1,
        BASELINECENTRE =  2,
        UPPERLEFT      =  3,
        UPPERRIGHT     =  4,
        UPPERCENTRE    =  5,
        LOWERLEFT      =  6,
        LOWERRIGHT     =  7,
        LOWERCENTRE    =  8,
        MIDDLELEFT     =  9,
        MIDDLERIGHT    = 10,
        MIDDLECENTRE   = 11
    };

    /**
     * Available coordinate systems, in which text can be placed.
     */
    enum CoordinateSystem {
        CLIPSPACE  = 0,
        WORLDSPACE = 1,
        LONLATP    = 2
    };

    MTextManager();
    ~MTextManager() override;

    /**
     * Specify the default font and size in which the fonts are rasterized. This method
     * must be called before the OpenGL initialisation of this actor is called.
     * @param fontFile The font file to set as the default font.
     * @param pixelSize The pixel size to rasterize the fonts in.
     */
    void setDefaultFont(QString fontFile, int pixelSize);

    /**
     * Return the default font used for text rendered by this actor.
     * @return The default font.
     */
    QFont getDefaultFont() const;

    void reloadShaderEffects() override;

    QString getSettingsID() override { return staticSettingsID(); }
    static QString staticSettingsID() { return "TextManager"; }

    /**
     * Creates a new label and adds it to the text manager.
     * Uses the default font of the text manager.
     * Generates the geometry for the text label.
     * @param text The text to add.
     * @param coordSys The used coordinate system.
     * @param x The x coordinate in the given coordinate system where the text is placed.
     * @param y The y coordinate in the given coordinate system where the text is placed.
     * @param z The z coordinate in the given coordinate system where the text is placed.
     * @param size The size of the text.
     * @param colour The colour of the text.
     * @param anchor The anchor of the label.
     * @param bbox Whether to draw the bounding box / background of the label.
     * @param bboxColour The colour of the bounding box / background.
     * @param bboxPadFraction Padding of the bounding box / background.
     * @param rotationDeg The rotation of the label.
     * @return The created label.
     */
    MLabel *addText(QString text, CoordinateSystem coordSys,
                    float x, float y, float z, float size,
                    QColor colour, TextAnchor anchor=BASELINELEFT,
                    bool bbox = false, QColor bboxColour = QColor(0,0,0,50),
                    float bboxPadFraction = 0.1, float rotationDeg = 0.f);

    /**
     * Creates a new label and adds it to the text manager.
     * Uses the given font family to render the font.
     * Generates the geometry for the text label.
     * @param text The text to add.
     * @param fontFamily The font family to use.
     * @param coordSys The used coordinate system.
     * @param x The x coordinate in the given coordinate system where the text is placed.
     * @param y The y coordinate in the given coordinate system where the text is placed.
     * @param z The z coordinate in the given coordinate system where the text is placed.
     * @param size The size of the text.
     * @param colour The colour of the text.
     * @param anchor The anchor of the label.
     * @param bbox Whether to draw the bounding box / background of the label.
     * @param bboxColour The colour of the bounding box / background.
     * @param bboxPadFraction Padding of the bounding box / background.
     * @param rotationDeg The rotation of the label.
     * @return The created label.
     */
    MLabel* addText(QString text, QString fontFamily,
                    CoordinateSystem coordsys,
                    float x, float y, float z, float size,
                    QColor colour, TextAnchor anchor=BASELINELEFT,
                    bool bbox=false, QColor bboxColour=QColor(0,0,0,50),
                    float bboxPadFraction=0.1, float rotationDeg = 0.f);

    /**
     * Removes the label with index @p id (obtained from @ref addText()) from
     * the label pool.
     */
    void removeText(MLabel* label);

    /**
     * Renders a list of labels.
     * @param sceneView The scene to render to.
     * @param labelList The labels to render.
     */
    void renderLabelList(MSceneViewGLWidget *sceneView,
                         const QList<MLabel *> &labelList);

    /**
     * Get the bounds of the given label in clip space.
     * @param sceneView The scene view in which the label is rendered.
     * @param label The label to get the bounds for.
     * @return The bounds of the label in clip space.
     */
    QRectF getLabelClipSpaceBounds(Met3D::MSceneViewGLWidget *sceneView, Met3D::MLabel *label);

    /**
     * Returns the list of supported font families.
     * @return The currently supported font families.
     */
    const QVector<QString> &getSupportedFontFamilies() const { return supportedFontFamilies; }

    bool hasLoadedFont(const QFont &font) const { return supportedFontFamilies.contains(font.family()); };

    /**
     * Adds a new font from the given Qt font, if it has not been added yet.
     * @param font The font to add.
     */
    void addFont(const QFont &font);

protected:
    void initializeActorResources() override;

    /**
      Renders the text queue. Overrides the virtual method
      MActor::renderToCurrentContext().

      @todo Possibly displaces labels that collide and draws a line from the
      desired position to the actual label.
      */
      //void renderToCurrentContext(MSceneViewGLWidget *sceneView) override;

private:
    /**
     * Encapsulates a text atlas for a single font or font file.
     */
    class TextAtlas
    {
    public:
        TextAtlas(const QFont &font, int pixelSize);
        ~TextAtlas();

        /**
         * Generates the texture atlas for this font for a given set of chars.
         * Leverages @c QRawFont to read and load the font and render the bitmaps
         * of the individual chars.
         * @param chars The chars to be rendered to the texture atlas.
         * @return @c true, if the atlas was generated otherwise @c false.
         */
        bool generateAtlas(const QVector<QChar> &chars);

        /**
         * Inserts a new char into the atlas, if the maximum number of chars in the
         * atlas has not yet been reached, or the char is not yet part of the atlas.
         * @param c The char to add.
         * @return true, if the char was successfully added.
         */
        bool insertChar(const QChar &c);

        /**
         * Inserts the glyph at the given glyph index into the font atlas.
         * @param idx The glyph index that corresponds to the added glyph.
         * @return true if added otherwise false.
         */
        bool insertGlyph(const quint32 &idx);

        /**
         * Checks if the glyph at the given glyph index @p glyph from the font is already
         * contained in the atlas.
         * @param glyph The glyph index to check.
         * @return true if contained, otherwise false.
         */
        bool contains(const quint32 &glyph) const { return characterInfos.contains(glyph); }

        /**
         * Checks if the text atlas is full.
         * @return true if no more glyphs can be stored.
         */
        bool isFull() const { return numGlyphs >= charsPerCol * charsPerRow; }

        /**
         * The font used to create the atlas.
         */
        QFont font;

        /**
         * The raw font used to create the atlas.
         */
        QRawFont rawFont;

        /**
         * The generated text atlas.
         */
        GL::MTexture *atlasTexture;

        // characterInfo provides information on which character can be found
        // where in the texture map and how it can be correctly aligned.
        QMap<quint32, MTextureAtlasCharacterInfo> characterInfos;
        int atlasHeight;  // in pixels of the image uploaded to GPU
        int atlasWidth;
        int maxCharWidth; // the largest char width in the font
        int maxCharHeight; // the largest char height in the font

        const int charsPerCol = 32; // the amount of char glyphs stored in a single column on the font atlas.
        const int charsPerRow = 32; // the amount of char glyphs stored in a row on the font atlas.

        int numGlyphs; // the number of glyphs stored on the atlas.
    };

    /**
     * Adds a new text atlas object to this text manager.
     * If the font family of the text atlas is already added, the new atlas
     * will not be added.
     * @param atlas The atlas to add.
     */
    void addTextAtlas(TextAtlas *atlas);

    /**
     * All the font atlases currently created.
     */
    QMap<QString, TextAtlas*> fontAtlases;

    /**
     * All currently supported font families loaded at this point.
     */
    QVector<QString> supportedFontFamilies;

    /**
     * All default characters, that will be loaded for each font. Other characters
     * wil be loaded on demand.
     */
    QVector<QChar> defaultAsciiChars;

    /**
     * The font object of the registered default font.
     */
    QFont defaultFont;

    /**
     * The size in pixels for a single font glyph.
     */
    int fontPixelSize;

    /**
     * Shader effect to render fonts.
     */
    std::shared_ptr<GL::MShaderEffect> textEffect;

    // One vertex buffer for the geometry, can be shared across OpenGL contexts
    // (= scene views).
    GLuint directRenderingTextVBO;
    GLuint directRenderingBBoxVBO;

    // The hash set "labelPool" stores the pool of labels (addText(),
    // removeText(), renderToCurrentContext()).
    QSet<MLabel*> labelPool;
};

} // namespace Met3D

#endif // TEXTMANAGER_H
