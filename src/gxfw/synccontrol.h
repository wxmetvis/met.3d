/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2021 Marc Rautenhaus [*, previously +]
**  Copyright 2016-2017 Bianca Tost [+]
**  Copyright 2022-2023 Thorwin Vogt [*]
**  Copyright 2024 Christoph Fischer [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef SYNCCONTROL_H
#define SYNCCONTROL_H

// standard library imports

// related third party imports
#include <QHBoxLayout>
#include <QPushButton>
#include <QActionGroup>
#include <QWidgetAction>
#include <QDateTimeEdit>
#include <QSpinBox>
#include <QSet>
#include <QComboBox>
#include <QLabel>
#include <QSettings>
#include <QUndoCommand>
#include <QQueue>

// local application imports
#include "util/mstopwatch.h"
#include "mcameraanimationcontroller.h"
#include "gxfw/properties/mboolproperty.h"

namespace Ui {
class MSyncControl;
}

namespace Met3D
{

class MSynchronizedObject;
class MSceneViewGLWidget;
class MWeatherPredictionDataSource;
class MTrajectoryReader;

enum MSynchronizationType
{
    SYNC_VALID_TIME = 0,
    SYNC_INIT_TIME = 1,
    SYNC_INIT_VALID_TIME = 2,
    SYNC_ENSEMBLE_MEMBER = 3,
    SYNC_UNKNOWN = 4
};


/**
 @brief MLabelledWidgetAction provides custom entries (=actions) for QMenus.
 A widget that should be displayed can be specified, as well as text that
 appears before and after the widget.
 */
class MLabelledWidgetAction : public QWidgetAction
{
    Q_OBJECT

public:
    MLabelledWidgetAction(const QString& labelFront, const QString& labelBack,
                          QWidget* customWidget, QWidget *parent = nullptr);

    QWidget* getCustomWidget() { return customWidget; }

private:
    QWidget *customWidget;
};

/**
 * A struct describing a sync event that should be processed.
 */
struct MSyncEvent
{
    /**
     * The type of the sync event.
     */
    MSynchronizationType syncType;

    /**
     * The QVariants passed along the sync event.
     */
    QVector<QVariant> syncVariants;
};


/**
 @brief MSyncControl provides time and ensemble settings with which individual
 actors can synchronize. The functionality has been separated from
 @ref MSceneControl to allow for comprehensive synchronization possibilities.

 @todo MSyncControl should query all connected actors for available ensemble
 members.
 */
class MSyncControl : public QWidget
{
friend class MSessionManagerDialog;
    Q_OBJECT
    
public:
    explicit MSyncControl(QString id, QWidget *parent = 0);

    ~MSyncControl() override;

    /**
      Returns the current valid time.
     */
    QDateTime validDateTime() const;

    /**
     * Set the current valid date time if possible.
     * @param valid The valid date time.
     * @param noEvent Optional: The sync event will not be emitted if true.
     */
    void setValidDateTime(const QDateTime &dateTime, bool noEvent = false);

    /**
      Returns the current initialisation time (forecast base time).
     */
    QDateTime initDateTime() const;

    /**
     * Set the current init date time if possible.
     * @param init The init time.
     * @param noEvent Optional: The sync event will not be emitted if true.
     */
    void setInitDateTime(const QDateTime &dateTime, bool noEvent = false);

    void setJointInitValidDateTime(const QDateTime &initDateTime,
                                   const QDateTime &validDateTime);

    /**
      Select the init time that is at the @p posInAvailableInitTimes position
      in the list of init times available from the data sources set by
      @ref restrictControlToDataSources().
      If @p posInAvailableInitTimes == -1, the last available init time will
      be selected.
      The valid time will be set to a time equal to the init time if possible
      (i.e. lead time = 0), if this valid time does not exist the next following
      valid time will be selected.
     */
    void selectInitTimeAsListPosition(int posInAvailableInitTimes);

    void copyValidTimeToTimeAnimationFromTo();

    /**
     Returns the current ensemble member. If the ensemble mode is set to
     "mean", returns -1.
     */
    int ensembleMember() const;

    QString getID() { return syncID; }

    void registerSynchronizedClass(MSynchronizedObject *object);

    void deregisterSynchronizedClass(MSynchronizedObject *object);

    /**
     * Update the synchronization order of synchronization events emitted
     * to synchronized objects my making sure that @c first happens
     * before @c second.
     * @param first The object to be notified before @c second.
     * @param second The object to be notified after @c first.
     */
    void updateSynchronizationOrder(MSynchronizedObject *first,
                                    MSynchronizedObject *second);

    void synchronizationCompleted(MSynchronizedObject *object);

    /**
      Disconnect all registered synchronized object.
     */
    void disconnectSynchronizedObjects();

    /**
      Returns the delay between two animation steps in milliseconds, as
      specified by the user in the animation pane.
     */
    unsigned int getAnimationDelay_ms();

    /**
      Enable/disable whether images written to disk in animation mode should
      automatically be overwritten in case the files already exist.
     */
    void setOverwriteAnimationImageSequence(bool overwrite);

    /**
      Sets the "begin"/"end" animation GUI elements in the animation pane
      to the specified times.
     */
    void setAnimationTimeRange(QDateTime startTime, QDateTime endTime);

    /**
      Used in batch mode to setup sync control to use the
      setup camera sequence for the rendered scene.
     */
    void setupCameraSequenceBatchMode(bool useCameraSequence);

    /**
     * Add a data source to this sync control and restrict the sync control to
     * the times of the previously added data sources and the new data source.
     * @param dataSourceIdentifier The identifier of the new data source to
     * be integrated into this sync control.
     */
    void addDataSourceToControl(const QString& dataSourceIdentifier);

    /**
     * @return Whether a synchronization is currently in progress.
     */
    bool isSynchronizationInProgress() const { return synchronizationInProgress; }

    /**
      Advance scene time by the value specified in @p ui->timeStepSpinBox and
      @p ui->timeUnitComboBox.
      */
    void timeForward();

    /**
      Backward version of @ref timeForward().
      */
    void timeBackward();

public slots:
    /**
     * Advances the time of the sync control forward when the corresponding UI button is clicked.
     */
    void onTimeForwardClicked();

    /**
     * Advances the time of the sync control forward when the corresponding UI button is clicked.
     */
    void onTimeBackwardClicked();

    /**
      Opens dialog to select from which data sources to draw valid times,
      init times and members to restrict control to.
      */
    void selectDataSources();

    /**
      Opens dialog to save configuration of sync control.
      */
    void saveConfigurationToFile(QString filename = "");

    /**
      Opens dialog to load configuration of sync control.
      */
    void loadConfigurationFromFile(QString filename = "");

    /**
     Save sync control-specific configuration to the @ref QSettings object @p
     settings.
     */
    void saveConfiguration(QSettings *settings);

    /**
     Load sync control-specific configuration from the @ref QSettings object @p
     settings.
     */
    void loadConfiguration(QSettings *settings);

    /**
      @brief Checks @p dataSources for consistency and prints error
      messages if a given data source id has no corresponding data source or if
      the data source does not contain any init times, valid times and ensemble
      members.

      If @p selectedDataSources does not contain any, loadDataSourcesFromFrontend
      tests all register data sources for data sources with init times, valid
      times and ensemble members. If the method cannot find any suitable
      data source it prints a error message and returns.
      */
    void restrictToDataSourcesFromSettings(const QStringList& dataSources);

    /**
      @brief Fetches init and valid times and members from the data sources
      given by their IDs stored in @p selectedDataSources if present.

      If @p selectedDataSources is empty, it is assumed to use all available
      data sources. It is essential that if the data sources are given that they
      are valid data sources containing init times, valid times and ensemble
      members. But if loadDataSourcesTimesAndMembers uses all available data
      sources, it checks whether they contain init times, valid times and
      ensemble members and quits quietly if no suitable data source was found.
     */
    void restrictControlToDataSources(
            QStringList selectedDataSources=QStringList());

    /**
    * Synchronizes the time selection widgets in the frontend with the time properties
    * of the currently linked data sources in the sync control. This method updates
    * the time restrictions to reflect the time ranges with the currently linked
     * data sources.
    */
    void updateTimeSelectionWidgetsFromDataSources();

    /**
     * Add the available init and valid times from the given @p dataSource
     * to this sync control's available init and valid times.
     * @param dataSource the NWP data source to scan for times.
     */
    void addAvailableTimesFromNWPSource(MWeatherPredictionDataSource* dataSource);

    /**
     * Add the available init and valid times from the given @p dataSource
     * to this sync control's available init and valid times.
     * @param dataSource the trajectory data source to scan for times.
     */
    void addAvailableTimesFromTrajectorySource(MTrajectoryReader* dataSource);
    
    /**
      Advance time (forward or backward, depending on settings) in animation
      mode (called by the animation timer).
      */
    void timeAnimationAdvanceTimeStep();

    /**
      Start animation over time.
      */
    void startTimeAnimation();

    /**
      Stop animation over time.
      */
    void stopTimeAnimation();

    /**
      Programmatically start the time animation. If @p saveImages is @p true,
      image saving will be enabled.
      If @p useCameraSequence is true, the animation is controlled by the camera sequence setup
      for the currently rendered scene.
     */
    void startTimeAnimationProgrammatically(bool saveImages=true);

    /**
     * Check if an animation is currently running and active.
     * @return True, if an animation is running.
     */
    bool active() const;

signals:
    /**
     Emitted before any of the synchronization signals below are emitted. Can
     be used, for instance, to block operations during synchonization.
     */
    void beginSynchronization();

    /**
     Emitted after any of the synchronization signals below have been emitted.
     */
    void endSynchronization();

    /**
     Emitted whenever the valid time changes.
     */
    void validDateTimeChanged(const QDateTime &datetime);

    /**
     Emitted whenever the init time changes.
     */
    void initDateTimeChanged(const QDateTime &datetime);

    /**
     Emitted whenever the current ensemble member or ensemble mode changes. If
     the ensemble mode is "mean", a "-1" is transmitted.
     */
    void ensembleMemberChanged(const int member);

    void imageOfTimeAnimationReady(QString path, QString fileName);

    void timeAnimationBegins();

    void timeAnimationEnds();

    void synchronizationStarted(Met3D::MSyncEvent event);

protected slots:
    void onValidDateTimeChange(const QDateTime &datetime);

    void onInitDateTimeChange(const QDateTime &datetime);

    void onEnsembleMemberChanged(int index);

    void onEnsembleMeanChanged(int state);

    /**
      Copy initial time to @ref timeAnimationFrom.
      */
    void copyInitToFrom();

    /**
      Copy valid time to @ref timeAnimationFrom.
      */
    void copyValidToFrom();

    /**
      Copy inital time to @ref timeAnimationTo.
      */
    void copyInitToTo();

    /**
      Copy valid time to @ref timeAnimationTo.
      */
    void copyValidToTo();

    void onAnimationLoopGroupChanged(QAction *action);

    MSceneViewGLWidget* getSceneViewChosenInAnimationPane();

    void activateTimeAnimationImageSaving(bool activate);

    /**
      Emit signal so that connected scene view can save the current image.
     */
    void emitSaveImageSignal();

    void switchSelectedView(QString viewID);

    void changeSaveAnimationDirectory();

    void adjustSaveAnimationDirectoryLabelText();

    /**
     * Slot called when the UI spinbox for the time step changes its value.
     * @param newValue The new value of the time step.
     */
    void onTimeStepChanged(int newValue);

    /**
     * Slot called when the UI combobox for the time step unit changes its index.
     * @param index The new index.
     */
    void onTimeStepUnitChanged(int index);

    /**
     * Slot called when the UI combobox for the time integration type changes its index.
     * @param index The new index.
     */
    void onIntegrationTypeChanged(int index);

    /**
     * Slot that is called, when this sync control finishes a synchronization event.
     * It should be called via a queued connection.
     * @param event The completed sync event.
     */
    void onSynchronizationCompleted(Met3D::MSyncEvent event);

private:
    /**
      Get the time relative to the given @p time in positive (sign=1) or
      negative (sign=-1) direction based on the currently set UI parameters.
      This gets called to get the new time when clicking next/previous time
      step via the UI.
      @param time The time from which to compute the new time.
      @param sign the direction (-1 for previous, +1 for next)
      @return The relative time.
      */
    QDateTime getRelativeTimeStepTo(QDateTime time, int sign) const;

    /**
     * Checks whether the displayed init time would change by changing it to
     * the given @p newInitDateTime.
     * @param newInitDateTime The new time to check.
     * @return True if the displayed init time would change, thus is not
     * currently set but can be changed to.
     */
    bool wouldInitDateTimeChange(const QDateTime& newInitDateTime) const;

    /**
     * Checks whether the displayed valid time would change by changing it to
     * the given @p newValidDateTime.
     * @param newValidDateTime The new time to check.
     * @return True if the displayed valid time would change, thus is not
     * currently set but can be changed to.
     */
    bool wouldValidDateTimeChange(const QDateTime& newValidDateTime) const;

    /**
     Updates the label that displays the time difference between valid and init
     time.
     */
    void updateTimeDifference();

    /**
     * Guess the synchronization type to be shown in the UI based on the current
     * init and valid times known to this sync control. This current simple
     * heuristic results in SYNC_VALID_TIME by default. If for each valid time
     * there is a corresponding init time, it results in SYNC_INIT_VALID_TIME
     * instead. The UI dropdown element is updated accordingly.
     */
    void updateSynchronizationTypeBestGuess();

    /**
     * Get a valid date time from the list of available date times @p availableDateTimes,
     * based on the current date time @p oldDateTime and the requested date time
     * @p dateTime.
     * @param dateTime The requested date time from the list of date times.
     * @param oldDateTime The current date time.
     * @param availableDateTimes The available date times.
     * @return The new date time
     */
    static QDateTime handleMissingDateTime(const QDateTime &dateTime,
                               const QDateTime &oldDateTime,
                               const QList<QDateTime> &availableDateTimes);

    void beginSceneSynchronization();

    void endSceneSynchronization();

    /**
     * Process a synchronization event.
     * @param event The event to process.
     */
    void processSynchronizationEvent(const MSyncEvent &event);

    /**
     * Queue a new synchronization event. If no events are currently
     * queued, it is processed immediately.
     * @param syncType The type of sync event to queue.
     * @param syncVariants The synchronization variants.
     * @return The newly queued synchronization event.
     */
    MSyncEvent queueSynchronizationEvent(MSynchronizationType syncType,
                                                const QVector<QVariant>& syncVariants);

    /**
      Enables/disables the GUI elements that control time. (Used to disable
      time control during time animation).
     */
    void setTimeSynchronizationGUIEnabled(bool enabled);

    void setSynchronizationGUIEnabled(bool enabled);

    void setAnimationTimeToStartTime(const QDateTime& startDateTime);

    /**
      Prints information about the current synchronization status (contents of
      @ref pendingSynchronizations, etc.).
      Specify the @p callPoint to identify the part of the code from which
      the function was called.
     */
    void debugOutputSyncStatus(QString callPoint);

    /**
     * Check if the given date time has reached the time animation limit.
     * @param dateTime The given date time to check.
     * @param backward Whether we check a backwards animation limit.
     * @return True, if the limit has been reached by the date time.
     */
    bool dateTimeLimitReached(const QDateTime &dateTime, bool backward) const;

    /**
     * Set the delta time step programmatically to avoid triggering an undoable
     * command.
     * @param delta The new time step delta.
     */
    void setDeltaTimeStep(int delta);

    /**
     * Set the delta time step unit programmatically to avoid triggering an undoable
     * command.
     * @param newUnit The new delta time step unit.
     */
    void setDeltaTimeStepUnit(int newUnit);

    /**
     * Set the integration type of this sync control.
     * @param type The new integration type.
     */
    void setIntegrationType(MSynchronizationType type);

    /**
     * Apply an integration time step and handle the limits of the time range.
     * @param backwards Whether to integrate backwards.
     * @param start The start of the animation. Used for loop animations.
     */
    void integrateTimeStep(bool backwards, const QDateTime &start);

    /**
     * Sets the synchronized ensemble member, if possible.
     */
    void setEnsembleMember(int member);

    /**
     * Enables or disables the ensemble mean mode.
     * @param enable Whether to enable or disable it.
     */
    void toggleEnsembleMean(bool enable);

    /**
     * Set the currently selected data sources.
     * @param dataSources The new data sources.
     */
    void setSelectedDataSources(const QStringList &dataSources);

    Ui::MSyncControl *ui;

    // Properties to control time animations.
    QMenu *timeAnimationDropdownMenu;
    QSpinBox *timeAnimationDelaySpinBox;
    QWidget *timeAnimationFromWidget;
    QWidget *timeAnimationToWidget;
    QHBoxLayout *timeAnimationFromLayout;
    QHBoxLayout *timeAnimationToLayout;
    QDateTimeEdit *timeAnimationFrom;
    QDateTimeEdit *timeAnimationTo;
    QPushButton *copyInitTimeToAnimationFromButton;
    QPushButton *copyValidTimeToAnimationFromButton;
    QPushButton *copyInitTimeToAnimationToButton;
    QPushButton *copyValidTimeToAnimationToButton;
    QActionGroup *timeAnimationLoopGroup;
    QAction *timeAnimationSinglePassAction;
    QAction *timeAnimationUseSequenceAction;
    QAction *timeAnimationLoopTimeAction;
    QAction *timeAnimationBackForthTimeAction;
    QAction *timeAnimationReverseTimeDirectionAction;
    QTimer *animationTimer;

    // Properties for time animations using a camera sequence
    /**
      Animation controller used for camera animation
     */
    MCameraAnimationController *cameraAnimationController;

    /**
      Delay used between animation frames.
      Essentially the frame time for the camera animation.
     */
    int currentAnimDelay;

    // Properties to control saving of images of time serie.
    MBoolProperty saveAnimationImages;
    QLineEdit *saveAnimationFileNameLineEdit;
    QComboBox *saveAnimationFileExtensionComboBox;
    QLabel *saveAnimationDirectoryLabel;
    QPushButton *saveAnimationDirectoryChangeButton;
    QComboBox *saveAnimationSceneViewsComboBox;
    MSceneViewGLWidget *saveAnimationSceneView;
    bool overwriteAnimationImageSequence;

    QString syncID;

    /**
     * All pending synchronization events.
     */
    QQueue<MSyncEvent> pendingEvents;

    /**
     * The sync event that is currently being processed.
     * If no sync event is being processed, this is invalid.
     */
    MSyncEvent currentSyncEvent;

    // Whether the initial guess (on loading the first data source) what type of
    // synchronization to use has already been made. This is done when loading
    // a data set for the first time.
    bool initialVTITGuessMade = false;

    /**
      @brief frameNumber Current rendered frame. Used to index and sort frames rendered during time animation.
     */
    int frameNumber;

    // Whether the time animation is running.
    bool animationIsRunning = false;

    /**
      @brief animatorAdvanceTime Whether or not the camera animation controller should advance time this frame.
     */
    bool animatorAdvanceTime;
    bool synchronizationInProgress;
    /**
      Handle sync event if valid and init time should be updated, but only
      valid time changes due to restriction to the data set.
     */
    bool validDateTimeHasChanged;
    QWidget *lastFocusWidget;
    MSynchronizationType currentSyncType;
    QList<MSynchronizedObject*> synchronizedObjects;
    QSet<MSynchronizedObject*> pendingSynchronizations;

    // Properties to control configuration.
    QMenu *configurationDropdownMenu;
    QAction *selectDataSourcesAction;
    QList<QDateTime> availableInitDateTimes;
    QList<QDateTime> availableValidDateTimes;
    QSet<unsigned int> availableEnsembleMembers;
    QList<QAction*> selectedDataSourceActionList;
    QStringList selectedDataSources;
    QAction *loadConfigurationAction;
    QAction *saveConfigurationAction;

    /**
     * The type of the integration used when advancing time.
     */
    MSynchronizationType integrationType;

    /**
     * The current time step delta.
     */
    int deltaTimeStep;

    /**
     * The current time step unit.
     */
    int timeStepUnit;

    /**
     * The current valid time.
     */
    QDateTime validTime;

    /**
     * The current init time.
     */
    QDateTime initTime;

    /**
     * The selected ensemble member.
     */
    int selectedEnsMember;

    /**
     * If ensemble mean mode is active.
     */
    bool showEnsembleMean;

#ifdef ENABLE_MET3D_STOPWATCH
    MStopwatch stopwatch;
#endif

public: // Undo commands for various actions performed by the user.

    /**
     * Undo command for advancing time forward / backward.
     */
    class AdvanceTimeCommand : public QUndoCommand
    {
    public:
        AdvanceTimeCommand(MSyncControl *sync, bool backwards);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * Whether the time advances forward or backward on @c redo()
         */
        bool backward;

        /**
         * The init time of the sync control to undo to.
         */
        QDateTime originalInitTime;

        /**
         * The valid time of the sync control to undo to.
         */
        QDateTime originalValidTime;
    };


    /**
     * An undo command for changing the time step.
     */
    class ChangeDeltaTimeCommand : public QUndoCommand
    {
    public:
        ChangeDeltaTimeCommand(MSyncControl *sync, int deltaTime);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * The delta time to redo.
         */
        int deltaTime;

        /**
         * The delta time to undo.
         */
        int oldDeltaTime;
    };


    /**
     * An undo command for changing the time step unit.
     */
    class ChangeDeltaTimeUnitCommand : public QUndoCommand
    {
    public:
        ChangeDeltaTimeUnitCommand(MSyncControl *sync, int unit);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * The new time step unit index.
         */
        int unit;

        /**
         * The old time step unit index.
         */
        int oldUnit;
    };


    /**
     * An undo command for changing the time integration type.
     */
    class ChangeIntegrationTypeCommand : public QUndoCommand
    {
    public:
        ChangeIntegrationTypeCommand(MSyncControl *sync, MSynchronizationType type);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * The new time integration type.
         */
        MSynchronizationType type;

        /**
         * The old time integration type.
         */
        MSynchronizationType oldType;
    };


    /**
     * An undo command for changing the init or valid time.
     */
    class SetInitValidTimeCommand : public QUndoCommand
    {
    public:
        SetInitValidTimeCommand(MSyncControl *sync, QDateTime dateTime, bool isValidTime);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * The old date-time.
         */
        QDateTime oldTime;

        /**
         * The new date-time.
         */
        QDateTime newTime;

        /**
         * Whether the valid time was changed instead of the init time.
         */
        bool setValidTime;
    };


    /**
     * An undo command for changing the selected ensemble member.
     */
    class SetEnsembleMemberCommand : public QUndoCommand
    {
    public:
        SetEnsembleMemberCommand(MSyncControl *sync, int ensMember);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * The previously selected ensemble member.
         */
        int oldEnsMember;

        /**
         * The newly selected ensemble member.
         */
        int newEnsMember;
    };


    /**
     * An undo command for toggling the ensemble mean mode.
     */
    class ToggleEnsembleMeanModeCommand : public QUndoCommand
    {
    public:
        ToggleEnsembleMeanModeCommand(MSyncControl *sync, bool enable);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * Whether to enable the mean ensemble mode or not.
         */
        bool enable;
    };

    /**
     * An undo command when the user selects data sources.
     */
    class SelectDataSourcesCommand : public QUndoCommand
    {
    public:
        SelectDataSourcesCommand(MSyncControl *sync, QStringList dataSources);

        void undo() override;
        void redo() override;

    private:
        /**
         * The sync control that created the command.
         */
        MSyncControl *sync;

        /**
         * The data sources selected previously.
         */
        QStringList oldDataSources;

        /**
         * The newly selected data sources.
         */
        QStringList newDataSources;
    };
};


/**

 */
class MSynchronizedObject
{
public:
    MSynchronizedObject() { synchronizationControl = nullptr; }

    /**
       Handles synchronization event. The type of the synchronization event is
       given by @p syncType while @p data stores the data for updating.
       @p data is implemented as a vector to handle the simultanious
       synchronization event of init(index 0) and valid(index 1) time.
     */
    virtual bool synchronizationEvent(
            MSynchronizationType syncType, QVector<QVariant> data) = 0;

    virtual void synchronizeWith(
            MSyncControl *sync, bool updateGUIProperties=true) = 0;

    MSyncControl *getSynchronizationControl() { return synchronizationControl; }
    void setSynchronizationControl(MSyncControl *synchronizationControl)
    { this->synchronizationControl = synchronizationControl; }

protected:
    /* Synchronization (Pointer to the MSyncControl with which time/ensemble is
       synchronised and corresponding property). */
    MSyncControl *synchronizationControl;
};

} // namespace Met3D

// Declare the sync event a meta type to be able to be passed along signal / slots.
Q_DECLARE_METATYPE(Met3D::MSyncEvent)

#endif // SYNCCONTROL_H
