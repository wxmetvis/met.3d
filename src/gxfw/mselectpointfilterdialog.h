/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MSELECTPOINTFILTERDIALOG_H
#define MET_3D_MSELECTPOINTFILTERDIALOG_H

// standard library imports

// related third party imports
#include <QDialog>

// local application imports

namespace Ui {
class MSelectPointFilterDialog;
}

namespace Met3D
{
/**
 * Dialog class where the user can select a point filter. Point filters are
 * registered in the @c MPointFilterFactory.
 */
class MSelectPointFilterDialog : public QDialog
{
Q_OBJECT

public:
    /**
     * Creates a new dialog and fills the table in the dialog with available
     * point filters.
     */
    explicit MSelectPointFilterDialog(QWidget *parent = nullptr);

    ~MSelectPointFilterDialog() override;

    /**
     * @return The currently selected filter name, or an empty string if no
     * selection.
     */
    QString getSelectedFilterName();

private:
    /**
     * Creates the table entries.
     */
    void createFilterEntries();

    Ui::MSelectPointFilterDialog *ui;
};

} // Met3D


#endif //MET_3D_MSELECTPOINTFILTERDIALOG_H
