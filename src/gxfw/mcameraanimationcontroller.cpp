/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022-2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mcameraanimationcontroller.h"

// standard library imports

// related third party imports
#include <QVector3D>
#include <cmath>

// local application imports
#include "gxfw/msceneviewglwidget.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MCameraAnimationController::MCameraAnimationController()
        : animationState(nullptr),
          isActive(false),
          isAnimationFinished(false)
{
}


MCameraAnimationController::~MCameraAnimationController()
= default;


void MCameraAnimationController::initAnimation(MSceneViewGLWidget *view,
                                               MCameraSequence *sequence)
{
    // Delete old animation state.
    if (animationState)
    {
        // Delete old copy of animation sequence.
        delete animationState->sequence;
    }
    delete animationState;

    // Create a copy of the animation sequence to avoid it being edited.
    auto *copy = new MCameraSequence(*sequence);

    auto *state = new AnimationState(view, copy);
    state->currentDistance = 0;
    state->currentKey = 0;

    isActive = true;
    isAnimationFinished = false;

    animationState = state;

    updateTotalPathLength();
}


bool MCameraAnimationController::advanceCurrentAnimation()
{
    bool advanceTime = false; // Change to true if we moved over a key that
    // advances time animation.

    auto state = animationState;
    MCameraSequence *s = state->sequence;

    if (s->keys.isEmpty()) // If we don't have any keys, don't animate
    {
        advanceTime = false;
        isAnimationFinished = true;
        return advanceTime;
    }

    int key1Index = state->currentKey;
    // If our current key is out of bounds, use the last key. Only happens when
    // editing sequence while it animates.
    if (key1Index >= s->keys.length())
        key1Index = s->keys.length() - 1;
    if (key1Index < 0)
        key1Index = 0;

    MSequenceKey *key1 = s->keys[key1Index];

    // Get the rotation and location for the camera at key1, used later.
    QVector3D camPos = key1->position;
    QVector3D camRot = key1->rotation;

    if (s->keys.length() > 1) // Only advance along path if it even exists.
    {
        int key2Index;
        MSequenceKey *key2;

        // Calculate delta as distance the camera moved in deltaTime time.
        double totalPathLength = state->spline.length();
        long runTime = static_cast<long>(s->sequenceRuntime) * 1000;
        double delta = totalPathLength / static_cast<double>(runTime) * s->sequenceFrameTime;
        double finalDst = state->currentDistance + delta;

        if (finalDst >= totalPathLength)
        {
            if (s->loopSequence)
            {
                finalDst = finalDst - totalPathLength;
            }
            else
            {
                isAnimationFinished = true;
                finalDst = totalPathLength;
            }
        }

        key1Index = state->spline.findSegment(finalDst);
        key2Index = key1Index + 1;

        key1 = s->keys[key1Index];

        if (key2Index >= s->keys.length())
        {
            if (s->loopSequence)
            {
                key2Index = 0;
                key2 = s->keys[key2Index];
            }
            else
            {
                key2Index = s->keys.length() - 1;
                key2 = s->keys[key2Index];
            }
        }
        else
        {
            key2 = s->keys[key2Index];
        }

        // We move over to key1, check if we should advance time.
        if (state->currentKey != key1Index)
        {
            advanceTime |= key1->advanceTimestep;
        }

        state->currentKey = key1Index;
        state->currentDistance = finalDst;

        // Pair found, now update camera position and rotation
        // Teleport transition, just set position and rotation to key2 position
        // and rotation.
        if (key1->transition == MCameraTransition::TELEPORT)
        {
            camPos = key2->position;
            camRot = key2->rotation;

            state->currentKey = key2Index;
            state->currentDistance = state->spline.segmentStart(key2Index);

            advanceTime |= key1->advanceTimestep;
        }
        else if (key1 == key2) // Same key, no movement.
        {
            camPos = key1->position;
            camRot = key1->rotation;
        }
        else // Linear interpolate between key1 and key2.
        {
            // Total distance between both keys.
            int segment = key1Index;
            double segmentDistance = state->spline.segmentStart(segment);
            double segmentLength = state->spline.segmentLength(segment);
            double distanceInSegment = state->currentDistance - segmentDistance;

            camPos = state->spline.interpolate(static_cast<float>(finalDst));

            // TODO (tv, 15Aug2023): Implement splines for rotations too.
            //  Currently, rotations are still interpolated linearly.
            //  Though in order for rotation splines, we would need a good quaternion implementation.
            camRot = lerpRotations(key1->rotation, key2->rotation,
                                   (float) (distanceInSegment / segmentLength));
        }
    }
    else
    {
        isAnimationFinished = true;
    }
    MCamera *cam = state->view->getCamera();

    cam->setOrigin(camPos);
    cam->setRotation(camRot.x(), camRot.y(), camRot.z());

    state->view->updateCamera();

    return advanceTime;
}


void MCameraAnimationController::stopAnimation()
{
    isActive = false;

    if (animationState == nullptr)
        return;

    delete animationState;

    animationState = nullptr;
}


void MCameraAnimationController::updateTotalPathLength() const
{
    MCameraSequence *s = animationState->sequence;

    for (auto key : s->keys)
    {
        animationState->spline.add(key->position);
    }
    animationState->spline.setClosed(s->loopSequence);

    animationState->spline.setTension(s->sequenceSplineTension);

    animationState->spline.update();
}


QVector3D MCameraAnimationController::lerpRotations(const QVector3D& rot1,
                                                    const QVector3D& rot2,
                                                    const float alpha)
{
    QVector3D delta = rot2 - rot1;

    if (std::fabs(delta.x()) > 180.0f)
    {
        if (delta.x() < 0)
        {
            delta.setX(360.0f + delta.x());
        }
        else
        {
            delta.setX(-360.0f + delta.x());
        }
    }

    if (std::fabs(delta.y()) > 180.0f)
    {
        if (delta.y() < 0)
        {
            delta.setY(360.0f + delta.y());
        }
        else
        {
            delta.setY(-360.0f + delta.y());
        }
    }

    if (std::fabs(delta.z()) > 180.0f)
    {
        if (delta.z() < 0)
        {
            delta.setZ(360.0f + delta.z());
        }
        else
        {
            delta.setZ(-360.0f + delta.z());
        }
    }

    QVector3D res = rot1 + delta * alpha;

    return res;
}

} // namespace Met3D
