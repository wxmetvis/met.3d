/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mfileutils.h"

// standard library imports
#include <utility>

// related third party imports
#include <QFileDialog>
#include <QObject>

// local application imports
#include "util/mutil.h"

namespace Met3D
{
MFileType::MFileType()
= default;


MFileType::MFileType(QString fileTypeName, QStringList extensions)
        : fileTypeName(std::move(fileTypeName)), fileExtensions(std::move(extensions))
{
}


MFileType::MFileType(QString fileTypeName, const QString &extension)
        : fileTypeName(std::move(fileTypeName))
{
    fileExtensions.append(extension);
}


MFileType::MFileType(const QMimeType &mimeType)
{
    fileTypeName = mimeType.comment();
    fileExtensions = mimeType.suffixes();
}


QString MFileType::getFilterString() const
{
    if (!isValid()) return "";

    QString filter = fileTypeName + " (";

    for (const QString &extension : fileExtensions)
    {
        if (extension.startsWith('.'))
        {
            filter += '*' + extension;
        }
        else if (extension.startsWith('*'))
        {
            filter += extension;
        }
        else
        {
            filter += "*." + extension;
        }

        filter += ' ';
    }

    if (filter.endsWith(' '))
        filter.remove(filter.length() - 1, 1);

    filter += ')';

    return filter;
}


QString MFileType::getDefaultExtension() const
{
    if (!isValid()) return "";

    QString defaultSuffix = fileExtensions.first();

    if (defaultSuffix.startsWith('*'))
    {
        defaultSuffix.remove(0, 1);
    }

    if (!defaultSuffix.startsWith('.'))
    {
        defaultSuffix.insert(0, '.');
    }

    return defaultSuffix;
}


bool MFileType::isValid() const
{
    return (!fileExtensions.isEmpty()) && (!fileTypeName.isEmpty());
}


bool MFileType::isType(const QFile &file) const
{
    QString name = file.fileName();

    return isType(name);
}


bool MFileType::isType(const QString &file) const
{
    for (QString extension : fileExtensions)
    {
        // Wildcard case.
        if (extension == "*") return true;

        // Wildcard with extension.
        if (extension.startsWith('*'))
        {
            extension.remove(0, 1);
        }

        // Extension without preceding dot.
        if (!extension.startsWith('.'))
        {
            extension.insert(0, '.');
        }

        if (file.endsWith(extension, Qt::CaseInsensitive)) return true;
    }

    return false;
}


const MFileType &MFileType::getWildcard()
{
    static const MFileType wildCard = {"Any File", "*"};
    return wildCard;
}


MTempConfigFile::MTempConfigFile()
{
    tmpFile.open();
    settings = new QSettings(tmpFile.fileName(), QSettings::InvalidFormat);

    // Create settings header, otherwise the copying would create an error for all actors with bounding boxes.
    settings->beginGroup("FileFormat");
    // Save version id of Met.3D.
    settings->setValue("met3dVersion", met3dVersionString);
    settings->endGroup();
}


MTempConfigFile::~MTempConfigFile()
{
    delete settings;
    tmpFile.close();
}


QString MFileUtils::getSaveFileName(QWidget *parent, const QString &title, const MFileType &fileFilter,
                                    const QString &directory, const QString &defaultFileName)
{
    return getSaveFileName(parent, title, QVector<MFileType>{fileFilter}, directory, defaultFileName);
}


QString MFileUtils::getSaveFileName(QWidget *parent, const QString &title, const QVector<MFileType> &fileFilters,
                                    const QString &directory, const QString &defaultFileName)
{
    QStringList filters;
    QString defaultSuffix;

    for (const MFileType &fileType : fileFilters)
    {
        if (!fileType.isValid()) continue;
        filters.append(fileType.getFilterString());

        if (defaultSuffix.isEmpty())
        {
            defaultSuffix = fileType.getDefaultExtension();
        }
    }

    if (fileFilters.isEmpty())
    {
        filters.append(MFileType::getWildcard().getFilterString());
        defaultSuffix = "";
    }

    QFileDialog dialog(parent);
    dialog.setFileMode(QFileDialog::AnyFile);
    dialog.setAcceptMode(QFileDialog::AcceptSave);
    dialog.setNameFilters(filters);
    dialog.setWindowTitle(title);
    dialog.setDirectory(directory);
    dialog.setDefaultSuffix(defaultSuffix);
#ifndef USE_NATIVE_DIALOG
    dialog.setOption(QFileDialog::DontUseNativeDialog);
#endif

    // Update default suffix to selected filter suffix using lambda connection to the signal filterSelected.
    QObject::connect(&dialog, &QFileDialog::filterSelected, [&](const QString &filter)
    {
        int filterIdx = filters.indexOf(filter);
        const MFileType &selectedFilter = fileFilters[filterIdx];
        defaultSuffix = selectedFilter.getDefaultExtension();

        dialog.setDefaultSuffix(defaultSuffix);
    });

    if (!defaultFileName.isEmpty())
    {
        dialog.selectFile(defaultFileName + defaultSuffix);
    }

    if (dialog.exec())
    {
        QStringList files = dialog.selectedFiles();

        if (files.empty()) return "";

        QString name = files.first();

        // Check if filename is valid.
        for (const MFileType &type : fileFilters)
        {
            if (type.isType(name))
            {
                return name;
            }
        }

        // If not valid, append default suffix making it valid.
        return name + defaultSuffix;
    }

    return "";
}


QString MFileUtils::getOpenFileName(QWidget *parent, const QString &title, const MFileType &fileFilter,
                                    const QString &directory)
{
    return getOpenFileName(parent, title, QVector<MFileType>{fileFilter}, directory);
}


QString MFileUtils::getOpenFileName(QWidget *parent, const QString &title, const QVector<MFileType> &fileFilters,
                                    const QString &directory)
{
    QStringList filters;

    for (const MFileType &fileType : fileFilters)
    {
        if (!fileType.isValid()) continue;
        filters.append(fileType.getFilterString());
    }

    if (fileFilters.isEmpty())
    {
        filters.append(MFileType::getWildcard().getFilterString());
    }

    QFileDialog dialog(parent);
    dialog.setFileMode(QFileDialog::ExistingFile);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setNameFilters(filters);
    dialog.setWindowTitle(title);
    dialog.setDirectory(directory);
#ifndef USE_NATIVE_DIALOG
    dialog.setOption(QFileDialog::DontUseNativeDialog);
#endif
    if (dialog.exec())
    {
        QStringList files = dialog.selectedFiles();

        if (files.empty()) return "";

        return files.first();
    }

    return "";
}


QString MFileUtils::getOpenDirectory(QWidget *parent, const QString &title, const QString &directory)
{
    QFileDialog dialog(parent);
    dialog.setFileMode(QFileDialog::DirectoryOnly);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setWindowTitle(title);
    dialog.setDirectory(directory);
    dialog.setOption(QFileDialog::DontResolveSymlinks);
    dialog.setOption(QFileDialog::ShowDirsOnly);
#ifndef USE_NATIVE_DIALOG
    dialog.setOption(QFileDialog::DontUseNativeDialog);
#endif
    if (dialog.exec())
    {
        QStringList files = dialog.selectedFiles();

        if (files.empty()) return "";

        return files.first();
    }

    return "";
}


QStringList MFileUtils::getOpenFileNames(QWidget *parent, const QString &title, const MFileType &fileFilter,
                                         const QString &directory)
{
    return getOpenFileNames(parent, title, QVector<MFileType>{fileFilter}, directory);
}


QStringList MFileUtils::getOpenFileNames(QWidget *parent, const QString &title, const QVector<MFileType> &fileFilters,
                                         const QString &directory)
{
    QStringList filters;

    for (const MFileType &fileType : fileFilters)
    {
        if (!fileType.isValid()) continue;
        filters.append(fileType.getFilterString());
    }

    if (fileFilters.isEmpty())
    {
        filters.append(MFileType::getWildcard().getFilterString());
    }

    QFileDialog dialog(parent);
    dialog.setFileMode(QFileDialog::ExistingFiles);
    dialog.setAcceptMode(QFileDialog::AcceptOpen);
    dialog.setNameFilters(filters);
    dialog.setWindowTitle(title);
    dialog.setDirectory(directory);
#ifndef USE_NATIVE_DIALOG
    dialog.setOption(QFileDialog::DontUseNativeDialog);
#endif
    if (dialog.exec())
    {
        return dialog.selectedFiles();
    }

    return {};
}
} // Met3D