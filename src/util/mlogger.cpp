/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mlogger.h"

// standard library imports
#include <iostream>
#include <utility>

// related third party imports

// local application imports
#include "gxfw/msystemcontrol.h"

namespace Met3D
{

// Definition of static member.
MLogger MLogger::Instance;


MLogger::MLogMessage::MLogMessage(log4cplus::LogLevel level, std::string msg)
        : level(level),
          msg(std::move(msg))
{
}


MLogger::MLogMessage *MLogger::getLastLogMessage()
{
    return logMessageHistory.top();
}


QVector<MLogger::MLogMessage *>::iterator MLogger::begin() noexcept
{
    return logMessageHistory.begin();
}


QVector<MLogger::MLogMessage *>::const_iterator MLogger::begin() const noexcept
{
    return logMessageHistory.begin();
}


QVector<MLogger::MLogMessage *>::iterator MLogger::end() noexcept
{
    return logMessageHistory.end();
}


QVector<MLogger::MLogMessage *>::const_iterator MLogger::end() const noexcept
{
    return logMessageHistory.end();
}


MLogger::~MLogger()
{
    clear();
}


void MLogger::log(log4cplus::LogLevel level, const std::string &message)
{
    auto *msg = new MLogMessage(level, message);
    logMessageHistory.push(msg);

    emit onLogMessageReceived(msg);
}


void MLogger::clear()
{
    for (MLogMessage *message : logMessageHistory)
    {
        delete message;
    }
    logMessageHistory.clear();
}

}