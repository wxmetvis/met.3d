/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Luka Elwart [*]
**  Copyright 2023 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef INTERSECTIONUTILS_H
#define INTERSECTIONUTILS_H

// related third party imports
#include <QVector3D>
#include <QVector4D>
//https://iquilezles.org/www/articles/intersectors/intersectors.htm
namespace Met3D
{
/**
  @brief dot2 Returns the dot product of @p vector with itself.
  @param vector The vector to calculate the dot product for.
  @return The dot product of @p vector with itself.
 */
float dot2(const QVector3D& vector);

/**
  @brief rayCylinderIntersection Calculates the intersection distance from ray origin @p ro to the intersection point with the cylinder.
  @param ro Ray origin
  @param rd Ray direction
  @param pa Cylinder start position
  @param pb Cylinder end position
  @param ra Cylinder radius
  @return distance (>= 0) along the ray where cylinder is hit, -1 if no hit was detected
 */
float rayCylinderIntersection(QVector3D ro, QVector3D rd, QVector3D pa, QVector3D pb, float ra);

/**
  @brief rayConeIntersection Calculates the intersection distance from ray origin @p ro to the intersection point with the cone.
  @param ro Ray origin
  @param rd Ray direction
  @param pa Center position of cone base
  @param pb Center position of cone tip
  @param ra Radius of cone base
  @param rb Radius of cone tip
  @return distance (>= 0) along the ray where cone is hit, -1 if no hit was detected
 */
float rayConeIntersection(QVector3D  ro, QVector3D rd, QVector3D  pa, QVector3D  pb, float ra, float rb);

/**
  @brief rayPlaneIntersection Calculates the intersection distance from ray origin @p ro to the intersection point with the plane.
  @param ro Ray origin
  @param rd Ray direction
  @param p Plane definition. p.xyz is the plane normal that intersects the point p.xyz * p.w
  @return distance (>= 0) along the ray where plane is hit
 */
float rayPlaneIntersection(QVector3D ro, QVector3D rd, QVector4D p);

/**
 * @brief plaIntersectBounded Calculates the intersection distance from ray origin @p ro to the intersection point with the plane defined around @p center with size @p size.
 * @param ro Ray origin
 * @param rd Ray direction
 * @param p Plane definition. p.xyz is the plane normal that intersects the point p.xyz * p.w
 * @param center Plane center
 * @param size Plane size around center as a quad.´
 * @return distance (>= 0) along the ray where plane is hit, -1 if no hit was detected
 */
float plaIntersectBounded(const QVector3D& ro, const QVector3D& rd, const QVector4D& p, const QVector3D& center, const float& size);

/**
 * @brief circlePlaneIntersection Calculates the intersection distance from ray origin @ref ro to the intersection point
 * with a circle defined in plane @p p around @p c with inner radius @p r1 and outer radius @p r2
 * @param ro Ray origin
 * @param rd Ray direction
 * @param p Plane definition. p.xyz is the plane normal that intersects the point p.xyz * p.w
 * @param c Circle center
 * @param r1 Inner radius
 * @param r2 Outer radius
 * @return distance (>= 0) along the ray where circle is hit, -1 if no hit was detected
 */
float circlePlaneIntersection(const QVector3D& ro, const QVector3D& rd, const QVector4D& p, const QVector3D& c, const float& r1, const float r2);

/**
 * @brief partCirclePlaneIntersection Calculates the intersection distance from ray origin @p ro to the intersection point
 * with a part circle defined in plane @p p around @p c with inner radius @p r1 and outer radius @p r2 and angular constraint @p a1 and @p a2
 * @param ro Ray origin
 * @param rd Ray direction
 * @param p Plane definition. p.xyz is the plane normal that intersects the point p.xyz * p.w
 * @param pUp The up direction within the plane, in world space, used as reference point for the angular constraints.
 * @param c Circle center in world space
 * @param r1 Inner radius
 * @param r2 Outer radius
 * @param a1 Lower angular constraint of the part circle in degrees. 0° corresponds to the @p pUp direction.
 * Should be in range [0, 360], counter-clockwise from @p pUp when viewed in plane normal direction.
 * @param a2 Upper angular constraint of the part circle in degrees. 180° corresponds to the negative @p direction.
 * Should be in range [0, 360], counter-clockwise from @p pUp when viewed in plane normal direction.
 * @return distance (>= 0) along the ray where part circle is hit, -1 if no hit was detected
 */
float partCirclePlaneIntersection(const QVector3D &ro, const QVector3D &rd,
                                  const QVector4D &p, const QVector3D &pUp, const QVector3D &c,
                                  const float &r1, float r2,
                                  const float &a1, float a2);

}

#endif // INTERSECTIONUTILS_H
