/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mgputimer.h"

// standard library imports

// related third party imports
#include "log4cplus/loggingmacros.h"

// local application imports
#include "mutil.h"

namespace Met3D
{
MGpuTimer::MGpuTimer()
        : elapsedTime(0),
        queryID(-1),
        running(false)
{
    glGenQueries(1, &queryID); CHECK_GL_ERROR;
}


MGpuTimer::~MGpuTimer()
{
    glDeleteQueries(1, &queryID); CHECK_GL_ERROR;
}


void MGpuTimer::start()
{
    glBeginQuery(GL_TIME_ELAPSED, queryID); CHECK_GL_ERROR;
    running = true;
}


void MGpuTimer::stop()
{
    if (running)
    {
        glEndQuery(GL_TIME_ELAPSED); CHECK_GL_ERROR;

        glGetQueryObjectui64v(queryID, GL_QUERY_RESULT, &elapsedTime); CHECK_GL_ERROR;

        running = false;
    }
    else
    {
        LOG4CPLUS_DEBUG(mlog, "GPU Timer stopped but was never started. "
                              "Please check your code, that you actually started the timer.");
    }
}


double MGpuTimer::getElapsedTime(MStopwatch::TimeUnits units) const
{
    switch (units)
    {
    case MStopwatch::TimeUnits::MICROSECONDS:
        return double(elapsedTime) / 1.e3;
    case MStopwatch::TimeUnits::MILLISECONDS:
        return double(elapsedTime) / 1.e6;
    case MStopwatch::TimeUnits::SECONDS:
        return double(elapsedTime) / 1.e9;
    }
    return -1;
}

} // Met3D