/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MLOGGERAPPENDER_H
#define MET_3D_MLOGGERAPPENDER_H

// standard library imports

// related third party imports
#include "log4cplus/appender.h"
#include "log4cplus/layout.h"

// local application imports
#include "mlogger.h"

namespace log4cplus
{

using namespace Met3D;

/**
 * This is a log message appender for log4cplus,
 * that sends the log messages to the @c MLogger object,
 * that saves them in a history and provides them for the
 * @c MLogOutputWidget.
 */
class MLoggerAppender : public Appender
{
public:
    explicit MLoggerAppender(const helpers::Properties &properties);
    ~MLoggerAppender() override;

    void close() override;

    /**
     * Register this appender to the log4cplus system.
     */
    static void registerAppender();

protected:
    void append(const spi::InternalLoggingEvent &event) override;
};

} // Met3D

#endif //MET_3D_MLOGGERAPPENDER_H
