/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mloggerappender.h"

// standard library imports

// related third party imports
#include "log4cplus/spi/loggingevent.h"
#include "log4cplus/helpers/property.h"
#include "log4cplus/spi/factory.h"


// local application imports

namespace log4cplus
{

MLoggerAppender::MLoggerAppender(const helpers::Properties &properties)
        : Appender(properties)
{
}

MLoggerAppender::~MLoggerAppender()
{
    destructorImpl();
}


void MLoggerAppender::close()
{
}


void MLoggerAppender::append(const spi::InternalLoggingEvent &event)
{
    std::stringstream stream;
    layout->formatAndAppend(stream, event);
    std::string message = stream.str();

    MLogger::Instance.log(event.getLogLevel(), message);
}


void MLoggerAppender::registerAppender()
{
    spi::AppenderFactoryRegistry &reg = spi::getAppenderFactoryRegistry();
    LOG4CPLUS_REG_APPENDER(reg, MLoggerAppender);
}


} // Met3D