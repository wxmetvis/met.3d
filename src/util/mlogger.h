/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MLOGGER_H
#define MET_3D_MLOGGER_H

// standard library imports
#include <string>
#include <sstream>

// related third party imports
#include <QStack>
#include <QObject>
#include <log4cplus/loglevel.h>

// local application imports

namespace Met3D
{

/**
 * The MLogger class contains functionality to save log messages from the @c MLoggerAppender class,
 * and provide them for other classes in Met.3D to use, such as  the @c MLogOutputWidget.
 * It should never be used directly to log messages, only to read the log history and
 * receive notification, when log messages appear.
 * Instead of using this class directly, use the dedicated logging macros of log4cplus.
 * This class itself is iterable. The @c begin() and @c end() functions return iterators to the underlying
 * log message history. Therefore, when iterating this class, you iterate the log message history.
 */
class MLogger : public QObject
{
Q_OBJECT

public:

    /**
     * Struct specifying the contents of a single log message for log history.
     */
    struct MLogMessage
    {
        const log4cplus::LogLevel level;
        const std::string msg;

        MLogMessage(log4cplus::LogLevel level, std::string msg);
    };

    /**
     * Clears the log message history.
     */
    void clear();

    /**
     * Retrieves the latest log message.
     * @return A pointer to the latest log message.
     */
    MLogMessage *getLastLogMessage();

    QVector<MLogger::MLogMessage *>::iterator begin() noexcept;

    QVector<MLogger::MLogMessage *>::const_iterator begin() const noexcept;

    QVector<MLogger::MLogMessage *>::iterator end() noexcept;

    QVector<MLogger::MLogMessage *>::const_iterator end() const noexcept;

    /**
     * Print a log message of given @p level.
     * Dont use it to print your log messages. Rather use the log4cplus macros.
     * @param level Log message level.
     * @param message Log message.
     */
    void log(log4cplus::LogLevel level, const std::string &message);

    /**
     * Only one logger should exist in the entire program.
     * It also needs to be ready instantly.
     */
    static MLogger Instance;

signals:

    /**
     * Signal is emitted, when a log message or error message was received.
     */
    void onLogMessageReceived(MLogMessage *message);

private:
    MLogger()
    {};

    ~MLogger() override;

    /**
     * A history of all log messages. It contains all log messages that were logged to
     * this point from the last time the history was cleared or program start.
     */
    QStack<MLogMessage *> logMessageHistory;
};

}
#endif //MET_3D_MLOGGER_H