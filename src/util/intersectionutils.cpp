/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Luka Elwart [*]
**  Copyright 2023 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "intersectionutils.h"

// standard library imports
#include <cmath>
#include <limits>

namespace Met3D
{

/**
 * COPYRIGHT NOTICE
 * Part of the following code was taken from an article by Iniqo Quilez, last accessed in January 2024.
 * The article can be found here: https://iquilezles.org/www/articles/intersectors/intersectors.htm
 * The following functions contain code taken or were adapted from the article:
 * @ref rayCylinderIntersection()
 * @ref rayConeIntersection()
 * @ref rayPlaneIntersection()
 * @ref plaIntersectBounded()
 * @ref circlePlaneIntersection()
 */


float dot2(const QVector3D &vector)
{
    return QVector3D::dotProduct(vector, vector);
}

float rayCylinderIntersection(QVector3D ro, QVector3D rd, QVector3D pa,
                              QVector3D pb, float ra)
{
    QVector3D ca = pb - pa;
    QVector3D oc = ro - pa;
    float caca = QVector3D::dotProduct(ca, ca);
    float card = QVector3D::dotProduct(ca, rd);
    float caoc = QVector3D::dotProduct(ca, oc);
    float a = caca - card * card;
    float b = caca * QVector3D::dotProduct(oc, rd) - caoc * card;
    float c =
        caca * QVector3D::dotProduct(oc, oc) - caoc * caoc - ra * ra * caca;
    float h = b * b - a * c;
    if (h < 0.0) return -1.f; //no intersection
    h = std::sqrt(h);
    float t = (-b - h) / a;
    // body
    float y = caoc + t * card;
    if (y > 0.0 && y < caca) return t;
    // caps
    t = (((y < 0.0f) ? 0.0f : caca) - caoc) / card;
    if (abs(b + a * t) < h) return t;
    return -1.f; //no intersection
}

float rayConeIntersection(QVector3D ro, QVector3D rd, QVector3D pa,
                          QVector3D pb, float ra, float rb)
{
    QVector3D ba = pb - pa;
    QVector3D oa = ro - pa;
    QVector3D ob = ro - pb;
    float m0 = QVector3D::dotProduct(ba, ba);
    float m1 = QVector3D::dotProduct(oa, ba);
    float m2 = QVector3D::dotProduct(rd, ba);
    float m3 = QVector3D::dotProduct(rd, oa);
    float m5 = QVector3D::dotProduct(oa, oa);
    float m9 = QVector3D::dotProduct(ob, ba);

    // caps
    if (m1 < 0.0)
    {
        if (dot2(oa * m2 - rd * m1) < (ra * ra * m2 * m2)) // delayed division
            return -m1 / m2;
    }
    else if (m9 > 0.0)
    {
        float t = -m9 / m2; // NOT delayed division
        if (dot2(ob + rd * t) < (rb * rb)) return t;
    }

    // body
    float rr = ra - rb;
    float hy = m0 + rr * rr;
    float k2 = m0 * m0 - m2 * m2 * hy;
    float k1 = m0 * m0 * m3 - m1 * m2 * hy + m0 * ra * (rr * m2 * 1.0f);
    float k0 =
        m0 * m0 * m5 - m1 * m1 * hy + m0 * ra * (rr * m1 * 2.0f - m0 * ra);
    float h = k1 * k1 - k2 * k0;
    if (h < 0.0) return -1.f; //no intersection
    float t = (-k1 - std::sqrt(h)) / k2;
    float y = m1 + t * m2;
    if (y < 0.0 || y > m0) return -1.f; //no intersection
    return t;
}

float rayPlaneIntersection(QVector3D ro, QVector3D rd, QVector4D p)
{
    QVector3D pxyz = QVector3D(p.x(), p.y(), p.z());
    float divisor = QVector3D::dotProduct(rd, pxyz);
    if (divisor == 0.f) return std::numeric_limits<float>::max();
    return -(QVector3D::dotProduct(ro, pxyz) - p.w()) / divisor;
}

float plaIntersectBounded(const QVector3D &ro, const QVector3D &rd,
                          const QVector4D &p, const QVector3D &center,
                          const float &size)
{
    float d = rayPlaneIntersection(ro, rd, p);

    // hit position relative to gizmo location
    QVector3D relP = ro + rd * d - center;

    // 0 out normal component (axis which is irrelevant for this plane)
    relP *= QVector3D(1.f, 1.f, 1.f) - QVector3D(p.x(), p.y(), p.z());

    // is hit position inside the bounded plane?
    if (relP.x() >= 0.f && relP.y() >= 0.f && relP.z() >= 0.f
        && relP.x() <= size && relP.y() <= size && relP.z() <= size)
        return d;

    // invalid / no hit
    return -1;
}

float circlePlaneIntersection(const QVector3D &ro, const QVector3D &rd,
                              const QVector4D &p, const QVector3D &c,
                              const float &r1, const float r2)
{
    float d = rayPlaneIntersection(ro, rd, p);
    QVector3D relPos = ro + rd * d - c;
    float rl = relPos.length();
    if (rl >= r1 && rl <= (r2)) return d;
    return -1.f;
}

float partCirclePlaneIntersection(const QVector3D &ro, const QVector3D &rd,
                                  const QVector4D &p, const QVector3D &pUp, const QVector3D &c,
                                  const float &r1, const float r2,
                                  const float &a1, const float a2)
{
    float d = rayPlaneIntersection(ro, rd, p);
    QVector3D relPos = ro + rd * d - c;

    // Calculate angle between up vector and relative position.
    float dot = QVector3D::dotProduct(relPos.normalized(), pUp.normalized());
    // Determine the normal between up vector and relative position, to determine the direction of the previous dot product.
    QVector3D relNormal = QVector3D::crossProduct(relPos.normalized(), pUp.normalized());
    // Determine direction (negative or positive angle) by calculating dot product between the normal of the circle
    // and normal from the relative position. If the angle is negative, this dot product will be negative too.
    float dir = QVector3D::dotProduct(QVector3D(p.x(), p.y(), p.z()), relNormal) >= 0 ? 1 : -1;

    double angle = acos(double(dot)) / M_PI * 180.0;
    if (dir < 0) angle = -angle;
    if (angle < 0) angle += 360.0;

    if (angle < a1 || angle > a2) return -1;

    float rl = relPos.length();
    if (rl >= r1 && rl <= (r2)) return d;
    return -1.f;
}

} // namespace Met3D
