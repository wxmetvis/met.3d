/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2020 Marc Rautenhaus
**  Copyright 2024 Christoph Fischer
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef GEOMETRYHANDLING_H
#define GEOMETRYHANDLING_H

// standard library imports
#include <functional>

// related third party imports
#include <QtCore>
#include "ogrsf_frmts.h"
#include "cpl_error.h"
#include <proj.h>

// local application imports


namespace Met3D
{

/**
  @brief MGeometryHandling provides methods to create, load, and transform 2D
  geometries. Geometries are generally handled as @ref QVector<QPolygonF>.
  Methods include loading geometry from shapefiles, generation of graticule
  geometry, projection methods for rotated lon-lat and proj-supported projections,
  and clipping to bounding boxes.
 */
class MGeometryHandling
{
public:
    MGeometryHandling();
    virtual ~MGeometryHandling();

    QVector<QPolygonF> generate2DGraticuleGeometry(
            QVector<float> longitudes, QVector<float> latitudes,
            QVector2D lonLatVertexSpacing);

    QVector<QPolygonF> read2DGeometryFromShapefile(QString fname, QRectF bbox);

    void initProjProjection(QString projString);

    void destroyProjProjection();

    QPointF geographicalToProjectedCoordinates(QPointF point, bool inverse=false);

    QVector<QPolygonF> geographicalToProjectedCoordinates(
            QVector<QPolygonF> polygons, bool inverse=false);

    void initRotatedLonLatProjection(QPointF rotatedPoleLonLat);

    QPointF geographicalToRotatedCoordinates(QPointF point);

    QVector<QPolygonF> geographicalToRotatedCoordinates(
            QVector<QPolygonF> polygons);

    QPointF rotatedToGeographicalCoordinates(QPointF point);

    QVector<QPolygonF> splitLineSegmentsLongerThanThreshold(
            QVector<QPolygonF> polygons, double thresholdDistance);

    QVector<QPolygonF> enlargeGeometryToBBoxIfNecessary(
            QVector<QPolygonF> polygons, QRectF bbox);

    QVector<QPolygonF> clipPolygons(QVector<QPolygonF> polygons, QRectF bbox);

    void flattenPolygonsToVertexList(
            QVector<QPolygonF> polygons,
            QVector<QVector2D> *vertexList,
            QVector<int> *polygonStartIndices,
            QVector<int> *polygonVertexCount);

    /**
     * @brief canConnectPointPairInProjection_
     * @param p1 First point
     * @param p2 Second point
     *
     * Functions to check supported projection whether a line between p1 and p2 crosses the domain boundary. Then, the graticule should not be drawm.
     * @return whether the boundary is crossed
     */
    bool canConnectPointPairInProjectionLcc(QPointF p1, QPointF p2,
                                            float lon_0);
    bool canConnectPointPairInProjectionLatlong(QPointF p1, QPointF p2);
    bool canConnectPointPairInProjectionStereographic(QPointF p1, QPointF p2);

private:
    int cohenSutherlandCode(QPointF &point, QRectF &bbox) const;

    bool cohenSutherlandClip(QPointF *p1, QPointF *p2, QRectF &bbox);

    OGRPolygon* convertQRectToOGRPolygon(QRectF &rect);

    QPolygonF convertOGRLineStringToQPolygon(OGRLineString *lineString);

    void appendOGRLineStringsFromOGRGeometry(QList<OGRLineString*> *lineStrings,
                                             OGRGeometry *geometry);

    // active transformations
    PJ *pjSrcDstTransformation, *pjDstSrcTransformation;

    // Active callbacks to connection checking functions.
    std::function<bool(QPointF,QPointF)> srcConnectionFunc;
    std::function<bool(QPointF,QPointF)> dstConnectionFunc;

    // Scale factor when projection from the source to the destination projection.
    float srcToDstScaleFactorForProjection;

    QPointF rotatedPole;
};

} // namespace Met3D

#endif // GEOMETRYHANDLING_H
