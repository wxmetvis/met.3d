/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2019 Marc Rautenhaus [*, previously +]
**  Copyright 2015-2017 Michael Kern [+]
**  Copyright 2020-2023 Andreas Beckert [*]
**  Copyright 2024      Susanne Fuchs [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef METROUTINES_H
#define METROUTINES_H

// standard library imports

// related third party imports
#include <QVector2D>

// local application imports
#include "util/mutil.h"


namespace Met3D
{

/******************************************************************************
***                               CONSTANTS                                 ***
*******************************************************************************/

// Definition of atmospheric science constants. Reference: e.g. Wallace and
// Hobbs (2006), pp. 467ff.
namespace MetConstants
{
// Universal constants

// Air
const double GAS_CONSTANT_DRY_AIR = 287.058; // J K^-1 kg^-1
const double SPECIFIC_HEAT_DRYAIR_CONST_PRESSURE = 1004.; // J K^-1 kg^-1

// Water substance

// Earth and sun
const double GRAVITY_ACCELERATION = 9.80665; // m s^-2
const double EARTH_RADIUS_km = 6371.; // km

// Source: http://hpiers.obspm.fr/eop-pc/models/constants.html
const double EARTHS_ANGULAR_SPEED_OF_ROTATION = 7.292115E-5; // rad/s

// Length units
// Source: https://www.bipm.org/ (accessed 26 Jan 2024)
const double NAUTICAL_MILE_IN_METRES = 1852.0; // One nautical mile in metres

// Scaling factor for projections, since Met.3D has originally been designed
// such that horizontal coordinates are in the range (-180..180 and -90..90)
// (lon and lat). For projection coordinates that are given in e.g. meters,
// we need to scale to fit into that range. Example: 1.e6 will scale
// stereographic units in m to 10^3 km.
const double scaleFactorToFitProjectedCoordsTo360Range = 1.e6;

// Roughly the distance of one degree in latitudinal direction
// on the Earth's sphere, ~111.2 km.
const double deltaLatM = 1.112e5;
const double deltaLatKm = deltaLatM / 1000;
}


/******************************************************************************
***                                METHODS                                  ***
*******************************************************************************/

inline double kelvinToDegC(double temperature_K)
{ return temperature_K - 273.15; }


inline double degCToKelvin(double temperature_degC)
{ return temperature_degC + 273.15; }


inline double degreesToRadians(double angle)
{ return angle / 180. * M_PI; }


inline double radiansToDegrees(double angle)
{ return angle / M_PI * 180.; }


/**
  Computes the haversine: haversin(theta) = sin^2(theta/2.).
  @see https://en.wikipedia.org/wiki/Haversine_formula
 */
inline double haversin(double radians);


double gcDistanceUnitSphere_2(
        const double lon1_rad, const double lat1_rad,
        const double lon2_rad, const double lat2_rad);


/**
  Great circle distance between two points on a unit sphere, in radians.

  Reference: @see http://www.movable-type.co.uk/scripts/gis-faq-5.1.html

  "Presuming a spherical Earth with radius R (see below), and the locations of
  the two points in spherical coordinates (longitude and latitude) are
  lon1,lat1 and lon2,lat2 then the

  Haversine Formula (from R.W. Sinnott, "Virtues of the Haversine", Sky and
  Telescope, vol. 68, no. 2, 1984, p. 159):

        dlon = lon2 - lon1
        dlat = lat2 - lat1
        a = sin^2(dlat/2) + cos(lat1) * cos(lat2) * sin^2(dlon/2)
        c = 2 * arcsin(min(1,sqrt(a)))
        d = R * c

  will give mathematically and computationally exact results. The intermediate
  result c is the great circle distance in radians. The great circle distance d
  will be in the same units as R."

  @see gcDistance()

  @return The intermediate result c (the great circle distance in radians).
 */
double gcDistanceUnitSphere(
        const double lon1_rad, const double lat1_rad,
        const double lon2_rad, const double lat2_rad);


/**
  Great circle distance between two points on a sphere with radius @p radius,
  in the same units as @p radius.
 */
double gcDistance(const double lon1_rad, const double lat1_rad,
                         const double lon2_rad, const double lat2_rad,
                         const double radius);


/**
  Same as @ref gcDistance() but with lon/lat given in degrees.
 */
double gcDistance_deg(const double lon1, const double lat1,
                             const double lon2, const double lat2,
                             const double radius);

/**
 * Calculates the great circle azimuth angle in radians from the point with geographic
 * coordinates @p lon1_rad, @p lat1_rad to the point with geographic
 * coordinates @p lon2_rad, @p lat2_rad. Angles are in radians.
 *
 * The azimuth angle or bearing angle is the angle between two lines: one between
 * position 1 and the North Pole, and the other line between position 1 and 2.
 *
 * Reference: @see https://www.movable-type.co.uk/scripts/latlong.html, Section: Bearing
 *
 * @param lon1_rad Radial longitude of the start point.
 * @param lat1_rad Radial latitude of the start point.
 * @param lon2_rad Radial longitude of the end point.
 * @param lat2_rad Radial latitude of the end point.
 * @return great circle azimuth angle in radians.
 */
float gcAzimuth(const float lon1_rad, const float lat1_rad,
                const float lon2_rad, const float lat2_rad);

/**
 * Calculates the great circle azimuth angle from the point with geographic
 * coordinates @p lon1_deg, @p lat1_deg to the point with geographic
 * coordinates @p lon2_deg, @p lat2_deg. Angles are in degrees.
 *
 * The azimuth angle or bearing angle is the angle between two lines: one between
 * position 1 and the North Pole, and the other line between position 1 and 2.
 *
 * Reference: @see https://www.movable-type.co.uk/scripts/latlong.html, Section: Bearing
 *
 * @param lon1_rad Longitude [°] of the start point.
 * @param lat1_rad Latitude [°] of the start point.
 * @param lon2_rad Longitude [°] of the end point.
 * @param lat2_rad Latitude [°] of the end point.
 * @return great circle azimuth angle in degrees.
 */
float gcAzimuth_deg(const float lon1_deg, const float lat1_deg,
                    const float lon2_deg, const float lat2_deg);


inline double cot(double radians);


/**
  Computes the area of a spherical triangle given by three points on a sphere.

  Similar to NCL's gc_tarea() function:
  @ref http://www.ncl.ucar.edu/Document/Functions/Built-in/gc_tarea.shtml
 */
double gcTriangleArea(const double lon1, const double lat1,
                      const double lon2, const double lat2,
                      const double lon3, const double lat3,
                      const double radius);


/**
  Computes the area of of a quadrilateral patch on a sphere of radius @p
  radius.

  Similar to NCL's gc_qarea() function:
  @ref http://www.ncl.ucar.edu/Document/Functions/Built-in/gc_qarea.shtml

  The area is computed by dividing the quadrilateral into two triangles (@see
  gcTriangleArea()) and summing the areas of those triangles.
 */
double gcQuadrilateralArea(const double lon1, const double lat1,
                           const double lon2, const double lat2,
                           const double lon3, const double lat3,
                           const double lon4, const double lat4,
                           const double radius);

/**
 * @return Approximate conversion of a longitudinal distance @p s in meters at position
 * @p positionLongitude to degrees latitude.
 * @param s Distance [m] in longitude direction.
 * @param positionLatitude Latitude [°] of the position.
 */
float longitudeLengthMeterToDegree(float s, float positionLatitude);

/**
 * @return Conversion of a latitudinal distance @p s in meters to degrees latitude.
 * @param s Distance [m] in latitude direction.
 * @param positionLatitude Latitude [°] of the position.
 */
float latitudeLengthMeterToDegree(float s);

/**
 * @brief Converts a vector @p pos containing geographic
 * coordinates longitude, latitude, height [meter] to global
 * cartesian coordinates.
 * @param pos The geographic coordinates to convert.
 */
QVector3D lonLatMetreToGlobalCart(QVector3D pos);

/**
 * @brief Converts containing geographic longitude, latitude,
 * height [meter] to global cartesian coordinates.
 */
QVector3D lonLatMetreToGlobalCart(float lon, float lat, float height);

/**
 * @brief Converts a vector @p pos containing geographic
 * coordinates longitude, latitude, pressure [Pa] to global
 * cartesian coordinates.
 * @param pos The geographic coordinates to convert.
 */
QVector3D lonLatPressureToGlobalCart(QVector3D pos);

/**
 * @brief Converts containing geographic longitude, latitude,
 * pressure [Pa] to global cartesian coordinates.
 */
QVector3D lonLatPressureToGlobalCart(float lon, float lat, float pressure_Pa);

/**
 * @return Approximate conversion of a longitudinal distance @p s in meters at position
 * @p positionLongitude to degrees latitude.
 * @param s Distance [m] in longitude direction.
 * @param positionLatitude Latitude [°] of the position.
 */
float longitudeLengthMeterToDegree(float s, float positionLatitude);

/**
 * @return Conversion of a latitudinal distance @p s in meters to degrees latitude.
 * @param s Distance [m] in latitude direction.
 * @param positionLatitude Latitude [°] of the position.
 */
float latitudeLengthMeterToDegree(float s);


/**
 * Calculates the y component of the cross product between vectors @p a and @p b.
 * @param a First vector.
 * @param b Second vector.
 * @return The y component of the resulting cross product.
 */
inline double crossProductY(const QVector2D& a, const QVector2D& b)
{
    return a.x() * b.y() - a.y() * b.x();
}


/**
 * Calculates the intersection point between two line segments in 2D.
 * @param p Start point of the first line.
 * @param p2 End point of the first line.
 * @param q Start point of the second line.
 * @param q2 End point of the second line.
 * @param out The intersection point.
 * @return Whether or not an intersection point exists.
 */
bool getLineSegmentsIntersectionPoint(
        const QVector2D &p, const QVector2D &p2,
        const QVector2D &q, const QVector2D &q2,
        QVector2D &out);


/**
    Conversion of pressure (Pa) to geometric height (m) with hydrostatic
    equation, according to the profile of the ICAO standard atmosphere.

    Reference:
        For example, H. Kraus, Die Atmosphaere der Erde, Springer, 2001,
        470pp., Sections II.1.4. and II.6.1.2.

    @param p_Pa pressure (Pa)
    @return geometric elevation in m
 */
double pressure2metre_standardICAO(double p_Pa);


/**
    Conversion of geometric height (given in m) to pressure (Pa) with
    hydrostatic equation, according to the profile of the ICAO standard
    atmosphere.

    Reference:
        For example, H. Kraus, Die Atmosphaere der Erde, Springer, 2001,
        470pp., Sections II.1.4. and II.6.1.2.

    @param z_m elevation in m
    @return pressure in Pa
 */
double metre2pressure_standardICAO(double z_m);


/**
    International standard atmosphere temperature at the given elevation.

    Reference:
        For example, H. Kraus, Die Atmosphaere der Erde, Springer, 2001,
        470pp., Sections II.1.4. and II.6.1.2.

    @param z_m elevation in m
    @return temperature (K)
 */
double isaTemperature(double z_m);


/**
  Convert @p flightlevel in hft to metres.
 */
double flightlevel2metre(double flightlevel);


/**
  Convert @p z_m in m to flightlevel in hft.
 */
double metre2flightlevel(double z_m);


/**
  Computes the mass (kg) of the air in a column with surface area @p area_m2
  (m^2), between bottom and top pressures @p pbot_Pa (Pa) and @p ptop_Pa (Pa).

  Reference:
    Wallace and Hobbs (2006), Atmospheric Science 2nd ed., p 68, Fig. 3.1:
    downward force Fd = mass * grav. accel. == upward force Fu = deltap * area.

  @return airmass (kg)
 */
double columnAirmass(double pbot_Pa, double ptop_Pa, double area_m2);


/**
  Computes the volume (m^3) of a box with air pressure @p p_Pa (Pa), air mass
  @p mass_kg (kg) and air temperature @p temp_K (K). The ideal gas law with
  the gas constant for dry air is used.

  Reference:
    Wallace and Hobbs (2006), Atmospheric Science 2nd ed., Ch. 3.
    Ideal gas law: p*V = m*R*T --> V = m*R*T / p

  @return volume in m^3
 */
double boxVolume_dry(double p_Pa, double mass_kg, double temp_K);


/**
  Computes the volume of a regular lon/lat/pressure grid box.

  If @p temp_K is not specified, the temperature is estimated from the
  international standard atmosphere. @see isaTemperature().

  @return volume in m^3
 */
double boxVolume_dry(double northWestLon, double northWestLat,
                     double southEastLon, double southEastLat,
                     double pmid_Pa, double pbot_Pa, double ptop_Pa,
                     double temp_K = M_MISSING_VALUE);

/**
  Computes wind speed in [m.s-1] from eastward wind (@p u_ms) and northward
  wind (@p v_ms).
 */
double windSpeed_ms(double u_ms, double v_ms);


/**
  Computes 3D wind speed (aka magnitude of air velocity in CF naming) in
  [m.s-1] from eastward wind (@p u_ms), northward wind (@p v_ms) and
  upward_air_velocity (@p w_ms).
 */
double windSpeed3D_ms(double u_ms, double v_ms, double w_ms);


/**
  Computes potential temperature in [K] from temperature @p T_K in [K] and
  pressure @p p_Pa in [Pa].

  Method:
                            theta = T * (p0/p)^(R/cp)
      with p0 = 100000. Pa, R = 287.058 JK-1kg-1, cp = 1004 JK-1kg-1.
 */
double potentialTemperature_K(double T_K, double p_Pa);


/**
  Computes ambient temperature in [K] of a given potential temperature in [K]
  at pressure @p p_Pa in [Pa].

  Method:
                            T = theta / (p0/p)^(R/cp)
      with p0 = 100000. Pa, R = 287.058 JK-1kg-1, cp = 1004 JK-1kg-1.
 */
double ambientTemperatureOfPotentialTemperature_K(double theta_K, double p_Pa);


/**
  Computes the virtual temperature in [K] from temperature @p T_K in [K] and
  specific humidity @p q_kgkg in [kg/kg].

  Method:
                  Tv = T * (q + 0.622(1-q)) / 0.622

  Reference: Wallace&Hobbs 2nd ed., eq. 3.16, 3.59, and 3.60
             (substitute w=q/(1-q) in 3.16 and 3.59 to obtain the exact
             formula).
 */
double virtualTemperature_K(double T_K, double q_kgkg);


/**
  Computes the thickness of an atmospheric layer in terms of geopotential
  height in [m], using the hypsometric question (equation (1.17) from Holton,
  "An Introduction to Dynamic Meteorology", 3rd edition, 1992) in its
  geopotential height formulation (equation (1.18 and 1.19) from Holton).

  @p layerMeanVirtualTemperature_K specifies the layer mean (virtual)
  temperature (use the virtual temperature to account for humidity) in [K],
  @p p_bot and @p p_top the bounding pressure heights (can be either both in
  [Pa] or both in [hPa]; only their ratio is used).
 */
double geopotentialThicknessOfLayer_m(double layerMeanVirtualTemperature_K,
                                      double p_bot, double p_top);


/**
  Computes the mixing ratio w from specific humidity @p q_kgkg in [kg/kg].

  Method: w = q / (1.-q)
 */
double mixingRatio_kgkg(double q_kgkg);


/**
  Computes the specific humidity from mixing ratio @p w_kgkg in [kg/kg].

  Method: q = w / (1.+w)
 */
double specificHumidity_kgkg(double w_kgkg);


/**
  Computes dew point in [K] from pressure @p p_Pa in [Pa] and specific
  humidity @p q_kgkg in [kg/kg].

  Method: Use the inversion of Bolton (1980), eq. 10, to compute dewpoint.
  According to Bolton, this is accurate to 0.03 K in the range 238..308 K. See
  also Emanuel (1994, 'Atmospheric Convection', eq. 4.6.2).
 */
double dewPointTemperature_K_Bolton(double p_Pa, double q_kgkg);


/**
  Computes equivalent potential temperature in [K] from temperature @p T_K
  in [K], pressure @p p_Pa in [Pa] and specific humidity @p q_kgkg in [kg/kg].

  Method: Equation (43) of Bolton (MWR, 1980), "The Computation of Equivalent
  Potential Temperature".
 */
double equivalentPotentialTemperature_K_Bolton(double T_K, double p_Pa,
                                               double q_kgkg);


/**
  Computes the ambient temperature at pressure @p p_Pa along a saturated
  adiabat defined by wet-bulb potential temperature @p thetaW_K. Computation
  uses the (approximative) noniterative approach described by Moisseeva and
  Stull (ACP, 2017), "Technical note: A noniterative approach to modelling
  moist thermodynamics".
 */
double temperatureAlongSaturatedAdiabat_K_MoisseevaStull(
        double thetaW_K, double p_Pa);

/**
  Inverse of @ref temperatureAlongSaturatedAdiabat_K_MoisseevaStull().
 */
double wetBulbPotentialTemperatureOfSaturatedAdiabat_K_MoisseevaStull(
        double T_K, double p_Pa);

/**
  Coriolis parameter at specified latitude @p lat (in degrees).
 */
double coriolisParameter_deg(double lat);

/**
  Computes the saturation vapour pressure over water and over ice, using
  the equations presented by Huang (JAMC, 2018), "A Simple Accurate Formula
  for Calculating Saturation VaporPressure of Water and Ice",
  DOI: 10.1175/JAMC-D-17-0334.1
 */
double saturationVapourPressure_Pa_Huang2018(double T_K);

/**
  Computes relative humidity, using the saturation vapour pressure computed
  by @ref saturationVapourPressure_Pa_Huang2018().
 */
double relativeHumdity_Huang2018(double p_Pa, double T_K, double q_kgkg);

/**
 * Converts the height of earth's surface above sea level (in meters) to the
 * corresponding geopotential.
 * Implementation as in MetPy 1.6:
 * https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.height_to_geopotential.html
 * Based on Hobbs, P. V., and J. M. Wallace, 2006: Atmospheric Science:
 * An Introductory Survey. 2nd ed. Academic Press, 504
 * @param surfaceHeight_m Height of earth's surface above sea level in meters.
 * @return The geopotential in m^2 / s^2.
 */
double surfaceHeightToSurfaceGeopotential(double surfaceHeight_m);

/**
 * Computes the omega wind speed (Pa/s) from the vertical wind speed (m/s), and
 * the pressure and temperature at a point assuming hydrostatic conditions.
 * This is only an approximation!
 * Implementation as in MetPy 1.6:
 * https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.vertical_velocity_pressure.html
 * Based on Hobbs, P. V., and J. M. Wallace, 2006: Atmospheric Science:
 * An Introductory Survey. 2nd ed. Academic Press, 504
 * @param w_ms The vertical wind speed in m/s.
 * @param p_Pa The pressure in Pa.
 * @param T_K The temperature in K.
 * @return Omega in Pa/s.
 */
double omegaFromVerticalWindSpeedHydrostatic
    (double w_ms, double p_Pa, double T_K);

/******************************************************************************
***            WRAPPER for LAGRANTO LIBCALVAR FORTRAN FUNCTIONS             ***
*******************************************************************************/

/**
  Wrapper for Fortran implementation of potential temperature in the LAGRANTO
  package (libcalvar.f; https://svn.iac.ethz.ch/websvn/pub/wsvn/lagranto.ecmwf/).

  See http://www.yolinux.com/TUTORIALS/LinuxTutorialMixingFortranAndC.html
  for information about how to mix Fortran and C++. In particular the index
  dimension of the arrays is of importance. However, the methods in libcalvar
  are conveniently written so that arrays are accessed in FORTRAN i,j,k order,
  which corresponds to the k,j,i oder used in thje C++ code of Met.3D.

  @note All fields below are in the index order that is used in MStructuredGrid
  EXCEPT the lat dimension, which needs to be reversed prior to calling this
  method.

  @param[out] pt Potential temperature in K; pointer to 3D field.
  @param[in] t Temperature in K; pointer to 3D field.
  @param[in] sp Surface pressure in hPa; pointer to 2D field.
  @param ie Array size in lon dimension.
  @param je Array size in lat dimension.
  @param ke Array size in lev dimension.
  @param[in] ak Hybrid coefficients.
  @param[in] bk Hybrid coefficients.
 */
void potentialTemperature_K_calvar(
        float *pt, float *t, float *sp,
        int ie, int je, int ke,
        float *ak, float *bk);


/**
  Wrapper for Fortran implementation of potential vorticity in the LAGRANTO
  package (libcalvar.f; https://svn.iac.ethz.ch/websvn/pub/wsvn/lagranto.ecmwf/).

  See above comment of @ref potentialTemperature_K_calvar.

  @param[out] pv Potential vorticity in PVU; pointer to 3D field.
  @param[in] uu u-wind in ms-1; pointer to 3D field.
  @param[in] vv v-wind in ms-1; pointer to 3D field.
  @param[in] th Potential temperature in K; pointer to 3D field.
  @param[in] sp Surface pressure in hPa; pointer to 2D field.
  @param[in] cl cos(lat); pointer to 2D field.
  @param[in] f Coriolis parameter; pointer to 2D field.
  @param ie Array size in lon dimension.
  @param je Array size in lat dimension.
  @param ke Array size in lev dimension.
  @param[in] ak Hybrid coefficients.
  @param[in] bk Hybrid coefficients.
  @param[in] vmin 4-element array containing [min lon, min lat, ?, ?]
  @param[in] vmax 4-element array containing [max lon, max lat, ?, ?]
 */
void potentialVorticity_PVU_calvar(
        float *pv, float *uu, float *vv, float *th,
        float *sp, float *cl, float *f,
        int ie, int je, int ke,
        float *ak, float *bk,
        float *vmin, float *vmax);


/**
  Wrapper for Fortran implementation of partial derivatives filter in the LAGRANTO
  package (libcalvar.f; https://svn.iac.ethz.ch/websvn/pub/wsvn/lagranto.ecmwf/).

  See http://www.yolinux.com/TUTORIALS/LinuxTutorialMixingFortranAndC.html
  for information about how to mix Fortran and C++. In particular the index
  dimension of the arrays is of importance. However, the methods in libcalvar
  are conveniently written so that arrays are accessed in FORTRAN i,j,k order,
  which corresponds to the k,j,i oder used in thje C++ code of Met.3D.

  @note All fields below are in the index order that is used in MStructuredGrid
  EXCEPT the lat dimension, which needs to be reversed prior to calling this
  method.

  @param[in] a Pointer to 3D input field.
  @param[out] d Pointer to 3D output field.
  @param[in] ps Pointer to surface pressure 2D field.
  @param[in] dps Pointer to derivative of surface pressure 2D field in hPa.
  @param[in] cl Pointer to cosinus of latitude 2d field.
  @param[in] dir Direction of partial derivative ("X" or "Y")
  @param[in] ie Array size in lon dimension.
  @param[in] je Array size in lat dimension.
  @param[in] ke Array size in lev dimension.
  @param[in] datmin Array of size 4, containing mininum longitude (array position 0) and latitude (array position 1)
  @param[in] datmax Array of size 4, containing maximum longitude (array position 0) and latitude (array position 1)
  @param[in] ak Hybrid coefficients.
  @param[in] bk Hybrid coefficients.
 */
void horizontalPartialDerivative_calvar(
        float *a, float *d, float *ps, float *dps, float *cl,
        char dir, int ie, int je, int ke, float *datmin,
        float *datmax, float *ak, float *bk);


/******************************************************************************
***            Test functions for meteorological computations.              ***
*******************************************************************************/

namespace MetRoutinesTests
{

void test_EQPT();
void test_temperatureAlongSaturatedAdiabat_K_MoisseevaStull();
void test_saturationVapourPressure();
void test_omegaFromVerticalWindSpeedHydrostatic();
void test_surfaceHeightToSurfaceGeopotential();

void runMetRoutinesTests();

} // namespace MetRoutinesTests

} // namespace Met3D

#endif // METROUTINES_H
