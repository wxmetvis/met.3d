/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MFILETYPES_H
#define MET_3D_MFILETYPES_H

// standard library imports

// related third party imports
#include <QMimeDatabase>

// local application imports
#include "util/mfileutils.h"

/**
 * This namespace contains static file type definitions of all file types in use by Met.3D.
 * All file types in use by Met.3D should be defined here, so that they are easy to find and change.
 */
namespace FileTypes
{

// Window layout configuration file type.
static const Met3D::MFileType M_WINDOW_LAYOUT = {"Window layout configuration file", ".winlayout.conf"};

// Pipeline configuration file type.
static const Met3D::MFileType M_PIPELINE_CONFIG = {"Dataset configuration", ".pipeline.conf"};

// Geotiff and tiff image file type.
static const Met3D::MFileType M_GEO_TIFF_IMAGES = {"Tiff Image Files", {".tif", ".geotiff"}};

// Image file type for gif, png and jpeg images.
static const Met3D::MFileType M_IMAGES = {"Image Files", {".gif", ".png", ".jpg", ".jpeg"}};

// PNG image file type
static const Met3D::MFileType M_IMAGE_PNG = {"Image Files", ".png"};

// JPEG image file type
static const Met3D::MFileType M_IMAGE_JPG = {"Image Files", {".jpg", ".jpeg"}};

// WEBP image file type
static const Met3D::MFileType M_IMAGE_WEBP = {"Image Files", ".webp"};

// BMP image file type
static const Met3D::MFileType M_IMAGE_BMP = {"Image Files", ".bmp"};

// PPM image file type
static const Met3D::MFileType M_IMAGE_PPM = {"Image Files", ".ppm"};

// TIFF image file type
static const Met3D::MFileType M_IMAGE_TIFF = {"Image Files", ".tiff"};

// XBM image file type
static const Met3D::MFileType M_IMAGE_XBM = {"Image Files", ".xbm"};

// XPM image file type
static const Met3D::MFileType M_IMAGE_XPM = {"Image Files", ".xpm"};

// Lagranto .2 file type.
static const Met3D::MFileType M_LAGRANTO_2 = {"Lagranto", ".2"};

// Camera sequence config file type.
static const Met3D::MFileType M_CAMERA_SEQUENCE_CONFIG = {"Camera Sequence XML", ".xml"};

// Flight track config file type.
static const Met3D::MFileType M_FLIGHT_TRACK_CONFIG = {"Flight Track XML", ".ftml"};

// Camera config file type.
static const Met3D::MFileType M_CAMERA_CONFIG = {"Camera configuration file", ".camera.conf"};

// Actor config file type.
static const Met3D::MFileType M_ACTOR_CONFIG = {"Actor configuration file", ".actor.conf"};

// Scene view config file type.
static const Met3D::MFileType M_SCENE_VIEW_CONFIG = {"Scene view configuration file", ".sceneview.conf"};

// Session config file type. It is IMPORTANT, that this file type only has one viable extension.
static const Met3D::MFileType M_SESSION_CONFIG = {"Session configuration file", ".session.config"};

// Synchronization control config file type.
static const Met3D::MFileType M_SYNC_CONTROL_CONFIG = {"Sync control configuration file", ".synccontrol.conf"};

//Bounding box config file type.
static const Met3D::MFileType M_BOUNDING_BOX_CONFIG = {"Bounding boxes configuration file", ".bbox.conf"};

// Vapor transfer function file type.
static const Met3D::MFileType M_VAPOR_XML = {"Vapor transfer function XML", ".vtf"};

// ParaView Colormap XML file.
static const Met3D::MFileType M_PARA_VIEW_COLORMAP_XML = {"ParaView colormap XML", ".xml"};

// NetCDF file.
static const Met3D::MFileType M_NETCDF = {"NetCDF file", ".nc"};

// Log file type.
static const Met3D::MFileType M_LOG_FILE = {"Log file", ".log"};

// Wavefront OBJ file.
static const Met3D::MFileType M_WAVEFRONT_OBJ = {"Wavefront OBJ", ".obj"};

// txt file.
static const Met3D::MFileType M_TXT = {"Text", ".txt"};

// Python file.
static const Met3D::MFileType M_PYTHON = {"Python", ".py"};

}

#endif //MET_3D_MFILETYPES_H
