/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2023-2024 Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mutil.h"

// standard library imports

// related third party imports
#include "GL/glew.h"
#include <log4cplus/loggingmacros.h>

// local application imports


/******************************************************************************
***                             LOGGING                                     ***
*******************************************************************************/

// Global reference to the Met3D application logger (defined in mutil.cpp).
log4cplus::Logger mlog =
        log4cplus::Logger::getInstance(LOG4CPLUS_TEXT("met3d"));


/******************************************************************************
***                          UTILITY FUNCTIONS                              ***
*******************************************************************************/

void checkOpenGLError(const char* file, int line)
{
    GLenum error = glGetError();
    if (error != GL_NO_ERROR)
    {
        if (line >= 0)
        {
            LOG4CPLUS_ERROR(mlog, "OPENGL ERROR # " << gluErrorString(error)
                            << " (line " << line << " in " << file << ")");
        }
        else
        {
            LOG4CPLUS_ERROR(mlog, "OPENGL ERROR # " << gluErrorString(error));
        }
    }
}


QVersionNumber parseVersionString(const QString& versionString)
{
    QVersionNumber versionNumber;
    QRegExp currentFormatRegExp(currentVersionNumberingRegex);
    QRegExp oldFormatRegExp(oldVersionNumberingRegex);

    if (currentFormatRegExp.indexIn(versionString) >= 0)
    {
        // Version string is in current format (major.minor.micro/MonthYear).
        QStringList parsedValues = currentFormatRegExp.capturedTexts();

        // Assemble version number.
        versionNumber = QVersionNumber(
            parsedValues[1].toInt(), parsedValues[2].toInt(),
            parsedValues[3].toInt());
    }
    else if (oldFormatRegExp.indexIn(versionString) >= 0)
    {
        // Version string is in current format (major.minor/year.month.micro).
        QStringList parsedValues = oldFormatRegExp.capturedTexts();

        // Assemble version number.
        versionNumber = QVersionNumber(
            parsedValues[1].toInt(), parsedValues[2].toInt(),
            parsedValues[5].toInt());
    }
    else
    {
        LOG4CPLUS_WARN(mlog, "Unable to parse version number ' "
                             << versionString.toStdString() << "'. "
                             << "Setting it to 1.0.0.");
        versionNumber = QVersionNumber(1, 0, 0);
    }
    return versionNumber;
}


QVersionNumber readConfigVersion(QSettings *settings)
{
    QStringList groupList = settings->group().split("/", Qt::SkipEmptyParts);
    // To get to FileFormat group end all current groups.
    for (int i = 0; i < groupList.size(); i++)
    {
        settings->endGroup();
    }

    settings->beginGroup("FileFormat");
    QString versionString = (settings->value("met3dVersion",
                                             defaultConfigVersion).toString());
    settings->endGroup();

    // Remove appendix.
    versionString = versionString.split("-")[0];
    // Parse version.
    QVersionNumber versionNumber = parseVersionString(versionString);

    // Restore group state from before entering this function.
    for (const QString& groupName : groupList)
    {
        settings->beginGroup(groupName);
    }
    return versionNumber;
}


QString expandEnvironmentVariables(QString path)
{
    QRegExp regExpEnvVar("\\$([A-Za-z0-9_]+)");

    int i;
    while ((i = regExpEnvVar.indexIn(path)) != -1)
    {
        QString envVar = regExpEnvVar.cap(1);
        QString expansion;
        if (envVar == "$HOME")
        {
            // Special case "$HOME" environment variable: use QDir::homePath()
            // to obtain correct home directory on all platforms.
            expansion = QDir::homePath();
        }
        else
        {
            expansion = QProcessEnvironment::systemEnvironment().value(
                                envVar);
        }

        if (expansion.isEmpty())
        {
            LOG4CPLUS_ERROR(mlog, "ERROR: Environment variable "
                            << envVar.toStdString()
                            << " has not been defined. Cannot expand variable.");
            break;
        }
        else
        {
            path.remove(i, regExpEnvVar.matchedLength());
            path.insert(i, expansion);
        }
    }
    makePathAbsolute(path);
    return path;
}


QString inputVarsMapToString(const QMap<QString, QString>& map)
{
    QString stringRep;

    for (auto it = map.keyValueBegin(); it != map.keyValueEnd(); it++)
    {
        const QString& stdName = it->first;
        const QString& varName = it->second;
        if (varName.isEmpty())
        {
            continue;
        }
        stringRep.append(stdName);
        stringRep.append(":");
        stringRep.append(varName);
        stringRep.append("/");
    }
    stringRep.chop(1);
    return stringRep;
}


QMap<QString, QString> inputVarsStringToMap(const QString& stringRep)
{
    QMap<QString, QString> map;
    QStringList derivedVarsMappingList =
        stringRep.split("/", Qt::SkipEmptyParts);

    for (const QString &derivedVarsMapping : derivedVarsMappingList)
    {
        QStringList stdNameToVarName =
            derivedVarsMapping.split(":", Qt::SkipEmptyParts);
        if (stdNameToVarName.size() != 2)
        {
            // Bad formatted.
            continue;
        }
        map.insert(stdNameToVarName[0], stdNameToVarName[1]);
    }
    return map;
}


void makePathAbsolute(QString& path)
{
    if (path.startsWith("~/"))
    {
        path.remove(0, 1);
        path.prepend(QDir::homePath());
    }
    path = QDir(path).absolutePath();
}


bool isValidObjectName(QString name)
{
    return name != "None";
}


QVector<float> parseFloatRangeString(QString levels)
{
    // Clear the current list of values. If "levels" does not
    // match any accepted format, no values are returned.
    QVector<float> floatValueList;

    // Empty strings, i.e. no values, are accepted.
    if (levels.isEmpty())
    {
        return floatValueList;
    }

    // Match strings of format "[0,100,10]" or "[0.5,10,0.5]".
    QRegExp rxRange("^\\[([\\-|\\+]*\\d+\\.*\\d*),([\\-|\\+]*\\d+\\.*\\d*),"
                            "([\\-|\\+]*\\d+\\.*\\d*)\\]$");
    // Match strings of format "1,2,3,4,5" or "0,0.5,1,1.5,5,10" (number of
    // values is arbitrary).
    QRegExp rxList("^(\\s*[\\-|\\+]*\\d+\\.*\\d*\\s*,*)+$");

    if (rxRange.exactMatch(levels))
    {
        QStringList rangeValues = rxRange.capturedTexts();

        bool ok;
        double from = rangeValues.value(1).toFloat(&ok);
        if (!ok) return floatValueList;
        double to   = rangeValues.value(2).toFloat(&ok);
        if (!ok) return floatValueList;
        double step = rangeValues.value(3).toFloat(&ok);
        if (!ok) return floatValueList;

        if (step > 0)
        {
            for (float d = from; d <= to; d += step)
            {
                floatValueList << d;
            }
        }
        else if (step < 0)
        {
            for (float d = from; d >= to; d += step)
            {
                floatValueList << d;
            }
        }
    }
    else if (rxList.exactMatch(levels))
    {
        QStringList listValues = levels.split(",");

        bool ok;
        for (QString str : listValues)
        {
            float value = str.toFloat(&ok);
            if (ok) floatValueList << value;
        }
    }

    return floatValueList;
}


QString listOfPressureLevelsAsString(QVector<float> levels, QString delimiter)
{
    // Create empty string.
    QString stringValue = QString("");

    // Add values to string with given delimiter.
    for (int i = 0; i < levels.size(); i++)
    {
        if (i == 0)
        {
            stringValue.append(QString("%1").arg(levels.at(i)));
        }
        else
        {
            stringValue.append(QString("%1%2").arg(delimiter).arg(levels.at(i)));
        }
    }

    return stringValue;
}


// The following method has been adapted  from
// https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
bool floatIsAlmostEqualRelativeAndAbs(float floatA, float floatB,
                                      float maxDiff, float maxRelDiff)
{
    // Check if the numbers are really close -- needed
    // when comparing numbers near zero.
    float diff = fabs(floatA - floatB);
    if (diff <= maxDiff) return true;

    floatA = fabs(floatA);
    floatB = fabs(floatB);
    float largest = (floatB > floatA) ? floatB : floatA;

    if (diff <= largest * maxRelDiff) return true;
    return false;
}


float scaleFactorToHPa(const std::string &unit)
{
    if (unit == "hPa" || unit == "millibar" || unit == "millibars")
    {
        return 1.0;
    }
    if (unit == "Pa")
    {
        return 0.01; // 1 Pa -> 0.01 hPa
    }
    if (unit == "bar" || unit == "bars")
    {
        return 1000.0; // 1 bar -> 1000 hPa
    }
    if (unit == "decibar" || unit == "decibars")
    {
        return 100.0; // 1 decibar = 0.1 bar -> 100 hPa
    }
    if (unit == "atmosphere" || unit == "atm")
    {
        return 1013.25; // 1 atm pressure = 1013.25 hPa
    }
    return NAN;
}


QStringList getSupportedPressureUnits()
{
    return {"hPa", "millibar", "millibars", "Pa", "bar", "bars",
            "decibar", "decibars", "atmosphere", "atm"};
}


QStringList getSupportedLengthUnits()
{
    return {"m", "metre", "metres", "meter", "meters"};
}
