/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MFILEUTILS_H
#define MET_3D_MFILEUTILS_H

// standard library imports

// related third party imports
#include <QString>
#include <QVector>
#include <QStringList>
#include <QWidget>

// local application imports
#include "gxfw/msystemcontrol.h"

/**
 * Use this define to use the native os file dialog.
 * It has issues with conda and clion though,
 * where a path to runtime directories (/run/) is returned instead of the correct file path.
 * The non-native dialog doesn't have this issue.
 */
//#define USE_NATIVE_DIALOG

namespace Met3D
{

/**
 * Struct for describing a file type using a display name and a list of extensions.
 * @ref @c mfiletype.h for all currently used file types. Please also declare your file types there.
 */
struct MFileType
{
public:
    /**
     * The full file type name.
     * @example Images JPEG
     */
    QString fileTypeName;

    /**
     * A list of all compatible file extensions for this file type.
     * @example @c jpg, @c .jpg or @c *.jpg are valid extensions.
     */
    QStringList fileExtensions;

    /**
     * Construct an empty file type with no name and no extensions.
     */
    MFileType();

    /**
     * Construct a file type with name @p fileTypeName and allowed extensions @p extensions.
     */
    MFileType(QString fileTypeName, QStringList extensions);

    /**
     * Construct a file type with name @p fileTypeName extension @p extension.
     */
    MFileType(QString fileTypeName, const QString &extension);

    /**
     * Construct a file type from a given @p mimeType.
     * @ref QMimeDatabase To retrieve correct mime types.
     */
    explicit MFileType(const QMimeType &mimeType);

    /**
     * Creates a filter string for use with the @c QFileDialog.
     */
    QString getFilterString() const;

    /**
     * Returns the default file extension of this file type.
     * The extension is formatted as "@c.extension": @c.jpg, @c.png, and @c.gif.
     * If this filetype is invalid, it returns an empty string.
     * @example @c .png
     * @example @c .session.conf
     */
    QString getDefaultExtension() const;

    /**
     * Checks if the file type is valid.
     * A file type is valid, if it has a specified type name, and at least one file extension.
     * @return True if valid, otherwise false.
     */
    bool isValid() const;

    /**
     * Checks if the given file is of this file type.
     * @param file File to check.
     * @return True, if the file extension of the checked file is listed in the file extensions of this type, otherwise false.
     */
    bool isType(const QFile &file) const;

    /**
     * Checks if the given filename is of this file type.
     * @param file Filename to check.
     * @return True, if the file extension of the checked file is listed in the file extensions of this type, otherwise false.
     */
    bool isType(const QString &file) const;

    /**
     * Returns a wildcard file type, that includes all files.
     */
    static const MFileType &getWildcard();
};


/**
 * A temporary config file for Met.3D.
 * Can be used to store actor config temporarily without writing
 * it to a file that persists.
 */
struct MTempConfigFile
{
    MTempConfigFile();
    ~MTempConfigFile();

    /**
     * The settings object stored im the temp file.
     */
    QSettings *settings;
    /**
     * The temp file used to store the settings object.
     */
    QTemporaryFile tmpFile;
};

class MFileUtils
{

public:
    /**
     * Opens an interactive file dialog that can be used to select or create a save file.
     * If the user doesnt manually input an extension to its save file name, the dialog will use
     * the first valid extension from the @p fileFilter and append it.
     * If the user manually adds an invalid extension, the selected filters first valid extension is added to the filename.
     * @param parent The parent widget that should own the dialog.
     * @param title The title of the file dialog.
     * @param fileFilter A file filter used to filter the selectable files.
     * @param directory The directory to open in the file dialog. If none is specified, the Met.3D working directory is used.
     * @param defaultFileName The filename that is used by default. It should NOT contain an extension. Default is no file name.
     * @return The filename of the save file, or an empty string if the dialog was cancelled.
     */
    static QString getSaveFileName(QWidget *parent,
                                   const QString &title,
                                   const MFileType &fileFilter,
                                   const QString &directory = MSystemManagerAndControl::getInstance()
                                           ->getMet3DWorkingDirectory().path(),
                                   const QString &defaultFileName = "");

    /**
     * Opens an interactive file dialog that can be used to select or create a save file.
     * If the user doesnt manually input an extension to its save file name, the dialog will use
     * the first valid extension from the @p fileFilters and append it.
     * If the user manually adds an invalid extension, the selected filters first valid extension is added to the filename.
     * @param parent The parent widget that should own the dialog.
     * @param title The title of the file dialog.s
     * @param fileFilters A list of file filters used to filter the selectable files.
     * If an empty list is provided, all files are selectable. The first entry is used as the default file extension.
     * @param directory The directory to open in the file dialog. If none is specified, the Met.3D working directory is used.
     * @param defaultFileName The filename that is used by default. It should NOT contain an extension. Default is no file name.
     * @return The filename of the save file, or an empty string if the dialog was cancelled.
     */
    static QString getSaveFileName(QWidget *parent,
                                   const QString &title,
                                   const QVector<MFileType> &fileFilters,
                                   const QString &directory = MSystemManagerAndControl::getInstance()
                                           ->getMet3DWorkingDirectory().path(),
                                   const QString &defaultFileName = "");

    /**
     * Opens an interactive file dialog that can be used to open a file.
     * @param parent The parent widget that should own the dialog.
     * @param title The title of the file dialog.
     * @param fileFilter A file filter used to filter the selectable files.
     * @param directory The directory to open in the file dialog. If none is specified, the Met.3D working directory is used.
     * @return The filename of the opened file, or an empty string if the dialog was cancelled.
     */
    static QString getOpenFileName(QWidget *parent,
                                   const QString &title,
                                   const MFileType &fileFilter,
                                   const QString &directory = MSystemManagerAndControl::getInstance()
                                           ->getMet3DWorkingDirectory().path());

    /**
     * Opens an interactive file dialog that can be used to open a file.
     * @param parent The parent widget that should own the dialog.
     * @param title The title of the file dialog.s
     * @param fileFilters A list of file filters used to filter the selectable files.
     * If an empty list is provided, all files are selectable. The first entry is used as the default file extension.
     * @param directory The directory to open in the file dialog. If none is specified, the Met.3D working directory is used.
     * @return The filename of the opened file, or an empty string if the dialog was cancelled.
     */
    static QString getOpenFileName(QWidget *parent,
                                   const QString &title,
                                   const QVector<MFileType> &fileFilters,
                                   const QString &directory = MSystemManagerAndControl::getInstance()
                                           ->getMet3DWorkingDirectory().path());

    /**
     * Opens an interactive file dialog that can be used to open a directory.
     * @param parent The parent widget that should own the dialog.
     * @param title The title of the file dialog.s
     * @param fileFilters A list of file filters used to filter the selectable files.
     * If an empty list is provided, all files are selectable. The first entry is used as the default file extension.
     * @param directory The directory to open in the file dialog. If none is specified, the Met.3D working directory is used.
     * @return The filename of the opened file, or an empty string if the dialog was cancelled.
     */
    static QString getOpenDirectory(QWidget *parent,
                                    const QString &title,
                                    const QString &directory = MSystemManagerAndControl::getInstance()
                                            ->getMet3DWorkingDirectory().path());

    /**
     * Opens an interactive file dialog that can be used to open several files, or none.
     * @param parent The parent widget that should own the dialog.
     * @param title The title of the file dialog.
     * @param fileFilter A file filter used to filter the selectable files.
     * @param directory The directory to open in the file dialog. If none is specified, the Met.3D working directory is used.
     * @return A string list of opened filenames. It can also be empty, if no file was selected.
     */
    static QStringList getOpenFileNames(QWidget *parent,
                                        const QString &title,
                                        const MFileType &fileFilter,
                                        const QString &directory = MSystemManagerAndControl::getInstance()
                                                ->getMet3DWorkingDirectory().path());

    /**
     * Opens an interactive file dialog that can be used to open several files, or none.
     * @param parent The parent widget that should own the dialog.
     * @param title The title of the file dialog.s
     * @param fileFilters A list of file filters used to filter the selectable files.
     * If an empty list is provided, all files are selectable. The first entry is used as the default file extension.
     * @param directory The directory to open in the file dialog. If none is specified, the Met.3D working directory is used.
     * @return A string list of opened filenames. It can also be empty, if no file was selected.
     */
    static QStringList getOpenFileNames(QWidget *parent,
                                        const QString &title,
                                        const QVector<MFileType> &fileFilters,
                                        const QString &directory = MSystemManagerAndControl::getInstance()
                                                ->getMet3DWorkingDirectory().path());
};

} // Met3D

#endif //MET_3D_MFILEUTILS_H
