/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2025 Marc Rautenhaus [*, previously +]
**  Copyright 2015-2017 Bianca Tost [+]
**  Copyright 2023      Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MUTIL_H
#define MUTIL_H

// standard library imports
#include <typeinfo>
#include <float.h>
#include <string>

// related third party imports
#include <QtCore>
#include "log4cplus/logger.h"
#include <QVersionNumber>
#include <QVector3D>
#include <QPointF>

// local application imports
#include "glsl/shared/msharedconstants.h"


/******************************************************************************
***                      VERSION INFORMATION                                ***
*******************************************************************************/

// Fill this with your own ID, e.g., "-research-my-name".
const QString met3dVersionBranchID = "";
// Set to "/devel" for development versions.
const QString met3dVersionDevelID = "";

const QString met3dVersionString = "1.15.0/Feb25" + met3dVersionDevelID
        + met3dVersionBranchID;
// String containing default value for missing version number in config.
const QString defaultConfigVersion = "1.0.0";
const QString met3dBuildDate = QString("built on %1 %2").arg(__DATE__).arg(__TIME__);

// Regular expressions for old (1.6-1.10) and current (1.11+) version numbering.
// Current matches major.minor.micro(/MonthYear) with Month being a 3-letter
// abbreviation of the month, and Year the last two digits of the year
const QString currentVersionNumberingRegex = R"(^(\d+)\.(\d+)\.(\d+)[/\D\D\D\d\d]?)";
// Old matches major.minor/year.month.micro with year and month both being
// two digits.
const QString oldVersionNumberingRegex = R"(^(\d+)\.(\d+)/(\d+)\.(\d+)\.(\d+))";

/// The version when the actor property rework was introduced,
/// to check for compatibility with config files created before the rework.
static const QVersionNumber ACTOR_PROPERTY_REWORK_COMP_VER = {1, 13, 4};

/******************************************************************************
***                             LOGGING                                     ***
*******************************************************************************/

// Global reference to the Met3D application logger (defined in mutil.cpp).
extern log4cplus::Logger mlog;


/******************************************************************************
***                              MACROS                                     ***
*******************************************************************************/

// Shortcut for OpenGL error checking.
#define GLErr(x) x; checkOpenGLError(__FILE__, __LINE__)
#define CHECK_GL_ERROR checkOpenGLError(__FILE__, __LINE__)

// Define a floating point modulo function that behaves like the Python
// modulo, i.e. -40.2 mod 360. == 319.8 and NOT -40.2, as the C++ fmod()
// function does. Required for cyclic grids.
#define MMOD(a, b) ((a) - floor((a) / (b)) * (b))

#define MFRACT(a) ((a) - int(a))

#define MMIX(x, y, a) (((x) * (1.-(a))) + ((y)*(a)))


/******************************************************************************
***                 DEFINES COMMON TO THE ENTIRE SYSTEM                     ***
*******************************************************************************/

// Maximum number of OpenGL contexts that can display a scene.
#define MET3D_MAX_SCENEVIEWS 6 // changed from 4 to handle 6 views

#define M_INVALID_TRAJECTORY_POS -999.99f

#define IS_MISSING(x) (qIsInf(x))

#define LAT_TO_METER 1.112e5

//WORKAROUND
    // NOTE (mr, Dec2013): Workaround to fix a float accuracy problem
    // occuring with some NetCDF data files (e.g. converted from GRIB with
    // netcdf-java): For example, such longitude arrays can occur:
    // -18, -17, -16, -15, -14, -13, -12, -11, -10, -9.000004, -8.000004,
    // The latter should be equal to -9.0, -8.0 etc. The inaccuracy causes
    // problems in MNWP2DHorizontalActorVariable::computeRenderRegionParameters()
    // and MClimateForecastReader::readGrid(), hence we compare to this
    // absolute epsilon to determine equality of two float values.
    // THIS WORKAROUND NEEDS TO BE REMOVED WHEN HIGHER RESOLUTIONS THAN 0.00001
    // ARE HANDLED BY MET.3D.
    // Cf. http://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
    // for potentially better solutions. -- see below.
#define M_LONLAT_RESOLUTION 0.00001


/******************************************************************************
***                            FUNCTIONS                                    ***
*******************************************************************************/

void checkOpenGLError(const char* file="", int line=-1);

inline float clamp(float x, float a, float b)
{
    return x < a ? a : (x > b ? b : x);
}

inline float clamp(double x, double a, double b)
{
    return x < a ? a : (x > b ? b : x);
}

inline int clamp(int x, int a, int b)
{
    return x < a ? a : (x > b ? b : x);
}

/**
 * Parses the version number from the given @c versionString.
 * Checks whether the version number is in the version format used for versions
 * 1.6 through 1.10, or in the current format used since version 1.11 by doing
 * regex pattern matching, and infers the major, minor and micro version.
 * Old format: major.minor/year.month.micro e.g. 1.10/24.02.07
 * New format: major.minor.micro/Monthyear e.g. 1.10.09/Feb24
 *   or just major.minor.micro e.g. 1.10.09
 * @return The version number populated with major, minor and micro version.
 * If parsing unsuccessful, print a warning and fall back to Version 1.0.0.
 */
QVersionNumber parseVersionString(const QString& versionString);

/**
 * Reads the Met3D version from the configuration given the @c settings of
 * an actor. Calls @c parseVersionString() to parse the version.
 * @return The version number populated with major, minor and micro version.
 * If parsing unsuccessful, print a warning and fall back to Version 1.0.0.
 */
QVersionNumber readConfigVersion(QSettings *settings);

/**
  Expands environment variables of format $VARIABLE in the string @p path.
  Example: If the envrionment variable "MET3D_HOME" is set to "/home/user/m3d",
  the path "$MET3D_HOME/config/data" would be expaned to
  "/home/user/m3d/config/data".
 */
QString expandEnvironmentVariables(QString path);

/**
 * This function takes a QMap where keys are standard variable names used in
 * Met.3D (for calculating derived variables), and values are the corresponding
 * user-defined names. The function returns a QString that represents this
 * mapping, which can be serialized and saved to a configuration file for later
 * use.
 * String format: var_standard_name1:v1/var_standard_name2:v2/...
 * @param map A QMap containing the mapping of standard variable names to
 * user-defined names.
 * @return A QString representation of the mapping that is suitable for
 * serialization.
 */
QString inputVarsMapToString(const QMap<QString, QString>& map);

/**
 * This function takes a QString, which represents a mapping of standard names
 * used in Met.3D for calculating derived variables and maps them back to the
 * corresponding user-defined variable names. The result is a QMap object with
 * keys as standard names and values as user-defined names.
 * String format: var_standard_name1:v1/var_standard_name2:v2/...
 * @param stringRep A QString representation of the mapping to be converted to
 * a QMap.
 * @return A QMap containing the mapping of standard variable names to
 * user-defined names.
 */
QMap<QString, QString> inputVarsStringToMap(const QString& stringRep);

/**
 * Make the given file or directory @p path absolute by expanding the '~' and
 * removing redundant '.' and '..'.
 */
void makePathAbsolute(QString& path);

/**
   @brief Checks if @p name is a valid name for actors, bounding boxes, sync
   controls or waypoint models. (e.g. "None" is not a valid name.)
 */
bool isValidObjectName(QString name);

/**
  Parse pressure levels string @p levels which should contain pressure levels
  either given by a range (e.g. "[0, 100, 10]") or by a list of values (e.g.
  "10, 20, 50, 100, 200, 500, 700, 900").
 */
QVector<float> parseFloatRangeString(QString levels);

/**
  Builds a string containing all values given by @p levels by separating the
  values by @p delimiter.
 */
QString listOfPressureLevelsAsString(QVector<float> levels, QString delimiter);

/**
  Combined relative and absolute float comparison. Be careful when using this
  method!

  The method has been taken from
  https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/

  Many details can be found on the webpage.
 */
bool floatIsAlmostEqualRelativeAndAbs(
        float floatA, float floatB,
        float maxDiff, float maxRelDiff = FLT_EPSILON);

/**
  Retrieve scale factor to convert given @param unit to hPa.
 */
float scaleFactorToHPa(const std::string &unit);

/**
  Get the list of supported pressure units
 */
QStringList getSupportedPressureUnits();

/**
  Get the list of supported length units
 */
QStringList getSupportedLengthUnits();


/******************************************************************************
***                            OVERLOADS                                    ***
*******************************************************************************/

// OStream implementation for QStrings. Allows for QString to be used
// in string stream operations. That means, for Met.3D, that QStrings can be
// logged in LOG4CPLUS macros, without manually converting them to std strings.
template<typename charT, typename traits>
std::basic_ostream<charT, traits> &operator<<(std::basic_ostream<charT, traits> &os, const QString &value)
{
    // do the insertion of p_oParagraph
    return os << value.toStdString();
}


// OStream implementation for QVector3D. Allows for QVector3D to be used
// in string stream operations. That means, for Met.3D, that QVector3Ds can be
// logged in LOG4CPLUS macros, without manually converting them to std strings.
template<typename charT, typename traits>
std::basic_ostream<charT, traits> &operator<<(std::basic_ostream<charT, traits> &os, const QVector3D &value)
{
    // do the insertion of p_oParagraph
    return os << "[" << value.x() << ", " << value.y() << ", " << value.z() << "]";
}


// OStream implementation for QPointF. Allows for QPointF to be used
// in string stream operations. That means, for Met.3D, that QPointF can be
// logged in LOG4CPLUS macros, without manually converting them to std strings.
template<typename charT, typename traits>
std::basic_ostream<charT, traits> &operator<<(std::basic_ostream<charT, traits> &os, const QPointF &value)
{
    // do the insertion of p_oParagraph
    return os << "[" << value.x() << ", " << value.y() << "]";
}


#endif // MUTIL_H
