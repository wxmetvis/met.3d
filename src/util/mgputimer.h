/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MGPUTIMER_H
#define MET_3D_MGPUTIMER_H

// standard library imports

// related third party imports

// local application imports
#include "mstopwatch.h"
#include "GL/glew.h"

namespace Met3D
{

/**
   @brief MGpuTimer implements a timer to measure the elapsed GPU time at
   different parts of the program.

   It can be used to measure the call time of a single OpenGL call
   or a sequence of multiple calls.
   Uses OpenGL queries to obtain time in nanosecond resolution.
 */
class MGpuTimer
{
public:
    /**
     * Constructs a new stopwatch.
     */
    MGpuTimer();

    ~MGpuTimer();

    /**
     * Start the timer.
     */
    void start();

    /**
     * Stop the timer. Might cause the CPU to hang up temporarily, as it waits synchronously on the gpu
     * to complete the calls done before this stop.
     */
    void stop();

    /**
     * Returns the time elapsed between the call to start() and stop().
     * If it is the first time you ran the timer, it might return 0, as OpenGL does that.
     *
     * @param units TimeUnits enum that specifies the units of the returned time value.
     * @return The time in the requested unit.
     */
    double getElapsedTime(MStopwatch::TimeUnits units) const;

private:
    GLuint64 elapsedTime;
    unsigned int queryID;
    bool running;
};

} // Met3D

#endif //MET_3D_MGPUTIMER_H
