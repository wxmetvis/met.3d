/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Thorwin Vogt
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/
// FSR defines
#define A_GPU 1
#define A_GLSL 1

// Include of FFX_A.h
#include "ffx-fsr/ffx_a.h"

// FSR feature activation
#define FSR_EASU_F 1
#define FSR_RCAS_F 1

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

// Constants used for FSR
AU4 con0;
AU4 con1;
AU4 con2;
AU4 con3;

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

// Textures and inputs
uniform vec4 sourceViewport; // Size of the input viewport
uniform vec4 sourceSize; // Size of the input image
uniform vec4 outputSize; // Size of the output image
uniform float sharpness; // Sharpness for RCAS, 0 is highest, 2 is effectively lowest, though it higher values are possible

layout(rgba32f, binding = 0) uniform image2D outputTexture;
uniform sampler2D inputSampler;

// Callbacks for FSR_EASU
AF4 FsrEasuRF(AF2 p)
{
    AF4 res = textureGather(inputSampler, p, 0);
    return res;
}

AF4 FsrEasuGF(AF2 p)
{
    AF4 res = textureGather(inputSampler, p, 1);
    return res;
}

AF4 FsrEasuBF(AF2 p)
{
    AF4 res = textureGather(inputSampler, p, 2);
    return res;
}

// Callbacks for FSR_RCAS
AF4 FsrRcasLoadF(ASU2 p)
{
    return texelFetch(inputSampler, ASU2(p), 0);
}
void FsrRcasInputF(inout AF1 r,inout AF1 g,inout AF1 b){}

// Include FSR proper
#include "ffx-fsr/ffx_fsr1.h"

/**
  Function using EASU to upscale image
  */
void easu(AU2 pos)
{
    AF3 outputColor = AF3(0, 0, 0);

    FsrEasuF(outputColor, pos, con0, con1, con2, con3);

    imageStore(outputTexture, ASU2(pos), AF4(outputColor, 1));
}

void rcas(AU2 pos)
{
    AF3 outputColor = AF3(0, 0, 0);

    FsrRcasF(outputColor.r, outputColor.g, outputColor.b, pos, con0);

    imageStore(outputTexture, ASU2(pos), AF4(outputColor, 1));
}

/*****************************************************************************
 ***                          COMPUTE SHADER
 *****************************************************************************/

/**
  Compute shader upscaling an input image using FSR 1.0
  */
shader CSFSREASU()
{
    FsrEasuCon(con0, con1, con2, con3, //FSR constants
               sourceViewport.x, sourceViewport.y, // Viewport resolution to upscale from
               sourceSize.x, sourceSize.y, // Input image size
               outputSize.x, outputSize.y); // output image size, resolution to upscale to.

    AU2 gxy = ARmp8x8(gl_LocalInvocationID.x) + AU2(gl_WorkGroupID.x << 4u, gl_WorkGroupID.y << 4u);

    easu(gxy);
    gxy.x += 8u;
    easu(gxy);
    gxy.y += 8u;
    easu(gxy);
    gxy.x -= 8u;
    easu(gxy);
}

/**
  Compute shader sharpening an input image using RCAS
  */
shader CSFSRRCAS()
{
    FsrRcasCon(con0, sharpness); // Second parameter is sharpness level, 0 is highest

    AU2 gxy = ARmp8x8(gl_LocalInvocationID.x) + AU2(gl_WorkGroupID.x << 4u, gl_WorkGroupID.y << 4u);

    rcas(gxy);
    gxy.x += 8u;
    rcas(gxy);
    gxy.y += 8u;
    rcas(gxy);
    gxy.x -= 8u;
    rcas(gxy);
}

/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

/**
  Shader programm to execute RCAS pass
  */
program RCAS_PASS
{
    cs(450)=CSFSRRCAS() : in(local_size_x = 64);
};

/**
  Shader programm to execute EASU pass (upscaler)
  */
program EASU_PASS
{
    cs(450)=CSFSREASU() : in(local_size_x = 64);
};
