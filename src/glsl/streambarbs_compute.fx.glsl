/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2016-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/

// include constants
#include "shared/msharedconstants.h"
#include "hsec_sample_data_routine.fx.glsl"

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/


/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform float       worldZ;     // world z coordinate of this section

// sample textures for u/v component of wind
uniform sampler3D   vectorDataLonComponent3D;
uniform sampler3D   vectorDataLatComponent3D;

uniform sampler2D   vectorDataLonComponent2D;
uniform sampler2D   vectorDataLatComponent2D;

uniform sampler2D   surfacePressureLon;    // surface pressure field in Pa
uniform sampler1D   hybridCoefficientsLon; // hybrid sigma pressure coefficients
uniform sampler2D   surfacePressureLat;    // surface pressure field in Pa
uniform sampler1D   hybridCoefficientsLat; // hybrid sigma pressure coefficients
uniform sampler3D   auxPressureFieldLon_hPa; // 3D pressure field in hPa
uniform sampler3D   auxPressureFieldLat_hPa; // 3D pressure field in hPa

//----------------STREAMLINES---------------------

uniform float   integrationStepSize; // Integration step size
uniform int     maxNumLineSegments; // maximal number of line segments
uniform int     maxNumLines; // maximal number of lines

uniform vec2    rangeLon;
uniform vec2    rangeLat;



// Shader buffer storage object of the init points.
layout (std430, binding=0) buffer InitPointBuffer
{
    vec2 initPoints[];
};

// Shader buffer storage object of computed streamline vertices.
layout (std430, binding=1) buffer StreamlineBuffer
{
    vec2 lines[];
};


/*****************************************************************************
 ***                             STREAMLINES
 *****************************************************************************/

vec2 windDirection(vec2 position)
{
    vec3 pos = vec3(position.x, position.y, worldZ);

    // Sample both wind textures to obtain wind speed in u and v direction.
    vec4 dirCorrForProjMaps;
    float windU = sampleDataAtPos(
                pos, vectorDataLonComponent3D, vectorDataLonComponent2D,
                surfacePressureLon, hybridCoefficientsLon,
                auxPressureFieldLon_hPa, dirCorrForProjMaps);
    float windV = sampleDataAtPos(
                pos, vectorDataLatComponent3D, vectorDataLatComponent2D,
                surfacePressureLat, hybridCoefficientsLat,
                auxPressureFieldLat_hPa, dirCorrForProjMaps);

    // Optain vector field direction at posWorld (e.g. wind).
    vec3 windMagnitude = vec3(windU, windV, 0);

    vec4 data = vec4(windMagnitude, 1).xyzw;
    if (applyVectorCorrectionForProjMaps)
    {
        // dirCorrForProjMaps.xy contains basis vector in eastward direction;
        // dirCorrForProjMaps.zw contains basis vector in northward direction
        data = vec4(windU * dirCorrForProjMaps.xy
        + windV * dirCorrForProjMaps.zw, 0, 1).xyzw;
        //DEBUG (mr, 14Jan2021) -- if the following line is uncommented all
        // vector glyphs should be parallel to parallels (equal latitude).
        //data = vec4(dirCorrForProjMaps.xy, 0, 1).xyzw;
        //DEBUG (mr, 14Jan2021) -- if the following line is uncommented all
        // vector glyphs should be parallel to meridians (equal longitude).
        //data = vec4(dirCorrForProjMaps.zw, 0, 1).xyzw;
    }

    vec2 dir = normalize(data.xy);

    return dir;
}


vec2 rungeKutta4Integration(vec2 currentPos, int direction)
{
    if (currentPos.x <= rangeLon.x)
    {
        currentPos = vec2(rangeLon.x + 0.01, currentPos.y);
    }
    if (currentPos.x >= rangeLon.y)
    {
        currentPos = vec2(rangeLon.y - 0.01, currentPos.y);
    }
    if (currentPos.y <= rangeLat.x)
    {
        currentPos = vec2(currentPos.x, rangeLat.x + 0.01);
    }
    if (currentPos.y >= rangeLat.y)
    {
        currentPos = vec2(currentPos.x, rangeLat.y - 0.01);
    }

    vec2 p0 = windDirection(currentPos) * integrationStepSize * direction;
    vec2 p1 = windDirection(currentPos + (p0 / 2)) * integrationStepSize * direction;
    vec2 p2 = windDirection(currentPos + (p1 / 2)) * integrationStepSize * direction;
    vec2 p3 = windDirection(currentPos + p2) * integrationStepSize * direction;
    vec2 point = currentPos + (p0 / 6) + (p1 / 3) + (p2 / 3) + (p3 / 6);

    return point;
}


bool checkIfMissingvalue(vec2 point)
{
    vec3 pos = vec3(point.x, point.y, worldZ);

    // Sample both wind textures to obtain wind speed in u and v direction.
    vec4 dirCorrForProjMaps;
    float windU = sampleDataAtPos(
                pos, vectorDataLonComponent3D, vectorDataLonComponent2D,
                surfacePressureLon, hybridCoefficientsLon,
                auxPressureFieldLon_hPa, dirCorrForProjMaps);
    float windV = sampleDataAtPos(
                pos, vectorDataLatComponent3D, vectorDataLatComponent2D,
                surfacePressureLat, hybridCoefficientsLat,
                auxPressureFieldLat_hPa, dirCorrForProjMaps);

    if (windU == M_MISSING_VALUE || windV == M_MISSING_VALUE)
    {
        return true;
    }
    return false;
}


bool checkOutsideDataField(vec2 point)
{
    if (point.x <= rangeLon.x)
    {
        return true;
    }
    if (point.x >= rangeLon.y)
    {
        return true;
    }
    if (point.y <= rangeLat.x)
    {
        return true;
    }
    if (point.y >= rangeLat.y)
    {
        return true;
    }
    return false;
}


void rungeKutta(uint initPointIndex)
{
    // Compute index in normal curve line buffer.
    uint index = initPointIndex * (maxNumLineSegments + 2);

    lines[index] = initPoints[initPointIndex];
    vec2 current = initPoints[initPointIndex];

    for(int i = 1; i <= maxNumLineSegments; ++i)
    {
        vec2 point = rungeKutta4Integration(current, 1);

        if (checkOutsideDataField(point) || checkIfMissingvalue(current))
        {
            break;
        }
        else
        {
            lines[index + i] = point;
            current = point;
        }
    }
}


void rungeKuttaDouble(uint initPointIndex)
{
    // Compute index in normal curve line buffer.
    uint index = initPointIndex * (maxNumLineSegments + 2);

    vec2 current = initPoints[initPointIndex];

    for(int i = 1; i < maxNumLineSegments / 2; ++i)
    {
        vec2 point = rungeKutta4Integration(current, -1);

        if (checkOutsideDataField(point) || checkIfMissingvalue(current))
        {
            break;
        }
        else
        {
            lines[index + (maxNumLineSegments / 2) - i] = point;
            current = point;
        }
    }

    current = initPoints[initPointIndex];

    for(int i = 1; i <= maxNumLineSegments / 2; ++i)
    {
        vec2 point = rungeKutta4Integration(current, 1);

        if (checkOutsideDataField(point) || checkIfMissingvalue(current))
        {
            break;
        }
        else
        {
            lines[index + (maxNumLineSegments / 2) + i] = point;
            current = point;
        }
    }

    lines[index + (maxNumLineSegments / 2)] = initPoints[initPointIndex];
}


/*****************************************************************************
 ***                          COMPUTE SHADER
 *****************************************************************************/

shader CSstreamlineIntegration()
{
    uint initPointIndex = gl_GlobalInvocationID.x;

    if (initPointIndex >= maxNumLines)
    {
        return;
    }

    rungeKutta(initPointIndex);
}


// Shader that integrates in both directions.
shader CSdoubleLineIntegration()
{
    uint initPointIndex = gl_GlobalInvocationID.x;

    if (initPointIndex >= maxNumLines)
    {
        return;
    }

    rungeKuttaDouble(initPointIndex);
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program Standard
{
    //cs(430)=CSstreamlineIntegration() : in(local_size_x = 128);
    cs(430)=CSdoubleLineIntegration() : in(local_size_x = 128);
};


