/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Susanne Fuchs
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/

#include "shared/msharedconstants.h"
#include "radar_utils.fx.glsl"
#include "includes/lighting.glsl"

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/


/*****************************************************************************
 ***                          UTILITY FUNCTIONS
 *****************************************************************************/


/*****************************************************************************
 ***                             INTERFACES
 *****************************************************************************/

interface GStoFS
{
    smooth float scalar; // data value of the fragment.
    smooth float flag; // negative if fragment should be discarded.
    smooth float undetect; // positive if fragment should get "undetect" color.
    smooth vec3 worldPos; // World space position of the fragment.
};

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform mat4 mvpMatrix; // Model-view-projection matrix
uniform vec2 pToWorldZParams; // Parameters to convert p[hPa] to world z

uniform int elIdx; // Index of the currently rendered elevation
uniform sampler2D dataField2D; // 2D texture holding the scalar data

// Minimum and maximum azimuth and pressure values.
uniform float minAz;
uniform float maxAz;
uniform float vSectionAz; // Azimuth angle for vertical profile mode.

// Height/pressure variables.
uniform bool selectAltitude;
uniform float min_hPa;
uniform float max_hPa;
uniform int nbinsH; // Maximum number of range bins
uniform int nraysH; // Number of azimuth angles in horizontal section
uniform sampler3D lonLatPresField; // Holds lon, lat and pressure (used for horizontal section)

// Transfer function and color.
uniform sampler1D transferFunction; // 1D transfer function
uniform float scalarMinimum; // Min data values to scale to 0..1
uniform float scalarMaximum; // Max data values to scale to 0..1
uniform bool colourUndetectEnabled; // Use background colour for undetected values.
uniform float undetectValue; // Data value meaning no radar signal was detected.
uniform vec4 undetectColour; // Background colour for undetected values.

/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

shader VSmain()
{
    // Pass vertex and instance ID to the geometry shader:
    // gl_InstanceID = bin index, gl_VertexID = azimuth index.
    gl_Position = vec4(gl_InstanceID, gl_VertexID, 0, 0);
}


/*****************************************************************************
 ***                          GEOMETRY SHADER
 *****************************************************************************/

shader GS2D(out GStoFS outStruct)
{
    ivec2 ij = ivec2(gl_in[0].gl_Position.xy);

    int nrays = int(radarMetadata[elIdx].nrays); // Number of azimuth angles in the data.
    int nbins = int(radarMetadata[elIdx].nbins); // Number of range bins in the data.

    // Define grid cell index in azimuth and bin directions, considering if
    // azimuth/bin index starts not at 0.
    int azOffset = int(floor(minAz));
    int binOffset = 0;
    int azIdx = (ij.y + azOffset) % nrays;
    int binIdx = ij.x + binOffset;

    // Azimuth angles.
    float az_left, az_right;

    az_left = float(azIdx);
    az_right = float((azIdx+1)); // modulo nrays is applied after comparison with maxAz.

    float rangeInner, rangeOuter;

    float binDistance = radarMetadata[elIdx].binDistance; // In metres.

    rangeInner = radarMetadata[elIdx].rangeStart + binDistance * float(binIdx);
    rangeOuter = radarMetadata[elIdx].rangeStart + binDistance * float(binIdx+1);

    float elAngle =radarMetadata[elIdx].elevationAngleDegrees; // In degrees.


    // The scalar value is the same for all emitted vertices.
    outStruct.scalar = texelFetch(dataField2D, ivec2(binIdx, azIdx), 0).r;

    // set "undetect" if no radar echo is presented.
    outStruct.undetect = -100.;
    if (floatIsAlmostEqual(outStruct.scalar, undetectValue, 1.e-3f))
    {
        outStruct.undetect = 1.0;
    }

    // Discard grid cell if variable value is missing.
    outStruct.flag = 1.0;
    if (outStruct.scalar == M_MISSING_VALUE)
    {
        outStruct.flag = -100.0;
    }

    // Discard cell if it isn't inside the azimuth angle interval.
    bool isInAzInterval = false;
    if (minAz <= maxAz)
    {
        isInAzInterval = (minAz <= az_left && az_right <= maxAz);
    }
    else
    {
        // When interval crosses 0° line.
        isInAzInterval = (minAz <= az_left || az_right <= maxAz);
    }
    if (! isInAzInterval)
    {
        outStruct.flag = -100.0;
    }

    //Define 4 vertices.
    // Inner left vertex
    vec3 pos_00 = radarToLonLatPressure(elAngle, az_left, rangeInner);
    // Inner right vertex
    vec3 pos_01 = radarToLonLatPressure(elAngle, az_right, rangeInner);
    float pres_lower = pos_01.z;
    // Outer left vertex
    vec3 pos_10 = radarToLonLatPressure(elAngle, az_left, rangeOuter);
    // Outer right vertex
    vec3 pos_11 = radarToLonLatPressure(elAngle, az_right, rangeOuter);
    float pres_upper = pos_11.z;

    // Discard cell if it doesn't overlap with min-max pressure interval.
    if (selectAltitude && (pres_upper > max_hPa || pres_lower < min_hPa))
    {
        outStruct.flag = -100.;
    }

    pos_11.z = (log(pos_11.z) - pToWorldZParams.x) * pToWorldZParams.y;
    pos_10.z = (log(pos_10.z) - pToWorldZParams.x) * pToWorldZParams.y;
    pos_01.z = (log(pos_01.z) - pToWorldZParams.x) * pToWorldZParams.y;
    pos_00.z = (log(pos_00.z) - pToWorldZParams.x) * pToWorldZParams.y;

    //Emit vertices.
    gl_Position = mvpMatrix * vec4(pos_11, 1.);
    outStruct.worldPos = pos_11;
    EmitVertex();

    gl_Position = mvpMatrix * vec4(pos_10, 1.);
    outStruct.worldPos = pos_10;
    EmitVertex();

    gl_Position = mvpMatrix * vec4(pos_01, 1.);
    outStruct.worldPos = pos_01;
    EmitVertex();

    gl_Position = mvpMatrix * vec4(pos_00, 1.);
    outStruct.worldPos = pos_00;
    EmitVertex();
}


shader GS3D(out GStoFS outStruct)
{
    ivec2 ij = ivec2(gl_in[0].gl_Position.xy);

    int nrays = int(radarMetadata[elIdx].nrays); // Number of azimuth angles in the data.
    int nbins = int(radarMetadata[elIdx].nbins);

    // Define grid cell index in azimuth and bin directions, considering if azimuth/bin index starts not at 0.
    int azOffset = int(floor(minAz));
    int binOffset = 0;
    int binIdx = ij.x + binOffset;
    int azIdx = (ij.y + azOffset) % nrays;

    // Azimuth angles.
    float az_left, az_right;

    az_left = float(azIdx);
    az_right = float((azIdx+1)); // %nrays is applied after comparison with maxAz.

    // Range distance along the ray from the radar.
    float rangeInner, rangeOuter;

    float binDistance = radarMetadata[elIdx].binDistance; // In metres.

    rangeInner = radarMetadata[elIdx].rangeStart + binDistance * float(binIdx);
    rangeOuter = radarMetadata[elIdx].rangeStart + binDistance * float(binIdx+1);

    // Elevation angle (between the radar ray and the ground).
    float elAngle =radarMetadata[elIdx].elevationAngleDegrees; // In degrees.
    float verticalBeamwidth = radarMetadata[elIdx].verticalBeamwidth; // In degrees.

    // Lower and upper elevation of the grid cell.
    float el_lower, el_upper;

    el_lower = elAngle - verticalBeamwidth/2.0;
    el_upper = elAngle + verticalBeamwidth/2.0;

    // The scalar value is the same for all emitted vertices.
    outStruct.scalar = texelFetch(dataField2D, ivec2(binIdx, azIdx), 0).r;

    // set "undetect" if no radar echo is presented.
    outStruct.undetect = -100.;
    if (floatIsAlmostEqual(outStruct.scalar, undetectValue, 1.e-3f))
    {
        outStruct.undetect = 1.0;
    }

    // Discard grid cell if variable value is missing.
    outStruct.flag = 1.0;
    if (outStruct.scalar == M_MISSING_VALUE)
    {
        outStruct.flag = -100.0;
    }

    // Discard cell if it isn't inside the azimuth angle interval.
    bool isInAzInterval = false;
    if (minAz <= maxAz)
    {
        isInAzInterval = (minAz <= az_left && az_right <= maxAz);
    }
    else
    {
        // When interval crosses 0° line.
        isInAzInterval = (minAz <= az_left || az_right <= maxAz);
    }
    if (! isInAzInterval)
    {
        outStruct.flag = -100.0;
    }

    //Define 8 vertices of a cube:
    // lower elevation, left azimuth angle, inner bin
    vec3 pos_000 = radarToLonLatPressure(el_lower, az_left, rangeInner);
    float pres_lower = pos_000.z;
    // Convert to world space.
    pos_000.z = (log(pos_000.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // lower elevation, right azimuth angle, inner bin
    vec3 pos_001 = radarToLonLatPressure(el_lower, az_right, rangeInner);
    pos_001.z = (log(pos_001.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // lower elevation, left azimuth angle, outer bin
    vec3 pos_010 = radarToLonLatPressure(el_lower, az_left, rangeOuter);
    pos_010.z = (log(pos_010.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // lower elevation, right azimuth angle, outer bin
    vec3 pos_011 = radarToLonLatPressure(el_lower, az_right, rangeOuter);
    pos_011.z = (log(pos_011.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // upper elevation, left azimuth angle, inner bin
    vec3 pos_100 = radarToLonLatPressure(el_upper, az_left, rangeInner);
    pos_100.z = (log(pos_100.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // upper elevation, right azimuth angle, inner bin
    vec3 pos_101 = radarToLonLatPressure(el_upper, az_right, rangeInner);
    pos_101.z = (log(pos_101.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // upper elevation, left azimuth angle, outer bin
    vec3 pos_110 = radarToLonLatPressure(el_upper, az_left, rangeOuter);
    pos_110.z = (log(pos_110.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // upper elevation, right azimuth angle, outer bin
    vec3 pos_111 = radarToLonLatPressure(el_upper, az_right, rangeOuter);
    float pres_upper = pos_111.z;
    pos_111.z = (log(pos_111.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // Not shown if cell doesn't overlap with min-max interval.
    if (selectAltitude && (pres_upper > max_hPa || pres_lower < min_hPa))
    {
        outStruct.flag = -100.;
    }

    // Emit four vertices for each of the six sides of the cell.
    // TOP SIDE
    // 7 Upper outer right vertex
    gl_Position = mvpMatrix * vec4(pos_111, 1.);
    outStruct.worldPos = pos_111;
    EmitVertex();

    // 6 Upper outer left vertex
    gl_Position = mvpMatrix * vec4(pos_110, 1.);
    outStruct.worldPos = pos_110;
    EmitVertex();

    // 5 Upper inner right vertex
    gl_Position = mvpMatrix * vec4(pos_101, 1.);
    outStruct.worldPos = pos_101;
    EmitVertex();

    // 4 Upper inner left vertex
    gl_Position = mvpMatrix * vec4(pos_100, 1.);
    outStruct.worldPos = pos_100;
    EmitVertex();

    EndPrimitive();


    // BOTTOM SIDE
    // 1 Lower inner right vertex
    gl_Position = mvpMatrix * vec4(pos_001, 1.);
    outStruct.worldPos = pos_001;
    EmitVertex();

    // 0 Lower inner left vertex
    gl_Position = mvpMatrix * vec4(pos_000, 1.);
    outStruct.worldPos = pos_000;
    EmitVertex();

    // 3 Lower outer right vertex
    gl_Position = mvpMatrix * vec4(pos_011, 1.);
    outStruct.worldPos = pos_011;
    EmitVertex();

    // 2 Lower outer left vertex
    gl_Position = mvpMatrix * vec4(pos_010, 1.);
    outStruct.worldPos = pos_010;
    EmitVertex();

    EndPrimitive();


    // INNER SIDE
    // 5 Upper inner right vertex
    gl_Position = mvpMatrix * vec4(pos_101, 1.);
    outStruct.worldPos = pos_101;
    EmitVertex();

    // 4 Upper inner left vertex
    gl_Position = mvpMatrix * vec4(pos_100, 1.);
    outStruct.worldPos = pos_100;
    EmitVertex();

    // 1 Lower inner right vertex
    gl_Position = mvpMatrix * vec4(pos_001, 1.);
    outStruct.worldPos = pos_001;
    EmitVertex();

    // 0 Lower inner left vertex
    gl_Position = mvpMatrix * vec4(pos_000, 1.);
    outStruct.worldPos = pos_000;
    EmitVertex();

    EndPrimitive();


    // OUTER SIDE
    // 6 Upper outer left vertex
    gl_Position = mvpMatrix * vec4(pos_110, 1.);
    outStruct.worldPos = pos_110;
    EmitVertex();

    // 7 Upper outer right vertex
    gl_Position = mvpMatrix * vec4(pos_111, 1.);
    outStruct.worldPos = pos_111;
    EmitVertex();

    // 2 Lower outer left vertex
    gl_Position = mvpMatrix * vec4(pos_010, 1.);
    outStruct.worldPos = pos_010;
    EmitVertex();

    // 3 Lower outer right vertex
    gl_Position = mvpMatrix * vec4(pos_011, 1.);
    outStruct.worldPos = pos_011;
    EmitVertex();

    EndPrimitive();


    // LEFT SIDE
    // 4 Upper inner left vertex
    gl_Position = mvpMatrix * vec4(pos_100, 1.);
    outStruct.worldPos = pos_100;
    EmitVertex();

    // 6 Upper outer left vertex
    gl_Position = mvpMatrix * vec4(pos_110, 1.);
    outStruct.worldPos = pos_110;
    EmitVertex();

    // 0 Lower inner left vertex
    gl_Position = mvpMatrix * vec4(pos_000, 1.);
    outStruct.worldPos = pos_000;
    EmitVertex();

    // 3 Lower outer left vertex
    gl_Position = mvpMatrix * vec4(pos_011, 1.);
    outStruct.worldPos = pos_011;
    EmitVertex();

    EndPrimitive();


    //RIGHT SIDE
    // 7 Upper outer right vertex
    gl_Position = mvpMatrix * vec4(pos_111, 1.);
    outStruct.worldPos = pos_111;
    EmitVertex();

    // 5 Upper inner right vertex
    gl_Position = mvpMatrix * vec4(pos_101, 1.);
    outStruct.worldPos = pos_101;
    EmitVertex();

    // 3 Lower outer right vertex
    gl_Position = mvpMatrix * vec4(pos_011, 1.);
    outStruct.worldPos = pos_011;
    EmitVertex();

    // 1 Lower inner right vertex
    gl_Position = mvpMatrix * vec4(pos_001, 1.);
    outStruct.worldPos = pos_001;
    EmitVertex();

    EndPrimitive();
}


shader GSvsection(out GStoFS outStruct)
{
    // Number of azimuth angles in the data.
    int nrays = int(radarMetadata[elIdx].nrays);

    int binIdx = int(gl_in[0].gl_Position.x);
    int azIdx = int(vSectionAz);

    // Elevation angle: lower and upper end of grid cell.
    float el_lower, el_upper;

    float elAngle =radarMetadata[elIdx].elevationAngleDegrees; // In degrees.
    float verticalBeamwidth = radarMetadata[elIdx].verticalBeamwidth; // In degrees.
    el_lower = elAngle - verticalBeamwidth/2.0;
    el_upper = elAngle + verticalBeamwidth/2.0;

    // Range along the radar ray.
    float rangeInner, rangeOuter;
    float binDistance = radarMetadata[elIdx].binDistance; // In metres.
    rangeInner = radarMetadata[elIdx].rangeStart + binDistance * float(binIdx);
    rangeOuter = radarMetadata[elIdx].rangeStart + binDistance * float(binIdx+1);

    // The scalar value is the same for all emitted vertices.
    outStruct.scalar = texelFetch(dataField2D, ivec2(binIdx, azIdx), 0).r;

    // set "undetect" if no radar echo is presented.
    outStruct.undetect = -100.;
    if (floatIsAlmostEqual(outStruct.scalar, undetectValue, 1.e-3f))
    {
        outStruct.undetect = 1.0;
    }

    // Discard grid cell if variable value is missing.
    outStruct.flag = 1.;
    if (outStruct.scalar == M_MISSING_VALUE)
    {
        outStruct.flag = -100.;
    }

    // Lower inner vertex.
    vec3 pos_00 = radarToLonLatPressure(el_lower, vSectionAz, rangeInner);
    float pres_lower = pos_00.z;

    // Lower outer vertex.
    vec3 pos_01 = radarToLonLatPressure(el_lower, vSectionAz, rangeOuter);

    // Upper inner vertex.
    vec3 pos_10 = radarToLonLatPressure(el_upper, vSectionAz, rangeInner);

    // Upper outer vertex.
    vec3 pos_11 = radarToLonLatPressure(el_upper, vSectionAz, rangeOuter);
    float pres_upper = pos_11.z;

    // Not shown if cell doesn't overlap with min-max interval.
    if (pres_upper > max_hPa || pres_lower < min_hPa)
    {
        outStruct.flag = -100.;
    }

    pos_00.z = (log(pos_00.z) - pToWorldZParams.x) * pToWorldZParams.y;
    pos_01.z = (log(pos_01.z) - pToWorldZParams.x) * pToWorldZParams.y;
    pos_10.z = (log(pos_10.z) - pToWorldZParams.x) * pToWorldZParams.y;
    pos_11.z = (log(pos_11.z) - pToWorldZParams.x) * pToWorldZParams.y;

    // Emit 4 vertices.
    // Lower inner vertex.
    gl_Position = mvpMatrix * vec4(pos_00, 1.);
    outStruct.worldPos = pos_00;
    EmitVertex();

    // Lower outer vertex.
    gl_Position = mvpMatrix * vec4(pos_01, 1.);
    outStruct.worldPos = pos_01;
    EmitVertex();

    // Upper inner vertex.
    gl_Position = mvpMatrix * vec4(pos_10, 1.);
    outStruct.worldPos = pos_10;
    EmitVertex();

    // Upper outer vertex.
    gl_Position = mvpMatrix * vec4(pos_11, 1.);
    outStruct.worldPos = pos_11;
    EmitVertex();
}


shader GShsection(out GStoFS outStruct)
{
    ivec2 ij = ivec2(gl_in[0].gl_Position.xy);
    int binIdx = ij.x;
    int azIdx = ij.y;

    // set data value.
    outStruct.scalar = texelFetch(dataField2D, ivec2(azIdx, binIdx), 0).r;

    // set "undetect" if no radar echo is presented.
    outStruct.undetect = -100.;
    if (floatIsAlmostEqual(outStruct.scalar, undetectValue, 1.e-3f))
    {
        outStruct.undetect = 1.0;
    }

    // set vertex coordinates:
    // 00: this bin, this azimuth angle
    // 01: this bin, next azimuth angle
    // 10: next bin, this azimuth angle
    // 11: next bin, next azimuth angle
    // minus10: last bin, this azimuth angle
    // minus11: last bin, next azimuth angle

    float lon_00 = texelFetch(lonLatPresField, ivec3(0, azIdx, binIdx), 0).r;
    float lat_00 = texelFetch(lonLatPresField, ivec3(1, azIdx, binIdx), 0).r;
    float pres = texelFetch(lonLatPresField, ivec3(2, azIdx, binIdx), 0).r;
    float worldZ = (log(pres) - pToWorldZParams.x) * pToWorldZParams.y;

    int azIdxRight = (azIdx + 1) % nraysH;

    float lon_01 = texelFetch(lonLatPresField, ivec3(0, azIdxRight, binIdx), 0).r;
    float lat_01 = texelFetch(lonLatPresField, ivec3(1, azIdxRight, binIdx), 0).r;

    int binIdxOuter = binIdx + 1;
    if (binIdxOuter >= nbinsH)
    {
        binIdxOuter = binIdx;
    }

    float lon_10 = texelFetch(lonLatPresField, ivec3(0, azIdx, binIdxOuter), 0).r;
    float lat_10 = texelFetch(lonLatPresField, ivec3(1, azIdx, binIdxOuter), 0).r;

    float lon_11 = texelFetch(lonLatPresField, ivec3(0, azIdxRight, binIdxOuter), 0).r;
    float lat_11 = texelFetch(lonLatPresField, ivec3(1, azIdxRight, binIdxOuter), 0).r;

    int binIdxInner = binIdx - 1;
    if (binIdxInner < 0)
    {
        binIdxInner = 0;
    }

    float lon_minus10 = texelFetch(lonLatPresField, ivec3(0, azIdx, binIdxInner), 0).r;
    float lat_minus10 = texelFetch(lonLatPresField, ivec3(1, azIdx, binIdxInner), 0).r;

    float lon_minus11 = texelFetch(lonLatPresField, ivec3(0, azIdxRight, binIdxInner), 0).r;
    float lat_minus11 = texelFetch(lonLatPresField, ivec3(1, azIdxRight, binIdxInner), 0).r;


    float az_left = float(azIdx);
    float az_right = float((azIdx + 1)); // without '% nraysH' to handle maxAz=360°.

    // Discard grid cell if variable value is missing.
    outStruct.flag = 1.0;
    if (outStruct.scalar == M_MISSING_VALUE)
    {
        outStruct.flag = -100.0;
    }

    // Discard cell if it isn't inside the azimuth angle interval.
    bool isInAzInterval = false;
    if (minAz <= maxAz)
    {
        isInAzInterval = (minAz <= az_left && az_right <= maxAz);
    }
    else
    {
        // When interval crosses 0° line.
        isInAzInterval = (minAz <= az_left || az_right <= maxAz);
    }
    if (! isInAzInterval)
    {
        outStruct.flag = -100.0;
    }

    vec3 worldSpacePosition = vec3(0,0,0);

    // Outer right vertex
    worldSpacePosition = vec3((lon_01+lon_11)/2.0,
                              (lat_01+lat_11)/2.0,
                              worldZ);
    gl_Position             = mvpMatrix * vec4(worldSpacePosition, 1.);
    outStruct.worldPos = worldSpacePosition;
    EmitVertex();

    // Outer left vertex
    worldSpacePosition = vec3((lon_00+lon_10)/2.0,
                              (lat_00+lat_10)/2.0,
                              worldZ);
    gl_Position             = mvpMatrix * vec4(worldSpacePosition, 1.);
    outStruct.worldPos = worldSpacePosition;
    EmitVertex();

    // Inner right vertex
    worldSpacePosition = vec3((lon_01+lon_minus11)/2.0,
                              (lat_01+lat_minus11)/2.0,
                              worldZ);
    gl_Position             = mvpMatrix * vec4(worldSpacePosition, 1.);
    outStruct.worldPos = worldSpacePosition;
    EmitVertex();

    // Inner left vertex
    worldSpacePosition = vec3((lon_00+lon_minus10)/2.0,
                              (lat_00+lat_minus10)/2.0,
                              worldZ);
    gl_Position             = mvpMatrix * vec4(worldSpacePosition, 1.);
    outStruct.worldPos = worldSpacePosition;
    EmitVertex();
}


/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

shader FSmain(in GStoFS inStruct, out vec4 fragColour)
{
    // Discard the element if it was flagged to be discarded in the GS.
    if (inStruct.flag < 0.0)
    {
        discard;
    }

    // Scale the scalar range to 0..1.
    float scalar_ = (inStruct.scalar - scalarMinimum) / (scalarMaximum - scalarMinimum);

    if (inStruct.undetect > 0.0 || scalar_ < 0.0 || scalar_ > 1.0)
    {
        if (colourUndetectEnabled)
        {
             fragColour = undetectColour;
        }
        else
        {
            discard;
        }
    }
    else
    {
        // Fetch colour from the transfer function and apply shading term.
        fragColour = texture(transferFunction, scalar_);
    }

    vec3 normal = normalize(cross(dFdx(inStruct.worldPos), dFdy(inStruct.worldPos)));

    fragColour.rgb *= getShadow(inStruct.worldPos, normal);
}

/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program Standard
{
    vs(430)=VSmain();
    gs(430)=GS2D() : in(points), out(triangle_strip, max_vertices = 4);
    fs(430)=FSmain();
};

program PseudoColours2D
{
    vs(430)=VSmain();
    gs(430)=GS2D() : in(points), out(triangle_strip, max_vertices = 4);
    fs(430)=FSmain();
};

program PseudoColours3D
{
    vs(430)=VSmain();
    gs(430)=GS3D() : in(points), out(triangle_strip, max_vertices = 24);
    fs(430)=FSmain();
};

program PseudoColoursVerticalSection
{
    vs(430)=VSmain();
    gs(430)=GSvsection() : in(points), out(triangle_strip, max_vertices = 4);
    fs(430)=FSmain();
};

program PseudoColoursHorizontalSection
{
    vs(430)=VSmain();
    gs(430)=GShsection() : in(points), out(triangle_strip, max_vertices = 4);
    fs(430)=FSmain();
};
