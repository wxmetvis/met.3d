/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**  Copyright 2015 Michael Kern
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

// - This file contains multiple functions to shade and render isosurface on volumes
// - required files:
//     o volume_sample_utils.glsl
// - required uniforms:
//
// uniform int     bisectionSteps;
// uniform bool    isoEnables[MAX_ISOSURFACES];
// uniform float   isoValues[MAX_ISOSURFACES];
// uniform vec4    isoColors[MAX_ISOSURFACES];
// uniform int     isoColorModes[MAX_ISOSURFACES];
//
// and all uniforms from volume_sample_utils.glsl

// Compute the isosurface color according to the given options
vec4 getSurfaceColor(in vec3 rayPosition,
                     in vec3 gradient,
                     in float scalar, // scalar value (for transfer function)
                     in int i) // isovalue index
{
    // if isovalue not enabled, skip this isosurface and return color zero
    if(!isoEnables[i])
    {
        return vec4(0);
    }

    if (isoColorModes[i] == TRANSFER_FUNC_COLOUR)
    {
        vec3 diffuseColor = vec3(0.);
        // If tfMaximum == tfMinimum skip sampling of transfer texture.
        // This is used by the CPU code to signal that no valid transfer
        // function can be bound.
        if (dataExtent.tfMaximum != dataExtent.tfMinimum)
        {
            float t = (scalar - dataExtent.tfMinimum)
                          / (dataExtent.tfMaximum - dataExtent.tfMinimum);
            diffuseColor = texture(transferFunction, t).rgb;
        }
        return vec4(diffuseColor, isoColors[i].a);
    }
    else if (isoColorModes[i] == TRANSFER_FUNC_SHADINGVAR)
    {
        float scalarShV = sampleShadingDataAtPos(rayPosition);
        vec3 diffuseColor = vec3(0.);
        if (dataExtentShV.tfMaximum != dataExtentShV.tfMinimum)
        {
            float t = (scalarShV - dataExtentShV.tfMinimum)
                          / (dataExtentShV.tfMaximum - dataExtentShV.tfMinimum);
            diffuseColor = texture(transferFunctionShV, t).rgb;
        }
        return vec4(diffuseColor, isoColors[i].a);
    }
    else if (isoColorModes[i] == TRANSFER_FUNC_SHADINGVAR_MAXNB)
    {
        float scalarShV = sampleShadingDataAtPos_maxNeighbour(rayPosition);
        vec3 diffuseColor = vec3(0.);
        if (dataExtentShV.tfMaximum != dataExtentShV.tfMinimum)
        {
            float t = (scalarShV - dataExtentShV.tfMinimum)
                          / (dataExtentShV.tfMaximum - dataExtentShV.tfMinimum);
            diffuseColor = texture(transferFunctionShV, t).rgb;
        }
        return vec4(diffuseColor, isoColors[i].a);
    }
    else
    {
        return isoColors[i];
    }
}


vec4 blendIsoSurfaces(in int crossingLevelFront, in int crossingLevelBack,
                      in vec3 rayPosition, in vec3 gradient, in float scalar)
{
    vec4 blendColor = vec4(0);

    const bool invertedCrossing = crossingLevelFront < crossingLevelBack;

    vec4 surfaceColor = vec4(0);

    if (invertedCrossing)
    {
        for (int i = crossingLevelBack - 1; i >= crossingLevelFront; --i)
        {
            surfaceColor = getSurfaceColor(rayPosition, gradient, scalar, i);

            blendColor.rgb += (1 - blendColor.a) * surfaceColor.a * surfaceColor.rgb;
            blendColor.a += (1 - blendColor.a) * surfaceColor.a;
        }
    }
    else
    {
        for (int i = crossingLevelBack; i < crossingLevelFront; ++i)
        {
            surfaceColor = getSurfaceColor(rayPosition, gradient, scalar, i);

            blendColor.rgb += (1 - blendColor.a) * surfaceColor.a * surfaceColor.rgb;
            blendColor.a += (1 - blendColor.a) * surfaceColor.a;
        }
    }

    return blendColor;
}

