This directory contains Fidelity FX FSR 1.0 header files.
They are published by GPUOpen under MIT License.
The files are published on GitHub: [FidelityFX-FSR](https://github.com/GPUOpen-Effects/FidelityFX-FSR)

At the time of writing, AMD already published FSR 1.1 as part of the 
[FidelityFX-SDK](https://github.com/GPUOpen-LibrariesAndSDKs/FidelityFX-SDK).
This is, however, only compatible with vulkan, while version 1.0 can be used with OpenGL
without too much effort.