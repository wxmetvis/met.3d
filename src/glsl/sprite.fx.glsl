/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Luka Elwart
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

// MVP matrix for world space to clip space transformation.
uniform mat4 mvMatrix;
uniform mat4 pMatrix;

// Sprite texture.
layout(binding=0) uniform sampler2D spriteTex;

// The position of the sprite. Either in world space or clip space coordinates.
uniform vec3 anchorPos;

// The size of the sprite in clip space coordinates.
uniform vec2 size;

// The opacity of the sprite.
uniform float opacity;

/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

// Shader for world space sprites.
// The world space position is defined in the uniform anchorPos.
shader VSSprite(in vec4 vertex : 0, out vec2 uv)
{
    // vertex is already in clip space coordinates, as the sprite geometry is
    // defined as.
    // -0.5,0.5 ----- 0.5,0.5
    //     |             |
    //     |     0,0     |
    //     |             |
    // -0.5,-0.5 ---- 0.5,-0.5
    // That way, we can keep the same geometry for all sprites, and just apply
    // scale and position in the shader, which might be dependent on the scene.

    // Calculate view position.
    vec4 p = mvMatrix * vec4(anchorPos, 1.0);

    // Scale sprite and multiply with z to keep viewport size uniform,
    // regardless of camera distance.
    p.xy += vertex.xy * size * p.z;

    // Project to clip space.
    p = pMatrix * p;

    gl_Position = vec4(p.xy, -1.0, p.w);

    uv = vertex.zw;
}

shader VSClipSprite(in vec4 vertex : 0, out vec2 uv)
{
    // vertex is already in cip space coordinates, as the sprite geometry is
    // defined as.
    // -0.5,0.5 ----- 0.5,0.5
    //     |             |
    //     |     0,0     |
    //     |             |
    // -0.5,-0.5 ---- 0.5,-0.5
    // That way, we can keep the same geometry for all sprites, and just apply
    // scale and position in the shader, which might be dependent on the scene.

    vec2 p = vec2(vertex.xy * size);
    p += anchorPos.xy;

    gl_Position = vec4(p.xy, anchorPos.z, 1.0f);

    uv = vertex.zw;
}

/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

shader FSSprite(in vec2 uv, out vec4 fragColor)
{
    fragColor = texture(spriteTex, uv);
    fragColor.a *= opacity;

    // for fully transparent pixels skip writing to the depth buffer as well
    // (otherwise we get ugly cut-out artifacts with the volumetric ray caster)
    if(fragColor.a <= 0.f) { discard; }
}

/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program Sprite
{
    vs(420)=VSSprite();
    fs(420)=FSSprite();
};

program UISprite
{
    vs(420)=VSClipSprite();
    fs(420)=FSSprite();
};
