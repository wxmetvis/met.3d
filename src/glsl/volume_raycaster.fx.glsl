/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus [*, previously +]
**  Copyright 2015      Michael Kern [+]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2021-2023 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

#include "volume_defines.glsl"

#define ALLOW_MULTIPLE_VARIABLES
#define RAYCASTER // Needed for includes

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

const int MAX_ISOSURFACES = 10;
const int MAX_VARIABLES = 3;

const int TRANSFER_FUNC_COLOUR = 1;
const int TRANSFER_FUNC_SHADINGVAR = 2;
const int TRANSFER_FUNC_SHADINGVAR_MAXNB = 3;

// Vertical level type; see structuredgrid.h.
const int SURFACE_2D = 0;
const int PRESSURE_LEVELS_3D = 1;
const int HYBRID_SIGMA_PRESSURE_3D = 2;
const int POTENTIAL_VORTICITY_2D = 3;
const int LOG_PRESSURE_LEVELS_3D = 4;
const int AUXILIARY_PRESSURE_3D = 5;
const int RADAR_LEVELS_3D = 6;

const int RENDER_ISOSURFACES = 0;
const int RENDER_DVR = 2;

const int VAR_MODE_TRANSFER = 0;
const int VAR_MODE_CLOUD = 1;
const int VAR_MODE_COMBINED = 2;

const int SCALE_FIX_NONE = 0;
const int SCALE_FIX_NORMAL = 1;
const int SCALE_FIX_PHYS = 2;

// Workaround; see usage below.
const float LAMBDA_OFFSET = 0.001;

const float DEFAULT_VERTICAL_SCALE = 36.;

/*****************************************************************************
 ***                             INTERFACES
 *****************************************************************************/

interface VStoFS
{
    smooth vec3 worldSpaceCoordinate; // interpolate the world space coordinate
                                      // to get the fragment position in world
                                      // space
};


/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

// textures
// ========
uniform sampler1D transferFunction;
uniform sampler1D transferFunctionSec;
uniform sampler1D transferFunctionThird;
uniform sampler1D transferFunctionShV; // if separate shading var is used
// contains precomputed z-coordinates
uniform sampler1D pressureTable; // PRESSURE_LEVEL
uniform sampler1D pressureTableSec;
uniform sampler1D pressureTableThird;
uniform sampler1D pressureTableShV; // PRESSURE_LEVEL
// contains hybrid coefficients a,b
uniform sampler1D hybridCoefficients; // HYBRID_SIGMA
uniform sampler1D hybridCoefficientsSec; // HYBRID_SIGMA
uniform sampler1D hybridCoefficientsThird; // HYBRID_SIGMA
uniform sampler1D hybridCoefficientsShV; // HYBRID_SIGMA
// contains surface pressure at grid point (i, j)
uniform sampler2D surfacePressure; // HYBRID_SIGMA
uniform sampler2D surfacePressureSec; // HYBRID_SIGMA
uniform sampler2D surfacePressureThird; // HYBRID_SIGMA
uniform sampler2D surfacePressureShV; // HYBRID_SIGMA
// contains pressure field
uniform sampler3D auxPressureField3D_hPa; // AUXILIARY_PRESSURE_3D
uniform sampler3D auxPressureField3DSec_hPa; // AUXILIARY_PRESSURE_3D
uniform sampler3D auxPressureField3DThird_hPa; // AUXILIARY_PRESSURE_3D
uniform sampler3D auxPressureField3DShV_hPa; // AUXILIARY_PRESSURE_3D

#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
// contains precomputed z-coordinates
uniform sampler2D pressureTexCoordTable2D; // HYBRID_SIGMA
uniform sampler2D pressureTexCoordTable2DSec; // HYBRID_SIGMA
uniform sampler2D pressureTexCoordTable2DThird; // HYBRID_SIGMA
uniform sampler2D pressureTexCoordTable2DShV; // HYBRID_SIGMA
#endif

#ifdef ENABLE_MINMAX_ACCELERATION
// Regular grid that uniformly divides space and stores min/max for each voxel.
// Is used to skip regions in which an isosurface cannot be located. See
// MStructuredGrid::getMinMaxAccelTexture3D().
uniform sampler3D minMaxAccel3D;
uniform sampler3D minMaxAccel3DSec;
uniform sampler3D minMaxAccel3DThird;
uniform sampler3D minMaxAccel3DShV;
#endif

uniform sampler2DMS depthBufferMirror; // mirrored OpenGL depth buffer for
                                     // DVR ray termination

uniform sampler3D dataVolume;
uniform sampler3D dataVolumeSec;
uniform sampler3D dataVolumeThird;
uniform sampler3D dataVolumeShV; // shading var data
uniform sampler1D lonLatLevAxes;
uniform sampler1D lonLatLevAxesSec;
uniform sampler1D lonLatLevAxesThird;
uniform sampler1D lonLatLevAxesShV;

// Contains the volumetric lightmap
uniform sampler3D lightingVolume;
layout(rgba8, binding = 0) uniform image3D volumeLightMap;

// matrices
// ========
uniform mat4    mvpMatrix;
uniform mat4    mvpMatrixInverted;

// vectors
// =======
uniform vec3    cameraPosition;
uniform vec3    cameraDirection;
uniform vec3    volumeBottomSECrnr;
uniform vec3    volumeTopNWCrnr;
uniform vec3    renderRegionBottomSWCrnr;
uniform vec3    renderRegionTopNECrnr;

// scalars
// =======
uniform float   stepSize;
uniform uint    bisectionSteps;

uniform int     viewPortWidth;
uniform int     viewPortHeight;

uniform int     volumetricLightMode;

uniform float   verticalScaling;

uniform float   volumeTopP_hPa;
uniform float   volumeBottomP_hPa;
uniform float   volumePressureExtent_hPa;

// Multi-isosurface settings
// =========================
uniform bool    isoEnables[MAX_ISOSURFACES];
uniform float   isoValues[MAX_ISOSURFACES];
uniform vec4    isoColors[MAX_ISOSURFACES];
uniform int     isoColorModes[MAX_ISOSURFACES];
uniform int     numIsoValues;

uniform vec2    pToWorldZParams;
uniform bool    spatialCDFVisEnabled;
uniform float   numPressureLevels;

// Mode
// ====
uniform uint    renderingMode;
uniform uint    scalingFixMode;
uniform bool    isOrthographic;
uniform bool    shadowMode;

// DVR
// ===
uniform bool    useDithering;
uniform float   ditherStrength;
uniform int     refScaleUnit;
uniform float   refScale;
uniform bool    enabledVariableSlots[MAX_VARIABLES];

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/

// include constants
#include "shared/mvolumeraycasterconstants.h"
#include "shared/msharedconstants.h"
// include global structs
#include "volume_global_structs_utils.glsl"
// include hybrid model volume sampling methods
#include "volume_hybrid_utils.glsl"
// include model level volume with auxiliary pressure field sampling methods
#include "volume_auxiliarypressure_utils.glsl"
// include pressure levels volume sampling methods
#include "volume_pressure_utils.glsl"
// include radar sampling methods, must come before sample_utils
#include "radar_utils.fx.glsl"
#include "volume_radar_utils.fx.glsl"
// defines subroutines and auxiliary ray-casting functions
#include "volume_sample_utils.glsl"
// shading variable sampling methods
#include "volume_sample_shv_utils.glsl"
// procedures to determine the color of the surfaces
#include "volume_render_utils.glsl"
// cloud variables sampling methods
#include "volume_sample_cloud_utils.glsl"
// utility functions for ray empty space skipping
#include "volume_acceleration_utils.glsl"
// randomization functions
#include "includes/random.glsl"
// light and shadow map sampling.
#include "includes/lighting.glsl"
// volume lighting utility methods.
#include "volume_lighting_utils.glsl"

/*****************************************************************************
 ***              VOLUMETRIC LIGHTING COMPUTE SHADER
 *****************************************************************************/

shader CSOpticalThickness()
{
    ivec3 size = imageSize(volumeLightMap);
    vec3 boundsMin = renderRegionBottomSWCrnr;
    vec3 boundsMax = renderRegionTopNECrnr;

    ivec3 volumeCoord = ivec3(gl_GlobalInvocationID.xyz); // Volume texel index
    vec3 voxelCoord = (vec3(volumeCoord) + vec3(0.5)) / vec3(size); // UVW coordinate of texel
    vec3 worldPosition = boundsMin + (boundsMax - boundsMin) * vec3(voxelCoord.xyz);

    if (usePressureScale)
    {
        float pTop = worldZToPressure(boundsMax.z);
        float pBot = worldZToPressure(boundsMin.z);
        float p = voxelCoord.z * (pBot - pTop) + pTop;
        float worldZ = pressureToWorldZ(p);

        worldPosition.z = worldZ;
    }

    vec3 shadowCol = calculateShadowColour(worldPosition, getBoundsMin(), getBoundsMax());
    imageStore(volumeLightMap, volumeCoord, vec4(shadowCol, 0));
}


/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

shader VSmain(in vec3 vertex : 0, out VStoFS Output)
{
    // Convert pressure to world Z coordinate.
    float worldZ = (log(vertex.z) - pToWorldZParams.x) * pToWorldZParams.y;
    // Convert from world space to clip space.
    gl_Position = mvpMatrix * vec4(vertex.xy, worldZ, 1.);
    // Pass the world space coordinate on to the fragment shader. It will be
    // interpolated to yield the fragment position in world space.
    Output.worldSpaceCoordinate = vec3(vertex.xy, worldZ);
}

shader VSShadow(in vec2 vertex0 : 0, in vec3 border0 : 1, out VStoFS Output)
{
    // pass vertex unhandled through vertex shader
    Output.worldSpaceCoordinate = border0;
    // ndc coordinates.xy | -1 = near plane | 1 = point
    gl_Position = vec4(vertex0,-1,1);
}


/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

/**
  Traverse a section of the data volume from lambda to lambdaExit, using 
  constant step size.
  
  Returns true if the entire section is traversed; false if any break condition
  (opacity saturated, normal curve hit) leads to early ray termination.
 */
bool traverseSectionOfDataVolume(
        in vec3 h_gradient, in vec3 rayPosIncrement,
        in float lambdaExit,
        inout float lambda, inout vec3 prevRayPosition, inout float prevLambda,
        inout vec4 rayColor, inout vec3 rayPosition,
        inout int crossingLevelFront, inout int crossingLevelBack,
        inout bool firstCrossing, inout bool lastCrossing,
        inout vec3 crossingPosition)
{
    // Main ray caster loop. Advance the current position along the ray
    // direction through the volume, fetching scalar values from texture memory
    // and testing for isosurface crossing.
    bool prevSampleIsValid = true;
    while (lambda < lambdaExit)
    {
        float scalar = sampleDataAtPos(rayPosition);

        if (scalar != M_MISSING_VALUE)
        {
            crossingLevelFront = computeCrossingLevel(scalar);

            // Missing value handling: in cases in which the ray passes through
            // regions of missing data (e.g., cuts through terrain) and
            // re-enters the "valid" data volume we need to skip the crossing
            // test for the first new valid sample. Otherwise the code below
            // will search for an isosurface crossing between the last valid
            // sample before the missing data and the first valid sample behind.
            // This leads to the bisection correction searching in missing data
            // space, incorrect surfaces are the result. (mm&mr, 21Feb2020)
            if (prevSampleIsValid && (crossingLevelFront != crossingLevelBack))
            {
                vec3 gradient = vec3(0);
                vec3 normal = gradient;

                bisectionCorrection(rayPosition, lambda, prevRayPosition,
                                    prevLambda, crossingLevelFront,
                                    crossingLevelBack);

                gradient = computeGradient(rayPosition, h_gradient);

                float dir = dot(gradient, normalize(rayPosIncrement));
                normal = gradient * -sign(dir);

                vec4 blendColor = blendIsoSurfaces(
                        crossingLevelFront, crossingLevelBack, rayPosition,
                        gradient, scalar);

                // Remember the position either of the first isosurface crossing
                // or of the first crossing with an opaque (alpha = 1) surface.
                if (!firstCrossing || blendColor.a == 1)
                {
                    crossingPosition = rayPosition;
                    firstCrossing = true;
                }

                // Only sample shadow if we actually have something to shade.
                if (blendColor.a > 0.00001 && !shadowRay)
                {
                    blendColor.rgb = getBlinnPhongColor(rayPosition - rayPosIncrement, normal, blendColor.rgb);
                }

                rayColor.rgb += (1 - rayColor.a) * blendColor.rgb;
                rayColor.a += (1 - rayColor.a) * blendColor.a;
            }

            prevSampleIsValid = true;
        }
        else
        {
            // Missing value has been encountered, set flag to ensure correct
            // handling if ray re-enters the data volume (see above).
            prevSampleIsValid = false;
        }
        
        prevLambda  = lambda;
        prevRayPosition = rayPosition;

        lambda += stepSize;
        rayPosition += rayPosIncrement;

        crossingLevelBack = crossingLevelFront;

        // Depth correction for normal curves.
        // ===================================
        if (!shadowRay)
        {
            // Depth has to be corrected to create correct vis when rendering
            // normal curves.
            vec4 clipSpacePos = mvpMatrix * vec4(rayPosition,1);
            clipSpacePos /= clipSpacePos.w;
            float depth = clipSpacePos.z * 0.5 + 0.5;
        }

        // Terminate ray if alpha is saturated.
        if (rayColor.a > 0.99) return false;
    } // raycaster loop
    
    // The section has been traversed up to lambdaExit: Return true.
    return true;
}


/**
  Blends all currently enabled variables together at the given ray position p.
  Simply averages the color of all variables at this position weighed by their opacity.
 */
vec4 blendEnabledVariableColors(
        in float[MAX_VARIABLES] scalars, in vec3 pos, in vec3 rayIncrement,
        in float unitStepSizeM, in float stepSizeM)
{
    vec4 blendColor = vec4(0.0);
    vec4 tempColor = vec4(0.0);

    for (int i = 0; i < MAX_VARIABLES; i++)
    {
        if (enabledVariableSlots[i])
        {
            if (scalars[i] == M_MISSING_VALUE) continue;
            if (ciwcSlot == i)
            {
                tempColor = getCiwcColour(scalars[i], pos);
            }
            else if (clwcSlot == i)
            {
                tempColor = getClwcColour(scalars[i], pos);
            }
            else
            {
                // For now, hardcoded if statements for indices, until
                // variable uniforms are refactored into structs for each variable, and an
                // array that holds all variable structs, except shading var.
                // TODO (tv, 7Jul2023): Rework variable uniforms as described above.
                if (i == 0)
                {
                    float t = (scalars[i] - dataExtent.tfMinimum) / (dataExtent.tfMaximum - dataExtent.tfMinimum);
                    tempColor = texture(transferFunction, t).rgba;
                }
                else if (i == 1)
                {
                    float t = (scalars[i] - dataExtentSec.tfMinimum) / (dataExtentSec.tfMaximum - dataExtentSec.tfMinimum);
                    tempColor = texture(transferFunctionSec, t).rgba;
                }
                else if (i == 2)
                {
                    float t = (scalars[i] - dataExtentThird.tfMinimum) / (dataExtentThird.tfMaximum - dataExtentThird.tfMinimum);
                    tempColor = texture(transferFunctionThird, t).rgba;
                }
                tempColor.a = convertToExtinctionCoeff(tempColor.a, pos, rayIncrement, unitStepSizeM, stepSizeM);
            }
            blendColor = blendColors(blendColor, tempColor);
        }
    }

    return blendColor;
}


/**
  Traverse a section of the data volume from lambda to lambdaExit, using
  constant step size.

  Returns true if the entire section is traversed; false if any break condition
  (opacity saturated, normal curve hit) leads to early ray termination.
 */
bool traverseSectionOfDataVolume_DVR(
        in vec3 h_gradient, in vec3 rayPosIncrement,
        in float lambdaExit,
        inout float lambda, inout vec3 prevRayPosition, inout float prevLambda,
        inout vec4 rayColor, inout vec3 rayPosition,
        inout int crossingLevelFront, inout int crossingLevelBack,
        inout bool firstCrossing, inout bool lastCrossing,
        inout vec3 crossingPosition)
{
    if (!enabledVariableSlots[0] && !enabledVariableSlots[1] && !enabledVariableSlots[2]) return false;

    // Pre-multiply colour by alpha.
    rayColor.rgb = rayColor.rgb * rayColor.a;

    // Array holding variable scalars for each ray step.
    // We don't need to create it every step anew, we can
    // just overwrite the old scalars.
    float[MAX_VARIABLES] scalars;

    // Main ray caster loop. Advance the current position along the ray
    // direction through the volume, fetching scalar values from texture memory
    // and testing for isosurface crossing.
    bool fullTraversal = true;
    while (lambda < lambdaExit)
    {
        // Step size conversion to meters, later used in opacity correction.
        float stepSizeMeters = worldStepToMeters(rayPosition, rayPosIncrement);
        float unitStepSizeMeters = worldStepToMeters(rayPosition, normalize(rayPosIncrement));

        // Sample variable scalars first.
        sampleEnabledVariables(rayPosition, scalars);

        // Retreive colours of each variable, and blend them together.
        vec4 blendColor = blendEnabledVariableColors(scalars, rayPosition, rayPosIncrement, unitStepSizeMeters, stepSizeMeters);

        if (shadowRay)
        {
            blendColor = vec4(0.0, 0.0, 0.0, blendColor.a * 1.0);
        }
        else
        {
            if (blendColor.a > 0.00001)
            {
                if (enableVolLight)
                {
                    blendColor.rgb *= getVolumeLightAtPos(rayPosition, unitStepSizeMeters, blendColor.a);
                }

                blendColor.rgb = getSimpleShading(rayPosition, vec3(0, 0, 1), blendColor.rgb);
            }
        }

        // Opacity correction.
        // Reference: Engel et al. (Real-time volume graphics, 2006), Eq. 1.17
        // float stepSizeRatio = stepSize / 1.; // reference step size is 1.
        // blendColor.a = 1. - pow(1. - blendColor.a, stepSizeRatio);
        // Alternative from TUM.3D practical course IVDA, WS2016/16 slide 14:
        blendColor.a = 1. - exp(-1. * blendColor.a * stepSizeMeters);

        // Front to back alpha blending.
        blendColor.rgb *= blendColor.a;
        rayColor.rgb += (1. - rayColor.a) * blendColor.rgb;
        rayColor.a += (1. - rayColor.a) * blendColor.a;

        prevLambda  = lambda;
        prevRayPosition = rayPosition;

        lambda += stepSize;
        rayPosition += rayPosIncrement;

        // Terminate ray if alpha is saturated.
        if (rayColor.a > 0.99)
        {
            fullTraversal = false;
            break;
        }
    } // raycaster loop

    // Avoid divison by 0.
    if (rayColor.a != 0.f)
    {
        // Restore ray colour from alpha-multiplied value.
        rayColor.rgb = rayColor.rgb / rayColor.a;
    }

    return fullTraversal;
}


/**
  Set up variables required for raycasting and invoke volume traversal loop.
 */
void raycaster(in vec3 h_gradient, in Ray ray, in vec2 lambdaNearFar,
               inout vec4 rayColor, out vec3 rayPosition,
               out vec3 crossingPosition)
{
    float offset = 0.0; // Dithering offset, 0 = no dithering

    // Initialize ray casting parameters.
    float lambda = lambdaNearFar.x;
    rayPosition = ray.origin + lambdaNearFar.x * ray.direction;

    // Don't use dithering in ISOSURFACE mode, only in DVR.
    if (useDithering && renderingMode == RENDER_DVR)
    {
        offset = hash13(rayPosition) * stepSize * ditherStrength;
    }

    lambda += offset;
    rayPosition += ray.direction * offset;

    vec3 rayPosIncrement = stepSize * ray.direction;
    vec3 prevRayPosition = rayPosition;
    float prevLambda = lambda;

    float scalar = sampleDataAtPos(rayPosition);

    int crossingLevelBack, crossingLevelFront;
    crossingLevelBack = crossingLevelFront = computeCrossingLevel(scalar);
    crossingPosition = rayPosition;
    bool firstCrossing = false;
    bool lastCrossing = false;

#ifndef ENABLE_MINMAX_ACCELERATION
    // Empty space skipping acceleration is not enabled:
    // Traverse entire data volume with fixed step size.
    // =================================================

    if (renderingMode == RENDER_ISOSURFACES)
    {
        traverseSectionOfDataVolume(
            h_gradient, rayPosIncrement,
            lambdaNearFar.y,
            lambda, prevRayPosition, prevLambda,
            rayColor, rayPosition,
            crossingLevelFront, crossingLevelBack,
            firstCrossing, lastCrossing,
            crossingPosition);
    }
    else if (renderingMode == RENDER_DVR)
    {
        traverseSectionOfDataVolume_DVR(
            h_gradient, rayPosIncrement,
            lambdaNearFar.y,
            lambda, prevRayPosition, prevLambda,
            rayColor, rayPosition,
            crossingLevelFront, crossingLevelBack,
            firstCrossing, lastCrossing,
            crossingPosition);
    }
        
    // =================================================
        
#else
    // Empty space skipping acceleration is enabled:
    // Step through acceleration structure and only traverse volume bricks
    // that potentially contain an isosurface.
    // ===================================================================

    if (renderingMode == RENDER_DVR)
    {
// Alternative empty space skipping methods.
// Should only be activated for debugging purposes of that algorithm.
// Otherwise, the original algorithm is faster in this use-case, but does not support
// different data field extents for each variable.
// To use the alternative algorithm, comment out the following line:
#define USE_ORIGINAL_DVR_EMPTY_SPACE_SKIPPING
#ifndef USE_ORIGINAL_DVR_EMPTY_SPACE_SKIPPING
        if (!volumeHasEmptySpace())
        {
            traverseSectionOfDataVolume_DVR(
            h_gradient, rayPosIncrement,
            lambdaNearFar.y,
            lambda, prevRayPosition, prevLambda,
            rayColor, rayPosition,
            crossingLevelFront, crossingLevelBack,
            firstCrossing, lastCrossing,
            crossingPosition);
            return;
        }

        // Lambda where we entered the volume of the raycaster + dithering offset.
        float lambdaStart = lambdaNearFar.x + offset;
        vec2 tNearFar;

        while(lambda < lambdaNearFar.y)
        {
            if(shouldTraverseDataSection(ray, lambda, tNearFar))
            {
                // Move lambda and ray position onto next step on ray
                // 1. Zero lambda
                lambda -= lambdaStart;
                // 2. Move lambda onto nearest ray step
                lambda = round(lambda / stepSize) * stepSize;
                // 3. Undo 1.
                lambda += lambdaStart;
                // 4. Update rayPosition, previousRayPosition and previousLambda
                prevLambda  = lambda - stepSize;
                rayPosition = ray.origin + lambda * ray.direction;
                prevRayPosition = rayPosition - rayPosIncrement;

                // The brick is to be traversed, invoke traversal.
                bool brickHasBeenFullyTraversed = false;
                brickHasBeenFullyTraversed = traverseSectionOfDataVolume_DVR(
                h_gradient, rayPosIncrement,
                min(tNearFar.y, lambdaNearFar.y), // don't overshoot!
                lambda, prevRayPosition, prevLambda,
                rayColor, rayPosition,
                crossingLevelFront, crossingLevelBack,
                firstCrossing, lastCrossing,
                crossingPosition);

                // Has the ray been terminated in the brick? Then also terminate the
                // min/max map traversal.
                if (!brickHasBeenFullyTraversed) break;
            }
            else
            {
                // The brick shall not be traversed, skip the space and advance
                // ray position etc. to the brick exit.
                lambda = tNearFar.y + stepSize;
                prevLambda  = lambda - stepSize;
                rayPosition = ray.origin + lambda * ray.direction;
                prevRayPosition = rayPosition - rayPosIncrement;
            }
        }
#else
        // Don't use empty space skipping for DVR-mode if neither the minimum value
        // nor the maximum value have an alpha equal to 0 (This means, no space to
        // skip).
        // Also check if variable grids cover different areas and acceleration bricks dont align.
        // if that is the case, we also cannot use empty space skipping.
        // TODO (tv, 10Feb2023): Implement an empty space skipper that can account for grid misalignment.
        if (!volumeHasEmptySpace() || !areVariableDataExtentsAligned)
        {
            traverseSectionOfDataVolume_DVR(
                        h_gradient, rayPosIncrement,
                        lambdaNearFar.y,
                        lambda, prevRayPosition, prevLambda,
                        rayColor, rayPosition,
                        crossingLevelFront, crossingLevelBack,
                        firstCrossing, lastCrossing,
                        crossingPosition);
            return;
        }

        // TODO (tv, 31Jan2023): This empty space skipping doesnt really work with 3 variables.
        // They are checked on whether to skip the current brick, but they can technically
        // have different extents and therefore brick sizes. This would need to
        // fixed.

        // Lambda where we entered the volume of the raycaster + dithering offset.
        float lambdaStart = lambdaNearFar.x + offset;

        // Grid spacing of the acceleration structure.

        ivec3 nMinMaxAccel3D = ivec3(textureSize(minMaxAccel3D, 0));

        // Special case: If the grid is cyclic in longitude, shift the eastern
        // longitude one grid spacing east (e.g. make 359. 360.).
        float dataSECrnr_x = dataExtent.dataSECrnr.x;
        if (dataExtent.gridIsCyclicInLongitude) dataSECrnr_x += dataExtent.deltaLon;

        vec3 deltaAccel = vec3(
                abs(dataSECrnr_x - dataExtent.dataNWCrnr.x) /
                    float(nMinMaxAccel3D.x),
                abs(dataExtent.dataSECrnr.y - dataExtent.dataNWCrnr.y) /
                    float(nMinMaxAccel3D.y),
                abs(dataExtent.dataSECrnr.z - dataExtent.dataNWCrnr.z) /
                    float(nMinMaxAccel3D.z));

        // In longitude, the world space coordinate system can be shifted by
        // multiples of 360 degrees with respect to the data coordinate system
        // (e.g. if the data volume is defined from -60..60 degrees it may be
        // rendered in the range 300..420 degrees; or cyclic global grids may
        // be repeated at the boundaries).
        // To compute the correct entry indices into the acceleration structure,
        // find closest (shifted) dataNWCrnr.x WEST of rayPosition.x by shifting by
        // multiples of 360. degrees longitude.
        // Example: if dataNWCrnr.x == -60. and rayPosition.x == 320., then
        // dataNWCrnr.x is shifted to 300. If rayPosition.x == -70., then
        // dataNWCrnr.x is shifted to -420.
        float distanceRayPosDataNW_x = rayPosition.x - dataExtent.dataNWCrnr.x;
        int numShift360degrees = int(distanceRayPosDataNW_x / 360.);
        if (rayPosition.x < dataExtent.dataNWCrnr.x) numShift360degrees -= 1;
        float dataNWCrnr_x = dataExtent.dataNWCrnr.x + numShift360degrees * 360.;

        // Indices of the position of ray entry into the acceleration structure
        // (the min/max map covers the same world space as the data volume).
        // "iaccel" is updated during the traversal to keep track of the current
        // brick.
        float mixI = (rayPosition.x - dataNWCrnr_x) / deltaAccel.x;
        float mixJ = (dataExtent.dataNWCrnr.y - rayPosition.y) / deltaAccel.y;
        float mixK = (dataExtent.dataNWCrnr.z - rayPosition.z) / deltaAccel.z;
        ivec3 iaccel = ivec3(int(mixI), int(mixJ), int(mixK));

        // Intersection coordinates of the ray with the axis-perpendicular planes upon
        // exit of the initial acceleration brick (these are three independent
        // coordinates that do not form a point; they are stored in a vector for
        // convenience).
        vec3 posExit = vec3(
                dataNWCrnr_x
                + (iaccel.x + (ray.direction.x >= 0.0 ? 1 : 0)) * deltaAccel.x,
                dataExtent.dataNWCrnr.y
                - (iaccel.y + (ray.direction.y >= 0.0 ? 0 : 1)) * deltaAccel.y,
                dataExtent.dataNWCrnr.z
                - (iaccel.z + (ray.direction.z >= 0.0 ? 0 : 1)) * deltaAccel.z);

        vec3 rayDirInv = vec3(1./ray.direction.x,
                              1./ray.direction.y,
                              1./ray.direction.z);

        // The above plane-ray intersection coordinates parameterised to lambda.
        vec3 lambdaExitAccel = vec3((posExit.x - ray.origin.x) * rayDirInv.x,
                                    (posExit.y - ray.origin.y) * rayDirInv.y,
                                    (posExit.z - ray.origin.z) * rayDirInv.z);
        lambdaExitAccel = abs(lambdaExitAccel);

        // Distance in lambda-"coordinates" between two bricks in x/y/z direction.
        vec3 deltaLambdaAccel = vec3(deltaAccel.x * rayDirInv.x,
                                     deltaAccel.y * rayDirInv.y,
                                     deltaAccel.z * rayDirInv.z);
        deltaLambdaAccel = abs(deltaLambdaAccel);

        // Traversal direction for the acceleration structure in grid index space.
        ivec3 iincr = ivec3(ray.direction.x >= 0.0 ? 1 : -1, // world x = lon = same axis as data
                            ray.direction.y >= 0.0 ? -1 : 1,
                            ray.direction.z >= 0.0 ? -1 : 1);

        // The min/max isovalues of the isosurfaces that are rendered in this
        // render pass.
        vec2 isoMinMax = vec2(isoValues[0], isoValues[0]);
        vec2 isoMinMax2 = vec2(isoValues[0], isoValues[0]);
        vec2 isoMinMax3 = vec2(isoValues[0], isoValues[0]);
        // For DVR use the minimum and maximum value of the transfer function.
        // Set isoMin to -infinity. (No skip if not changed.)
        isoMinMax.x = -1.0 / 0.0;
        // Set isoMax to infinity. (No skip if not changed.)
        isoMinMax.y = 1.0 / 0.0;

        // Set isoMin to -infinity. (No skip if not changed.)
        isoMinMax2.x = -1.0 / 0.0;
        // Set isoMax to infinity. (No skip if not changed.)
        isoMinMax2.y = 1.0 / 0.0;

        // Set isoMin to -infinity. (No skip if not changed.)
        isoMinMax3.x = -1.0 / 0.0;
        // Set isoMax to infinity. (No skip if not changed.)
        isoMinMax3.y = 1.0 / 0.0;

        if (enabledVariableSlots[0])
        {
            if (clwcSlot == 0 || ciwcSlot == 0)
            {
                isoMinMax.x = 0.0;
                isoMinMax.y = 1.0;
            }
            else
            {
                // Since the user is free to set the maximum value of the transfer
                // function to a value smaller than the minimum value of the transfer
                // function, both cases have to be considered.
                if (dataExtent.tfMinimum < dataExtent.tfMaximum)
                {
                    // Only skip isovalues smaller minimum isovalue of transfer function
                    // if alpha of minimum equals 0.
                    if (texture(transferFunction, 0.).a == 0.)
                    {
                        isoMinMax.x = dataExtent.tfMinimum;
                    }
                    // Only skip isovalues greater maximum isovalue of transfer function
                    // if alpha of maximum equals 0.
                    if (texture(transferFunction, 1.).a == 0.)
                    {
                        isoMinMax.y = dataExtent.tfMaximum;
                    }

                }
                else
                {
                    // Only skip isovalues greater maximum isovalue of transfer function
                    // if alpha of maximum equals 0. (In this case tfMinimum stores max)
                    if (texture(transferFunction, 0.).a == 0.)
                    {
                        isoMinMax.y = dataExtent.tfMinimum;
                    }
                    // Only skip isovalues smaller minimum isovalue of transfer function
                    // if alpha of minimum equals 0. (In this case tfMaximum stores min)
                    if (texture(transferFunction, 1.).a == 0.)
                    {
                        isoMinMax.x = dataExtent.tfMaximum;
                    }
                }
            }
        }

        if (enabledVariableSlots[1])
        {
            if (clwcSlot == 1 || ciwcSlot == 1)
            {
                isoMinMax2.x = 0.0;
                isoMinMax2.y = 1.0;
            }
            else
            {
                // Since the user is free to set the maximum value of the transfer
                // function to a value smaller than the minimum value of the transfer
                // function, both cases have to be considered.
                if (dataExtentSec.tfMinimum < dataExtentSec.tfMaximum)
                {
                    // Only skip isovalues smaller minimum isovalue of transfer function
                    // if alpha of minimum equals 0.
                    if (texture(transferFunctionSec, 0.).a == 0.)
                    {
                        isoMinMax2.x = dataExtentSec.tfMinimum;
                    }
                    // Only skip isovalues greater maximum isovalue of transfer function
                    // if alpha of maximum equals 0.
                    if (texture(transferFunctionSec, 1.).a == 0.)
                    {
                        isoMinMax2.y = dataExtentSec.tfMaximum;
                    }
                }
                else
                {
                    // Only skip isovalues greater maximum isovalue of transfer function
                    // if alpha of maximum equals 0. (In this case tfMinimum stores max)
                    if (texture(transferFunctionSec, 0.).a == 0.)
                    {
                        isoMinMax2.y = dataExtentSec.tfMinimum;
                    }
                    // Only skip isovalues smaller minimum isovalue of transfer function
                    // if alpha of minimum equals 0. (In this case tfMaximum stores min)
                    if (texture(transferFunctionSec, 1.).a == 0.)
                    {
                        isoMinMax2.x = dataExtentSec.tfMaximum;
                    }
                }
            }
        }

        if (enabledVariableSlots[2])
        {
            if (clwcSlot == 2 || ciwcSlot == 2)
            {
                isoMinMax3.x = 0.0;
                isoMinMax3.y = 1.0;
            }
            else
            {
                // Since the user is free to set the maximum value of the transfer
                // function to a value smaller than the minimum value of the transfer
                // function, both cases have to be considered.
                if (dataExtentThird.tfMinimum < dataExtentThird.tfMaximum)
                {
                    // Only skip isovalues smaller minimum isovalue of transfer function
                    // if alpha of minimum equals 0.
                    if (texture(transferFunctionThird, 0.).a == 0.)
                    {
                        isoMinMax3.x = dataExtentThird.tfMinimum;
                    }
                    // Only skip isovalues greater maximum isovalue of transfer function
                    // if alpha of maximum equals 0.
                    if (texture(transferFunctionThird, 1.).a == 0.)
                    {
                        isoMinMax3.y = dataExtentThird.tfMaximum;
                    }
                }
                else
                {
                    // Only skip isovalues greater maximum isovalue of transfer function
                    // if alpha of maximum equals 0. (In this case tfMinimum stores max)
                    if (texture(transferFunctionThird, 0.).a == 0.)
                    {
                        isoMinMax3.y = dataExtentThird.tfMinimum;
                    }
                    // Only skip isovalues smaller minimum isovalue of transfer function
                    // if alpha of minimum equals 0. (In this case tfMaximum stores min)
                    if (texture(transferFunctionThird, 1.).a == 0.)
                    {
                        isoMinMax3.x = dataExtentThird.tfMaximum;
                    }
                }
            }
        }

        // Loop that traverses the min/max structure. "Empty" (with respect to the
        // targeted isosurfaces) space is skipped; bricks that potentially contain
        // an isosurface are traversed.
        // References: Krueger and Westermann (2003); Shirley, Fundamentals of
        // Computer Graphics, 3rd ed. (2009), Ch. 12.2.3.
        while (lambda < lambdaNearFar.y)
        {

            // Get min/max scalar values contained in the current brick. Check
            // if an isosurface can be contained.
            vec2 brickMinMax = texelFetch(minMaxAccel3D, iaccel, 0).rg;
            vec2 brickMinMax2 = texelFetch(minMaxAccel3DSec, iaccel, 0).rg;
            vec2 brickMinMax3 = texelFetch(minMaxAccel3DThird, iaccel, 0).rg;

            bool doBrickTraversal1, doBrickTraversal2, doBrickTraversal3 = false;
            bool doBrickTraversal = false;

            if (enabledVariableSlots[0])
            {
                if (clwcSlot == 0 || ciwcSlot == 0)
                {
                    doBrickTraversal1 = (! ((brickMinMax.y <= isoMinMax.x)
                                            || (brickMinMax.x > isoMinMax.y)));
                }
                else
                {
                    doBrickTraversal1 = (! ((brickMinMax.y < isoMinMax.x)
                                            || (brickMinMax.x > isoMinMax.y)));
                }
            }
            if (enabledVariableSlots[1])
            {
                if (clwcSlot == 1 || ciwcSlot == 1)
                {
                    doBrickTraversal2 = (! ((brickMinMax2.y <= isoMinMax2.x)
                                            || (brickMinMax2.x > isoMinMax2.y)));
                }
                else
                {
                    doBrickTraversal2 = (! ((brickMinMax2.y < isoMinMax2.x)
                                            || (brickMinMax2.x > isoMinMax2.y)));
                }
            }
            if (enabledVariableSlots[2])
            {
                if (clwcSlot == 2 || ciwcSlot == 2)
                {
                    doBrickTraversal3 = (! ((brickMinMax3.y <= isoMinMax3.x)
                                            || (brickMinMax3.x > isoMinMax3.y)));
                }
                else
                {
                    doBrickTraversal3 = (! ((brickMinMax3.y < isoMinMax3.x)
                                            || (brickMinMax3.x > isoMinMax3.y)));
                }
            }

            doBrickTraversal = doBrickTraversal1 || doBrickTraversal2 || doBrickTraversal3;
            float lambdaBrickExit = lambda;

            // DEBUG -- Uncomment to visualise the boundary artefacts described
            // in the TODO (mr, 17Nov2014) in the fragment shader.
            //rayColor = vec4(brickMinMax.g / 30., 0., 0., 1.); break;

            // DEBUG -- Uncomment to see whether we're getting the right entry
            // brick.
            //rayColor = vec4(iaccel.z / 32., iaccel.y / 32., iaccel.x / 32., 1.); break;

            // Determine which axis-perpendicular plane is crossed next. This
            // corresponds to the currently smallest "lambdaExit".
            if (lambdaExitAccel.x < lambdaExitAccel.y)
            {
                if (lambdaExitAccel.x < lambdaExitAccel.z) // x is smallest
                {
                    // The ray exits the brick through the boundary perpendicular
                    // to the x (i.e. longitude) coordinate. If the brick is to
                    // be traversed, set lambdaBrickExit to the exit point.
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.x;
                    // If the brick cannot not contain any isosurface, advance ray
                    // to next brick.
                    else lambda = lambdaExitAccel.x;

                    // Update lambdaExitAccel.x and iaccel.x to represent the exit
                    // position and index of the next brick.
                    lambdaExitAccel.x += deltaLambdaAccel.x;
                    iaccel.x += iincr.x;

                    // If the grid is cyclic in longitude, map iaccel.x range to
                    // (0 .. nMinMaxAccel3D.x - 1).
                    if (dataExtent.gridIsCyclicInLongitude) iaccel.x %= nMinMaxAccel3D.x;
                }

                else // z < x < y => z is smallest
                {
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.z;
                    else lambda = lambdaExitAccel.z;

                    lambdaExitAccel.z += deltaLambdaAccel.z;
                    iaccel.z += iincr.z;
                }
            }
            else
            {
                if (lambdaExitAccel.y < lambdaExitAccel.z) // y is smallest
                {
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.y;
                    else lambda = lambdaExitAccel.y;

                    lambdaExitAccel.y += deltaLambdaAccel.y;
                    iaccel.y += iincr.y;
                }

                else // z < y < x => z is smallest
                {
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.z;
                    else lambda = lambdaExitAccel.z;

                    lambdaExitAccel.z += deltaLambdaAccel.z;
                    iaccel.z += iincr.z;
                }
            }

            if (doBrickTraversal)
            {
                // Move lambda and ray position onto next step on ray
                // 1. Zero lambda
                lambda -= lambdaStart;
                // 2. Move lambda onto nearest ray step
                lambda = round(lambda / stepSize) * stepSize;
                // 3. Undo 1.
                lambda += lambdaStart;
                // 4. Update rayPosition, previousRayPosition and previousLambda
                prevLambda  = lambda - stepSize;
                rayPosition = ray.origin + lambda * ray.direction;;
                prevRayPosition = rayPosition - rayPosIncrement;

                // The brick is to be traversed, invoke traversal.
                bool brickHasBeenFullyTraversed = false;
                brickHasBeenFullyTraversed = traverseSectionOfDataVolume_DVR(
                        h_gradient, rayPosIncrement,
                        min(lambdaBrickExit, lambdaNearFar.y), // don't overshoot!
                        lambda, prevRayPosition, prevLambda,
                        rayColor, rayPosition,
                        crossingLevelFront, crossingLevelBack,
                        firstCrossing, lastCrossing,
                        crossingPosition);

                // Debug code to visualize bricks that are not skipped.
                // Uses variable i' data extents to color bricks.
                //rayColor = vec4(iaccel.x / 32., iaccel.y / 32., iaccel.z / 32., 1.);
                //break;


                // Has the ray been terminated in the brick? Then also terminate the
                // min/max map traversal.
                if (!brickHasBeenFullyTraversed) break;
            }
            else
            {
                // The brick shall not be traversed, skip the space and advance
                // ray position etc. to the brick exit.
                prevLambda  = lambda - stepSize;
                rayPosition = ray.origin + lambda * ray.direction;;
                prevRayPosition = rayPosition - rayPosIncrement;
            }

            // Special case for non-cyclic grids. If parts of a non-cyclic data
            // volume are repeated (e.g. a volume defined in the range -60..40 deg
            // in longitude is rendered from 0..360 deg) and the samping position
            // has left valid data space, skip the empty space and re-enter the
            // data volume where its next recurrence starts. The case is simply
            // detected by testing whether iaccel.x still referes to valid brick
            // indidces.
            if ( (iaccel.x < 0) || (iaccel.x >= nMinMaxAccel3D.x) )
            {
                // Skip empty space and compute the lambda of the position where
                // the ray enters the next recurrence of the data volume.
                if (iaccel.x >= nMinMaxAccel3D.x)
                {
                    // ray.direction.x >= 0.0: move eastward.
                    dataNWCrnr_x += 360.;
                    lambda = abs((dataNWCrnr_x - ray.origin.x) * rayDirInv.x) + offset;
                }
                else
                {
                    // ray.direction.x < 0.0: move westward.
                    dataNWCrnr_x -= 360.;
                    float widthOfDataVolume = dataExtent.dataSECrnr.x - dataExtent.dataNWCrnr.x;
                    lambda = abs((dataNWCrnr_x + widthOfDataVolume - ray.origin.x) * rayDirInv.x) + offset;
                }

    //TODO -- avoid noise -- see other usage of LAMBDA_OFFSET below.
                lambda += LAMBDA_OFFSET;

                // Compute the new "rayPosition" from lambda.
                prevLambda  = lambda - stepSize;
                rayPosition = ray.origin + lambda * ray.direction;;
                prevRayPosition = rayPosition - rayPosIncrement;

                // Repeat statements from the initialization above: compute
                // iaccel, posExit and lambdaExitAccel at the new "rayPosition".
                float mixI = (rayPosition.x - dataNWCrnr_x) / deltaAccel.x;
                float mixJ = (dataExtent.dataNWCrnr.y - rayPosition.y) / deltaAccel.y;
                float mixK = (dataExtent.dataNWCrnr.z - rayPosition.z) / deltaAccel.z;
                iaccel = ivec3(int(mixI), int(mixJ), int(mixK));

                posExit = vec3(
                        dataNWCrnr_x
                        + (iaccel.x + (ray.direction.x >= 0.0 ? 1 : 0)) * deltaAccel.x,
                        dataExtent.dataNWCrnr.y
                        - (iaccel.y + (ray.direction.y >= 0.0 ? 0 : 1)) * deltaAccel.y,
                        dataExtent.dataNWCrnr.z
                        - (iaccel.z + (ray.direction.z >= 0.0 ? 0 : 1)) * deltaAccel.z);

                lambdaExitAccel = vec3(
                        (posExit.x - ray.origin.x) * rayDirInv.x,
                        (posExit.y - ray.origin.y) * rayDirInv.y,
                        (posExit.z - ray.origin.z) * rayDirInv.z);
                lambdaExitAccel = abs(lambdaExitAccel);
            }
        }

        // DEBUG.
        // rayColor = vec4(iaccel.z / 32., iaccel.y / 32., iaccel.x / 32., 1.);
#endif
    }
    else
    {

        // Grid spacing of the acceleration structure.

        ivec3 nMinMaxAccel3D = ivec3(textureSize(minMaxAccel3D, 0));

        // Special case: If the grid is cyclic in longitude, shift the eastern
        // longitude one grid spacing east (e.g. make 359. 360.).
        float dataSECrnr_x = dataExtent.dataSECrnr.x;
        if (dataExtent.gridIsCyclicInLongitude) dataSECrnr_x += dataExtent.deltaLon;

        vec3 deltaAccel = vec3(
                abs(dataSECrnr_x - dataExtent.dataNWCrnr.x) /
                    float(nMinMaxAccel3D.x),
                abs(dataExtent.dataSECrnr.y - dataExtent.dataNWCrnr.y) /
                    float(nMinMaxAccel3D.y),
                abs(dataExtent.dataSECrnr.z - dataExtent.dataNWCrnr.z) /
                    float(nMinMaxAccel3D.z));

        // In longitude, the world space coordinate system can be shifted by
        // multiples of 360 degrees with respect to the data coordinate system
        // (e.g. if the data volume is defined from -60..60 degrees it may be
        // rendered in the range 300..420 degrees; or cyclic global grids may
        // be repeated at the boundaries).
        // To compute the correct entry indices into the acceleration structure,
        // find closest (shifted) dataNWCrnr.x WEST of rayPosition.x by shifting by
        // multiples of 360. degrees longitude.
        // Example: if dataNWCrnr.x == -60. and rayPosition.x == 320., then
        // dataNWCrnr.x is shifted to 300. If rayPosition.x == -70., then
        // dataNWCrnr.x is shifted to -420.
        float distanceRayPosDataNW_x = rayPosition.x - dataExtent.dataNWCrnr.x;
        int numShift360degrees = int(distanceRayPosDataNW_x / 360.);
        if (rayPosition.x < dataExtent.dataNWCrnr.x) numShift360degrees -= 1;
        float dataNWCrnr_x = dataExtent.dataNWCrnr.x + numShift360degrees * 360.;

        // Indices of the position of ray entry into the acceleration structure
        // (the min/max map covers the same world space as the data volume).
        // "iaccel" is updated during the traversal to keep track of the current
        // brick.
        float mixI = (rayPosition.x - dataNWCrnr_x) / deltaAccel.x;
        float mixJ = (dataExtent.dataNWCrnr.y - rayPosition.y) / deltaAccel.y;
        float mixK = (dataExtent.dataNWCrnr.z - rayPosition.z) / deltaAccel.z;
        ivec3 iaccel = ivec3(int(mixI), int(mixJ), int(mixK));

        // Intersection coordinates of the ray with the axis-perpendicular planes upon
        // exit of the initial acceleration brick (these are three independent
        // coordinates that do not form a point; they are stored in a vector for
        // convenience).
        vec3 posExit = vec3(
                dataNWCrnr_x
                + (iaccel.x + (ray.direction.x >= 0.0 ? 1 : 0)) * deltaAccel.x,
                dataExtent.dataNWCrnr.y
                - (iaccel.y + (ray.direction.y >= 0.0 ? 0 : 1)) * deltaAccel.y,
                dataExtent.dataNWCrnr.z
                - (iaccel.z + (ray.direction.z >= 0.0 ? 0 : 1)) * deltaAccel.z);

        vec3 rayDirInv = vec3(1./ray.direction.x,
                              1./ray.direction.y,
                              1./ray.direction.z);

        // The above plane-ray intersection coordinates parameterised to lambda.
        vec3 lambdaExitAccel = vec3((posExit.x - ray.origin.x) * rayDirInv.x,
                                    (posExit.y - ray.origin.y) * rayDirInv.y,
                                    (posExit.z - ray.origin.z) * rayDirInv.z);
        lambdaExitAccel = abs(lambdaExitAccel);

        // Distance in lambda-"coordinates" between two bricks in x/y/z direction.
        vec3 deltaLambdaAccel = vec3(deltaAccel.x * rayDirInv.x,
                                     deltaAccel.y * rayDirInv.y,
                                     deltaAccel.z * rayDirInv.z);
        deltaLambdaAccel = abs(deltaLambdaAccel);

        // Traversal direction for the acceleration structure in grid index space.
        ivec3 iincr = ivec3(ray.direction.x >= 0.0 ? 1 : -1, // world x = lon = same axis as data
                            ray.direction.y >= 0.0 ? -1 : 1,
                            ray.direction.z >= 0.0 ? -1 : 1);

        // The min/max isovalues of the isosurfaces that are rendered in this
        // render pass.
        vec2 isoMinMax = vec2(isoValues[0], isoValues[0]);
        for (int i = 1; i < numIsoValues; i++)
        {
            if (isoValues[i] < isoMinMax.x) isoMinMax.x = isoValues[i];
            if (isoValues[i] > isoMinMax.y) isoMinMax.y = isoValues[i];
        }

        // Loop that traverses the min/max structure. "Empty" (with respect to the
        // targeted isosurfaces) space is skipped; bricks that potentially contain
        // an isosurface are traversed.
        // References: Krueger and Westermann (2003); Shirley, Fundamentals of
        // Computer Graphics, 3rd ed. (2009), Ch. 12.2.3.
        while (lambda < lambdaNearFar.y)
        {
            // Get min/max scalar values contained in the current brick. Check
            // if an isosurface can be contained.
            vec2 brickMinMax = texelFetch(minMaxAccel3D, iaccel, 0).rg;
            bool doBrickTraversal = (! ((brickMinMax.y < isoMinMax.x)
                                            || (brickMinMax.x > isoMinMax.y)));

            float lambdaBrickExit = lambda;

            // DEBUG -- Uncomment to visualise the boundary artefacts described
            // in the TODO (mr, 17Nov2014) in the fragment shader.
            //rayColor = vec4(brickMinMax.g / 30., 0., 0., 1.); break;

            // DEBUG -- Uncomment to see whether we're getting the right entry
            // brick.
            //rayColor = vec4(iaccel.z / 32., iaccel.y / 32., iaccel.x / 32., 1.); break;

            // Determine which axis-perpendicular plane is crossed next. This
            // corresponds to the currently smallest "lambdaExit".
            if (lambdaExitAccel.x < lambdaExitAccel.y)
            {
                if (lambdaExitAccel.x < lambdaExitAccel.z) // x is smallest
                {
                    // The ray exits the brick through the boundary perpendicular
                    // to the x (i.e. longitude) coordinate. If the brick is to
                    // be traversed, set lambdaBrickExit to the exit point.
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.x;
                    // If the brick cannot not contain any isosurface, advance ray
                    // to next brick.
                    else lambda = lambdaExitAccel.x;

                    // Update lambdaExitAccel.x and iaccel.x to represent the exit
                    // position and index of the next brick.
                    lambdaExitAccel.x += deltaLambdaAccel.x;
                    iaccel.x += iincr.x;

                    // If the grid is cyclic in longitude, map iaccel.x range to
                    // (0 .. nMinMaxAccel3D.x - 1).
                    if (dataExtent.gridIsCyclicInLongitude) iaccel.x %= nMinMaxAccel3D.x;
                }

                else // z < x < y => z is smallest
                {
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.z;
                    else lambda = lambdaExitAccel.z;

                    lambdaExitAccel.z += deltaLambdaAccel.z;
                    iaccel.z += iincr.z;
                }
            }

            else
            {
                if (lambdaExitAccel.y < lambdaExitAccel.z) // y is smallest
                {
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.y;
                    else lambda = lambdaExitAccel.y;

                    lambdaExitAccel.y += deltaLambdaAccel.y;
                    iaccel.y += iincr.y;
                }

                else // z < y < x => z is smallest
                {
                    if (doBrickTraversal) lambdaBrickExit = lambdaExitAccel.z;
                    else lambda = lambdaExitAccel.z;

                    lambdaExitAccel.z += deltaLambdaAccel.z;
                    iaccel.z += iincr.z;
                }
            }

            if (doBrickTraversal)
            {
                // The brick is to be traversed, invoke traversal.
                bool brickHasBeenFullyTraversed = traverseSectionOfDataVolume(
                            h_gradient, rayPosIncrement,
                            min(lambdaBrickExit, lambdaNearFar.y), // don't overshoot!
                            lambda, prevRayPosition, prevLambda,
                            rayColor, rayPosition,
                            crossingLevelFront, crossingLevelBack,
                            firstCrossing, lastCrossing,
                            crossingPosition);

                // Has the ray been terminated in the brick? Then also terminate the
                // min/max map traversal.
                if (!brickHasBeenFullyTraversed) break;
            }
            else
            {
                // The brick shall not be traversed, skip the space and advance
                // ray position etc. to the brick exit.
                prevLambda  = lambda - stepSize;
                rayPosition = ray.origin + lambda * ray.direction;;
                prevRayPosition = rayPosition - rayPosIncrement;
            }

            // Special case for non-cyclic grids. If parts of a non-cyclic data
            // volume are repeated (e.g. a volume defined in the range -60..40 deg
            // in longitude is rendered from 0..360 deg) and the samping position
            // has left valid data space, skip the empty space and re-enter the
            // data volume where its next recurrence starts. The case is simply
            // detected by testing whether iaccel.x still referes to valid brick
            // indidces.
            if ( (iaccel.x < 0) || (iaccel.x >= nMinMaxAccel3D.x) )
            {
                // Skip empty space and compute the lambda of the position where
                // the ray enters the next recurrence of the data volume.
                if (iaccel.x >= nMinMaxAccel3D.x)
                {
                    // ray.direction.x >= 0.0: move eastward.
                    dataNWCrnr_x += 360.;
                    lambda = abs((dataNWCrnr_x - ray.origin.x) * rayDirInv.x);
                }
                else
                {
                    // ray.direction.x < 0.0: move westward.
                    dataNWCrnr_x -= 360.;
                    float widthOfDataVolume = dataExtent.dataSECrnr.x - dataExtent.dataNWCrnr.x;
                    lambda = abs((dataNWCrnr_x + widthOfDataVolume - ray.origin.x) * rayDirInv.x);
                }

    //TODO -- avoid noise -- see other usage of LAMBDA_OFFSET below.
                lambda += LAMBDA_OFFSET;

                // Compute the new "rayPosition" from lambda.
                prevLambda  = lambda - stepSize;
                rayPosition = ray.origin + lambda * ray.direction;;
                prevRayPosition = rayPosition - rayPosIncrement;

                // Reset the current crossinglevel (otherwise isosurfaces that
                // should be open can be closed, e.g. if the last scalar value
                // before the empty-space-skip is below the isovalue and the first
                // scalar after re-entering the data volume is above...).
                float scalar = sampleDataAtPos(rayPosition);
                crossingLevelBack = crossingLevelFront = computeCrossingLevel(scalar);

                // Repeat statements from the initialization above: compute
                // iaccel, posExit and lambdaExitAccel at the new "rayPosition".
                float mixI = (rayPosition.x - dataNWCrnr_x) / deltaAccel.x;
                float mixJ = (dataExtent.dataNWCrnr.y - rayPosition.y) / deltaAccel.y;
                float mixK = (dataExtent.dataNWCrnr.z - rayPosition.z) / deltaAccel.z;
                iaccel = ivec3(int(mixI), int(mixJ), int(mixK));

                posExit = vec3(
                        dataNWCrnr_x
                        + (iaccel.x + (ray.direction.x >= 0.0 ? 1 : 0)) * deltaAccel.x,
                        dataExtent.dataNWCrnr.y
                        - (iaccel.y + (ray.direction.y >= 0.0 ? 0 : 1)) * deltaAccel.y,
                        dataExtent.dataNWCrnr.z
                        - (iaccel.z + (ray.direction.z >= 0.0 ? 0 : 1)) * deltaAccel.z);

                lambdaExitAccel = vec3(
                        (posExit.x - ray.origin.x) * rayDirInv.x,
                        (posExit.y - ray.origin.y) * rayDirInv.y,
                        (posExit.z - ray.origin.z) * rayDirInv.z);
                lambdaExitAccel = abs(lambdaExitAccel);
            }
        }

        // DEBUG.
        //rayColor = vec4(iaccel.z / 32., iaccel.y / 32., iaccel.x / 32., 1.);
    }
#endif        
}


shader FSmain(in VStoFS Input, out vec4 fragColor : 0)
{
    vec3 h_gradient = initGradientSamplingStep(dataExtent);

    // Ray in parameterised form and parameter bounds for the render volume.
    Ray ray;
    vec2 lambdaNearFar = vec2(0., 0.);

    shadowRay = shadowMode;

    if (isOrthographic)
    {
        ray.origin = Input.worldSpaceCoordinate;
        ray.direction = cameraDirection;
    }
    else
    {
        // Volume rendering mode: The ray direction is determined by the camera
        // position and the fragment position, both in world space.
        ray.origin = cameraPosition;
        ray.direction = normalize(Input.worldSpaceCoordinate - cameraPosition);
    }

    // Compute the intersection points of the ray with this bounding box.
    // If the ray does not intersect with the box discard this fragment.
    bool rayIntersectsRenderVolume = rayBoxIntersection(
            ray, renderRegionBottomSWCrnr, renderRegionTopNECrnr, lambdaNearFar);
    if (!rayIntersectsRenderVolume)
    {
        discard;
    }

    // Adjust ray origin to entry point for orthographic projections
    if (isOrthographic)
    {
        ray.origin = ray.origin + ray.direction * lambdaNearFar.x;

        lambdaNearFar.y = lambdaNearFar.y - lambdaNearFar.x;
        lambdaNearFar.x = 0;
    }

    // If the value for lambdaNear is < 0 the camera is located inside the
    // bounding box. It makes no sense to start the ray traversal behind the
    // camera, hence move lambdaNear to 0 to start in front of the camera.
    lambdaNearFar.x = max(lambdaNearFar.x, 0.01);

    // Treatment of longitude: The bounding box above uses the user-specified
    // east/west boundaries. In the longitude dimension, parts of the data
    // grid can be repeated in the display or shifted in location (example:
    // data is defined in the range -60..+40 degrees but displayed in the
    // range 0..360 degrees => the part from -60..0 needs to be displayed
    // at 300..360).
    // To avoid sampling at locations where no data values are defined,
    // the longitude boundaries of the bounding box need to be adjusted
    // (example continued: the ray intersects with the bounding box at
    // 270 deg, looking EASTward (ray.direction.x > 0). No data is defined
    // at 270 deg. The next cyclic entry position into the data volume
    // (-60 deg + N * 360 deg) in eastward direction is at 300 deg. Hence,
    // move the western boundary of the bounding box from 0 deg (the user
    // specified value) to 300 deg. (If the camera is looking WESTward,
    // the next EASTERN boundary is at 40 deg).

    // Compute the next shifted western boundary of the data volume
    // (dataNWCrnr_x) WEST of the computed intersection (entry) point
    // (given by lambdaNearFar.x).
    vec3 rayEntryIntoBBox = ray.origin + lambdaNearFar.x * ray.direction;
    float distanceRayEntryLonToDataWestLon = rayEntryIntoBBox.x - renderRegionBottomSWCrnr.x;
    int numShift360degrees = int(distanceRayEntryLonToDataWestLon / 360.);
    if (rayEntryIntoBBox.x < renderRegionBottomSWCrnr.x) numShift360degrees -= 1;
    float dataNWCrnr_x = renderRegionBottomSWCrnr.x + numShift360degrees * 360.;

    // Determine whether the current entry position is located inside or
    // outside of the data volume (inside = distance of entry to next
    // western data boundary needs to be smaller than width of data volume).
    distanceRayEntryLonToDataWestLon = rayEntryIntoBBox.x - dataNWCrnr_x;
    float widthOfDataVolume = renderRegionTopNECrnr.x - renderRegionBottomSWCrnr.x;
    // If the grid is cyclic in longitude, add one grid width to avoid
    // a gap at the data volume boundary.
    if (dataExtent.gridIsCyclicInLongitude) widthOfDataVolume += dataExtent.deltaLon;

    // We cant assign to uniforms, so create a local copy
    vec3 renderRegionBottomSWCrnrCyclic = renderRegionBottomSWCrnr;
    vec3 renderRegionTopNECrnrCyclic = renderRegionTopNECrnr;

    if (distanceRayEntryLonToDataWestLon > widthOfDataVolume)
    {
        // Ray entry position is outside of data volume.

        if (ray.direction.x > 0)
        {
            // Camera is looking EAST: modify western boundary of bbox.
            renderRegionBottomSWCrnrCyclic.x = max(
                    volumeTopNWCrnr.x, dataNWCrnr_x + 360.);
        }
        else
        {
            // Camera is looking WEST: modify eastern boundary of bbox.
            renderRegionTopNECrnrCyclic.x = min(
                    volumeBottomSECrnr.x, dataNWCrnr_x + widthOfDataVolume);
        }

        // Again compute intersection points of ray and modified bounding
        // box.
        bool rayIntersectsRenderVolume = rayBoxIntersection(
            ray, renderRegionBottomSWCrnrCyclic, renderRegionTopNECrnrCyclic, lambdaNearFar);
        if (!rayIntersectsRenderVolume) discard;
    }

//TODO (mr, 17Nov2014): The sampling at the position computed from 
//      lambdaNearFar.x often fails, resulting in artefacts at the data volume
//      boundaries. This might be due to numerical inaccuracies that cause
//      lambda_near to be located slightly before the box. I move the ray
//      a bit foward here, but that is not a clean solution. INVESTIGATE THE
//      ISSUE!
    lambdaNearFar.x += LAMBDA_OFFSET;

    vec4 rayColor = vec4(0);
    vec3 rayPosition;
    vec3 crossingPosition;
    vec3 depthPosition;

    if (renderingMode == RENDER_DVR)
    {
        // For direct volume rendering (not necessary for shadow map generation
        // and also breaks shadow map code..):
        // Correct "far" lambda so that the raycaster terminates at the depth
        // that is stored in the depth buffer *before* this raycaster shader is
        // started. Read the current depth from the depth buffer texture that
        // mirrors the depth buffer (see CPP code, method
        // mirrorDepthBufferToTexture()), reconstruct its world space position
        // (assuming that the existing "back" fragment is located on the ray)
        // and compute its distance to the ray origin. Since ray.direction is
        // if unit length, the distance is the lambda value.
        // References:
        // https://forums.khronos.org/showthread.php/79310-Convert-to-Worldspace-from-depth-buffer
        // https://www.khronos.org/opengl/wiki/Compute_eye_space_from_window_space
        vec2 texCoordDepthBuffer = vec2(gl_FragCoord.x / viewPortWidth,
                                        gl_FragCoord.y / viewPortHeight);

        float depthFromBuffer = texelFetch(depthBufferMirror, ivec2(gl_FragCoord), 1).x;

     // For DEBUG see: http://glampert.com/2014/01-26/visualizing-the-depth-buffer/
        // rayColor = vec4((depthFromBuffer-0.95)/0.05, 0., 0., 1.); break;
        vec4 posBackFragment = vec4(texCoordDepthBuffer.xy * 2. - 1.,
                                    depthFromBuffer * 2. - 1., 1.);
        posBackFragment = mvpMatrixInverted * posBackFragment;
        posBackFragment /= posBackFragment.w;
        float lambdaDepth = distance(posBackFragment.xyz, ray.origin);
        lambdaNearFar.y = min(lambdaNearFar.y, lambdaDepth);
    }

    // Invoke Raycaster.
    raycaster(h_gradient, ray, lambdaNearFar, rayColor, rayPosition,
              crossingPosition);

    // No shadow is rendered -- discard fragment if no isosurface has
    // been crossed by the ray.
    if (rayColor.a == 0.) discard;
    depthPosition = crossingPosition;

    // Compute correct depth value.
    gl_FragDepth = computeDepthFromWorld(depthPosition);

    fragColor = rayColor;

    if (shadowRay && renderingMode != RENDER_DVR)
    {
        fragColor = vec4(depthPosition, 1.0);
    }
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program Volume
{
    vs(430)=VSmain();
    fs(430)=FSmain();
};

program VolumeLighting
{
    cs(430)=CSOpticalThickness() : in(local_size_x = 1, local_size_y = 1, local_size_z = 1);
};

program Shadow
{
    vs(430)=VSShadow();
    fs(430)=FSmain();
};