/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/


float worldZToMetre(in float z)
{
    return z * worldZMeterScaling;
}


/** 
 * Converts world coordinates to radar indices. Returns (-100,-100,-100) if the
 * point is outside the radar range, and (-1,-1,-1) if it is inside the range
 * but not inside of a radar cell.
 */
ivec3 lonLatWorldZToRadarIndices(in vec3 pos, in DataVolumeExtent dataExtent)
{
    float lon = pos.x;
    float lat = pos.y;
    // Convert z-component to height in metres.
    pos.z = worldZToMetre(pos.z);

    // Convert to global cartesian coordinates.
    vec3 pointCart = lonLatMetreToGlobalCart(pos);

    // The length of the straight line between the radar site and the point of interest.
    float directDistance = length(pointCart - dataExtent.cartesianCoordinatesRadar);

    float Re = EARTH_RADIUS_km * 1000.; // Earth radius in metres.
    float R = 4. * Re; // Approximated radius of the radar beam curvature.
    float h0 = dataExtent.lonLatHeightRadar.z; // Height coordinate of radar antenna.

    // The angle of a curved beam of radius R between the radar site and the point of interest.
    float beta = asin(directDistance / (2 * R)) * 2;
    // The length of the potential radar ray between the radar site and the point of interest.
    float rayDist = beta * R;

    // Points outside the radar range are discarded.
    if (rayDist > dataExtent.radarRange) { return ivec3(-100, -100, -100); }

    // The potential radar ray elevation angle in radians and degrees.
    float elevationAngleRad = acos((pow(Re + h0, 2.0) + pow(directDistance, 2.0) - pow(Re + pos.z, 2.0))
                                   /(2 * (Re + h0) * directDistance)) - (M_PI - beta)/2;
    float elevationAngleDeg = radiansToDegrees(elevationAngleRad);

    ivec3 indexOfClosestPoint = ivec3(-1, -1, -1);

    // Check which (if any) elevation includes the point of interest.
    float smallestAngleDifference = 1.0/0.0;
    int closestElevationIdx = -1;
    int closestBinIdx = -1;

    // Number of elevations in the data volume.
    int nEls = textureSize(dataVolume, 0)[2];

    for (int elIdx=0; elIdx < nEls; elIdx++)
    {
        // Vertical angular distance between the central angle of this elevation
        // and the potential elevation angle for the sampled position.
        float dif = abs(elevationAngleDeg - radarMetadata[elIdx].elevationAngleDegrees);

        // Beamwidth in one direction.
        float vHalfWidth = radarMetadata[elIdx].verticalBeamwidth/2.0;

        // Is the potential beam angle under which the point is reached covered
        // by the beamwidth of this elevation?
        if (dif < vHalfWidth && dif < smallestAngleDifference)
        {
            closestElevationIdx = elIdx;

            // Check if a bin of this elevation includes the point of interest.
            // For that, calculate the point's hypothetical bin index,
            // and compare it with the maximum index of the elevation.
            int binIdx = int(floor((rayDist - radarMetadata[elIdx].rangeStart)/radarMetadata[elIdx].binDistance));

            if (binIdx < radarMetadata[elIdx].nbins)
            {
                // If the point is inside an elevation and bin: remember it.
                smallestAngleDifference = dif;
                closestBinIdx = binIdx;
                // Found the correct elevation and bin.
                // break; // If the elevations don't overlap, we can stop here.
            }
        }
    }

    // The azimuth angle between the point pos and the radar site.
    float azimuthDeg = calculateAzimuthFromRadarSite(lon, lat, dataExtent.lonLatHeightRadar);
    int closestAzimuthIdx = int((floor(azimuthDeg * dataExtent.standardAzimuthGridsize / 360.0)));

    return ivec3(closestBinIdx, closestAzimuthIdx, closestElevationIdx);
}


/** 
 * Sample radar data at world position pos.
 * Analogous to MRadarGrid::inverseTransformationInterpolation.
 */
float sampleRadarVolumeAtPos(in sampler3D dataVolume,
                             in DataVolumeExtent dataExtent,
                             in vec3 pos)
{
    ivec3 binAzElIndices = lonLatWorldZToRadarIndices(pos, dataExtent);

    // Return missing value if pos is outside the radar range.
    if (binAzElIndices.x == -100 || binAzElIndices.y == -100 || binAzElIndices.z == -100)
    {
        return M_MISSING_VALUE;
    }

    return sampleRadarVolumeAtRadarBinAzElIndices(dataVolume, binAzElIndices);
}


/** Converts radar indices of a point P to world coordinates,
 * clamped to the data range.
 */
vec3 radarIndicesToWorldClamped(in int elIndex,
                  in int azimuthIndex,
                  in int binIdx,
                  in DataVolumeExtent dataExtent)
{
    if (elIndex < 0)
    {
        elIndex = 0;
    }
    else if (elIndex > 9) {
        elIndex = 9;
    }

    if (binIdx < 0)
    {
        binIdx = 0;
    }
    else if (binIdx >= radarMetadata[elIndex].nbins)
    {
        binIdx = int(radarMetadata[elIndex].nbins - 1);
    }

    float elevationAngleDegrees = radarMetadata[elIndex].elevationAngleDegrees;

    // Centre of the grid cell: shifted by 0.5 to index for azimuth and range directions.
    azimuthIndex = azimuthIndex % int(radarMetadata[elIndex].nrays);
    float azimuthAngleDegrees = ((float(azimuthIndex)+0.5) * 360.0 / dataExtent.standardAzimuthGridsize);

    float rangeInMetres = radarMetadata[elIndex].rangeStart + (0.5 + float(binIdx)) * radarMetadata[elIndex].binDistance;

    vec3 pos = radarToLonLatMetre(elevationAngleDegrees,
                                  azimuthAngleDegrees,
                                  rangeInMetres,
                                  dataExtent.lonLatHeightRadar);

    pos.z = pos.z/worldZMeterScaling; // Height in world z.

    return pos;
}
