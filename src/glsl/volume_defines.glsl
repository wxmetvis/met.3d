#define ENABLE_HYBRID_PRESSURETEXCOORDTABLE
#define ENABLE_MINMAX_ACCELERATION
// Enables conversion of alpha values to extinction coefficients by applying an inverse beers law.
//#define USE_EXT_COEFF_CONVERSION