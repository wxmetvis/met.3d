/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

/****************************************************************************
***                             UNIFORMS                                  ***
*****************************************************************************/
uniform vec3        cameraPosition;
uniform vec3        lightDirection;


// Compute color of blinn-phong shaded surface
// Moved here from "simple_geometry_generation.fx.glsl"
void getBlinnPhongColor(in vec3 worldPos, in vec3 normalDir, in vec3 ambientColor,
in vec3 diffuseColor, out vec3 color)
{
    const vec3 lightColor = vec3(1,1,1);

    const vec3 kA = 0.3 * ambientColor;
    const vec3 kD = 0.5 * ambientColor;
    const float kS = 0.2;
    const float s = 10;

    const vec3 n = normalize(normalDir);
    const vec3 v = normalize(cameraPosition - worldPos);
    const vec3 l = normalize(-lightDirection); // specialCase
    const vec3 h = normalize(v + l);

    vec3 diffuse = kD * clamp(abs(dot(n,l)),0.0,1.0) * diffuseColor;
    vec3 specular = kS * pow(clamp(abs(dot(n,h)),0.0,1.0),s) * lightColor;

    color = kA + diffuse + specular;
}

// -----------------------------------------------------------------------------