/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Jessica Blankenburg [+]
**  Copyright 2016-2017 Marc Rautenhaus [*, previously +]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/


/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform mat4      mvpMatrix;
uniform float     worldZ;     // world z coordinate of this section
uniform vec4      glyphColor;

uniform vec2      bboxLons;          // western and eastern lon of the bbox
uniform vec2      bboxLats;          // southern and northern lat of the bbox

//----------------STREAMLINES---------------------

uniform float     minKnots;
uniform float     maxKnots;

uniform sampler1D transferFunction; // 1D transfer function
uniform bool      useTransferFunction;
uniform bool      fading;

interface VStoFS
{
    smooth float knots; // wind speed
    smooth float lon; // world space
    smooth float lat;
};


/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

shader VS(in vec3 worldXY, out VStoFS vsout)
{
    vsout.knots = worldXY.z;

    float x = worldXY.x;
    float y = worldXY.y;
    vec4 vecStreamLine = vec4(x, y, worldZ, 1.f);

    vsout.lon = x;
    vsout.lat = y;

    gl_Position = mvpMatrix * vecStreamLine;
}


/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/


shader FSStreamBarbs(in VStoFS fsin, out vec4 fragColor)
{
    // Discard the element if it is outside the bounding box.
    if (fsin.lon < bboxLons.x || fsin.lon > bboxLons.y
            || fsin.lat < bboxLats.x || fsin.lat > bboxLats.y)
    {
        discard;
    }

    // Discard the element if its speed is negative.
    if (fsin.knots < 0.)
    {
        discard;
    }

    if (useTransferFunction)
    {
        // Scale the scalar range to 0..1.
        float scalarTF = (fsin.knots - minKnots) / (maxKnots - minKnots);
        if (minKnots == maxKnots)
        {
            scalarTF = 0.0;
        }

        // Fetch colour from the transfer function and apply shading term.
        fragColor = texture(transferFunction, scalarTF);
    }
    else
    {
        fragColor = glyphColor;
    }

    // Fade streambarbs by speed.
    if (fading)
    {
        // If min data = max data, just use full opacity.
        if (minKnots == maxKnots)
        {
            fragColor.a = 1.0;
            return;
        }
        // fade so that minSpeed = 0.25 x opacity ; 75% of maxSpeed = 1.0 x opacity
        float knots01 = (fsin.knots - minKnots) / (maxKnots - minKnots);
        float opacityFactor = min(1.0, knots01 + 0.25);
        fragColor.a *= opacityFactor;
    }
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program StreamBarbs
{
    vs(330)=VS();
    fs(330)=FSStreamBarbs();
};

