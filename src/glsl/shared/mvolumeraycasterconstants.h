/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MVOLUMERAYCASTERCONSTANTS_H
#define MVOLUMERAYCASTERCONSTANTS_H

/**
 * This file is a shared header for both, C++ and GLSL code for the volume ray-caster.
 * This file can be included in both.
 * It will contain constants, that should be shared between both
 * sides of the code, to make updates to those constants easier,
 * and ease the use of those constants by other developers.
 */

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

/** Definitions for light limits */
const int MAX_NUM_DIRECTIONAL_LIGHTS = 3;
const int MAX_NUM_POINT_LIGHTS = 5;
const int MAX_NUM_SPOT_LIGHTS = 5;
const int MAX_NUM_RECT_LIGHTS = 5;

/** Definitions for reference scale units */
const int REF_SCALE_M = 0;
const int REF_SCALE_W = 1;

#endif // MVOLUMERAYCASTERCONSTANTS_H
