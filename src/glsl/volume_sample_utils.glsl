/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2018 Marc Rautenhaus
**  Copyright 2015      Michael Kern
**  Copyright 2017-2018 Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

// global extents of volume data corresponding to the variable and shading variable
uniform DataVolumeExtent dataExtent;
uniform DataVolumeExtent dataExtentSec;
uniform DataVolumeExtent dataExtentThird;

/******************************************************************************
***                               SUBROUTINES                               ***
*******************************************************************************/

// sample any data type at world position
subroutine float sampleDataAtPosType(vec3 pos);
subroutine vec3 computeGradientType(vec3 pos, in vec3 h);

// define subroutine uniform names
subroutine uniform sampleDataAtPosType      sampleDataAtPos;
subroutine uniform computeGradientType      computeGradient;


/******************************************************************************
***                            SAMPLE SUBROUTINES                           ***
*******************************************************************************/

// sample any kind of data at given world position
subroutine(sampleDataAtPosType)
float samplePressureLevel(in vec3 pos)
{
    return samplePressureLevelVolumeAtPos(dataVolume, dataExtent,
                                          pressureTable, pos);
}


#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
subroutine(sampleDataAtPosType)
float sampleHybridLevel(in vec3 pos)
{
    return sampleHybridSigmaVolumeAtPos_LUT(dataVolume, dataExtent,
                                            surfacePressure, hybridCoefficients,
                                            pressureTexCoordTable2D, pos);
}
#else
subroutine(sampleDataAtPosType)
float sampleHybridLevel(in vec3 pos)
{
    return sampleHybridSigmaVolumeAtPos(dataVolume, dataExtent,
                                        surfacePressure, hybridCoefficients,
                                        pos);
}
#endif


subroutine(sampleDataAtPosType)
float sampleAuxiliaryPressure(in vec3 pos)
{
    return sampleAuxiliaryPressureVolumeAtPos(dataVolume, dataExtent,
                                              auxPressureField3D_hPa, pos);
}


subroutine(sampleDataAtPosType)
float sampleRadarGrid(in vec3 pos)
{
    // inverse transformation interpolation
    return sampleRadarVolumeAtPos(dataVolume, dataExtent, pos);
}


#ifdef ALLOW_MULTIPLE_VARIABLES


// sample second variable slot if enabled
float sampleSecondVariable(in vec3 pos)
{
    float data = 0.0;

    if(dataExtentSec.levelType == 0)
    {
        data = samplePressureLevelVolumeAtPos(dataVolumeSec, dataExtentSec, pressureTableSec, pos);
    }
    else if(dataExtentSec.levelType == 1)
    {
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
        data = sampleHybridSigmaVolumeAtPos_LUT(dataVolumeSec, dataExtentSec, surfacePressureSec,
                                               hybridCoefficientsSec, pressureTexCoordTable2DSec, pos);
#else
        data = sampleHybridSigmaVolumeAtPos(dataVolumeSec, dataExtentSec, surfacePressureSec,
                                               hybridCoefficientsSec, pos);
#endif // END ENABLE_HYBRID_PRESSURETEXCOORDTABLE
    }
    else if(dataExtentSec.levelType == 6)
    {
        return sampleRadarVolumeAtPos(dataVolumeSec, dataExtentSec, pos);
    }
    else
    {
        data = sampleAuxiliaryPressureVolumeAtPos(dataVolumeSec, dataExtentSec,
                                                 auxPressureField3DSec_hPa, pos);
    }
    return data;
}


// sample third variable slot if enabled
float sampleThirdVariable(in vec3 pos)
{
    float data = 0.0;

    if(dataExtentThird.levelType == 0)
    {
        data = samplePressureLevelVolumeAtPos(dataVolumeThird, dataExtentThird, pressureTableThird, pos);
    }
    else if(dataExtentThird.levelType == 1)
    {
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
        data = sampleHybridSigmaVolumeAtPos_LUT(dataVolumeThird, dataExtentThird, surfacePressureThird,
                                               hybridCoefficientsThird, pressureTexCoordTable2DThird, pos);
#else
        data = sampleHybridSigmaVolumeAtPos(dataVolumeThird, dataExtentThird, surfacePressureThird,
                                               hybridCoefficientsThird, pos);
#endif // END ENABLE_HYBRID_PRESSURETEXCOORDTABLE
    }
    else if(dataExtentThird.levelType == 6)
    {
        data =  sampleRadarVolumeAtPos(dataVolumeThird, dataExtentThird, pos);
    }
    else
    {
        data = sampleAuxiliaryPressureVolumeAtPos(dataVolumeThird, dataExtentThird,
                                                      auxPressureField3DThird_hPa, pos);
    }
    return data;
}


/**
 Samples all variables that are currently available.
*/
void sampleEnabledVariables(vec3 position, out float[MAX_VARIABLES] scalars)
{
    for (int i = 0; i < MAX_VARIABLES; i++)
    {
        if (enabledVariableSlots[i])
        {
            if (i == 0)
            {
                scalars[i] = sampleDataAtPos(position);
            }
            if (i == 1)
            {
                scalars[i] = sampleSecondVariable(position);
            }
            if (i == 2)
            {
                scalars[i] = sampleThirdVariable(position);
            }
        }
        else
        {
            scalars[i] = M_MISSING_VALUE;
        }
    }
}

#endif // END ALLOW_MULTIPLE_VARIABLES


/******************************************************************************
***                            GRADIENT SUBROUTINES                         ***
*******************************************************************************/

/**
  Computes the gradient at position @p pos, using sampling step size @p h.

  Central differences are used for the computation, except for positions close
  to the boundaries of the data volume. Here, h is scaled and reduced to
  simple forward differences for positions less than one grid size away from
  the boundary. Also, correction is performed for missing values.

  For pressure level and hybrid sigma pressure level grids, the vertical
  difference hz used to compute the central/forward differences are scaled
  according to the vertical level distance (which will or can vary with height).
 */

vec3 gradientAtPos(in vec3 pos, in vec3 h, float ztop, float zbot)
{
    vec3 gradient;

    // 1. Sample with horizontal displacement. Check if the grid is cyclic in
    // longitude.

    vec3 pos_east;
    vec3 pos_west;
    if (dataExtent.gridIsCyclicInLongitude)
    {
        pos_east = vec3(pos.x + h.x, pos.yz);
        pos_west = vec3(pos.x - h.x, pos.yz);
    }
    else
    {
        // Non-cyclic grids: clamp to data boundary.
        pos_east = vec3(min(pos.x + h.x, dataExtent.dataSECrnr.x), pos.yz);
        pos_west = vec3(max(pos.x - h.x, dataExtent.dataNWCrnr.x), pos.yz);
    }

    float x1 = sampleDataAtPos(pos_east);
    float x2 = sampleDataAtPos(pos_west);
    if (x1 == M_MISSING_VALUE)
    {
        // Missing value correction: If the grid sampled at position pos.x + h.x
        // is missing, "fall back" to position pos.x. The gradient reduces to
        // a simple forward difference.
        pos_east = pos;
        x1 = sampleDataAtPos(pos_east);
    }
    if (x2 == M_MISSING_VALUE)
    {
//CHECKME (mr, 21Feb2020) -- in rare cases both sampled values could be
// missing. In such cases the gradient would be zero and we'd have a division
// by zero..
        pos_west = pos;
        x2 = sampleDataAtPos(pos_west);
    }
    float hx = pos_east.x - pos_west.x;

    vec3 pos_north = vec3(pos.x, min(pos.y + h.y, dataExtent.dataNWCrnr.y), pos.z);
    vec3 pos_south = vec3(pos.x, max(pos.y - h.y, dataExtent.dataSECrnr.y), pos.z);
    float y1 = sampleDataAtPos(pos_north);
    float y2 = sampleDataAtPos(pos_south);
    if (y1 == M_MISSING_VALUE)
    {
        pos_north = pos;
        y1 = sampleDataAtPos(pos_north);
    }
    if (y2 == M_MISSING_VALUE)
    {
        pos_south = pos;
        y2 = sampleDataAtPos(pos_south);
    }
    float hy = pos_north.y - pos_south.y;

    // 2. Sample with vertical displacement, considering data volume
    // boundaries.

    vec3 pos_top = vec3(pos.xy, min(pos.z + h.z, ztop));
    vec3 pos_bot = vec3(pos.xy, max(pos.z - h.z, zbot));
    float z1 = sampleDataAtPos(pos_top);
    float z2 = sampleDataAtPos(pos_bot);
    if (z1 == M_MISSING_VALUE)
    {
        pos_top = pos;
        z1= sampleDataAtPos(pos_top);
    }
    if (z2 == M_MISSING_VALUE)
    {
        pos_bot = pos;
        z2= sampleDataAtPos(pos_bot);
    }
    float hz = pos_top.z - pos_bot.z;

    // 3. Compute gradient.
    
    gradient = vec3((x1 - x2) / abs(hx), (y1 - y2) / abs(hy), (z1 - z2) / abs(hz));

    return normalize(gradient);
}


vec3 gradientAtRadarPos(in vec3 pos)
{
    vec3 gradient;

    ivec3 binAzElIndices = lonLatWorldZToRadarIndices(pos, dataExtent);
    int binIdx = binAzElIndices.x;
    int azIdx = binAzElIndices.y;
    int elIdx = binAzElIndices.z;
    if (binIdx  < 0 || azIdx < 0 || elIdx < 0)
    {
        return vec3(0, 0, 0);
    }

    // 1) Neighboring grid cells in azimuth direction.
    // World space positions of grid cells neighboring pos in azimuth direction.
    vec3 pos_left = radarIndicesToWorldClamped(elIdx, azIdx - 1, binIdx, dataExtent);
    vec3 pos_right = radarIndicesToWorldClamped(elIdx, azIdx + 1, binIdx, dataExtent);

    // Data values of grid cell neighbors in azimuth direction.
    float vAz1 = sampleDataAtPos(pos_left);
    float vAz2 = sampleDataAtPos(pos_right);
    if (vAz1 == M_MISSING_VALUE)
    {
        pos_left = pos;
        vAz1 = sampleDataAtPos(pos_left);
    }
    if (vAz2 == M_MISSING_VALUE)
    {
        pos_right = pos;
        vAz2 = sampleDataAtPos(pos_right);
    }
    vec3 azVec = pos_right - pos_left;
    azVec /= length(azVec);

    // 2) Neighboring grid cells in range direction.
    // World space positions of grid cells neighboring pos in range direction.
    vec3 pos_outer = radarIndicesToWorldClamped(elIdx, azIdx, binIdx + 1, dataExtent);
    vec3 pos_inner = radarIndicesToWorldClamped(elIdx, azIdx, binIdx - 1, dataExtent);

    // Data values of grid cell neighbors in bin direction.
    float vBin1 = sampleDataAtPos(pos_outer);
    float vBin2 = sampleDataAtPos(pos_inner);
    if (vBin1 == M_MISSING_VALUE)
    {
        pos_outer = pos;
        vBin1 = sampleDataAtPos(pos_outer);
    }
    if (vBin2 == M_MISSING_VALUE)
    {
        pos_inner = pos;
        vBin2 = sampleDataAtPos(pos_inner);
    }
    vec3 binVec = pos_outer - pos_inner;
    binVec /= length(binVec);

    // 3) Neighboring grid cells in elevation direction.
    // World space positions of grid cells neighboring pos in elevation direction.
    vec3 pos_top = radarIndicesToWorldClamped(elIdx + 1, azIdx, binIdx, dataExtent);
    vec3 pos_bot = radarIndicesToWorldClamped(elIdx - 1, azIdx, binIdx, dataExtent);

    // Data values of grid cell neighbors in elevation direction.
    float vEl1 = sampleDataAtPos(pos_top);
    float vEl2 = sampleDataAtPos(pos_bot);
    if (vEl1 == M_MISSING_VALUE)
    {
        pos_top = pos;
        vEl1= sampleDataAtPos(pos_top);
    }
    if (vEl2 == M_MISSING_VALUE)
    {
        pos_bot = pos;
        vEl2= sampleDataAtPos(pos_bot);
    }
    vec3 elVec = pos_top - pos_bot;
    elVec /= length(elVec);

    // Compute gradient.
    gradient = (vAz1-vAz2) * azVec + (vBin1-vBin2) * binVec + (vEl1-vEl2) * elVec;

    return normalize(gradient);
}


subroutine(computeGradientType)
vec3 pressureLevelGradient(in vec3 pos, in vec3 h)
{
    // Find the pressue levels enclosing pos.z to estimate hz for the
    // vertical difference.

    // Compute pressure from world z coordinate.
    float p = exp(pos.z / pToWorldZParams.y + pToWorldZParams.x);

    // Binary search to find model levels k, k1 that enclose pressure level p.
    int k = 0;
    int k1 = dataExtent.nLev - 1;
    int vertOffset = dataExtent.nLon + dataExtent.nLat;

    while (abs(k1 - k) > 1)
    {
        int kMid = (k + k1) / 2;
        float pMid = texelFetch(lonLatLevAxes, kMid + vertOffset, 0).r;
        if (p >= pMid) k = kMid; else k1 = kMid;
    }

    float lnPk  = log(texelFetch(lonLatLevAxes, k + vertOffset, 0).r);
    float lnPk1 = log(texelFetch(lonLatLevAxes, k1 + vertOffset, 0).r);

    float hz1 = (lnPk - pToWorldZParams.x) * pToWorldZParams.y;
    float hz2 = (lnPk1 - pToWorldZParams.x) * pToWorldZParams.y;
    vec3 h_ = vec3(h.x, h.y, abs(hz1 - hz2));

    float ztop = dataExtent.dataNWCrnr.z;
    float zbot = dataExtent.dataSECrnr.z;

    return gradientAtPos(pos, h_, ztop, zbot);
}


subroutine(computeGradientType)
vec3 hybridLevelGradient(in vec3 pos, in vec3 h)
{
    // Determine the approximate distance between the model levels above and
    // below the current position ("hz").
    float zbot, ztop;
    getHybridSigmaBotTopLevelAtPos(dataExtent, surfacePressure,
                                   hybridCoefficients, pos, zbot, ztop);

    float hz = getHybridApproxWorldZLevelDistanceAtPos(dataExtent, surfacePressure,
                                                       hybridCoefficients, pos);
    vec3 h_ = vec3(h.x, h.y, hz);
                                                       
    return gradientAtPos(pos, h_, ztop, zbot);
}


subroutine(computeGradientType)
vec3 auxiliaryPressureGradient(in vec3 pos, in vec3 h)
{
    // Determine the approximate distance between the model levels above and
    // below the current position ("hz").
    float zbot, ztop;
    getAuxiliaryPressureBotTopLevelAtPos(dataExtent, auxPressureField3D_hPa,
                                         pos, zbot, ztop);

    float hz = getAuxiliaryPressureApproxWorldZLevelDistanceAtPos(
                dataExtent, auxPressureField3D_hPa, pos);
    vec3 h_ = vec3(h.x, h.y, hz);

    return gradientAtPos(pos, h_, ztop, zbot);
}


subroutine(computeGradientType)
    vec3 radarGradient(in vec3 pos, in vec3 h)
{
    return gradientAtRadarPos(pos);
}


/******************************************************************************
***                          RAYCASTING SAMPLE UTILS                        ***
*******************************************************************************/
#ifdef RAYCASTER
/**
  Computes the intersection points of a ray with a box. Returns tnear and
  tfar (packed in a vec2), which are the entry and exit parameters of the
  ray into the box when a position pos on the ray is described by
        pos = ray.origin + t * ray.direction

  boxCrnr1 and boxCrnr2 are two opposite corners of the box.

  Literature: Williams et al. (2005), "An efficient and robust ray-box
  intersection algorithm." (notes 29/03/2012).

  Note (tv, 26Oct2023): This function was edited due to a bug, where
  intersections with a surface were detected, when the ray is parallel
  to the surface, and lies within that surface.
  Even if another surface was the true exit, the parallel surface was chosen.
  However, the function returned false in that case, even though a true exit might exist.
  So instead, the function now only checks intersections with surfaces that are actually crossed
  and not in parallel with the ray.
  */
bool rayBoxIntersection(
        in Ray ray, in vec3 boxCrnr1, in vec3 boxCrnr2, out vec2 tNearFar)
{
    float tnear = -1.0 / 0.0;
    float tfar  = 1.0 / 0.0;

    vec3 rayDirInv = vec3(1./ray.direction.x, 1./ray.direction.y,
                          1./ray.direction.z);

    if(ray.direction.x > 0.0)
    {
        tnear = max(tnear, (boxCrnr1.x - ray.origin.x) * rayDirInv.x);
        tfar  = min(tfar,  (boxCrnr2.x - ray.origin.x) * rayDirInv.x);
    }
    else if(ray.direction.x < 0.0)
    {
        tnear = max(tnear, (boxCrnr2.x - ray.origin.x) * rayDirInv.x);
        tfar  = min(tfar,  (boxCrnr1.x - ray.origin.x) * rayDirInv.x);
    }

    if(ray.direction.y > 0.0)
    {
        tnear = max(tnear, (boxCrnr1.y - ray.origin.y) * rayDirInv.y);
        tfar  = min(tfar,  (boxCrnr2.y - ray.origin.y) * rayDirInv.y);
    }
    else if(ray.direction.y < 0.0)
    {
        tnear = max(tnear, (boxCrnr2.y - ray.origin.y) * rayDirInv.y);
        tfar  = min(tfar,  (boxCrnr1.y - ray.origin.y) * rayDirInv.y);
    }

    if(ray.direction.z > 0.0)
    {
        tnear = max(tnear, (boxCrnr1.z - ray.origin.z) * rayDirInv.z);
        tfar  = min(tfar,  (boxCrnr2.z - ray.origin.z) * rayDirInv.z);
    }
    else if(ray.direction.z < 0.0)
    {
        tnear = max(tnear, (boxCrnr2.z - ray.origin.z) * rayDirInv.z);
        tfar  = min(tfar,  (boxCrnr1.z - ray.origin.z) * rayDirInv.z);
    }

    tNearFar = vec2(tnear, tfar);
    return (tnear < tfar) && !(isinf(tnear) && isinf(tfar));
}


bool insideBox(
        in Ray ray, in float t, in vec3 boxCrnr1, in vec3 boxCrnr2)
{
    vec3 rayPosition = ray.origin + t * ray.direction;
    if (rayPosition.x < min(boxCrnr1.x, boxCrnr2.x)) return false;
    if (rayPosition.x > max(boxCrnr1.x, boxCrnr2.x)) return false;
    if (rayPosition.y < min(boxCrnr1.y, boxCrnr2.y)) return false;
    if (rayPosition.y > max(boxCrnr1.y, boxCrnr2.y)) return false;
    if (rayPosition.z < min(boxCrnr1.z, boxCrnr2.z)) return false;
    if (rayPosition.z > max(boxCrnr1.z, boxCrnr2.z)) return false;
    return true;
}


int computeCrossingLevel(in float scalar)
{
    int level = 0;

    // Isovalues are sorted in MLonLatMultiPurposeVolumeActor
    // ::RayCasterSettings::sortIsoValues() according to increasing value.
    // Disabled values are put to the back of the array and the number of
    // enabled values is passed to the shader. Hence we do not need to test
    // for an isovalue to be enabled here; all values with indices 
    // < numIsoValues are enabled and valid.
    
    for (int i = 0; i < numIsoValues; i++)
        level += int(scalar >= isoValues[i]);

    return level;
}


void bisectionCorrection(inout vec3 rayPosition, inout float lambda,
                         in vec3 prevRayPosition, in float prevLambda,
                         inout int crossingLevelFront, inout int crossingLevelBack)
{
    vec3 rayCenterPosition;
    float centerLambda;
    int crossingLevelCenter;

    for (int i = 0; i < bisectionSteps; ++i)
    {
        rayCenterPosition = (rayPosition + prevRayPosition) / 2.0;
        centerLambda = (lambda + prevLambda) / 2.0;

        const float scalar = sampleDataAtPos(rayCenterPosition);

        crossingLevelCenter = computeCrossingLevel(scalar);

        if (crossingLevelCenter != crossingLevelBack)
        {
            rayPosition = rayCenterPosition;
            lambda = centerLambda;
            crossingLevelFront = crossingLevelCenter;
        }
        else
        {
            prevRayPosition = rayCenterPosition;
            prevLambda = centerLambda;
            crossingLevelBack = crossingLevelCenter;
        }
    }
}


// depth computation
float computeDepthFromWorld(in vec3 rayPosition)
{
    // first convert position to clip space
    vec4 clipSpacePos = mvpMatrix * vec4(rayPosition,1);
    // perform perspective division
    clipSpacePos.z /= clipSpacePos.w;
    // -> z = OPENGL ? [-1;1] : [0;1]
    // calculate depth value -> convert to [0,1]
    return clipSpacePos.z * 0.5 + 0.5;
}


vec3 initGradientSamplingStep(in DataVolumeExtent dve)
{
    // dve.deltaLnP is not used... (instead computed in gradient methods).
    return vec3(dve.deltaLon, dve.deltaLat, dve.deltaLnP);
}

#endif