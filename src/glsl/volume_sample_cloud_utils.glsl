/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Thorwin Vogt
**  Copyright 2018 Florian Maerkl
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

// requires volume_global_structs_utils.glsl
// requires volume_<datatype>_utils.glsl
// requires volume_sample_utils.glsl
// requires volume_raycaster.glsl
// requires volume_defines.glsl

#define EARTH_RADIUS 6.3781e6
#define M_PI 3.1415926535897932384626433832795
#define EARTH_PERIMETER_METERS 2 * M_PI * EARTH_RADIUS

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform int ciwcSlot;
uniform int clwcSlot;
uniform vec4 ciwcColor;
uniform vec4 clwcColor;
uniform bool useShadingVarForCloudColor;
uniform bool uniformViewScale;
uniform bool earthCurvatureEnabled;
uniform float spaceScale;

// TODO: (tv, 14Nov2023) This is a constant value specifing a rough conversion between z axis units and meters.
//      This is, however, not accurate and should rather use an ICAO geometric height conversion from pressure.
//      Due to the uniform scaling setting, this is not easily doable though,
//      as it applies the same scale on the x and y axis, which dont have corresponding pressure values to their unit values.

/*****************************************************************************
 ***                             FUNCTIONS
 *****************************************************************************/

/**
  Blends rgba colors together, wheighed by their alpha channel.
 */
vec4 blendColors(in vec4 color1, in vec4 color2)
{
    vec4 res = vec4(0.0);
    res.a = color1.a + color2.a;

    if (res.a == 0.0)
    {
        res.rgb = vec3(0.0);
    }
    else
    {
        res.rgb = (color1.rgb * color1.a + color2.rgb * color2.a) / res.a;
    }

    return res;
}


/**
  Calculates the relative scene vertical scaling in regards to the default scale.
  This can be used to adjust the alpha values from transfer functions, so that opacity
  stays the same on the vertical axis, regardless of the vertical scene scale.
 */
float getSceneScale(in vec3 rayPosIncrement)
{
    float scale = 1.0;

    if (scalingFixMode == SCALE_FIX_NORMAL)
    {
        scale = mix(1.0, (verticalScaling / DEFAULT_VERTICAL_SCALE), abs(normalize(rayPosIncrement)).z);
    }
    else if (scalingFixMode == SCALE_FIX_PHYS)
    {
        scale = verticalScaling / DEFAULT_VERTICAL_SCALE;
    }

    return scale;
}


/**
  Calculates the distance in meters from the coordinate center
  to the given lon/lat coordinates.
 */
vec2 lonLatToMeters(vec2 lonLat)
{
    vec2 ret = vec2((lonLat.x / 360.0) * EARTH_PERIMETER_METERS,
                    (lonLat.y / 360.0) * EARTH_PERIMETER_METERS)
            * spaceScale;

    if (earthCurvatureEnabled)
    {
        float latRad = lonLat.y * M_PI / 180.0;
        ret.x *= cos(latRad);
    }

    return ret;
}


/**
  Converts a given world position (lon, lat, z)
  to a coordinate system using meters. This means the
  returned vector contains the distance in meters along lon lat and z axis to
  the given point form the world origin at 0 degrees lon, 0 degrees lat.
 */
vec3 worldPosToMeters(vec3 pos)
{
    if (uniformViewScale)
    {
        return vec3(pos * worldZMeterScaling * spaceScale);
    }
    else
    {
        return vec3(lonLatToMeters(pos.xy), pos.z * worldZMeterScaling * spaceScale);
    }
}


/**
  Calculates the distance in meters gone by starting at rayPosition (lon, lat, z)
  and going algon rayPosIncrement (lon, lat, z).
  TODO: (tv, 3may2023) Projected grids wont work?
 */
float worldStepToMeters(in vec3 rayPosition, in vec3 rayPosIncrement)
{
    vec3 dstMeters = worldPosToMeters(rayPosition + rayPosIncrement);
    vec3 posMeters = worldPosToMeters(rayPosition);
    return length(dstMeters - posMeters);
}


/**
  Converts the given world z coordinate into pressure in hPa.
*/
float worldZToPressure(in float worldZ)
{
    return exp(worldZ / pToWorldZParams.y + pToWorldZParams.x);
}


/**
  Converts the given pressure in hPa to the corresponding world Z coordinate.
*/
float pressureToWorldZ(in float pressure)
{
    return (log(pressure) - pToWorldZParams.x) * pToWorldZParams.y;
}


/**
  Used to correct transfer function alpha to correctly blend with clouds by converting it to
  an extinction coefficient.
 */
float convertToExtinctionCoeff(
        in float alpha, in vec3 worldPosition, in vec3 rayIncrement,
        in float unitStepSizeM, in float stepSizeM)
{
    float scale = getSceneScale(rayIncrement);

#ifndef USE_EXT_COEFF_CONVERSION
    // This is a simple "visual" conversion of alpha values to extinction coefficients,
    // that tries to keep the same visual as the old raycaster for the same alpha values.

    // The distance of a single reference unit step in world units.
    // Default is 1 world unit.
    float unitStep = 1.0;
    if (refScaleUnit == REF_SCALE_M)
    {
        // Set the unit step to one meter in world units.
        unitStep = 1.0 / unitStepSizeM;
    }

    return alpha * (length(rayIncrement) / (refScale * unitStep)) / scale / stepSizeM;
#else
    // This converts alpha values to extinction coefficients, by applying Beer's law inversely.
    if (alpha > 0)
    {
        return -log(1 - min(alpha, 0.999999)) / stepSizeM;
    }
    else
    {
        return 0;
    }
#endif
}


/**
  Calculates air density from hPa in [kg / m^3].
 */
float calculateAirDensity(float pressure)
{
    float temp = 20.0;

    float gasConstant = 287.058;
    float densityOfAir = pressure * 100.0 / (gasConstant * (temp + 273.15));//[kg / m^3]

    return densityOfAir;
}


/**
  Calculate IWC extinction coefficient from a given value at a given world gl_Position.
 */
float calculateIwcExtCoeff(in float value, in vec3 worldPosition)
{
    if (value < 0.0f) return 0.0f;

    float p = exp(worldPosition.z / pToWorldZParams.y + pToWorldZParams.x);

    float densityOfAir = calculateAirDensity(p);//[kg / m^3]

    float iceDensity = 917.0; //[kg / m^3]
    float radiusM = 25.0 / 1000000.0; //effective mean radius of ice water droplets in meters [m]

    float IWC = value;
    float IWCExtCoeff = (4.0 * sqrt(3.0) / 3.0) * IWC * densityOfAir / (iceDensity * radiusM);

    return IWCExtCoeff;
}


/**
  Calculate LWC extinction coefficient from a given value at a given world position.
 */
float calculateLwcExtCoeff(in float value, in vec3 worldPosition)
{
    if (value < 0.0f) return 0.0f;

    float p = exp(worldPosition.z / pToWorldZParams.y + pToWorldZParams.x);

    float densityOfAir = calculateAirDensity(p);//[kg / m^3]

    float waterDensity = 1000.0; //[kg / m^3]
    float radiusM = (10 / 1000000.0); //effective mean radius of liquid water droplets in meters

    float LWC = value;
    float LWCExtCoeff = 1.5 * LWC * densityOfAir / (waterDensity * radiusM);

    return LWCExtCoeff;
}


/**
  Return the colour of CIWC clouds at the given world position for scalar value value.
 */
vec4 getCiwcColour(in float value, in vec3 worldPosition)
{
    vec4 baseColour = vec4(0.0f);

    if (useShadingVarForCloudColor)
    {
        float scalar = sampleShadingDataAtPos(worldPosition);

        float t = (scalar - dataExtentShV.tfMinimum)
                      / (dataExtentShV.tfMaximum - dataExtentShV.tfMinimum);
        baseColour = texture(transferFunctionShV, t);
    }
    else
    {
        baseColour = ciwcColor;
    }

    baseColour.a *= calculateIwcExtCoeff(value, worldPosition);

    return baseColour;
}


/**
  Return the colour of CLWC clouds at the given world position for scalar value value.
 */
vec4 getClwcColour(in float value, in vec3 worldPosition)
{
    vec4 baseColour = vec4(0.0f);

    if (useShadingVarForCloudColor)
    {
        float scalar = sampleShadingDataAtPos(worldPosition);

        float t = (scalar - dataExtentShV.tfMinimum)
                      / (dataExtentShV.tfMaximum - dataExtentShV.tfMinimum);
        baseColour = texture(transferFunctionShV, t);
    }
    else
    {
        baseColour = clwcColor;
    }

    baseColour.a *= calculateLwcExtCoeff(value, worldPosition);

    return baseColour;
}
