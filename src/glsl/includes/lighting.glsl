/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
/**
 * This file provides functions to apply scene lighting onto the object.
 * Occupied binding points: 3 and 4
 * Used texture units: Defined by you when binding. One is needed for the shadow map.
 * Make sure you do not use these in your shaders as well.
 * Usage:
 * Include this file in your shader.
 *
 * getShadow() returns a percentage of light at a given world position. Essentially,
 * it will give you a value to multiply your fragment color with to apply the shadow map.
 *
 * getBlinnPhongColor() will apply the blinn phong color model on the given fragment.
 * The input diffuse color is the non-lit fragment color, meaning the color it should be
 * if no light and shadows exist. This function will also already apply the shadow map.
 *
 * Only use either getShadow() or getBlinnPhongColor(), as the blinn phong model
 * already applies the shadow map.
 *
 * On the C++ side, you only need to bind the shadow map to some texture unit
 * and call MSceneViewGLWidget::bindShadowMap() to bind it to your shader.
 * If you do not do this, your might get OpenGL errors or a black fragment.
 */

/****************************************************************************
***                             UNIFORMS                                  ***
*****************************************************************************/

struct Light
{
    /**
     * The position of the light in world space.
     * Vec4, as it is not advised to use vec3 in buffers due to padding.
     */
    vec4 positionWS; // Base alignment 0 bytes

    /**
     * The direction of the light in world space.
     */
    vec4 directionWS; // Base alignment 16 bytes

    /**
     * The color of the light.
     */
    vec4 color; // Base alignment 32 bytes

    /**
     * The right vector of the rectangle of a rect light.
     * The w component is the width of the rectangle.
     */
    vec4 rectRight; // Base alignment 48 bytes

    /**
     * The up vector of the rectangle of a rect light.
     * The w component is the height of the rectangle.
     */
    vec4 rectUp; // Base alignment 64 bytes

    /**
     * The intensity of the light.
     */
    float intensity; // Base alignment 80 bytes

    /**
     * The cosine of the opening angle of the spotlight cone.
     */
    float spotlightAngle; // Base alignment 84 bytes

    /**
     * The cosine of the half angle of the spotlight cone.
     */
    float spotlightHalfAngle;  // Base alignment 88 bytes

    /**
     * The type of the light.
     * 0 = Directional light,
     * 1 = Point light,
     * 2 = Spot light,
     * 3 = Rect light
     */
    int type; // Base alignment 92 bytes

    // Size is 96 bytes.
};

layout (std430, binding=3) buffer Lights
{
    Light sceneLights[];
};

layout (std140, binding=4) uniform Lighting
{
    mat4 shadowLightMatrix[3];  // Base alignment 0, length 192
    mat4 camViewMatix;          // Base alignment 192, length 64
    vec3 shadowLightDirection;  // Base alignment 256, length 12
    bool shadowMapAvailable;    // Base alignment 268, length 4
    vec3 ambientLightColor;     // Base alignment 272, length 12
    float ambientLight;         // Base alignment 284, length 4
    vec3 cameraPositionWS;      // Base alignment 288, length 12
    float diffuseLight;         // Base alignment 300, length 4
    vec2 shadowMapTextMapScale; // Base alignment 304, length 8
    float specularLight;        // Base alignment 312, length 4
    float splitDistance1;       // Base alignment 316, length 4
    float splitDistance2;       // Base alignment 320, length 4
    float splitDistance3;       // Base alignment 324, length 4
    bool receiveShadows;        // Base alignment 328, length 4
    int shadowMapSampleCount;   // Base alignment 332, length 4
};

// Sampler has to be outside of uniform block.
uniform sampler2DMSArray sceneShadowMap;

// The min intensity of a light, below which it will be ignored.
// This is mainly used for light fallof.
const float lightIntensityLimit = 0.0001f;

/**
 * Shadow map texture lookup with a given offset at the given st location and sample s.
 */
float offset_lookup(vec4 loc, vec2 offset, int layer, int s)
{
    float depth = texelFetch(sceneShadowMap, ivec3(loc.st / shadowMapTextMapScale + offset, layer), s).x;
    return depth;
}

/**
 * Get the shadow percentage at the given world position calculated in the shadow
 * pass. 0 means full shadow, 1 means full light.
 * This method can be used to only draw the shadow map without any other light
 * interactions.
 * The shadow map is a cascading shadow map with 3 splits, fitted around the
 * cameras view frustum. The frustum is split into 3 areas, each one with its
 * own shadow map, in order to increase detail.
 * When no normal is available for you to use, input a vec3(0), so a fixed
 * shadow bias is used, instead of one based on the normal.
 * @return The shadow / light value.
 */
float getShadow(in vec3 worldPosition, in vec3 normal)
{
    if (!shadowMapAvailable || !receiveShadows) return 1.0;
    
    vec4 viewPos = camViewMatix * vec4(worldPosition, 1.0);
    float fragDistance = abs(viewPos.z);

    // Shadow map split distances for each shadow cascade.
    float splitDistances[3];
    splitDistances[0] = splitDistance1;
    splitDistances[1] = splitDistance2;
    splitDistances[2] = splitDistance3;

    // Find the shadow cascade that is relevant for this fragment.
    int layer = -1;
    for (int i = 0; i < 3; i++)
    {
        if (fragDistance < splitDistances[i])
        {
            layer = i;
            break;
        }
    }

    // The fragment is further from the camera than any shadowmap gets rendered.
    // Return full light.
    if (layer == -1)
    {
        return 1;
    }

    mat4 lightMatrix = shadowLightMatrix[layer];
    vec4 lightSpacePos = lightMatrix * vec4(worldPosition, 1.0);

    // If a normal is given, shift the sampling location a bit in the direction
    // of the normal vector. This should reduce shadow artefacts.
    if (normal != vec3(0))
    {
        vec3 wpNormalEnd = worldPosition + normal;
        vec4 lightSpaceNormalEnd = lightMatrix * vec4(wpNormalEnd, 1.0);
        vec4 lightSpaceNormal = normalize(lightSpaceNormalEnd - lightSpacePos);
        lightSpacePos.xy = lightSpacePos.xy + lightSpaceNormal.xy * shadowMapTextMapScale * 5;
    }

    lightSpacePos = lightSpacePos * 0.5 + 0.5;

    // Calculate a suitable shadow bias. Uses the angle between light direction and normal
    // to increase or decrease the shadow bias. The bias values were just experimented
    // with to give good results.
    float bias = mix(0.0005f, 0.0005f, abs(dot(normal, normalize(shadowLightDirection))));
    bias *= 1.0f / (splitDistances[2] / splitDistances[layer]);

    // Do not sample outside the shadow map
    if (lightSpacePos.z < 0.0) return 1;

    lightSpacePos.z = min(lightSpacePos.z, 1.0);

    float depthFromLight = lightSpacePos.z;
    vec2 texelCoord = lightSpacePos.xy * shadowMapTextMapScale;

    lightSpacePos.z = depthFromLight - bias;

    // Total shadow value.
    float shadow = 0.0f;
    // Kernel size, has to currently be 2.
    const int kernelSize = 2;
    // The amount of texels to sample.
    const int kernelSampleCount = kernelSize * kernelSize;

    // Weights for the different texels in the kernel.
    vec2 fracts = fract(lightSpacePos.xy / shadowMapTextMapScale);

    // Individual shadow values of the different texels in the kernel.
    float shadowValues[kernelSampleCount];

    // Initializes the shadow values.
    for (int i = 0; i < kernelSampleCount; i++)
    {
        shadowValues[i] = 0;
    }

    // Calculate all shadow values for the different texels in the kernel.
    int j = 0;
    for (int y = 0; y < kernelSize; y++)
    {
        for (int x = 0; x < kernelSize; x++)
        {
            // Sample all MSAA samples of the shadow map.
            // We could actually cap it to 8 but whatever.
            for (int i = 0; i < shadowMapSampleCount; i++)
            {
                float depth = offset_lookup(lightSpacePos, vec2(x,y), layer, i);

                // If the sampled depth is nearer than the fragment position, we have a shadow.
                if (depth <= lightSpacePos.z)
                {
                    shadowValues[j] += (1.0f / float(shadowMapSampleCount));
                }
            }
            j++;
        }
    }

    // Interpolate all the different shadow values of the different texels in the kernel.
    shadow = mix(mix(shadowValues[0], shadowValues[1], fracts.x),
    mix(shadowValues[2], shadowValues[3], fracts.x), fracts.y);

    return 1.0f - shadow;
}

/**
 * Calculates the light intensity of the given light at the
 * given world position.
 * @return 0, if no light reaches the position.
 */
float getLightIntensity(in vec3 pToL, in Light light, in vec3 normal)
{
    vec3 dir = pToL;
    float distance = length(dir);
    float distSqr = distance * distance;

    if (light.type == 0)
    {
        return light.intensity;
    }
    if (light.type == 1)
    {
        float intensity = light.intensity / max(distSqr, 0.01 * 0.01);

        if (intensity < lightIntensityLimit) return 0;

        return intensity;
    }
    if (light.type == 2)
    {
        float theta = dot(normalize(dir), normalize(-light.directionWS.xyz));
        float epsilon   = light.spotlightHalfAngle - light.spotlightAngle;
        float intensity = clamp((theta - light.spotlightAngle) / epsilon, 0.0, 1.0);
        intensity = (intensity * light.intensity) / max(distSqr, 0.01 * 0.01);
        return intensity;
    }
    if (light.type == 3)
    {
        // Source: Lagarde, Sebastien and Charles De Rousiers,
        // "Moving Frostbite to Physically Based Rendering 3.0",
        // SIGGRAPH Course Physically Based Shading in Theory and Practice 3 (2014)
        // https://www.ea.com/frostbite/news/moving-frostbite-to-pb,
        // Course Notes, page 40ff.

        if (dot(-normalize(pToL), light.directionWS.xyz) <= 0)
        {
            return 0;
        }

        vec3 pos = light.positionWS.xyz - pToL;
        vec3 right = light.rectRight.xyz * light.rectRight.w / 2.0f;
        vec3 up = light.rectUp.xyz * light.rectUp.w / 2.0f;

        vec3 ll = (light.positionWS.xyz - right - up);
        vec3 lr = (light.positionWS.xyz + right - up);
        vec3 tl = (light.positionWS.xyz - right + up);
        vec3 tr = (light.positionWS.xyz + right + up);

        vec3 v0 = tr - pos;
        vec3 v1 = lr - pos;
        vec3 v2 = ll - pos;
        vec3 v3 = tl - pos;

        vec3 n0 = normalize(cross(v0, v1));
        vec3 n1 = normalize(cross(v1, v2));
        vec3 n2 = normalize(cross(v2, v3));
        vec3 n3 = normalize(cross(v3, v0));
        float g0 = acos(dot(-n0, n1));
        float g1 = acos(dot(-n1, n2));
        float g2 = acos(dot(-n2 , n3));
        float g3 = acos(dot(-n3, n0));

        float solidAngle = g0 + g1 + g2 + g3 - 2.0 * 3.14159265359;

        float intensity = solidAngle * 0.2 * (
        clamp(dot(normalize(v0), normal) +
        clamp(dot(normalize(v1), normal), 0.0, 1.0) +
        clamp(dot(normalize(v2), normal), 0.0, 1.0) +
        clamp(dot(normalize(v3), normal), 0.0, 1.0) +
        clamp(dot(normalize(pToL), normal), 0.0f, 1.0), 0.0, 1.0));

        return light.intensity * intensity;
    }

    return 0;
}

/**
 * Calculates the color of the rendered fragment with Blinn-Phong lighting.
 * This iterates over all scene lights and calculates the lighting from them.
 * @param worldPos World position of the rendered fragment.
 * @param normal Fragment normal at world position.
 * @param objectColor The color of the fragment / object.
 * @param shininess The shininess of the fragment.
 * @param diffuse Optional: Diffuse amount, [0, 1], default 1.0.
 * @param specular Optional: Specular amount, [0, 1], default 0.2.
 * @param ambient Optional: Ambient light amount, [0, 1], default 0.3.
 * @return The resulting color when lighting and shadows are applied.
 */
vec3 getBlinnPhongColor(in vec3 worldPos, in vec3 normal,
                        in vec3 objectColor, in float shininess, in float diffuse,
                        in float specular, in float ambient)
{
    vec3 n = normalize(normal);
    vec3 v = normalize(cameraPositionWS - worldPos);

    vec3 kA = ambient * ambientLightColor;
    float kD = diffuse;
    float kS = specular;

    vec3 color = vec3(0);

    vec4 lightColor;
    vec3 lightDir;

    for (int i = 0; i < sceneLights.length(); ++i)
    {
        Light light = sceneLights[i];
        vec3 dir = light.positionWS.xyz - worldPos;

        float intensity = getLightIntensity(dir, light, normal);

        if (intensity == 0) continue;

        lightColor = light.color * intensity;

        if (light.type == 1)
        {
            // For point lights, the light emits to all directions equally.
            lightDir = normalize(-dir);
        }
        else
        {
            lightDir = light.directionWS.xyz;
        }

        float lambertian = max(dot(n, -lightDir), 0.0);

        vec3 halfDir = normalize(-lightDir + v);
        float specAngle = max(dot(halfDir, n), 0.0);
        float s = pow(specAngle, shininess);

        vec3 diffuseC = kD * lambertian * lightColor.rgb;
        vec3 specularC = kS * s * lightColor.rgb;

        color += diffuseC + specularC;

        // The first light has a shadow map attached.
        if (i == 0 && light.type == 0)
        {
            color *= getShadow(worldPos, normal);
        }
    }

    color += kA;

    return color * objectColor;
}
vec3 getBlinnPhongColor(in vec3 worldPos, in vec3 normal,
in vec3 objectColor, in float shininess)
{
    return getBlinnPhongColor(worldPos, normal, objectColor, shininess,
    diffuseLight, specularLight, ambientLight);
}

vec3 getBlinnPhongColor(in vec3 worldPos, in vec3 normal,
in vec3 objectColor)
{
    return getBlinnPhongColor(worldPos, normal, objectColor, 10.0f,
    diffuseLight, specularLight, ambientLight);
}

/**
 * Calculates the color of the rendered fragment with Blinn-Phong lighting.
 * This is a simplified implementation ignoring scene lights and shadows.
 * This is usefull for gizmos or other handles that should not directly be
 * influenced by lighting.
 * @param worldPos World position of the rendered fragment.
 * @param normal Fragment normal at world position.
 * @param objectColor The color of the fragment / object.
 * @param shininess The shininess of the fragment.
 * @param color The resulting color when lighting and shadows are applied.
 * @param diffuse Optional: Diffuse amount, [0, 1], default 1.0.
 * @param specular Optional: Specular amount, [0, 1], default 0.2.
 * @param ambient Optional: Ambient light amount, [0, 1], default 0.3.
 */
vec3 getSimpleBlinnPhongColor(in vec3 worldPos, in vec3 normal,
in vec3 objectColor, in float shininess, in float diffuse,
in float specular, in float ambient)
{
    vec3 n = normalize(normal);
    vec3 v = normalize(cameraPositionWS - worldPos);

    vec3 kA = ambient * ambientLightColor;
    float kD = diffuse;
    float kS = specular;

    vec4 lightColor = vec4(1.0);
    vec3 lightDir = vec3(0, 0, -1);

    float lambertian = abs(dot(n, -lightDir));

    vec3 halfDir = normalize(-lightDir + v);
    float specAngle = max(dot(halfDir, n), 0.0);
    float s = pow(specAngle, shininess);

    vec3 diffuseC = kD * lambertian * lightColor.rgb;
    vec3 specularC = kS * s * lightColor.rgb;

    vec3 color = diffuseC + specularC + kA;

    return color * objectColor;
}

/**
 * A simplified version of the getBlinnPhongLighting function, which only applies the lights without accounting
 * for the angle between light and surface, aswell as the ambient light term.
 * This is usefull when wanting to apply shadows and ambient light to an object
 * without applying shininess and diffuse.
 * It avoids darker surfaces when the angle between light and surface approaches 90°.
 * @param worldPos World position of the rendered fragment.
 * @param normal Fragment normal at world position.
 * @param objectColor The color of the fragment / object.
 * @param ambient Optional: Ambient light amount, [0, 1], default 0.3.
 * @return The resulting color when simplified lighting and shadows are applied.
 */
vec3 getSimpleShading(in vec3 worldPos, in vec3 normal, in vec3 objectColor)
{
    vec3 kA = ambientLight * ambientLightColor;

    vec3 color = vec3(0);

    vec4 lightColor;
    vec3 lightDir;

    for (int i = 0; i < sceneLights.length(); ++i)
    {
        Light light = sceneLights[i];
        vec3 dir = light.positionWS.xyz - worldPos;

        float intensity = getLightIntensity(dir, light, normal);

        if (intensity == 0) continue;

        lightColor = light.color * intensity;

        vec3 diffuseC = (1.0f - ambientLight) * lightColor.rgb;

        color += diffuseC;

        // The first light has a shadow map attached.
        if (i == 0 && light.type == 0)
        {
            color *= getShadow(worldPos, normal);
        }
    }

    color += kA;

    return color * objectColor;
}