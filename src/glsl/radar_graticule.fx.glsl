/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Susanne Fuchs
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/

#include "shared/msharedconstants.h"
#include "radar_utils.fx.glsl"

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/


/*****************************************************************************
 ***                          UTILITY FUNCTIONS
 *****************************************************************************/


/*****************************************************************************
 ***                             INTERFACES
 *****************************************************************************/


/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform mat4 mvpMatrix; // Model-view-projection matrix
uniform vec2 pToWorldZParams; // Parameters to convert p[hPa] to world z

uniform int elIdx; // Index of the currently rendered elevation
uniform float minAz; // Minimum azimuth angle.
uniform float maxAz; // Maximum azimuth angle.
uniform float vSectionAz; // Azimuth angle for vertical profile mode.
uniform bool showVolumeThickness; // Data rendered as 2D or 3D planes

// Graticule uniforms.
uniform int graticuleAzimuthStep;
uniform int graticuleRangeStep;
uniform vec4 graticuleColour;
uniform int ncircles; // Number of circles to draw.
uniform int numRayLines; // Number of beams to draw.
uniform float deltaAz; // Azimuth step size in the data.

/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

shader VSmain()
{
    // Pass vertex and instance ID to the geometry shader:
    // gl_InstanceID = bin index, gl_VertexID = azimuth index.
    gl_Position = vec4(gl_InstanceID, gl_VertexID, 0, 0);
}


shader VSgraticuleBeams(out float flag)
{
    // switched in comparison to VSgraticuleRings
    int binIdx = gl_VertexID;

    int nrays = int(radarMetadata[elIdx].nrays); // Number of azimuth angles in the data.
    int azIdx = gl_InstanceID % numRayLines;

    // Elevation angle.
    float elAngle =radarMetadata[elIdx].elevationAngleDegrees; // In degrees.
    float verticalBeamwidth = radarMetadata[elIdx].verticalBeamwidth; // In degrees.
    if (showVolumeThickness) // data as 2D or 3D planes
    {
        if (gl_InstanceID < numRayLines)
        {
            elAngle -= verticalBeamwidth/2.0;
        }
        else
        {
            elAngle += verticalBeamwidth/2.0;
        }
    }

    // Azimuth angle.
    float azAngle = float(azIdx * graticuleAzimuthStep); // Left angle for horizontal volume.
    if (vSectionAz >= 0)
    {
        // Set to vertical section angle if it exists.
        azAngle = vSectionAz;
    }

    // Range distance along the radar beam.
    float binDistance = radarMetadata[elIdx].binDistance; // In metres.
    float rangeInner = radarMetadata[elIdx].rangeStart + binDistance * float(binIdx);
    // We have (nbins+1) points, therefore the last rendered point is the start
    // of first bin interval outside the radar range, which is equivalent to the
    // end point of last bin inside the radar range.

    // set flag to indicate if fragments containing the vertex should be drawn or discarded.
    // -100: discard, +1: draw.
    flag = 1;

    // Compare with azimuth angle range:
    if (minAz <= maxAz)
    {
        if (azAngle < minAz || azAngle > maxAz) flag = -100;
        if (azAngle == 0 && maxAz == 360) flag = 1; // 360° == 0°
    }
    else
    {
        // If the minimum angle is larger than the maximum angle
        // (which is possible because they are on a circle)
        // the fragments with azimuth angle values inbetween should
        // be discarded.
        if(azAngle > maxAz && azAngle < minAz) flag = -100;
    }

    vec3 pos = radarToLonLatPressure(elAngle, azAngle, rangeInner);
    pos.z = (log(pos.z) - pToWorldZParams.x) * pToWorldZParams.y;
    gl_Position = mvpMatrix * vec4(pos, 1.);
}


shader VSgraticuleRings(out float flag)
{
    int binIdx = gl_InstanceID % ncircles;
    int azIdx = gl_VertexID;

    float elAngle =radarMetadata[elIdx].elevationAngleDegrees; // In degrees.
    float verticalBeamwidth = radarMetadata[elIdx].verticalBeamwidth; // In degrees.
    if (showVolumeThickness) // 2D or 3D
    {
        if (gl_InstanceID < ncircles)
        {
            elAngle -= verticalBeamwidth/2.0;
        }
        else
        {
            elAngle += verticalBeamwidth/2.0;
        }
    }

    float az_left = float(azIdx) * deltaAz;

    float binDistance = radarMetadata[elIdx].binDistance; // In metres.
    float range = 0;
    if (binIdx == ncircles-1 || binIdx == ncircles*2 - 1)
    {
        // Last circle: outer boundary of the radar data grid.
        int nbins = int(radarMetadata[elIdx].nbins);
        range = radarMetadata[elIdx].rangeStart + binDistance * float(nbins);
    }
    else
    {
        range = radarMetadata[elIdx].rangeStart + graticuleRangeStep * float(binIdx);
    }

    // set flag to indicate if fragments containing the vertex should be drawn or discarded.
    // -100: discard, +1: draw.
    flag = 1;

    if (minAz <= maxAz)
    {
        if (az_left < minAz || az_left > maxAz)
        {
            flag = -100;
        }
    }
    else
    {
        // If the minimum angle is larger than the maximum angle
        // (which is possible because they are on a circle)
        // the fragments with azimuth angle values inbetween should
        // be discarded.
        if(az_left > maxAz && az_left < minAz) flag = -100;
    }

    vec3 pos = radarToLonLatPressure(elAngle, az_left, range);
    pos.z = (log(pos.z) - pToWorldZParams.x) * pToWorldZParams.y;
    gl_Position = mvpMatrix * vec4(pos, 1.);
}


/*****************************************************************************
 ***                          GEOMETRY SHADER
 *****************************************************************************/

// Draws vertical lines e.g. separating radar bins.
shader GSgraticuleBinLines(out float flag)
{
    ivec2 ij = ivec2(gl_in[0].gl_Position.xy);

    int nrays = int(radarMetadata[elIdx].nrays); // Number of azimuth angles in the data.
    int azIdx = ij.x;
    int binIdx = ij.y % ncircles;

    float elAngle =radarMetadata[elIdx].elevationAngleDegrees; // In degrees.
    float verticalBeamwidth = radarMetadata[elIdx].verticalBeamwidth; // In degrees.
    float elLower = elAngle - verticalBeamwidth/2.0;
    float elUpper = elAngle + verticalBeamwidth/2.0;

    flag = 1.;

    // Azimuth angle.
    float azAngle = float(azIdx * graticuleAzimuthStep); // Left angle for horizontal volume.
    if (vSectionAz >= 0)
    {
        // Set to vertical section angle if it exists.
        azAngle = vSectionAz;
    }
    else
    {
        // Compare with azimuth angle range:
        if (minAz <= maxAz)
        {
            if (azAngle < minAz || azAngle > maxAz) flag = -100;
            if (azAngle == 0 && maxAz == 360) flag = 1; // 360° == 0°
        }
        else
        {
            // If the minimum angle is larger than the maximum angle
            // (which is possible because they are on a circle)
            // the fragments with azimuth angle values inbetween should
            // be discarded.
            if(azAngle > maxAz && azAngle < minAz) flag = -100;
        }
    }

    float binDistance = radarMetadata[elIdx].binDistance; // In metres.
    float range = 0;
    if (binIdx == ncircles-1)
    {
        // Last circle: outer boundary of the radar data grid.
        int nbins = int(radarMetadata[elIdx].nbins);
        range = radarMetadata[elIdx].rangeStart + binDistance * float(nbins);
    }
    else
    {
        range = radarMetadata[elIdx].rangeStart + graticuleRangeStep * float(binIdx);
    }

    vec3 pos = radarToLonLatPressure(elLower, azAngle, range);
    pos.z = (log(pos.z) - pToWorldZParams.x) * pToWorldZParams.y;
    gl_Position = mvpMatrix * vec4(pos, 1.);
    EmitVertex();

    pos = radarToLonLatPressure(elUpper, azAngle, range);
    pos.z = (log(pos.z) - pToWorldZParams.x) * pToWorldZParams.y;
    gl_Position = mvpMatrix * vec4(pos, 1.);
    EmitVertex();
}


/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

shader FSgraticule(in float flag, out vec4 fragColour)
{
    // Discard the element if it should not be visible
    if (flag < 0.)
    {
        discard;
    }
    else
    {
        fragColour = graticuleColour;
    }
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

// Lines in radar ray direction.
program GraticuleBeams
{
    vs(430)=VSgraticuleBeams();
    fs(430)=FSgraticule();
};

// Circles perpendicular to the radar rays.
program GraticuleRings
{
    vs(430)=VSgraticuleRings();
    fs(430)=FSgraticule();
};

// Lines in elevation angle direction.
program GraticuleBinLines
{
    vs(430)=VSmain();
    gs(430)=GSgraticuleBinLines() : in(points), out(line_strip, max_vertices = 2);
    fs(430)=FSgraticule();
};
