/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2018 Florian Maerkl [+]
**  Copyright 2022-2023 Thorwin Vogt [*]
**  Copyright 2022 Luka Elwart [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

// requires includes/random.glsl
// requires shared/mvolumeraycasterconstants.h
// requires volume_global_structs_utils.glsl
// requires volume_raycaster.fx.glsl
// requires volume_sample_cloud_utils.glsl
// requires volume_acceleration_utils.glsl

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform bool enableVolLight;

uniform float powderStrength;
uniform float powderDepth;

uniform float densityScale;

uniform bool usePressureScale;

struct DirLight {
    vec3 dir;
    vec3 col;
    float intensity;
    float stepSize;
    float maxDist;
};

uniform DirLight dirLights[MAX_NUM_DIRECTIONAL_LIGHTS];
uniform int numDirectionalLights;

struct PointLight {
    vec3 pos;
    vec3 col;
    float intensity;
    float stepSize;
    float maxDist;
};

uniform PointLight pointLights[MAX_NUM_POINT_LIGHTS];
uniform int numPointLights;

struct SpotLight {
    vec3 pos;
    vec3 dir;
    vec3 col;
    float intensity;
    float stepSize;
    float maxDist;
    float angleCos; // Opening angle cosine
};

uniform SpotLight spotLights[MAX_NUM_SPOT_LIGHTS];
uniform int numSpotLights;

struct RectLight {
    vec3 pos; // center position
    vec3 dir;
    vec3 col;
    float intensity;
    float stepSize;
    float maxDist;
    vec4 rightW; // .xyz is normalized right vector/tangent; .w is width
    vec4 upH; // .xyz is normalized up vector/binormal; .w is height
};

uniform RectLight rectLights[MAX_NUM_RECT_LIGHTS];
uniform int numRectLights;

/*****************************************************************************
 ***                             FUNCTIONS
 *****************************************************************************/

/**
  Returns the minimum bounds position of the volume.
 */
vec3 getBoundsMin()
{
    return vec3(volumeTopNWCrnr.x,
                volumeBottomSECrnr.y,
                volumeBottomSECrnr.z);
}


/**
  Returns the maximum bounds position of the volume.
 */
vec3 getBoundsMax()
{
    return vec3(volumeBottomSECrnr.x,
                volumeTopNWCrnr.y,
                volumeTopNWCrnr.z);
}


/**
  Returns the minimum bounds position of the data in the volume.
 */
vec3 getDataBoundsMin()
{
    return renderRegionBottomSWCrnr;
}


/**
  Returns the maximum bounds position of the data in the volume.
 */
vec3 getDataBoundsMax()
{
    return renderRegionTopNECrnr;
}


/**
  Convert world coordinates to texture coordinates within the bounds of the ray.caster.
  */
vec3 worldToBoundsTexCoord(in vec3 pos)
{
    vec3 maxExtent = min(getDataBoundsMax(), getBoundsMax());
    vec3 minExtent = max(getDataBoundsMin(), getBoundsMin());

    vec3 extent = maxExtent - minExtent;

    if (extent.x < 0.0001 || extent.y < 0.0001 || extent.z < 0.0001)
    {
        return vec3(0.0);
    }

    vec3 voxelCoord = (pos - minExtent);
    voxelCoord.x = mod(voxelCoord.x, 360);
    voxelCoord /= extent;

    if (usePressureScale)
    {
        float pTop = worldZToPressure(maxExtent.z);
        float pBot = worldZToPressure(minExtent.z);
        float p = worldZToPressure(pos.z);
        float alpha = (p - pTop) / (pBot - pTop);
        voxelCoord.z = alpha;
    }

    return voxelCoord;
}


/**
  Sample the extinction coefficient of all variables currently used in the volume at the given position.
 */
float sampleExtinctionCoeff(
        in float[MAX_VARIABLES] scalars, in vec3 pos, in vec3 rayIncrement,
        in float unitStepM, in float stepSizeM)
{
    float k = 0.0;
    float tempK = 0.0;
    for (int i = 0; i < MAX_VARIABLES; i++)
    {
        if (enabledVariableSlots[i])
        {
            if (scalars[i] == M_MISSING_VALUE) continue;
            if (ciwcSlot == i)
            {
                tempK = getCiwcColour(scalars[i], pos).a;
            }
            else if (clwcSlot == i)
            {
                tempK = getClwcColour(scalars[i], pos).a;
            }
            else
            {
                // For now, hardcoded if statements for indices, until
                // variable uniforms are refactored into structs for each variable, and an
                // array that holds all variable structs, except shading var.
                // TODO (tv, 7Jul2023): Rework variable uniforms as described above.
                if (i == 0)
                {
                    float t = (scalars[i] - dataExtent.tfMinimum)
                                / (dataExtent.tfMaximum - dataExtent.tfMinimum);
                    tempK = texture(transferFunction, t).a;
                }
                else if (i == 1)
                {
                    float t = (scalars[i] - dataExtentSec.tfMinimum)
                                / (dataExtentSec.tfMaximum - dataExtentSec.tfMinimum);
                    tempK = texture(transferFunctionSec, t).a;
                }
                else if (i == 2)
                {
                    float t = (scalars[i] - dataExtentThird.tfMinimum)
                                / (dataExtentThird.tfMaximum - dataExtentThird.tfMinimum);
                    tempK = texture(transferFunctionThird, t).a;
                }
                tempK = convertToExtinctionCoeff(tempK, pos, rayIncrement, unitStepM, stepSizeM);
            }

            if (isnan(tempK)) tempK = 0;

            k += tempK;
        }
    }

    return k;
}


/**
  Sample the lighting value at the given ray position.
  The lighting value is simply a color.
 */
vec3 getVolumeLightAtPos(in vec3 rayPos, in float stepSizeM, in float k)
{
    vec3 texCoords = worldToBoundsTexCoord(rayPos);
    if (!(texCoords.p > 0.0) || !(texCoords.p < 1.0))
    {
        return vec3(0.f);
    }

    vec3 lightColor = texture(lightingVolume, texCoords).rgb;
    float powder = 1.0 - exp(-powderDepth * k * stepSizeM) * powderStrength;

    return lightColor * clamp(powder, 0.0, 1.0);
}


/**
  Calculate the total optical thickness along a ray through the volume.
  Steps backwards from pos towards endPos.
 */
float sampleOpticalThicknessAlongRay(
        in vec3 pos, in vec3 endPos, in vec3 boundsMin,
        in vec3 boundsMax, in float stepSize)
{
    // if opacity falls to low (below threshold) we want to stop early
    const float cutOffThickness = -log(0.01);

    Ray ray;
    ray.origin = pos;
    ray.direction = normalize(endPos - pos);

    float lambda = 0;
    // The lambda describing entry and exit points of the volume bounding box
    vec2 lambdaNearFar = vec2(0., 0.);
    vec3 rayIncrement = ray.direction * stepSize;
    vec3 currentRayPos = pos;

    bool rayIntersectsRenderVolume = rayBoxIntersection(
    ray, renderRegionBottomSWCrnr, renderRegionTopNECrnr, lambdaNearFar);
    if (!rayIntersectsRenderVolume)
    {
        return 0.0;
    }

    float opticalThickness = 0.0;

    // Array holding variable scalars for each ray step.
    // We don't need to create it every step anew, we can
    // just overwrite the old scalars.
    float[MAX_VARIABLES] scalars;

    // Entry and exit lambda for the currently traversed data section.
    vec2 tNearFar;

    // Step backwards along the ray towards the end.
    while(lambda < lambdaNearFar.y)
    {
        if(shouldTraverseDataSection(ray, lambda, tNearFar))
        {
            // Limit tNearFar to volume bounds
            tNearFar = clamp(tNearFar, vec2(-1), lambdaNearFar);

            // Traverse current data section
            while (lambda < tNearFar.y)
            {
                currentRayPos += rayIncrement;
                lambda += stepSize;

                // Step size conversion to meters, later used in opacity correction.
                float stepSizeMeters = worldStepToMeters(currentRayPos, -rayIncrement);
                float unitStepMeters = worldStepToMeters(currentRayPos, -normalize(rayIncrement));

                // Sample variable scalars first.
                sampleEnabledVariables(currentRayPos, scalars);

                // Retreive extinction coefficients of each variable, and add them together.
                float extinctionCoefficient = sampleExtinctionCoeff(scalars, currentRayPos, -rayIncrement, unitStepMeters, stepSizeMeters);

                // Calculate optical thickness from the extinction coefficient at this location.
                opticalThickness += stepSizeMeters * extinctionCoefficient * densityScale;

                if (opticalThickness > cutOffThickness) return opticalThickness;
            }
        }
        else
        {
            lambda = tNearFar.y;
            currentRayPos = ray.origin + lambda * ray.direction;
        }
    }

    return opticalThickness;
}


/**
  Calculate the light colour at a given ray position. Will produce shadows by calculating  how much arrives at this point
  for every light.
 */
vec3 calculateShadowColour(vec3 rayPosition, vec3 boundsMin, vec3 boundsMax)
{
    const float intensityThreshold = 0.001f;
    float invValRange = 1.f / (dataExtent.tfMaximum - dataExtent.tfMinimum);

    vec3 shadowCol = vec3(0.f);

    // DIRECTIONAL LIGHTS
    for (int j = 0; j < numDirectionalLights; j++) {
        DirLight light = dirLights[j];

        float opticalThickness = sampleOpticalThicknessAlongRay(rayPosition, rayPosition - light.dir * light.maxDist, boundsMin, boundsMax, light.stepSize);

        shadowCol += light.col * light.intensity * exp(-opticalThickness);
    }

    // POINT LIGHTS
    for (int j = 0; j < numPointLights; j++) {
        PointLight light = pointLights[j];

        vec3 delta = light.pos - rayPosition;

        // https://en.wikipedia.org/wiki/Inverse-square_law
        float intensity = light.intensity / (length(delta)*length(delta));

        // intensity is too low, skip this light
        if (intensity < intensityThreshold) continue;

        vec3 dir = normalize(delta);
        // Maximum trace distance
        float traceDist = min(length(delta), light.maxDist);

        float opticalThickness = sampleOpticalThicknessAlongRay(rayPosition, rayPosition + dir * traceDist, boundsMin, boundsMax, light.stepSize);

        shadowCol += intensity * light.col * exp(-opticalThickness);
    }

    // SPOT LIGHTS
    for (int j = 0; j < numSpotLights; j++) {
        SpotLight light = spotLights[j];

        if(light.maxDist <= 0) continue;

        vec3 delta = light.pos - rayPosition;

        // https://en.wikipedia.org/wiki/Inverse-square_law
        float intensity = min(1., light.intensity / (length(delta) * length(delta)));

        float spotDot = dot(normalize(delta), -light.dir);

        // remap so that spotDot values < light.angleCos get remapped to 0: map [angleCos, 1] to [0, 1]
        spotDot = (spotDot - light.angleCos) / (1 - light.angleCos);
        spotDot = max(0.f, spotDot);

        // Uncomment this for full brightness in light cone instead of gradient falloff over dot product.
        spotDot = sign(spotDot);
        intensity *= spotDot;

        // intensity is too low, skip this light
        if (intensity < intensityThreshold) continue;

        vec3 dir = normalize(delta);

        // Maximum trace distance
        float traceDist = min(length(delta), light.maxDist);

        float opticalThickness = sampleOpticalThicknessAlongRay(rayPosition, rayPosition + dir * traceDist, boundsMin, boundsMax, light.stepSize);

        shadowCol += intensity * light.col * exp(-opticalThickness);
    }

    // RECT LIGHTS
    for (int j = 0; j < numRectLights; j++) {
        RectLight light = rectLights[j];

        if (light.stepSize <= 0.f) break;

        // Distance to plane
        float sampleToPlaneDist = dot(light.dir, rayPosition - light.pos);

        // Check if we are behind the plane
        if (sampleToPlaneDist < 0.f) continue;

        // Project sample position onto plane
        vec3 projP = rayPosition - light.dir * sampleToPlaneDist;

        float w = abs(dot(projP - light.pos, light.rightW.xyz));
        float h = abs(dot(projP - light.pos, light.upH.xyz));
        bool inside = w <= light.rightW.w * 0.5f && h <= light.upH.w * 0.5f; // does the point lie in the orthographic rect-frustum?
        if (!inside) continue;

        // https://en.wikipedia.org/wiki/Inverse-square_law
        float intensity = light.intensity / (sampleToPlaneDist * sampleToPlaneDist);

        // intensity is too low, skip this light
        if (intensity < intensityThreshold) continue;

        // Maximum trace distance
        float traceDist = min(sampleToPlaneDist, light.maxDist);

        float opticalThickness = sampleOpticalThicknessAlongRay(rayPosition, rayPosition - light.dir * traceDist, boundsMin, boundsMax, light.stepSize);

        shadowCol += intensity * light.col * exp(-opticalThickness);
    }

    return shadowCol;
}
