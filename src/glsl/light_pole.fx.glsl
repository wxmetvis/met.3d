/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Luka Elwart
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform mat4        mvpMatrix;
uniform vec4        colour;
uniform float       poleHeight;

/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

shader VSLightPole(in vec4 pos : 0)
{
    gl_Position = mvpMatrix * vec4(pos.xyz * vec3(1, 1, poleHeight), 1.f);
}

/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

shader FSLightPole(out vec4 fragColor)
{
    fragColor = colour;
}

/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program LightPole
{
    vs(420)=VSLightPole();
    fs(420)=FSLightPole();
};
