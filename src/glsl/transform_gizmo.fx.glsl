/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2022 Luka Elwart
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform mat4        mvpMatrix;
uniform mat4        modelMatrix;

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/

// include constants
#include "includes/lighting.glsl"

/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

shader VSGizmo(in vec4 pos : 0, out float axisID, out vec3 worldPos, out vec4 normal)
{
    gl_Position = mvpMatrix * vec4(pos.xyz, 1.f);

    // try to draw in front of everything (set NDC.z to -1 and just apply a tiny depth offset so the gizmo itself is correctly sorted
    // -> otherwise gizmo axis may draw on top of each other in arbitrary order)

    gl_Position.z = (-1.f + gl_Position.z * 0.0001f) * gl_Position.w;
    axisID = pos.w;

    worldPos = (modelMatrix * vec4(pos.xyz, 1.f)).xyz;

    int iAxisID = int(round(axisID));
    normal = vec4(0,0,0,0);
    vec3 offset = vec3(0.f);
    if (iAxisID == 0) offset = -vec3(1, 0, 0) * dot(pos.xyz, vec3(1, 0, 0));
    else if (iAxisID == 1) offset = -vec3(0, 1, 0) * dot(pos.xyz, vec3(0, 1, 0));
    else if (iAxisID == 2) offset = -vec3(0, 0, 1) * dot(pos.xyz, vec3(0, 0, 1));
    else if (iAxisID == 3) normal.z = 1.f;
    else if (iAxisID == 4) normal.y = 1.f;
    else if (iAxisID == 5) normal.x = 1.f;
    else normal.z = 1.f;

    if (iAxisID <= 2) normal.xyz = pos.xyz + offset;
    float ln = length(normal.xyz);
    if (ln == 0.f) normal.xyz = normalize(-offset);
    else normal.xyz /= ln;
}

/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

shader FSGizmo(out vec4 fragColor, in float axisID, in vec3 worldPos, in vec4 normal)
{
    vec3 col;
    float alpha = 1.f;
    int iAxisID = int(round(axisID));
    if (iAxisID == 0) col = vec3(1, 0, 0);
    else if (iAxisID == 1) col = vec3(0, 1, 0);
    else if (iAxisID == 2) col = vec3(0, 0, 1);
    else if (iAxisID == 3) col = vec3(1, 1, 0);
    else if (iAxisID == 4) col = vec3(1, 0, 1);
    else col = vec3(0, 1, 1);

    float desat = axisID < 2.5f ? 0.33f : 0.66f; // desaturate 2d planes stronger
    col = mix(col, vec3(1.f), desat); // desaturate

    col = getSimpleBlinnPhongColor(worldPos, normal.xyz, col, 10.0f, 1.0, 0.2, 0.1);

    fragColor = vec4(col, alpha);
}

/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program gizmoShader
{
    vs(430)=VSGizmo();
    fs(430)=FSGizmo();
};
