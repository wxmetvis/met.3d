/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
/****************************************************************************
***                             CONSTANTS                                 ***
*****************************************************************************/


/****************************************************************************
***                             INTERFACES                                ***
*****************************************************************************/

interface VStoFSSimple
{
    smooth vec3 worldPos;
    smooth vec4 vertexColor;
    smooth vec3 normal;
};

/****************************************************************************
***                             UNIFORMS                                  ***
*****************************************************************************/

// Definition of uniform variables common to all shader stages.
// View Projection matrix to convert from world to clip space.
uniform mat4 vpMatrix;
uniform mat4 modelMatrix;
uniform mat4 normalMatrix;
uniform vec2 pToWorldZParams;
uniform vec3 geometryColor;
uniform sampler1D transferFunction; // 1D transfer function
uniform float scalarMinimum;    // min/max data values to scale to 0..1
uniform float scalarMaximum;

// Uniforms needed to sample variables.
// contains precomputed z-coordinates
uniform sampler1D pressureTable; // PRESSURE_LEVEL
// contains hybrid coefficients a,b
uniform sampler1D hybridCoefficients; // HYBRID_SIGMA
// contains surface pressure at grid point (i, j)
uniform sampler2D surfacePressure; // HYBRID_SIGMA
// contains pressure field
uniform sampler3D auxPressureField3D_hPa; // AUXILIARY_PRESSURE_3D
#ifdef ENABLE_HYBRID_PRESSURETEXCOORDTABLE
// contains precomputed z-coordinates
uniform sampler2D pressureTexCoordTable2D; // HYBRID_SIGMA
#endif

uniform sampler3D dataVolume;
uniform sampler1D lonLatLevAxes;


/****************************************************************************
***                             INCLUDES                                  ***
*****************************************************************************/

// Include files.
#include "shared/mvolumeraycasterconstants.h"
#include "shared/msharedconstants.h"
// include global structs
#include "volume_global_structs_utils.glsl"
// include hybrid model volume sampling methods
#include "volume_hybrid_utils.glsl"
// include model level volume with auxiliary pressure field sampling methods
#include "volume_auxiliarypressure_utils.glsl"
// include pressure levels volume sampling methods
#include "volume_pressure_utils.glsl"
// include radar sampling methods, must come before sample_utils
#include "radar_utils.fx.glsl"
#include "volume_radar_utils.fx.glsl"
// defines subroutines and auxiliary ray-casting functions
#include "volume_sample_utils.glsl"
#include "includes/lighting.glsl"

/****************************************************************************
***                           VERTEX SHADER                               ***
*****************************************************************************/

shader VSUntexturedMesh(in vec3 localPos : 0, in vec3 normal : 1, out VStoFSSimple Output)
{
    // World pos in world coordinates.
    vec3 worldPos = (modelMatrix * vec4(localPos, 1.0)).xyz;

    // Transform normal to world normal.
    vec3 worldNorm = normalize((normalMatrix * vec4(normal, 1.0)).xyz);

    gl_Position = vpMatrix * vec4(worldPos, 1.0);
    Output.worldPos = worldPos;
    Output.normal = normalize(worldNorm);
}


shader VSVariableShadedMesh(in vec3 localPos : 0, in vec3 normal : 1, out VStoFSSimple Output)
{
    // World pos in world coordinates.
    vec3 worldPos = (modelMatrix * vec4(localPos, 1.0)).xyz;

    // Transform normal to world normal.
    vec3 worldNorm = normalize((normalMatrix * vec4(normal, 1.0)).xyz);

    float scalar = sampleDataAtPos(worldPos);
    float t = (scalar - dataExtent.tfMinimum) / (dataExtent.tfMaximum - dataExtent.tfMinimum);
    vec4 color = texture(transferFunction, t).rgba;

    gl_Position = vpMatrix * vec4(worldPos, 1.0);
    Output.worldPos = worldPos;
    Output.vertexColor = color;
    Output.normal = normalize(worldNorm);
}


shader VSAttributedMesh(in vec3 localPos : 0, in vec3 normal : 1, in float attr : 3, out VStoFSSimple Output)
{
    // World pos in world coordinates.
    vec3 worldPos = (modelMatrix * vec4(localPos, 1.0)).xyz;
    vec3 worldNorm = normalize((normalMatrix * vec4(normal, 1.0)).xyz);

    float scalar_ = (attr - scalarMinimum) / (scalarMaximum - scalarMinimum);

    gl_Position = vpMatrix * vec4(worldPos, 1.0);
    Output.worldPos = worldPos;
    Output.vertexColor = texture(transferFunction, scalar_);
    Output.normal = normalize(worldNorm);
}


/****************************************************************************
***                          FRAGMENT SHADER                              ***
*****************************************************************************/

shader FSSurfaceColor(in VStoFSSimple Input, out vec4 fragColor)
{
    fragColor.rgb = getBlinnPhongColor(Input.worldPos, Input.normal,
                                       geometryColor, 10.0f);

    fragColor.a = 1;
}


shader FSVertexColor(in VStoFSSimple Input, out vec4 fragColor)
{
    fragColor.rgb = getBlinnPhongColor(Input.worldPos, Input.normal,
                                       Input.vertexColor.rgb, 10.0f);

    fragColor.a = 1;
}


/****************************************************************************
***                             PROGRAMS                                  ***
*****************************************************************************/

program UntexturedMesh
{
    vs(430)=VSUntexturedMesh();
    fs(430)=FSSurfaceColor();
};


program VarShadedMesh
{
    vs(430)=VSVariableShadedMesh();
    fs(430)=FSVertexColor();
};


program AttributeTexturedMesh
{
    vs(430)=VSAttributedMesh();
    fs(430)=FSVertexColor();
};