/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus [*, previously +]
**  Copyright 2025 Thorwin Vogt [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

/*****************************************************************************
 ***                             INTERFACES
 *****************************************************************************/

interface VStoFS
{
    smooth vec2 texCoord;
};


/*****************************************************************************
 ***                           VERTEX SHADER
 *****************************************************************************/

uniform mat3 transform;
uniform float zAnchor;

shader VSTextPool(in vec3 vertexCoord : 0, in vec2 texCoordIn : 1, out VStoFS Output)
{
    gl_Position = vec4(vec3(transform * vec3(vertexCoord.xy, 1)).xy, zAnchor, 1);
    Output.texCoord = texCoordIn;
}

shader VSTextBackground(in vec4 vertexCoord : 0)
{
    gl_Position = vec4(vec3(transform * vec3(vertexCoord.xy, 1)).xy, zAnchor, 1);
}


/*****************************************************************************
 ***                          FRAGMENT SHADER
 *****************************************************************************/

uniform sampler2D textAtlas;
uniform vec4 colour;

shader FSmain(in VStoFS Input, out vec4 fragColour)
{
    fragColour = vec4(1, 1, 1, texture(textAtlas, Input.texCoord).r) * colour;
}

shader FSBackground(out vec4 fragColour)
{
    fragColour = colour;
}


/*****************************************************************************
 ***                             PROGRAMS
 *****************************************************************************/

program TextPool
{
    vs(330)=VSTextPool();
    fs(330)=FSmain();
};

program Background
{
    vs(400)=VSTextBackground();
    fs(400)=FSBackground();
};