/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023 Thorwin Vogt [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
******************************************************************************/

// requires shared/mvolumeraycasterconstants.h
// requires volume_global_structs_utils.glsl
// requires volume_raycaster.fx.glsl
// requires volume_sample_cloud_utils.glsl

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform bool areVariableDataExtentsAligned;

/*****************************************************************************
 ***                             FUNCTIONS
 *****************************************************************************/

/**
  Checks, whether or not the currently rendered variables have any empty space to skip.
  A variable has empty space to skip, iff either the transfer function begins or ends with full transparency,
  or it is rendered as a cloud ice water or liquid water variable.
*/
bool volumeHasEmptySpace()
{
    return !(((texture(transferFunction, 0.).a > 0.
                    && texture(transferFunction, 1.).a > 0.
                    && clwcSlot != 0
                    && ciwcSlot != 0)
                || !enabledVariableSlots[0])
            && ((texture(transferFunctionSec, 0.).a > 0.
                    && texture(transferFunctionSec, 1.).a > 0.
                    && clwcSlot != 1
                    && ciwcSlot != 1)
                || !enabledVariableSlots[1])
            && ((texture(transferFunctionThird, 0.).a > 0.
                    && texture(transferFunctionThird, 1.).a > 0.
                    && clwcSlot != 2
                    && ciwcSlot != 2)
                || !enabledVariableSlots[2]));
}


/**
  Checks, whether or not an acceleration brick of the min-max acceleration structure can be skipped.
  The input variable i is the index of the checked variabe, while iaccel is the index of the brick in the
  acceleration structure.
*/
bool canSkipBrick(in int i, in ivec3 iaccel, DataVolumeExtent extent, in sampler1D tf, in sampler3D accelerationStructure)
{
    // The min/max isovalues of the isosurfaces that are rendered in this
    // render pass.
    vec2 isoMinMax = vec2(isoValues[0], isoValues[0]);
    // For DVR use the minimum and maximum value of the transfer function.
    // Set isoMin to -infinity. (No skip if not changed.)
    isoMinMax.x = -1.0 / 0.0;
    // Set isoMax to infinity. (No skip if not changed.)
    isoMinMax.y = 1.0 / 0.0;

    ivec3 size = textureSize(accelerationStructure, 0);

    // Check if the requested brick exists
    if (any(greaterThanEqual(iaccel, size)) || any(lessThan(iaccel, ivec3(0.0))))
    {
        return true;
    }

    // Get min/max scalar values contained in the current brick.
    vec2 brickMinMax = texelFetch(accelerationStructure, iaccel, 0).rg;
    bool skipBrick = true;

    if (enabledVariableSlots[i])
    {
        if (clwcSlot == i || ciwcSlot == i)
        {
            isoMinMax.x = 0.0;
            isoMinMax.y = 1.0;
        }
        else
        {
            // Since the user is free to set the maximum value of the transfer
            // function to a value smaller than the minimum value of the transfer
            // function, both cases have to be considered.
            if (extent.tfMinimum < extent.tfMaximum)
            {
                // Only skip isovalues smaller minimum isovalue of transfer function
                // if alpha of minimum equals 0.
                if (texture(tf, 0.).a == 0.)
                {
                    isoMinMax.x = extent.tfMinimum;
                }
                // Only skip isovalues greater maximum isovalue of transfer function
                // if alpha of maximum equals 0.
                if (texture(tf, 1.).a == 0.)
                {
                    isoMinMax.y = extent.tfMaximum;
                }

            }
            else
            {
                // Only skip isovalues greater maximum isovalue of transfer function
                // if alpha of maximum equals 0. (In this case tfMinimum stores max)
                if (texture(tf, 0.).a == 0.)
                {
                    isoMinMax.y = extent.tfMinimum;
                }
                // Only skip isovalues smaller minimum isovalue of transfer function
                // if alpha of minimum equals 0. (In this case tfMaximum stores min)
                if (texture(tf, 1.).a == 0.)
                {
                    isoMinMax.x = extent.tfMaximum;
                }
            }
        }

        if (clwcSlot == i || ciwcSlot == i)
        {
            skipBrick = ((brickMinMax.y <= isoMinMax.x)
            || (brickMinMax.x > isoMinMax.y));
        }
        else
        {
            skipBrick = ((brickMinMax.y < isoMinMax.x)
            || (brickMinMax.x > isoMinMax.y));
        }
    }

    return skipBrick;
}


/**
  Finds the acceleration structure brick surrounding the current ray position.
  It returns the near and far lambda of the ray to the brick bounds.
  It also outputs the bricks index in the acceleration structure as the output variable iaccel.
*/
vec2 findCurrentAccelerationStructureBrick(in Ray ray, in float lambda, in DataVolumeExtent extent, in sampler3D accel3D, out ivec3 iaccel)
{
    // Grid spacing of the acceleration structure.
    ivec3 nMinMaxAccel3D = ivec3(textureSize(accel3D, 0));

    // Small offset, so that the found tFar is always larger than lambda.
    const float lambdaOffset = 0.001;

    vec3 rayPos = ray.origin + (lambda + lambdaOffset) * ray.direction;

    // Special case: If the grid is cyclic in longitude, shift the eastern
    // longitude one grid spacing east (e.g. make 359. 360.).
    float dataEastExtent = extent.dataSECrnr.x;
    if (extent.gridIsCyclicInLongitude) dataEastExtent += extent.deltaLon;

    vec3 deltaAccel = vec3(
    abs(dataEastExtent - extent.dataNWCrnr.x) /
    float(nMinMaxAccel3D.x),
    abs(extent.dataSECrnr.y - extent.dataNWCrnr.y) /
    float(nMinMaxAccel3D.y),
    abs(extent.dataSECrnr.z - extent.dataNWCrnr.z) /
    float(nMinMaxAccel3D.z));

    // In longitude, the world space coordinate system can be shifted by
    // multiples of 360 degrees with respect to the data coordinate system
    // (e.g. if the data volume is defined from -60..60 degrees it may be
    // rendered in the range 300..420 degrees; or cyclic global grids may
    // be repeated at the boundaries).
    // To compute the correct entry indices into the acceleration structure,
    // find closest (shifted) dataNWCrnr.x WEST of rayPosition.x by shifting by
    // multiples of 360. degrees longitude.
    // Example: if dataNWCrnr.x == -60. and rayPosition.x == 320., then
    // dataNWCrnr.x is shifted to 300. If rayPosition.x == -70., then
    // dataNWCrnr.x is shifted to -420.
    float distanceRayPosDataNW_x = rayPos.x - extent.dataNWCrnr.x;
    int numShift360degrees = int(distanceRayPosDataNW_x / 360.);
    if (rayPos.x < extent.dataNWCrnr.x) numShift360degrees -= 1;
    float dataWestExtent = extent.dataNWCrnr.x + numShift360degrees * 360.;

    // Indices of the position of ray entry into the acceleration structure
    // (the min/max map covers the same world space as the data volume).
    // "iaccel" is updated during the traversal to keep track of the current
    // brick.
    float mixI = (rayPos.x - dataWestExtent) / deltaAccel.x;
    float mixJ = (extent.dataNWCrnr.y - rayPos.y) / deltaAccel.y;
    float mixK = (extent.dataNWCrnr.z - rayPos.z) / deltaAccel.z;
    iaccel = ivec3(int(mixI), int(mixJ), int(mixK));

    vec3 dataSWCrnr = vec3(dataWestExtent, extent.dataSECrnr.yz);
    vec3 dataNECrnr = vec3(dataEastExtent, extent.dataNWCrnr.yz);

    ivec3 idx = ivec3(floor((rayPos - dataSWCrnr) / deltaAccel));
    ivec3 idxInv = ivec3(floor((dataNECrnr - rayPos) / deltaAccel));

    vec3 boxCrnrL = idx * deltaAccel + dataSWCrnr;

    vec3 boxCrnrT = dataNECrnr - idxInv * deltaAccel;

    vec2 tNearFar = vec2(0.0);

    rayBoxIntersection(ray, boxCrnrL, boxCrnrT, tNearFar);

    return tNearFar;
}


/**
  Checks, whether the current data section should be traversed or skipped.
  The output variable tNearFar outputs the data sections near and far lambda values on the input ray.
  The input lambda is specifies the distance on the ray from its origin,
  and is used to determine the current ray position.
*/
// In some cases, the exit lambda might be smaller than our current lambda,
// in which case we would traverse the same data section again.
// A workaround is to move the ray lambda forward one step if this happens, and check again.
// TODO: (tv, 12Sep2023) Find the cause in findCurrentAccelerationStructureBrick() in volume_acceleration_utils,
//                       so that this issue doesnt need to be handled separately.
bool shouldTraverseDataSection(in Ray ray, in float lambda, out vec2 tNearFar)
{
#ifdef ENABLE_MINMAX_ACCELERATION
    if (!volumeHasEmptySpace())
    {
        return true;
    }

    tNearFar = vec2(-1.0 / 0.0, 1.0 / 0.0);

    bool traverseDataSection = false;
    vec2 tNearFarBrick = vec2(-1.0 / 0.0, 1.0 / 0.0);
    bool skip = false;
    ivec3 iaccel;

    // Each variable is checked separately, as their data extents
    // might not align.
    for (int i = 0; i < MAX_VARIABLES; i++)
    {
        if (!enabledVariableSlots[i]) continue;

        if (i == 0)
        {
            if (!areVariableDataExtentsAligned || (areVariableDataExtentsAligned && all(isinf(tNearFarBrick))))
            {
                tNearFarBrick = findCurrentAccelerationStructureBrick(ray, lambda, dataExtent, minMaxAccel3D, iaccel);
            }

            skip = canSkipBrick(0, iaccel, dataExtent, transferFunction, minMaxAccel3D);
        }
        if (i == 1)
        {
            if (!areVariableDataExtentsAligned || (areVariableDataExtentsAligned && all(isinf(tNearFarBrick))))
            {
                tNearFarBrick = findCurrentAccelerationStructureBrick(ray, lambda, dataExtentSec, minMaxAccel3DSec, iaccel);
            }

            skip = canSkipBrick(1, iaccel, dataExtentSec, transferFunctionSec, minMaxAccel3DSec);
        }
        if (i == 2)
        {
            if (!areVariableDataExtentsAligned || (areVariableDataExtentsAligned && all(isinf(tNearFarBrick))))
            {
                tNearFarBrick = findCurrentAccelerationStructureBrick(ray, lambda, dataExtentThird, minMaxAccel3DThird, iaccel);
            }

            skip = canSkipBrick(2, iaccel, dataExtentThird, transferFunctionThird, minMaxAccel3DThird);
        }

        tNearFar = vec2(max(tNearFarBrick.x, tNearFar.x), min(tNearFarBrick.y, tNearFar.y));
        traverseDataSection = traverseDataSection || !skip;
    }

    return traverseDataSection;
#else
    tNearFar = vec2(-1.0 / 0.0, 1.0 / 0.0);
    return true;
#endif
}