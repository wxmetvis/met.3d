/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Susanne Fuchs
**  Copyright 2022-2023 Justus Jakobi
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

/*****************************************************************************
 ***                             INCLUDES
 *****************************************************************************/

/*****************************************************************************
 ***                             CONSTANTS
 *****************************************************************************/

#define M_PI 3.1415926535897932384626433832795
#define EARTH_RADIUS_km 6371.0

/*****************************************************************************
 ***                             INTERFACES
 *****************************************************************************/

struct RadarMetadataStruct
{
    uint nbins;
    uint nrays;
    uint elevationNumber;
    uint scanOrderIndex;
    float range;
    float binDistance;
    float elevationAngleDegrees;
    float verticalBeamwidth;
    float horizontalBeamwidth;
    float rangeStart;
};

// Buffer for radar meta data.
layout (std430, binding = 9) buffer radarMetadataBuffer
{
    RadarMetadataStruct radarMetadata[];
};

/*****************************************************************************
 ***                             UNIFORMS
 *****************************************************************************/

uniform float radarRange;
uniform float standardAzimuthGridsize;
uniform vec3 lonLatHeightRadar;
uniform vec3 cartesianCoordinatesRadar;

/*****************************************************************************
 ***                          UTILITY FUNCTIONS
 *****************************************************************************/

bool floatIsAlmostEqual(float floatA, float floatB, float maxDiff)
{
    float diff = abs(floatA - floatB);
    if (diff <= maxDiff)
    {
        return true;
    }

    return false;
}


float degreesToRadians(in float angle)
{
    return (angle * M_PI) / 180.0;
}


float radiansToDegrees(in float angle)
{
    return (angle * 180.0) / M_PI;
}


float metre2pressure_standardICAO(float z_m)
{
    // g and R are used by all equations below.
    float g = 9.80665; // m s^-2
    float R = 287.058; // J K^-1 kg^-1

    float p = M_MISSING_VALUE;

    if (z_m <= 11000.)
    {
        // ICAO standard atmosphere between 0 and 11 km: T(z=0km) = 15 degC,
        // p(z=0km) = 1013.25 hPa. Temperature gradient is 6.5 K/km.
        float z0 = 0.;
        float T0 = 288.15;
        float gamma = 6.5e-3;
        float p0 = 101325.;

        // Hydrostatic equation with linear temperature gradient.
        p = p0 * pow((T0-gamma*z_m) / (T0-gamma*z0), g/(gamma*R));
    }
    else if (z_m <= 20000.)
    {
        // ICAO standard atmosphere between 11 and 20 km: T(z=11km) = -56.5
        // degC, p(z=11km) = 226.32 hPa. Temperature is constant at -56.5 degC.
        float z0 = 11000.;
        float p0 = 22632.;
        float T = 216.65;

        // Hydrostatic equation with constant temperature profile.
        p = p0 * exp(-g * (z_m-z0) / (R*T));
    }
    else if (z_m <= 32000.)
    {
        // ICAO standard atmosphere between 20 and 32 km: T(z=20km) = -56.5
        // degC, p(z=20km) = 54.75 hPa. Temperature gradient is -1.0 K/km.
        float z0 = 20000.;
        float T0 = 216.65;
        float gamma = -1.0e-3;
        float p0 = 5475.006582501095;

        // Hydrostatic equation with linear temperature gradient.
        p = p0 * pow((T0-gamma*z_m) / (T0-gamma*z0), g/(gamma*R));
    }
    // Metre to pressure conversion not implemented for z > 32km.

    return p;
}


/** Converts radar coordinates of a point P to geographic coordinates (z coord. in metre).
*/
vec3 radarToLonLatMetre(in float elevationAngleDegrees,
                           in float azimuthAngleDegrees,
                           in float rangeInMetres,
                           in vec3 lonLatHeightRadar)
{
    vec3 pos;

    float R_e = EARTH_RADIUS_km * 1000; // Earth radius in m.
    float R_r = 4 * R_e; // Refraction radius of the radar beam.

    float alpha = degreesToRadians(elevationAngleDegrees);
    float delta = degreesToRadians(azimuthAngleDegrees);
    float s = rangeInMetres; // Length of the radar beam between station and P.
    float h0 = lonLatHeightRadar.z;

    float beta = s /R_r; // Angle between radar station and P on the circle with radius R_r.
    float latRad = degreesToRadians(lonLatHeightRadar.y);

    float kappa = M_PI/2 + alpha - beta;
    // Coordinates of point P in a coord. system that has its centre in the
    // centre of the refraction circle.
    float x_refract = R_r * cos(kappa);
    float y_refract = R_r * sin(kappa);

    // Distances between the centre of the Earth and the centre of the
    // refraction circle.
    float d = R_r * sin(alpha);
    float c = R_r * cos(alpha) - (R_e + h0);

    // Coordinates of point P in a coord. system that has its centre in the
    // centre of the Earth.
    float x = x_refract + d;
    float y = y_refract - c;

    // Height of point P in metres (distance between P and the Earth centre via
    // Pythagoras, minus Earth radius).
    float height = sqrt(x*x + y*y) - R_e;

    // Angle between radar station, centre of the Earth, and P.
    float ThetaE = asin(x / sqrt(x*x + y*y));

    // Latitude and longitude of P via spherical laws of sines and cosines.
    float latitudeRadians = asin(sin(latRad) * cos(ThetaE) +
                                 cos(latRad) * sin(ThetaE) * cos(delta));
    float tau = asin(sin(delta) * sin(ThetaE) / cos(latitudeRadians));
    pos.x = lonLatHeightRadar.x + radiansToDegrees(tau); // Longitude in °.
    pos.y = radiansToDegrees(latitudeRadians); // Latitude in °.

    pos.z = height; // Height in metres.

    return pos;
}


/** Converts radar coordinates of a point P to geographic coordinates (z coord. in hPa).
*/
vec3 radarToLonLatPressure(in float elevationAngleDegrees,
                         in float azimuthAngleDegrees,
                         in float rangeInMetres)
{
    vec3 pos = radarToLonLatMetre(elevationAngleDegrees,
                          azimuthAngleDegrees,
                          rangeInMetres,
                          lonLatHeightRadar);

    pos.z = metre2pressure_standardICAO(pos.z)/100; // Height in hPa.

    return pos;
}


/** Converts geographic coordinates to cartesian coordinates.
*/
vec3 lonLatMetreToGlobalCart(in float lon, in float lat, in float height)
{
    float r, x, y, z;
    r = (EARTH_RADIUS_km * 1000) + height;
    x = r * cos(degreesToRadians(lat)) * cos(degreesToRadians(lon));
    y = r * cos(degreesToRadians(lat)) * sin(degreesToRadians(lon));
    z = r * sin(degreesToRadians(lat));
    return vec3(x, y, z);
}


/** Converts geographic coordinates to cartesian coordinates.
*/
vec3 lonLatMetreToGlobalCart(in vec3 lonLatHeight)
{
    return lonLatMetreToGlobalCart(lonLatHeight.x, lonLatHeight.y, lonLatHeight.z);
}


/**
    Calculates the azimuth angle (bearing angle), analogous to gcAzimuth_deg,
    between the radar site (position: lonLatHeightRadar) and a point described
    by lon, lat.

    Reference: https://www.movable-type.co.uk/scripts/latlong.html (Bearing)

    All angles are in degrees.
*/
float calculateAzimuthFromRadarSite(in float lon,
                                    in float lat,
                                    in vec3 llh)
{
    float phi1, phi2;
    phi1 = degreesToRadians(llh.y);
    phi2 = degreesToRadians(lat);
    float lambda1, lambda2;
    lambda1 = degreesToRadians(llh.x);
    lambda2 = degreesToRadians(lon);
    float deltaLambda;
    deltaLambda = lambda2 - lambda1;
    float y, x, theta;
    y = sin(deltaLambda) * cos(phi2);
    x = cos(phi1) * sin(phi2) - sin(phi1) * cos(phi2) * cos(deltaLambda);
    theta = atan(y, x); // c++: atan2(y, x) does the same as glsl: atan(y, x)
    if (theta < 0) theta += 2 * M_PI;

    return radiansToDegrees(theta);
}


float sampleRadarVolumeAtRadarBinAzElIndices(in sampler3D dataVolume,
                             in ivec3 binAzElIndices)
{
    // Return missing value if pos is outside the radar cells.
    if (binAzElIndices.x < 0 || binAzElIndices.y < 0 || binAzElIndices.z < 0)
    {
        return M_MISSING_VALUE;
    }

    // Fetch data from the closest point in the data volume and return it.
    return texelFetch(dataVolume, binAzElIndices, 0).a;
}


float gcDistanceEarth(const float lon1, const float lat1,
                      const float lon2, const float lat2)
{
    float lon1_rad = degreesToRadians(lon1);
    float lat1_rad = degreesToRadians(lat1);
    float lon2_rad = degreesToRadians(lon2);
    float lat2_rad = degreesToRadians(lat2);

    float dlon = lon2_rad - lon1_rad;
    float dlat = lat2_rad - lat1_rad;

    float sin_dlat = sin(dlat/2.);
    float sin_dlon = sin(dlon/2.);
    float a = sin_dlat*sin_dlat
              + cos(lat1_rad) * cos(lat2_rad) * sin_dlon*sin_dlon;
    float c = 2. * asin(min(1.,sqrt(a)));
    return c*EARTH_RADIUS_km;
}