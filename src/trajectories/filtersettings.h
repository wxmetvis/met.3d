/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_FILTERSETTINGS_H
#define MET_3D_FILTERSETTINGS_H

// standard library imports

// related third party imports

// local application imports
#include "pipelineelementsettings.h"

namespace Met3D
{

class MActor;

/**
 * Abstract UI class for all filters.
 */
class MFilterSettings : public MPipelineElementSettings
{
public:
    MFilterSettings(const QString& filterType, MActor* actor, MProperty* parentProperty);

    /**
     * @return The filter type, which is defined by its name.
     */
    QString getFilterType() const { return getName(); }
};

} // namespace Met3D

#endif //MET_3D_FILTERSETTINGS_H
