/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pointsourcefactory.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/selectpointsourcedialog.h"
#include "pointgeneratorinterfacesettings.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPointSourceFactory *MPointSourceFactory::instance = nullptr;


QList<MPointGeneratorInterface *>
MPointSourceFactory::getPointGeneratingInterfaces() const
{
    return creators.keys();
}


MPointGeneratorInterface *MPointSourceFactory::getInterface(
    MPointGeneratorInterfaceType type, const QString &id)
{
    auto interfaces = getInstance()->getPointGeneratingInterfaces();
    for (MPointGeneratorInterface *interface : interfaces)
    {
        if (interface->getType() == type
            && interface->getPointInterfaceName() == id)
        {
            return interface;
        }
    }
    return nullptr;
}


void MPointSourceFactory::deregisterPointGeneratingInterface(MPointGeneratorInterface *interface)
{
    creators.remove(interface);
    uiCreators.remove(interface);
}


MPointSourceFactory *MPointSourceFactory::getInstance()
{
    if (! MPointSourceFactory::instance)
    {
        MPointSourceFactory::instance = new MPointSourceFactory();
    }
    return MPointSourceFactory::instance;
}


MPointGeneratorInterface *MPointSourceFactory::showCreationDialog()
{
    MSelectPointSourceDialog dialog;
    if (dialog.exec() == QDialog::Rejected)
    {
        return nullptr;
    }

    MPointGeneratorInterface* interface = dialog.getSelectedInterface();
    return interface;
}


MPointGeneratorInterfaceSettings *
MPointSourceFactory::createUI(MPointGeneratorInterface *interface,
                               MActor *actor, MProperty *parent)
{
    if (! uiCreators.contains(interface))
    {
        return nullptr; // Interface not registered.
    }
    // Return new UI instance.
    return uiCreators[interface](actor, parent);
}


MPointGeneratorSource *
MPointSourceFactory::create(MPointGeneratorInterfaceSettings *pointSourceUI)
{
    MPointGeneratorInterface* interface = pointSourceUI->getInterface();
    if (! creators.contains(interface))
    {
        return nullptr; // Filter not registered.
    }
    // Return new instance.
    return creators[interface](pointSourceUI);
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D
