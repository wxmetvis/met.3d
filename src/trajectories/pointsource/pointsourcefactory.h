/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_POINTSOURCEFACTORY_H
#define MET_3D_POINTSOURCEFACTORY_H

// standard library imports

// related third party imports
#include <QList>
#include <QSettings>

// local application imports
#include "gxfw/mactor.h"
#include "trajectories/pointsource/pointgeneratorsource.h"

namespace Met3D
{
class MPointGeneratorSource;
class MPointGeneratorInterface;
class MPointGeneratorInterfaceSettings;

/**
 * This class is the factory for point generating sources. The sources are
 * created from @c MPointGeneratingInterface s, which implement functionality
 * on generating seed points.
 */
class MPointSourceFactory
{
public:
    /**
     * @return The factory instance
     */
    static MPointSourceFactory * getInstance();

    /**
     * @return The list of currently registered @c MPointGeneratorInterface.
     */
    QList<MPointGeneratorInterface*> getPointGeneratingInterfaces() const;

    /**
     * @return The @c MPointGeneratorInterface of the given type and id.
     * Returns a nullptr if no interface is registered that fits the type and
     * id.
     */
    static MPointGeneratorInterface *
    getInterface(MPointGeneratorInterfaceType type, const QString &id);

    /**
     * Create a new point generating source given the UI class.
     * @param pointSourceUI The UI class the source should be based on.
     */
    MPointGeneratorSource* create(MPointGeneratorInterfaceSettings *pointSourceUI);

    /**
     * Creates an UI object for the given source interface.
     * @param interface The interface used for registration to the factory.
     * @param actor Reference to the actor where to add the properties to.
     * @param parent The parent property to attach the UI to.
     * @return The newly created UI object.
     */
    MPointGeneratorInterfaceSettings* createUI(MPointGeneratorInterface* interface, MActor* actor, MProperty *parent);

    /**
     * @return The list of already reserved source data request identifiers.
     * Required to not re-use data request IDs to avoid false memory cache
     * hits.
     */
    QStringList getReservedRequestStrings() const { return reservedRequestKeys; }

    /**
     * @param interface The interface to be registered.
     */
    template<typename U>
    void registerPointGeneratingInterface(MPointGeneratorInterface* interface)
    {
        // Add creator for this point source type.
        creators[interface] = [](MPointGeneratorInterfaceSettings* interfaceUI)
        {
            auto* source = new MPointGeneratorSource(interfaceUI);
            getInstance()->reservedRequestKeys.append(source->locallyRequiredKeys());
            return source;
        };

        uiCreators[interface] = [interface](MActor* actor, MProperty* parent) {
            U* ui = new U(interface, actor, parent);
            return ui;
        };
    }

    /**
     * @param interface The interface to be removed from the list of registered
     * interfaces.
     */
    void deregisterPointGeneratingInterface(MPointGeneratorInterface* interface);

    /**
     * Opens the dialog to select a new point generating interface.
     * @return The interface selected, or nullptr if cancelled.
     */
    static MPointGeneratorInterface* showCreationDialog();

private:
    static MPointSourceFactory *instance;

    // List of reserved request IDs. We have to make sure that we do not reuse
    // request IDs even after deletion of filters.
    QStringList reservedRequestKeys;

    QMap<MPointGeneratorInterface *,
         std::function<MPointGeneratorSource *(MPointGeneratorInterfaceSettings*)>>
        creators;
    QMap<MPointGeneratorInterface *,
         std::function<MPointGeneratorInterfaceSettings *(MActor *, MProperty*)>>
        uiCreators;
};

} // Met3D


#endif //MET_3D_POINTSOURCEFACTORY_H
