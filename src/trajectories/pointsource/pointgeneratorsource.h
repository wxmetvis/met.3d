/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_POINTGENERATORSOURCE_H
#define MET_3D_POINTGENERATORSOURCE_H

// standard library imports

// related third party imports

// local application imports
#include "pointgeneratorinterface.h"
#include "pointsource.h"

namespace Met3D
{
class MPointGeneratorInterface;
class MPointGeneratorInterfaceSettings;
/**
 * This class represents a data source which can generate data points. It
 * requires an interface (@c MPointGeneratorInterface) which gets called
 * and implements the point sampling. For example, bounding boxes and certain
 * actors can inherit from that interface. The interface creates these sources.
 * A interface (e.g., actor) can be linked to multiple of these
 * @c MPointGeneratorSource.
 */
class MPointGeneratorSource : public MPointSource
{
    Q_OBJECT

    friend class MPointSourceFactory;
public:
    MPointGeneratorSource(MPointGeneratorInterfaceSettings *interfaceUI);

    ~MPointGeneratorSource() override;

    MPoints *getData(MDataRequest request) override
    {
        return dynamic_cast<MPoints *>
        (MPointSource::getData(request));
    }

    MPoints *produceData(MDataRequest request) override;

    MTask *createTaskGraph(MDataRequest request) override;

signals:
    /**
     * Emitted whenever the state of the point source changed, for example when
     * a point source actor is moved and therefore the points to generate
     * change.
     */
    void pointSourceStateChanged();

private:
    // The associated interface.
    MPointGeneratorInterface* interface;
};

} // Met3D


#endif //MET_3D_POINTGENERATORSOURCE_H
