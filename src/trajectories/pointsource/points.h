/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_POINTS_H
#define MET_3D_POINTS_H

// standard library imports

// related third party imports

// local application imports
#include "data/abstractdataitem.h"

namespace Met3D
{
class MStructuredGrid;

/**
 * Data item representing a list of 3-D points.
 */
class MPoints : public MAbstractDataItem
{
public:
    MPoints();

    ~MPoints() override;

    unsigned int getMemorySize_kb() override;

    /**
     * @return True if the data object is empty.
     */
    bool isEmpty() const;

    /**
     * @return The data as point list.
     */
    QVector<QVector3D>& getPoints();

    /**
     * @return The source grid if available.
     */
    MStructuredGrid* getSourceGrid();

    /**
     * Set the source grid. The source grid can be used as "accompanying" object
     * to indicate along what grid the points are aligned. If the points are not
     * aligned along any grid, this stays a nullptr. Used by the thinout filter
     * to compute the thinned out points.
     * This data item handles deletion of the structured grid when this data
     * item is deleted.
     */
    void setSourceGrid(MStructuredGrid* g);

    /**
     * Append a point to the data object.
     * @param point The point to add.
     */
    void append(QVector3D point);

    /**
     * Forwards the source grid from the given data item @p points into this
     * data item. Creates a copy of the grid meta data.
     * @param points The points from which to use the source grid.
     */
    void forwardSourceGrid(MPoints* points);

private:
    QVector<QVector3D> points;

    // Optionally a grid containing the structure of the point source. This
    // can be used in filters down the pipeline, e.g. the thin out filter uses
    // this grid to thin out the points. If a point filter alters the points,
    // it can return a data item without a sourceGrid to show that the points
    // are not aligned on the grid anymore.
    MStructuredGrid* sourceGrid;
};

} // Met3D


#endif //MET_3D_POINTS_H
