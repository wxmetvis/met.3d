/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_POINTSOURCE_H
#define MET_3D_POINTSOURCE_H

// standard library imports

// related third party imports

// local application imports
#include "points.h"
#include "data/scheduleddatasource.h"

namespace Met3D
{
class MPipelineElementSettings;

/**
 * Abstract data source for all sources that return a list of points wrapped
 * in a @c MPoints data item.
 */
class MPointSource : public MScheduledDataSource
{
public:

    MPoints *getData(MDataRequest request) override
    {
        return dynamic_cast<MPoints *>
        (MScheduledDataSource::getData(request));
    }

    MPipelineElementSettings* getUI() { return ui; }

    QStringList locallyRequiredKeys() const { return requestKeys; }

    /**
     * Insert the state of the current source into the request @p rh.
     * @param rh The request helper to populate.
     */
    void insertCurrentStateToRequest(MDataRequestHelper& rh);

protected:
    const QStringList locallyRequiredKeys() override;

    // The request keys to use by this filter in data requests.
    QStringList requestKeys;
    // Optional a UI object handling the frontend settings.
    MPipelineElementSettings * ui;
};

}

#endif //MET_3D_POINTSOURCE_H
