/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "points.h"

// standard library imports

// related third party imports

// local application imports
#include "data/structuredgrid.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPoints::MPoints() : sourceGrid(nullptr)
{}


MPoints::~MPoints()
{
    delete sourceGrid;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

unsigned int MPoints::getMemorySize_kb()
{
    size_t sizeKb = points.size() * sizeof(QVector3D) / 1024;
    if (sourceGrid)
    {
        sizeKb += sourceGrid->getMemorySize_kb();
    }
    return sizeKb;
}


bool MPoints::isEmpty() const
{
    return points.isEmpty();
}


void MPoints::append(QVector3D pt)
{
    points.append(pt);
}


QVector<QVector3D>& MPoints::getPoints()
{
    return points;
}


MStructuredGrid* MPoints::getSourceGrid()
{
    return sourceGrid;
}


void MPoints::setSourceGrid(MStructuredGrid* g)
{
    sourceGrid = g;
}


void MPoints::forwardSourceGrid(MPoints *forwardPoints)
{
    MStructuredGrid* parentGrid = forwardPoints->getSourceGrid();
    if (! parentGrid)
    {
        return;
    }
    if (sourceGrid)
    {
        delete sourceGrid;
        sourceGrid = nullptr;
    }

    unsigned int nlevs = parentGrid->getNumLevels();
    unsigned int nlats = parentGrid->getNumLats();
    unsigned int nlons = parentGrid->getNumLons();

    // Create grid and fill lat, lon, level coordinate data.
    sourceGrid = new MRegularLonLatStructuredPressureGrid(nlevs, nlats, nlons);
    for (unsigned int p = 0; p < nlats; p++)
    {
        sourceGrid->setLat(p, parentGrid->getLats()[p]);
    }
    for (unsigned int p = 0; p < nlons; p++)
    {
        sourceGrid->setLon(p, parentGrid->getLons()[p]);
    }
    for (unsigned int p = 0; p < nlevs; p++)
    {
        sourceGrid->setLevel(p, parentGrid->getLevels()[p]);
    }
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/


} // Met3D