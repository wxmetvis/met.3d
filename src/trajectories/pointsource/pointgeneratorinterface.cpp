/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pointgeneratorinterface.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mglresourcesmanager.h"
#include "pointsourcefactory.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/


MPointGeneratorInterface::MPointGeneratorInterface(MPointGeneratorInterfaceType type)
    : type(type)
{
}


MPointGeneratorInterface::~MPointGeneratorInterface()
{
    for (MPointGeneratorSource* source : sourcesOfThisInstance)
    {
        delete source;
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MPointGeneratorInterface::pointInterfaceChanged() {
    for (MPointGeneratorSource* source : sourcesOfThisInstance)
    {
        emit source->pointSourceStateChanged();
    }
}


void MPointGeneratorInterface::registerPointSource(MPointGeneratorSource* source)
{
    sourcesOfThisInstance.append(source);
}


void MPointGeneratorInterface::deregisterPointSource(MPointGeneratorSource *source)
{
    sourcesOfThisInstance.removeOne(source);
}


QString MPointGeneratorInterface::generatorTypeToString(MPointGeneratorInterfaceType t)
{
    switch (t)
    {
    case ACTOR:
        return "Actor";
    case BOUNDING_BOX:
        return "Bounding Box";
    default:
        return "Other";
    }
}


MPointGeneratorInterfaceType MPointGeneratorInterface::stringToInterfaceType(const QString &str)
{
    if (str == "Actor")
    {
        return ACTOR;
    }
    else if (str == "Bounding Box")
    {
        return BOUNDING_BOX;
    }
    return OTHER;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // Met3D