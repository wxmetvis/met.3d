/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pointgeneratorsource.h"

// standard library imports

// related third party imports

// local application imports
#include "pointgeneratorinterface.h"
#include "pointgeneratorinterfacesettings.h"
#include "pointsourcefactory.h"
#include "util/mutil.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPointGeneratorSource::MPointGeneratorSource(MPointGeneratorInterfaceSettings *interfaceUI)
    : interface(interfaceUI->getInterface())
{
    this->ui = interfaceUI;

    // Get the list of reserved request string identifiers, so we can generate
    // a new unused one.
    QStringList reservedKeys =
        MPointSourceFactory::getInstance()->getReservedRequestStrings();
    QStringList baseRequestKeys = interface->getPointGeneratingSourceBaseRequestKeys();


    requestKeys = baseRequestKeys;
    // If one of the filter request keys already exist, pick a different one.
    for (int i = 0; i < requestKeys.size(); i++)
    {
        // Check if the base request key is already in use: then, add _1 or _2..
        if (reservedKeys.contains(requestKeys[i]))
        {
            int iter = 0;
            do
            {
                iter += 1;
                requestKeys[i] = QString(baseRequestKeys[i] + "_%1").arg(iter);
            } while (reservedKeys.contains(requestKeys[i]));
        }
    }

    // Register point source to the interface to notify changes from the interface.
    interface->registerPointSource(this);
}


MPointGeneratorSource::~MPointGeneratorSource()
{
    interface->deregisterPointSource(this);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPoints *MPointGeneratorSource::produceData(MDataRequest request)
{
    MDataRequestHelper rh(request);
    QString pointGeneratingString = rh.value(requestKeys[0]);

    // Request the seed points from the connected point generating interface.
    MPoints *seedPoints =
        interface->generatePointsFromString(pointGeneratingString);
    return seedPoints;
}


MTask *MPointGeneratorSource::createTaskGraph(MDataRequest request)
{
    // Generates data, no parent to add.
    MDataRequestHelper rh(request);
    rh.removeAllKeysExcept(requiredKeys());

    auto* task = new MTask(rh.request(), this);
    return task;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D