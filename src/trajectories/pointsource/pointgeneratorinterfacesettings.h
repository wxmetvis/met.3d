/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_POINTGENERATORINTERFACESETTINGS_H
#define MET_3D_POINTGENERATORINTERFACESETTINGS_H

// standard library imports

// related third party imports

// local application imports
#include "trajectories/pipelineelementsettings.h"
#include "gxfw/properties/mnumberproperty.h"
#include "gxfw/properties/mvector3dproperty.h"
#include "gxfw/properties/mpointfproperty.h"

namespace Met3D
{

class MPointGeneratorInterface;
/**
 * This is the superclass for all UI objects which represent point generating
 * data sources (@c MPointGeneratorSource, @c MPointGeneratorInterface).
 */
class MPointGeneratorInterfaceSettings : public MPipelineElementSettings
{
    Q_OBJECT
public:
    /**
     * Creates a new UI object linked to an interface which uses information
     * from this UI to generate points.
     */
    MPointGeneratorInterfaceSettings(MPointGeneratorInterface *i, MActor *actor,
                               MProperty *pProperty);

    void saveConfiguration(QSettings *settings) const final;

    MPointGeneratorInterface *getInterface() const { return interface; }

    void setInterface(MPointGeneratorInterface *i) { interface = i; }

    void insertCurrentStateToRequest(MDataRequestHelper &rh, const QStringList& keys) override;

private:
    MPointGeneratorInterface *interface;

protected:
    // Identifier to reconstruct the type of point source in case it gets lost.
    QString reconstructionName;
};

/**
 * UI for the bounding box point generator interface.
 */
class MBoundingBoxPointGeneratorSettings : public MPointGeneratorInterfaceSettings
{
    Q_OBJECT
public:
    MBoundingBoxPointGeneratorSettings(MPointGeneratorInterface *interface,
                                 MActor *actor, MProperty *pProperty);

    ~MBoundingBoxPointGeneratorSettings() override;

    void loadElementConfiguration(QSettings *settings) override;

    MVector3DProperty spacingProp;
};

/**
 * UI for the horizontal section point generator interface.
 */
class MHSecPointGeneratorSettings : public MPointGeneratorInterfaceSettings
{
public:
    MHSecPointGeneratorSettings(MPointGeneratorInterface *interface, MActor *actor,
                          MProperty *pProperty);
    ~MHSecPointGeneratorSettings() override;

    void loadElementConfiguration(QSettings *settings) override;

    MPointFProperty spacingProp;
};

/**
 * UI for the vertical section point generator interface.
 */
class MVSecPointGeneratorSettings : public MPointGeneratorInterfaceSettings
{
public:
    MVSecPointGeneratorSettings(MPointGeneratorInterface *interface, MActor *actor,
                          MProperty *pProperty);
    ~MVSecPointGeneratorSettings() override;

    void loadElementConfiguration(QSettings *settings) override;

    // Horizontal density and pressure spacing along cross-section.
    MIntProperty horDensityProp;
    MDoubleProperty pSpacingProp;
};

/**
 * UI for the pole actor point generator interface.
 */
class MPoleActorPointGeneratorSettings : public MPointGeneratorInterfaceSettings
{
public:
    MPoleActorPointGeneratorSettings(MPointGeneratorInterface *interface,
                               MActor *actor, MProperty *pProperty);
    ~MPoleActorPointGeneratorSettings() override;

    void loadElementConfiguration(QSettings *settings) override;

    MDoubleProperty pSpacingProp;
    // Min and max pressure for the seed point range.
    MDoubleProperty pMinProp, pMaxProp;
};

} // namespace Met3D

#endif //MET_3D_POINTGENERATORINTERFACESETTINGS_H
