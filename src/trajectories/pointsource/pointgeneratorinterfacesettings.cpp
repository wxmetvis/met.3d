/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pointgeneratorinterfacesettings.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mscenecontrol.h"
#include "pointgeneratorinterface.h"
#include "actors/nwphorizontalsectionactor.h"
#include "actors/nwpverticalsectionactor.h"
#include "gxfw/properties/mpropertytemplates.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPointGeneratorInterfaceSettings::MPointGeneratorInterfaceSettings(
    MPointGeneratorInterface *i, MActor *actor, MProperty *pProperty)
    : MPipelineElementSettings(i->getPointInterfaceName(), actor, pProperty),
      interface(i)
{
    name = interface->getPointInterfaceName();
}


void MPointGeneratorInterfaceSettings::saveConfiguration(QSettings *settings) const
{
    MPointGeneratorInterfaceType type = interface->getType();
    QString sourceType = MPointGeneratorInterface::generatorTypeToString(type);
    // Type of point source, e.g., Actor, Bounding Box
    settings->setValue(savePrefix + "Type", sourceType);
    // Key to reconstruct the point source if the actor, bounding box etc. is
    // not present. This is the static actor name for actors, so a new actor
    // can be constructed or a fitting one loaded from file.
    settings->setValue(savePrefix + "ReconstructionKey", reconstructionName);
    // The identifiable name.
    settings->setValue(savePrefix + "Name", name);
}


void MPointGeneratorInterfaceSettings::insertCurrentStateToRequest(
    MDataRequestHelper &rh, const QStringList& keys)
{
    QString val = interface->generateStringForPointGeneration(this);
    rh.insert(keys[0], val);
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MBoundingBoxPointGeneratorSettings::loadElementConfiguration(
    QSettings *settings)
{
    spacingProp.setX(settings->value(savePrefix + "lonSpacing", 5.f).toFloat());
    spacingProp.setY(settings->value(savePrefix + "latSpacing", 5.f).toFloat());
    spacingProp.setZ(settings->value(savePrefix + "pSpacing", 50.f).toFloat());
}


MBoundingBoxPointGeneratorSettings::MBoundingBoxPointGeneratorSettings(
    MPointGeneratorInterface *interface, MActor *actor, MProperty *pProperty)
    : MPointGeneratorInterfaceSettings(interface, actor, pProperty)
{
    MPointGeneratorInterfaceType type = interface->getType();
    reconstructionName = MPointGeneratorInterface::generatorTypeToString(type);

    // Create property group for this seed actor.
    spacingProp = MPropertyTemplates::GeographicPosition3D("Seed point spacing", {5.f, 5.f, 50.f});
    spacingProp.getXEditor()->setMinMax(.1, 180.);
    spacingProp.getYEditor()->setMinMax(.1, 180.);
    spacingProp.setConfigKey("bbox_seed_point_spacing");
    spacingProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(spacingProp);
}


MBoundingBoxPointGeneratorSettings::~MBoundingBoxPointGeneratorSettings()
= default;


void MHSecPointGeneratorSettings::loadElementConfiguration(QSettings *settings)
{
    spacingProp.setX(settings->value(savePrefix + "lonSpacing", 5.f).toDouble());
    spacingProp.setY(settings->value(savePrefix + "latSpacing", 5.f).toDouble());
}


MHSecPointGeneratorSettings::MHSecPointGeneratorSettings(
    MPointGeneratorInterface *interface, MActor *actor, MProperty *pProperty)
    : MPointGeneratorInterfaceSettings(interface, actor, pProperty)
{
    reconstructionName = MNWPHorizontalSectionActor::staticActorType();

    // Create property group for this seed actor.
    spacingProp = MPropertyTemplates::GeographicPosition2D("Seed point spacing", {5.f, 5.f});
    spacingProp.setConfigKey("hsec_seed_point_spacing");
    spacingProp.setMinMax(.1, 180.);
    spacingProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(spacingProp);
}


MHSecPointGeneratorSettings::~MHSecPointGeneratorSettings()
= default;


void MVSecPointGeneratorSettings::loadElementConfiguration(QSettings *settings)
{
    horDensityProp = settings->value(savePrefix + "horDensity", 3).toInt();
    pSpacingProp = settings->value(savePrefix + "pSpacing", 50.f).toDouble();
}


MVSecPointGeneratorSettings::MVSecPointGeneratorSettings(
    MPointGeneratorInterface *interface, MActor *actor, MProperty *pProperty)
    : MPointGeneratorInterfaceSettings(interface, actor, pProperty)
{
    reconstructionName = MNWPVerticalSectionActor::staticActorType();

    horDensityProp = MIntProperty("Horizontal density", 3);
    horDensityProp.setConfigKey("horizontal_density");
    horDensityProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(horDensityProp);

    pSpacingProp = MDoubleProperty("Pressure spacing", 50.);
    pSpacingProp.setConfigKey("pressure_spacing");
    pSpacingProp.setMinMax(1, 1000);
    pSpacingProp.setDecimals(1);
    pSpacingProp.setStep(5.);
    pSpacingProp.setSuffix(" hPa");
    pSpacingProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(pSpacingProp);
}


MVSecPointGeneratorSettings::~MVSecPointGeneratorSettings()
= default;


void MPoleActorPointGeneratorSettings::loadElementConfiguration(
    QSettings *settings)
{
    pSpacingProp = settings->value(savePrefix + "pSpacing", 50.f).toDouble();
    pMinProp = settings->value(savePrefix + "pMin", 100.f).toDouble();
    pMaxProp = settings->value(savePrefix + "pMax", 1000.f).toDouble();
}


MPoleActorPointGeneratorSettings::MPoleActorPointGeneratorSettings(
    MPointGeneratorInterface *interface, MActor *actor, MProperty *pProperty)
    : MPointGeneratorInterfaceSettings(interface, actor, pProperty)
{
    reconstructionName = MMovablePoleActor::staticActorType();

    pMinProp = MDoubleProperty("Min. pressure", 100.0);
    pMinProp.setConfigKey("min_pressure");
    pMinProp.setMinMax(0.01, 1050.);
    pMinProp.setDecimals(1);
    pMinProp.setStep(25);
    pMinProp.setSuffix(" hPa");
    pMinProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(pMinProp);

    pMaxProp = MDoubleProperty("Max. pressure", 1000.f);
    pMaxProp.setConfigKey("max_pressure");
    pMaxProp.setMinMax(1., 1050.);
    pMaxProp.setDecimals(1);
    pMaxProp.setStep(25.);
    pMaxProp.setSuffix(" hPa");
    pMaxProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(pMaxProp);

    pSpacingProp = MDoubleProperty("Pressure spacing", 50.f);
    pSpacingProp.setConfigKey("pressure_spacing");
    pSpacingProp.setMinMax(1, 1000);
    pSpacingProp.setDecimals(1);
    pSpacingProp.setStep(5);
    pSpacingProp.setSuffix(" hPa");
    pSpacingProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(pSpacingProp);
}


MPoleActorPointGeneratorSettings::~MPoleActorPointGeneratorSettings()
= default;

} // namespace Met3D