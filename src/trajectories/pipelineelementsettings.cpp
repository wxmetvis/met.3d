/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pipelineelementsettings.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mactor.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPipelineElementSettings::MPipelineElementSettings(const QString& name, MActor* actor,
                                                   MProperty *pProperty)
    : name(name)
{
    parentProperty = pProperty;
    groupProp = MProperty(name);
    parentProperty->addSubProperty(groupProp);

    removeProp = MButtonProperty("Remove", "Remove");
    removeProp.registerValueCallback(this, &MPipelineElementSettings::elementRemoved);
    groupProp.addSubProperty(removeProp);
}


MPipelineElementSettings::~MPipelineElementSettings()
{
    removeSelfFromParent();
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MPipelineElementSettings::saveConfiguration(QSettings *settings) const
{
    settings->setValue(savePrefix + "Name", name);
}


void MPipelineElementSettings::loadConfigurationPrior_V_1_14(QSettings *settings)
{
    loadElementConfiguration(settings);
}


void MPipelineElementSettings::removeSelfFromParent() {
    parentProperty->removeSubProperty(groupProp);
}


void MPipelineElementSettings::renameGroup(const QString& newName)
{
    name = newName;
    groupProp.setName(newName);
}


QString MPipelineElementSettings::getName() const
{
    return name;
}


void MPipelineElementSettings::setSavePrefix(const QString &prefix)
{
    savePrefix = prefix;

    // Set the group property config group identifier to the prefix.
    groupProp.setConfigGroup(savePrefix);
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // namespace Met3D