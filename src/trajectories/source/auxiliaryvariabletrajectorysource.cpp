/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "auxiliaryvariabletrajectorysource.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/nwpactorvariable.h"
#include "trajectorydatasource.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

int MAuxiliaryVariableTrajectorySource::counter = 0;

MAuxiliaryVariableTrajectorySource::MAuxiliaryVariableTrajectorySource()
    : MFloatPerTrajectoryVertexSource(),
      auxiliaryVariable(nullptr)
{
    // Since this data source can be removed and added back to the trajectory
    // pipeline, we need to create a new request ID for the lifetime of Met.3D.
    // Otherwise, the memory manager might try to access a request that seems
    // to be computed already, but the data source changed - therefore it is
    // not accessible anymore.
    QString baseRequestKey = "TRAJECTORY_AUX_VAR";

    if (counter > 0)
    {
        requestKeys.append(baseRequestKey + "_" + QString::number(counter));
    }
    else
    {
        requestKeys.append(baseRequestKey);
    }
    counter += 1;

    // Further keys needed as they are required to determine which auxiliary
    // grids are required to compute the result.
    requestKeys.append("INIT_TIME");
    requestKeys.append("VALID_TIME");
    requestKeys.append("END_TIME_FORWARD");
    requestKeys.append("END_TIME_BACKWARD");
    requestKeys.append("INTEGRATE_INIT_AND_VALID");
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MAuxiliaryVariableTrajectorySource::setAuxiliaryVariable(
    MNWPActorVariable *var)
{
    // Remove registration of previously registered data source.
    if (auxiliaryVariable)
    {
        deregisterInputSource(auxiliaryVariable->dataSource);
    }
    registerInputSource(var->dataSource);
    auxiliaryVariable = var;
}


MFloatPerTrajectoryVertexSupplement *
MAuxiliaryVariableTrajectorySource::produceData(MDataRequest request)
{
    MDataRequestHelper rh(request);

    // Required infos from request.
    QDateTime reqInitTime = rh.timeValue("INIT_TIME");
    QDateTime reqValidTime = rh.timeValue("VALID_TIME");
    QDateTime reqEndTimeForward = rh.timeValue("END_TIME_FORWARD");
    QDateTime reqEndTimeBackward = rh.timeValue("END_TIME_BACKWARD");
    bool integrateInitAndValidTimes = rh.intValue("INTEGRATE_INIT_AND_VALID");

    QString trajectoryAuxVarIdentifier = rh.value(getRequestKey());
    QString auxVarName = trajectoryAuxVarIdentifier.split('/').last();

    // Get the requests to the auxiliary data source that are required to
    // interpolate all points along the trajectories.
    QMap<QDateTime, MDataRequest> requiredAuxRequests =
        getRequiredAuxRequests(auxVarName, integrateInitAndValidTimes,
                                  reqInitTime, reqEndTimeBackward, reqEndTimeForward);

    if (requiredAuxRequests.isEmpty())
    {
        // No aux data available. Releasing items not necessary as they were
        // not requested.
        LOG4CPLUS_WARN(mlog, "Selected auxiliary rendering variable not "
                             "available at the times of the trajectories. "
                             "Coloring by auxiliary data not available.");
        return nullptr;
    }

    QList<QDateTime> auxVarTimes = requiredAuxRequests.keys();

    // Get the trajectories, with their times and vertices.
    MTrajectories *trajectories = trajectorySource->getData(request);
    const QVector<QDateTime>& trajectoryTimes = trajectories->getTimes();

    QVector<QVector3D> vertices = trajectories->getVertices();
    int numVertices = vertices.size();

    // Initialized the auxiliary supplement to return.
    auto auxSupplement = new MFloatPerTrajectoryVertexSupplement(
        trajectories->getNumTrajectories(), numVertices);

    // Get the auxiliary data grids.
    QMap<QDateTime, MStructuredGrid *> auxFields;
    for (auto it = requiredAuxRequests.keyValueBegin();
         it != requiredAuxRequests.keyValueEnd(); ++it)
    {
        MStructuredGrid *auxData =
            auxiliaryVariable->dataSource->getData(it->second);
        auxFields[it->first] = auxData;
    }

    // Iterate over all the trajectory times, find the two nearest aux data
    // fields, and interpolate at the given vertex between both times.
    for (int i = 0; i < trajectoryTimes.size(); i++)
    {
        QDateTime currentTrajectoryTime = trajectoryTimes[i];

        // Get the index in the auxiliary data time list nearest to the
        // current trajectory time.
        int auxTimeIndexAfterCurrent =
            std::lower_bound(auxVarTimes.begin(), auxVarTimes.end(),
                             currentTrajectoryTime)
            - auxVarTimes.begin();
        int auxTimeIndexBeforeCurrent = auxTimeIndexAfterCurrent - 1;

        MStructuredGrid *auxFieldBefore = nullptr;
        MStructuredGrid *auxFieldAfter = nullptr;

        if (auxTimeIndexBeforeCurrent >= 0)
        {
            auxFieldBefore = auxFields[auxVarTimes.at(auxTimeIndexBeforeCurrent)];
        }
        if (auxTimeIndexAfterCurrent < auxVarTimes.size())
        {
            auxFieldAfter = auxFields[auxVarTimes.at(auxTimeIndexAfterCurrent)];
        }

        // Interpolation weight between the two times auxiliary data is available.
        // Only interpolate if both fields are present, otherwise we are at
        // the start or end.
        float weight = -1.0;
        if (auxFieldBefore && auxFieldAfter)
        {
            // (tTrajectory - tAuxBefore) / (tAuxAfter - tAuxBefore)
            // tTrajectory - tAuxBefore
            long traj_t1 = auxVarTimes.at(auxTimeIndexBeforeCurrent)
                              .secsTo(currentTrajectoryTime);
            // tAuxAfter - tAuxBefore
            long t2_t1 = auxVarTimes.at(auxTimeIndexBeforeCurrent)
                            .secsTo(auxVarTimes.at(auxTimeIndexAfterCurrent));
            // The interpolation weight.
            weight = static_cast<float>(traj_t1) / static_cast<float>(t2_t1);
        }

        // Iterate over all trajectories with regard to the current trajectory
        // time.
        for (int t = 0; t < trajectories->getNumTrajectories(); t++)
        {
            // Get the vertex at this trajectory.
            int idx = t * trajectories->getNumTimeStepsPerTrajectory() + i;
            const QVector3D& vertex = trajectories->getVertices()[idx];

            float interpolatedAuxValue;
            // Check if one of the fields is missing, then we don't have to
            // interpolate both fields.
            if (! auxFieldBefore && !auxFieldAfter)
            {
                // No data present, skip.
                continue;
            }
            if (! auxFieldBefore)
            {
                interpolatedAuxValue = auxFieldAfter->interpolateValue(vertex);
            }
            else if (! auxFieldAfter)
            {
                interpolatedAuxValue =
                    auxFieldBefore->interpolateValue(vertex);
            }
            else
            {
                float earlierData = auxFieldBefore->interpolateValue(vertex);
                float laterData = auxFieldAfter->interpolateValue(vertex);

                interpolatedAuxValue =
                    (1.f - weight) * earlierData + weight * laterData;
            }

            auxSupplement->setValue(idx, interpolatedAuxValue);
        }
    }

    // Release all the data items.
    for (MStructuredGrid *auxGrid : auxFields.values())
    {
        auxiliaryVariable->dataSource->releaseData(auxGrid);
    }
    trajectorySource->releaseData(trajectories);

    return auxSupplement;
}


MTask *MAuxiliaryVariableTrajectorySource::createTaskGraph(MDataRequest request)
{
    // Create a new task.
    auto *task = new MTask(request, this);
    MDataRequestHelper rh(request);

    // Get info from request needed to build dependent requests.
    QDateTime reqInitTime = rh.timeValue("INIT_TIME");
    QDateTime reqValidTime = rh.timeValue("VALID_TIME");
    QDateTime reqEndTimeForward = rh.timeValue("END_TIME_FORWARD");
    QDateTime reqEndTimeBackward = rh.timeValue("END_TIME_BACKWARD");
    bool integrateInitAndValidTimes = rh.intValue("INTEGRATE_INIT_AND_VALID");
    QString trajectoryAuxVarIdentifier = rh.value(getRequestKey());
    QString auxVarName = trajectoryAuxVarIdentifier.split('/').last();

    // Get the required times from the auxiliary variable field, and the
    // corresponding requests of the grids needed.
    QMap<QDateTime, MDataRequest> requiredAuxRequestMap =
        getRequiredAuxRequests(auxVarName, integrateInitAndValidTimes,
                                  reqInitTime, reqEndTimeBackward, reqEndTimeForward);

    if (requiredAuxRequestMap.isEmpty())
    {
        // No fitting aux data available.
        return task;
    }

    // Add aux var data grids as parents.
    for (const MDataRequest &req : requiredAuxRequestMap.values())
    {
        task->addParent(auxiliaryVariable->dataSource->getTaskGraph(req));
    }
    // Trajectories as parent.
    task->addParent(trajectorySource->getTaskGraph(rh.request()));
    return task;
}


const QStringList MAuxiliaryVariableTrajectorySource::locallyRequiredKeys()
{
    return requestKeys;
}


QMap<QDateTime, MDataRequest>
MAuxiliaryVariableTrajectorySource::getRequiredAuxRequests(
    const QString& var, bool integrateInitAndValid, const QDateTime &initTime,
    const QDateTime &endTimeBackward, const QDateTime &endTimeForward)
{
    QList<QDateTime> availableTimes;

    // First get the available times in the auxiliary variable data: Depending
    // on whether to integrate only VT or IT and VT, check for what times the
    // auxiliary data is present.
    if (integrateInitAndValid)
    {
        QList<QDateTime> availAuxInitTimes =
            auxiliaryVariable->dataSource->availableInitTimes(
                auxiliaryVariable->levelType, var);
        for (const auto &curInitTime : availAuxInitTimes)
        {
            QList<QDateTime> availValidTimesForInitTime =
                auxiliaryVariable->dataSource->availableValidTimes(
                    auxiliaryVariable->levelType, var, curInitTime);
            if (availValidTimesForInitTime.contains(curInitTime))
            {
                // IT=VT pair present at this init time, add to list.
                availableTimes.append(curInitTime);
                continue;
            }
        }
    }
    else
    {
        // Available times are the valid times for this init time.
        if (auxiliaryVariable->dataSource
                ->availableInitTimes(auxiliaryVariable->levelType,
                                     auxiliaryVariable->variableName)
                .contains(initTime))
        {
            availableTimes = auxiliaryVariable->dataSource->availableValidTimes(
                auxiliaryVariable->levelType, auxiliaryVariable->variableName,
                initTime);
        }
    }

    // If no times are found that match, return without any requests.
    if (availableTimes.isEmpty())
    {
        return QMap<QDateTime, MDataRequest>();
    }

    // Subset these available times, so we only request the times we need. For
    // interpolation, we need the times spanning from endTimeBackward to
    // endTimeForward as both backward and forward trajectories can be requested.

    // Lower bound returns the first item equal or higher than the provided
    // element. Reverse time if endTime < validTime (backward trajectories).
    QDateTime t1 = endTimeBackward;
    QDateTime t2 = endTimeForward;

    // Index of first relevant aux pressure time.
    int lowerIndex = std::lower_bound(availableTimes.begin(),
                                      availableTimes.end(), t1)
                     - availableTimes.begin();
    if (t1 != availableTimes.at(lowerIndex))
    {
        lowerIndex -= 1;
    }
    // Index of last relevant aux pressure time.
    int upperIndex =
        std::lower_bound(availableTimes.begin(), availableTimes.end(), t2)
        - availableTimes.begin();

    lowerIndex = std::max(lowerIndex, 0);
    upperIndex = std::min(upperIndex, availableTimes.size() - 1);

    // Build the requests for each of the available times in the time range.
    QMap<QDateTime, MDataRequest> requests;
    MDataRequestHelper auxDataRequestHelper(
        auxiliaryVariable->buildDataRequest());
    // Works implicitly also for streamlines, as VALID_TIME and END_TIME are
    // the same. Request the required aux fields.
    for (int i = lowerIndex; i <= upperIndex; i++)
    {
        if (integrateInitAndValid)
        {
            auxDataRequestHelper.insert("INIT_TIME", availableTimes.at(i));
        }
        else
        {
            auxDataRequestHelper.insert("INIT_TIME", initTime);
        }
        auxDataRequestHelper.insert("VALID_TIME", availableTimes.at(i));
        auxDataRequestHelper.insert("VARIABLE", var);

        requests[availableTimes.at(i)] = auxDataRequestHelper.request();
    }
    return requests;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // namespace Met3D