/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_TRAJECTORYSUPPLEMENTDATASOURCE_H
#define MET_3D_TRAJECTORYSUPPLEMENTDATASOURCE_H

// standard library imports

// related third party imports

// local application imports
#include "data/scheduleddatasource.h"
#include "trajectories/data/trajectories.h"

namespace Met3D
{
class MTrajectoryDataSource;
class MTrajectorySelectionSource;

/**
 * The general data source for all data sources associated with trajectories.
 * It has references to a trajectory source and a selection source.
 */
class MTrajectorySupplementDataSource : public MScheduledDataSource
{
public:
    MSupplementalTrajectoryData *getData(MDataRequest request) override
    {
        return dynamic_cast<MSupplementalTrajectoryData *>(
            MScheduledDataSource::getData(request));
    }

    virtual void setTrajectorySource(MTrajectoryDataSource* s);

    virtual void setInputSelectionSource(MTrajectorySelectionSource* s);

    MTrajectoryDataSource* trajectorySource;
    MTrajectorySelectionSource* inputSelectionSource;
};

} // Met3D


#endif //MET_3D_TRAJECTORYSUPPLEMENTDATASOURCE_H
