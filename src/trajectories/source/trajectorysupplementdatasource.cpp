/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectorysupplementdatasource.h"

// standard library imports

// related third party imports

// local application imports
#include "trajectorydatasource.h"
#include "trajectoryselectionsource.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

void MTrajectorySupplementDataSource::setTrajectorySource(MTrajectoryDataSource* s)
{
    deregisterInputSource(trajectorySource);
    registerInputSource(s);
    trajectorySource = s;
}


void MTrajectorySupplementDataSource::setInputSelectionSource(MTrajectorySelectionSource *s)
{
    deregisterInputSource(inputSelectionSource);
    registerInputSource(s);
    inputSelectionSource = s;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D