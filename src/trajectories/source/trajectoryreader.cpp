/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020      Marcel Meyer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectoryreader.h"

// standard library imports
#include <iostream>
#include <limits>

// related third party imports
#include <QThread>
#include <QString>
#include <qtconcurrentrun.h>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "util/mstopwatch.h"
#include "gxfw/mglresourcesmanager.h"
#include "data/nccfvar.h"

using namespace netCDF;
using namespace netCDF::exceptions;


namespace Met3D
{


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryReader::MTrajectoryReader(QString identifier)
    : MTrajectoryDataSource(),
      MAbstractDataReader(identifier)
{
}


MTrajectoryReader::~MTrajectoryReader()
{
    QMutexLocker openFilesLocker(&openFilesMutex);
    for (MTrajectoryFileInfo* finfo : openFiles) delete finfo;
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

QList<QDateTime> MTrajectoryReader::availableInitTimes()
{
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    return availableTrajectories.keys();
}


QList<QDateTime> MTrajectoryReader::availableValidTimes(
        const QDateTime& initTime)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableTrajectories.keys().contains(initTime))
        throw MBadDataFieldRequest(
                "unkown init time requested: " +
                initTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);
    return availableTrajectories.value(initTime).keys();
}


bool MTrajectoryReader::trajectoryEndTime(const QDateTime &initTime,
                                          const QDateTime &startTime,
                                          QDateTime &endTime)
{
    if (! availableTrajectories.contains(initTime))
    {
        return false;
    }
    if (! availableTrajectories[initTime].contains(startTime))
    {
        return false;
    }
    endTime = availableTrajectories[initTime][startTime].endTime;
    return true;
}


QList<QDateTime> MTrajectoryReader::validTimeOverlap(
        const QDateTime &initTime, const QDateTime &validTime)
{
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableTrajectories.keys().contains(initTime))
        throw MBadDataFieldRequest(
                "unkown init time requested: " +
                initTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);
    if (!availableTrajectories.value(initTime).keys().contains(validTime))
        throw MBadDataFieldRequest(
                "unkown valid time requested: " +
                validTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);

    return availableTrajectories.value(initTime).value(validTime)
            .validTimeOverlap;
}


QSet<unsigned int> MTrajectoryReader::availableEnsembleMembers()
{
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    return availableMembers;
}


QStringList MTrajectoryReader::availableAuxiliaryVariables()
{
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    return availableAuxDataVariables.values();
}


MTrajectories* MTrajectoryReader::produceData(MDataRequest request)
{
#ifdef MSTOPWATCH_ENABLED
    MStopwatch stopwatch;
#endif

    MDataRequestHelper rh(request);

    QDateTime initTime  = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    unsigned int member = rh.intValue("MEMBER");
    QString timeSpan    = rh.value("TIME_SPAN");

    // If only a specified time interval of the trajectories should be read
    // (timeSpan != ALL), timeSpan contains either a single time or a time
    // interval separated by a "/".
    QDateTime startTime, stopTime;
    if (timeSpan != "ALL")
    {
        QStringList args = timeSpan.split("/");
        startTime = QDateTime::fromString(args[0], Qt::ISODate);
        if (args.size() > 1)
            stopTime = QDateTime::fromString(args[1], Qt::ISODate);
        else
            stopTime = startTime;

        startTime.setTimeSpec(Qt::UTC);
        stopTime.setTimeSpec(Qt::UTC);
    }

    LOG4CPLUS_DEBUG(mlog, "Reading trajectories for IT="
                    << initTime.toString(Qt::ISODate) << ", VT="
                    << validTime.toString(Qt::ISODate) << ", MEM="
                    << member << ", INTERVAL=("
                    << startTime.toString(Qt::ISODate) << "/"
                    << stopTime.toString(Qt::ISODate) << ")");

    // Check validity of initTime and startTime.
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableTrajectories.keys().contains(initTime))
        throw MBadDataFieldRequest(
                "unkown init time requested: " +
                initTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);
    else if (!availableTrajectories.value(initTime).keys().contains(validTime))
        throw MBadDataFieldRequest(
                "unkown start time requested: " +
                validTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);

    // Get the name of the file that stores the requested init and start times
    // and open the NetCDF file..
    QString filename = availableTrajectories.value(initTime).value(validTime)
            .filename;
    availableItemsReadLocker.unlock();

    checkFileOpen(filename);
    openFilesMutex.lock();
    MTrajectoryFileInfo* finfo = openFiles.value(filename);
    openFilesMutex.unlock();

    // Lock access (until method return) to "filename" and associated data.
    QMutexLocker accessMutexLocker( &(finfo->accessMutex) );

    unsigned int numTimeSteps    = finfo->numTimeSteps;
    unsigned int numTrajectories = finfo->numTrajectories;

    // Check if the requested member exists.
    if (member > finfo->numEnsembleMembers - 1)
        throw MBadDataFieldRequest(
                "invalid ensemble member requested",
                __FILE__, __LINE__);

    // Correct the number of timesteps if only a part of the trajectory is read.
    unsigned int startIndex = 0;
    QVector<QDateTime> times;
    if (timeSpan != "ALL")
    {
        startIndex = finfo->times.indexOf(startTime);
        unsigned int stopIndex = finfo->times.indexOf(stopTime);
        numTimeSteps = stopIndex - startIndex + 1;
        times = finfo->times.mid(startIndex, numTimeSteps);
    }
    else
    {
        times = finfo->times;
    }

    // Create temporary memory space for data (we can't read directly into the
    // QVectors that are used in MTrajectories).
    int numVertices = numTimeSteps * numTrajectories;
    float *lons = new float[numVertices];
    float *lats = new float[numVertices];
    float *pres = new float[numVertices];
    float *auxData = new float[numVertices];

    // Read data from file.
    std::vector<size_t> start = {member, 0, startIndex};
    std::vector<size_t> count = {1, numTrajectories, numTimeSteps};
    if (! finfo->hasEnsembleDimension)
    {
        // No ensemble dimension, cut dimension from NetCDF access index.
        start.erase(start.begin());
        count.erase(count.begin());
    }

    QMutexLocker ncAccessMutexLocker(&staticNetCDFAccessMutex);
    try
    {
        finfo->lonVar.getVar(start, count, lons);
        finfo->latVar.getVar(start, count, lats);
        finfo->prsVar.getVar(start, count, pres);
    }
    catch (NcException& e)
    {
        LOG4CPLUS_ERROR(mlog, "Cannot retrieve lon/lat/p data "
                << "for requested trajectories: '" << request << "'.");
        return nullptr;
    }

    // Trajectory pressure coordinate needs to be in hPa; hence scale if
    // given in Pa.
    float toHpaFactor = scaleFactorToHPa(finfo->prsVarUnits.toStdString());
    if (!isnan(toHpaFactor))
    {
        for (int i = 0; i < numVertices; i++) pres[i] *= toHpaFactor;
    }

    // Replace missing values other than "M_INVALID_TRAJECTORY_POS" by
    // "M_INVALID_TRAJECTORY_POS".
    if (finfo->lonMissingValue != M_INVALID_TRAJECTORY_POS)
    {
        for (int i = 0; i < numVertices; i++)
        {
            if (isnan(lons[i]) || floatIsAlmostEqualRelativeAndAbs(
                        lons[i], finfo->lonMissingValue, 1.E-6f))
            {
                lons[i] = M_INVALID_TRAJECTORY_POS;
            }
        }
    }
    if (finfo->latMissingValue != M_INVALID_TRAJECTORY_POS)
    {
        for (int i = 0; i < numVertices; i++)
        {
            if (isnan(lats[i]) || floatIsAlmostEqualRelativeAndAbs(
                        lats[i], finfo->latMissingValue, 1.E-6f))
            {
                lats[i] = M_INVALID_TRAJECTORY_POS;
            }
        }
    }
    if (finfo->prsMissingValue != M_INVALID_TRAJECTORY_POS)
    {
        for (int i = 0; i < numVertices; i++)
        {
            if (isnan(pres[i]) || floatIsAlmostEqualRelativeAndAbs(
                        pres[i], finfo->prsMissingValue, 1.E-6f))
            {
                pres[i] = M_INVALID_TRAJECTORY_POS;
            }
        }
    }


    // Create the trajectory data struct that is returned from this function.
    Met3D::MTrajectories* trajectories =
            new Met3D::MTrajectories(numTrajectories, times);

    trajectories->setMetaData(
                initTime, validTime, "PRECOMPUTED_trajectories", member);

    // Copy temporary data with vertex positions into the return data struct.
    trajectories->copyVertexDataFrom(lons, lats, pres);

    // Read all auxiliary data variables from file and copy to the return
    // data struct.
    for (int iIndexAuxData = 0; iIndexAuxData < finfo->auxDataVars.size();
         iIndexAuxData++)
    {
        // Note: Here we assume that variable is of type float or double,
        // as "auxData" is of type float.

        // Read auxiliary data into temporary data array.
        finfo->auxDataVars[iIndexAuxData].getVar(start, count, auxData);

        // Copy this auxiliary data, iAuxDataVar, to the aux. data struct
        // in the trajectories class.
        trajectories->copyAuxDataPerVertex(auxData, iIndexAuxData);
     }

    ncAccessMutexLocker.unlock();

    // Copy the names of auxiliary data variables.
    trajectories->setAuxDataVariableNames(finfo->auxDataVarNames);

    // Copy start grid geometry, if available.
    trajectories->setStartGrid(finfo->startGrid);


    // Fix the longitude wrapping: If trajectories start at a longitude of +175 and then move over
    // to -175, we should change its longitude to +185 to resolve artifacts.
    trajectories->fixLongitudeWrapping();

    // Debug output.
    // trajectories->dumpStartVerticesToLog(300);

    // Delete temporary data fields.
    delete[] lons;
    delete[] lats;
    delete[] pres;
    delete[] auxData;

#ifdef MSTOPWATCH_ENABLED
    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "single member trajectory read in "
                    << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
                    << " seconds.");
#endif

    return trajectories;
}


MTask* MTrajectoryReader::createTaskGraph(MDataRequest request)
{
    // No dependencies, so we create a plain task.
    MTask* task = new MTask(request, this);
    // However, this task accesses the hard drive.
    task->setDiskReaderTask();
    return task;
}


MFloatPerTrajectorySupplement* MTrajectoryReader::readFloatPerTrajectorySupplement(
        MDataRequest trajectoryRequest, MDataRequest supplementRequest)
{
    // 1) Indentify trajectory file from which supplement should be read.
    // ==================================================================

    MDataRequestHelper rh(trajectoryRequest);

    QDateTime initTime  = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    unsigned int member = rh.intValue("MEMBER");

    LOG4CPLUS_DEBUG(mlog, "Reading float-pre-trajectory supplement for IT="
                    << initTime.toString(Qt::ISODate) << ", VT="
                    << validTime.toString(Qt::ISODate) << ", MEM="
                    << member);

    // Check validity of initTime and validTime.
    // cf.  MClimateForecastReader::availableVariables() .
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (!availableTrajectories.keys().contains(initTime))
        throw MBadDataFieldRequest(
                "unkown init time requested: " +
                initTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);
    else if (!availableTrajectories.value(initTime).keys().contains(validTime))
        throw MBadDataFieldRequest(
                "unkown start time requested: " +
                validTime.toString(Qt::ISODate).toStdString(),
                __FILE__, __LINE__);

    // Get the name of the file that stores the requested init and start times
    // and open the NetCDF file..
    QString filename = availableTrajectories.value(initTime).value(validTime)
            .filename;
    availableItemsReadLocker.unlock();

    checkFileOpen(filename);
    openFilesMutex.lock();
    MTrajectoryFileInfo* finfo = openFiles.value(filename);
    openFilesMutex.unlock();

    // Lock access (until method return) to "filename" and associated data.
    QMutexLocker accessMutexLocker( &(finfo->accessMutex) );

    // 2) Read supplement.
    // ===================

    MDataRequestHelper srh(supplementRequest);
    MFloatPerTrajectorySupplement *supplement = nullptr;

    if (srh.contains("MAX_DELTA_PRESSURE_HOURS"))
    {
        // NetCDF library is not thread-safe (at least the regular C/C++
        // interface is not; hence all NetCDF calls need to be serialized
        // globally in Met.3D! (notes Feb2015).
        QMutexLocker ncAccessMutexLocker(&staticNetCDFAccessMutex);

        // Find the index of the requested time interval.
        int timeInterval_hrs = srh.intValue("MAX_DELTA_PRESSURE_HOURS");

        NcDim tintDim = finfo->ncFile->getDim("time_interval");
        if (tintDim.isNull())
        {
            LOG4CPLUS_ERROR(mlog, "Cannot find precomputed max. delta "
                            "pressure data.");
            return nullptr;
        }
        unsigned int numTimeIntervals = tintDim.getSize();

        NcVar tintVar = finfo->ncFile->getVar("time_interval");
        if (tintVar.isNull())
        {
            LOG4CPLUS_ERROR(mlog, "Cannot find precomputed max. delta "
                            "pressure data.");
            return nullptr;
        }

        QVector<float> timeIntervals(numTimeIntervals);
        tintVar.getVar(timeIntervals.data());

        int tintIndex = timeIntervals.indexOf(timeInterval_hrs);
        if (tintIndex == -1)
        {
            LOG4CPLUS_ERROR_FMT(mlog, "Cannot find requested time "
                                "interval %i for max. delta pressure.",
                                timeInterval_hrs);
        }
        else
        {
            // Read delta pressure data.
            NcVar dpVar = finfo->ncFile->getVar(
                        "delta_pressure_per_time_interval");
            if (dpVar.isNull())
            {
                LOG4CPLUS_ERROR(mlog, "Cannot find precomputed max. "
                                "delta pressure data.");
                return nullptr;
            }

            supplement = new MFloatPerTrajectorySupplement(
                        finfo->numTrajectories);

            std::vector<size_t> start = {member, 0, size_t(tintIndex)};
            std::vector<size_t> count = {1, finfo->numTrajectories, 1};
            if (!finfo->hasEnsembleDimension)
            {
                start.erase(start.begin());
                count.erase(count.begin());
            }
            dpVar.getVar(start, count, supplement->values.data());
        }
    }

    return supplement;
}



/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MTrajectoryReader::scanDataRoot()
{
    // dataRoot has been set in MAbstractDataReader::setDataRoot().

    // Lock access to all availableXX data fields.
    QWriteLocker availableItemsWriteLocker(&availableItemsLock);

    LOG4CPLUS_INFO(mlog, "Scanning directory " << dataRoot.absolutePath()
                                               << " for files with trajectory forecast data.");

    // Get a list of all files in the directory that match the wildcard name
    // filter in "dirFileFilters".
    QStringList availableFiles;
    getAvailableFilesFromFilters(availableFiles);

    // Create progress bar tasks.
    auto progressBar = MProgressBar::getInstance();
    auto taskHandles = progressBar->addTasks(availableFiles.size(), "Reading trajectory files...");

    // For each file, extract information about the contained start time and
    // valid times. Store these information in "availableTrajectories".
    for (int i = 0; i < availableFiles.size(); i++)
    {
        // Mark file from last iteration as completed so we don't have to
        // do this at every 'continue'.
        if (i > 0)
        {
            progressBar->firstTaskCompleted(taskHandles);
        }
        QString filename = availableFiles[i];

        LOG4CPLUS_INFO(mlog, "\tParsing file " << filename << "...");

        // NetCDF library is not thread-safe (at least the regular C/C++
        // interface is not; hence all NetCDF calls need to be serialized
        // globally in Met.3D! (notes Feb2015).
        QMutexLocker ncAccessMutexLocker(&staticNetCDFAccessMutex);

        // Open the file.
        NcFile *ncFile;
        try
        {
            ncFile = new NcFile(dataRoot.filePath(filename).toStdString(),
                                NcFile::read);
        }
        catch (NcException& e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot open NetCDF file with trajectories "
                << filename << ".");
            continue;
        }

        // Get start time and forecast init time of this file. Assume that
        // there is a variable "pressure" from which the time variable can
        // be found.
        NcCFVar currCFVar(ncFile->getVar("pressure"));
        QDateTime startTime  = NcCFVar(currCFVar.getTimeVar())
                .getTimeFromAttribute(QString("trajectory_starttime"));
        QDateTime initTime  = NcCFVar(currCFVar.getTimeVar())
                .getTimeFromAttribute(QString("forecast_inittime"));

        LOG4CPLUS_DEBUG(mlog,
                        "\tstart time: " << startTime.toString(Qt::ISODate)
                                         << ", init time: "
                                         << initTime.toString(Qt::ISODate));

        auto timeVals = currCFVar.getTimeValues();
        QDateTime endTime = currCFVar.getTimeValues().last();

        // Store the time values with the current filename in
        // "availableTrajectories".
        availableTrajectories[initTime][startTime].filename = filename;
        availableTrajectories[initTime][startTime].isStartTime = true;
        availableTrajectories[initTime][startTime].endTime = endTime;

        // Add this valid (=start) time to all other valid times it overlaps
        // with. This might introduce wrong valid times -- they are removed
        // below.
        QList<QDateTime> trajectoryTimes = currCFVar.getTimeValues();
        for (int t = 0; t < trajectoryTimes.size(); t++)
        {
            availableTrajectories[initTime][trajectoryTimes[t]]
                    .validTimeOverlap.append(startTime);
        }

        // Determine the available ensemble members if present.
        unsigned int numMembers = 1;
        NcDim ensembleDim = ncFile->getDim("ensemble");
        if (!ensembleDim.isNull())
        {
            numMembers = ensembleDim.getSize();
        }
        for (unsigned int m = 0; m < numMembers; m++)
        {
            availableMembers.insert(m);
        }

        // Determine available auxiliary data variables.
        // Get auxiliary data along trajectories by screening
        // all available ncvars in the input file and picking
        // those vars indicated as "aux. data" by the
        // nc var attribute "auxiliary_data".
        std::multimap<std::string, NcVar> ncVariables = ncFile->getVars();
        for (std::multimap<std::string, NcVar>::iterator iVar = ncVariables.begin();
             iVar != ncVariables.end(); iVar++)
        {
            QString varName = QString::fromStdString(iVar->first);
            NcCFVar iCFVar(ncFile->getVar(varName.toStdString()));

            std::string auxDataIndicator = "";
            try { auxDataIndicator = iCFVar.getAttributeValueAsString("auxiliary_data"); }
            catch (NcException&) {}

            if (auxDataIndicator == "yes")
            {
                availableAuxDataVariables.insert(varName);
            }
        }

        delete ncFile;
    } // for (files)

    // Last file completed.
    progressBar->tasksCompleted(taskHandles);

    // After all files have been scanned remove wrong valid times (see above).
    // (Do not use availableInitTimes() and availableValidTimes() as they
    // would lock for read and produce a deadlock.)
    QList<QDateTime> initTimes = availableTrajectories.keys();
    for (int i = 0; i < initTimes.size(); i++)
    {
        QList<QDateTime> validTimes = availableTrajectories[initTimes[i]].keys();
        for (int j = 0; j < validTimes.size(); j++)
            if (!availableTrajectories[initTimes[i]][validTimes[j]].isStartTime)
                availableTrajectories[initTimes[i]].remove(validTimes[j]);
    }
}


const QStringList MTrajectoryReader::locallyRequiredKeys()
{
    return (QStringList() << "INIT_TIME" << "VALID_TIME" << "MEMBER"
            << "TIME_SPAN");
}


void MTrajectoryReader::checkFileOpen(QString filename)
{
    openFilesMutex.lock();
    // Check if a new MTrajectoryFileInfo object needs to be inserted into
    // the dictionary.
    if ( !openFiles.contains(filename) )
        openFiles.insert(filename, new MTrajectoryFileInfo());
    MTrajectoryFileInfo* finfo = openFiles.value(filename);
    openFilesMutex.unlock();

    // Lock access to "filename" and associated data.
    QMutexLocker accessMutexLocker( &(finfo->accessMutex) );

    // Is this file opened for the first time?
    bool initialFileAccess = (finfo->ncFile == nullptr);

    if (initialFileAccess)
    {
        // The file is accessed for the first time -- open.
        LOG4CPLUS_INFO(mlog, "Initial file access for trajectory file "
                        << filename << "; opening file.");

        // NetCDF library is not thread-safe (at least the regular C/C++
        // interface is not; hence all NetCDF calls need to be serialized
        // globally in Met.3D! (notes Feb2015).
        QMutexLocker ncAccessMutexLocker(&staticNetCDFAccessMutex);

        try
        {
            finfo->ncFile = new NcFile(
                        dataRoot.filePath(filename).toStdString(), NcFile::read);
        }
        catch (NcException& e)
        {
            LOG4CPLUS_ERROR(mlog, "Cannot open NetCDF file with trajectories "
                << filename);
            throw;
        }

        NcFile *ncFile = finfo->ncFile;

        // Query dimension sizes.
        NcDim timeDim       = ncFile->getDim("time");
        NcDim trajectoryDim = ncFile->getDim("trajectory");
        NcDim ensembleDim   = ncFile->getDim("ensemble");
        finfo->numTimeSteps       = timeDim.getSize();
        finfo->numTrajectories    = trajectoryDim.getSize();
        finfo->hasEnsembleDimension = !ensembleDim.isNull();

        if (finfo->hasEnsembleDimension)
        {
            finfo->numEnsembleMembers = ensembleDim.getSize();
        }
        else
        {
            finfo->numEnsembleMembers = 1;
        }

        // Get coordinate data variables.
        finfo->lonVar = ncFile->getVar("lon");
        finfo->latVar = ncFile->getVar("lat");
        finfo->prsVar = ncFile->getVar("pressure");

        // Get units of pressure variable.
        std::string units = "";
        try
        {
            units = finfo->prsVar.getAttributeValueAsString("units");
        }
        catch (NcException&)
        {
            LOG4CPLUS_WARN(mlog, "Cannot determine units of trajectory "
                           "pressure variable. Assuming 'hPa'.");
            units = "hPa";
        }
        finfo->prsVarUnits = units.c_str();

        // Check if missing values are provided.
//TODO (mr, 02Nov2020) -- eliminate the code repition below...
        try
        {
            finfo->lonVar.getAtt("_FillValue").getValues(
                        &(finfo->lonMissingValue));
        }
        catch (NcException&)
        {
            try
            {
                finfo->lonVar.getAtt("missing_value").getValues(
                            &(finfo->lonMissingValue));
            }
            catch (NcException&) {}
        }
        if (finfo->lonMissingValue != M_INVALID_TRAJECTORY_POS)
        {
            LOG4CPLUS_DEBUG(mlog, "Trajectories: Missing value has been "
                                  "provided for 'lon': "
                            << finfo->lonMissingValue << ".");
        }

        try
        {
            finfo->latVar.getAtt("_FillValue").getValues(
                        &(finfo->latMissingValue));
        }
        catch (NcException&)
        {
            try
            {
                finfo->latVar.getAtt("missing_value").getValues(
                            &(finfo->latMissingValue));
            }
            catch (NcException&) {}
        }
        if (finfo->latMissingValue != M_INVALID_TRAJECTORY_POS)
        {
            LOG4CPLUS_DEBUG(mlog, "Trajectories: Missing value has been "
                                  "provided for 'lat': "
                            << finfo->latMissingValue << ".");
        }

        try
        {
            finfo->prsVar.getAtt("_FillValue").getValues(
                        &(finfo->prsMissingValue));
        }
        catch (NcException&)
        {
            try
            {
                finfo->prsVar.getAtt("missing_value").getValues(
                            &(finfo->prsMissingValue));
            }
            catch (NcException&) {}
        }
        if (finfo->prsMissingValue != M_INVALID_TRAJECTORY_POS)
        {
            LOG4CPLUS_DEBUG(mlog, "Trajectories: Missing value has been "
                                  "provided for 'pressure': "
                            << finfo->prsMissingValue << ".");
        }


        // Get auxiliary data along trajectories by screening
        // all available ncvars in the input file and picking
        // those vars indicated as "aux. data" by the
        // nc var attribute "auxiliary_data".
        std::multimap<std::string, NcVar> ncVariables = ncFile->getVars();
        for (std::multimap<std::string, NcVar>::iterator iVar = ncVariables.begin();
             iVar != ncVariables.end(); iVar++)
        {
            QString varName = QString::fromStdString(iVar->first);
            NcCFVar iCFVar(ncFile->getVar(varName.toStdString()));

            std::string auxDataIndicator = "";
            try { auxDataIndicator = iCFVar.getAttributeValueAsString("auxiliary_data"); }
            catch (NcException&) {}

            if (auxDataIndicator == "yes")
            {
                // Currently we can only handle variables of type float or
                // double.
                NcType varType = iCFVar.getType();
                if (varType == NcType::nc_FLOAT || varType == NcType::nc_DOUBLE)
                {
                    finfo->auxDataVarNames.append(varName);
                    finfo->auxDataVars.append(ncFile->getVar(
                                                  varName.toStdString()));
                }
            }
        }

        // Get time values.
        NcCFVar cfVar(finfo->prsVar);
        finfo->times = QVector<QDateTime>::fromList(cfVar.getTimeValues());

        // Read start grid geometry (positions on which the trajectories were
        // started), if available in file.
        int nSLons = 0;
        NcDim sLonDim = ncFile->getDim("start_lon");
        if (!sLonDim.isNull()) nSLons = sLonDim.getSize();

        int nSLats = 0;
        NcDim sLatDim = ncFile->getDim("start_lat");
        if (!sLatDim.isNull()) nSLats = sLatDim.getSize();

        int nSLevels = 0;
        NcDim sLevelDim = ncFile->getDim("start_isobaric");
        if (!sLevelDim.isNull())
        {
            nSLevels = sLevelDim.getSize();
            NcCFVar sLevelVar = ncFile->getVar("start_isobaric");
            std::string axistype = sLevelVar.getAttributeValueAsString("axistype");

            if (axistype == "pressure levels")
                finfo->startGrid = std::shared_ptr<MStructuredGrid>(
                            new MRegularLonLatStructuredPressureGrid(
                                nSLevels, nSLats, nSLons));
            else if (axistype == "regular ln(p) levels")
                finfo->startGrid = std::shared_ptr<MStructuredGrid>(
                            new MRegularLonLatLnPGrid(
                                nSLevels, nSLats, nSLons));

            NcVar sLonVar = ncFile->getVar("start_lon");
            sLonVar.getVar(finfo->startGrid->lons);
            NcVar sLatVar = ncFile->getVar("start_lat");
            sLatVar.getVar(finfo->startGrid->lats);
            sLevelVar = ncFile->getVar("start_isobaric");
            sLevelVar.getVar(finfo->startGrid->levels);
        }
        else
        {
            sLevelDim = ncFile->getDim("start_hybrid");
            if (!sLevelDim.isNull())
            {
                nSLevels = sLevelDim.getSize();
                MLonLatHybridSigmaPressureGrid *hybGrid =
                        new MLonLatHybridSigmaPressureGrid(
                            nSLevels, nSLats, nSLons);

                NcCFVar akVar = ncFile->getVar("hyam");
                akVar.getVar(hybGrid->ak_hPa);
                NcCFVar bkVar = ncFile->getVar("hybm");
                bkVar.getVar(hybGrid->bk);

                // Check the units of the ak coefficients: Pa or hPa? We need hPa!
                std::string units = "";
                try { units = akVar.getAttributeValueAsString("units"); }
                catch (NcException&) {}

                float toHpaFactor = scaleFactorToHPa(units);
                if (!isnan(toHpaFactor))
                {
                    // convert ak to hPa
                    for (int i = 0; i < nSLevels; i++) hybGrid->ak_hPa[i] *= toHpaFactor;
                }
                else
                {
                    // .. and if they're in completely different units, raise an
                    // exception.

                    QString err = QString(
                        "invalid units '%1' for ak coefficients, "
                        "expected one of: (%2)").arg(units.c_str(),
                        getSupportedPressureUnits().join(", "));

                    throw MNcException("NcException", err.toStdString(),
                                       __FILE__, __LINE__);
                }

                MRegularLonLatGrid *psfcGrid =
                        new MRegularLonLatGrid(nSLats, nSLons);
                NcCFVar psfcVar = ncFile->getVar(
                            "ensemble_minimum_of_surface_pressure");
                psfcVar.getVar(psfcGrid->data);

                // We need Pa for surface pressure.
                try { units = psfcVar.getAttributeValueAsString("units"); }
                catch (NcException&) {}
                toHpaFactor = scaleFactorToHPa(units);

                if (!isnan(toHpaFactor))
                {
                    // convert to Pa: convert to hPa * 100
                    for (int i = 0; i < nSLons*nSLats; i++)
                        psfcGrid->data[i] *= toHpaFactor * 100.;
                }
                else
                {
                    QString err = QString(
                        "invalid units '%1' for ensemble_minimum_of_surface_pressure, "
                        "expected one of: (%2)").arg(units.c_str(),
                        getSupportedPressureUnits().join(", "));

                    throw MNcException("NcException", err.toStdString(),
                                       __FILE__, __LINE__);
                }

                NcVar sLonVar = ncFile->getVar("start_lon");
                sLonVar.getVar(hybGrid->lons);
                sLonVar.getVar(psfcGrid->lons);
                NcVar sLatVar = ncFile->getVar("start_lat");
                sLatVar.getVar(hybGrid->lats);
                sLatVar.getVar(psfcGrid->lats);
                NcVar sLevelVar = ncFile->getVar("start_hybrid");
                sLevelVar.getVar(hybGrid->levels);

                hybGrid->surfacePressure = psfcGrid;

                finfo->startGrid = std::shared_ptr<MStructuredGrid>(hybGrid);
            }
        }
    } // initialFileAccess
}


} // namespace Met3D
