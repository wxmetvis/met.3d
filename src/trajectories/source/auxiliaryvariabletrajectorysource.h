/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_AUXILIARYVARIABLETRAJECTORYSOURCE_H
#define MET_3D_AUXILIARYVARIABLETRAJECTORYSOURCE_H

// standard library imports

// related third party imports

// local application imports
#include "floatpertrajectoryvertexsource.h"

namespace Met3D
{
class MNWPActorVariable;

/**
 * This data source produces data items, which contain for each vertex in the
 * trajectory data item the value of the auxiliary data variable field.
 */
class MAuxiliaryVariableTrajectorySource : public MFloatPerTrajectoryVertexSource
{
public:
    MAuxiliaryVariableTrajectorySource();

    void setAuxiliaryVariable(MNWPActorVariable* var);

    MFloatPerTrajectoryVertexSupplement* produceData(MDataRequest request) override;

    MTask* createTaskGraph(MDataRequest request) override;

    QString getRequestKey() { return requestKeys[0]; }

protected:
    const QStringList locallyRequiredKeys() override;

private:
    /**
     * Helper function that returns a map of required times to interpolate
     * the auxiliary variable data at the given time range, mapped to the
     * data requests to the auxiliary variable data source.
     * @param var The auxiliary variable name.
     * @param integrateInitAndValid Whether to integrate only along valid
     * time (false) or both init and valid time (true)
     * @param initTime The init time (if @p integrateInitAndValid is false)
     * @param endTimeBackward The earliest time to be interpolated, thus,
     * the maximum backward integration time.
     * @param endTimeForward The end time for the interpolation, thus, the
     * maximum forward integration time.
     * @return A map of all required auxiliary variable times along with the
     * corresponding requests to interpolate the data.
     */
    QMap<QDateTime, MDataRequest> getRequiredAuxRequests(
        const QString& var, bool integrateInitAndValid, const QDateTime &initTime,
        const QDateTime &endTimeBackward, const QDateTime &endTimeForward);

    MNWPActorVariable* auxiliaryVariable;

    // See constructor documentation.
    static int counter;

    // Request keys. First one is the identifiable variable, followed by
    // shared request keys also needed here, e.g., INIT_TIME, VALID_TIME.
    QStringList requestKeys;
};

} // namespace Met3D

#endif //MET_3D_AUXILIARYVARIABLETRAJECTORYSOURCE_H
