/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_TRAJECTORYCOMPUTATIONSOURCE_H
#define MET_3D_TRAJECTORYCOMPUTATIONSOURCE_H

// standard library imports

// related third party imports

// local application imports
#include "trajectorydatasource.h"

namespace Met3D
{
class MTrajectoryRequestDispatcher;
class MPointSource;

/**
 * This class represents the computation source for trajectories computed
 * within Met.3D. It takes a list of points as input, and calls the
 * TrajectoryComputation class to perform the computation.
 */
class MTrajectoryComputationSource : public MTrajectoryDataSource
{
public:
    /**
     * Creates a new computation source to use given the memory manager.
     */
    MTrajectoryComputationSource(MAbstractMemoryManager *memoryManager);

    /**
     * Set the pSource point source from which the seed points are requested.
     * @param pSource The point source.
     */
    void setPointSource(MPointSource *pSource);

    MTrajectories *produceData(MDataRequest request) override;

    MTask *createTaskGraph(MDataRequest request) override;

    const QStringList locallyRequiredKeys() override;

    QList<QDateTime> availableInitTimes() override;

    QList<QDateTime> availableValidTimes(const QDateTime &initTime) override;

    QList<QDateTime> validTimeOverlap(const QDateTime &initTime,
                                      const QDateTime &validTime) override;

    QSet<unsigned int> availableEnsembleMembers() override;

    QStringList availableAuxiliaryVariables() override;

    /**
     * Set the reference to the computationDispatcher emitting the trajectory
     * computation request.
     */
    void setDispatcher(MTrajectoryRequestDispatcher *computationDispatcher);

private:
    // Reference to the point source returning seed points for the computation.
    MPointSource *pointSource;
    // Reference to the dispatcher for shared information.
    MTrajectoryRequestDispatcher *dispatcher;
};

} // namespace Met3D

#endif //MET_3D_TRAJECTORYCOMPUTATIONSOURCE_H
