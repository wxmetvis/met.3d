/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectorycomputationsource.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/nwpactorvariable.h"
#include "trajectories/pointsource/pointsource.h"
#include "trajectories/trajectorycomputation.h"
#include "trajectories/trajectoryrequestdispatcher.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryComputationSource::MTrajectoryComputationSource(
    MAbstractMemoryManager *memoryMgr)
    : pointSource(nullptr),
      dispatcher(nullptr)
{
    setMemoryManager(memoryMgr);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MTrajectoryComputationSource::setPointSource(MPointSource *pSource)
{
    deregisterInputSource(this->pointSource);
    registerInputSource(pSource);
    this->pointSource = pSource;
}


MTrajectories *MTrajectoryComputationSource::produceData(MDataRequest request)
{
    MPoints* seedPoints = pointSource->getData(request);

    // Create the computation instance.
    MTrajectoryComputation computer(dispatcher);
    // Fill it with the required computation data.
    computer.setSeedPoints(seedPoints->getPoints());
    computer.fillParameters(request);

    // Execute the computation.
    computer.compute();
    MTrajectoryComputationResult* result = computer.getResult();

    // Create the trajectory data struct and fill content. This value is
    // returned by this function.
    auto *trajectories =
        new MTrajectories(result->seedPoints.size(), result->times);
    trajectories->copyVertexDataFrom(result->vertices);
    trajectories->setStartGrid(seedPoints->getSourceGrid());

    pointSource->releaseData(seedPoints);
    return trajectories;
}


MTask *MTrajectoryComputationSource::createTaskGraph(MDataRequest request)
{
    MDataRequestHelper rh(request);
    rh.removeAllKeysExcept(requiredKeys());

    // Create a new task.
    auto* task = new MTask(rh.request(), this);

    // Need to know here if we integrate both init and valid times, since this
    // results in different requests to the wind data sources.
    bool integrateInitAndValidTimes = rh.intValue("INTEGRATE_INIT_AND_VALID");

    // Remove variables used by this instance.
    rh.remove("TIME_SPAN");
    rh.remove("LINE_TYPE");
    rh.remove("INTEGRATION_METHOD");
    rh.remove("INTERPOLATION_METHOD");
    rh.remove("SUBTIMESTEPS_PER_DATATIMESTEP");
    rh.remove("VERTEX_AT_SUBTIMESTEPS");
    rh.remove("STREAMLINE_DELTA_S");
    rh.remove("STREAMLINE_LENGTH");
    rh.remove("INTEGRATE_INIT_AND_VALID");
    rh.remove("U_VAR");
    rh.remove("V_VAR");
    rh.remove("W_VAR");

    // Get parameters from request.
    QDateTime initTime = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    QDateTime endTimeForward = rh.timeValue("END_TIME_FORWARD");
    QDateTime endTimeBackward = rh.timeValue("END_TIME_BACKWARD");
    QList<MNWPActorVariable*> variables = dispatcher->getInputWindVariables();

    // Get valid times for given init time.
    QList<QDateTime> validTimes =
        dispatcher->getAvailableTrajectoriesToCompute().value(initTime).keys();

    // Get index range of times to request. From final backward to final
    // forward integration step.
    int startIndex = validTimes.indexOf(endTimeBackward);
    int endIndex = validTimes.indexOf(endTimeForward);

    // Request variables required for computation.
    for (MNWPActorVariable* var : variables)
    {
        for (int i = startIndex; i <= endIndex; i++)
        {
            MDataRequest varReq = var->buildDataRequest();

            MDataRequestHelper rhVar(varReq);
            if (integrateInitAndValidTimes)
            {
                rhVar.insert("INIT_TIME", validTimes.at(i));
            }
            else
            {
                rhVar.insert("INIT_TIME", initTime);
            }
            rhVar.insert("VALID_TIME", validTimes.at(i));
            rhVar.insert("VARIABLE", var->variableName);
            rhVar.insert("LEVELTYPE", var->levelType);

            rhVar.removeAllKeysExcept(var->dataSource->requiredKeys());
            task->addParent(var->dataSource->getTaskGraph(rhVar.request()));
        }
    }

    // Add the point source as parent.
    task->addParent(pointSource->getTaskGraph(rh.request()));

    return task;
}


const QStringList MTrajectoryComputationSource::locallyRequiredKeys()
{
    auto keys = QStringList() << "INIT_TIME" << "VALID_TIME" <<
                "END_TIME_BACKWARD" << "END_TIME_FORWARD" <<
                "MEMBER" << "TIME_SPAN" << "SUBTIMESTEPS_PER_DATATIMESTEP" <<
                "VERTEX_AT_SUBTIMESTEPS" << "STREAMLINE_DELTA_S" <<
                "STREAMLINE_LENGTH" << "LINE_TYPE" << "INTEGRATION_METHOD" <<
                "INTERPOLATION_METHOD" << "INTEGRATE_INIT_AND_VALID" <<
                "U_VAR" << "V_VAR" << "W_VAR";
    return keys;
}


QList<QDateTime> MTrajectoryComputationSource::availableInitTimes()
{
    return dispatcher->availableInitTimes();
}


QList<QDateTime>
MTrajectoryComputationSource::availableValidTimes(const QDateTime &initTime)
{
    return dispatcher->availableValidTimes(initTime);
}


QList<QDateTime>
MTrajectoryComputationSource::validTimeOverlap(const QDateTime &initTime,
                                               const QDateTime &validTime)
{
    return dispatcher->validTimeOverlap(initTime, validTime);
}


QSet<unsigned int> MTrajectoryComputationSource::availableEnsembleMembers()
{
    return dispatcher->availableEnsembleMembers();
}


QStringList MTrajectoryComputationSource::availableAuxiliaryVariables()
{
    return dispatcher->availableAuxiliaryVariables();
}


void MTrajectoryComputationSource::setDispatcher(
    MTrajectoryRequestDispatcher *computationDispatcher)
{
    this->dispatcher = computationDispatcher;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // Met3D