/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "deltapressurepertrajectory.h"

// standard library imports
#include "assert.h"

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "trajectories/data/trajectories.h"
#include "trajectoryreader.h"
#include "util/mutil.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MDeltaPressurePerTrajectorySource::MDeltaPressurePerTrajectorySource(Mode mode)
        : MFloatPerTrajectorySource(), mode(mode)
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MFloatPerTrajectorySupplement* MDeltaPressurePerTrajectorySource::produceData(
        MDataRequest request)
{
    assert(trajectorySource != nullptr);

    MDataRequestHelper rh(request);
    int  timeInterval_hrs = rh.intValue("MAX_DELTA_PRESSURE_HOURS");

    // Request required input trajectory data.
    rh.remove("MAX_DELTA_PRESSURE_HOURS");

    // Compute result from trajectory data.
    // ====================================

    MTrajectories *traj = trajectorySource->getData(rh.request());

    // Number of time steps covering the given time interval.
    int numTimeIntervals = timeInterval_hrs /
            (traj->getTimeStepLength_sec() / 3600.);

    // Range checks: Increment the given number of time intervals by one to get
    // the number of values (i.e. time steps) to compare (e.g. one time step
    // required two values to determine the pressure difference, two time steps
    // correspond to three values etc.). The method requires a minimum of two
    // timesteps, but a maximum of _numTimeStepsPerTrajectory.
    int numTimeStepsPerTrajectory = traj->getNumTimeStepsPerTrajectory();
    int numTimeSteps = numTimeIntervals + 1;
    numTimeSteps = std::max(2, numTimeSteps);
    numTimeSteps = std::min(numTimeSteps, numTimeStepsPerTrajectory);

    LOG4CPLUS_TRACE(mlog, "Computing max. delta pressure over "
                    << numTimeIntervals << " time intervals ("
                    << numTimeSteps << " time steps)...");

    MFloatPerTrajectorySupplement *deltaPressure =
            new MFloatPerTrajectorySupplement(traj->getNumTrajectories());

    QVector<QVector3D> vertices = traj->getVertices();

    int numTrajectories = traj->getNumTrajectories();
    for (int i = 0; i < numTrajectories; i++)
    {
        int trajStartIndex = i * numTimeStepsPerTrajectory;
        deltaPressure->setValue(i, 0.);

        // For every time window [t, t+numTimeSteps] compare the pressure at t
        // with the pressure at (t, t+numTimeSteps]. Maximum and minimum pressure
        // values represent the descent/ascent difference in the time window.
        int lastPossibleStartIndex = numTimeStepsPerTrajectory - numTimeSteps;
        for (int t = 0; t <= lastPossibleStartIndex; t++)
        {
            float pUpper = 1200.f; // upper height
            float pLower = 0.f; // lower height
            float pReference = vertices.at(trajStartIndex + t).z();

            int maxT = t + numTimeSteps;
            for (int it = t; it < maxT; it++)
            {
                float pres = vertices.at(trajStartIndex+it).z();

                // Check for (pres > 0) to skip missing values.
                if ((pres > 0) && (pres < pUpper)) pUpper = pres;
                if ((pres > 0) && (pres > pLower)) pLower = pres;
            }

            float dp; // dp between start vertex of current time window and max/min p within time window.
            if (mode == ASCENT)
            {
                dp = pReference - pUpper;
            }
            else // mode == DESCENT
            {
                dp = pLower - pReference;
            }
            dp = std::max(dp, 0.f);

             // Remember the largest dp (of all sliding time windows) for this trajectory.
            if (dp > deltaPressure->getValues()[i])
            { deltaPressure->setValue(i, dp); }
        }
    }

    trajectorySource->releaseData(traj);
    return deltaPressure;
}


MTask* MDeltaPressurePerTrajectorySource::createTaskGraph(MDataRequest request)
{
    assert(trajectorySource != nullptr);

    MTask *task = new MTask(request, this);

    // Add dependency: the trajectories.
    MDataRequestHelper rh(request);
    rh.removeAll(locallyRequiredKeys());
    task->addParent(trajectorySource->getTaskGraph(rh.request()));

    return task;
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MDeltaPressurePerTrajectorySource::locallyRequiredKeys()
{
    return { "MAX_DELTA_PRESSURE_HOURS" };
}


/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

} // namespace Met3D
