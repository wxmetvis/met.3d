/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2017      Michael Kern [+]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef TRAJECTORIES_H
#define TRAJECTORIES_H

// standard library imports

// related third party imports
#include "GL/glew.h"
#include <QGLWidget>

// local application imports
#include "data/abstractdataitem.h"
#include "gxfw/gl/typedvertexbuffer.h"
#include "data/structuredgrid.h"


namespace Met3D
{

enum class MSupplementalTrajectoryDataType {
    TRAJECTORIES,
    NORMALS,
    FLOAT_PER_TRAJECTORY,
    FLOAT_PER_VERTEX,
    SELECTION,
    SINGLE_TIME_SELECTION,
    OTHER
};

/**
  @brief Base class for all objects that store supplemental data along with
  trajectory data.
 */
class MSupplementalTrajectoryData : public MAbstractDataItem
{
public:
    MSupplementalTrajectoryData(MSupplementalTrajectoryDataType type,
                                unsigned int numTrajectories);

    int getNumTrajectories() const { return numTrajectories; }

    virtual MSupplementalTrajectoryDataType getSupplementsOfType() const
    { return supplementType; }

    void setSupplementType(MSupplementalTrajectoryDataType t)
    { supplementType = t; }

protected:
    int numTrajectories;

private:
    // The type of the trajectory supplement.
    MSupplementalTrajectoryDataType supplementType;

};


/**
  @brief Defines a selection of a trajectory dataset.
 */
class MTrajectorySelection : public MSupplementalTrajectoryData
{
public:
    MTrajectorySelection(unsigned int numTrajectories,
                         QVector<QDateTime> timeValues,
                         QVector3D startGridStride=QVector3D(1,1,1));
    ~MTrajectorySelection();

    unsigned int getMemorySize_kb();

    /**
      Index [i_filtered] stores the start index of filtered trajectory
      i_filtered in the @ref MTrajectories vertex field (@see
      MTrajectories::getVertices()).

     @note Index i_filtered is not equal to trajectory i_full in the "full",
     unfiltered field of trajectories returned by @ref
     MTrajectories::getVertices(). Use i_full =
     ceil(float(startIndex)/numTimes) to get this index.
     */
    inline const GLint* getStartIndices() const { return startIndices; }

    /**
      Index [i] stores the number of vertices of filtered trajectory i in the
      @ref MTrajectories vertex field (@see MTrajectories::getVertices()).
     */
    inline const GLsizei* getIndexCount() const { return indexCount; }

    /**
      Total number of timesteps of each trajectory. This number need not be
      equal to a trajectory's index count!
     */
    inline int getNumTimeStepsPerTrajectory() const { return times.size(); }

    inline const QVector<QDateTime>& getTimes() const { return times; }

    inline const QVector3D getStartGridStride() const { return startGridStride; }

protected:
    friend class MIsosurfaceIntersectionSource;
    friend class MLineGeometryFilter;
    GLint   *startIndices;
    GLsizei *indexCount;
    int      maxNumTrajectories;

    QVector<QDateTime> times;

    QVector3D startGridStride; // this is 1 for each coordinate unless
                               // trajectories have been thinned out
};

/**
  @brief Defines a selection of a trajectory dataset.
 */
class MTrajectoryEnsembleSelection : public MTrajectorySelection
{
public:
    MTrajectoryEnsembleSelection(unsigned int numTrajectories,
                                 QVector<QDateTime> timeValues,
                                 QVector3D startGridStride = QVector3D(1, 1, 1),
                                 unsigned int numEnsembles = 1);

    ~MTrajectoryEnsembleSelection();

    inline const QVector<GLint> getEnsembleStartIndices() const
    { return ensembleStartIndices; }

    inline const QVector<GLsizei> getEnsembleIndexCount() const
    { return ensembleIndexCount; }

    inline const unsigned int getNumEnsembleMembers() const
    { return numEnsembleMembers; }

protected:
    QVector<GLint> ensembleStartIndices;
    QVector<GLsizei> ensembleIndexCount;
    unsigned int numEnsembleMembers;
};


/**
  @brief As @ref MTrajectorySelection, but can be written.
 */
class MWritableTrajectorySelection : public MTrajectorySelection
{
public:
    MWritableTrajectorySelection(unsigned int numTrajectories,
                                 QVector<QDateTime> timeValues,
                                 QVector3D startGridStride);

    inline void setStartIndex(unsigned int i, int value)
    { startIndices[i] = value; }

    inline void setIndexCount(unsigned int i, int value)
    { indexCount[i] = value; }

    /**
      Only modify start grid stride if trajectories have been thinned out!
     */
    inline void setStartGridStride(QVector3D stride)
    { startGridStride = stride; }

    /**
      Decrease the number of selected trajectories to @p n. @p n needs to be
      smaller than the number of trajectories specified in the constructor.
     */
    void decreaseNumSelectedTrajectories(int n);

    void increaseNumSelectedTrajectories(int n);

};

/**
@brief As @ref MTrajectorySelection, but can be written.
*/
class MWritableTrajectoryEnsembleSelection : public MTrajectoryEnsembleSelection
{
public:
    MWritableTrajectoryEnsembleSelection(unsigned int numTrajectories,
                                         QVector<QDateTime> timeValues,
                                         QVector3D startGridStride,
                                         unsigned int numEnsembles);

    inline void setStartIndex(unsigned int i, int value)
    { startIndices[i] = value; }

    inline void setIndexCount(unsigned int i, int value)
    { indexCount[i] = value; }

    inline void setEnsembleStartIndex(unsigned int i, int value)
    { ensembleStartIndices[i] = value; }

    inline void setEnsembleIndexCount(unsigned int i, int value)
    { ensembleIndexCount[i] = value; }

    /**
      Only modify start grid stride if trajectories have been thinned out!
     */
    inline void setStartGridStride(QVector3D stride)
    { startGridStride = stride; }

    /**
      Decrease the number of selected trajectories to @p n. @p n needs to be
      smaller than the number of trajectories specified in the constructor.
     */
    void decreaseNumSelectedTrajectories(int n);

    void increaseNumSelectedTrajectories(int n);

};


/**
  Supplements each trajectory of an @ref MTrajectories item with a float
  argument (only one value per trajectory, not one value per vertex!).
 */
class MFloatPerTrajectorySupplement : public MSupplementalTrajectoryData
{
public:
    MFloatPerTrajectorySupplement(unsigned int numTrajectories);

    unsigned int getMemorySize_kb();

    const QVector<float>& getValues() const { return values; }

    inline void setValue(unsigned int i, float value) { values[i] = value; }

protected:
    friend class MTrajectoryReader;

    QVector<float> values;
};

/**
 * Supplement containing one float per trajectory vertex.
 */
class MFloatPerTrajectoryVertexSupplement : public MSupplementalTrajectoryData
{
public:
    MFloatPerTrajectoryVertexSupplement(unsigned int numTrajectories,
                                        unsigned int numVertices);

    unsigned int getMemorySize_kb() override;

    const QVector<float>& getValues() const { return values; }

    inline void setValue(unsigned int i, float value) { values[i] = value; }

    GL::MVertexBuffer *getVertexBuffer(QGLWidget *currentGLContext = 0);

    void releaseVertexBuffer();

private:
    QVector<float> values;
};


/**
  @brief Normals associated with a trajectory dataset and a specific view
  (normals depend on the view's z-scaling).
 */
class MTrajectoryNormals : public MSupplementalTrajectoryData
{
public:
    MTrajectoryNormals(unsigned int numTrajectories,
                       unsigned int numTimeStepsPerTrajectory,
                       QStringList views);

    // When trajectories have different lengths.
    MTrajectoryNormals(unsigned int numVertices);

    ~MTrajectoryNormals();

    unsigned int getMemorySize_kb();

    const QVector<QVector3D>& getWorldSpaceNormals(const QString& sceneName)
    {
        return normalsPerScene[sceneName];
    }

    inline void setNormal(const QString& sceneName, int i, QVector3D normal)
    {
        QVector<QVector3D>& normals = normalsPerScene[sceneName];
        normals[i] = normal;
    }

    /**
     * Get the vertex buffer for the trajectory normals for the given @p scene.
     */
    GL::MVertexBuffer *getVertexBuffer(QString sceneName, QGLWidget *currentGLContext = 0);

    /**
     * Get the list of vertex buffers, one per scene.
     */
    QList<GL::MVertexBuffer*> getVertexBuffers(QGLWidget *currentGLContext = 0);

    /**
     * Release  the vertex buffer related to the given @p sceneName.
     */
    void releaseVertexBuffer(QString sceneName);

    /**
     * Release vertex buffers of all scenes.
     */
    void releaseVertexBuffers();

private:
    QMap<QString, QVector<QVector3D>> normalsPerScene;
};


/**
 @brief Stores the trajectories of a single forecast member at a single
 timestep. The smallest entity that can be read from disk.
 */
class MTrajectories :
        public MWritableTrajectoryEnsembleSelection, public MWeatherPredictionMetaData
{
public:
    /**
      Constructor requires data size for memory allocation.

      Call @ref MWeatherPredictionMetaData::setMetaData() to set init, valid
      time, name and ensemble member.
     */
    MTrajectories(unsigned int numTrajectories,
                  QVector<QDateTime> timeValues);

    /** Destructor frees data in verticesLonLatP. */
    ~MTrajectories();

    virtual unsigned int getMemorySize_kb() override;

    MSupplementalTrajectoryDataType getSupplementsOfType() const override
    { return MSupplementalTrajectoryDataType::TRAJECTORIES; }

    /**
      Copies data from the given float arrays (longitude in degrees, latitude
      in degrees, pressure in hPa) to the internal QVector-based vertex array.
      All three arrays must have the size (numTrajectories *
      numTimeStepsPerTrajectory).
     */
    void copyVertexDataFrom(float *lons, float *lats, float *pres);

    /**
      Copies data from the given vector (numTrajectories ->
      numTimeStepsPerTrajectory -> (longitude in degrees, latitude in degrees,
      pressure in hPa) to the internal QVector-based vertex array.

      Used to copy vertex data from trajectories variable during trajectory
      computation.
    */
    void copyVertexDataFrom(QVector<QVector<QVector3D>> &vertices);

    const QVector<QVector3D>& getVertices() { return vertices; }

    /**
      Copy auxiliary data given as float arrays in the trajectory data file to
      the internal QVector-based auxiliary data array.
     */
    void copyAuxDataPerVertex(float *auxData, int iIndexAuxData);

    /**
      Copy auxiliary data from the QVector-array that is filled during the
      trajectory calculation in Met3D to the internal (trajectory class)
      QVector-based auxiliary data array.
     */
    void copyAuxDataPerVertex(QVector<QVector<QVector<float>>>
                              &auxDataAtVertices);

    /**
      Copy the names of the auxiliary data variables.
     */
    void setAuxDataVariableNames(QStringList varNames);

    /**
     * Fixes longitude wrapping of the underlying vertex buffer. Iterates through
     * the trajectories amd checks whether a trajectory crosses the antimeridian
     * boundary. If they do, fix by adding 360 degrees to the respecting vertices.
     * e.g., trajectory moves from 175 to -175, will be changed to 175 to 185.
     */
    void fixLongitudeWrapping();

    /**
      Get auxiliary data, size of auxiliary data array and names
      of auxiliary data variables.
     */
    const QVector<float>& getAuxDataAtVertex(int i)
    { return auxDataAtVertices[i]; }

    unsigned int getSizeOfAuxDataAtVertices()
    { return auxDataAtVertices.size(); }

    QStringList getAuxDataVarNames() { return auxDataVarNames; }

    /**
      Returns the length of a single time step in seconds.
     */
    unsigned int getTimeStepLength_sec();

    /**
      Pass an MStructuredGrid instance that contains the geometry of the grid
      on which the trajectories were started.
     */
    void setStartGrid(std::shared_ptr<MStructuredGrid> sg) { startGrid = sg; }

    /**
     * Set the start grid by passing a pointer and creating a copy packed in
     * a shared pointer.
     */
    void setStartGrid(MStructuredGrid* sg);

    std::shared_ptr<MStructuredGrid> getStartGrid() const { return startGrid; }

    /**
      Return a vertex buffer object that contains the trajectory data. The
      vertex buffer is created (and data uploaded) on the first call to this
      method.

      The @p currentGLContext argument is necessary as a GPU upload can switch
      the currently active OpenGL context. As this method is usually called
      from a render method, it should switch back to the current render context
      (given by @p currentGLContext).
     */
    GL::MVertexBuffer *getVertexBuffer(QGLWidget *currentGLContext = 0);

    void releaseVertexBuffer();

    /**
      Return a vertex buffer object that contains the auxiliary data along
      trajectories. The vertex buffer is created (and data uploaded) on the
      first call to this method.

      The @p currentGLContext argument is necessary as a GPU upload can switch
      the currently active OpenGL context. As this method is usually called
      from a render method, it should switch back to the current render context
      (given by @p currentGLContext).

      The QString is passed for identifying the requested auxiliary data
      variable.

      @note Be perticularly careful to call the correct @ref
      releaseAuxDataVertexBuffer() after use of the returned vertex buffer has
      finished to avoid memory leaks.
     */
    GL::MVertexBuffer *getAuxDataVertexBuffer(QString requestedAuxDataVarName,
                                              QGLWidget *currentGLContext = 0);

    /**
      Release vertex buffer with auxiliary data. As there can be more than one
      aux.-data var per vertex, its name is provided as a unique identifier.
     */
    void releaseAuxDataVertexBuffer(QString requestedAuxDataVarName);

    /**
     * Release all aux data vertex buffers.
     */
    void releaseAuxDataVertexBuffers();

    /**
      Debug method to dump the start positions if the first @p num trajectories
      to the debug log. If @p selection is specified, dump the first @p num
      trajectories of the selection.
     */
    void dumpStartVerticesToLog(int num, MTrajectorySelection *selection=nullptr);

protected:
    friend class MLineGeometryFilter;
    friend class MIsosurfaceIntersectionSource;

private:
    std::shared_ptr<MStructuredGrid> startGrid;
    QVector<QVector3D> vertices;

    // TODO cf-2024-06: can be removed if this is a supplement also for precomputed trajectories.
    QVector<QVector<float>> auxDataAtVertices;
    QStringList auxDataVarNames;
};


/**
 * Trajectory arrow head geometry data item.
 * Contains the geometry data for the arrow heads of trajectories.
 */
class MTrajectoryArrowHeads : public MAbstractDataItem
{
public:
    struct ArrowHeadVertex
    {
        QVector3D pos;
        QVector3D direction;
        float value;

        ArrowHeadVertex()
                : pos(), direction(), value(0)
        {}

        ArrowHeadVertex(const QVector3D pos, const QVector3D direction, const float value)
                : pos(pos), direction(direction), value(value)
        {}
    };

    explicit MTrajectoryArrowHeads(int numArrows);
    ~MTrajectoryArrowHeads() override;

    unsigned int getMemorySize_kb() override;

    const QVector<ArrowHeadVertex>& getVertices() const { return arrowHeads; }
    void setVertex(int index, const ArrowHeadVertex& arrow);

    GL::MVertexBuffer *getVertexBuffer(QGLWidget *currentGLContext = nullptr);

    void releaseVertexBuffer();

private:
    QVector<ArrowHeadVertex> arrowHeads;
};

/**
 * Data item that contains the values at the vertices of a trajectory.
 */
class MTrajectoryValues : public MAbstractDataItem
{
public:
    explicit MTrajectoryValues(int numValues);
    ~MTrajectoryValues() override;

    unsigned int getMemorySize_kb() override;

    const QVector<float>& getValues() const { return values; }
    void setVertex(int index, float value);

private:
    QVector<float> values;
};

/**
 @brief Stores the isosurface intersection lines in a @ref MTrajectories.
 */
class MIsosurfaceIntersectionLines : public MTrajectories
{
public:
    MIsosurfaceIntersectionLines() :
            MTrajectories(0, QVector<QDateTime>()), lines(nullptr)
    {}

    ~MIsosurfaceIntersectionLines() override;

    unsigned int getMemorySize_kb() override;

protected:
    friend class MIsosurfaceIntersectionSource;
    friend class MIsosurfaceIntersectionActor;
    friend class MLineGeometryFilter;
    friend class MVariableTrajectoryFilter;

    QVector<QVector<QVector3D> *> *lines;
    QVector<float> values;
};


} // namespace Met3D

#endif // TRAJECTORIES_H
