/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2017      Michael Kern [+]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2023      Thorwin Vogt [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectories.h"

// standard library imports
#include <iostream>
#include <limits>

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mexception.h"
#include "gxfw/mglresourcesmanager.h"
#include "glsl/shared/msharedconstants.h"


namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MSupplementalTrajectoryData::MSupplementalTrajectoryData(
        MSupplementalTrajectoryDataType type,
        unsigned int numTrajectories)
    : MAbstractDataItem(),
      numTrajectories(numTrajectories),
      supplementType(type)
{
}


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectorySelection::MTrajectorySelection(unsigned int numTrajectories,
                                           QVector<QDateTime> timeValues,
                                           QVector3D startGridStride)
    : MSupplementalTrajectoryData(MSupplementalTrajectoryDataType::SELECTION,
                                  numTrajectories),
      startIndices(new GLint[numTrajectories]),
      indexCount(new GLsizei[numTrajectories]),
      maxNumTrajectories(numTrajectories),
      times(timeValues),
      startGridStride(startGridStride)
{
}


MTrajectorySelection::~MTrajectorySelection()
{
    delete[] startIndices;
    delete[] indexCount;
}


unsigned int MTrajectorySelection::getMemorySize_kb()
{
    return ( sizeof(MTrajectorySelection)
             + times.size() * sizeof(QDateTime)
             + maxNumTrajectories * (sizeof(GLint) + sizeof(GLsizei))
             ) / 1024.;
}

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryEnsembleSelection::MTrajectoryEnsembleSelection(
        unsigned int numTrajectories,
        QVector<QDateTime> timeValues,
        QVector3D startGridStride,
        unsigned int numEnsembles)
        : MTrajectorySelection(numTrajectories, timeValues,
                               startGridStride),
          ensembleStartIndices(numEnsembles),
          ensembleIndexCount(numEnsembles),
          numEnsembleMembers(numEnsembles)
{
}


MTrajectoryEnsembleSelection::~MTrajectoryEnsembleSelection()
{
}

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MWritableTrajectorySelection::MWritableTrajectorySelection(
        unsigned int numTrajectories,
        QVector<QDateTime> timeValues, QVector3D startGridStride)
    : MTrajectorySelection(numTrajectories, timeValues,
                           startGridStride)
{
}


void MWritableTrajectorySelection::decreaseNumSelectedTrajectories(int n)
{
    if (n <= numTrajectories)
        numTrajectories = n;
    else
        throw MValueError("number of selected trajectories cannot be increased",
                          __FILE__, __LINE__);
}


void MWritableTrajectorySelection::increaseNumSelectedTrajectories(int n)
{
    numTrajectories = n;
}

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MWritableTrajectoryEnsembleSelection::MWritableTrajectoryEnsembleSelection(
        unsigned int numTrajectories,
        QVector<QDateTime> timeValues,
        QVector3D startGridStride,
        unsigned int numEnsembles) :
        MTrajectoryEnsembleSelection(numTrajectories,
                                     timeValues, startGridStride,
                                     numEnsembles)
{
}


void MWritableTrajectoryEnsembleSelection::decreaseNumSelectedTrajectories(
        int n)
{
    if (n <= numTrajectories)
        numTrajectories = n;
    else
        throw MValueError("number of selected trajectories cannot be increased",
                          __FILE__, __LINE__);
}


void MWritableTrajectoryEnsembleSelection::increaseNumSelectedTrajectories(
        int n)
{
    numTrajectories = n;
}


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MFloatPerTrajectorySupplement::MFloatPerTrajectorySupplement(
        unsigned int numTrajectories)
    : MSupplementalTrajectoryData(MSupplementalTrajectoryDataType::FLOAT_PER_TRAJECTORY,
                                  numTrajectories)
{
    values.resize(numTrajectories);
}


unsigned int MFloatPerTrajectorySupplement::getMemorySize_kb()
{
    return ( sizeof(MFloatPerTrajectorySupplement)
             + values.size() * sizeof(float)
             ) / 1024.;
}


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MFloatPerTrajectoryVertexSupplement::MFloatPerTrajectoryVertexSupplement(
    unsigned int numTrajectories, unsigned int numVertices)
    : MSupplementalTrajectoryData(MSupplementalTrajectoryDataType::FLOAT_PER_VERTEX,
                                  numTrajectories)
{
    values.resize(numVertices);
}


unsigned int MFloatPerTrajectoryVertexSupplement::getMemorySize_kb()
{
    return ( sizeof(MFloatPerTrajectoryVertexSupplement)
            + values.size() * sizeof(float)
                ) / 1024.;
}


GL::MVertexBuffer* MFloatPerTrajectoryVertexSupplement::getVertexBuffer(QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    QString gpuItemID = getID();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MVertexBuffer *vb =
        dynamic_cast<GL::MVertexBuffer *>(glRM->getGPUItem(gpuItemID));
    if (vb)
    {
        return vb;
    }

    // No texture with this item's data exists. Create a new one.
    auto *newVB = new GL::MFloatVertexBuffer(gpuItemID, values.size());

    if (glRM->tryStoreGPUItem(newVB))
    {
        newVB->upload(values, currentGLContext);
    }
    else
    {
        delete newVB;
    }

    return dynamic_cast<GL::MVertexBuffer *>(glRM->getGPUItem(gpuItemID));
}


void MFloatPerTrajectoryVertexSupplement::releaseVertexBuffer()
{
    MGLResourcesManager::getInstance()->releaseGPUItem(getID());
}


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryNormals::MTrajectoryNormals(unsigned int numTrajectories,
                                       unsigned int numTimeStepsPerTrajectory,
                                       QStringList sceneNames)
    : MSupplementalTrajectoryData(MSupplementalTrajectoryDataType::NORMALS,
                                  numTrajectories)
{
    for (const QString& sceneName : sceneNames)
    {
        QVector<QVector3D> normals;
        normals.resize(numTrajectories * numTimeStepsPerTrajectory);
        normalsPerScene.insert(sceneName, normals);
    }
}


MTrajectoryNormals::MTrajectoryNormals(unsigned int numVertices)
    : MSupplementalTrajectoryData(MSupplementalTrajectoryDataType::NORMALS, 0)
{
    normalsPerScene.clear();
}


MTrajectoryNormals::~MTrajectoryNormals()
{
    if (! MGLResourcesManager::hasInstance())
    {
        return;
    }

    // Make sure the corresponding data is removed from GPU memory as well.
    for (QString sceneName : normalsPerScene.keys())
    {
        QString gpuItemID = getID() + "_view_" + sceneName;
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(gpuItemID);
    }
}


unsigned int MTrajectoryNormals::getMemorySize_kb()
{
    int sizeOfNormalMap = 0;
    if (! normalsPerScene.isEmpty())
    {
        // Pick first scene entry, all have the same size. Multiply it with amount of scenes.
        sizeOfNormalMap = normalsPerScene.values()[0].size()
                * normalsPerScene.keys().size() * sizeof(QVector3D);
    }
    return (sizeof(MTrajectoryNormals) + sizeOfNormalMap) / 1024.;
}


GL::MVertexBuffer* MTrajectoryNormals::getVertexBuffer(QString sceneName,
        QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();
    QString gpuItemID = getID() + "_view_" + sceneName;
    QVector<QVector3D>& normals = normalsPerScene[sceneName];

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MVertexBuffer *vb = static_cast<GL::MVertexBuffer*>(
                glRM->getGPUItem(gpuItemID));
    if (vb) return vb;

    // No texture with this item's data exists. Create a new one.
    GL::MVector3DVertexBuffer *newVB = new GL::MVector3DVertexBuffer(
            gpuItemID, normals.size());

    if (glRM->tryStoreGPUItem(newVB))
        newVB->upload(normals, currentGLContext);
    else
        delete newVB;

    return static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(gpuItemID));
}


QList<GL::MVertexBuffer*>
MTrajectoryNormals::getVertexBuffers(QGLWidget *currentGLContext)
{
    QList<GL::MVertexBuffer*> buffers;
    for (QString sceneID : normalsPerScene.keys())
    {
        buffers.append(getVertexBuffer(sceneID, currentGLContext));
    }
    return buffers;
}


void MTrajectoryNormals::releaseVertexBuffer(QString sceneName)
{
    QString gpuItemID = getID() + "_view_" + sceneName;
    MGLResourcesManager::getInstance()->releaseGPUItem(gpuItemID);
}


void MTrajectoryNormals::releaseVertexBuffers()
{
    for (QString sceneID : normalsPerScene.keys())
    {
        releaseVertexBuffer(sceneID);
    }
}

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectories::MTrajectories(unsigned int numTrajectories,
        QVector<QDateTime> timeValues)
    : MWritableTrajectoryEnsembleSelection(numTrajectories, timeValues,
        QVector3D(), 1),
      MWeatherPredictionMetaData()
{
    setSupplementType(MSupplementalTrajectoryDataType::TRAJECTORIES);

    int numTimeStepsPerTrajectory = times.size();
    // Allocate memory for each time step of each trajectory (lon/lat/p).
    vertices.resize(numTrajectories*numTimeStepsPerTrajectory);

    // Assign arrays with trajectory start indices and index counts; required
    // for calls to glMultiDrawArrays().
    for (unsigned int i = 0; i < numTrajectories; i++) {
        startIndices[i] = i * numTimeStepsPerTrajectory;
        indexCount[i]   = numTimeStepsPerTrajectory;
    }
}


MTrajectories::~MTrajectories()
{
    // Make sure the corresponding data is removed from GPU memory as well.
    if (MGLResourcesManager::hasInstance())
    {
        MGLResourcesManager::getInstance()->releaseAllGPUItemReferences(getID());
    }
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

unsigned int MTrajectories::getMemorySize_kb()
{
    return ( MTrajectorySelection::getMemorySize_kb()
             + sizeof(MTrajectories)
             + vertices.size() * sizeof(QVector3D)
             + (startGrid ? startGrid->getMemorySize_kb() : 0)
             ) / 1024.;
}


void MTrajectories::copyVertexDataFrom(
        float *lons, float *lats, float *pres)
{
    for (int i = 0; i < getVertices().size(); i++)
    {
        vertices[i] = QVector3D(lons[i], lats[i], pres[i]);
    }
}


void MTrajectories::copyVertexDataFrom(QVector<QVector<QVector3D>> &v)
{
    for (int i = 0; i < v.size(); ++i)
    {
        int viSize = v[i].size();
        for (int j = 0; j < viSize; ++j)
        {
            vertices[i * viSize + j] = v[i][j];
        }
    }
}


void MTrajectories::copyAuxDataPerVertex(float *auxData, int iIndexAuxData)
{
    auxDataAtVertices.resize(getVertices().size());
    for (int i = 0; i < getVertices().size(); i++)
    {        
        auxDataAtVertices[i].resize(iIndexAuxData+1);
        auxDataAtVertices[i][iIndexAuxData] = auxData[i];
    }
}


void MTrajectories::fixLongitudeWrapping()
{
    // Iterate over trajectories.
    for (unsigned int t = 0; t < numTrajectories; t++) {
        if (indexCount[t] == 0) continue;
        int trajectoryStartIndex = startIndices[t];

        // Check for each consecutive pair of vertices if they cross the antimeridian.
        for (int i = 1; i < indexCount[t]; i++)
        {
            int vertexIndex1 = trajectoryStartIndex + i - 1;
            int vertexIndex2 = trajectoryStartIndex + i;

            float lon1 = vertices[vertexIndex1].x();
            float lon2 = vertices[vertexIndex2].x();
            // Crossing the anti-meridian: Add/subtract 360 from latter point.
            // This will then also cascade down to later vertices.
            if (abs(lon1 - lon2) > 180.0)
            {
                if (lon1 > lon2)
                    vertices[vertexIndex2].setX(vertices[vertexIndex2].x() + 360.f);
                else
                    vertices[vertexIndex2].setX(vertices[vertexIndex2].x() - 360.f);
            }
        }
    }
}


void MTrajectories::copyAuxDataPerVertex(QVector<QVector<QVector<float>>> &av)
{
    // Copy auxiliary data from one QVector array to another QVector array
    // with the format of the internal (trajectory class) auxiliary data array.
    // Array-Indexing:
    // Input: The input to this method is a 3-D array (Qvector of Qvector of Qvector
    // with dimensions: (i) trajectories, (ii) time-steps per trajectory,
    // (iii) aux data.
    // Output: The auxiliary data in the trajectory class is stored in the
    // format QVector<Qvector<float>> where the vertices of all trajectories
    // are stored in one long list in the first dimension of the QVector
    // (traj1_v1, traj1_v2,...,traj1_vN, traj2_v1,traj2_v2,...), and the
    // auxiliary data variables at each of the vertices are stored in the
    // second dimension (QVector<float>).
    int numTrajs = av.size();
    for (int i = 0; i < numTrajs; ++i)
    {
        int numVerticesPerTraj = av[i].size();
        for (int j = 0; j < numVerticesPerTraj; ++j)
        {
            if ((i == 0) && (j == 0))
            {
                auxDataAtVertices.resize(av.size()*av[0].size());
            }
            int numAuxDataVars = av[i][j].size();
            for (int k = 0; k < numAuxDataVars; k++)
            {
                auxDataAtVertices[i * numVerticesPerTraj + j].resize(
                            av[0][0].size());
                auxDataAtVertices[i * numVerticesPerTraj + j][k] =
                        av.at(i).at(j).at(k);
            }
        }
    }
}


void MTrajectories::setAuxDataVariableNames(QStringList varNames)
{
    auxDataVarNames = varNames;
}


unsigned int MTrajectories::getTimeStepLength_sec()
{
    if (times.size() < 2)
        return 0;
    else
        return times.at(0).secsTo(times.at(1));
}


GL::MVertexBuffer* MTrajectories::getVertexBuffer(QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MVertexBuffer *vb = static_cast<GL::MVertexBuffer*>(
                glRM->getGPUItem(getID()));
    if (vb) return vb;

    // No texture with this item's data exists. Create a new one.
    GL::MVector3DVertexBuffer *newVB = new GL::MVector3DVertexBuffer(
                getID(), vertices.size());

    if (glRM->tryStoreGPUItem(newVB))
    {
        newVB->upload(vertices, currentGLContext);
    }
    else
    {
        delete newVB;
    }

    return static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(getID()));
}


GL::MVertexBuffer* MTrajectories::getAuxDataVertexBuffer(
        QString requestedAuxDataVarName,
        QGLWidget *currentGLContext)
{
    // Initialize instance of openGL resource manager.
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    // Check if requested variable name exists as aux var.
    assert(auxDataVarNames.contains(requestedAuxDataVarName));

    QString gpuItemID = getID() + "_aux_" + requestedAuxDataVarName;

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MVertexBuffer *vb = static_cast<GL::MVertexBuffer*>(
                glRM->getGPUItem(gpuItemID));
    if (vb) return vb;

    // Get the requested auxiliary data from the QVector with all auxiliary
    // data vars along trajectories.
    QVector<float> requestedAuxDataAtVertices(auxDataAtVertices.size());
    int i = auxDataVarNames.indexOf(requestedAuxDataVarName);
    for (int j = 0; j < auxDataAtVertices.size(); j++)
    {
        requestedAuxDataAtVertices[j] = auxDataAtVertices.at(j).at(i);
    }

    // If no texture with this item's data exists, then create a new one.
    GL::MFloatVertexBuffer *newVB = new GL::MFloatVertexBuffer(
                gpuItemID, requestedAuxDataAtVertices.size());

    // Upload the requested auxiliary data.
    if (glRM->tryStoreGPUItem(newVB))
    {
        newVB->upload(requestedAuxDataAtVertices, currentGLContext);
    }
    else
    {
        delete newVB;
    }

    return static_cast<GL::MVertexBuffer*>(glRM->getGPUItem(gpuItemID));
}


void MTrajectories::releaseVertexBuffer()
{
    MGLResourcesManager::getInstance()->releaseGPUItem(getID());
}


void MTrajectories::releaseAuxDataVertexBuffer(QString requestedAuxDataVarName)
{
    // Check if requested variable name exists as aux var.
    assert(auxDataVarNames.contains(requestedAuxDataVarName));

    QString gpuItemID = getID() + "_aux_" + requestedAuxDataVarName;
    MGLResourcesManager::getInstance()->releaseGPUItem(gpuItemID);
}


void MTrajectories::dumpStartVerticesToLog(int num,
                                           MTrajectorySelection *selection)
{
    if (selection == nullptr) selection = this;
    int tindex = times.indexOf(validTime);
    for (int i = 0; i < std::min(num, selection->getNumTrajectories()); i++)
    {
        QVector3D v = vertices.at(selection->getStartIndices()[i] + tindex);
        LOG4CPLUS_DEBUG_FMT(mlog, "Trajectory %i: (%.2f/%.2f/%.2f)", i,
                            v.x(), v.y(), v.z());

    }
}


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/


MTrajectoryArrowHeads::MTrajectoryArrowHeads(const int numArrows)
    : arrowHeads(numArrows)
{
}


MTrajectoryArrowHeads::~MTrajectoryArrowHeads()
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

unsigned int MTrajectoryArrowHeads::getMemorySize_kb()
{
    return arrowHeads.size() * sizeof(ArrowHeadVertex) / 1024;
}


void MTrajectoryArrowHeads::setVertex(const int index,
                                      const ArrowHeadVertex &arrow)
{
    arrowHeads[index] = arrow;
}


GL::MVertexBuffer *MTrajectoryArrowHeads::getVertexBuffer(
        QGLWidget *currentGLContext)
{
    MGLResourcesManager *glRM = MGLResourcesManager::getInstance();

    const QString vbKey = getID() + "arrowHeads";
    const uint32_t numFloats = 7;

    // Check if a texture with this item's data already exists in GPU memory.
    GL::MVertexBuffer *vb = dynamic_cast<GL::MVertexBuffer *>(
            glRM->getGPUItem(vbKey));
    if (vb) return vb;

    // No texture with this item's data exists. Create a new one.
    auto *newVB = new GL::MFloatVertexBuffer(
            vbKey, arrowHeads.size() * numFloats);

    if (glRM->tryStoreGPUItem(newVB))
    {
        newVB->upload(reinterpret_cast<GLfloat *>(arrowHeads.data()),
                      arrowHeads.size() * numFloats, currentGLContext);
    }
    else
    {
        delete newVB;
    }

    return dynamic_cast<GL::MVertexBuffer *>(glRM->getGPUItem(vbKey));
}


void MTrajectoryArrowHeads::releaseVertexBuffer()
{
    const QString vbKey = getID() + "arrowHeads";
    MGLResourcesManager::getInstance()->releaseGPUItem(vbKey);
}


/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryValues::MTrajectoryValues(const int numValues)
        : values(numValues)
{
}


MTrajectoryValues::~MTrajectoryValues()
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

unsigned int MTrajectoryValues::getMemorySize_kb()
{
    return values.size() * sizeof(float);
}


void MTrajectoryValues::setVertex(const int index, const float value)
{
    values[index] = value;
}


MIsosurfaceIntersectionLines::~MIsosurfaceIntersectionLines()
{
    for (auto l : *lines)
    {
        delete l;
    }

    delete lines;
}


unsigned int MIsosurfaceIntersectionLines::getMemorySize_kb()
{
    uint32_t size = MTrajectories::getMemorySize_kb();

    uint32_t localSize = 0;
    for (QVector<QVector3D>* line : *lines)
    {
        localSize += line->length() * sizeof(QVector3D);
        localSize += sizeof(*line);
    }

    localSize += values.length() * sizeof(float);
    localSize += sizeof(values);

    size += localSize / 1024;

    return size;
}


void MTrajectories::releaseAuxDataVertexBuffers()
{
    for (const QString& auxDataVarName : auxDataVarNames)
    {
        releaseAuxDataVertexBuffer(auxDataVarName);
    }
}


void MTrajectories::setStartGrid(MStructuredGrid *sg)
{
    if (! sg)
    {
        return;
    }
    unsigned int nlevs = sg->getNumLevels();
    unsigned int nlats = sg->getNumLats();
    unsigned int nlons = sg->getNumLons();

    // Create grid and fill lat, lon, level coordinate data.
    startGrid = std::make_shared<MRegularLonLatStructuredPressureGrid>(nlevs, nlats, nlons);

    for (unsigned int p = 0; p < nlats; p++)
    {
        startGrid->setLat(p, sg->getLats()[p]);
    }
    for (unsigned int p = 0; p < nlons; p++)
    {
        startGrid->setLon(p, sg->getLons()[p]);
    }
    for (unsigned int p = 0; p < nlevs; p++)
    {
        startGrid->setLevel(p, sg->getLevels()[p]);
    }
}

} // namespace Met3D
