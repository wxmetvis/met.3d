/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectorydatacollection.h"

// standard library imports
#include <utility>

// related third party imports

// local application imports
#include "data/memorymanageddatasource.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryDataCollection::MTrajectoryDataCollection(
    MTrajectories *trajectories, MTrajectoryNormals *normals,
    MTrajectorySelection *selection, MTrajectorySelection *singleTimeSelection,
    MFloatPerTrajectoryVertexSupplement *vertexSupp,
    QList<MSupplementalTrajectoryData *> supplements)
    : trajectories(trajectories),
      normals(normals),
      selection(selection),
      singleTimeSelection(singleTimeSelection),
      auxiliaryData(vertexSupp),
      supplements(supplements),
      pointSource(nullptr)
{
}


MTrajectoryDataCollection::~MTrajectoryDataCollection()
{
    // Release references of this data item.
    QList<MAbstractDataItem*> references;
    references.append(trajectories);
    references.append(normals);
    references.append(selection);
    references.append(singleTimeSelection);
    references.append(auxiliaryData);

    for (MAbstractDataItem* reference : references)
    {
        if (! reference || ! reference->getStoringObject())
        {
            continue;
        }
        auto refSource = dynamic_cast<MMemoryManagedDataSource *>(
            reference->getStoringObject());
        if (refSource)
        {
            refSource->getMemoryManager()->releaseData(
                refSource, reference->getGeneratingRequest());
        }
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

unsigned int MTrajectoryDataCollection::getMemorySize_kb()
{
    // All the class members are data objects themselves and saved in
    // their own data items in the memory manager. The collection just has
    // references to these objects.
    size_t sizeB = sizeof(MTrajectories*) + sizeof(MTrajectoryNormals*)
                + sizeof(MTrajectorySelection*)
                + sizeof(MSupplementalTrajectoryData*) * supplements.size();

    return static_cast<unsigned int>(sizeB / 1024);
}


const QVector<QVector3D> &
MTrajectoryDataCollection::getNormals(const QString &sceneName)
{
    return normals->getWorldSpaceNormals(sceneName);
}


MTrajectorySelection *MTrajectoryDataCollection::getSingleTimeSelection()
{
    return singleTimeSelection;
}


QList<MSupplementalTrajectoryData*>
MTrajectoryDataCollection::getSupplementsOfType(MSupplementalTrajectoryDataType type)
{
    QList<MSupplementalTrajectoryData*> supplementsOfType;
    for (MSupplementalTrajectoryData* supplement : supplements)
    {
        if (supplement->getSupplementsOfType() == type)
        {
            supplementsOfType.append(supplement);
        }
    }
    return supplementsOfType;
}


void MTrajectoryDataCollection::releaseBuffers()
{
    if (trajectories)
    {
        trajectories->releaseVertexBuffer();
        trajectories->releaseAuxDataVertexBuffers();
    }
    if (normals)
    {
        normals->releaseVertexBuffers();
    }
    if (auxiliaryData)
    {
        auxiliaryData->releaseVertexBuffer();
    }
}


void MTrajectoryDataCollection::setPointGenerator(
    MPointGeneratorSource *generator)
{
    this->pointSource = generator;
}


void MTrajectoryDataCollection::createVertexBuffers()
{
    if (trajectories)
    {
        trajectories->getVertexBuffer();
        // Precomputed auxiliary data if existent.
        for (auto& auxName : trajectories->getAuxDataVarNames())
        {
            trajectories->getAuxDataVertexBuffer(auxName);
        }

        // Print a warning to the user if the generated trajectories have
        // less than four vertices each. Four vertices are required for rendering
        // to include adjacency information.
        if (trajectories->getNumTimeStepsPerTrajectory() < 4)
        {
            LOG4CPLUS_WARN(mlog,
                QString("Received trajectories have %1 vertices per "
                        "trajectory. At least 4 vertices are required. "
                        "Increase the integration time or number of vertices "
                        "per integration timestep to get visible trajectories.")
                    .arg(trajectories->getNumTimeStepsPerTrajectory()));
        }
    }
    if (normals)
    {
        normals->getVertexBuffers();
    }
    if (auxiliaryData)
    {
        auxiliaryData->getVertexBuffer();
    }
}

/********************* MTrajectoryDataCollectionGroup ************************/

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryDataCollectionGroup::~MTrajectoryDataCollectionGroup()
{
    // Release references of this data item.
    for (MTrajectoryDataCollection* collection : dataList)
    {
        auto pipeline = dynamic_cast<MMemoryManagedDataSource *>(
            collection->getStoringObject());
        if (! pipeline)
        {
            continue;
        }
        pipeline->getMemoryManager()->releaseData(
            pipeline, collection->getGeneratingRequest());
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

unsigned int MTrajectoryDataCollectionGroup::getMemorySize_kb()
{
    // All the class members are data objects themselves and saved in
    // their own data items in the memory manager. The collection just has
    // references to these objects.
    int sizeB = dataList.size() * sizeof(MTrajectoryDataCollection*);
    return static_cast<unsigned int>(sizeB / 1024.);
}


void MTrajectoryDataCollectionGroup::append(
    MTrajectoryDataCollection *collection)
{
    dataList.append(collection);
}


void MTrajectoryDataCollectionGroup::releaseBuffers()
{
    for (MTrajectoryDataCollection* collection : dataList)
    {
        collection->releaseBuffers();
    }
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // Met3D