/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_TRAJECTORYDATACOLLECTION_H
#define MET_3D_TRAJECTORYDATACOLLECTION_H

// standard library imports

// related third party imports

// local application imports
#include "trajectories.h"

namespace Met3D
{
/**
 * This data item is returned by the trajectory pipeline. It contains the
 * trajectory vertices themselves, as well as various additional data like
 * normals, single time vertices, filtered trajectories, auxiliary data per
 * vertex and further supplemental data.
 * Note that this item only contains references to the individual data
 * objects contained in this collection.
 */
class MTrajectoryDataCollection : public MAbstractDataItem
{
public:
    MTrajectoryDataCollection(MTrajectories* trajectories,
                              MTrajectoryNormals* normals,
                              MTrajectorySelection* selection,
                              MTrajectorySelection* singleTimeSelection,
                              MFloatPerTrajectoryVertexSupplement* vertexSupp,
                              QList<MSupplementalTrajectoryData*> supplements);

    ~MTrajectoryDataCollection() override;

    unsigned int getMemorySize_kb() override;

    /**
     * Set the point generating source that was used to generate seed points for
     * the trajectory computation. If the trajectories are precomputed,
     * this remains a nullptr.
     */
    void setPointGenerator(MPointGeneratorSource* generator);

    const QVector<QVector3D>& getNormals(const QString& sceneName);

    MTrajectories* getTrajectories() { return trajectories; }

    MTrajectoryNormals* getNormals() { return normals; }

    MTrajectorySelection* getSelection() { return selection; }

    MFloatPerTrajectoryVertexSupplement* getAuxiliaryData()
    { return auxiliaryData; }

    MTrajectorySelection* getSingleTimeSelection();

    MPointGeneratorSource * getPointGenerator() { return pointSource; }

    /**
     * Get the supplement data items that are of the given @p type.
     * @param type The type used to filter the supplements.
     * @return The list of supplements of the provided type.
     */
    QList<MSupplementalTrajectoryData *>
    getSupplementsOfType(MSupplementalTrajectoryDataType type);

    /**
     * Create vertex buffers for trajectories, normals, and auxiliary data
     * per vertex.
     */
    void createVertexBuffers();

    /**
     * Release the data buffers within this data object.
     */
    void releaseBuffers();

private:
    MTrajectories* trajectories;
    MTrajectoryNormals* normals;
    MTrajectorySelection* selection;
    MTrajectorySelection* singleTimeSelection;
    MFloatPerTrajectoryVertexSupplement* auxiliaryData;

    // Everything else...
    QList<MSupplementalTrajectoryData*> supplements;

    // Source that generated the seeding points, if applicable.
    MPointGeneratorSource * pointSource;
};

/**
 * The @c MTrajectoryDataCollectionGroup is a data item consisting of a list of
 * @c MTrajectoryDataCollection. It only contains references to these
 * collections. This is used for the trajectory request dispatcher, which
 * handles a list of trajectory pipelines, each one returning a
 * @c MTrajectoryDataCollection. These collections are bundled into this
 * group and returned to the trajectory actor.
 */
class MTrajectoryDataCollectionGroup : public MAbstractDataItem
{
public:
    ~MTrajectoryDataCollectionGroup() override;

    unsigned int getMemorySize_kb() override;

    void append(MTrajectoryDataCollection* collection);

    QList<MTrajectoryDataCollection*> getCollections() { return dataList; }

    bool isEmpty() const { return dataList.isEmpty(); }

    /**
     * Release the data buffers within this data object.
     */
    void releaseBuffers();

private:
    QList<MTrajectoryDataCollection*> dataList;
};

} // Met3D


#endif //MET_3D_TRAJECTORYDATACOLLECTION_H
