/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_POINTPIPELINE_H
#define MET_3D_POINTPIPELINE_H

// standard library imports

// related third party imports
#include <QObject>

// local application imports
#include "data/datarequest.h"

namespace Met3D
{
class MPointGeneratorSource;
class MPointFilter;
class MPoints;
class MPointSource;

/**
 * This class encapsulates a point pipeline within Met.3D. It contains of a
 * point generating source at the start of the pipeline, and (optionally) a
 * list of point filters, which can add, remove, or modify the points.
 */
class MPointPipeline : public QObject
{
    Q_OBJECT
public:
    MPointPipeline();
    ~MPointPipeline() override;

    /**
     * Add a point filter to the pipeline chain.
     * @param filter The filter to add.
     */
    void addFilter(MPointFilter* filter);

    /**
     * Remove a filter from the pipeline chain. Has no effect if filter is not
     * part of the chain.
     * @param filter The filter to remove, or the index to remove.
     */
    void removeFilter(MPointFilter* filter);
    void removeFilter(int index);

    /**
     * Set the point generating source.
     * @param generator The source.
     */
    void setGenerator(MPointGeneratorSource* generator);

    /**
     * Force an update of the filter chain, thus, inform all filters in the
     * pipeline about their pointSource filter.
     */
    void updateFilterChain();

    /**
     * Get the data from the pipeline given the request. The data is requested
     * from the last element in the pipeline.
     * @param request The data request.
     * @return The @c MPoints data object, or nullptr if data not present.
     */
    MPoints *getData(MDataRequest request) const;

    /**
     * Get the last element of the pipeline. This can either be a point filter,
     * or the point generator if no filter is present.
     * @return The last element in the pipeline chain.
     */
    MPointSource *getLastSourceInChain() const;

    /**
     * @return The point generating source.
     */
    MPointGeneratorSource *getPointGenerator();

    /**
     * Insert the current state of the pipeline to the given request. This
     * asks all the elements in the pipeline to insert their current state into
     * the request.
     * @param rh The request helper to add the state to.
     */
    void insertCurrentStateIntoRequest(MDataRequestHelper& rh) const;

signals:
    /**
     * Signal emitted whenever the state of the point pipeline changed which
     * would require a new data request.
     */
    void pointPipelineStateChanged();

private:
    MPointGeneratorSource *pointGenerator;
    QList<MPointFilter*> pointFilterChain;
};

} // Met3D


#endif //MET_3D_POINTPIPELINE_H
