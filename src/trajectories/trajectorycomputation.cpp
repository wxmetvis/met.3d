/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2017-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "trajectorycomputation.h"

// standard library imports
#include <utility>
#include <array>

// related third party imports

// local application imports
#include "gxfw/nwpactorvariable.h"
#include "trajectoryrequestdispatcher.h"

#define EULER_ITERATION 3

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryComputation::MTrajectoryComputation(
    MTrajectoryRequestDispatcher *dispatcher)
    : dispatcher(dispatcher),
      result(nullptr),
      integrationMethod(EULER),
      interpolationMethod(MET3D_INTERPOLATION),
      lineType(PATH_LINE),
      startTimeStep(0),
      endTimeStepBackward(0),
      endTimeStepForward(0),
      subTimeStepsPerDataTimeStep(0),
      vertexAtSubTimeSteps(false),
      streamlineDeltaS(1.),
      streamlineLength(1),
      integrateInitAndValid(false)
{
}


MTrajectoryComputation::~MTrajectoryComputation()
{
    delete result;
}


void MTrajectoryComputation::setSeedPoints(QVector<QVector3D> pts)
{
    delete result;
    // Create new result object and initialize it with the seed points.
    result = new MTrajectoryComputationResult();
    result->seedPoints = std::move(pts);
}


MTrajectoryComputationResult *MTrajectoryComputation::getResult()
{
    return result;
}


void MTrajectoryComputation::fillParameters(const MDataRequest& request)
{
    // Read parameters from incoming request string.
    // Request data of all variables.
    MDataRequestHelper rh(request);
    QDateTime initTime = rh.timeValue("INIT_TIME");
    QDateTime validTime = rh.timeValue("VALID_TIME");
    QDateTime endTimeForward = rh.timeValue("END_TIME_FORWARD");
    QDateTime endTimeBackward = rh.timeValue("END_TIME_BACKWARD");

    integrationMethod = MTrajectoryComputationIntegrationMethod(
        rh.intValue("INTEGRATION_METHOD"));
    interpolationMethod = MTrajectoryComputationInterpolationMethod(
        rh.intValue("INTERPOLATION_METHOD"));
    lineType = MTrajectoryComputationLineType(rh.intValue("LINE_TYPE"));

    subTimeStepsPerDataTimeStep = rh.intValue("SUBTIMESTEPS_PER_DATATIMESTEP");
    vertexAtSubTimeSteps = rh.intValue("VERTEX_AT_SUBTIMESTEPS") == 1;
    streamlineDeltaS = rh.floatValue("STREAMLINE_DELTA_S");
    streamlineLength = rh.intValue("STREAMLINE_LENGTH");
    integrateInitAndValid = rh.intValue("INTEGRATE_INIT_AND_VALID");

    // Get the variable names from the identifiers.
    QStringList uVarIdentifier = rh.value("U_VAR").split('/');
    uVarName = uVarIdentifier[2];
    QStringList vVarIdentifier = rh.value("V_VAR").split('/');
    vVarName = vVarIdentifier[2];
    QStringList wVarIdentifier = rh.value("W_VAR").split('/');
    wVarName = wVarIdentifier[2];

    // Change request to obtain base request for NWP data access.
    rh.remove("END_TIME_FORWARD");
    rh.remove("END_TIME_BACKWARD");
    rh.remove("TIME_SPAN");
    rh.remove("LINE_TYPE");
    rh.remove("INTEGRATION_METHOD");
    rh.remove("INTERPOLATION_METHOD");
    rh.remove("SUBTIMESTEPS_PER_DATATIMESTEP");
    rh.remove("VERTEX_AT_SUBTIMESTEPS");
    rh.remove("STREAMLINE_DELTA_S");
    rh.remove("STREAMLINE_LENGTH");

    // Determine available valid times.
    MTrajectoryInitTimeMap availableTrajectories =
        dispatcher->getAvailableTrajectoriesToCompute();
    validTimes = availableTrajectories.value(initTime).keys();

    startTimeStep = validTimes.indexOf(validTime);
    endTimeStepBackward = validTimes.indexOf(endTimeBackward);
    endTimeStepForward = validTimes.indexOf(endTimeForward);
    baseRequestHelper = rh;
    baseRequest = rh.request();
}


void MTrajectoryComputation::compute()
{
    switch (lineType)
    {
    case PATH_LINE:
        computePathLines();
        break;
    case STREAM_LINE:
        computeStreamLines();
        break;
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MTrajectoryComputation::computeStreamLines()
{
    int numTrajectories = result->seedPoints.size();
    QList<MNWPActorVariable *> uvwVariables = {
        dispatcher->getInputVariable(uVarName),
        dispatcher->getInputVariable(vVarName),
        dispatcher->getInputVariable(wVarName)};

    // Array to store grids with wind data sampled along trajectories. These are
    // passed to the integration methods.
    // trajectoryIntegrationEuler() and trajectoryIntegrationRungeKutta() both
    // require two timesteps to be passed. As for streamlines we only consider
    // a single timestep, the current implementation simply stores pointers to
    // the same data for both time steps (see below).
    QVector<QVector<MStructuredGrid *>> grids(
        2, QVector<MStructuredGrid *>(3, nullptr));

    MDataRequestHelper rh(baseRequest);

    // Initialize computation information.
    result->numStoredVerticesPerTrajectory = streamlineLength;
    result->times.reserve(result->numStoredVerticesPerTrajectory);
    result->vertices = QVector<QVector<QVector3D>>(numTrajectories);

    for (int t = 0; t < numTrajectories; t++)
    {
        result->vertices[t].reserve(result->numStoredVerticesPerTrajectory);
    }

    // Add start time step and seed points.
    QVector<bool> validPosition(numTrajectories);
    result->times.push_back(validTimes.at(startTimeStep));
    for (int trajectory = 0; trajectory < numTrajectories; trajectory++)
    {
        result->vertices[trajectory].push_back(
            result->seedPoints.at(trajectory));
        validPosition[trajectory] = true;
    }

    // Load wind data to be sampled along streamlines (see comment above; two
    // timesteps are required for the integration methods, here both grids are
    // the same as only a single time step is required for streamline
    // computation).
    QDateTime timeStep = validTimes.at(startTimeStep);

    for (int v = 0; v < uvwVariables.size(); ++v)
    {
        MNWPActorVariable *windVar = uvwVariables[v];
        MDataRequestHelper varReqHelper =
            MDataRequestHelper(windVar->buildDataRequest());
        if (integrateInitAndValid)
        {
            varReqHelper.insert("INIT_TIME", timeStep);
        }
        else
        {
            varReqHelper.insert("INIT_TIME",
                                baseRequestHelper.value("INIT_TIME"));
        }
        varReqHelper.insert("VALID_TIME", timeStep);
        varReqHelper.insert("VARIABLE", windVar->variableName);
        varReqHelper.insert("LEVELTYPE", windVar->levelType);

        // get data at current time-step
        grids[0][v] = windVar->dataSource->getData(varReqHelper.request()); // timestep 0
        grids[1][v] = grids[0][v]; // timestep 1 (link)

        if (grids[0][v] == nullptr)
        {
            QString errorMsg =
                QString("ERROR: Not all wind components required for the "
                        "streamline computation could be loaded. Please check "
                        "the console output and your datasets. "
                        "Aborting streamline computation.");
            LOG4CPLUS_ERROR(mlog, errorMsg);
            return;
        }
    }

    // cInfo.times contains the list of timesteps that correspond to the
    // trajectory vertices. For streamlines, there is only one timestep. How
    // should this be handled? Currently, all times are set to the same value.
    //TODO (mr, 13Jul2018) -- how should this be handled?
    for (int i = 1; i <= result->numStoredVerticesPerTrajectory; ++i)
    {
        result->times.push_back(validTimes.at(startTimeStep));
    }

    // Compute the streamlines.
#pragma omp parallel for
    for (int iStreamline = 0; iStreamline < numTrajectories; iStreamline++)
    {
        // Integrate streamline in wind field.
        for (int iVertex = 1; iVertex <= result->numStoredVerticesPerTrajectory; iVertex++)
        {
            // Get current position from vertex buffer.
            QVector3D currentPosition = result->vertices[iStreamline].back();

            // Compute next vertex with Euler or Runge-Kutta integration.
            QVector3D nextPos;
            switch (integrationMethod)
            {
            case EULER:
                nextPos = trajectoryIntegrationEuler(
                    currentPosition, streamlineDeltaS, 0, 0,
                    interpolationMethod, grids, validPosition[iStreamline]);
                break;
            case RUNGE_KUTTA:
                nextPos = trajectoryIntegrationRungeKutta(
                    currentPosition, streamlineDeltaS, 0, 0,
                    interpolationMethod, grids, validPosition[iStreamline]);
                break;
            }

            // Check validity of computed vertex position.
            if (! validPosition[iStreamline])
            {
                nextPos = QVector3D(M_INVALID_TRAJECTORY_POS,
                                    M_INVALID_TRAJECTORY_POS,
                                    M_INVALID_TRAJECTORY_POS);
            }

            // Store position in vertex buffer.
            result->vertices[iStreamline].push_back(nextPos);
        }
    }

    // Release wind and auxiliary data grids.
    for (int v = 0; v < uvwVariables.size(); ++v)
    {
        if (grids[0][v])
        {
            uvwVariables[v]->dataSource->releaseData(grids[0][v]);
        }
    }
}


QMap<int, QVector<MStructuredGrid *>> MTrajectoryComputation::getRequiredGridsForPathLines(bool& allDataFieldsValid)
{
    QMap<int, QVector<MStructuredGrid *>> grids;

    QList<MNWPActorVariable *> uvwVariables = dispatcher->getInputWindVariables();

    for (int step = endTimeStepBackward; step <= endTimeStepForward; step++)
    {
        QDateTime validTime = validTimes.at(step);

        for (int v = 0; v < uvwVariables.size(); ++v)
        {
            MDataRequestHelper varReqHelper =
                    MDataRequestHelper(uvwVariables[v]->buildDataRequest());
            if (integrateInitAndValid)
            {
                varReqHelper.insert("INIT_TIME", validTime);
            } else
            {
                varReqHelper.insert("INIT_TIME",
                                    baseRequestHelper.value("INIT_TIME"));
            }
            varReqHelper.insert("VALID_TIME", validTime);
            varReqHelper.insert("VARIABLE", uvwVariables[v]->variableName);
            varReqHelper.insert("LEVELTYPE", uvwVariables[v]->levelType);

            MStructuredGrid* currentGrid = uvwVariables[v]->dataSource->getData(varReqHelper.request());
            if (! currentGrid)
            {
                allDataFieldsValid = false;
                return grids;
            }
            grids[step].push_back(currentGrid);
        }
    }
    allDataFieldsValid = true;
    return grids;
}


void MTrajectoryComputation::computePathLines()
{
    int numTrajectories = result->seedPoints.size();

    bool allGridsValid = false;
    // Get the required grids. If not all grids are present, abort.
    QMap<int, QVector<MStructuredGrid *>> requiredGrids = getRequiredGridsForPathLines(allGridsValid);
    if (! allGridsValid)
    {
        QString errorMsg =
                QString("ERROR: Not all wind components required for the "
                        "streamline computation could be loaded. Please check "
                        "the console output and your datasets. "
                        "Aborting streamline computation.");
        LOG4CPLUS_ERROR(mlog, errorMsg);
        return;
    }

    // QVector<QDateTime> timeSteps(2);
    MDataRequestHelper rh(baseRequest);

    // Initialize computation information.
    if (vertexAtSubTimeSteps)
    {
        result->numStoredVerticesPerTrajectory =
            abs(endTimeStepForward - endTimeStepBackward) * subTimeStepsPerDataTimeStep + 1;
    }
    else
    {
        result->numStoredVerticesPerTrajectory =
            abs(endTimeStepForward - endTimeStepBackward) + 1;
    }
    result->times.reserve(result->numStoredVerticesPerTrajectory);
    result->vertices = QVector<QVector<QVector3D>>(numTrajectories);

    for (int t = 0; t < numTrajectories; ++t)
    {
        result->vertices[t].reserve(result->numStoredVerticesPerTrajectory);
    }

    // Create temporary vector to cache trajectory vertex positions.
    QVector<QVector3D> positions(numTrajectories);
    QVector<bool> validPosition(numTrajectories);

    // Compute path lines for all time steps involved.
    // Make a backward pass first, then a forward pass.
    for (int pass = 0; pass < 2; pass++)
    {
        bool forward = pass;
        int endTimeStep = forward ? endTimeStepForward : endTimeStepBackward;

        if (pass == 0)
        {
            // Add start time step and seed points only on first pass.
            result->times.push_back(validTimes.at(startTimeStep));
            for (int trajectory = 0; trajectory < numTrajectories; ++trajectory)
            {
                positions[trajectory] = result->seedPoints[trajectory];
                validPosition[trajectory] = true;

                result->vertices[trajectory].push_back(positions[trajectory]);
            }
        }
        else
        {
            // Do not add seed points again, just reset the last positions to the seed points.
            for (int trajectory = 0; trajectory < numTrajectories; ++trajectory)
            {
                // Reset tracking cache.
                positions[trajectory] = result->seedPoints[trajectory];
                validPosition[trajectory] = true;
            }
        }

        for (int step = startTimeStep; step != endTimeStep;
             forward ? step++ : step--)
        {
            // Set previous and next data times and time between data time
            // steps.
            int nextStep = step + (forward ? 1 : -1);
            QDateTime t1 = validTimes.at(step);
            QDateTime t2 = validTimes.at(nextStep);
            const qint64 timeStepSeconds = t1.secsTo(t2)
                                           / subTimeStepsPerDataTimeStep;

            if (vertexAtSubTimeSteps)
            {
                // Need to store sub-time-steps before "current" timestep.
                // Calculated as last time-step (index[0]) plus the extra time
                // from the sub-time-step.
                // Do not add the last sub-time-step as that is the "current"
                // time-step, which is added below.
                for (int iSubTimeStep = 1; iSubTimeStep < subTimeStepsPerDataTimeStep; iSubTimeStep++)
                {
                    result->times.push_back(t1.addSecs(timeStepSeconds * iSubTimeStep));
                }
            }

            // The "current" timestep for which the new vertex position needs to
            // be computed is the "next" timestep (index [1]).
            result->times.push_back(t2);

            QVector<QVector<MStructuredGrid *>> grids(2, QVector<MStructuredGrid *>(3));
            grids[0][0] = requiredGrids[step][0];
            grids[0][1] = requiredGrids[step][1];
            grids[0][2] = requiredGrids[step][2];
            grids[1][0] = requiredGrids[nextStep][0];
            grids[1][1] = requiredGrids[nextStep][1];
            grids[1][2] = requiredGrids[nextStep][2];

// Compute the trajectories.
#pragma omp parallel for
            for (int iTrajectory = 0; iTrajectory < numTrajectories; ++iTrajectory)
            {
                // Iterate over "sub-timesteps" (i.e. internally time-interpolated
                // interpolation nodes; cf. trajectory integration implemented in
                // LAGRANTO and described in Sprenger and Wernli (GMD, 2015)).
                for (int iSubTimeStep = 1;
                     iSubTimeStep <= subTimeStepsPerDataTimeStep; ++iSubTimeStep)
                {
                    // Compute time interpolation weights.
                    const float timeIntWeight0 = static_cast<float>(iSubTimeStep - 1)
                                                 / subTimeStepsPerDataTimeStep;
                    const float timeIntWeight1 = static_cast<float>(iSubTimeStep)
                                                 / subTimeStepsPerDataTimeStep;

                    // Compute next vertex with Euler or Runge-Kutta integration.
                    switch (integrationMethod)
                    {
                        case EULER:
                            positions[iTrajectory] = trajectoryIntegrationEuler(
                                    positions[iTrajectory], timeStepSeconds, timeIntWeight0,
                                    timeIntWeight1, interpolationMethod, grids,
                                    validPosition[iTrajectory]);
                            break;
                        case RUNGE_KUTTA:
                            positions[iTrajectory] = trajectoryIntegrationRungeKutta(
                                    positions[iTrajectory], timeStepSeconds, timeIntWeight0,
                                    timeIntWeight1, interpolationMethod, grids,
                                    validPosition[iTrajectory]);
                            break;
                    }

                    // Check validity of computed vertex position.
                    if (!validPosition[iTrajectory])
                    {
                        positions[iTrajectory] = QVector3D(
                                M_INVALID_TRAJECTORY_POS, M_INVALID_TRAJECTORY_POS,
                                M_INVALID_TRAJECTORY_POS);
                    }

                    // do not emit position as vertex
                    if (iSubTimeStep != subTimeStepsPerDataTimeStep
                        && !vertexAtSubTimeSteps)
                    {
                        continue;
                    }

                    // Save computed vertex position for the "current" time step.
                    result->vertices[iTrajectory].push_back(positions[iTrajectory]);
                }
            }
        }

        // End of backward pass. Reverse the times and the vertices already pushed.
        // This leads the times and vertices being sorted by time at the end.
        if (! forward)
        {
            std::reverse(result->times.begin(), result->times.end());
            for (auto& verticesPerTrajectory : result->vertices)
            {
                std::reverse(verticesPerTrajectory.begin(), verticesPerTrajectory.end());
            }
        }
    }

    // Delete invalid trajectories, thus, trajectories with already an
    // invalid position at the first time step.
    for (int tId = numTrajectories - 1; tId >= 0; tId--)
    {
        auto& firstVertex = result->vertices[tId][0];
        if (firstVertex.z() == M_INVALID_TRAJECTORY_POS)
        {
            result->vertices.removeAt(tId);
            result->seedPoints.removeAt(tId);
        }
    }

    // Release all remaining not-yet released data fields.
    QList<MNWPActorVariable *> uvwVariables = {
            dispatcher->getInputVariable(uVarName),
            dispatcher->getInputVariable(vVarName),
            dispatcher->getInputVariable(wVarName)};
    for (auto& gridsAtTime : requiredGrids.values())
    {
        for (int v = 0; v < uvwVariables.size(); ++v)
        {
            uvwVariables[v]->dataSource->releaseData(gridsAtTime[v]);
        }
    }
}


QVector3D MTrajectoryComputation::trajectoryIntegrationEuler(
    QVector3D pos, float deltaT, float timePos0, float timePos1,
    MTrajectoryComputationInterpolationMethod method,
    QVector<QVector<MStructuredGrid *>> &grids, bool &valid)
{
    QVector3D p0 = pos;
    QVector3D v0 =
        sampleVelocity3DSpaceTime(p0, timePos0, method, grids, valid);
    QVector3D p1 = p0;

    for (int i = 0; i < EULER_ITERATION; i++)
    {
        QVector3D v1 =
            sampleVelocity3DSpaceTime(p1, timePos1, method, grids, valid);
        QVector3D v = (v0 + v1) / 2.0f;
        p1 = p0 + (convertWindVelocityFromMetricToSpherical(v, pos) * deltaT);
    }

    return p1;
}


QVector3D MTrajectoryComputation::trajectoryIntegrationRungeKutta(
    QVector3D pos, float deltaT, float timePos0, float timePos1,
    MTrajectoryComputationInterpolationMethod method,
    QVector<QVector<MStructuredGrid *>> &grids, bool &valid)
{
    QVector3D s;
    QVector<QVector3D> k(4);
    float timeInterpolationWeight;

    for (int i = 0; i < 4; i++)
    {
        if (i == 0)
        {
            s = QVector3D(0.0f, 0.0f, 0.0f);
            timeInterpolationWeight = timePos0;
        }
        else if (i == 3)
        {
            s = k[i - 1];
            timeInterpolationWeight = timePos1;
        }
        else
        {
            s = k[i - 1] / 2.0f;
            timeInterpolationWeight = (timePos0 + timePos1) / 2.0f;
        }

        QVector3D v = sampleVelocity3DSpaceTime(
            pos + s, timeInterpolationWeight, method, grids, valid);
        k[i] = convertWindVelocityFromMetricToSpherical(v, pos);
    }

    return pos + ((k[0] + 2.0f * k[1] + 2.0f * k[2] + k[3]) * deltaT / 6.0f);
}


QVector3D MTrajectoryComputation::sampleVelocity3DSpaceTime(
    QVector3D pos, float timeInterpolationValue,
    MTrajectoryComputationInterpolationMethod method,
    QVector<QVector<MStructuredGrid *>> &grids, bool &valid)
{
    // Initialise velocity.
    QVector3D velocity(0, 0, 0);

    // Sample velocity with given interpolation method.
    // NOTE: See Philipp Kaiser's master's thesis (TUM 2017) for details on the
    // different interpolation approaches. The LAGRANTO_INTERPOLATION method
    // follows the implementation of LAGRANTO v2
    // (http://dx.doi.org/10.5194/gmd-8-2569-2015).
    switch (method)
    {
    case LAGRANTO_INTERPOLATION:
    {
        //TODO (mr, 11Jul2018) -- I assume this is still the UNCORRECTED version
        //  of LAGRANTO. Has this been corrected in a new LAGRANTO version? Check
        //  consistency!!
        QVector3D uIndex = floatIndexAtPosInterpolatedInTime(
            pos, grids[0][0], grids[1][0], timeInterpolationValue);
        QVector3D vIndex = floatIndexAtPosInterpolatedInTime(
            pos, grids[0][1], grids[1][1], timeInterpolationValue);
        QVector3D pIndex = floatIndexAtPosInterpolatedInTime(
            pos, grids[0][2], grids[1][2], timeInterpolationValue);

        // Check if index is valid.
        if (uIndex.x() < 0 || uIndex.y() < 0 || uIndex.z() < 0 || vIndex.x() < 0
            || vIndex.y() < 0 || vIndex.z() < 0 || pIndex.x() < 0
            || pIndex.y() < 0 || pIndex.z() < 0)
        {
            valid = false;
            break;
        }

        float u = sampleDataValueAtFloatIndexAndInterpolateInTime(
            uIndex, grids[0][0], grids[1][0], timeInterpolationValue);
        float v = sampleDataValueAtFloatIndexAndInterpolateInTime(
            vIndex, grids[0][1], grids[1][1], timeInterpolationValue);
        float p = sampleDataValueAtFloatIndexAndInterpolateInTime(
            pIndex, grids[0][2], grids[1][2], timeInterpolationValue);

        velocity = QVector3D(u, v, p);
        break;
    }

    case MET3D_INTERPOLATION:
    {
        // Sample velocity.
        float uT0 = grids[0][0]->interpolateValue(pos.x(), pos.y(), pos.z());
        float vT0 = grids[0][1]->interpolateValue(pos.x(), pos.y(), pos.z());
        float pT0 = grids[0][2]->interpolateValue(pos.x(), pos.y(), pos.z());
        float uT1 = grids[1][0]->interpolateValue(pos.x(), pos.y(), pos.z());
        float vT1 = grids[1][1]->interpolateValue(pos.x(), pos.y(), pos.z());
        float pT1 = grids[1][2]->interpolateValue(pos.x(), pos.y(), pos.z());

        // Check velocity for missing values; interpolate if ok.
        if (IS_MISSING(uT0) || IS_MISSING(vT0) || IS_MISSING(pT0)
            || IS_MISSING(uT1) || IS_MISSING(vT1) || IS_MISSING(pT1))
        {
            valid = false;
            break;
        }

        velocity = QVector3D(MMIX(uT0, uT1, timeInterpolationValue),
                             MMIX(vT0, vT1, timeInterpolationValue),
                             MMIX(pT0, pT1, timeInterpolationValue));
        break;
    }
    }

    return velocity;
}


QVector3D MTrajectoryComputation::convertWindVelocityFromMetricToSpherical(
    QVector3D velocity_ms_ms_Pas, QVector3D position_lon_lat_p)
{
    // Convert velocity from m/s to (lat, lon, p)/s .
    QVector3D vel;
    vel.setX(static_cast<float>(velocity_ms_ms_Pas.x()
        / (LAT_TO_METER * cos(position_lon_lat_p.y() * M_PI / 180.0))));
    vel.setY(static_cast<float>(velocity_ms_ms_Pas.y() / LAT_TO_METER));
    vel.setZ(static_cast<float>(velocity_ms_ms_Pas.z() / 100.0));
    return vel;
}


float MTrajectoryComputation::samplePressureAtFloatIndex(QVector3D index,
                                                         MStructuredGrid *grid)
{
    std::array<int, 3> p0 = {static_cast<int>(floor(index.x())),
                             static_cast<int>(floor(index.y())),
                             static_cast<int>(floor(index.z()))};
    std::array<int, 3> p1 = {static_cast<int>(ceil(index.x())),
                             static_cast<int>(ceil(index.y())),
                             static_cast<int>(ceil(index.z()))};

    float fraci = index.x() - static_cast<float>(p0[0]);
    float fracj = index.y() - static_cast<float>(p0[1]);
    float frack = index.z() - static_cast<float>(p0[2]);

    float val000 = grid->getPressure(p0[2], p0[1], p0[0]);
    float val001 = grid->getPressure(p0[2], p0[1], p1[0]);
    float val010 = grid->getPressure(p0[2], p1[1], p0[0]);
    float val011 = grid->getPressure(p0[2], p1[1], p1[0]);
    float val100 = grid->getPressure(p1[2], p0[1], p0[0]);
    float val101 = grid->getPressure(p1[2], p0[1], p1[0]);
    float val110 = grid->getPressure(p1[2], p1[1], p0[0]);
    float val111 = grid->getPressure(p1[2], p1[1], p1[0]);

    return MMIX(
        MMIX(MMIX(val000, val001, fraci), MMIX(val010, val011, fraci), fracj),
        MMIX(MMIX(val100, val101, fraci), MMIX(val110, val111, fraci), fracj),
        frack);
}


float MTrajectoryComputation::sampleDataValueAtFloatIndex(QVector3D index,
                                                          MStructuredGrid *grid)
{
    std::array<int, 3> p0 = {static_cast<int>(floor(index.x())),
                             static_cast<int>(floor(index.y())),
                             static_cast<int>(floor(index.z()))};
    std::array<int, 3> p1 = {static_cast<int>(ceil(index.x())),
                             static_cast<int>(ceil(index.y())),
                             static_cast<int>(ceil(index.z()))};

    // Wrap around the longitude index if grid is cyclic.
    if (p1[0] == static_cast<int>(grid->getNumLons())
        && grid->gridIsCyclicInLongitude())
    {
        p1[0] = 0;
    }

    float fraci = index.x() - static_cast<float>(p0[0]);
    float fracj = index.y() - static_cast<float>(p0[1]);
    float frack = index.z() - static_cast<float>(p0[2]);

    float val000 = grid->getValue(p0[2], p0[1], p0[0]);
    float val001 = grid->getValue(p0[2], p0[1], p1[0]);
    float val010 = grid->getValue(p0[2], p1[1], p0[0]);
    float val011 = grid->getValue(p0[2], p1[1], p1[0]);
    float val100 = grid->getValue(p1[2], p0[1], p0[0]);
    float val101 = grid->getValue(p1[2], p0[1], p1[0]);
    float val110 = grid->getValue(p1[2], p1[1], p0[0]);
    float val111 = grid->getValue(p1[2], p1[1], p1[0]);

    return MMIX(
        MMIX(MMIX(val000, val001, fraci), MMIX(val010, val011, fraci), fracj),
        MMIX(MMIX(val100, val101, fraci), MMIX(val110, val111, fraci), fracj),
        frack);
}


QVector3D MTrajectoryComputation::floatIndexAtPos(QVector3D pos,
                                                  MStructuredGrid *grid)
{
    float i = MMOD(pos.x() - grid->getLons()[0], 360.)
              / abs(grid->getLons()[1] - grid->getLons()[0]);
    float j = static_cast<float>((grid->getLats()[0] - pos.y())
              / abs(grid->getLats()[1] - grid->getLats()[0]));

    // Allow an upper bound of the float index past the last one to support
    // wrapping grids.
    unsigned int allowedUpperLonBound = grid->gridIsCyclicInLongitude()
                                   ? grid->getNumLons()
                                   : (grid->getNumLons() - 1);

    if (i < 0 || i > allowedUpperLonBound || j < 0 || j > (grid->getNumLats() - 1))
    {
        return {-1, -1, -1};
    }
    int kl = 0;
    int ku = static_cast<int>(grid->getNumLevels() - 1);

    float pp0 = samplePressureAtFloatIndex(
        QVector3D(i, j, static_cast<float>(kl)), grid);
    float pp1 = samplePressureAtFloatIndex(
        QVector3D(i, j, static_cast<float>(ku)), grid);

    // Perform the binary search.
    while ((ku - kl) > 1)
    {
        // Element midway between klower and kupper.
        int kmid = (ku + kl) / 2;
        float ppm = samplePressureAtFloatIndex(
            QVector3D(i, j, static_cast<float>(kmid)), grid);

        // Cut interval in half.
        if (ppm >= pos.z())
        {
            ku = kmid;
            pp1 = ppm;
        }
        else
        {
            kl = kmid;
            pp0 = ppm;
        }
    }

    float k = static_cast<float>(kl) + (pp0 - pos.z()) / (pp0 - pp1);

    if (k < 0 || k > (grid->getNumLevels() - 1))
    {
        return {i, j, -1};
    }

    return {i, j, k};
}


QVector3D MTrajectoryComputation::floatIndexAtPosInterpolatedInTime(
    QVector3D pos, MStructuredGrid *grid0, MStructuredGrid *grid1,
    float timeInterpolationValue)
{
    // Get indices for both grids.
    QVector3D index0 = floatIndexAtPos(pos, grid0);
    QVector3D index1 = floatIndexAtPos(pos, grid1);

    // Check if any of the indices has a missing value (-1) and return
    // interpolated index.
    QVector3D mixIndex;
    mixIndex.setX((index0.x() < 0 || index1.x() < 0) ? -1
                      : MMIX(index0.x(), index1.x(), timeInterpolationValue));
    mixIndex.setY((index0.y() < 0 || index1.y() < 0) ? -1
                      : MMIX(index0.y(), index1.y(), timeInterpolationValue));
    mixIndex.setZ((index0.z() < 0 || index1.z() < 0) ? -1
                      : MMIX(index0.z(), index1.z(), timeInterpolationValue));
    return mixIndex;
}


float MTrajectoryComputation::sampleDataValueAtFloatIndexAndInterpolateInTime(
    QVector3D index, MStructuredGrid *grid0, MStructuredGrid *grid1,
    float timeInterpolationValue)
{
    float v0 = sampleDataValueAtFloatIndex(index, grid0);
    float v1 = sampleDataValueAtFloatIndex(index, grid1);
    return MMIX(v0, v1, timeInterpolationValue);
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // namespace Met3D