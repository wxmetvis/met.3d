/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2017-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectoryrequestdispatcher.h"

// standard library imports
#include <utility>

// related third party imports

// local application imports
#include "gxfw/nwpactorvariable.h"
#include "pointfilter/pointfilterfactory.h"
#include "source/trajectorycomputationsource.h"
#include "filter/trajectoryfilterfactory.h"
#include "trajectorypipelinemanager.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryRequestDispatcher::MTrajectoryRequestDispatcher()
        : windEastwardVariable(nullptr),
          windNorthwardVariable(nullptr),
          windVerticalVariable(nullptr),
          auxRenderingVariable(nullptr),
          precomputedDataSource(nullptr),
          integrateInitAndValidTime(false),
          mode(MTrajectoryMode::COMPUTED_IN_MET3D)
{
    connect(this, &MTrajectoryRequestDispatcher::dataRequestCompleted,
            this, &MTrajectoryRequestDispatcher::trajectoriesAvailable);
}


MTrajectoryRequestDispatcher::~MTrajectoryRequestDispatcher()
{
    // Force to delete these items first before the ones in the pipelines, since
    // the pipeline data items are still referenced by this item here.
    memoryManager->deleteItemsFromDataSource(this);

    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        deregisterInputSource(pipeline);
        disconnect(pipeline, &MTrajectoryPipelineManager::trajectoryPipelineChanged,
                this, &MTrajectoryRequestDispatcher::pipelineChangedSignal);
        delete pipeline;
    }
    disconnect(this, &MTrajectoryRequestDispatcher::dataRequestCompleted,
            this, &MTrajectoryRequestDispatcher::trajectoriesAvailable);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MTrajectoryDataCollectionGroup *
MTrajectoryRequestDispatcher::getData(MDataRequest request)
{
    return dynamic_cast<MTrajectoryDataCollectionGroup *>(
        MScheduledDataSource::getData(request));
}


MTrajectoryDataCollectionGroup *
MTrajectoryRequestDispatcher::produceData(MDataRequest request)
{
    auto dataCollections = new MTrajectoryDataCollectionGroup();

    // Iterate over the pipelines and get the data object from each.
    for (MTrajectoryPipelineManager* trajPipeline : pipelines)
    {
        MDataRequestHelper rh(request);
        rh.removeAllKeysExcept(trajPipeline->requiredKeys());
        MTrajectoryDataCollection *pipelineData =
            trajPipeline->getData(rh.request());

        dataCollections->append(pipelineData);
        // Do not release the data here: Keep a reference until this created
        // data item gets destroyed. Do not destroy the references before this
        // item.
    }
    return dataCollections;
}


const QStringList MTrajectoryRequestDispatcher::locallyRequiredKeys()
{
    return {};
}


MTask *MTrajectoryRequestDispatcher::createTaskGraph(MDataRequest request)
{
    auto* task = new MTask(request, this);
    task->setProcessingLabel("Computing trajectories...");

    for (MTrajectoryPipelineManager* trajPipeline : pipelines)
    {
        MTask* trajPipelineTask = trajPipeline->getTaskGraph(request);
        task->addParent(trajPipelineTask);
    }
    return task;
}


bool MTrajectoryRequestDispatcher::isInitialized() const
{
    if (memoryManager == nullptr)
    {
        return false;
    }
    if (mode == MTrajectoryMode::COMPUTED_IN_MET3D
        && (windEastwardVariable == nullptr || windNorthwardVariable == nullptr
            || windVerticalVariable == nullptr))
    {
        return false;
    }
    if (mode == MTrajectoryMode::PRECOMPUTED && precomputedDataSource == nullptr)
    {
        return false;
    }
    return true;
}


void MTrajectoryRequestDispatcher::dispatch(MTrajectoryDispatchInfo& dispatchInfo)
{
    if (mode == MTrajectoryMode::COMPUTED_IN_MET3D && pipelines.isEmpty())
    {
        LOG4CPLUS_WARN(mlog,
                       "Please add at least one seed point source to generate "
                       "starting points for the trajectory computation.");
        return;
    }
    // Check whether there are any problems with the request and the current
    // state of the pipeline, which can be logged as a warning. Just check one
    // pipeline for issues since they are always in the same state.
    if (pipelines[0]->checkForPipelineIssues(dispatchInfo))
    {
        // Do not request if we encounter a critical issue.
        return;
    }

    MDataRequest request = buildRequestFromDispatchInfo(dispatchInfo);

    LOG4CPLUS_DEBUG(
        mlog, "Dispatching trajectory request. mode:" << int(mode) <<
              " Forecast IT="
                  << dispatchInfo.initTime.toString(Qt::ISODate).toStdString()
                  << ", trajectory start="
                  << dispatchInfo.validTime.toString(Qt::ISODate).toStdString()
                  << ", trajectory end forward="
                  << dispatchInfo.endTimeForward.toString(Qt::ISODate).toStdString()
                  << ", trajectory end backward="
                  << dispatchInfo.endTimeBackward.toString(Qt::ISODate).toStdString()
                  << " ensemble member=" << dispatchInfo.member);

    // Request the data.
    this->requestData(request);
}


MDataRequest MTrajectoryRequestDispatcher::buildRequestFromDispatchInfo(
    MTrajectoryDispatchInfo &dispatchInfo)
{
    MDataRequestHelper baseRh;
    // Add required keys for variables to request.
    baseRh.insert("INIT_TIME", dispatchInfo.initTime);
    baseRh.insert("VALID_TIME", dispatchInfo.validTime);
    baseRh.insert("MEMBER", dispatchInfo.member);
    baseRh.insert("TIME_SPAN", "ALL");

    // The end time for precomputed trajectories need to be accessed from
    // the reader, the UI setting should not have an effect here. The end time
    // is needed if we want to sample auxiliary data from other sources.
    if (mode == MTrajectoryMode::PRECOMPUTED)
    {
        precomputedDataSource->trajectoryEndTime(dispatchInfo.initTime,
                                                 dispatchInfo.validTime,
                                                 dispatchInfo.endTimeForward);
    }
    baseRh.insert("END_TIME_FORWARD", dispatchInfo.endTimeForward);
    baseRh.insert("END_TIME_BACKWARD", dispatchInfo.endTimeBackward);

    if (mode == MTrajectoryMode::COMPUTED_IN_MET3D)
    {
        // Insert computation information into data request.
        baseRh.insert("LINE_TYPE", dispatchInfo.lineType);
        baseRh.insert("SUBTIMESTEPS_PER_DATATIMESTEP", dispatchInfo.numSubTimeSteps);
        baseRh.insert("VERTEX_AT_SUBTIMESTEPS", dispatchInfo.vertexAtSubTimeSteps);
        baseRh.insert("STREAMLINE_DELTA_S", static_cast<int>(dispatchInfo.streamlineDeltaS));
        baseRh.insert("STREAMLINE_LENGTH", dispatchInfo.streamlineLength);
        baseRh.insert("INTEGRATION_METHOD", dispatchInfo.integrationMethod);
        baseRh.insert("INTERPOLATION_METHOD", dispatchInfo.interpolationMethod);
        baseRh.insert("INTEGRATE_INIT_AND_VALID", integrateInitAndValidTime);

        // Put as value for wind ID the data source id, leveltype, varName,
        // and the current state of the attached properties. This is required
        // so the request key changes when for example the smooth settings
        // change.
        auto vars = QList<MNWPActorVariable *>()
                    << windEastwardVariable << windNorthwardVariable
                    << windVerticalVariable;
        QStringList varKeys = QStringList() << "U_VAR" << "V_VAR" << "W_VAR";
        for (int i = 0; i < 3; i++)
        {
            MDataRequestHelper varRh(vars[i]->buildDataRequest());
            varRh.remove("LEVELTYPE");
            varRh.remove("VARIABLE");
            varRh.remove("INIT_TIME");
            varRh.remove("VALID_TIME");
            varRh.remove("MEMBER");

            // Get the identifier, and the values from the data request of the
            // actor variable, and add them together to get a clear hashed
            // identifier of the current state of the grid.
            QString varIdentifier = vars[i]->getIdentifier();

            QString keyHash = QString::number(qHash(varRh.keys()));
            QString valueHash = QString::number(qHash(varRh.map().values()));
            if (! varRh.keys().isEmpty())
            {
                varIdentifier += "/" + keyHash + valueHash;
            }

            baseRh.insert(varKeys[i], varIdentifier);
        }
    }
    else
    {
        // Disable IT=VT integration since we take the IT and VT information
        // from the precomputed data source.
        baseRh.insert("INTEGRATE_INIT_AND_VALID", 0);
    }

    QMap<QString, QVector2D> normalInfo = dispatchInfo.pressureToWorldZNormalsPerView;
    QString normalString;
    // Insert viewName/x/y per scene view into request for normal computation.
    for (auto it = normalInfo.begin(); it != normalInfo.end(); ++it) {
        normalString += QString("%1/%2/%3").arg(it.key())
                            .arg(it.value().x()).arg(it.value().y()) + "/";
    }
    normalString.chop(1);

    baseRh.insert("NORMALS_LOGP_SCALED", normalString);

    // Add the filter timestep if we are rendering points along trajectories.
    if ((dispatchInfo.renderMode == MTrajectoryRenderType::SINGLETIME_POSITIONS)
        || (dispatchInfo.renderMode == MTrajectoryRenderType::TUBES_AND_SINGLETIME)
        || (dispatchInfo.renderMode == MTrajectoryRenderType::BACKWARDTUBES_AND_SINGLETIME))
    {
        baseRh.insert("FILTER_TIMESTEP",
                      QString("%1").arg(dispatchInfo.particlePosTimeStep));
    }
    else
    {
        baseRh.insert("FILTER_TIMESTEP", "ALL");
    }

    // Add pipeline state to request.
    for (MTrajectoryPipelineManager* trajPipeline : pipelines)
    {
        trajPipeline->insertPipelineStateIntoRequest(baseRh);
        trajPipeline->insertAuxiliaryIntoRequest(baseRh, auxRenderingVariable);
    }
    return baseRh.request();
}


void MTrajectoryRequestDispatcher::updateSourceMode(MTrajectoryMode tMode)
{
    this->mode = tMode;
    updateAvailableTrajectories();
}


void MTrajectoryRequestDispatcher::setPrecomputedDataSource(
    const QString& identifier)
{
    MAbstractDataSource *dataSource =
        MSystemManagerAndControl::getInstance()->getDataSource(identifier
                                                               + " Reader");
    auto trajectorySource = dynamic_cast<MTrajectoryReader*>(dataSource);
    // Check whether the selected source is actually a trajectory source.
    if (! trajectorySource)
    {
        LOG4CPLUS_ERROR(mlog,
                        "Selected precomputed data source "
                            << identifier
                            << " is not a trajectory reader. Setting the "
                               "data source will be skipped.");
        return;
    }

    setPrecomputedDataSource(trajectorySource);
}


void MTrajectoryRequestDispatcher::setPrecomputedDataSource(
    MTrajectoryReader *source)
{
    precomputedDataSource = source;

    deletePipelines();

    auto precomputedPipeline = new MTrajectoryPipelineManager(
        memoryManager, MTrajectoryMode::PRECOMPUTED);
    precomputedPipeline->switchToPrecomputedSource(source);

    connect(precomputedPipeline, SIGNAL(trajectoryPipelineChanged()),
            this, SIGNAL(pipelineChangedSignal()));

    pipelines.append(precomputedPipeline);
}


void MTrajectoryRequestDispatcher::setWindVariableU(MNWPActorVariable *var)
{
    this->windEastwardVariable = var;
    updateAvailableTrajectories();
}


void MTrajectoryRequestDispatcher::setWindVariableV(MNWPActorVariable *var)
{
    this->windNorthwardVariable = var;
    updateAvailableTrajectories();
}


void MTrajectoryRequestDispatcher::setWindVariableW(MNWPActorVariable *var)
{
    this->windVerticalVariable = var;
    updateAvailableTrajectories();
}


void MTrajectoryRequestDispatcher::setRenderingAuxiliaryVariable(
    MNWPActorVariable *var)
{
    auxRenderingVariable = var;
    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        pipeline->setRenderingAuxiliaryVariable(var);
    }
    updatePipelines(false);
}


void MTrajectoryRequestDispatcher::addPipelineForMet3DTrajectories(
    MPointGeneratorInterfaceSettings *ui,
    const QList<MPointFilterSettings*>& pointFilterUIs,
    const QList<MTrajectoryFilterSettings*>& trajectoryFilterUIs)
{
    // Create a new pipeline based on the source and filters.
    auto *pipeline = new MTrajectoryPipelineManager(memoryManager,
                                             MTrajectoryMode::COMPUTED_IN_MET3D);
    pipeline->initializePointPipeline(ui, pointFilterUIs);
    pipeline->initializeTrajectoryFilters(trajectoryFilterUIs);
    pipeline->setRenderingAuxiliaryVariable(auxRenderingVariable);

    pipelines.append(pipeline);

    // Add the computation source.
    auto computer = new MTrajectoryComputationSource(memoryManager);
    computer->setDispatcher(this);
    pipeline->switchToTrajectoryComputationSource(computer);

    connect(pipeline, SIGNAL(trajectoryPipelineChanged()),
            this, SIGNAL(pipelineChangedSignal()));
}


void MTrajectoryRequestDispatcher::rebuild(
    const QList<MPointGeneratorInterfaceSettings *>& pointSourceUIs,
    const QList<MPointFilterSettings *>& pointFilterUIs,
    const QList<MTrajectoryFilterSettings *>& trajectoryFilterUIs)
{
    // Delete all current pipelines.
    deletePipelines();

    // In precomputed mode, add one pipeline, in real-time computation add
    // one pipeline per point source.
    if (mode == MTrajectoryMode::PRECOMPUTED)
    {
        auto *pipeline = new MTrajectoryPipelineManager(memoryManager, mode);

        pipeline->switchToPrecomputedSource(precomputedDataSource);
        pipeline->initializeTrajectoryFilters(trajectoryFilterUIs);
        pipeline->setRenderingAuxiliaryVariable(auxRenderingVariable);

        connect(pipeline, SIGNAL(trajectoryPipelineChanged()), this,
                SIGNAL(pipelineChangedSignal()));

        pipelines.append(pipeline);
    }
    else
    {
        for (MPointGeneratorInterfaceSettings* pointSourceUI : pointSourceUIs)
        {
            addPipelineForMet3DTrajectories(pointSourceUI, pointFilterUIs,
                                    trajectoryFilterUIs);
        }
    }
    updatePipelines();
}


void MTrajectoryRequestDispatcher::rebuild(
    const QList<MTrajectoryFilterSettings *>& trajectoryFilterUIs)
{
    rebuild(QList<MPointGeneratorInterfaceSettings *>(),
            QList<MPointFilterSettings *>(), trajectoryFilterUIs);
}


void MTrajectoryRequestDispatcher::addPointFilter(
    MPointFilterSettings *ui)
{
    QString filterType = ui->getFilterType();
    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        MPointFilter *filter =
            MPointFilterFactory::getInstance()->create(filterType);
        filter->setUI(ui);
        filter->setMemoryManager(memoryManager);
        pipeline->addPointFilter(filter);
    }

    updatePipelines();
}


void MTrajectoryRequestDispatcher::addTrajectoryFilter(
    MTrajectoryFilterSettings *filterUI)
{
    QString filterType = filterUI->getFilterType();
    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        MTrajectoryFilter* filter = MTrajectoryFilterFactory::getInstance()->create(filterType);
        filter->setUI(filterUI);
        filter->setMemoryManager(memoryManager);
        pipeline->addTrajectoryFilter(filter);
    }

    updatePipelines();
}


void MTrajectoryRequestDispatcher::removePointSourceByIndex(int index)
{
    MTrajectoryPipelineManager* pipelineToRemove = pipelines[index];

    disconnect(pipelineToRemove, SIGNAL(trajectoryPipelineChanged()),
               this, SIGNAL(pipelineChangedSignal()));
    pipelines.removeAt(index);
    deregisterInputSource(pipelineToRemove);

    // Delete all items from this data source. When deleting a pipeline, the
    // collection group is not of use: We won't request the same data
    // item from the same sources anymore.
    memoryManager->deleteItemsFromDataSource(this);

    delete pipelineToRemove;
}


void MTrajectoryRequestDispatcher::removePointFilterByIndex(int index)
{
    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        pipeline->removePointFilter(index);
    }
}


void MTrajectoryRequestDispatcher::removeTrajectoryFilterByIndex(int index)
{
    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        pipeline->removeTrajectoryFilter(index);
    }
}


void MTrajectoryRequestDispatcher::updatePipelines(bool emitChangedSignal)
{
    // Update the pipelines in a batch. Disable emission of the signal
    // so that we do not trigger any requests during the update process.
    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        pipeline->setPipelineChangedSignal(false);
        pipeline->updatePipeline();
        registerInputSource(pipeline);
        pipeline->setPipelineChangedSignal(true);
    }
    if (emitChangedSignal)
    {
        emit pipelineChangedSignal();
    }
}


QList<QDateTime> MTrajectoryRequestDispatcher::availableInitTimes()
{
    if (! isInitialized())
    {
        return {};
    }
    if (mode == MTrajectoryMode::PRECOMPUTED && precomputedDataSource)
    {
        return precomputedDataSource->availableInitTimes();
    }
    QReadLocker availableItemsReadLocker(&availableItemsLock);
    return availableTrajectories.keys();
}


QList<QDateTime> MTrajectoryRequestDispatcher::availableValidTimes(
        const QDateTime& initTime)
{
    if (! isInitialized())
    {
        return {};
    }
    if (mode == MTrajectoryMode::PRECOMPUTED && precomputedDataSource)
    {
        return precomputedDataSource->availableValidTimes(initTime);
    }

    QReadLocker availableItemsReadLocker(&availableItemsLock);
    if (! availableTrajectories.contains(initTime))
    {
        LOG4CPLUS_ERROR(
            mlog, "Unknown init time requested when computing trajectories: "
                      << initTime.toString(Qt::ISODate)
                      << ". Returning no valid times.");
        return {};
    }
    return availableTrajectories.value(initTime).keys();
}


QList<QDateTime> MTrajectoryRequestDispatcher::validTimeOverlap(
        const QDateTime& initTime, const QDateTime& validTime)
{
    if (! isInitialized())
    {
        return {};
    }
    if (mode == MTrajectoryMode::PRECOMPUTED && precomputedDataSource)
    {
        return precomputedDataSource->validTimeOverlap(initTime, validTime);
    }

    LOG4CPLUS_ERROR(
        mlog, "Requested the valid time overlap for real-time computed "
              "trajectories. This feature is currently not supported. "
              "Returning no valid time overlap.");
    return {};
}


QSet<uint> MTrajectoryRequestDispatcher::availableEnsembleMembers()
{
    if (! isInitialized())
    {
        return {};
    }
    if (mode == MTrajectoryMode::PRECOMPUTED && precomputedDataSource)
    {
        return precomputedDataSource->availableEnsembleMembers();
    }

    QReadLocker availableItemsReadLocker(&availableItemsLock);
    return availableMembers;
}


QStringList MTrajectoryRequestDispatcher::availableAuxiliaryVariables()
{
    if (mode == MTrajectoryMode::PRECOMPUTED && precomputedDataSource)
    {
        return precomputedDataSource->availableAuxiliaryVariables();
    }
    if (auxRenderingVariable)
    {
        return QStringList() << auxRenderingVariable->variableName;
    }
    return {};
}


void MTrajectoryRequestDispatcher::setIntegrateInitAndValidTime(bool enabled)
{
    integrateInitAndValidTime = enabled;
    // Reinitialize data source to update available trajectories.
    updateAvailableTrajectories();
}


QList<MNWPActorVariable *>
MTrajectoryRequestDispatcher::getInputWindVariables()
{
    if (mode == MTrajectoryMode::PRECOMPUTED)
    {
        return {};
    }

    return QList<MNWPActorVariable *>()
           << windEastwardVariable << windNorthwardVariable
           << windVerticalVariable;
}


MNWPActorVariable *
MTrajectoryRequestDispatcher::getInputVariable(const QString& varName)
{
    if (windEastwardVariable->variableName == varName)
    {
        return windEastwardVariable;
    }
    if (windNorthwardVariable->variableName == varName)
    {
        return windNorthwardVariable;
    }
    if (windVerticalVariable->variableName == varName)
    {
        return windVerticalVariable;
    }
    return nullptr;
}

/******************************************************************************
***                            PUBLIC SLOTS                                 ***
*******************************************************************************/

void MTrajectoryRequestDispatcher::trajectoriesAvailable(MDataRequest request)
{
    MTrajectoryDataCollectionGroup* data = getData(std::move(request));
    emit dispatchCompleted(data);
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MTrajectoryRequestDispatcher::updateAvailableTrajectories()
{
    if (! isInitialized() || mode == MTrajectoryMode::PRECOMPUTED)
    {
        return;
    }
    QWriteLocker availableItemsWriteLocker(&availableItemsLock);

    availableTrajectories.clear();
    availableMembers.clear();

    // Store init and valid times.
    // Extract the set of init times are available for all required
    // variables.
    QList<QDateTime> initTimesEastward =
        windEastwardVariable->dataSource->availableInitTimes(
            windEastwardVariable->levelType,
            windEastwardVariable->variableName);
    QList<QDateTime> initTimesNorthward =
        windNorthwardVariable->dataSource->availableInitTimes(
            windNorthwardVariable->levelType,
            windNorthwardVariable->variableName);
    QList<QDateTime> initTimesVertical =
        windVerticalVariable->dataSource->availableInitTimes(
            windVerticalVariable->levelType,
            windVerticalVariable->variableName);

    // Create a map mapping init times to valid times where trajectories
    // are available in a consistent manner, thus, all required variables
    // (u, v, w) have these init and valid times available.
    QMap<QDateTime, QList<QDateTime>> initTimeToValidTimeMap;
    QSet<QDateTime> foundValidTimes; // All consistent valid times encountered.

    for (const QDateTime &initTimeEastward : initTimesEastward)
    {
        if (initTimesNorthward.contains(initTimeEastward)
            && initTimesVertical.contains(initTimeEastward))
        {
            // Available in u, v, and w.
            initTimeToValidTimeMap.insert(initTimeEastward,
                                                    QList<QDateTime>());
        }
    }

    for (auto& initTime : initTimeToValidTimeMap.keys())
    {
        // Here, also check if the valid times are available for all directions
        // regarding this specific init time.
        QList<QDateTime> validTimesEastward =
            windEastwardVariable->dataSource->availableValidTimes(
                windEastwardVariable->levelType,
                windEastwardVariable->variableName, initTime);
        QList<QDateTime> validTimesNorthward =
            windNorthwardVariable->dataSource->availableValidTimes(
                windNorthwardVariable->levelType,
                windNorthwardVariable->variableName, initTime);
        QList<QDateTime> validTimesVertical =
            windVerticalVariable->dataSource->availableValidTimes(
                windVerticalVariable->levelType,
                windVerticalVariable->variableName, initTime);

        for (const QDateTime &validTimeEastward : validTimesEastward)
        {
            if (validTimesNorthward.contains(validTimeEastward)
                && validTimesVertical.contains(validTimeEastward))
            {
                // Available in u, v, and w.
                initTimeToValidTimeMap[initTime].append(
                    validTimeEastward);
                foundValidTimes.insert(validTimeEastward);
            }
        }
    }
    QList<QDateTime> consistentInitTimes = initTimeToValidTimeMap.keys();
    // Finished setting up init-valid map.

    if (! integrateInitAndValidTime)
    {
        // Not integrate init=valid time, so normal forecast mode. For each
        // init time add the available valid times to the trajectory start times.
        for (const QDateTime& initTime : consistentInitTimes)
        {
            for (const QDateTime& validTime : initTimeToValidTimeMap.value(initTime))
            {
                availableTrajectories[initTime][validTime].filename = QString();
                availableTrajectories[initTime][validTime].isStartTime = true;
            }
        }
    }
    else
    {
        // IT = VT integration mode, e.g. GRIB ERA5 data. Here, we want to
        // iterate both over IT and VT to construct trajectories.
        // To achieve that, we look at all found valid times in the data set,
        // and check if there is a corresponding init time with the same
        // timestep (so an IT = VT pair). If found, add this as possible
        // valid time for the integration.
        for (const QDateTime& initTime : consistentInitTimes)
        {
            QList<QDateTime> validTimesForInitValidIntegration;
            for (const QDateTime& validTime : foundValidTimes.values())
            {
                if (consistentInitTimes.contains(validTime) &&
                    initTimeToValidTimeMap.value(validTime).contains(validTime))
                {
                    validTimesForInitValidIntegration.append(validTime);
                }
            }

            for (const QDateTime &validTime : validTimesForInitValidIntegration)
            {
                availableTrajectories[initTime][validTime].filename = QString();
                availableTrajectories[initTime][validTime].isStartTime = true;
            }
        }
    }

    // Store available ensemble members.
    availableMembers =
        windEastwardVariable->dataSource->availableEnsembleMembers(
            windEastwardVariable->levelType,
            windEastwardVariable->variableName);
    availableMembers.intersect(
        windNorthwardVariable->dataSource->availableEnsembleMembers(
            windNorthwardVariable->levelType,
            windNorthwardVariable->variableName));
    availableMembers.intersect(
        windVerticalVariable->dataSource->availableEnsembleMembers(
            windVerticalVariable->levelType,
            windVerticalVariable->variableName));
}


void MTrajectoryRequestDispatcher::deletePipelines()
{
    for (MTrajectoryPipelineManager* pipeline : pipelines)
    {
        deregisterInputSource(pipeline);
        disconnect(pipeline, SIGNAL(trajectoryPipelineChanged()),
                   this, SIGNAL(pipelineChangedSignal()));
        delete pipeline;
    }
    pipelines.clear();
}

} // namespace Met3D
