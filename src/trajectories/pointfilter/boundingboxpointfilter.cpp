/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "boundingboxpointfilter.h"

// standard library imports

// related third party imports
#include <utility>

// local application imports
#include "gxfw/boundingbox/boundingbox.h"
#include "gxfw/mactor.h"

namespace Met3D
{
/********************** MBoundingBoxPointFilterSettings *****************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MBoundingBoxPointFilterSettings::MBoundingBoxPointFilterSettings(QString filterType,
                                                     MActor *actor,
                                                     MProperty *parentProperty)
    : MPointFilterSettings(filterType, actor, parentProperty)
{
    bboxInterface = dynamic_cast<MBoundingBoxInterface *>(actor);
    if (! bboxInterface)
    {
        LOG4CPLUS_ERROR(mlog,
                        "Cannot create a bounding box filter for an actor "
                        "which does not implement the MBoundingBoxInterface.");
        bboxInterface = nullptr;
        return;
    }

    boundingBoxInterfaceIndex =
        bboxInterface->insertBoundingBoxProperty(groupProp);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MBoundingBoxPointFilterSettings::loadElementConfiguration(QSettings *settings)
{
    // Loading and saving handled by the bounding box interface.
}


MBoundingBoxConnection *MBoundingBoxPointFilterSettings::getBBoxConnection() const
{
    return bboxInterface->getBBoxConnection(boundingBoxInterfaceIndex);
}


void MBoundingBoxPointFilterSettings::insertCurrentStateToRequest(
    MDataRequestHelper &rh, const QStringList& keys)
{
    // Get the current state of the attached bounding box.
    MBoundingBoxConnection* bboxConnection = getBBoxConnection();
    if (! bboxConnection || ! bboxConnection->getBoundingBox())
    {
        // "None" bounding box selected in UI. Insert empty value.
        rh.insert(keys[0], "");
        return;
    }

    double west = bboxConnection->westLon();
    double south = bboxConnection->southLat();
    double lonExtent = bboxConnection->eastWestExtent();
    double latExtent = bboxConnection->northSouthExtent();
    double top_hPa = bboxConnection->topPressure_hPa();
    double bottom_hPa = bboxConnection->bottomPressure_hPa();

    QString bboxString = QString("%1/%2/%3/%4/%5/%6")
                             .arg(west)
                             .arg(south)
                             .arg(lonExtent)
                             .arg(latExtent)
                             .arg(top_hPa)
                             .arg(bottom_hPa);

    rh.insert(keys[0], bboxString);
}

/*********************** MBoundingBoxPointFilter ******************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MBoundingBoxPointFilter::MBoundingBoxPointFilter() :
      MPointFilter(getBaseRequestKeys())
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPoints *MBoundingBoxPointFilter::produceData(MDataRequest request)
{
    assert(parentFilter != nullptr);

    auto *pts = new MPoints();
    MPoints *prevFilterOut = parentFilter->getData(request);

    MDataRequestHelper rh(request);

    QStringList bboxStringList = rh.value(requestKeys[0]).split("/");
    if (bboxStringList.size() <= 1)
    {
        // No bounding box information. Forward all points.
        for (QVector3D pt : prevFilterOut->getPoints())
        {
            pts->append(pt);
        }
    }
    else
    {
        double west = bboxStringList[0].toDouble();
        double south = bboxStringList[1].toDouble();
        double lonExtent = bboxStringList[2].toDouble();
        double latExtent = bboxStringList[3].toDouble();
        double top_hPa = bboxStringList[4].toDouble();
        double bottom_hPa = bboxStringList[5].toDouble();

        double east = west + lonExtent;
        double north = south + latExtent;

        for (QVector3D pt : prevFilterOut->getPoints())
        {
            if (pt.x() < west || pt.x() > east || pt.y() < south
                || pt.y() > north || pt.z() < top_hPa || pt.z() > bottom_hPa)
            {
                // Not in box.
                continue;
            }
            pts->append(pt);
        }
    }

    // Forward the source grid since we do not alter point positions.
    pts->forwardSourceGrid(prevFilterOut);

    parentFilter->releaseData(prevFilterOut);
    return pts;
}

} // namespace Met3D