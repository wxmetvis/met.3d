/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_POINTFILTERFACTORY_H
#define MET_3D_POINTFILTERFACTORY_H

// standard library imports
#include <memory>
#include <functional>

// related third party imports
#include <QStringList>

// local application imports
#include "pointfilter.h"

namespace Met3D
{

/**
 * This factory creates point filters (@c MPointFilter), as well as
 * corresponding UI objects.
 */
class MPointFilterFactory
{
public:
    /**
     * @return The factory instance.
     */
    static MPointFilterFactory* getInstance();

    /**
     * Creates a new point filter.
     * @param filterName The name of the filter used for registration, see
     * @c MPointFilter::getFilterName().
     * @return The newly created filter.
     */
    MPointFilter* create(const QString& filterName);

    /**
     * Creates an UI object for the given filter type.
     * @param filterName The name of the filter used for registration, see
     * @c MPointFilter::getFilterName().
     * @param actor Reference to the actor where to add the properties to.
     * @param parent The parent property to attach the UI to.
     * @return The newly created UI object.
     */
    MPointFilterSettings* createUI(const QString& filterName, MActor* actor, MProperty* parent);

    /**
     * @return The list of registered filter names.
     */
    QStringList getRegisteredFilterNames() const;

    /**
     * @return The list of already reserved filter request identifiers.
     */
    QStringList getReservedFilterRequestIDs() const;

    /**
    * Opens the dialog to select a new point filter to add to the pipeline.
    * @return The name of the point filter to add.
    */
    static QString showCreationDialog();

private:
    MPointFilterFactory();
    static MPointFilterFactory* instance;

    // Map of creators to construct point filters based on the given name.
    QMap<QString, std::function<MPointFilter*()>> creators;
    // Map of creators to construct UI objects based on the given name.
    QMap<QString, std::function<MPointFilterSettings*(MActor*, MProperty*)>> uiCreators;

    // List of reserved request IDs. We have to make sure that we do not reuse
    // request IDs even after deletion of filters.
    QStringList reservedRequestKeys;

    /**
     * Register a type of point filter to the factory, so it can be created from
     * the point filter dialog.
     * @tparam T The type (class) of the filter to register, must inherit from
     * the @c MPointFilter class.
     * @tparam U The type (class) of filter UI to register, must inherit from
     * the @c MPointFilterSettings class.
     */
    template<typename T, typename U>
    void registerPointFilter() {
        static_assert(std::is_base_of<MPointFilter, T>::value,
                      "T must inherit from MPointFilter");
        static_assert(std::is_base_of<MPointFilterSettings, U>::value,
                      "U must inherit from MPointFilterSettings");

        // Add creator for this filter type.
        creators[T::getStaticFilterName()] = []()
        {
            T* filter = new T();
            return filter;
        };

        // Add creator for this UI type.
        uiCreators[T::getStaticFilterName()] =
            [](MActor *actor, MProperty *parent)
        {
            U *ui = new U(T::getStaticFilterName(), actor, parent);
            return ui;
        };
    }

};

} // namespace Met3D

#endif //MET_3D_POINTFILTERFACTORY_H
