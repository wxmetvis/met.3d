/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_BOUNDINGBOXPOINTFILTER_H
#define MET_3D_BOUNDINGBOXPOINTFILTER_H

// standard library imports

// related third party imports

// local application imports
#include "pointfilter.h"

namespace Met3D
{
class MBoundingBoxInterface;
class MBoundingBoxConnection;

/**
 * UI class for the @c MBoundingBoxPointFilter. The UI handles the connection
 * to the bounding box interface used to select a bounding box.
 */
class MBoundingBoxPointFilterSettings : public MPointFilterSettings
{
public:
    MBoundingBoxPointFilterSettings(QString filterType, MActor* actor, MProperty* parentProperty);

    void loadElementConfiguration(QSettings *settings) override;

    void insertCurrentStateToRequest(MDataRequestHelper &rh, const QStringList& keys) override;

    MBoundingBoxConnection* getBBoxConnection() const;

private:
    MBoundingBoxInterface* bboxInterface;
    int boundingBoxInterfaceIndex;
};

/**
 * This filter is a point filter, which takes points as input, and returns
 * the subset of points which are located within the bounding box selected in
 * the corresponding UI object.
 */
class MBoundingBoxPointFilter : public MPointFilter
{
public:
    MBoundingBoxPointFilter();

    MPoints *produceData(MDataRequest request) override;

    QString getFilterName() override { return "Bounding box filter"; }

    static QString getStaticFilterName() { return "Bounding box filter"; };

    QStringList getBaseRequestKeys() override { return { "POINT_FILTER_BBOX" }; }
};

} // namespace Met3D

#endif //MET_3D_BOUNDINGBOXPOINTFILTER_H
