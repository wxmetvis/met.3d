/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "thresholdpointfilter.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mscenecontrol.h"
#include "gxfw/nwpactorvariable.h"

namespace Met3D
{

/*************************** User interface **********************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MThresholdPointFilterSettings::MThresholdPointFilterSettings(
    const QString& filterType, MActor *actor, MProperty *parentProperty)
    : MPointFilterSettings(filterType, actor, parentProperty)
{
    variableIndexProp = MNWPActorVarProperty("Filter variable");
    variableIndexProp.setConfigKey("filter_variable");
    variableIndexProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(variableIndexProp);

    minFilterGroupProp = MBoolProperty("Minimum seeding threshold", false);
    minFilterGroupProp.setConfigKey("min_seeding_threshold");
    minFilterGroupProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(minFilterGroupProp);

    lowerThrProp = MSciDoubleProperty("Minimum value", 0.f);
    lowerThrProp.setConfigKey("min_value");
    lowerThrProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    minFilterGroupProp.addSubProperty(lowerThrProp);

    maxFilterGroupProp = MBoolProperty("Maximum seeding threshold", false);
    maxFilterGroupProp.setConfigKey("max_seeding_threshold");
    maxFilterGroupProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(maxFilterGroupProp);

    upperThrProp = MSciDoubleProperty("Maximum value", 20.f);
    upperThrProp.setConfigKey("max_value");
    upperThrProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    maxFilterGroupProp.addSubProperty(upperThrProp);
}


MThresholdPointFilterSettings::~MThresholdPointFilterSettings()
= default;

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MThresholdPointFilterSettings::loadElementConfiguration(
    QSettings *settings)
{
    minFilterGroupProp = settings->value(savePrefix + "lowerThrEnabled", false).toBool();
    lowerThrProp = settings->value(savePrefix + "lowerThr", 0.f).toDouble();

    maxFilterGroupProp = settings->value(savePrefix + "upperThrEnabled", false).toBool();
    upperThrProp = settings->value(savePrefix + "upperThr", 20.f).toDouble();

    QString varName = settings->value(savePrefix + "varIndex").toString();
    for (const auto &var : variableIndexProp.getVariables())
    {
        if (var->variableName == varName)
        {
            variableIndexProp.setValue(var);
            break;
        }
    }
}


double MThresholdPointFilterSettings::getLowerThreshold()
{
    double lowerThrValue = lowerThrProp;
    // Set to -INFINITY if not enabled.
    if (!minFilterGroupProp.value())
    {
        lowerThrValue = -std::numeric_limits<double>::infinity();
    }
    return lowerThrValue;
}


double MThresholdPointFilterSettings::getUpperThreshold()
{
    double upperThrValue = upperThrProp;
    // Set to INFINITY if not enabled.
    if (!maxFilterGroupProp.value())
    {
        upperThrValue = std::numeric_limits<double>::infinity();
    }
    return upperThrValue;
}


void MThresholdPointFilterSettings::onAddActorVariable(MNWPActorVariable *var)
{
    //varNameList << var->variableName;
    variableIndexProp.addVariable(var);
}


void MThresholdPointFilterSettings::onChangeActorVariable(
    MNWPActorVariable *var)
{
    variableIndexProp.changeVariable(var);
}


void MThresholdPointFilterSettings::onDeleteActorVariable(
    MNWPActorVariable *var)
{
    // Get index of variable that is about to be removed.
    //int i = varList.indexOf(var);
    variableIndexProp.removeVariable(var);
}

/*************************** Point filter *************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MThresholdPointFilter::MThresholdPointFilter()
    : MPointFilter(getBaseRequestKeys())
{
    // We also need init and valid time filter the correct thresholds.
    // END_TIME is not of use here, for backward trajectories we are also
    // starting at valid time.
    requestKeys.append("INIT_TIME");
    requestKeys.append("VALID_TIME");
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPoints *MThresholdPointFilter::produceData(MDataRequest request)
{
    assert(parentFilter != nullptr);

    auto *filteredPts = new MPoints();
    // Get the points provided by the parent filter.
    MPoints *prevFilterOut = parentFilter->getData(request);

    MDataRequestHelper rh(request);

    QStringList thresholds = rh.value(requestKeys[0]).split("/");
    QString varName = thresholds[0];
    float lowerThr = thresholds[1].toFloat();
    float upperThr = thresholds[2].toFloat();

    if (varName.isEmpty())
    {
        // No valid variable selected. Forward all points.
        for (QVector3D pt : prevFilterOut->getPoints())
        {
            filteredPts->append(pt);
        }
    }
    else
    {
        // Get the filter variable.
        auto ui = dynamic_cast<MThresholdPointFilterSettings *>(getUI());
        auto actorVarProp = ui->getVarProperty();

        MNWPActorVariable* requestVar = nullptr;
        for (auto* var : actorVarProp.getVariables())
        {
            if (var->variableName == varName)
            {
                // This is the variable to request.
                // TODO (cf, Sep24): Use data request prefixes to clearly
                //     identify the variable. Only the name is not enough, also
                //     this currently does not work with a filtered variable, as
                //     smoothing information gets not saved in the request.
                requestVar = var;
            }
        }
        if (! requestVar)
        {
            return nullptr;
        }

        MDataRequestHelper varDataRequest = requestVar->buildDataRequest();

        // Replace init and valid time with the ones from the request.
        varDataRequest.insert("INIT_TIME", rh.value("INIT_TIME"));
        varDataRequest.insert("VALID_TIME", rh.value("VALID_TIME"));

        MStructuredGrid *dataGrid =
            requestVar->dataSource->getData(varDataRequest.request());

        // Check for each point if it fulfills the thresholds.
        for (QVector3D pt : prevFilterOut->getPoints())
        {
            float interpolatedValue = dataGrid->interpolateValue(pt);
            if (interpolatedValue >= lowerThr && interpolatedValue <= upperThr)
            {
                filteredPts->append(pt);
            }
        }
        // Only in this case, the variable has been added to the task graph.
        requestVar->dataSource->releaseData(dataGrid);
    }

    // Forward the source grid since we do not alter point positions.
    filteredPts->forwardSourceGrid(prevFilterOut);

    // Release the data items.
    parentFilter->releaseData(prevFilterOut);
    return filteredPts;
}


void MThresholdPointFilterSettings::insertCurrentStateToRequest(
    MDataRequestHelper &rh, const QStringList &keys)
{
    double lower = getLowerThreshold();
    double upper = getUpperThreshold();

    QString varName = "";
    if (variableIndexProp.value() != nullptr)
    {
        varName = variableIndexProp->variableName;
    }

    QString thresholds =
        varName + "/" + QString::number(lower) + "/" + QString::number(upper);
    rh.insert(keys[0], thresholds);
}


MTask *MThresholdPointFilter::createTaskGraph(MDataRequest request)
{
    MDataRequestHelper rh(request);
    rh.removeAllKeysExcept(requiredKeys());

    auto *task = new MTask(rh.request(), this);
    if (! parentFilter)
    {
        return task;
    }
    // Add the parent point source.
    task->addParent(parentFilter->getTaskGraph(rh.request()));

    QStringList thresholds = rh.value(requestKeys[0]).split("/");
    QString varName = thresholds[0];

    auto ui = dynamic_cast<MThresholdPointFilterSettings *>(getUI());
    // Add the data variable as parent.
    MNWPActorVariable *var = ui->getVar();
    if (var == nullptr)
    {
        // No variables available.
        return task;
    }
    MDataRequestHelper varDataRequest = var->buildDataRequest();

    // Replace init and valid time with the ones from the request.
    varDataRequest.insert("INIT_TIME", rh.value("INIT_TIME"));
    varDataRequest.insert("VALID_TIME", rh.value("VALID_TIME"));

    task->addParent(var->dataSource->getTaskGraph(varDataRequest.request()));
    return task;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // namespace Met3D