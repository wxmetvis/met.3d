/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPOINTFILTER_H
#define MET_3D_MPOINTFILTER_H

// standard library imports

// related third party imports

// local application imports
#include "trajectories/filtersettings.h"
#include "trajectories/pointsource/points.h"
#include "trajectories/pointsource/pointsource.h"

namespace Met3D
{
class MActor;

/**
 * Abstract UI class for all point filters.
 */
class MPointFilterSettings : public MFilterSettings
{
public:
    MPointFilterSettings(const QString& filterType, MActor* actor, MProperty* parentProperty);
};

/**
 * Filters that take a list of points as input and return a list of points,
 * e.g. filtering them by a value, or changing them all together.
 */
class MPointFilter : public MPointSource
{
    friend class MPointFilterFactory;

public:
    /**
     * Constructs a new point filter given the base request identifier of the
     * subclass, see @c getBaseRequestKeys().
     */
    MPointFilter(const QStringList &baseRequestKeys);
    ~MPointFilter() override;

    MPoints *getData(MDataRequest request) override
    {
        return dynamic_cast<MPoints *>
        (MPointSource::getData(request));
    }

    MTask *createTaskGraph(MDataRequest request) override;

    /**
     * @return The basic identifiers used in data requests. Note that the actual
     * identifiers can differ from the base identifiers and can be fetched via
     * @c locallyRequiredKeys(). This allows to add multiple instances of the
     * same filter type into one pipeline: These need to have unique request
     * identifiers.
     * e.g., adding two filters with base key FILT_XY would create unique keys
     *   FILT_XY and FILT_XY_1.
     */
    virtual QStringList getBaseRequestKeys() = 0;

    virtual QString getFilterName() = 0;

    void setUI(MPointFilterSettings *pUi) { this->ui = pUi; }

    /**
     * Set the parent source.
     */
    void setParentSource(MPointSource* parent);

protected:
    MPointSource* parentFilter; // Parent source within the chain.
};


} // Met3D


#endif //MET_3D_MPOINTFILTER_H
