/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pointfilterfactory.h"

// standard library imports

// related third party imports

// local application imports
#include "gxfw/mselectpointfilterdialog.h"
#include "thresholdpointfilter.h"
#include "boundingboxpointfilter.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPointFilterFactory::MPointFilterFactory()
{
    registerPointFilter<MThresholdPointFilter, MThresholdPointFilterSettings>();
    registerPointFilter<MBoundingBoxPointFilter, MBoundingBoxPointFilterSettings>();
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPointFilterFactory* MPointFilterFactory::instance = nullptr;


MPointFilterFactory *MPointFilterFactory::getInstance()
{
    if (! MPointFilterFactory::instance)
    {
        MPointFilterFactory::instance = new MPointFilterFactory();
    }
    return MPointFilterFactory::instance;
}


MPointFilter *MPointFilterFactory::create(const QString& filterName)
{
    if (! creators.contains(filterName))
    {
        return nullptr; // Filter not registered.
    }
    // Return new instance.
    MPointFilter* filter = creators[filterName]();
    reservedRequestKeys.append(filter->locallyRequiredKeys());

    return filter;
}


MPointFilterSettings *MPointFilterFactory::createUI(const QString& filterName,
                                              MActor* actor, MProperty* parent)
{
    if (! creators.contains(filterName))
    {
        return nullptr; // Filter not registered.
    }
    // Return new instance.
    return uiCreators[filterName](actor, parent);
}


QStringList MPointFilterFactory::getReservedFilterRequestIDs() const
{
    return reservedRequestKeys;
}


QString MPointFilterFactory::showCreationDialog()
{
    MSelectPointFilterDialog dialog;
    if (dialog.exec() == QDialog::Rejected)
    {
        return nullptr;
    }
    return dialog.getSelectedFilterName();
}


QStringList MPointFilterFactory::getRegisteredFilterNames() const
{
    return creators.keys();
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // namespace Met3D