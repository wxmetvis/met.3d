/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MTHRESHOLDPOINTFILTER_H
#define MET_3D_MTHRESHOLDPOINTFILTER_H

// standard library imports

// related third party imports

// local application imports
#include "pointfilter.h"
#include "gxfw/properties/mnwpactorvarproperty.h"
#include "gxfw/properties/mboolproperty.h"
#include "gxfw/properties/mnumberproperty.h"

namespace Met3D
{
/**
 * User interface class for the threshold point filter.
 */
class MThresholdPointFilterSettings : public MPointFilterSettings
{
public:
    MThresholdPointFilterSettings(const QString& filterType, MActor *actor,
                                    MProperty *parentProperty);

    ~MThresholdPointFilterSettings() override;

    void loadElementConfiguration(QSettings *settings) override;

    void insertCurrentStateToRequest(MDataRequestHelper &rh, const QStringList& keys) override;

    void onAddActorVariable(MNWPActorVariable *var) override;

    void onChangeActorVariable(MNWPActorVariable *var) override;

    void onDeleteActorVariable(MNWPActorVariable *var) override;

    /**
     * @return Returns the currently set lower threshold in the interface. If
     * the lower threshold is disabled, returns -INFINITY.
     */
    double getLowerThreshold();

    /**
     * @return Returns the currently set upper threshold in the interface. If
     * the upper threshold is disabled, returns +INFINITY.
     */
    double getUpperThreshold();

    /**
     * @return The variable selected in the interface or nullptr.
     */
    MNWPActorVariable *getVar() { return variableIndexProp; }

    MNWPActorVarProperty& getVarProperty() { return variableIndexProp; }

private:
    MBoolProperty minFilterGroupProp;
    MSciDoubleProperty lowerThrProp;
    MBoolProperty maxFilterGroupProp;
    MSciDoubleProperty upperThrProp;

    MNWPActorVarProperty variableIndexProp;
};

/**
 * This filter is able to filter points by given upper and lower threshold of
 * a given data field.
 */
class MThresholdPointFilter : public MPointFilter
{
public:
    MThresholdPointFilter();

    MTask *createTaskGraph(MDataRequest request) override;

    MPoints *produceData(MDataRequest request) override;

    QString getFilterName() override { return "Threshold filter"; }

    static QString getStaticFilterName() { return "Threshold filter"; };

    QStringList getBaseRequestKeys() override { return { "POINT_FILTER_THR" }; }
};

} // namespace Met3D

#endif //MET_3D_MTHRESHOLDPOINTFILTER_H
