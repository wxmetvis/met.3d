/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectorypipelinemanager.h"

// standard library imports

// related third party imports

// local application imports
#include "source/auxiliaryvariabletrajectorysource.h"
#include "filter/singletimetrajectoryfilter.h"
#include "source/trajectorynormalssource.h"
#include "pointfilter/pointfilterfactory.h"
#include "pointpipeline.h"
#include "pointsource/pointgeneratorinterfacesettings.h"
#include "pointsource/pointsourcefactory.h"
#include "source/trajectorycomputationsource.h"
#include "filter/trajectoryfilter.h"
#include "filter/trajectoryfilterfactory.h"
#include "trajectoryrequestdispatcher.h"
#include "gxfw/nwpactorvariable.h"
#include "filter/pressuretimetrajectoryfilter.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryPipelineManager::MTrajectoryPipelineManager(MAbstractMemoryManager* memoryManager,
                                         MTrajectoryMode mode) :
      seedPipeline(nullptr),
      trajectoryDataSource(nullptr),
      singleTimeFilter(nullptr),
      mode(mode),
      emitPipelineChangedSignal(true)
{
    setMemoryManager(memoryManager);

    // Add some default pipeline elements.
    enableNormalComputation(true);
    enableSingleTimePositionComputation(true);
}


MTrajectoryPipelineManager::~MTrajectoryPipelineManager()
{
    // Delete all collections before deleting the pipeline, otherwise we lose
    // the references.
    memoryManager->deleteItemsFromDataSource(this);

    // Delete pipeline from end to start.
    for (MTrajectoryFilter* filter : trajectoryFilterChain)
    {
        delete filter;
    }
    for (MTrajectorySupplementDataSource* supplement : supplementDataSources)
    {
        delete supplement;
    }

    delete singleTimeFilter;
    singleTimeFilter = nullptr;

    // Don't delete the reader in case of precomputed data sources.
    if (mode == MTrajectoryMode::COMPUTED_IN_MET3D)
    {
        delete trajectoryDataSource;
        trajectoryDataSource = nullptr;
    }

    if (seedPipeline)
    {
        disconnect(seedPipeline, SIGNAL(pointPipelineStateChanged()), this,
                   SIGNAL(trajectoryPipelineChanged()));
        delete seedPipeline;
        seedPipeline = nullptr;
    }

}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MTrajectoryDataCollection *MTrajectoryPipelineManager::getData(MDataRequest request)
{
    return dynamic_cast<MTrajectoryDataCollection *>(
        MScheduledDataSource::getData(request));
}


MTrajectoryDataCollection *
MTrajectoryPipelineManager::produceData(MDataRequest request)
{
    // Assembling the collection of trajectory data and their supplements.
    MTrajectories *trajectories = trajectoryDataSource->getData(request);
    // Call contains to increase the reference counter to the trajectories.
    // We only save a reference to them in our data object, so we are owning
    // it as well.

    // The selection.
    MTrajectorySelection *selection = nullptr;
    if (! trajectoryFilterChain.isEmpty())
    {
        selection = trajectoryFilterChain.last()->getData(request);
    }

    // The single time positions.
    MTrajectorySelection *singleTimeData = nullptr;
    if (singleTimeFilter)
    {
        singleTimeData = singleTimeFilter->getData(request);
    }

    MTrajectoryNormals *normalData = nullptr;
    MFloatPerTrajectoryVertexSupplement *auxiliaryData = nullptr;
    QList<MSupplementalTrajectoryData *> supplementalData;
    for (MTrajectorySupplementDataSource* sSource : supplementDataSources)
    {
        MDataRequestHelper rh(request);
        rh.removeAllKeysExcept(sSource->requiredKeys());
        MSupplementalTrajectoryData *supplementData =
            sSource->getData(rh.request());

        if (! supplementData)
        {
            // Supplement did not return valid data.
            continue;
        }

        // Handle normals and auxiliary data supplements separately so we
        // can access them later more easily.
        if (supplementData->getSupplementsOfType()
            == MSupplementalTrajectoryDataType::NORMALS)
        {
            normalData = dynamic_cast<MTrajectoryNormals *>(supplementData);
        }
        else if (supplementData->getSupplementsOfType()
                 == MSupplementalTrajectoryDataType::FLOAT_PER_VERTEX)
        {
            auxiliaryData = dynamic_cast<MFloatPerTrajectoryVertexSupplement *>(
                supplementData);
        }
        else
        {
            supplementalData.append(supplementData);
        }
    }

    // Form the collection.
    auto collection = new MTrajectoryDataCollection(
        trajectories, normalData, selection, singleTimeData, auxiliaryData,
        supplementalData);

    // Set the generating source if available.
    if (mode == MTrajectoryMode::COMPUTED_IN_MET3D)
    {
        collection->setPointGenerator(seedPipeline->getPointGenerator());
    }

    // Do not release these data items here: They are referenced by this item.
    // They are released when this data item is destroyed. Then we can free them
    // again.
    return collection;
}


MTask *MTrajectoryPipelineManager::createTaskGraph(MDataRequest request)
{
    auto* task = new MTask(request, this);

    task->addParent(trajectoryDataSource->getTaskGraph(request));

    if (! trajectoryFilterChain.isEmpty())
    {
        task->addParent(trajectoryFilterChain.last()->getTaskGraph(request));
    }

    if (singleTimeFilter)
    {
        task->addParent(singleTimeFilter->getTaskGraph(request));
    }

    for (MTrajectorySupplementDataSource* sSource : supplementDataSources)
    {
        task->addParent(sSource->getTaskGraph(request));
    }

    return task;
}


const QStringList MTrajectoryPipelineManager::locallyRequiredKeys()
{
    return {};
}


void MTrajectoryPipelineManager::insertPipelineStateIntoRequest(MDataRequestHelper &rh)
{
    insertCurrentPointStateIntoRequest(rh);
    insertTrajectoryChainIntoRequest(rh);
}


void MTrajectoryPipelineManager::insertAuxiliaryIntoRequest(MDataRequestHelper &rh,
                                                     MNWPActorVariable *var)
{
    auto auxDataSource = getAuxiliarySupplementDataSource();
    if (auxDataSource)
    {
        rh.insert(auxDataSource->getRequestKey(), var->getIdentifier());
    }
}

void MTrajectoryPipelineManager::initializePointPipeline(
    MPointGeneratorInterfaceSettings *genUI,
    const QList<MPointFilterSettings *> &pointFilterUIs)
{
    // Delete the existing point pipeline.
    if (seedPipeline)
    {
        disconnect(seedPipeline, SIGNAL(pointPipelineStateChanged()), this,
                   SIGNAL(trajectoryPipelineChanged()));
        delete seedPipeline;
        seedPipeline = nullptr;
    }

    seedPipeline = new MPointPipeline();

    // Create the point source.
    MPointGeneratorSource *source = MPointSourceFactory::getInstance()->create(genUI);
    source->setMemoryManager(getMemoryManager());

    seedPipeline->setGenerator(source);

    // Create the point filters.
    for (auto filterUI : pointFilterUIs)
    {
        QString filterType = filterUI->getFilterType();
        MPointFilter *filter =
            MPointFilterFactory::getInstance()->create(filterType);
        filter->setUI(filterUI);
        filter->setMemoryManager(getMemoryManager());
        seedPipeline->addFilter(filter);
    }

    connect(seedPipeline, SIGNAL(pointPipelineStateChanged()), this,
            SIGNAL(trajectoryPipelineChanged()));

    updatePipeline();
}


void MTrajectoryPipelineManager::initializeTrajectoryFilters(
    const QList<MTrajectoryFilterSettings *>& filterUIs)
{
    // Create each trajectory filter and add it to the chain.
    for (MTrajectoryFilterSettings *filterUI : filterUIs)
    {
        QString filterType = filterUI->getFilterType();
        MTrajectoryFilter *filter =
            MTrajectoryFilterFactory::getInstance()->create(filterType);
        filter->setUI(filterUI);
        filter->setMemoryManager(getMemoryManager());
        trajectoryFilterChain.append(filter);
    }
}


void MTrajectoryPipelineManager::switchToTrajectoryComputationSource(
    MTrajectoryComputationSource *comp)
{
    // Delete existing computation source if it is a computation source.
    // Precomputation sources don't need to be deleted, these are
    // TrajectoryReaders and are initialized in the pipeline configuration.
    if (trajectoryDataSource && mode == MTrajectoryMode::COMPUTED_IN_MET3D)
    {
        delete trajectoryDataSource;
    }
    mode = MTrajectoryMode::COMPUTED_IN_MET3D;
    trajectoryDataSource = comp;
    updatePipeline();
}


void MTrajectoryPipelineManager::switchToPrecomputedSource(
    MTrajectoryDataSource *source)
{
    // Delete existing computation source if it is a computation source.
    // Precomputation sources don't need to be deleted, these are
    // TrajectoryReaders and are initialized in the pipeline configuration.
    if (trajectoryDataSource && mode == MTrajectoryMode::COMPUTED_IN_MET3D)
    {
        delete trajectoryDataSource;
    }

    // Also we do not need the seed points anymore if precomputed.
    if (seedPipeline)
    {
        disconnect(seedPipeline, SIGNAL(pointPipelineStateChanged()), this,
                   SIGNAL(trajectoryPipelineChanged()));
        delete seedPipeline;
        seedPipeline = nullptr;
    }

    mode = MTrajectoryMode::PRECOMPUTED;
    trajectoryDataSource = source;
    updatePipeline();
}


void MTrajectoryPipelineManager::enableNormalComputation(bool enabled)
{
    // Create a normal source if requested and the normal source is not in the
    // pipeline yet.
    MTrajectoryNormalsSource* normalSourceInPipeline = getNormalDataSource();

    if (! normalSourceInPipeline && enabled)
    {
        // Create normal source and update pipeline.
        auto *trajectoryNormals = new MTrajectoryNormalsSource();
        trajectoryNormals->setMemoryManager(memoryManager);
        supplementDataSources.append(trajectoryNormals);
        updatePipeline();
    }
    else if (normalSourceInPipeline && ! enabled)
    {
        // Remove normal source from pipeline.
        supplementDataSources.removeOne(normalSourceInPipeline);
        delete normalSourceInPipeline;
        updatePipeline();
    }
}


void MTrajectoryPipelineManager::enableSingleTimePositionComputation(bool enabled)
{
    if (! singleTimeFilter && enabled)
    {
        // Create single time filter and update pipeline.
        singleTimeFilter = new MSingleTimeTrajectoryFilter();
        singleTimeFilter->setMemoryManager(memoryManager);
        updatePipeline();
    }
    else if (singleTimeFilter && ! enabled)
    {
        // Remove single time filter from pipeline.
        delete singleTimeFilter;
        singleTimeFilter = nullptr;
        updatePipeline();
    }
}


void MTrajectoryPipelineManager::setRenderingAuxiliaryVariable(MNWPActorVariable *var)
{
    MAuxiliaryVariableTrajectorySource *auxDataSource =
        getAuxiliarySupplementDataSource();

    if (! var)
    {
        // Disable the data source.
        if (auxDataSource)
        {
            supplementDataSources.removeOne(auxDataSource);
            delete auxDataSource;
        }
        return;
    }

    if (! auxDataSource)
    {
        auxDataSource = new MAuxiliaryVariableTrajectorySource();
        auxDataSource->setMemoryManager(getMemoryManager());
        supplementDataSources.append(auxDataSource);
    }

    auxDataSource->setAuxiliaryVariable(var);
}


MAuxiliaryVariableTrajectorySource *
MTrajectoryPipelineManager::getAuxiliarySupplementDataSource()
{
    MAuxiliaryVariableTrajectorySource *auxSourceInPipeline = nullptr;
    for (MTrajectorySupplementDataSource *sSource : supplementDataSources)
    {
        auxSourceInPipeline =
            dynamic_cast<MAuxiliaryVariableTrajectorySource *>(sSource);
        if (auxSourceInPipeline)
        {
            break;
        }
    }
    return auxSourceInPipeline;
}


MTrajectoryNormalsSource *MTrajectoryPipelineManager::getNormalDataSource()
{
    MTrajectoryNormalsSource *auxSourceInPipeline = nullptr;
    for (MTrajectorySupplementDataSource *sSource : supplementDataSources)
    {
        auxSourceInPipeline = dynamic_cast<MTrajectoryNormalsSource *>(sSource);
        if (auxSourceInPipeline)
        {
            break;
        }
    }
    return auxSourceInPipeline;
}


void MTrajectoryPipelineManager::addPointFilter(MPointFilter *filter)
{
    if (seedPipeline)
    {
        seedPipeline->addFilter(filter);
    }
}


void MTrajectoryPipelineManager::removePointFilter(int index)
{
    if (seedPipeline)
    {
        seedPipeline->removeFilter(index);
    }
}


void MTrajectoryPipelineManager::addTrajectoryFilter(MTrajectoryFilter *filter)
{
    trajectoryFilterChain.append(filter);
}


void MTrajectoryPipelineManager::removeTrajectoryFilter(int index)
{
    MTrajectoryFilter* filter = trajectoryFilterChain[index];
    filter->getUI()->removeSelfFromParent();
    trajectoryFilterChain.removeAt(index);
}


void MTrajectoryPipelineManager::setPipelineChangedSignal(bool enabled)
{
    emitPipelineChangedSignal = enabled;
}


bool MTrajectoryPipelineManager::checkForPipelineIssues(
    const MTrajectoryDispatchInfo &dispatchInfo)
{
    // Print warning if there is a trajectory ascent filter in the pipeline,
    // but we request streamlines, which do not have a time component,
    // therefore no time integration takes place: the ascent filter requires
    // this time component. The filter would return no trajectories.
    if (mode == MTrajectoryMode::COMPUTED_IN_MET3D
        && dispatchInfo.lineType == STREAM_LINE)
    {
        for (MTrajectoryFilter* filter : trajectoryFilterChain)
        {
            if (dynamic_cast<MPressureTimeTrajectoryFilter*>(filter))
            {
                LOG4CPLUS_WARN(
                    mlog,
                    "The trajectory filter pipeline contains a trajectory "
                    "ascent filter, but streamlines have been requested. Since "
                    "the ascent filter requires time integrated trajectories "
                    "for filtering, and streamlines are not time integrated, "
                    "this filter will not return any trajectories.");
            }
            break;
        }
    }
    return false;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MTrajectoryPipelineManager::updatePipeline()
{
    // Also re-register own sources.
    deregisterInputSources();

    if (! trajectoryDataSource)
    {
        // Pipeline not yet fully created.
        return;
    }

    // If we have a seed pipeline, update it. This also implies that we are
    // computing trajectories within Met.3D.
    if (seedPipeline)
    {
        seedPipeline->updateFilterChain();
        auto computer = dynamic_cast<MTrajectoryComputationSource *>(trajectoryDataSource);
        computer->setPointSource(seedPipeline->getLastSourceInChain());
    }

    registerInputSource(trajectoryDataSource);

    // Set the inputs for elements in the trajectory filter chain:
    // They get the trajectory data source, and the selection source
    // (parent filter).
    if (! trajectoryFilterChain.isEmpty())
    {
        trajectoryFilterChain[0]->setInputSelectionSource(trajectoryDataSource);
        trajectoryFilterChain[0]->setTrajectorySource(trajectoryDataSource);
    }

    for (int i = 1; i < trajectoryFilterChain.size(); i++)
    {
        trajectoryFilterChain[i]->setInputSelectionSource(
            trajectoryFilterChain[i - 1]);
        trajectoryFilterChain[i]->setTrajectorySource(trajectoryDataSource);
    }

    if (! trajectoryFilterChain.isEmpty())
    {
        registerInputSource(trajectoryFilterChain.last());
    }

    if (singleTimeFilter)
    {
        // Attach single time filter to last filter in trajectory filter chain.
        if (! trajectoryFilterChain.isEmpty())
        {
            singleTimeFilter->setInputSelectionSource(
                trajectoryFilterChain.last());
            singleTimeFilter->setTrajectorySource(trajectoryDataSource);
        }
        else
        {
            singleTimeFilter->setInputSelectionSource(trajectoryDataSource);
            singleTimeFilter->setTrajectorySource(trajectoryDataSource);
        }

        registerInputSource(singleTimeFilter);
    }

    // We use the computation source here also as filter source. Therefore,
    // the supplements are generated for all generated trajectories, and not
    // only for the filtered part. While this is more computation effort,
    // this makes changing the trajectory filters more interactive since this
    // does not require a re-computation of the filters.
    for (MTrajectorySupplementDataSource* sSource : supplementDataSources)
    {
        sSource->setInputSelectionSource(trajectoryDataSource);
        sSource->setTrajectorySource(trajectoryDataSource);

        registerInputSource(sSource);
    }

    if (emitPipelineChangedSignal)
    {
        emit trajectoryPipelineChanged();
    }
}

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MTrajectoryPipelineManager::insertCurrentPointStateIntoRequest(
    MDataRequestHelper &rh)
{
    if (seedPipeline)
    {
        seedPipeline->insertCurrentStateIntoRequest(rh);
    }
}


void MTrajectoryPipelineManager::insertTrajectoryChainIntoRequest(
    MDataRequestHelper &rh)
{
    for (MTrajectoryFilter* trajFilter : trajectoryFilterChain)
    {
        trajFilter->insertCurrentStateToRequest(rh);
    }
}

} // Met3D