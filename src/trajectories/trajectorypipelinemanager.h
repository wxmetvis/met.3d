/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_TRAJECTORYPIPELINEMANAGER_H
#define MET_3D_TRAJECTORYPIPELINEMANAGER_H

// standard library imports

// related third party imports

// local application imports
#include "data/scheduleddatasource.h"
#include "data/trajectorydatacollection.h"

namespace Met3D
{
class MTrajectoryDataCollection;
class MPointFilter;
class MPointFilterSettings;
class MTrajectoryFilter;
class MTrajectoryFilterSettings;
class MPointGeneratorInterfaceSettings;
class MTrajectoryComputationSource;
class MTrajectoryDataSource;
class MAuxiliaryVariableTrajectorySource;
class MPointPipeline;
class MTrajectorySupplementDataSource;
class MNWPActorVariable;
class MSingleTimeTrajectoryFilter;
class MTrajectoryNormalsSource;
class MTrajectoryDispatchInfo;
enum MTrajectoryMode : unsigned short;

/**
 * This class encapsulates a trajectory pipeline. The class itself also serves
 * as data source working as a "barrier" at the end of the trajectory pipeline.
 * Thus, once a data request is finished from this source, all trajectory data
 * items are available (vertices, normals, supplemental data...).
 * The pipeline has the following setup:
 * - A @c MPointGeneratorSource generates points to seed the trajectories.
 * - A list of @c MPointFilters are next in the pipeline to edit these points.
 * - A trajectory source computes trajectories from the list of points, or if
 *      the trajectories are precomputed, they are read from disk.
 * - The trajectories serve as input for the next pipeline steps, which compute
 *      supplemental data: normals, single time positions, auxiliary variables
 *      per vertex. More supplements can be added.
 * - The trajectory filter chain (@c MTrajectoryFilter) also takes the
 *      trajectories as input. The result of the filter chain is also provided
 *      to the single time positions to filter them accordingly.
 * - This class has all these supplements as parent sources and waits for their
 *      computation to be finished.
 */
class MTrajectoryPipelineManager : public MScheduledDataSource
{
    Q_OBJECT
public:
    /**
     * Creates a new pipeline.
     */
    MTrajectoryPipelineManager(MAbstractMemoryManager *memoryManager,
                        MTrajectoryMode mode);
    ~MTrajectoryPipelineManager() override;

    MTrajectoryDataCollection *getData(MDataRequest request) override;

    MTrajectoryDataCollection *produceData(MDataRequest request) override;

    MTask *createTaskGraph(MDataRequest request) override;

    /**
     * Insert the state of the sources and filters present in this pipeline
     * into the data request.
     * @param rh The request helper to add the state to.
     */
    void insertPipelineStateIntoRequest(MDataRequestHelper& rh);
    void insertAuxiliaryIntoRequest(MDataRequestHelper &rh, MNWPActorVariable* var);

    /**
     * Initialize the point pipeline from the given UI elements. This
     * initializes the point source and the point filters.
     * @param pointSourceUI The point source UI.
     * @param pointFilterUIs List of point filter UIs.
     */
    void initializePointPipeline(MPointGeneratorInterfaceSettings* pointSourceUI,
                                 const QList<MPointFilterSettings*>& pointFilterUIs);

    /**
     * Initialize the trajectory filter chain from the given filter UI elements.
     * @param filterUIs The list of trajectory filter UI instances.
     */
    void initializeTrajectoryFilters(const QList<MTrajectoryFilterSettings*>& filterUIs);

    /**
     * Set the trajectory computation source for this pipeline and switch to
     * this mode.
     * @param comp The computation source.
     */
    void switchToTrajectoryComputationSource(MTrajectoryComputationSource* comp);

    /**
     * Set the precomputed trajectory source and switch to this mode.
     * @param source The precomputed source.
     */
    void switchToPrecomputedSource(MTrajectoryDataSource* source);

    /**
     * Enable or disable normal computation, this updates the pipeline.
     */
    void enableNormalComputation(bool enabled);

    /**
     * Enable or disable computation of single time positions in the pipeline.
     */
    void enableSingleTimePositionComputation(bool enabled);

    /**
     * Set the auxiliary variable to compute values per trajectory vertices
     * in the pipeline.
     * @param var The actor variable.
     */
    void setRenderingAuxiliaryVariable(MNWPActorVariable* var);

    /**
     * @return The data source in the pipeline responsible for computing
     * values at trajectory vertices based on the auxiliary data field set.
     */
    MAuxiliaryVariableTrajectorySource* getAuxiliarySupplementDataSource();

    /**
     * @return The data source in the pipeline responsible for computing
     * values at trajectory vertices based on the auxiliary data field set.
     */
    MTrajectoryNormalsSource* getNormalDataSource();

    /**
     * Add a point filter to the pipeline.
     * @param filter The point filter to add.
     */
    void addPointFilter(MPointFilter* filter);

    /**
     * Remove a point filter to the pipeline.
     * @param index The index of the point filter to remove.
     */
    void removePointFilter(int index);

    /**
     * Add a trajectory filter to the pipeline.
     * @param filter The trajectory filter to add.
     */
    void addTrajectoryFilter(MTrajectoryFilter* filter);

    /**
     * Remove a trajectory filter to the pipeline.
     * @param index The index of the trajectory filter to remove.
     */
    void removeTrajectoryFilter(int index);

    /**
     * Enables or disables the emission of the pipeline changed signal. Emitting
     * the signal might lead to computation requests, while parts of the
     * pipeline are still not in an updated state.
     * @param enabled bool to enable or disable the signal.
     */
    void setPipelineChangedSignal(bool enabled);

    /**
     * Check for issues with the dispatch requested to execute with the current
     * state of the pipeline. If there are issues, print them to the user.
     * @return False if a critical issue is encountered, so that the provided
     * @p dispatchInfo should not trigger a data request. Otherwise,
     * if no or no critical issues are encountered, return True, but print
     * warnings, accordingly.
     */
    bool checkForPipelineIssues(const MTrajectoryDispatchInfo& dispatchInfo);

protected:
    const QStringList locallyRequiredKeys() override;

signals:
    /**
     * Emitted whenever the pipeline state changed and the signal flag for
     * emission is set to True.
     */
    void trajectoryPipelineChanged();

public slots:
    /**
     * Updates the pipeline. This updates the connections between filters and
     * sources in the pipeline and recomputes the required keys.
     */
    void updatePipeline();

private:
    /**
     * Insert the current state of the point pipeline into the provided
     * request helper @p rh.
     */
    void insertCurrentPointStateIntoRequest(MDataRequestHelper& rh);

    /**
     * Insert the current state of the trajectory filter pipeline into the
     * provided request helper @p rh.
     */
    void insertTrajectoryChainIntoRequest(MDataRequestHelper& rh);

    // The point pipeline handling the point source and point filters.
    MPointPipeline *seedPipeline;
    // The computation source or precomputed data source.
    MTrajectoryDataSource *trajectoryDataSource;
    // Filter chain for selection filters (getting a subset of trajectories).
    QList<MTrajectoryFilter*> trajectoryFilterChain;

    // Supplemental data: normals, float per trajectory...
    QList<MTrajectorySupplementDataSource*> supplementDataSources;
    // Single time positions.
    MSingleTimeTrajectoryFilter* singleTimeFilter;

    MTrajectoryMode mode;
    bool emitPipelineChangedSignal;
};

} // Met3D


#endif //MET_3D_TRAJECTORYPIPELINEMANAGER_H
