/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPIPELINEELEMENTUI_H
#define MET_3D_MPIPELINEELEMENTUI_H

// standard library imports

// related third party imports
#include <QSettings>

// local application imports
#include "data/datarequest.h"
#include "gxfw/properties/mproperty.h"
#include "gxfw/properties/mbuttonproperty.h"

namespace Met3D
{

class MActor;
class MNWPActorVariable;

/**
 * Abstract class representing the UI elements of one module in the data
 * pipeline. This class already contains the group property, as well as a
 * remove property. Implementations of this class can add further pipeline
 * module specific UI properties.
 */
class MPipelineElementSettings : public QObject
{

    Q_OBJECT

public:
    /**
     * Creates a new pipeline module UI object.
     * @param name The name to be displayed in the group.
     * @param actor The actor to whom the UI should be attached to.
     * @param parentProperty The parent property of this UI module.
     */
    MPipelineElementSettings(const QString& name, MActor* actor, MProperty *pProperty);

    ~MPipelineElementSettings() override;

    /**
     * Save the current configuration of the properties to the @c settings.
     * Calls @c saveElementConfiguration to let the implementation save the
     * specific settings.
     * @param settings The settings to save the property values to.
     */
    virtual void saveConfiguration(QSettings *settings) const;

    /**
     * Load the configuration saved in @c settings into the properties of this
     * class. Calls @c loadElementConfiguration for the implementation of
     * loading specific properties.
     * This is specifically for config files created prior to 1.14.
     * @param settings The settings to load from.
     * @param prefix The prefix to use for the keys when loading.
     */
    void loadConfigurationPrior_V_1_14(QSettings *settings);

    /**
     * Load the configuration saved in @c settings into the local properties.
     * @param settings The settings to load from.
     */
    virtual void loadElementConfiguration(QSettings *settings) = 0;

    virtual void onAddActorVariable(MNWPActorVariable* var) {};

    virtual void onChangeActorVariable(MNWPActorVariable* var) {};

    virtual void onDeleteActorVariable(MNWPActorVariable* var) {};

    /**
     * Remove this pipeline module UI from the property tree.
     */
    void removeSelfFromParent();

    /**
     * Insert the @p key / value pair of the current state of the settings
     * associated with this source into the data request @p rh. The value
     * should contain all required information to define unambiguous behaviour
     * of the filter.
     * @param rh The request helper to populate.
     * @param keys The keys used to store the current state as value at.
     */
    virtual void insertCurrentStateToRequest(MDataRequestHelper& rh, const QStringList& keys) = 0;

    QString getName() const;

    /**
     * Set the prefix of this pipeline element, which is used to identify
     * it when saving and loading from file.
     * @param prefix The prefix for this element.
     */
    void setSavePrefix(const QString &prefix);

public slots:
    /**
     * Rename the group property to the provided @p newName.
     */
    void renameGroup(const QString& newName);

signals:
    /**
     * Signal, that is emitted when this pipeline element is removed.
     */
    void elementRemoved();

    /**
     * Signal, that needs to be emitted from derived classes, when any of the
     * pipeline element settings change.
     * @param element The changed pipeline element.
     */
    void elementChanged();

protected:
    MProperty *parentProperty;
    MProperty groupProp;
    MButtonProperty removeProp;

    QString name;
    QString savePrefix;
};

} // namespace Met3D

#endif //MET_3D_MPIPELINEELEMENTUI_H
