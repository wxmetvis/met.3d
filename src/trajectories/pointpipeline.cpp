/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pointpipeline.h"

// standard library imports

// related third party imports

// local application imports
#include "pointfilter/pointfilter.h"
#include "pointsource/pointgeneratorinterface.h"
#include "pointsource/pointgeneratorsource.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPointPipeline::MPointPipeline() : pointGenerator(nullptr)
{
}


MPointPipeline::~MPointPipeline()
{
    // The point generating interface is responsible for deleting the point
    // generator source.
    for (MPointFilter* filter : pointFilterChain)
    {
        delete filter;
    }
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MPointPipeline::addFilter(MPointFilter *filter)
{
    pointFilterChain.append(filter);
    updateFilterChain();
}


void MPointPipeline::removeFilter(MPointFilter *filter)
{
    int filterIdx = -1;
    // Get the index of the filter to delete.
    for (int i = 0; i < pointFilterChain.size(); i++)
    {
        if (pointFilterChain[i] == filter)
        {
            filterIdx = i;
            break;
        }
    }

    removeFilter(filterIdx);
}


void MPointPipeline::removeFilter(int index)
{
    if (index == -1)
    {
        return;
    }
    MPointFilter* filterToRemove = pointFilterChain[index];

    // Remove filter and frontend connection.
    pointFilterChain.removeAt(index);
    filterToRemove->getUI()->removeSelfFromParent();
    delete filterToRemove;

    updateFilterChain();
}


void MPointPipeline::setGenerator(MPointGeneratorSource *generator)
{
    if (pointGenerator)
    {
        disconnect(pointGenerator, SIGNAL(pointSourceStateChanged()),
                   this, SIGNAL(pointPipelineStateChanged()));
    }

    this->pointGenerator = generator;

    connect(pointGenerator, SIGNAL(pointSourceStateChanged()),
            this, SIGNAL(pointPipelineStateChanged()));

    updateFilterChain();
}


void MPointPipeline::updateFilterChain()
{
    if (! pointFilterChain.isEmpty())
    {
        pointFilterChain[0]->setParentSource(pointGenerator);
    }
    for (int i = 1; i < pointFilterChain.size(); i++)
    {
        pointFilterChain[i]->setParentSource(pointFilterChain[i - 1]);
    }
}


MPoints *MPointPipeline::getData(MDataRequest request) const
{
    return getLastSourceInChain()->getData(std::move(request));
}


MPointSource *MPointPipeline::getLastSourceInChain() const
{
    if (pointFilterChain.isEmpty())
    {
        return pointGenerator;
    }
    else
    {
        return pointFilterChain.last();
    }
}


MPointGeneratorSource *MPointPipeline::getPointGenerator()
{
    return pointGenerator;
}


void MPointPipeline::insertCurrentStateIntoRequest(MDataRequestHelper &rh) const
{
    pointGenerator->insertCurrentStateToRequest(rh);
    for (MPointFilter* filter : pointFilterChain)
    {
        filter->insertCurrentStateToRequest(rh);
    }
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // Met3D