/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2017-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_TRAJECTORYCOMPUTATION_H
#define MET_3D_TRAJECTORYCOMPUTATION_H

// standard library imports
#include <memory>

// related third party imports
#include <QVector>
#include <QVector3D>
#include <QDateTime>

// local application imports
#include "data/datarequest.h"

namespace Met3D
{
class MNWPActorVariable;
class MTrajectoryRequestDispatcher;
class MStructuredGrid;

// Enums for different computation settings.
enum MTrajectoryComputationIntegrationMethod
{
    EULER,
    RUNGE_KUTTA
};

enum MTrajectoryComputationInterpolationMethod
{
    // NOTE: See Philipp Kaiser's master's thesis (TUM 2017) for details on the
    // different interpolation approaches. The LAGRANTO_INTERPOLATION method
    // follows the implementation of LAGRANTO v2
    // (http://dx.doi.org/10.5194/gmd-8-2569-2015).
    LAGRANTO_INTERPOLATION,
    MET3D_INTERPOLATION
};

enum MTrajectoryComputationLineType
{
    PATH_LINE,
    STREAM_LINE
};

/**
 * Stores data that for each trajectory computation.
 */
struct MTrajectoryComputationResult
{
    MTrajectoryComputationResult()
        : numStoredVerticesPerTrajectory(0)
    { }

    int numStoredVerticesPerTrajectory;

    // QVector3D (lon, lat, pres) * numStoredVerticesPerTrajectory * numTrajectories
    QVector<QVector<QVector3D>> vertices;

    // Times corresponding to the trajectory vertices. The times have to be ascending
    // to guarantee that all filters work properly.
    QVector<QDateTime> times;

    // Start grid geometry (i.e., seed points).
    QVector<QVector3D> seedPoints;

    // Start grid structure
    std::shared_ptr<MStructuredGrid> startGrid;
};

/**
 * This class handles the actual computation of trajectories. Sources computing
 * stream lines or path lines can create an instance of this class, fill the
 * required parameters, and call compute().
 */
class MTrajectoryComputation
{
public:
    MTrajectoryComputation(MTrajectoryRequestDispatcher* dispatcher);

    ~MTrajectoryComputation();

    /**
     * Set the seed points to start the trajectories from.
     */
    void setSeedPoints(QVector<QVector3D> pts);

    /**
     * Fill the computation parameters from the given data request.
     */
    void fillParameters(const MDataRequest& request);

    /**
     * Computes the trajectories. Afterwards, the result can be fetched with
     * @c getResult(). Calling another compute() deletes the previous result.
     */
    void compute();

    /**
     * @return The computation result.
     */
    MTrajectoryComputationResult* getResult();

private:
    /* Internal helper functions for trajectory computation. */

    /**
     * Compute the streamlines if requested.
     */
    void computeStreamLines();

    /**
     * Compute path lines if requested.
     */
    void computePathLines();

    /**
     * Helper method that calls @c getData() on all required data fields to perform the
     * computation.
     * @param allDataFieldsValid Set to true if all grids could be found.
     * @return A map of time indices to grids of that time index. The grids are the
     * u, v, and w grids.
     */
    QMap<int, QVector<MStructuredGrid *>> getRequiredGridsForPathLines(bool& allDataFieldsValid);

    /**
     * Converts 3D wind velocity in (m/s, m/s, Pa/s) to units (deg lon, deg lat,
     * hPa/s), taking current position on sphere into account.
     */
    static QVector3D convertWindVelocityFromMetricToSpherical(
        QVector3D velocity_ms_ms_Pas, QVector3D position_lon_lat_p);

    /**
     * Performs Euler integration between @p timePos0 and @p timePos1 on
     * @p grids with step size @p deltaT using the interpolation method given by
     * @p method to get wind velocity at @p pos.
     *
     * @p grids needs to contain eastward wind grid, northward wind grid and
     * vertical wind grid in this order.
     *
     * If the integration was successful @p valid will be set to true, false
     * otherwise.
     *
     * @note The method performs @ref EULER_ITERATION times the integration using
     * the results of the previous integration.
     */
    static QVector3D trajectoryIntegrationEuler(
        QVector3D pos, float deltaT, float timePos0, float timePos1,
        MTrajectoryComputationInterpolationMethod method,
        QVector<QVector<MStructuredGrid*>> &grids, bool& valid);

    /**
     * Performs Runge-Kutta integration between @p timePos0 and @p timePos1 on
     * @p grids with step size @p deltaT using the interpolation method given by
     * @p method to get wind velocity at @p pos.
     *
     * @p grids needs to contain eastward wind grid, northward wind grid and
     * vertical wind grid in this order.
     *
     * If the integration was successful @p valid will be set to true, false
     * otherwise.
     */
    static QVector3D trajectoryIntegrationRungeKutta(
        QVector3D pos, float deltaT, float timePos0, float timePos1,
        MTrajectoryComputationInterpolationMethod method,
        QVector<QVector<MStructuredGrid*>> &grids, bool& valid);

    /* Sampling methods. */
    /**
     * Samples wind velocity at @p pos in 3D space and time from @p grids using
     * the interpolation method @p method and the time interpolation
     * factor @p timeInterpolationValue (0..1).
     *
     * @p grids is a two-dimensional list of grids that needs to contain two
     * timesteps of eastward wind grid, northward wind grid and vertical wind
     * grid (in this order).
     *
     * If sampling was successful @p valid will be set to true, false otherwise.
     *
     * @note The different approaches to 3D space/time interpolation are
     * documented in Philipp Kaiser's master's thesis (TUM 2017).
     */
    static QVector3D sampleVelocity3DSpaceTime(
        QVector3D pos, float timeInterpolationValue,
        MTrajectoryComputationInterpolationMethod method,
        QVector<QVector<MStructuredGrid*>> &grids, bool& valid);

    /**
     * Performs tri-linear interpolation to obtain the pressure value of
     * @p grid at the float index position @p index.
     */
    static float samplePressureAtFloatIndex(QVector3D index, MStructuredGrid *grid);

    /**
     * Performs tri-linear interpolation to obtain the data value of
     * @p grid at the float index position @p indexstatic .
     */
    static float sampleDataValueAtFloatIndex(QVector3D index, MStructuredGrid *grid);

    /**
     * Determine indices (of lon, lat, pressure) as floating point numbers
     * representing @p pos with respect to position and dimensions of @p grid.
     *
     * If an index is out of bounds it is replaced by -1.
     */
    static QVector3D floatIndexAtPos(QVector3D pos, MStructuredGrid *grid);

    /**
     * Determines the float indices at position @p pos in @p grid0 and @p grid1,
     * then interpolates these indices linearly in time using the time using
     * the time interpolation factor @p timeInterpolationValue.
     *
     * @note The different approaches to 3D space/time interpolation are
     * documented in Philipp Kaiser's master's thesis (TUM 2017).
     */
    static QVector3D floatIndexAtPosInterpolatedInTime(
        QVector3D pos, MStructuredGrid* grid0,
        MStructuredGrid* grid1, float timeInterpolationValue);

    /**
     * For a pre-computed float index, first samples the data values in both
     * grids @p grid0 and @p grid1 using @ref sampleDataValueAtFloatIndex(),
     * then linearly interpolates the two values in time using the time
     * interpolation factor @p timeInterpolationValue.
     *
     * @note The different approaches to 3D space/time interpolation are
     * documented in Philipp Kaiser's master's thesis (TUM 2017).
     static */
    static float sampleDataValueAtFloatIndexAndInterpolateInTime(
        QVector3D index, MStructuredGrid *grid0,
        MStructuredGrid *grid1, float timeInterpolationValue);

    MTrajectoryRequestDispatcher* dispatcher;
    MTrajectoryComputationResult* result;

    /** Computation parameters */
    QList<QDateTime> validTimes;
    MDataRequestHelper baseRequestHelper;
    MDataRequest baseRequest;
    MTrajectoryComputationIntegrationMethod integrationMethod;
    MTrajectoryComputationInterpolationMethod interpolationMethod;
    MTrajectoryComputationLineType lineType;
    QString uVarName;
    QString vVarName;
    QString wVarName;
    int startTimeStep;
    int endTimeStepBackward, endTimeStepForward;
    int subTimeStepsPerDataTimeStep;
    bool vertexAtSubTimeSteps;
    float streamlineDeltaS; // delta of parameter "s" that parameterises
                             // the streamline: dx(s)/ds = v(x)
    int streamlineLength;
    bool integrateInitAndValid;
};

} // namespace Met3D

#endif //MET_3D_TRAJECTORYCOMPUTATION_H
