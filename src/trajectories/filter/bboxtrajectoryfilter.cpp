/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2016-2017 Marc Rautenhaus
**  Copyright 2016-2017 Bianca Tost
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "bboxtrajectoryfilter.h"

// standard library imports
#include <cassert>
#include <utility>

// related third party imports

// local application imports
#include "util/mutil.h"
#include "trajectories/source/trajectorydatasource.h"


namespace Met3D
{

MBoundingBoxTrajectoryFilterSettings::MBoundingBoxTrajectoryFilterSettings(
    QString filterType, MActor *actor, MProperty *parentProperty)
    : MTrajectoryFilterSettings(std::move(filterType), actor, parentProperty)
{
    bboxInterface = dynamic_cast<MBoundingBoxInterface *>(actor);
    if (! bboxInterface)
    {
        LOG4CPLUS_ERROR(mlog,
                        "Cannot create a bounding box filter for an actor"
                        " which does not implement the MBoundingBoxInterface"
                        " functionality.");
        bboxInterface = nullptr;
        return;
    }

    boundingBoxInterfaceIndex =
        bboxInterface->insertBoundingBoxProperty(groupProp);
}


void MBoundingBoxTrajectoryFilterSettings::loadElementConfiguration(
    QSettings *settings)
{
    // Loading and saving handled by the bounding box interface.
}


MBoundingBoxConnection *MBoundingBoxTrajectoryFilterSettings::getBBoxConnection() const
{
    return bboxInterface->getBBoxConnection(boundingBoxInterfaceIndex);
}


void MBoundingBoxTrajectoryFilterSettings::insertCurrentStateToRequest(
    MDataRequestHelper &rh, const QStringList& keys)
{
    // Get the current state of the bounding box.
    MBoundingBoxConnection* bboxConnection = getBBoxConnection();
    if (! bboxConnection || ! bboxConnection->getBoundingBox())
    {
        // None selected in UI.
        rh.insert(keys[0], "");
        return;
    }
    double west = bboxConnection->westLon();
    double south = bboxConnection->southLat();
    double lonExtent = bboxConnection->eastWestExtent();
    double latExtent = bboxConnection->northSouthExtent();
    double top_hPa = bboxConnection->topPressure_hPa();
    double bottom_hPa = bboxConnection->bottomPressure_hPa();

    QString bboxString = QString("%1/%2/%3/%4/%5/%6")
                             .arg(west)
                             .arg(south)
                             .arg(lonExtent)
                             .arg(latExtent)
                             .arg(top_hPa)
                             .arg(bottom_hPa);

    rh.insert(keys[0], bboxString);
}

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MBoundingBoxTrajectoryFilter::MBoundingBoxTrajectoryFilter() :
      MTrajectoryFilter(getBaseRequestKeys())
{}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MBoundingBoxTrajectoryFilter::setInputSelectionSource(
    MTrajectorySelectionSource* s)
{
    inputSelectionSource = s;
    registerInputSource(inputSelectionSource);
    // Enable pass-through to input trajectory source if required request
    // keys are not specified.
    // Use inputSelectionSource instead of trajectorySource since otherwise
    // pass through will ignore the input selection source (no call to
    // produceData-method of inputSelectionSource). Data of trajectorySource
    // is used only to modify data of inputSelectionSource thus should not be
    // passed through by this filter.
    enablePassThrough(inputSelectionSource);
}


MTrajectorySelection* MBoundingBoxTrajectoryFilter::produceData(
    MDataRequest request)
{
    assert(inputSelectionSource != nullptr);

    MDataRequestHelper rh(request);

    QStringList args = rh.value(requestKeys[0]).split("/");

    rh.remove(requestKeys[0]);
    MTrajectories *trajectories = trajectorySource->getData(rh.request());
    MTrajectorySelection *inputSelection =
        inputSelectionSource->getData(rh.request());

    MWritableTrajectorySelection *filterResult =
        new MWritableTrajectorySelection(inputSelection->getNumTrajectories(),
                                         inputSelection->getTimes(),
                                         inputSelection->getStartGridStride());

    // Filtering is implemented by simply looping over all trajectories.
    int numInputTrajectories = inputSelection->getNumTrajectories();
    int j = 0;

    if (args.size() <= 1)
    {
        for (int i = 0; i < numInputTrajectories; i++)
        {
            int startIndex = inputSelection->getStartIndices()[i];

            filterResult->setStartIndex(j, startIndex);
            filterResult->setIndexCount(j, inputSelection->getIndexCount()[i]);
            j++;
        }

        trajectorySource->releaseData(trajectories);
        inputSelectionSource->releaseData(inputSelection);
        return filterResult;
    }

    // Compute the filter.
    float lonWest  = args[0].toFloat();
    float latSouth = args[1].toFloat();
    float lonEast  = lonWest + args[2].toFloat();
    float latNorth = latSouth + args[3].toFloat();
    float top_hPa = args[4].toFloat();
    float bottom_hPa = args[5].toFloat();

    for (int i = 0; i < numInputTrajectories; i++)
    {
        int startIndex = inputSelection->getStartIndices()[i];
        QVector3D p = trajectories->getVertices().at(startIndex);

        if (p.x() < lonWest) continue;
        if (p.x() > lonEast) continue;
        if (p.y() > latNorth) continue;
        if (p.y() < latSouth) continue;
        if (p.z() < top_hPa) continue;
        if (p.z() > bottom_hPa) continue;

        filterResult->setStartIndex(j, startIndex);
        filterResult->setIndexCount(j, inputSelection->getIndexCount()[i]);
        j++;
    }

    filterResult->decreaseNumSelectedTrajectories(j);

    trajectorySource->releaseData(trajectories);
    inputSelectionSource->releaseData(inputSelection);
    return filterResult;
}


MTask* MBoundingBoxTrajectoryFilter::createTaskGraph(MDataRequest request)
{
    MDataRequestHelper rh(request);
    rh.removeAllKeysExcept(requiredKeys());

    auto *task = new MTask(rh.request(), this);

    task->addParent(trajectorySource->getTaskGraph(rh.request()));
    task->addParent(inputSelectionSource->getTaskGraph(rh.request()));

    return task;
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // namespace Met3D