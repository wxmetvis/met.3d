/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef TRAJECTORYFILTER_H
#define TRAJECTORYFILTER_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "trajectories/source/trajectoryselectionsource.h"
#include "data/datarequest.h"
#include "trajectories/filtersettings.h"

namespace Met3D
{

/**
 * Base UI class for trajectory filter interfaces.
 */
class MTrajectoryFilterSettings : public MFilterSettings
{
public:
    MTrajectoryFilterSettings(QString filterType, MActor* actor, MProperty* parentProperty);
};

/**
  * Base class to all trajectory filters.
  */
class MTrajectoryFilter : public MTrajectorySelectionSource
{
    friend class MTrajectoryFilterFactory;

public:
    /**
     * Create a new trajectory filter given the base request keys of the
     * specific filter type. This creates unique data request keys for the
     * filter which can be accessed via @c locallyRequiredKeys().
     */
    MTrajectoryFilter(QStringList baseRequestKeys);
    ~MTrajectoryFilter() override;

    /**
     * @return The display name of the filter.
     */
    virtual QString getFilterName() = 0;

    /**
     * @return The base key names used for data requests for this filter. Note
     * that the actual data requests use the uniquely generated key given by
     * @c locallyRequiredKeys().
     */
    virtual QStringList getBaseRequestKeys() = 0;

    void setUI(MTrajectoryFilterSettings *pUi) { this->ui = pUi; }

    MTrajectoryFilterSettings* getUI() { return ui; }

    /**
     * Insert the current state of the filter (can include information from
     * the linked UI object) into the data request.
     * @param rh The data request helper to add the state to.
     */
    void insertCurrentStateToRequest(MDataRequestHelper& rh);

protected:
    const QStringList locallyRequiredKeys() override;

    // Keys used by this filter in data requests.
    QStringList requestKeys;
    MTrajectoryFilterSettings *ui;
};

} // namespace Met3D

#endif // TRAJECTORYFILTER_H
