/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
 *
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "thinouttrajectoryfilter.h"

// standard library imports
#include "assert.h"

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "trajectories/source/trajectorydatasource.h"


namespace Met3D
{
/**************************** User interface **********************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MThinOutTrajectoryFilterSettings::
    MThinOutTrajectoryFilterSettings(QString filterType, MActor *actor,
                                    MProperty *parentProperty)
    : MTrajectoryFilterSettings(std::move(filterType), actor, parentProperty)
{
    strideLonProp = MIntProperty("Lon stride", 1);
    strideLonProp.setConfigKey("lon_stride");
    strideLonProp.setMinimum(1);
    strideLonProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(strideLonProp);

    strideLatProp = MIntProperty("Lat stride", 1);
    strideLatProp.setConfigKey("lat_stride");
    strideLatProp.setMinimum(1);
    strideLatProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(strideLatProp);

    strideLevProp = MIntProperty("Lev stride", 1);
    strideLevProp.setConfigKey("lev_stride");
    strideLevProp.setMinimum(1);
    strideLevProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(strideLevProp);
}


MThinOutTrajectoryFilterSettings::~MThinOutTrajectoryFilterSettings()
= default;

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MThinOutTrajectoryFilterSettings::loadElementConfiguration(
    QSettings *settings)
{
    strideLonProp = settings->value("strideLon", 1).toInt();
    strideLatProp = settings->value("strideLat", 1).toInt();
    strideLevProp = settings->value("strideLev", 1).toInt();
}


int MThinOutTrajectoryFilterSettings::getStrideLon()
{
    return strideLonProp;
}


int MThinOutTrajectoryFilterSettings::getStrideLat()
{
    return strideLatProp;
}


int MThinOutTrajectoryFilterSettings::getStrideLev()
{
    return strideLevProp;
}


void MThinOutTrajectoryFilterSettings::insertCurrentStateToRequest(
    MDataRequestHelper &rh, const QStringList& keys)
{
    int strideLon = getStrideLon();
    int strideLat = getStrideLat();
    int strideLev = getStrideLev();
    rh.insert(keys[0], QString("%1/%2/%3").arg(strideLon)
                       .arg(strideLat).arg(strideLev));
}

/************************** Trajectory filter ********************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MThinOutTrajectoryFilter::MThinOutTrajectoryFilter()
    : MTrajectoryFilter(getBaseRequestKeys())
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MThinOutTrajectoryFilter::setTrajectorySource(MTrajectoryDataSource* s)
{
    // Enable pass-through to input trajectory source if required request
    // keys are not specified.
    MTrajectoryFilter::setTrajectorySource(s);
    enablePassThrough(s);
}


MTrajectorySelection* MThinOutTrajectoryFilter::produceData(
        MDataRequest request)
{
    // NOTE: The filter currently assumes a dimension index order "lat/lon/lev"
    // of the start grid positions of the trajectories. This is valid for the
    // Lagranto trajectories processed for the p(WCB) study, but cannot be
    // assumed in general! (mr, 10Feb2014).

    assert(trajectorySource != nullptr);

    MDataRequestHelper rh(request);

    QStringList args = rh.value(requestKeys[0]).split("/");

    rh.remove(requestKeys[0]);
    MTrajectories *trajectoryData = trajectorySource->getData(rh.request());
    MTrajectorySelection* input = inputSelectionSource->getData(rh.request());

    MWritableTrajectorySelection *filterResult =
            new MWritableTrajectorySelection(input->getNumTrajectories(),
                                             input->getTimes(),
                                             input->getStartGridStride());

    // Stride for lon/lat/lev.
    int thinoutLon = args[0].toInt();
    int thinoutLat = args[1].toInt();
    int thinoutLev = args[2].toInt();

    // Get start grid size from trajectory data item.
    std::shared_ptr<MStructuredGrid> startGrid = trajectoryData->getStartGrid();

    bool canCompute = true;
    if (! startGrid)
    {
        // No start grid information available. Forward all points.
        // This happens when the points where not started on a structured
        // grid, e.g., a vertical section. Print a warning for the user.
        LOG4CPLUS_WARN(mlog, "The thin-out filter in the trajectory pipeline "
                             "currently has no effect as no seed point source "
                             "has been sampled along a structured grid.");
        canCompute = false;
    }
    else if (startGrid->getNumValues()
        != static_cast<unsigned int>(trajectoryData->getNumTrajectories()))
    {
        // There is a start grid, but the number of trajectories does not match
        // the number of grid points. With the current thin out filter, it is
        // not possible to create the correct subset.
        LOG4CPLUS_WARN(mlog, "The thin-out filter in the trajectory pipeline "
                             "currently has no effect as the number of "
                             "trajectories do not match the size of the "
                             "grid they were sampled on. Trajectories have to "
                             "be started at all grid points of the start grid.");
        canCompute = false;
    }

    if (! canCompute)
    {
        // Can't compute a selection. Forward all points as new selection.
        for (int t = 0; t < input->getNumTrajectories(); t++)
        {
            filterResult->setStartIndex(t, input->getStartIndices()[t]);
            filterResult->setIndexCount(t, input->getIndexCount()[t]);
        }

        trajectorySource->releaseData(trajectoryData);
        inputSelectionSource->releaseData(input);
        return filterResult;
    }

    int nlon = startGrid->getNumLons();
    int nlat = startGrid->getNumLats();
    int nlev = startGrid->getNumLevels();
    int nlevnlon = nlev * nlon;

    QVector<GLint> selectionStartIndices(input->getStartIndices(),
                                         input->getStartIndices()
                                             + input->getNumTrajectories());

    // Index for selected trajectories.
    int j = 0;

    // Dimension order: lat/lon/lev (cf. notes mr, 10Feb2014).
    for (int ilat = 0; ilat < nlat; ilat += thinoutLat)
    {
        int itrajlat = nlevnlon * ilat;
        for (int ilon = 0; ilon < nlon; ilon += thinoutLon)
        {
            int itrajlon = itrajlat + nlev * ilon;
            for (int ilev = 0; ilev < nlev; ilev += thinoutLev)
            {
                int i = itrajlon + ilev;
                // Only if the current trajectory selection in the filter
                // pipeline contains an index present in the original
                // trajectory data, push the trajectory to the thinned selection.
                if (selectionStartIndices.contains(
                        trajectoryData->getStartIndices()[i]))
                {
                    filterResult->setStartIndex(
                        j, trajectoryData->getStartIndices()[i]);
                    filterResult->setIndexCount(
                        j, trajectoryData->getIndexCount()[i]);
                    j++;
                }
            }
        }
    }

    filterResult->setStartGridStride(
        QVector3D(thinoutLon, thinoutLat, thinoutLev));
    filterResult->decreaseNumSelectedTrajectories(j);
    // Debug output.
    // input->dumpStartVerticesToLog(300, filterResult);

    trajectorySource->releaseData(trajectoryData);
    inputSelectionSource->releaseData(input);
    return filterResult;
}


MTask* MThinOutTrajectoryFilter::createTaskGraph(MDataRequest request)
{
    assert(trajectorySource != nullptr);
    MDataRequestHelper rh(request);
    rh.removeAllKeysExcept(requiredKeys());

    MTask *task = new MTask(rh.request(), this);

    task->addParent(trajectorySource->getTaskGraph(rh.request()));
    task->addParent(inputSelectionSource->getTaskGraph(rh.request()));

    return task;
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

const QStringList MThinOutTrajectoryFilter::locallyRequiredKeys()
{
    return requestKeys;
}

} // namespace Met3D
