/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pressuretimetrajectoryfilter.h"

// standard library imports
#include <cassert>
#include <utility>

// related third party imports

// local application imports
#include "util/mutil.h"
#include "trajectories/source/deltapressurepertrajectory.h"

namespace Met3D
{

/**************************** User interface **********************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPressureTimeTrajectoryFilterSettings::
    MPressureTimeTrajectoryFilterSettings(QString filterType, MActor *actor,
                                            MProperty *parentProperty)
    : MTrajectoryFilterSettings(std::move(filterType), actor, parentProperty)
{
    ascentDescentMethodProp = MEnumProperty("Mode", {"Ascent", "Descent", "Ascent or descent", "Both ascent and descent"});
    ascentDescentMethodProp.setConfigKey("ascent_descent_mode");
    ascentDescentMethodProp.setTooltip(
            "Choose whether only ascending, only descending, either ascending or descending, "
            "or both ascending and descending trajectories should be kept.");
    ascentDescentMethodProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(ascentDescentMethodProp);

    deltaPressureFilterProp = MDoubleProperty("Ascent / Descent", 500.);
    deltaPressureFilterProp.setConfigKey("ascent_descent");
    deltaPressureFilterProp.setMinMax(1, 1050);
    deltaPressureFilterProp.setDecimals(2);
    deltaPressureFilterProp.setStep(5);
    deltaPressureFilterProp.setSuffix(" hPa");
    deltaPressureFilterProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(deltaPressureFilterProp);

    deltaTimeFilterProp = MIntProperty("Time interval", 48);
    deltaTimeFilterProp.setConfigKey("time_interval");
    deltaTimeFilterProp.setMinMax(1, 48);
    deltaTimeFilterProp.setSuffix(" hrs");
    deltaTimeFilterProp.registerValueCallback(this, &MPipelineElementSettings::elementChanged);
    groupProp.addSubProperty(deltaTimeFilterProp);
}


MPressureTimeTrajectoryFilterSettings::
    ~MPressureTimeTrajectoryFilterSettings()
= default;

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/


void MPressureTimeTrajectoryFilterSettings::loadElementConfiguration(
    QSettings *settings)
{
    deltaPressureFilterProp =
        settings->value(savePrefix + "deltaPressure", 500.).toDouble();

    deltaTimeFilterProp.setMinMax(0, 1000);
    deltaTimeFilterProp = settings->value(savePrefix + "deltaTime", 48.).toInt();
}


double MPressureTimeTrajectoryFilterSettings::getDeltaPressure_hPa()
{
    return deltaPressureFilterProp;
}


double MPressureTimeTrajectoryFilterSettings::getDeltaTimeHrs()
{
    return deltaTimeFilterProp;
}


void MPressureTimeTrajectoryFilterSettings::insertCurrentStateToRequest(
    MDataRequestHelper &rh, const QStringList& keys)
{
    double dp = getDeltaPressure_hPa();
    double dt = getDeltaTimeHrs();

    QString stateString = QString("%1/%2/%3").arg(dp).arg(dt).arg(ascentDescentMethodProp);

    rh.insert(keys[0], stateString);
}

/************************** Trajectory filter ********************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPressureTimeTrajectoryFilter::MPressureTimeTrajectoryFilter()
    : MTrajectoryFilter(getBaseRequestKeys())
{
    deltaPressureAscentSource = new MDeltaPressurePerTrajectorySource(
            MDeltaPressurePerTrajectorySource::ASCENT);
    deltaPressureDescentSource = new MDeltaPressurePerTrajectorySource(
            MDeltaPressurePerTrajectorySource::DESCENT);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MTrajectorySelection *
MPressureTimeTrajectoryFilter::produceData(MDataRequest request)
{
    assert(inputSelectionSource != nullptr);
    assert(deltaPressureAscentSource != nullptr);
    assert(deltaPressureDescentSource != nullptr);

    MDataRequestHelper rh(request);

    QStringList args = rh.value(requestKeys[0]).split("/");

    rh.remove(requestKeys[0]);
    MTrajectorySelection *input = inputSelectionSource->getData(rh.request());

    auto *filterResult = new MWritableTrajectorySelection(
        input->getNumTrajectories(), input->getTimes(),
        input->getStartGridStride());

    float deltaPressure_hPa = args[0].toFloat();
    int deltaTime_hrs = args[1].toInt();
    int filterModeIdx = args[2].toInt();
    AscentDescentFilterMode filterMode = static_cast<AscentDescentFilterMode>(filterModeIdx);

    rh.insert("MAX_DELTA_PRESSURE_HOURS", deltaTime_hrs);

    // Get the maximum delta pressure per trajectory from the parent sources
    // if applicable, and extract the data.
    MFloatPerTrajectorySupplement *ascentDPPerTrajectory = nullptr;
    MFloatPerTrajectorySupplement *descentDPPerTrajectory = nullptr;
    QVector<float> ascentDeltaPressure;
    QVector<float> descentDeltaPressure;

    if (ascentRequired(filterMode))
    {
        ascentDPPerTrajectory = deltaPressureAscentSource->getData(rh.request());
        ascentDeltaPressure = ascentDPPerTrajectory->getValues();
    }
    if (descentRequired(filterMode))
    {
        descentDPPerTrajectory = deltaPressureDescentSource->getData(rh.request());
        descentDeltaPressure = descentDPPerTrajectory->getValues();
    }

    // Filtering is implemented by simply looping over all trajectories.
    int numTrajectories = input->getNumTrajectories();
    int numTimes = input->getNumTimeStepsPerTrajectory();
    int j = 0;

    for (int i = 0; i < numTrajectories; i++)
    {
        // i = index of trajectory in input selection
        // trajectoryIndex = index of trajectory in "full" dataset
        int startIndex = input->getStartIndices()[i];
        // TODO (cf, 2025-01-29): The following division presumes that all trajectories have
        //     the same number of vertices. This is currently the case due to the implementation,
        //     but it is not an optimal approach since shorter trajectories are unnecessarily
        //     padded with invalid values to match the length of the longest trajectory.
        //     For now, the division by numTimes assumes uniformity in trajectory length
        //     to derive the original trajectory index.
        int trajectoryIndex = startIndex / numTimes;
        if (startIndex % numTimes)
        {
            // Round up in case division is not exact.
            trajectoryIndex += 1;
        }

        // Depending on mode (ascent, descent, either or, both), check if
        // the threshold is fulfilled.
        bool acceptTrajectory = false;
        if (filterMode == ASCENT && ascentDeltaPressure.at(trajectoryIndex) >= deltaPressure_hPa)
        {
            acceptTrajectory = true;
        }
        else if (filterMode == DESCENT && descentDeltaPressure.at(trajectoryIndex) >= deltaPressure_hPa)
        {
            acceptTrajectory = true;
        }
        else if (filterMode == EITHER && (ascentDeltaPressure.at(trajectoryIndex) >= deltaPressure_hPa ||
                                                   descentDeltaPressure.at(trajectoryIndex) >= deltaPressure_hPa))
        {
            acceptTrajectory = true;
        }
        else if (filterMode == BOTH && (ascentDeltaPressure.at(trajectoryIndex) >= deltaPressure_hPa &&
                                                 descentDeltaPressure.at(trajectoryIndex) >= deltaPressure_hPa))
        {
            acceptTrajectory = true;
        }

        // Accept: add the trajectory to the filtered result.
        if (acceptTrajectory)
        {
            filterResult->setStartIndex(j, startIndex);
            filterResult->setIndexCount(j, input->getIndexCount()[i]);
            j++;
        }
    }

    filterResult->decreaseNumSelectedTrajectories(j);

    // Release data items.
    if (ascentDPPerTrajectory)
    {
        deltaPressureAscentSource->releaseData(ascentDPPerTrajectory);
    }
    if (descentDPPerTrajectory)
    {
        deltaPressureDescentSource->releaseData(descentDPPerTrajectory);
    }
    inputSelectionSource->releaseData(input);
    return filterResult;
}


MTask *MPressureTimeTrajectoryFilter::createTaskGraph(MDataRequest request)
{
    MDataRequestHelper rh(request);
    rh.removeAllKeysExcept(requiredKeys());

    auto *task = new MTask(rh.request(), this);

    // Add dependencies.
    QStringList args = rh.value(requestKeys[0]).split("/");
    int deltaTime_hrs = args[1].toInt();
    AscentDescentFilterMode ascentDescentMode = static_cast<AscentDescentFilterMode>(args[2].toInt());
    rh.insert("MAX_DELTA_PRESSURE_HOURS", deltaTime_hrs);

    // Add ascent and/or descent source as parent.
    if (ascentRequired(ascentDescentMode))
    {
        task->addParent(deltaPressureAscentSource->getTaskGraph(rh.request()));
    }
    if (descentRequired(ascentDescentMode))
    {
        task->addParent(deltaPressureDescentSource->getTaskGraph(rh.request()));
    }
    rh.remove("MAX_DELTA_PRESSURE_HOURS");

    task->addParent(inputSelectionSource->getTaskGraph(rh.request()));

    return task;
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MPressureTimeTrajectoryFilter::setTrajectorySource(
    MTrajectoryDataSource *source)
{
    MTrajectorySupplementDataSource::setTrajectorySource(source);

    deltaPressureAscentSource->setTrajectorySource(source);
    deltaPressureDescentSource->setTrajectorySource(source);

    if (! deltaPressureAscentSource->getMemoryManager())
    {
        deltaPressureAscentSource->setMemoryManager(memoryManager);
        deltaPressureDescentSource->setMemoryManager(memoryManager);
    }
}

} // namespace Met3D
