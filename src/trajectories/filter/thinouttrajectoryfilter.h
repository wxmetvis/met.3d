/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef THINOUTTRAJECTORYFILTER_H
#define THINOUTTRAJECTORYFILTER_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/datarequest.h"
#include "trajectoryfilter.h"
#include "trajectories/data/trajectories.h"

namespace Met3D
{

/**
 * UI class for the @c MThinOutTrajectoryFilter. It contains properties
 * to set the delta pressure and delta time settings.
 */
class MThinOutTrajectoryFilterSettings : public MTrajectoryFilterSettings
{
public:
    MThinOutTrajectoryFilterSettings(QString filterType, MActor *actor,
                                    MProperty *parentProperty);

    ~MThinOutTrajectoryFilterSettings() override;

    void loadElementConfiguration(QSettings *settings) override;

    void insertCurrentStateToRequest(MDataRequestHelper &rh,
                                     const QStringList &keys) override;

    /**
     * @return The currently set stride along longitudes.
     */
    int getStrideLon();

    /**
     * @return The currently set stride along latitudes.
     */
    int getStrideLat();

    /**
     * @return The currently set stride along pressure levels.
     */
    int getStrideLev();

private:
    MIntProperty strideLonProp;
    MIntProperty strideLatProp;
    MIntProperty strideLevProp;
};


/**
  @brief MThinOutTrajectoryFilter "thins out" a set of trajectories with
  respect to spatial resolution (e.g. select every n-th trajectory from the
  start grid in any direction).
  */
class MThinOutTrajectoryFilter : public MTrajectoryFilter
{
public:
    MThinOutTrajectoryFilter();

    void setTrajectorySource(MTrajectoryDataSource* s) override;

    MTrajectorySelection* produceData(MDataRequest request) override;

    MTask* createTaskGraph(MDataRequest request) override;

    QString getFilterName() override { return "Thin Out Trajectory Filter"; }

    static QString getStaticFilterName() { return "Thin Out Trajectory Filter"; };

    QStringList getBaseRequestKeys() override { return {"THINOUT_STRIDE"}; }

protected:
    const QStringList locallyRequiredKeys();

};

} // namespace Met3D

#endif // THINOUTTRAJECTORYFILTER_H
