/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015 Marc Rautenhaus
**
**  Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef PRESSURETIMETRAJECTORYFILTER_H
#define PRESSURETIMETRAJECTORYFILTER_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "trajectoryfilter.h"

namespace Met3D
{
class MDeltaPressurePerTrajectorySource;

/**
 * UI class for the @c MPressureTimeTrajectoryFilter. It contains properties
 * to set the delta pressure and delta time settings.
 */
class MPressureTimeTrajectoryFilterSettings : public MTrajectoryFilterSettings
{
public:
    MPressureTimeTrajectoryFilterSettings(QString filterType, MActor *actor,
                                    MProperty *parentProperty);

    ~MPressureTimeTrajectoryFilterSettings() override;

    void loadElementConfiguration(QSettings *settings) override;

    void insertCurrentStateToRequest(MDataRequestHelper &rh,
                                     const QStringList &keys) override;

    /**
     * @return The currently set delta pressure threshold.
     */
    double getDeltaPressure_hPa();

    /**
     * @return The currently set delta time window.
     */
    double getDeltaTimeHrs();

private:
    MDoubleProperty deltaPressureFilterProp;
    MIntProperty deltaTimeFilterProp;
    MEnumProperty ascentDescentMethodProp;
};

/**
  @brief Selects trajectories according to the delta pressure/delta time
  (dp/dt) criterion.
  */
class MPressureTimeTrajectoryFilter : public MTrajectoryFilter
{
public:
    MPressureTimeTrajectoryFilter();

    MTrajectorySelection *produceData(MDataRequest request) override;

    MTask *createTaskGraph(MDataRequest request) override;

    QString getFilterName() override { return "Trajectory Ascent/Descent Filter"; }

    static QString getStaticFilterName() { return "Trajectory Ascent/Descent Filter"; };

    void setTrajectorySource(MTrajectoryDataSource *source) override;

    QStringList getBaseRequestKeys() override { return {"TRAJ_ASCENT"}; }

private:
    enum AscentDescentFilterMode
    {
        ASCENT = 0, DESCENT = 1, EITHER = 2, BOTH = 3
    };

    /**
     * Whether the ascent source is required to filter trajectories with
     * the given @p mode.
     */
    inline bool ascentRequired(AscentDescentFilterMode mode)
    { return mode != DESCENT; }

    /**
     * Whether the descent source is required to filter trajectories with
     * the given @p mode.
     */
    inline bool descentRequired(AscentDescentFilterMode mode)
    { return mode != ASCENT; }

    MDeltaPressurePerTrajectorySource *deltaPressureAscentSource;
    MDeltaPressurePerTrajectorySource *deltaPressureDescentSource;
};

} // namespace Met3D

#endif // PRESSURETIMETRAJECTORYFILTER_H
