/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_TRAJECTORYFILTERFACTORY_H
#define MET_3D_TRAJECTORYFILTERFACTORY_H

// standard library imports
#include <functional>

// related third party imports
#include <QStringList>

// local application imports
#include "trajectoryfilter.h"

namespace Met3D
{

/**
 * This factory creates trajectory filters (@c MTrajectoryFilter), as well
 * as corresponding UI objects.
 */
class MTrajectoryFilterFactory
{
public:
    /**
     * @return The factory instance.
     */
    static MTrajectoryFilterFactory* getInstance();

    /**
     * Creates a new trajectory filter.
     * @param filterName The name of the filter used for registration, see
     * @c MTrajectoryFilter::getFilterName().
     * @return The newly created filter.
     */
    MTrajectoryFilter* create(const QString& filterName);

    /**
     * Creates an UI object for the given filter type.
     * @param filterName The name of the filter used for registration, see
     * @c MPointFilter::getFilterName().
     * @param actor Reference to the actor where to add the properties to.
     * @param parent The parent property to attach the UI to.
     * @return The newly created UI object.
     */
    MTrajectoryFilterSettings* createUI(const QString& filterName, MActor* actor, MProperty* parent);

    /**
     * @return The list of registered filter names.
     */
    QStringList getRegisteredFilterNames() const;

    /**
     * @return The list of already reserved filter request identifiers.
     */
    QStringList getReservedFilterRequestIDs() const;

    /**
     * Opens the dialog to select a registered trajectory filter.
     * @return The name of the trajectory filter selected, empty if None.
     */
    static QString showCreationDialog();

private:
    MTrajectoryFilterFactory();
    static MTrajectoryFilterFactory* instance;

    // Map of creators to construct trajectory filters by name.
    QMap<QString, std::function<MTrajectoryFilter*()>> creators;
    // Map of creators to construct trajectory filter UIs by name.
    QMap<QString, std::function<MTrajectoryFilterSettings*(MActor*, MProperty*)>> uiCreators;

    // List of reserved request IDs. We have to make sure that we do not reuse
    // request IDs even after deletion of filters.
    QStringList reservedRequestKeys;

    /**
     * Register a type of trajectory filter to the factory, so it can be created
     * from the trajectory filter dialog.
     * @tparam T The type (class) of the filter to register, must inherit from
     * the @c MTrajectoryFilter class.
     * @tparam U The type (class) of filter UI to register, must inherit from
     * the @c MTrajectoryFilterSettings class.
     */
    template<typename T, typename U>
    void registerTrajectoryFilter() {
        static_assert(std::is_base_of<MTrajectoryFilter, T>::value,
                      "T must inherit from MTrajectoryFilter");
        static_assert(std::is_base_of<MTrajectoryFilterSettings, U>::value,
                      "U must inherit from MTrajectoryFilterSettings");

        creators[T::getStaticFilterName()] = []()
        {
            T* filter = new T();
            auto trajectoryFilter = dynamic_cast<MTrajectoryFilter*>(filter);

            getInstance()->reservedRequestKeys.append(
                trajectoryFilter->locallyRequiredKeys());
            return filter;
        };

        uiCreators[T::getStaticFilterName()] =
            [](MActor *actor, MProperty *parent)
        {
            U *ui = new U(T::getStaticFilterName(), actor, parent);
            return ui;
        };
    }

};

} // namespace Met3D

#endif //MET_3D_TRAJECTORYFILTERFACTORY_H
