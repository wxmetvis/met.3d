/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectoryfilter.h"

// standard library imports

// related third party imports

// local application imports
#include "trajectoryfilterfactory.h"
#include "util/mutil.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryFilter::MTrajectoryFilter(QStringList baseRequestKeys)
    : MTrajectorySelectionSource(),
      ui(nullptr)
{
    // Get the list of already reserved filter data request IDs.
    QStringList reservedFilterKeys = MTrajectoryFilterFactory::getInstance()
                                      ->getReservedFilterRequestIDs();

    requestKeys = baseRequestKeys;
    // If one of the filter request keys already exist, pick a different one.
    for (int i = 0; i < requestKeys.size(); i++)
    {
        // Check if the base request key is already in use: then, add _1 or _2..
        if (reservedFilterKeys.contains(requestKeys[i]))
        {
            int iter = 0;
            do
            {
                iter += 1;
                requestKeys[i] = QString(baseRequestKeys[i] + "_%1").arg(iter);
            } while (reservedFilterKeys.contains(requestKeys[i]));
        }
    }
}


MTrajectoryFilter::~MTrajectoryFilter()
{
}


MTrajectoryFilterSettings::MTrajectoryFilterSettings(QString filterType, MActor *actor,
                                         MProperty *parentProperty)
    : MFilterSettings(filterType, actor, parentProperty)
{
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

const QStringList MTrajectoryFilter::locallyRequiredKeys()
{
    return requestKeys;
}


void MTrajectoryFilter::insertCurrentStateToRequest(MDataRequestHelper &rh)
{
    ui->insertCurrentStateToRequest(rh, requestKeys);
}

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

} // namespace Met3D
