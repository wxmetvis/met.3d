/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "trajectoryfilterfactory.h"

// standard library imports

// related third party imports

// local application imports
#include "bboxtrajectoryfilter.h"
#include "pressuretimetrajectoryfilter.h"
#include "thinouttrajectoryfilter.h"
#include "gxfw/addtrajectoryfilterdialog.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTrajectoryFilterFactory::MTrajectoryFilterFactory()
{
    registerTrajectoryFilter<MBoundingBoxTrajectoryFilter, MBoundingBoxTrajectoryFilterSettings>();
    registerTrajectoryFilter<MPressureTimeTrajectoryFilter, MPressureTimeTrajectoryFilterSettings>();
    registerTrajectoryFilter<MThinOutTrajectoryFilter, MThinOutTrajectoryFilterSettings>();
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MTrajectoryFilterFactory* MTrajectoryFilterFactory::instance = nullptr;


MTrajectoryFilterFactory *MTrajectoryFilterFactory::getInstance()
{
    if (! MTrajectoryFilterFactory::instance)
    {
        MTrajectoryFilterFactory::instance = new MTrajectoryFilterFactory();
    }
    return MTrajectoryFilterFactory::instance;
}


MTrajectoryFilter *MTrajectoryFilterFactory::create(const QString &filterName)
{
    if (! creators.contains(filterName))
    {
        return nullptr; // Filter not registered.
    }
    // Return new instance.
    return creators[filterName]();
}


MTrajectoryFilterSettings *
MTrajectoryFilterFactory::createUI(const QString &filterName, MActor *actor,
                                   MProperty *parent)
{
    if (! creators.contains(filterName))
    {
        return nullptr; // Filter not registered.
    }
    // Return new instance.
    return uiCreators[filterName](actor, parent);
}


QStringList MTrajectoryFilterFactory::getReservedFilterRequestIDs() const
{
    return reservedRequestKeys;
}


QString MTrajectoryFilterFactory::showCreationDialog()
{
    MAddTrajectoryFilterDialog dialog;
    if (dialog.exec() == QDialog::Rejected)
    {
        return "";
    }

    QString type = dialog.getSelectedFilterType();
    return type;
}


QStringList MTrajectoryFilterFactory::getRegisteredFilterNames() const
{
    return creators.keys();
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // namespace Met3D