/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2017-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2024      Christoph Fischer [*]
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MET_3D_TRAJECTORYREQUESTDISPATCHER_H
#define MET_3D_TRAJECTORYREQUESTDISPATCHER_H

// standard library imports

// related third party imports
#include <QtCore>

// local application imports
#include "data/scheduleddatasource.h"
#include "source/trajectoryreader.h"
#include "gxfw/synccontrol.h"
#include "trajectorycomputation.h"
#include "data/trajectorydatacollection.h"

namespace Met3D
{
class MTrajectoryComputationSource;
class MTrajectoryPipelineManager;
class MTrajectoryDataCollectionGroup;
class MTrajectoryFilterSettings;
class MPointFilterSettings;
class MPointGeneratorInterfaceSettings;

enum MTrajectoryMode : unsigned short
{
    PRECOMPUTED,
    COMPUTED_IN_MET3D
};

/** Render mode (tubes, spheres, etc.). */
enum MTrajectoryRenderType
{
    TRAJECTORY_TUBES = 0,
    ALL_POSITION_SPHERES = 1,
    SINGLETIME_POSITIONS = 2,
    TUBES_AND_SINGLETIME = 3,
    BACKWARDTUBES_AND_SINGLETIME = 4
};

/**
 * Struct containing required information to dispatch a trajectory computation
 * request.
 */
class MTrajectoryDispatchInfo
{
public:
    QDateTime initTime;
    QDateTime validTime;
    int member;
    QString timeSpan;
    QDateTime endTimeForward;
    QDateTime endTimeBackward;
    MTrajectoryComputationLineType lineType;
    MTrajectoryComputationIntegrationMethod integrationMethod;
    MTrajectoryComputationInterpolationMethod interpolationMethod;
    int numSubTimeSteps;
    bool vertexAtSubTimeSteps;
    double streamlineDeltaS;
    int streamlineLength;
    MTrajectoryRenderType renderMode;
    int particlePosTimeStep;

    // Per scene view
    QMap<QString, QVector2D> pressureToWorldZNormalsPerView;
};

class MTrajectoryActor;

/**
  * The MTrajectoryRequestDispatcher handles and dispatches the execution
  * of the trajectory pipeline. It also serves as data source, which collects
  * all returned data from the trajectory pipelines, and returns itself a
  * collection of these data objects. Therefore, it serves as "barrier", so
  * the trajectory actor can get informed once all pipelines finished their
  * execution. One pipeline is needed per point source for trajectories computed
  * within Met.3D. Otherwise, computations gets re-triggered for all point
  * sources if one point source changed.
  */
class MTrajectoryRequestDispatcher : public MScheduledDataSource
{
    Q_OBJECT
public:
    MTrajectoryRequestDispatcher();

    ~MTrajectoryRequestDispatcher() override;

    /** Implement data source interface */
    MTrajectoryDataCollectionGroup *getData(MDataRequest request) override;

    MTrajectoryDataCollectionGroup *produceData(MDataRequest request) override;

    const QStringList locallyRequiredKeys() override;

    MTask *createTaskGraph(MDataRequest request) override;

    /**
     * @return True if the dispatcher is initialized, thus, if trajectories are
     * precomputed, that the data source is initialized, and if they are
     * computed in Met.3D, that the required variables are present.
     */
    bool isInitialized() const;

    /**
     * Dispatch a new request to compute (or fetch) trajectories. Once the
     * dispatch is finished and the data is available, the signal
     * @c dispatchCompleted is triggered.
     * @param dispatchInfo The for the computation required information.
     */
    void dispatch(MTrajectoryDispatchInfo& dispatchInfo);

    /**
     * Build a request from the given dispatch info struct and the current state
     * of the dispatcher.
     * @return The data request.
     */
    MDataRequest
    buildRequestFromDispatchInfo(MTrajectoryDispatchInfo &dispatchInfo);

    /**
     * Update the mode of trajectories (precomputed, in Met.3D computed).
     * @param mode The mode to change to.
     */
    void updateSourceMode(MTrajectoryMode mode);

    /**
     * Set the @p identifier of the precomputed data source.
     */
    void setPrecomputedDataSource(const QString& identifier);

    /**
     * Set the precomputed data source.
     * @param source The precomputed trajectory data source.
     */
    void setPrecomputedDataSource(MTrajectoryReader *source);

    /**
     * Set the u wind component.
     * @param var The wind component.
     */
    void setWindVariableU(MNWPActorVariable *var);

    /**
     * Set the v wind component.
     * @param var The wind component.
     */
    void setWindVariableV(MNWPActorVariable *var);

    /**
     * Set the w wind component.
     * @param var The wind component.
     */
    void setWindVariableW(MNWPActorVariable *var);

    /**
     * Set the auxiliary data variable used for rendering.
     * @param var The auxiliary data variable.
     */
    void setRenderingAuxiliaryVariable(MNWPActorVariable *var);

    /**
     * Add a new pipeline from the given frontend settings for trajectories
     * computed in Met.3D.
     * @param pointSourceUI The point source settings.
     * @param pointFilterUIs List of point filter settings.
     * @param trajectoryFilterUIs List of trajectory filter settings.
     */
    void addPipelineForMet3DTrajectories(MPointGeneratorInterfaceSettings *pointSourceUI,
                  const QList<MPointFilterSettings *>& pointFilterUIs,
                  const QList<MTrajectoryFilterSettings *>& trajectoryFilterUIs);

    /**
     * Rebuild the pipelines from the given frontend settings. This deletes
     * the trajectory pipelines and initializes new ones. An overloaded method
     * just initializes the trajectory filters, useful for precomputed data
     * sources.
     * @param pointSourceUIs The point source settings.
     * @param pointFilterUIs List of point filter settings.
     * @param trajectoryFilterUIs List of trajectory filter settings.
     */
    void rebuild(const QList<MPointGeneratorInterfaceSettings *>& pointSourceUIs,
                 const QList<MPointFilterSettings *>& pointFilterUIs,
                 const QList<MTrajectoryFilterSettings *>& trajectoryFilterUIs);
    void rebuild(const QList<MTrajectoryFilterSettings *>& trajectoryFilterUIs);

    /**
     * Add a point filter to the currently set up pipelines.
     * @param ui The settings of the point filter.
     */
    void addPointFilter(MPointFilterSettings *ui);

    /**
     * Add a trajectory filter to the currently set up pipelines.
     * @param ui The settings of the trajectory filter.
     */
    void addTrajectoryFilter(MTrajectoryFilterSettings *ui);

    /**
     * Remove a point source by index in the list of point sources. Since a
     * point source defines the start of a pipeline, this also removes this
     * specific pipeline.
     * @param index The index to remove.
     */
    void removePointSourceByIndex(int index);

    /**
     * Remove a point filter by index in the list of point filters from all
     * pipelines.
     * @param index The index to remove.
     */
    void removePointFilterByIndex(int index);

    /**
     * Remove a trajectory filter by index in the list of point filters from all
     * pipelines.
     * @param index The index to remove.
     */
    void removeTrajectoryFilterByIndex(int index);

    /**
     * Update all pipelines. This updates the connections between pipeline
     * modules.
     */
    void updatePipelines(bool emitChangedSignal = true);

    /**
     * Delete all pipelines and related objects.
     */
    void deletePipelines();

    /**
      * @return A @c QList<QDateTime> containing the available forecast
      * initialisation times (base times).
      */
    QList<QDateTime> availableInitTimes();

    /**
     * @return a @c QList<QDateTime> containing the trajectory start times
     * available for the specified initialisation time @p initTime.
     * @param initTime The initialisation time.
     */
    QList<QDateTime> availableValidTimes(const QDateTime &initTime);

    /**
     * For a given init and valid time, returns the valid (=start) times of
     * those trajectories that overlap with the given valid time.
     */
    QList<QDateTime> validTimeOverlap(const QDateTime &initTime,
                                      const QDateTime &validTime);

    /**
     * @return The set of available ensemble members.
     */
    QSet<unsigned int> availableEnsembleMembers();

    /**
     * @return The list of auxiliary variables in the current trajectory mode.
     */
    QStringList availableAuxiliaryVariables();

    /**
     * Toggles the computation setting related to advancing both init and
     * valid times when integrating time for a trajectory.
     * @param enabled If set to True, both init and valid times will be
     * increased when integrating time for a trajectory. If False (default
     * setting), only valid time will be increased. The toggle should be enabled
     * when a data set has the same init and valid time for each grid,
     * e.g. for certain reanalysis data.
     */
    void setIntegrateInitAndValidTime(bool enabled);

    /**
     * @return The list of currently set wind variables if trajectories are
     * computed in Met.3D.
     */
    QList<MNWPActorVariable *> getInputWindVariables();

    /**
     * Used by the computation instance to get a reference to the actor
     * variable given the @p varName variable name specified in a data request.
     * @param varName The variable name.
     * @return A reference to the actor variable.
     */
    MNWPActorVariable* getInputVariable(const QString& varName);

    MTrajectoryInitTimeMap getAvailableTrajectoriesToCompute()
    { return availableTrajectories; }

signals:
    /**
     * Triggered whenever a new @p data item is available from a dispatch.
     */
    void dispatchCompleted(MTrajectoryDataCollectionGroup *data);

    /**
     * Triggered whenever the state of one pipeline changes.
     */
    void pipelineChangedSignal();

public slots:
    /**
     * Return slot of the data request called by @c dataRequestCompleted().
     * @param request The request which has been completed.
     */
    void trajectoriesAvailable(MDataRequest request);

protected:
    /**
     * Updates the available trajectories from the data source.
     */
    void updateAvailableTrajectories();

private:
    // Actor variables used for computation.
    MNWPActorVariable *windEastwardVariable;
    MNWPActorVariable *windNorthwardVariable;
    MNWPActorVariable *windVerticalVariable;
    MNWPActorVariable *auxRenderingVariable;

    // Reference to the precomputed data source if mode is used.
    MTrajectoryReader *precomputedDataSource;

    // Whether to integrate over both init and valid time.
    bool integrateInitAndValidTime;

    MTrajectoryMode mode;
    QList<MTrajectoryPipelineManager *> pipelines;

    // Dictionaries of available trajectory data. Access needs to be protected
    // by the provided read/write lock.
    MTrajectoryInitTimeMap availableTrajectories;
    QSet<unsigned int> availableMembers;
    QReadWriteLock availableItemsLock;
};

} // namespace Met3D

#endif // MET_3D_TRAJECTORYREQUESTDISPATCHER_H
