/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Thorwin Vogt
**  Copyright 2024      Susanne Fuchs
**
**  Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPIPELINECONFIGURATIONINFO_H
#define MET_3D_MPIPELINECONFIGURATIONINFO_H

// standard library imports

// related third party imports
#include <QSettings>

// local application imports

namespace Met3D
{

enum MNWPReaderFileFormat
{
    INVALID_FORMAT = 0,
    CF_NETCDF = 1,
    GRIB = 2,
    HDF5 = 3
};

enum PipelineType
{
    INVALID_PIPELINE_TYPE = 0,
    NWP_PIPELINE = 1,
    TRAJECTORIES_PIPELINE = 2,
    RADAR_PIPELINE = 3
};

/**
 * Class defining an abstract user configurable pipeline configuration.
 * The MAddDatasetDialog class represents a UI to interactively edit these pipeline configurations.
 * They can be saved to or loaded from file.
 * @see MAddDatasetDialog
 */
class MAbstractPipelineConfigurationInfo
{
public:
    /**
     * Save this pipeline configuration to the provided QSettings object.
     * @param settings Settings object to save to
     */
    virtual void saveConfiguration(QSettings *settings) = 0;

    /**
     * Load this pipeline configuration from the provided QSettings object.
     * @param settings Settings object to load from
     */
    virtual void loadConfiguration(QSettings *settings) = 0;

    /**
     * Load this pipeline configuration from the provided file.
     * @param filename Path to the configuration file.
     */
    bool loadConfigurationFromFile(const QString &filename);

    /**
     * Save this pipeline configuration to the provided file.
     * @param filename Path to the configuration file.
     */
    void saveConfigurationToFile(const QString &filename);

    /**
     * Check, if the provided QSettings object is a valid configuration
     * of this pipeline configuration.
     * @param settings QSettings object to test.
     * @return true, if valid, otherwise false.
     */
    virtual bool isValidConfiguration(QSettings *settings) = 0;

    /**
    * Returns the type of the pipeline
    * @return Type of pipeline.
    */
    virtual PipelineType getFormat()
    { return PipelineType::INVALID_PIPELINE_TYPE; }

    bool addTimesAndMembersToSyncControl;
};

/**
 * Implementation of MAbstractPipelineConfigurationInfo for NWP Pipelines.
 */
class MNWPPipelineConfigurationInfo : public MAbstractPipelineConfigurationInfo
{
public:
    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    bool isValidConfiguration(QSettings *settings) override;

    PipelineType getFormat() override;

    QString getNWPFormatString() const;

    /**
     * Return the corresponding NWPReaderFileFormat from the given @p text.
     * @param text String to get the corresponding NWP file format from.
     * @return The MNWPReaderFileFormat corresponding to the given text.
     */
    static MNWPReaderFileFormat getNWPFormatFromString(const QString& text);

    /**
     * Checks, if the provided QSettings object is a MNWPPipelineConfigurationInfo.
     * @param settings The QSettings object to test.
     * @return True, if valid, otherwise false.
     */
    static bool isNWPConfig(QSettings *settings);

    // Members

    QString name;
    QString fileDir;
    QString fileFilter;
    QString memoryManagerID;
    MNWPReaderFileFormat dataFormat;
    bool enableRegridding;
    bool enableProbabiltyRegionFilter;
    bool treatRotatedGridAsRegularGrid;
    bool treatProjectedGridAsRegularLonLatGrid;
    QString surfacePressureFieldType;
    bool convertGeometricHeightToPressure_ICAOStandard;
    QString auxiliary3DPressureField;
    QString consistencyCheckReferenceVariables;
    bool disableGridConsistencyCheck;
    QMap<QString, QString> inputVarsForDerivedVars;
};

/**
 * Implementation of MAbstractPipelineConfigurationInfo for Trajectory Pipelines.
 */
class MTrajectoriesPipelineConfigurationInfo : public MAbstractPipelineConfigurationInfo
{
public:
    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    bool isValidConfiguration(QSettings *settings) override;

    PipelineType getFormat() override;

    /**
     * Checks, if the provided QSettings object is a MTrajectoriesPipelineConfigurationInfo.
     * @param settings The QSettings object to test.
     * @return True, if valid, otherwise false.
     */
    static bool isTrajectoryConfig(QSettings *settings);

    // Members

    QString name;
    bool isEnsemble;
    QString fileDir;
    QString memoryManagerID;
    bool boundaryLayerTrajectories;
};


/**
 * Implementation of MAbstractPipelineConfigurationInfo for Radar Pipelines.
 */
class MRadarPipelineConfigurationInfo : public MAbstractPipelineConfigurationInfo
{
public:
    void saveConfiguration(QSettings *settings) override;

    void loadConfiguration(QSettings *settings) override;

    bool isValidConfiguration(QSettings *settings) override;

    PipelineType getFormat() override;

    /**
     * Checks, if the provided QSettings object is a MRadarPipelineConfigurationInfo.
     * @param settings The QSettings object to test.
     * @return True, if valid, otherwise false.
     */
    static bool isRadarConfig(QSettings *settings);

    // Members

    QString name;
    QString fileDir;
    QString fileFilter;
    int standardAzimuthGridsize;
    QString memoryManagerID;
};


}

#endif //MET_3D_MPIPELINECONFIGURATIONINFO_H
