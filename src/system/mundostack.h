/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MUNDOSTACK_H
#define MET_3D_MUNDOSTACK_H

// standard library imports

// related third party imports
#include <QUndoStack>

// local application imports

// forward declarations

namespace Met3D
{

/**
 * The undo stack for Met.3D.
 * Essentially just a wrapper around @c QUndoStack implemented as a singleton class,
 * so that all parts of the application can access it.
 */
class MUndoStack : public QUndoStack
{
public:
    /**
     * Get or create the instance of the undo stack.
     * @return The undo stack for Met.3D
     */
    static MUndoStack *getInstance();

    /**
     * Push an undoable command to the undo stack and execute it.
     * @param cmd The command to push.
     */
    static void run(QUndoCommand *cmd);
private:
    MUndoStack();
    static MUndoStack *instance;
};

} // Met3D

#endif //MET_3D_MUNDOSTACK_H
