/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Thorwin Vogt
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mundostack.h"

// standard library imports

// related third party imports

// local application imports

namespace Met3D
{

MUndoStack *MUndoStack::instance = nullptr;

MUndoStack::MUndoStack() : QUndoStack(nullptr)
{}


MUndoStack *MUndoStack::getInstance()
{
    if (instance == nullptr)
    {
        instance = new MUndoStack();
    }
    return instance;
}


void MUndoStack::run(QUndoCommand *cmd)
{
    getInstance()->push(cmd);
}
} // Met3D