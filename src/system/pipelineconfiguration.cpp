/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015-2020 Marc Rautenhaus [*, previously +]
**  Copyright 2017-2018 Bianca Tost [+]
**  Copyright 2017      Philipp Kaiser [+]
**  Copyright 2020      Marcel Meyer [*]
**  Copyright 2020-2023 Andreas Beckert [*]
**  Copyright 2023-2024 Thorwin Vogt [*]
**  Copyright 2024      Susanne Fuchs [*]
**
**  * Regional Computing Center, Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "pipelineconfiguration.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "util/mfileutils.h"
#include "util/mfiletypes.h"
#include "system/scheduling/scheduler.h"
#include "data/lrumemorymanager.h"
#include "gxfw/msystemcontrol.h"
#include "mainwindow.h"
#include "data/waypoints/waypointstablemodel.h"

#include "data/climateforecastreader.h"
#include "data/derivedvars/derivedmetvarsdatasource.h"
#include "data/differencedatasource.h"
#include "data/gribreader.h"
#include "data/probabilityregiondetector.h"
#include "data/structuredgridensemblefilter.h"
#include "data/verticalregridder.h"
#include "src/radar/dwdhdf5radarreader.h"
#include "radar/radarregridder.h"
#include "trajectories/source/deltapressurepertrajectory.h"
#include "data/partialderivativefilter.h"
#include "data/smoothfilter.h"
#include "trajectories/source/trajectoryreader.h"

namespace Met3D
{

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPipelineConfiguration::MPipelineConfiguration()
    : MAbstractApplicationConfiguration()
{
}


/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MPipelineConfiguration::configure()
{
    // If you develop new pipeline modules it might be easier to use a
    // hard-coded pipeline configuration in the development process.
//    initializeDevelopmentDataPipeline();
//    return;

    QString filename = "";

    // Scan global application command line arguments for pipeline definitions.
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    if (sysMC->isCommandLineArgumentSet("pipeline"))
    {
        filename = sysMC->getCommandLineArgumentValue("pipeline");
        filename = expandEnvironmentVariables(filename);
    }

    QString errMsg = "";

    if (filename.isEmpty())
    {
        // No pipeline file has been specified. Try to access default
        // pipeline.
        LOG4CPLUS_INFO(mlog, "No data pipeline configuration "
                             "file has been specified. Using default pipeline "
                             "instead. To specify a custom file, use the "
                             "'--pipeline=<file>' command line argument.");

        filename = "$MET3D_HOME/config/default_pipeline.cfg.template";
        filename = expandEnvironmentVariables(filename);
        QFileInfo fileInfo(filename);
        if (!fileInfo.isFile())
        {
            errMsg = QString(
                        "Default pipeline configuration file"
                        " does not exist. Location: ") + filename;
            filename = "";
            LOG4CPLUS_ERROR(mlog, errMsg);
        }
    }

    if (!filename.isEmpty())
    {
        // Production build: Read pipeline configuration from file.
        // Disadvantage: Can only read parameters for the predefined
        // pipelines.
        initializeDataPipelineFromConfigFile(filename);
    }
    else
    {
        throw MInitialisationError(errMsg.toStdString(), __FILE__, __LINE__);
    }
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

void MPipelineConfiguration::loadDatasetsFromCommandLineArg()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    // Get directories and file filters specified by path command line argument
    // if present.
    QList<MetviewGribFilePath> filePathList;
    if (sysMC->isConnectedToMetview())
    {
        getMetviewGribFilePaths(&filePathList);
    }

    if (sysMC->isCommandLineArgumentSet("datasets"))
    {
        QString datasetArgs = sysMC->getCommandLineArgumentValue("datasets");
        QStringList datasets = datasetArgs.split(';');

        for (const QString& dataset : datasets)
        {
            QString path = expandEnvironmentVariables(dataset);

            if (path.isEmpty()) continue;

            LOG4CPLUS_INFO(mlog, "Loading dataset " << path);

            loadDatasetFromFile(path, filePathList);
        }
    }
    else if (sysMC->isConnectedToMetview() && !filePathList.isEmpty())
    {
        // If Met.3D is called by Metview and no configuration files are given,
        // use default configuration files stored at
        // $MET3D_HOME/config/metview/default_dataset.pipeline.conf .
        QString filename = "$MET3D_HOME/config/metview/default_dataset.pipeline.conf";
        QString path = expandEnvironmentVariables(filename);

        LOG4CPLUS_INFO(mlog, "Loading MetView default dataset " << path);

        loadDatasetFromFile(path, filePathList);
    }
}


QVector<QSettings*> MPipelineConfiguration::readPipelineConfigsFromFiles(
        QStringList filenames)
{
    if (filenames.isEmpty())
    {
        QString configPath = MSystemManagerAndControl::getInstance()->
            getMet3DWorkingDirectory().absoluteFilePath("config/pipelines");
        filenames = MFileUtils::getOpenFileNames(nullptr,
            "Load pipeline configuration", FileTypes::M_PIPELINE_CONFIG, configPath);

        if (filenames.isEmpty())
        {
            return QVector<QSettings*>();
        }
    }

    QVector<QSettings*> configs;

    for (const auto& filename: filenames)
    {
        QFileInfo fileInfo(filename);
        if (!fileInfo.exists())
        {
            QMessageBox msg;
            msg.setWindowTitle("Error");
            msg.setText(QString("Pipeline configuration file"
                                " ' %1 ' does not exist.").arg(filename));
            msg.setIcon(QMessageBox::Warning);
            msg.exec();
            continue;
        }

        LOG4CPLUS_INFO(mlog, "Loading pipeline configuration from " << filename);

        QSettings *settings = new QSettings(filename, QSettings::IniFormat);

        QStringList groups = settings->childGroups();
        if ( !(groups.contains("NWPPipeline")
               || groups.contains("TrajectoriesPipeline")
               || groups.contains("RadarPipeline")) )
        {
            QMessageBox msg;
            msg.setWindowTitle("Error");
            msg.setText("The selected file does not contain a data pipeline "
                        "configuration.");
            msg.setIcon(QMessageBox::Warning);
            msg.exec();
            continue;
        }

        configs.push_back(settings);

        LOG4CPLUS_INFO(mlog, "... configuration has been loaded.");
    }

    return configs;
}


void MPipelineConfiguration::loadDatasetFromFile(const QString& path)
{
    loadDatasetFromFile(path, {});
}


void MPipelineConfiguration::loadDatasetFromFile(const QString& path, const QList<MetviewGribFilePath>& metViewGribPaths)
{
    QFileInfo fileInfo(path);
    if (!fileInfo.exists())
    {
        LOG4CPLUS_ERROR(mlog, "Dataset: " << path.toStdString() << " does not exist.");
        return;
    }

    if (!fileInfo.isFile())
    {
        LOG4CPLUS_ERROR(mlog, "Dataset: " << path.toStdString() << " is not a file.");
        return;
    }

    MNWPPipelineConfigurationInfo nwpPipelineConfig;
    MTrajectoriesPipelineConfigurationInfo trajPipelineConfig;
    MRadarPipelineConfigurationInfo radarPipelineConfig;


    if (nwpPipelineConfig.loadConfigurationFromFile(path))
    {
        if (!metViewGribPaths.isEmpty())
        {
            for (int i = 0; i < metViewGribPaths.size(); i++)
            {
                nwpPipelineConfig.name += QString("_%1").arg(i);

                nwpPipelineConfig.fileDir = metViewGribPaths.at(i).path;
                nwpPipelineConfig.fileFilter = metViewGribPaths.at(i).fileFilter;
                nwpPipelineConfig.dataFormat = GRIB; // Force format to GRIB, as MetView only outputs GRIBs.

                // Create new pipeline.
                initializeNWPPipeline(nwpPipelineConfig);
            }
        }
        else
        {
            // Create new pipeline.
            initializeNWPPipeline(nwpPipelineConfig);
        }
    }
    else if(trajPipelineConfig.loadConfigurationFromFile(path))
    {
        if (trajPipelineConfig.isEnsemble)
        {
            initializePrecomputedTrajectoriesPipeline(trajPipelineConfig);
        }
        else
        {
            LOG4CPLUS_WARN(mlog, "Deterministic precomputed trajectories"
                                 " pipeline has not been implemented yet;"
                                 " skipping.");
        }
    }
    else if (radarPipelineConfig.loadConfigurationFromFile(path))
    {
        initializeRadarPipeline(radarPipelineConfig);
    }
    else
    {
        LOG4CPLUS_ERROR(mlog, "Cannot load dataset " << path << ". "
                                                     << "It is not a valid pipeline configuration file..");
    }
}


void MPipelineConfiguration::initializeScheduler()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    int threadCount = QThreadPool::globalInstance()->maxThreadCount() - 1;

    // Check if threads command line argument is set. If so, parse it into
    // an integer and use it as number of threads for scheduler.
    if (sysMC->isCommandLineArgumentSet("threads"))
    {
        bool conversionOk = false;
        int parsedThreads = sysMC->getCommandLineArgumentValue("threads").toInt(
                &conversionOk);

        if (! conversionOk)
        {
            LOG4CPLUS_WARN(mlog, "Invalid 'threads' argument. Falling back to default: "
                    << threadCount << " threads.");
        }
        else if (parsedThreads > -1)
        {
            threadCount = parsedThreads;
        }
    }

    // Register the multithreaded scheduler
    MScheduler::initialize(threadCount);
}


void MPipelineConfiguration::initializeDataPipelineFromConfigFile(
        QString filename)
{
    LOG4CPLUS_INFO(mlog, "Loading data pipeline configuration from file "
                   << filename << "...");

    if ( !QFile::exists(filename) )
    {
        QString errMsg = QString("Cannot open pipeline configuration file %1: "
                                 "file does not exist.").arg(filename);
        LOG4CPLUS_FATAL(mlog, errMsg);
        throw MInitialisationError(errMsg.toStdString(), __FILE__, __LINE__);
    }

    initializeScheduler();

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    QMap<QString, QString> *defaultMemoryManagers =
            sysMC->getDefaultMemoryManagers();
    QSettings config(filename, QSettings::IniFormat);

    // Initialize memory manager(s).
    // =============================
    int size = config.beginReadArray("MemoryManager");

    for (int i = 0; i < size; i++)
    {
        config.setArrayIndex(i);

        // Read settings from file.
        QString name = config.value("name").toString();
        int size_MB = config.value("size_MB").toInt();

        LOG4CPLUS_INFO(mlog, "Initializing memory manager #" << i << ": ");
        LOG4CPLUS_INFO(mlog, "  name = " << name);
        LOG4CPLUS_INFO(mlog, "  size = " << size_MB << " MB");

        // Check parameter validity.
        if ( name.isEmpty()
             || (size <= 0) )
        {
            LOG4CPLUS_WARN(mlog, "invalid parameters encountered; skipping.");
            continue;
        }

        // Create new memory manager.
        sysMC->registerMemoryManager(
                    name, new MLRUMemoryManager(name, size_MB * 1024.));
    }

    config.endArray();


    // Default memory managers.
    // ========================
    config.beginGroup("DefaultMemoryManagers");

    QString defaultMemoryManager =
            config.value("defaultNWPMemoryManager", "").toString();
    checkAndStoreDefaultPipelineMemoryManager(
            defaultMemoryManager, "NWP", defaultMemoryManagers, sysMC);

    defaultMemoryManager =
            config.value("defaultAnalysisMemoryManager", "").toString();
    checkAndStoreDefaultPipelineMemoryManager(
            defaultMemoryManager, "Analysis", defaultMemoryManagers, sysMC);

    defaultMemoryManager =
            config.value("defaultTrajectoryMemoryManager", "").toString();
    checkAndStoreDefaultPipelineMemoryManager(
            defaultMemoryManager, "Trajectories", defaultMemoryManagers, sysMC);

    config.endGroup();


    // Load datasets from command line arguments:
    // ================

    // Initialize datasets loaded from command line arguments
    // so that configurable pipelines can use them later.
    loadDatasetsFromCommandLineArg();

    // Configurable pipeline(s).
    // ================
    size = config.beginReadArray("ConfigurablePipeline");

    for (int i = 0; i < size; i++)
    {
        config.setArrayIndex(i);

        // Read settings from file.
        QString typeName = config.value("type").toString();
        QString name = config.value("name").toString();
        QString inputSource0 = config.value("input1").toString();
        QString inputSource1 = config.value("input2").toString();
        QString baseRequest0 = config.value("baseRequest1").toString();
        QString baseRequest1 = config.value("baseRequest2").toString();
        QString memoryManagerID = config.value("memoryManagerID").toString();
        bool enableRegridding = config.value("enableRegridding").toBool();

        LOG4CPLUS_INFO(mlog, "Initializing configurable pipeline #" << i << ": ");
        LOG4CPLUS_DEBUG(mlog, "  type = " << typeName);
        LOG4CPLUS_DEBUG(mlog, "  name = " << name);
        LOG4CPLUS_DEBUG(mlog, "  input1 = " << inputSource0);
        LOG4CPLUS_DEBUG(mlog, "  input2 = " << inputSource1);
        LOG4CPLUS_DEBUG(mlog, "  baseRequest1 = " << baseRequest0);
        LOG4CPLUS_DEBUG(mlog, "  baseRequest2 = " << baseRequest1);
        LOG4CPLUS_DEBUG(mlog, "  memoryManagerID = " << memoryManagerID);
        LOG4CPLUS_DEBUG(mlog, "  regridding="
                        << (enableRegridding ? "enabled" : "disabled"));

        MConfigurablePipelineType pipelineType =
                configurablePipelineTypeFromString(typeName);

        // Check parameter validity.
        if ( name.isEmpty()
             || (pipelineType == INVALID_PIPELINE_TYPE)
             || inputSource0.isEmpty()
             || inputSource1.isEmpty()
             || baseRequest0.isEmpty()
             || baseRequest1.isEmpty()
             || memoryManagerID.isEmpty() )
        {
            LOG4CPLUS_WARN(mlog, "invalid parameters encountered; skipping.");
            continue;
        }

        // Create new pipeline.
        initializeConfigurablePipeline(
                    pipelineType, name, inputSource0, inputSource1, baseRequest0,
                    baseRequest1, memoryManagerID,
                    enableRegridding);
    }

    config.endArray();

    LOG4CPLUS_INFO(mlog, "Data pipeline has been configured.");
}


void MPipelineConfiguration::initializeNWPPipeline(
        MNWPPipelineConfigurationInfo pipelineConfigInfo)
{
    const QString dataSourceId = pipelineConfigInfo.name;
    const QString dataSourceIdDerived = dataSourceId + " derived";

    QStringList dataSourceIDs = QStringList()
            << dataSourceId
            << dataSourceIdDerived;

    if (pipelineConfigInfo.enableProbabiltyRegionFilter)
    {
        dataSourceIDs << (dataSourceId + QString(" ProbReg"))
                      << (dataSourceIdDerived + QString(" ProbReg"));
    }

    if (!checkUniquenessOfDataSourceNames(dataSourceId, dataSourceIDs))
    {
        return;
    }

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager* memoryManager = sysMC->getMemoryManager(pipelineConfigInfo.memoryManagerID);

    LOG4CPLUS_INFO(mlog, "Initializing NWP pipeline ''" << dataSourceId << "'' ...");

    // Pipeline for data fields that are stored on disk.
    // =================================================

    MWeatherPredictionReader *nwpReaderENS = nullptr;
    if (pipelineConfigInfo.dataFormat == CF_NETCDF)
    {
        nwpReaderENS = new MClimateForecastReader(
                    dataSourceId, pipelineConfigInfo.treatRotatedGridAsRegularGrid,
                    pipelineConfigInfo.treatProjectedGridAsRegularLonLatGrid,
                    pipelineConfigInfo.convertGeometricHeightToPressure_ICAOStandard,
                    pipelineConfigInfo.consistencyCheckReferenceVariables,
                    pipelineConfigInfo.auxiliary3DPressureField,
                    pipelineConfigInfo.surfacePressureFieldType,
                    pipelineConfigInfo.disableGridConsistencyCheck);
    }
    else if (pipelineConfigInfo.dataFormat == GRIB)
    {
        nwpReaderENS = new MGribReader(dataSourceId,
                                       pipelineConfigInfo.surfacePressureFieldType,
                                       pipelineConfigInfo.consistencyCheckReferenceVariables,
                                       pipelineConfigInfo.auxiliary3DPressureField,
                                       pipelineConfigInfo.disableGridConsistencyCheck);
    }
    nwpReaderENS->setMemoryManager(memoryManager);
    nwpReaderENS->setDataRoot(pipelineConfigInfo.fileDir, pipelineConfigInfo.fileFilter);

    MSmoothFilter *smoothFilter = new MSmoothFilter();
    smoothFilter->setMemoryManager(memoryManager);
    smoothFilter->setInputSource(nwpReaderENS);

    MPartialDerivativeFilter *partialDerivativeFilter = new MPartialDerivativeFilter();
    partialDerivativeFilter->setMemoryManager(memoryManager);
    partialDerivativeFilter->setInputSource(smoothFilter);

    MStructuredGridEnsembleFilter *ensFilter =
            new MStructuredGridEnsembleFilter();
    ensFilter->setMemoryManager(memoryManager);

    if (!pipelineConfigInfo.enableRegridding)
    {
        ensFilter->setInputSource(partialDerivativeFilter);
    }
    else
    {
        MStructuredGridEnsembleFilter *ensFilter1 =
                new MStructuredGridEnsembleFilter();
        ensFilter1->setMemoryManager(memoryManager);
        ensFilter1->setInputSource(partialDerivativeFilter);

        MVerticalRegridder *regridderEPS =
                new MVerticalRegridder();
        regridderEPS->setMemoryManager(memoryManager);
        regridderEPS->setInputSource(ensFilter1);

        ensFilter->setInputSource(regridderEPS);
    }

    sysMC->registerDataSource(dataSourceId,
                              ensFilter);

    if (pipelineConfigInfo.enableProbabiltyRegionFilter)
    {
        MProbabilityRegionDetectorFilter *probRegDetectorNWP =
                new MProbabilityRegionDetectorFilter();
        probRegDetectorNWP->setMemoryManager(memoryManager);
        probRegDetectorNWP->setInputSource(ensFilter);

        sysMC->registerDataSource(dataSourceId + QString(" ProbReg"),
                                  probRegDetectorNWP);
    }

    // Pipeline for derived variables (derivedMetVarsSource connects to
    // the reader and computes derived data fields. The rest of the pipeline
    // is the same as above).
    // =====================================================================

    MDerivedMetVarsDataSource *derivedMetVarsSource =
            new MDerivedMetVarsDataSource();
    derivedMetVarsSource->setMemoryManager(memoryManager);
    derivedMetVarsSource->setInputSource(nwpReaderENS);

    QMap<QString, QString> inputVars = pipelineConfigInfo.inputVarsForDerivedVars;
    for (auto it = inputVars.keyValueBegin(); it != inputVars.keyValueEnd(); ++it)
    {
        QString derivedVarsMappingString = it->first;
        QString variableName = it->second;

        // Check if variable name contains old grib variable suffix.
        static const QString errMsg = "Derived variable mapping %1 "
                                      "needs to be updated. "
                                      "The mapped variable name '%2' contains "
                                      "the '%3' suffix, which is no longer used. "
                                      "Removing the suffix for now. "
                                      "Please update the derived variable mapping to "
                                      "avoid this warning in the future.";

        for (const QString& suffix : QStringList({" (fc)", " (an)", " (ens)"}))
        {
            if (variableName.endsWith(suffix))
            {
                LOG4CPLUS_WARN(mlog, errMsg.arg(derivedVarsMappingString,
                                                variableName, suffix));

                // Get index of the suffix.
                int idx = variableName.lastIndexOf(suffix);

                variableName.remove(idx, suffix.length());
            }
        }

        derivedMetVarsSource->setInputVariableMapping(
            derivedVarsMappingString, variableName);
    }

    MSmoothFilter *smoothFilterDerived = new MSmoothFilter();
    smoothFilterDerived->setMemoryManager(memoryManager);
    smoothFilterDerived->setInputSource(derivedMetVarsSource);

    MPartialDerivativeFilter *partialDerivativeFilterDerived = new MPartialDerivativeFilter();
    partialDerivativeFilterDerived->setMemoryManager(memoryManager);
    partialDerivativeFilterDerived->setInputSource(smoothFilterDerived);

    MStructuredGridEnsembleFilter *ensFilterDerived =
            new MStructuredGridEnsembleFilter();
    ensFilterDerived->setMemoryManager(memoryManager);

    if (!pipelineConfigInfo.enableRegridding)
    {
        ensFilterDerived->setInputSource(partialDerivativeFilterDerived);
    }
    else
    {
        MStructuredGridEnsembleFilter *ensFilter1Derived =
                new MStructuredGridEnsembleFilter();
        ensFilter1Derived->setMemoryManager(memoryManager);
        ensFilter1Derived->setInputSource(partialDerivativeFilterDerived);

        MVerticalRegridder *regridderEPSDerived =
                new MVerticalRegridder();
        regridderEPSDerived->setMemoryManager(memoryManager);
        regridderEPSDerived->setInputSource(ensFilter1Derived);

        ensFilterDerived->setInputSource(regridderEPSDerived);
    }

    sysMC->registerDataSource(dataSourceIdDerived,
                              ensFilterDerived);

    if (pipelineConfigInfo.enableProbabiltyRegionFilter)
    {
        MProbabilityRegionDetectorFilter *probRegDetectorNWPDerived =
                new MProbabilityRegionDetectorFilter();
        probRegDetectorNWPDerived->setMemoryManager(memoryManager);
        probRegDetectorNWPDerived->setInputSource(ensFilterDerived);

        sysMC->registerDataSource(dataSourceIdDerived + QString(" ProbReg"),
                                  probRegDetectorNWPDerived);
    }

    if (pipelineConfigInfo.addTimesAndMembersToSyncControl)
    {
        // Add the times of this data source to the default sync control if it is
        // initialized. It might be uninitialized if the frontend file is loaded
        // after configuring this pipeline.
        MSyncControl *syncControl = MSystemManagerAndControl::getInstance()->
                                    getSyncControl("Synchronization");
        if (syncControl)
        {
            syncControl->addDataSourceToControl(dataSourceId);
        }
    }

    LOG4CPLUS_INFO(mlog, "Pipeline ''" << dataSourceId << "'' has been initialized.");
}


void MPipelineConfiguration::initializeRadarPipeline(MRadarPipelineConfigurationInfo pipelineConfigInfo)
{
    const QString dataSourceId = pipelineConfigInfo.name;

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager* memoryManager = sysMC->getMemoryManager(pipelineConfigInfo.memoryManagerID);

    LOG4CPLUS_INFO(mlog, "Initializing Radar pipeline ''"
                    << dataSourceId.toStdString() << "'' ...");

    MRadarReader *radarReader = new MDWDHDF5RadarReader(dataSourceId);
    radarReader->setStandardAzimuthGridsize(pipelineConfigInfo.standardAzimuthGridsize);
    radarReader->setMemoryManager(memoryManager);
    radarReader->setDataRoot(pipelineConfigInfo.fileDir, pipelineConfigInfo.fileFilter);

    sysMC->registerDataSource(dataSourceId,
                              radarReader);

    MRadarRegridder *radarRegridder = new MRadarRegridder();
    radarRegridder->setInputSource(radarReader);
    radarRegridder->setMemoryManager(memoryManager);

    sysMC->registerDataSource(dataSourceId + QString(" Regridder"), radarRegridder);

    if (pipelineConfigInfo.addTimesAndMembersToSyncControl)
    {
        // Add the times of this reader to the default sync control if it is
        // initialized. It might be uninitialized if the frontend file is loaded
        // after configuring this pipeline.
        MSyncControl *syncControl = MSystemManagerAndControl::getInstance()->
                                    getSyncControl("Synchronization");
        if (syncControl)
        {
            syncControl->addDataSourceToControl(dataSourceId);
        }
    }

    LOG4CPLUS_INFO(mlog, "Pipeline ''" << dataSourceId.toStdString()
                    << "'' has been initialized.");
}


void MPipelineConfiguration::initializePrecomputedTrajectoriesPipeline(
        MTrajectoriesPipelineConfigurationInfo pipelineConfigInfo)
{
    const QString dataSourceId = pipelineConfigInfo.name;
    QStringList dataSourceIDs = QStringList()
            << (dataSourceId + QString(" Reader"));

    if (!checkUniquenessOfDataSourceNames(dataSourceId, dataSourceIDs))
    {
        return;
    }

    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager* memoryManager =
            sysMC->getMemoryManager(pipelineConfigInfo.memoryManagerID);

    LOG4CPLUS_INFO(mlog,
                    "Initializing precomputed ensemble trajectories pipeline ''"
                    << dataSourceId << "'' ...");

    // Trajectory reader.
    MTrajectoryReader *trajectoryReader =
            new MTrajectoryReader(dataSourceId);
    trajectoryReader->setMemoryManager(memoryManager);
    trajectoryReader->setDataRoot(pipelineConfigInfo.fileDir, "*");
    sysMC->registerDataSource(dataSourceId + QString(" Reader"),
                              trajectoryReader);

    if (pipelineConfigInfo.addTimesAndMembersToSyncControl)
    {
        // Add the times of this reader to the default sync control if it is
        // initialized. It might be uninitialized if the frontend file is loaded
        // after configuring this pipeline.
        MSyncControl *syncControl = MSystemManagerAndControl::getInstance()->
                                    getSyncControl("Synchronization");
        if (syncControl)
        {
            syncControl->addDataSourceToControl(dataSourceId);
        }
    }

    LOG4CPLUS_INFO(mlog, "Trajectory reader ''" << dataSourceId << "'' has been initialized.");
}


void MPipelineConfiguration::initializeConfigurablePipeline(
        MConfigurablePipelineType pipelineType,
        QString name,
        QString inputSource0,
        QString inputSource1,
        QString baseRequest0,
        QString baseRequest1,
        QString memoryManagerID,
        bool enableRegridding)
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    MAbstractMemoryManager* memoryManager = sysMC->getMemoryManager(memoryManagerID);

    const QString dataSourceId = name;
    LOG4CPLUS_INFO(mlog, "Initializing configurable pipeline ''"
                    << dataSourceId << "'' ...");

    switch (pipelineType)
    {
    case DIFFERENCE:
    {
        QStringList dataSourceIDs = QStringList()
                << (dataSourceId);

        if (!checkUniquenessOfDataSourceNames(dataSourceId, dataSourceIDs))
        {
            return;
        }
        // Pipeline for difference variables.
        // ==================================
        const QString dataSourceIdDifference = dataSourceId;

        MDifferenceDataSource *differenceSource = new MDifferenceDataSource();
        differenceSource->setMemoryManager(memoryManager);

        differenceSource->setInputSource(
                    0, dynamic_cast<MWeatherPredictionDataSource*>(
                        sysMC->getDataSource(inputSource0)));
        differenceSource->setInputSource(
                    1, dynamic_cast<MWeatherPredictionDataSource*>(
                        sysMC->getDataSource(inputSource1)));

        differenceSource->setBaseRequest(0, baseRequest0);
        differenceSource->setBaseRequest(1, baseRequest1);

        MStructuredGridEnsembleFilter *ensFilterDifference =
                new MStructuredGridEnsembleFilter();
        ensFilterDifference->setMemoryManager(memoryManager);

        if (!enableRegridding)
        {
            ensFilterDifference->setInputSource(differenceSource);
        }
        else
        {
            MStructuredGridEnsembleFilter *ensFilter1Difference =
                    new MStructuredGridEnsembleFilter();
            ensFilter1Difference->setMemoryManager(memoryManager);
            ensFilter1Difference->setInputSource(differenceSource);

            MVerticalRegridder *regridderEPSDerived =
                    new MVerticalRegridder();
            regridderEPSDerived->setMemoryManager(memoryManager);
            regridderEPSDerived->setInputSource(ensFilter1Difference);

            ensFilterDifference->setInputSource(regridderEPSDerived);
        }

        sysMC->registerDataSource(dataSourceIdDifference,
                                  ensFilterDifference);
        break;
    }
    default:
    {
        LOG4CPLUS_ERROR(mlog,
                        "Invalid configurable pipeline type. Could not"
                        " initialize pipeline ''" << dataSourceId << "''.");
        return;
    }
    }

    LOG4CPLUS_INFO(mlog, "Pipeline ''" << dataSourceId << "'' has been initialized.");
}


void MPipelineConfiguration::initializeDevelopmentDataPipeline()
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    initializeScheduler();

    sysMC->registerMemoryManager("NWP",
               new MLRUMemoryManager("NWP", 10.*1024.*1024.));
    sysMC->registerMemoryManager("Analysis",
               new MLRUMemoryManager("Analysis", 10.*1024.));

    MNWPPipelineConfigurationInfo info1;
    info1.name = "ECMWF DET EUR_LL015";
    info1.fileDir = "/home/local/data/mss/grid/ecmwf/netcdf";
    info1.fileFilter = "*ecmwf_forecast*EUR_LL015*.nc";
    info1.memoryManagerID = "NWP";
    info1.dataFormat = CF_NETCDF;
    info1.enableRegridding = false;
    info1.enableProbabiltyRegionFilter = true;
    info1.treatRotatedGridAsRegularGrid = false;
    info1.treatProjectedGridAsRegularLonLatGrid = false;
    info1.surfacePressureFieldType = "auto";
    info1.convertGeometricHeightToPressure_ICAOStandard = false;
    info1.auxiliary3DPressureField = "";
    info1.consistencyCheckReferenceVariables = "";
    info1.disableGridConsistencyCheck = false;

    initializeNWPPipeline(info1);

    MNWPPipelineConfigurationInfo info2;
    info2.name = "ECMWF ENS EUR_LL10";
    info2.fileDir = "/home/local/data/mss/grid/ecmwf/netcdf";
    info2.fileFilter = "*ecmwf_ensemble_forecast*EUR_LL10*.nc";
    info2.memoryManagerID = "NWP";
    info2.dataFormat = CF_NETCDF;
    info2.enableRegridding = false;
    info2.enableProbabiltyRegionFilter = true;
    info2.treatRotatedGridAsRegularGrid = false;
    info2.treatProjectedGridAsRegularLonLatGrid = false;
    info2.surfacePressureFieldType = "auto";
    info2.convertGeometricHeightToPressure_ICAOStandard = false;
    info2.auxiliary3DPressureField = "";
    info2.consistencyCheckReferenceVariables = "";
    info2.disableGridConsistencyCheck = false;

    initializeNWPPipeline(info2);

    sysMC->registerMemoryManager("Trajectories DF-T psfc_1000hPa_L62",
               new MLRUMemoryManager("Trajectories DF-T psfc_1000hPa_L62",
                                     10.*1024.*1024.));

    MTrajectoriesPipelineConfigurationInfo trajInfo1;
    trajInfo1.name = "Lagranto ENS EUR_LL10 DF-T psfc_1000hPa_L62";
    trajInfo1.fileDir = "/mnt/ssd/data/trajectories/EUR_LL10/psfc_1000hPa_L62";
    trajInfo1.boundaryLayerTrajectories = false;
    trajInfo1.memoryManagerID = "Trajectories DF-T psfc_1000hPa_L62";

    initializePrecomputedTrajectoriesPipeline(trajInfo1);

    sysMC->registerMemoryManager("Trajectories DF-T psfc_min_L62",
               new MLRUMemoryManager("Trajectories  DF-T psfc_min_L62",
                                     12.*1024.*1024.));

    MTrajectoriesPipelineConfigurationInfo trajInfo2;
    trajInfo2.name = "Lagranto ENS EUR_LL10 DF-T psfc_min_L62";
    trajInfo2.fileDir = "/mnt/ssd/data/trajectories/EUR_LL10/psfc_min_L62";
    trajInfo2.boundaryLayerTrajectories = false;
    trajInfo2.memoryManagerID = "Trajectories DF-T psfc_min_L62";

    initializePrecomputedTrajectoriesPipeline(trajInfo2);

    sysMC->registerMemoryManager("Trajectories ABL-T psfc_min_L62_abl",
               new MLRUMemoryManager("Trajectories ABL-T psfc_min_L62_abl",
                                     10.*1024.*1024.));

    MTrajectoriesPipelineConfigurationInfo trajInfoAbl1;
    trajInfoAbl1.name = "Lagranto ENS EUR_LL10 ABL-T psfc_min_L62_abl";
    trajInfoAbl1.fileDir = "/mnt/ssd/data/trajectories/EUR_LL10/psfc_min_L62_abl";
    trajInfoAbl1.boundaryLayerTrajectories = true;
    trajInfoAbl1.memoryManagerID = "Trajectories ABL-T psfc_min_L62_abl";

    initializePrecomputedTrajectoriesPipeline(trajInfoAbl1);

    sysMC->registerMemoryManager("Trajectories ABL-T 10hPa",
               new MLRUMemoryManager("Trajectories ABL-T 10hPa",
                                     10.*1024.*1024.));

    MTrajectoriesPipelineConfigurationInfo trajInfoAbl2;
    trajInfoAbl2.name = "Lagranto ENS EUR_LL10 ABL-T 10hPa";
    trajInfoAbl2.fileDir = "/mnt/ssd/data/trajectories/EUR_LL10/blt_PL10hPa";
    trajInfoAbl2.boundaryLayerTrajectories = true;
    trajInfoAbl2.memoryManagerID = "Trajectories ABL-T 10hPa";

    initializePrecomputedTrajectoriesPipeline(trajInfoAbl2);
}


void MPipelineConfiguration::getMetviewGribFilePaths(
        QList<MPipelineConfiguration::MetviewGribFilePath> *gribFilePaths)
{
    gribFilePaths->clear();
    QStringList gribFilePathsStringList;
    gribFilePathsStringList.clear();
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();
    // Scan global application command line arguments for metview definition.
    if (sysMC->isCommandLineArgumentSet("path"))
    {
        QString paths = sysMC->getCommandLineArgumentValue("path");
        // Remove quotes if not already removed by shell.
        paths.remove(QChar('"'), Qt::CaseSensitive);
        // Get list of paths (directory and file filter).
        gribFilePathsStringList = paths.split(";", Qt::SkipEmptyParts);
        // Extract directory and file filter from given paths.
        for (QString& path : gribFilePathsStringList)
        {
            QFileInfo fileInfo(expandEnvironmentVariables(path));
            QString fileFilter = fileInfo.fileName();
            path.chop(fileFilter.length());
            MetviewGribFilePath metviewGribFilePath;
            metviewGribFilePath.path = path;
            metviewGribFilePath.fileFilter = fileFilter;
            gribFilePaths->append(metviewGribFilePath);
        }
    }
}


MPipelineConfiguration::MConfigurablePipelineType
MPipelineConfiguration::configurablePipelineTypeFromString(QString typeName)
{
    if (typeName == "DIFFERENCE")
    {
        return MConfigurablePipelineType::DIFFERENCE;
    }
    else
    {
        return MConfigurablePipelineType::INVALID_PIPELINE_TYPE;
    }
}


void MPipelineConfiguration::checkAndStoreDefaultPipelineMemoryManager(
        QString defaultMemoryManager, QString PipelineID,
        QMap<QString, QString> *defaultMemoryManagers,
        MSystemManagerAndControl *sysMC)
{
    if (defaultMemoryManager.isEmpty())
    {
        defaultMemoryManager = sysMC->getMemoryManagerIdentifiers().first();

        LOG4CPLUS_WARN(mlog, "No memory manager set as default for '"
                       << PipelineID << "' pipeline.");
    }
    else
    {
        if (!sysMC->getMemoryManagerIdentifiers().contains(defaultMemoryManager))
        {
            defaultMemoryManager = sysMC->getMemoryManagerIdentifiers().first();

            LOG4CPLUS_WARN(mlog, "Memory manager '" << defaultMemoryManager
                           << "' is set as default for '" << PipelineID
                           << "' pipeline but it does not exist.");
        }
    }
    if (!defaultMemoryManager.isEmpty())
    {
        LOG4CPLUS_INFO(mlog, "Using '" << defaultMemoryManager
                        << "' as default memory manager for '"
                        << PipelineID.toStdString() << "' pipeline.");
    }
    defaultMemoryManagers->insert(PipelineID, defaultMemoryManager);
}


bool MPipelineConfiguration::checkUniquenessOfDataSourceNames(
        const QString& dataSetName,
        QStringList &dataSources) const
{
    MMainWindow *mainWin =
            MSystemManagerAndControl::getInstance()->getMainWindow();
    QStringList existingDataSourcesList =
            MSystemManagerAndControl::getInstance()->getDataSourceIdentifiers();

    for (const QString& dataSourceID : dataSources)
    {
        if (existingDataSourcesList.contains(dataSourceID))
        {
            if (MSystemManagerAndControl::getInstance()->isUiReady())
            {
                QMessageBox::warning(
                        mainWin, "Adding data set",
                        "The name '" + dataSetName
                                + "'  is already in use by another data set."
                                  " Please choose a different name."
                                  " (The data set will NOT be added.)");
            }
            else
            {
                LOG4CPLUS_WARN(mlog, "The name '" << dataSetName
                                                  << "'  is already in use by another data set."
                                                     " (The data set will NOT be added.)");
            }

            return false;
        }
    }

    return true;
}


} // namespace Met3D
