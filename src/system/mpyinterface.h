/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MPYINTERFACE_H
#define MET_3D_MPYINTERFACE_H

// standard library imports

// related third party imports
#include <QString>

// local application imports
#include "data/netcdffile.h"
#include "data/derivedvars/pythonderivedprocessor.h"

// Necessary "hack" to import PyBind interface, as a pybind include header contains
// an attribute called "slots", which is a protected keyword in Qt applications.
#pragma push_macro("slots")
#undef slots
#include <pybind11/embed.h>
#include <pybind11/stl.h>
#pragma pop_macro("slots")

namespace py = pybind11;

namespace Met3D
{

/**
 * This class encapsulates the Python interface of Met.3D. It contains
 * functionality to export data to Python via a well-defined interface. Users
 * can provide a file and function name, which gets called with the current
 * variable data.
 */
class MPyInterface
{
private:
    /**
     * Creates the singleton instance of the Python interface.
     */
    MPyInterface();

    ~MPyInterface();


public:
    /**
     * @return Return the Python interface instance, creates the instance if it
     * does not exist yet.
     */
    static MPyInterface *getInstance();
    
    /**
     * Export the @p actorVars actor variables (more precisely, their current
     * underlying grids) to a Python endpoint. The endpoint is defined by the
     * path @p filePath to the Python file, and the name of the function
     * @p functionName. This method will be invoked on calling with a
     * xarray.Dataset data set as argument. This is an asynchronous function.
     * It starts a worker thread to do the export and instantly returns.
     * @param actorVars The actor variables to export to Python.
     * @param pyFilePath The Python file path.
     * @param pyFuncName The name of the function to invoke.
     */
    static void exportVariables(const QList<MNWPActorVariable *>& actorVars,
                                const QString& pyFilePath, const QString& pyFuncName);

    /**
     * Call the Python function identified by the @p pyFunc endpoint enum, providing a list of
     * input grids, and filling the grid at the pointer to @p out.
     * @param pyFunc The Python function to invoke.
     * @param in The list of input grids. Need to match the order of the data arrays in pyFunc.
     * @param out The output grid to be populated.
     * @return False if Python execution fails with an error, True if successful.
     */
    static bool computeVariable(MPythonDerivedProcessor::EndpointType pyFunc,
                                const QList<MStructuredGrid*>& in, MStructuredGrid* out);

    /**
     * Reloads the Python endpoints. This has to be called for changes in the
     * Python functions to take effect on the next interaction with the
     * interface.
     */
    static void reload();

    friend class MPyInterfaceNetCDFExporter;
private:

    /**
     * Underlying synchronous implementation of @c exportVariables.
     */
    static void exportVariablesImpl(const QList<MNWPActorVariable *>& actorVars,
                                    const QString& pyFilePath, const QString& pyFuncName);

    /** Singleton instance of python interface. */
    static MPyInterface* instance;
    static QMutex initializationMutex;

    /** The interpreter guard, and the interface module. */
    py::scoped_interpreter* guard;
    py::module_ interface;

    /** The endpoint functions in the interface module. */
    py::object exportNetCDF;
    py::object computeVarEndpoint;

    /** Pointer to the GIL release, so it stays released and becomes available
       in other threads. */
    std::unique_ptr<py::gil_scoped_release> mp_gil_release;
};


/**
 * Logger which serves as endpoint of log messages from the Python endpoints
 * to forward them correctly to our log4cplus interface.
 */
class MPyInterfaceLogger
{
public:
    static void info(std::string str) { LOG4CPLUS_INFO(mlog, str); }

    static void debug(std::string str) { LOG4CPLUS_DEBUG(mlog, str); }

    static void warn(std::string str) { LOG4CPLUS_WARN(mlog, str); }

    static void error(std::string str) { LOG4CPLUS_ERROR(mlog, str); }
};


/**
 * Worker class for calling the NetCDF export endpoint.
 */
class MPyInterfaceNetCDFExporter : public QObject {
Q_OBJECT
public:
    MPyInterfaceNetCDFExporter(QList<MNWPActorVariable *> actorVars,
                               QString pyFilePath,
                               QString pyFuncName) : vars(actorVars), file(pyFilePath), func(pyFuncName)
    {}

    ~MPyInterfaceNetCDFExporter() = default;

public slots:
    /**
     * Runs the worker. Calls the synchronous implementation of the export function.
     */
    void runWorker()
    {
        MPyInterface::exportVariablesImpl(vars, file, func);
    };

private:
    QList<MNWPActorVariable*> vars;
    QString file;
    QString func;
};

} // namespace Met3D


#endif //MET_3D_MPYINTERFACE_H
