/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2024 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "mpyinterface.h"

// standard library imports

// related third party imports

// local application imports
#include "data/structuredgrid.h"
#include "data/netcdfwriter.h"

namespace Met3D
{
using namespace netCDF;

/**
 * Define Python endpoints for existing C++ classes and structs.
 */
PYBIND11_EMBEDDED_MODULE(met3d_bindings, module) {

    /** netCDF::NC_memio struct containing pointer and size of NC file in memory. */
    py::class_<NC_memio>(module, "NC_memio")
        .def(py::init<>())
        .def_readwrite("size", &NC_memio::size)
        .def_readwrite("memory", &NC_memio::memory)
        .def_readwrite("flags", &NC_memio::flags);

    /** Logger endpoint to forward log messages to log4cplus. */
    py::class_<MPyInterfaceLogger>(module, "Logger")
        .def_static("info", &MPyInterfaceLogger::info)
        .def_static("debug", &MPyInterfaceLogger::debug)
        .def_static("warn", &MPyInterfaceLogger::warn)
        .def_static("error", &MPyInterfaceLogger::error);
};


MPyInterface* MPyInterface::instance = nullptr;
QMutex MPyInterface::initializationMutex;

/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MPyInterface::MPyInterface()
{
    MStopwatch stopwatch;
    LOG4CPLUS_INFO(mlog, "Initializing Python interface...");

    guard = new py::scoped_interpreter();
    // Releases the GIL so it can be accessed from other threads. Need to keep
    // pointer alive as we want to keep it released for other threads to access.
    mp_gil_release =
            std::unique_ptr<py::gil_scoped_release>(new py::gil_scoped_release());

    auto sysMC = MSystemManagerAndControl::getInstance();
    QString pythonInterfaceDir = sysMC->getMet3DHomeDir().absoluteFilePath("src/python/interface/");

    // Execute default header which adds this python subfolder to the
    // python path.
    std::stringstream ss;
    ss << "import sys" << std::endl;
    ss << "module_path = r\'" << pythonInterfaceDir << "\'" << std::endl;
    ss << "if module_path not in sys.path:" << std::endl;
    ss << "    sys.path.append(module_path) " << std::endl;

    py::gil_scoped_acquire acquire; // Acquire the global interpreter lock.
    py::dict globals = py::globals();

    try
    {
        py::exec(ss.str(), globals, globals);

        // Import the interface module interface.py
        interface = py::module::import("interface");

        // Define the available end point functions.
        exportNetCDF = interface.attr("export_netcdf");
        computeVarEndpoint = interface.attr("derived_var_endpoint");
    }
    catch (const std::exception &exception)
    {
        // Something went wrong initializing the Python interface to the endpoints.
        LOG4CPLUS_ERROR(mlog, "Error initializing PyBind interface: " << exception.what());
        return;
    }

    stopwatch.split();
    LOG4CPLUS_DEBUG(mlog, "Python interface has been initialized in "
            << stopwatch.getLastSplitTime(MStopwatch::SECONDS)
            << " seconds.");
}


MPyInterface::~MPyInterface()
{
    delete guard;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

MPyInterface* MPyInterface::getInstance()
{
    // Lock access to getInstance(), as otherwise worker threads might call this method in parallel, causing
    // initialization of multiple interfaces.
    QMutexLocker scAccessMutexLocker(&initializationMutex);
    if (MPyInterface::instance == nullptr)
    {
        MPyInterface::instance = new MPyInterface();
    }

    return MPyInterface::instance;
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

void MPyInterface::exportVariables(const QList<MNWPActorVariable *>& actorVars,
                                   const QString& pyFilePath,
                                   const QString& pyFuncName)
{
    // Running export variables in separate thread as otherwise it locks the UI.
    // Exporting variables the first time can take up to 30s if the Python endpoint
    // must be initialized.
    // Creating here a thread and a worker for this task, and start the thread.
    // QtConcurrent::run seems to interfere with the pybind interface.

    auto* worker = new MPyInterfaceNetCDFExporter(actorVars, pyFilePath, pyFuncName);
    auto* thread = new QThread();
    worker->moveToThread(thread);

    // Connect the thread's started signal to the worker's doWork slot
    QObject::connect(thread, &QThread::started, worker, &MPyInterfaceNetCDFExporter::runWorker);

    // Connect the worker's finished signal to quit the thread. Also delete the thread.
    QObject::connect(worker, &QObject::destroyed, thread, &QThread::quit);
    QObject::connect(thread, &QThread::finished, thread, &QThread::deleteLater);

    thread->start();
}


void MPyInterface::exportVariablesImpl(const QList<MNWPActorVariable *>& actorVars,
                                   const QString& pyFilePath,
                                   const QString& pyFuncName)
{
    auto handle = MProgressBar::getInstance()->addTask("Exporting variables to Python...");
    auto py = MPyInterface::getInstance();

    py::gil_scoped_acquire acquire; // Acquire the global interpreter lock.
    if (pyFilePath.isEmpty() || ! QFile::exists(pyFilePath))
    {
        LOG4CPLUS_WARN(mlog, "Provided Python file '" << pyFilePath
                << "' does not exist. Python interface will not be invoked.");
        return;
    }

    // Create a diskless file and populate it with the grids.
    MNcFile *ncFile = MNcFile::createDisklessFile();
    for (MNWPActorVariable* actorVar : actorVars)
    {
        ncFile->addActorVariable(actorVar);
    }

    // This also closes the diskless file.
    NC_memio memory = ncFile->getDataPointer();

    try
    {
        // Call Python endpoint with path, function, and struct to invoke.
        py->exportNetCDF(pyFilePath.toStdString(), pyFuncName.toStdString(),
                     &memory);
    }
    catch (const std::exception &e)
    {
        LOG4CPLUS_ERROR(mlog, "Error invoking Python function '"
                                  << pyFuncName << "' in file '" << pyFilePath
                                  << "': " << e.what());
    }
    MProgressBar::getInstance()->taskCompleted(handle);
}


bool MPyInterface::computeVariable(MPythonDerivedProcessor::EndpointType pyFunc,
                                   const QList<MStructuredGrid*>& in, MStructuredGrid *out)
{
    auto py = MPyInterface::getInstance();

    py::gil_scoped_acquire acquire;
    // Create a diskless file and populate it with the grids.
    MNcFile *ncFile = MNcFile::createDisklessFile();

    std::vector<std::string> inNames;
    for (auto inGrid : in)
    {
        ncFile->addStructuredGrid(inGrid);
        inNames.push_back(inGrid->getVariableName().toStdString());
    }
    ncFile->addStructuredGrid(out);

    // ncFile needs to be prepared for populating data. The Python endpoint
    // can NOT add new fields or in any other way change the size of the
    // memory used by the netcdf file in memory.
    // This also closes the diskless file.
    NC_memio memory = ncFile->getDataPointer();
    // Allocate data to populate by the Python interface.
    char* outData = new char[memory.size];

    try
    {
        // Call the Python endpoint, pass the input/output field names, and the address where
        // the result should be written to.
        py->computeVarEndpoint(pyFunc, inNames, out->getVariableName().toStdString(), &memory,
                          reinterpret_cast<uintptr_t>(outData));
    }
    catch (const std::exception &e)
    {
        LOG4CPLUS_ERROR(mlog, "Error invoking Python endpoint: " << e.what());
        delete ncFile;
        delete[] outData;
        return false;
    }

    // Delete the file sent to Python. Also releases the memory of the file.
    delete ncFile;

    // Create a new nc file from the same memory address.
    MNcFile* resultNc = MNcFile::createFromMemory(outData, memory.size);
    try
    {
        resultNc->fillStructuredGrid(out);
    }
    catch (const netCDF::exceptions::NcException& e)
    {
        LOG4CPLUS_ERROR(mlog, "Unable to parse result of Python computation into a "
                              "MStructuredGrid: " << e.what());
        delete resultNc;
        delete[] outData;
        return false;
    }

    // Delete the data populated by Python, and the ncFile.
    delete resultNc;
    delete[] outData;

    return true;
}


void MPyInterface::reload()
{
    auto py = MPyInterface::getInstance();
    py::gil_scoped_acquire acquire;

    // Reload interface and endpoint functions.
    py->interface.reload();
    py->exportNetCDF = py->interface.attr("export_netcdf");
    py->computeVarEndpoint = py->interface.attr("derived_var_endpoint");
}

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

} // namespace Met3D