/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2023-2024 Thorwin Vogt
**  Copyright 2024      Susanne Fuchs
**
**  Regional Computing Center, Visualization
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#include "mpipelineconfigurationinfo.h"

// standard library imports

// related third party imports
#include <QFileDialog>
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "gxfw/msystemcontrol.h"

namespace Met3D
{


void MAbstractPipelineConfigurationInfo::saveConfigurationToFile(const QString &filename)
{
    if (filename.isEmpty()) return;

    if (QFile::exists(filename))
    {
        QFile::remove(filename);
    }

    LOG4CPLUS_INFO(mlog, "Saving configuration to " << filename.toStdString());

    auto *settings = new QSettings(filename, QSettings::IniFormat);

    saveConfiguration(settings);

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been saved.");
}


bool MAbstractPipelineConfigurationInfo::loadConfigurationFromFile(const QString &filename)
{
    if (filename.isEmpty()) return false;

    QFileInfo fileInfo(filename);
    if (!fileInfo.exists()) return false;

    LOG4CPLUS_INFO(mlog,
                    "Loading pipeline configuration from "
                            << filename.toStdString());

    auto *settings = new QSettings(filename, QSettings::IniFormat);

    if (!isValidConfiguration(settings))
    {
        delete settings;
        return false;
    }

    loadConfiguration(settings);

    delete settings;

    LOG4CPLUS_INFO(mlog, "... configuration has been loaded.");

    return true;
}


void MNWPPipelineConfigurationInfo::saveConfiguration(QSettings *settings)
{
    // File Format.
    // ============
    settings->beginGroup("FileFormat");
    // Save version id of Met.3D.
    settings->setValue("met3dVersion", met3dVersionString);
    settings->endGroup();
    // ============

    // NWP Pipeline
    settings->beginGroup("NWPPipeline");
    settings->setValue("name", name);
    settings->setValue("path", fileDir);
    settings->setValue("fileFilter", fileFilter);
    settings->setValue("memoryManagerID", memoryManagerID);
    settings->setValue("fileFormat", getNWPFormatString());
    settings->setValue("enableRegridding", enableRegridding);
    settings->setValue("enableProbabilityRegionFilter",
                       enableProbabiltyRegionFilter);
    settings->setValue("treatRotatedGridAsRegularGrid",
                       treatRotatedGridAsRegularGrid);
    settings->setValue("treatProjectedGridAsRegularLonLatGrid",
                       treatProjectedGridAsRegularLonLatGrid);
    settings->setValue(
            "convertGeometricHeightToPressure_ICAOStandard",
            convertGeometricHeightToPressure_ICAOStandard);
    settings->setValue("gribSurfacePressureFieldType",
                       surfacePressureFieldType);
    settings->setValue("auxiliary3DPressureField",
                       auxiliary3DPressureField);
    settings->setValue("consistencyCheckRefVars",
                       consistencyCheckReferenceVariables);
    settings->setValue(
            "disableGridConsistencyCheck",
            disableGridConsistencyCheck);
    settings->setValue("inputVarsForDerivedVars",
                       inputVarsMapToString(inputVarsForDerivedVars));
    settings->endGroup();
}


void MNWPPipelineConfigurationInfo::loadConfiguration(QSettings *settings)
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    settings->beginGroup("NWPPipeline");
    name = settings->value("name", "").toString();
    fileDir = settings->value("path", "").toString();
    fileFilter = settings->value("fileFilter", "*").toString();
    memoryManagerID = settings->value(
                                      "memoryManagerID",
                                      sysMC->getDefaultMemoryManagers()->value("NWP"))
                              .toString();
    dataFormat = getNWPFormatFromString(
            settings->value("fileFormat", "CF_NETCDF").toString());
    enableRegridding = settings->value("enableRegridding", false).toBool();
    enableProbabiltyRegionFilter = settings->value(
            "enableProbabilityRegionFilter", false).toBool();
    treatRotatedGridAsRegularGrid = settings->value(
            "treatRotatedGridAsRegularGrid", false).toBool();
    treatProjectedGridAsRegularLonLatGrid = settings->value(
            "treatProjectedGridAsRegularLonLatGrid", false).toBool();
    convertGeometricHeightToPressure_ICAOStandard = settings->value(
            "convertGeometricHeightToPressure_ICAOStandard",
            false).toBool();
    surfacePressureFieldType = settings->value("gribSurfacePressureFieldType",
                                               "auto").toString();
    auxiliary3DPressureField = settings->value("auxiliary3DPressureField", "").toString();
    consistencyCheckReferenceVariables = settings->value("consistencyCheckRefVars", "").toString();
    disableGridConsistencyCheck = settings->value("disableGridConsistencyCheck",
                                                  false).toBool();
    inputVarsForDerivedVars = inputVarsStringToMap(
        settings->value("inputVarsForDerivedVars", "").toString());

    settings->endGroup();
}


bool MNWPPipelineConfigurationInfo::isValidConfiguration(QSettings *settings)
{
    return isNWPConfig(settings);
}


PipelineType MNWPPipelineConfigurationInfo::getFormat()
{
    return PipelineType::NWP_PIPELINE;
}


QString MNWPPipelineConfigurationInfo::getNWPFormatString() const
{
    switch (dataFormat)
    {
    case CF_NETCDF:
        return "CF_NETCDF";
    case GRIB:
        return "GRIB";
    case INVALID_FORMAT:
    case HDF5:
    default:
        return "";
    }
}


MNWPReaderFileFormat MNWPPipelineConfigurationInfo::getNWPFormatFromString(const QString& text)
{
    if (text == "CF_NETCDF")
    {
        return CF_NETCDF;
    }
    if (text == "GRIB" || text == "ECMWF_GRIB")
    {
        return GRIB;
    }

    return INVALID_FORMAT;
}


bool MNWPPipelineConfigurationInfo::isNWPConfig(QSettings *settings)
{
    return settings->childGroups().contains("NWPPipeline");
}


void MTrajectoriesPipelineConfigurationInfo::saveConfiguration(QSettings *settings)
{
    // File Format.
    // ============
    settings->beginGroup("FileFormat");
    // Save version id of Met.3D.
    settings->setValue("met3dVersion", met3dVersionString);
    settings->endGroup();
    // ============

    // Trajectory pipeline
    settings->beginGroup("TrajectoriesPipeline");
    settings->setValue("name", name);
    settings->setValue("isEnsemble", isEnsemble);
    settings->setValue("ABLTrajectories",
                       boundaryLayerTrajectories);
    settings->setValue("memoryManagerID", memoryManagerID);
    settings->setValue("path", fileDir);

    settings->endGroup();
}


void MTrajectoriesPipelineConfigurationInfo::loadConfiguration(QSettings *settings)
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    settings->beginGroup("TrajectoriesPipeline");
    name = settings->value("name", "").toString();
    isEnsemble = settings->value("isEnsemble", true).toBool();
    memoryManagerID = settings->value("memoryManagerID",
              sysMC->getDefaultMemoryManagers()->value("Trajectories")).toString();

    // Trajectory Actor rework for 1.13: We do not need the "precomputed" key
    // anymore: Trajectory Configs are now only for precomputed trajectories,
    // if precomputed=false, this setting file is not required anymore.
    if (settings->contains("precomputed")
        && ! settings->value("precomputed").toBool())
    {
        QMessageBox msg;
        msg.setWindowTitle("Error");
        msg.setText("The selected trajectory pipeline contains settings for "
                    "trajectories that are computed in Met.3D. Since 1.13, "
                    "these configuration files are obsolete. To compute "
                    "trajectories in Met.3D, simply load the corresponding "
                    "data set instead.");
        msg.setIcon(QMessageBox::Warning);
        msg.exec();
    }

    fileDir = settings->value("path", "").toString();

    boundaryLayerTrajectories = settings->value("ABLTrajectories", false).toBool();

    settings->endGroup();
}


bool MTrajectoriesPipelineConfigurationInfo::isValidConfiguration(QSettings *settings)
{
    return isTrajectoryConfig(settings);
}


PipelineType MTrajectoriesPipelineConfigurationInfo::getFormat()
{
    return PipelineType::TRAJECTORIES_PIPELINE;
}


bool MTrajectoriesPipelineConfigurationInfo::isTrajectoryConfig(QSettings *settings)
{
    return settings->childGroups().contains("TrajectoriesPipeline");
}



void MRadarPipelineConfigurationInfo::saveConfiguration(QSettings *settings)
{
    // File Format.
    // ============
    settings->beginGroup("FileFormat");
    // Save version id of Met.3D.
    settings->setValue("met3dVersion", met3dVersionString);
    settings->endGroup();
    // ============

    // Radar pipeline
    settings->beginGroup("RadarPipeline");
    settings->setValue("name", name);
    settings->setValue("memoryManagerID", memoryManagerID);
    settings->setValue("path", fileDir);
    settings->setValue("fileFilter", fileFilter);
    settings->setValue("azimuthAngles", standardAzimuthGridsize);

    settings->endGroup();
}


void MRadarPipelineConfigurationInfo::loadConfiguration(QSettings *settings)
{
    MSystemManagerAndControl *sysMC = MSystemManagerAndControl::getInstance();

    settings->beginGroup("RadarPipeline");
    name = settings->value("name", "").toString();
    memoryManagerID = settings->value(
                                      "memoryManagerID",
                                      sysMC->getDefaultMemoryManagers()->value("NWP"))
                              .toString();
    fileDir = settings->value("path", "").toString();
    fileFilter = settings->value("fileFilter", "*").toString();
    settings->endGroup();
    standardAzimuthGridsize = settings->value("azimuthAngles", 360).toInt();
}


bool MRadarPipelineConfigurationInfo::isValidConfiguration(QSettings *settings)
{
    return isRadarConfig(settings);
}


PipelineType MRadarPipelineConfigurationInfo::getFormat()
{
    return PipelineType::RADAR_PIPELINE;
}


bool MRadarPipelineConfigurationInfo::isRadarConfig(QSettings *settings)
{
    return settings->childGroups().contains("RadarPipeline");
}



}
