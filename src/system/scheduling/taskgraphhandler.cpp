/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "taskgraphhandler.h"

// standard library imports

// related third party imports

// local application imports
#include "scheduler.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTaskGraphHandler::MTaskGraphHandler(MScheduler *scheduler)
        : scheduler(scheduler)
{
    moveToThread(this);

    // Create connections to the event queue:
    // When a new task has been submitted to the graph, or a new run candidate
    // has been added to the graph, schedule and execute tasks.
    // Schedule the tasks if a new task has been submitted to the graph.
    connect(&taskGraph, &MTaskGraph::tasksSubmittedToGraph, this,
            &MTaskGraphHandler::scheduleTasks, Qt::QueuedConnection);

    // Execute new tasks if a task has been completed (as resources might be
    // back available), or if the list of tasks without parent nodes changed.
    connect(&taskGraph, &MTaskGraph::newTaskWithoutParent, this,
            &MTaskGraphHandler::executeTasks, Qt::QueuedConnection);
}


MTask *MTaskGraphHandler::taskGraphContainsDataRequest(
        MScheduledDataSource *dataSource, const MDataRequest& request)
{
    QMutexLocker tgLocker(&taskGraphLock);
    return taskGraph.containsDataRequest(dataSource, request);
}


void MTaskGraphHandler::printTaskGraph() const
{
    QMutexLocker tgLocker(&taskGraphLock);
    taskGraph.printCurrentState();
}


int MTaskGraphHandler::numRunningTasks() const
{
    return taskGraph.numRunningTasks();
}


bool MTaskGraphHandler::isTaskGraphEmpty() const
{
    return taskGraph.isEmpty();
}


void MTaskGraphHandler::submitTask(MTask *task)
{
    QMutexLocker tgLocker(&taskGraphLock);
    taskGraph.submitTask(task);
}


void MTaskGraphHandler::taskCompleted(MTask *task)
{
    QMutexLocker tgLocker(&taskGraphLock);
    taskGraph.setCompleted(task);
    tgLocker.unlock();

    // Force signal to trigger an execution of new tasks. Even when the list
    // of available tasks does not change, completing a task may free resources
    // to use.
    emit taskGraph.newTaskWithoutParent();
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

void MTaskGraphHandler::scheduleTasks()
{
    QMutexLocker tgLocker(&taskGraphLock);

    // Enqueue all tasks into the task graph that have been newly
    // submitted. Traverses recursively the parents of the enqueued tasks
    // and schedules them.
    taskGraph.enqueueSubmittedTasks();
}


void MTaskGraphHandler::executeTasks()
{
    QMutexLocker tgLocker(&taskGraphLock);
    // Look for tasks that can be run.
    const auto& tasks = taskGraph.getScheduledTasksWithoutParents();

    for (int i = tasks.size() - 1; i >= 0; i--)
    {
        MTask* task = tasks[i];
        // If scheduler accepts the task, run it.
        if (scheduler->acceptTaskToRun(task))
        {
            taskGraph.setStarted(task);
            scheduler->runTask(task);
        }
    }
    scheduler->updateBusyStatus();
}

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/
} // Met3D