/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MTASKGRAPHHANDLER_H
#define MET_3D_MTASKGRAPHHANDLER_H

// standard library imports

// related third party imports
#include <QThread>

// local application imports
#include "taskgraph.h"
#include "scheduler.h"

namespace Met3D
{

class MScheduler;

/**
 * The processor thread traversing and accessing the task graph. It enqueues
 * the tasks in the task graph and notifies the scheduler on tasks to
 * execute. This is the sole class that has access to the task graph and
 * locks access to it so it can operate on the task graph thread safe. Some
 * task graph operations are currently not performed by the task graph itself,
 * but for example by the tasks (managing their parents and children). The
 * processor does not handle memory safety for these instances.
 */
class MTaskGraphHandler : public QThread {
    Q_OBJECT
public:
    explicit MTaskGraphHandler(MScheduler* scheduler);

    /**
     * Submit a task to the task graph.
     */
    void submitTask(MTask* task);

    /**
     * Called whenever a task has been completed.
     */
    void taskCompleted(MTask* task);

    /**
     * Checks whether a task with corresponding data source and request string
     * is already present in the task graph.
     * @return The MTask if such a task has been previously submitted to the
     * task graph, and is not yet deleted (so still queued or running). Nullptr
     * otherwise.
     */
    MTask* taskGraphContainsDataRequest(MScheduledDataSource* dataSource,
                                        const MDataRequest& request);

    /**
     * Print the current state of the task graph.
     */
    void printTaskGraph() const;

    /**
     * @return The current number of running tasks.
     */
    int numRunningTasks() const;

    /**
     * @return True if the task graph is empty, false otherwise.
     */
    bool isTaskGraphEmpty() const;

private slots:
    /**
     * Enqueues new tasks into the task graph. This method is called async
     * from this thread.
     */
    void scheduleTasks();

    /**
     * Executes tasks in the task graph that can be executed. This is called
     * async from this thread.
     */
    void executeTasks();

private:
    MScheduler* scheduler;

    MTaskGraph taskGraph;
    mutable QMutex taskGraphLock;
};


} // Met3D


#endif //MET_3D_MTASKGRAPHHANDLER_H
