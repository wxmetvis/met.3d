/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef MET_3D_MWORKERTHREAD_H
#define MET_3D_MWORKERTHREAD_H

// standard library imports

// related third party imports
#include <QThread>

// local application imports

namespace Met3D
{

/**
 * Compact helper class for temporary QThreads.
 * This class extends from QThread adding functionality to
 * (a) run a lambda function
 * (b) destroying itself once the function has been completed.
 */
class MWorkerThread : public QThread
{
public:
    explicit MWorkerThread(std::function<void()> func);

    /**
     * Run the task itself.
     */
    void run() override;

private:
    // The function to run.
    std::function<void()> runFunc;
};


} // Met3d


#endif //MET_3D_MWORKERTHREAD_H
