/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "taskgraph.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>

// local application imports
#include "util/mutil.h"
#include "scheduler.h"

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MTaskGraph::MTaskGraph()
= default;

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MTaskGraph::submitTask(MTask *task)
{
    task->setState(SUBMITTED);

    QMutexLocker taskLocker(&taskMutex);
    submittedTasks.append(task);
    taskLocker.unlock();

    emit tasksSubmittedToGraph();
}


void MTaskGraph::setStarted(MTask *task)
{
    // Update the state of the task.
    task->setState(IN_PROGRESS);

    QMutexLocker taskLocker(&taskMutex);
    runningTasks.append(task);
    scheduledTasksWithoutParents.removeOne(task);
    taskLocker.unlock();
}


void MTaskGraph::setCompleted(MTask *task)
{
    task->setState(COMPLETED);

    removeTask(task);
    updateScheduledLists();
}


void MTaskGraph::printCurrentState() const
{
    QMutexLocker locker(&taskMutex);

    QVector<QVector<MTask *>> taskListsToPrint = {submittedTasks, scheduledTasksWithParents,
                                                  scheduledTasksWithoutParents, runningTasks};
    QStringList headersToPrint = { "\nTASKS SUBMITTED:\n",
                                   "\nTASKS SCHEDULED WITH PARENTS:\n",
                                   "\nTASKS SCHEDULED WITHOUT PARENTS:\n",
                                   "\nTASKS RUNNING:\n"};

    for (int i = 0; i < taskListsToPrint.size(); i++)
    {
        auto taskListToPrint = taskListsToPrint[i];
        QString str = headersToPrint[i];

        for (MTask* task : taskListToPrint)
        {
            QString parentsString;
            for (MTask* parent : task->getAndLockParents())
            {
                QString s;
                QTextStream(&s) << " " << parent;
                parentsString += s;
            }
            task->unlockParents();

            QString childrenString;
            for (MTask* child : task->getAndLockChildren())
            {
                QString s;
                QTextStream(&s) << " " << child;
                childrenString += s;
            }
            task->unlockChildren();

            QString taskString;
            QTextStream taskStringStream(&taskString);
            taskStringStream << "* task " << task << " [mem.res.: " << task->numAdditionalMemoryReservations()
                             << ", state: " << MTask::stateToString(task) << "]"
                             << "[children: " << task->numChildren() << "," << childrenString << "]"
                             << "[parents: " << task->numParents() << "," << parentsString << "],\n"
                             << "> data source " << task->getDataSource() << ": " << "request " << task->getRequest() << "\n";
            str += taskString;
        }

        LOG4CPLUS_DEBUG(mlog, str);
    }

    LOG4CPLUS_DEBUG(mlog, "\n");
}

/******************************************************************************
***                             PUBLIC SLOTS                                ***
*******************************************************************************/

/******************************************************************************
***                            PRIVATE METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/


MTask *MTaskGraph::containsDataRequest(MScheduledDataSource *dataSource,
                                       const MDataRequest &request)
{
    // In the unlikely event that containsDataRequest() is called between a call
    // to scheduleTaskGraph(), which submits a task to the task graph, and the
    // subsequent traversal of the task graph to enqueue and schedule this task
    // and its parent tasks, (MTaskGraphHandler::scheduleTasks()), we need to
    // enforce task graph traversal here.
    if (! submittedTasks.isEmpty())
    {
        enqueueSubmittedTasks();
    }

    QMutexLocker taskLocker(&taskMutex);

    auto candidateLists = {scheduledTasksWithParents,
                                           scheduledTasksWithoutParents,
                                           runningTasks};

    // Iterate over scheduled and running tasks, and look for one that matches
    // the data source and request string.
    for (auto taskList : candidateLists)
    {
        for (auto task : taskList)
        {
            if (task->getDataSource() == dataSource &&
                task->getRequest() == request)
            {
                return task;
            }
        }
    }

    return nullptr;
}


void MTaskGraph::removeTask(MTask *task)
{
    // Lock access to all lists to keep them bundled thread safe.
    QMutexLocker taskLocker(&taskMutex);

    scheduledTasksWithParents.removeOne(task);
    scheduledTasksWithoutParents.removeOne(task);
    runningTasks.removeOne(task);
    submittedTasks.removeOne(task);

    // Remove all links to the task node in the graph.
    task->removeFromTaskGraph();
    delete task;
}


void MTaskGraph::exchangeTask(MTask* taskToExchange, MTask* targetTask)
{
    // Exchange the link of all children of "task" to pointing to the
    // identified duplicate task instead of this one.
    if (taskToExchange->hasChildren())
    {
        for (MTask *child : taskToExchange->getAndLockChildren())
        {
            child->exchangeParent(taskToExchange, targetTask);
        }
        taskToExchange->unlockChildren();
    }
    else
    {
        targetTask->addAdditionalMemoryReservation(1);
    }
}


void MTaskGraph::traverseAndEnqueueDepthFirst(MTask *task)
{
    // Special case: The task graph is re-using tasks that already have been
    // scheduled, e.g., after replacing a duplicate task. Do not reschedule
    // this task again.
    if (task->hasBeenScheduled())
    {
        return;
    }

    // Check if a task with the same data source and same request (thus,
    // generating the same data item), is already queued. If so, exchange this
    // task with the already existing task in the task graph, and reuse the
    // duplicateTask for both instances. Then, delete the task requested to be
    // enqueued, including its parents.
    if (MTask *duplicateTask =
            containsDataRequest(task->getDataSource(), task->getRequest()))
    {
        exchangeTask(task, duplicateTask);

        // Delete "task" and all its parents; they are not needed anymore.
        deleteUnscheduledTaskGraph(task);
        return;
    }

    // Enqueue all parents (i.e. the dependencies) of this task first.
    for (MTask *parent : task->getAndLockParents())
    {
        traverseAndEnqueueDepthFirst(parent);
    }
    task->unlockParents();

    // Enqueue this task.
    task->setState(SCHEDULED);

    // Update the lists.
    QMutexLocker taskLocker(&taskMutex);
    submittedTasks.removeOne(task);
    scheduledTasksWithParents.append(task);
    taskLocker.unlock();
}


void MTaskGraph::deleteUnscheduledTaskGraph(MTask *task)
{
    // Special care needs to be taken if the task graph to be deleted
    // contains links to tasks that are already scheduled by another task
    // graph. Don't delete those (and their subgraphs)!
    if (task->hasBeenScheduled()) return;

    // Obtain a reference to the list of parent tasks. Iterate backwards
    // through this list: If the recursive call to deleteUnscheduledTaskGraph()
    // deletes the current parent task this will modify the list of parent tasks
    // (the parent task will be removed from the list). With forward iteration,
    // we would then need additional logic to adjust the current index to make
    // sure no parent task is missed.
    const QList<MTask*>& parents = task->getAndLockParents();
    for (int i = parents.size() - 1; i >= 0; i--)
    {
        MTask *parent = parents[i];
        deleteUnscheduledTaskGraph(parent);
    }
    task->unlockParents();

    // Cancel the task's input requests that were available during task
    // construction; they were reserved in the memory manager and are not
    // needed anymore.
    task->cancelInputRequestsWithoutParents();
    removeTask(task);
}


void MTaskGraph::enqueueSubmittedTasks()
{
    // Pop elements from the list of submitted tasks until it is empty.
    // Enqueue these tasks and their parents depth first.
    bool submitTasks = !submittedTasks.isEmpty();
    while (! submittedTasks.isEmpty())
    {
        QMutexLocker taskLock(&taskMutex);
        auto task = submittedTasks.takeFirst();
        taskLock.unlock();

        traverseAndEnqueueDepthFirst(task);
    }

    if (submitTasks) updateScheduledLists();
}


const QVector<MTask *> &MTaskGraph::getScheduledTasksWithoutParents() const
{
    return scheduledTasksWithoutParents;
}


int MTaskGraph::numRunningTasks() const
{
    return runningTasks.size();
}


void MTaskGraph::updateScheduledLists()
{
    QMutexLocker taskLocker(&taskMutex);

    bool listChanged = false;
    // Loop over all tasks with parents, and move these to the list of
    // tasks without parents if they do not have a parent (anymore).
    for (int i = scheduledTasksWithParents.size() - 1; i >= 0; i--)
    {
        auto task = scheduledTasksWithParents[i];
        if (! task->hasParents())
        {
            scheduledTasksWithParents.removeAt(i);
            scheduledTasksWithoutParents.append(task);
            listChanged = true;
        }
    }
    taskLocker.unlock();

    if (listChanged)
    {
        emit newTaskWithoutParent();
    }
}

} // Met3D