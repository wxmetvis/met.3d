/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/

#ifndef MET_3D_MTASKGRAPH_H
#define MET_3D_MTASKGRAPH_H

// standard library imports

// related third party imports
#include <QObject>
#include <QThread>
#include <QVector>
#include <QMutex>
#include <QWaitCondition>

// local application imports
#include "data/datarequest.h"

namespace Met3D
{

class MTask;
class MScheduledDataSource;

/**
 * The MTaskGraph class represents a directed graph of computational tasks.
 * It manages task submission, dependency resolution, and state tracking but
 * does not execute tasks itself. Instead, it ensures that tasks are properly
 * scheduled and avoids duplicate submissions.
 *
 * The class is itself NOT implemented thread safe. The MTaskGraphHandler is
 * used as single access point to the task graph.
 */
class MTaskGraph : public QObject
{
    Q_OBJECT
public:
    MTaskGraph();

    /**
     * Submits the task into the task graph.
     * @param task The task to submit.
     */
    void submitTask(MTask *task);

    /**
     * Set the task as started in the task graph. This only handles the graph
     * state, and does not execute the task.
     * @param task The task which is started.
     */
    void setStarted(MTask *task);

    /**
     * Set the @c task as completed. Updates its state and removes it from
     * the graph. Finally, deletes the task.
     * @param task The task which is completed.
     */
    void setCompleted(MTask * task);

    /**
     * Checks whether a task matching the given data source and request string
     * is already present in the task graph. This can be used to find
     * duplicate tasks.
     *
     * @param dataSource The data source.
     * @param request The request string.
     * @return The matching task in the task graph if one such task is present,
     * or the first match if multiple matches are in the graph. Nullptr if no
     * match is found.
     */
    MTask *containsDataRequest(MScheduledDataSource *dataSource,
                               const MDataRequest &request);

    /**
     * Prints the current state of the task graph to the debug log.
     */
    void printCurrentState() const;

    /**
     * Checks if the task graph is completely empty.
     * @return True if empty.
     */
    bool isEmpty() const
    {
        return submittedTasks.isEmpty() && scheduledTasksWithParents.isEmpty()
               && scheduledTasksWithoutParents.isEmpty();
    }

    /**
     * @return The number of currently running tasks in the graph, denoted by
     * the state @c MTaskState of each task.
     */
    int numRunningTasks() const;

    /**
     * Take all the submitted tasks from the submitted list, and schedule these
     * tasks and their parent tasks. Calls traverseAndEnqueueDepthFirst()
     * recursively to enqueue them into the scheduled list.
     */
    void enqueueSubmittedTasks();

    /**
     * Deletes the task graph starting from task to all of its parent tasks.
     * Calls itself recursively for the parents.
     * @param task The unscheduled task to delete.
     */
    void deleteUnscheduledTaskGraph(MTask *task);

    /**
     * @return A reference to the currently scheduled tasks without parents.
     */
    const QVector<MTask *> &getScheduledTasksWithoutParents() const;

signals:
    /**
     * This signal is emitted whenever there are new scheduled tasks without
     * parents in the task graph. Can be used to trigger their computation.
     */
    void newTaskWithoutParent();

    /**
     * This signal is emitted whenever there have been new tasks submitted
     * to the graph. Note: These tasks are not queued yet.
     */
    void tasksSubmittedToGraph();

private:
    /**
     * Remove the provided task from the task graph. This will update the
     * task lists managed by this class. Parent and children connections are
     * updated. Also, the task itself is deleted.
     * @param task The task to remove from the task graph.
     */
    void removeTask(MTask *task);

    /**
     * Exchanges the task at @c taskToExchange with the task at @targetTask.
     * This will exchange the parent of all children of taskToExchange from
     * self to targetTask.
     * @param taskToExchange The task to exchange.
     * @param targetTask The target task to exchange to.
     */
    void exchangeTask(MTask *taskToExchange, MTask *targetTask);

    /**
     * Enqueue the @c task and all parents of this task recursively depth
     * first into the queue to execute.
     */
    void traverseAndEnqueueDepthFirst(MTask *task);

    /**
     * Update the lists of scheduled tasks. This graph has two lists of
     * scheduled tasks, one containing tasks without and one containing tasks
     * with parents. This is a helpful performance boost to find potential
     * tasks to execute.
     */
    void updateScheduledLists();

    mutable QMutex taskMutex;

    // Current list of tasks in their respective state.
    QVector<MTask *> submittedTasks;
    QVector<MTask *> scheduledTasksWithParents;
    QVector<MTask *> scheduledTasksWithoutParents;
    QVector<MTask *> runningTasks;
};


} // Met3D


#endif //MET_3D_MTASKGRAPH_H
