/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2020-2024 Andreas Beckert [*]
**  Copyright 2025      Christoph Fischer [*]
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "scheduler.h"

// standard library imports

// related third party imports
#include <log4cplus/loggingmacros.h>
#include <QApplication>

// local application imports
#include "util/mutil.h"
#include "data/datarequest.h"
#include "taskgraphhandler.h"
#include "workerthread.h"

// #define DEBUG_OUTPUT_MULTITHREAD_SCHEDULER

namespace Met3D
{
MScheduler* MScheduler::instance = nullptr;

/******************************************************************************
***                            MScheduler                                   ***
*******************************************************************************/
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MScheduler::MScheduler(int maxNumThreads)
    : maxActiveTasks(maxNumThreads),
      maxActiveDiskReaderTasks(4),
      currentlyActiveDiskReaderTasks(0),
      gpuTaskActive(false)
{
    if (maxNumThreads == 0)
    {
        LOG4CPLUS_INFO(mlog, "Initializing single threaded scheduler. "
                             "All tasks will run in the main UI thread.");
    }
    else
    {
        LOG4CPLUS_INFO(mlog, "Initializing multi threaded scheduler. "
                             "Using upper limit of "
                               << maxNumThreads << " threads.");
    }

    // Let a different thread schedule the individual tasks for execution
    // and start the threads for the individual tasks.
    taskGraphHandler = new MTaskGraphHandler(this);
    taskGraphHandler->start();

    // Initialize monitoring of current processing status.
    busyStatus = false;
}

MScheduler::~MScheduler()
{
    // Delete the task graph handler.
    taskGraphHandler->quit();
    taskGraphHandler->wait();
    delete taskGraphHandler;
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MScheduler::initialize(int maxNumThreads)
{
    if (instance)
    {
        LOG4CPLUS_ERROR(mlog, "An instance of the Met.3D scheduler has already "
                              "been initialized. Skipping initialization.");
        return;
    }
    instance = new MScheduler(maxNumThreads);
}


MScheduler* MScheduler::getInstance()
{
    if (instance == nullptr)
    {
        LOG4CPLUS_ERROR(mlog, "Cannot retrieve instance of Met.3D scheduler "
                              "without prior initialization. Returning nullptr.");
    }
    return instance;
}


void MScheduler::deleteInstance()
{
    delete instance;
    instance = nullptr;
}


void MScheduler::scheduleTaskGraph(MTask *task)
{
    if (!task) return;

#ifdef DEBUG_OUTPUT_MULTITHREAD_SCHEDULER
    LOG4CPLUS_TRACE(mlog, "Scheduling task graph for execution: "
                    << task->getRequest());
#endif
    taskGraphHandler->submitTask(task);
}


MTask *MScheduler::isScheduled(MScheduledDataSource *dataSource,
                                          MDataRequest request)
{
    return taskGraphHandler->taskGraphContainsDataRequest(dataSource,
                                                            request);
}


/******************************************************************************
***                          PROTECTED METHODS                              ***
*******************************************************************************/

/******************************************************************************
***                           PRIVATE METHODS                               ***
*******************************************************************************/

bool MScheduler::acceptTaskToRun(MTask* task)
{
    // Do not accept task if too many tasks are already running. If maximum
    // allowed tasks is zero, all tasks are run in the main thread. Then, accept
    // the task.
    if (taskGraphHandler->numRunningTasks() >= maxActiveTasks &&
        maxActiveTasks != 0)
    {
#ifdef DEBUG_OUTPUT_MULTITHREAD_SCHEDULER
        LOG4CPLUS_TRACE(mlog, "Scheduler: Putting task execution on hold "
                              "(max threads reached).");
#endif
        return false;
    }

    if (task->isDiskReaderTask()
                    && currentlyActiveDiskReaderTasks >= maxActiveDiskReaderTasks)
    {
#ifdef DEBUG_OUTPUT_MULTITHREAD_SCHEDULER
        LOG4CPLUS_TRACE(mlog, "Scheduler: Putting disk reader task on hold: "
                        << task->getRequest());
#endif
        return false;
    }

    if (task->isGPUTask() && gpuTaskActive)
    {
#ifdef DEBUG_OUTPUT_MULTITHREAD_SCHEDULER
        LOG4CPLUS_TRACE(mlog, "Scheduler: Putting GPU task on hold: "
                        << task->getRequest());
#endif
        return false;
    }

    // Legal task.
    return true;
}


void MScheduler::runTask(MTask* task)
{
    if (task->isDiskReaderTask())
        currentlyActiveDiskReaderTasks++;
    gpuTaskActive = task->isGPUTask();

#ifdef DEBUG_OUTPUT_MULTITHREAD_SCHEDULER
    LOG4CPLUS_DEBUG(mlog, "Scheduler: Starting execution of task " << task->getRequest());
#endif

    // Run the thread either in the main thread (GPU task, or CPU task if
    // maximum allowed threads is set to zero), or in a new thread (CPU task).
    bool runInMainThread = task->isGPUTask() || maxActiveTasks == 0;

    // Notify the scheduler once the task has been completed.
    // In the future, we could also notify the scheduler here on success or
    // failure of the task.
    auto runner = [=]() { task->run(); this->onTaskCompleted(task); };

    // If GPU task: Run on main thread.
    if (runInMainThread)
    {
        // Trigger GPU task via invocation, so we land in the main thread.
        QMetaObject::invokeMethod(qApp->thread(), runner);
    }
    // CPU tasks: Start a new thread. The thread makes sure it gets deleted
    // after the task finished.
    else
    {
        QThread* executionThread = new MWorkerThread(runner);
        executionThread->start();
    }
#ifdef DEBUG_OUTPUT_MULTITHREAD_SCHEDULER
    printCurrentState();
#endif
}

void MScheduler::onTaskCompleted(MTask* task)
{
    // Decrease counters for specific task types, and update state.
    if (task->isDiskReaderTask()) currentlyActiveDiskReaderTasks--;
    if (task->isGPUTask()) gpuTaskActive = false;

    // Updating the state and the task graph.
    taskGraphHandler->taskCompleted(task);
}


void MScheduler::updateBusyStatus()
{
    QMutexLocker locker(&updateBusyStatusMutex);

    if (busyStatus && taskGraphHandler->isTaskGraphEmpty())
    {
        busyStatus = false;
        emit schedulerIsProcessing(false);
    }
    else if (! busyStatus && ! taskGraphHandler->isTaskGraphEmpty())
    {
        busyStatus = true;
        emit schedulerIsProcessing(true);
    }
}


void MScheduler::printCurrentState() const
{
    taskGraphHandler->printTaskGraph();
}

}
