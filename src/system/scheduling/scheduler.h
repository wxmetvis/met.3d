/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2015      Marc Rautenhaus [*, previously +]
**  Copyright 2020-2024 Andreas Beckert [*]
**  Copyright 2024 Thorwin Vogt [*]
**  Copyright 2025 Christoph Fischer [*]
**
**  * Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  + Computer Graphics and Visualization Group
**  Technische Universitaet Muenchen, Garching, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#ifndef SCHEDULER_H
#define SCHEDULER_H

// standard library imports

// related third party imports
#include <QObject>

// local application imports
#include "task.h"

namespace Met3D
{
class MTaskGraphHandler;
class MSyncControl;

/**
 * The multi thread scheduler uses a separate thread to traverse the task
 * graph and execute tasks that can be executed (thus, have no dependencies).
 * For each task, it starts a new thread and runs the task in the spawned
 * thread. If the task is a GPU task, it will be executed by the main thread
 * instead.
 */
class MScheduler : public QObject
{
friend class MTaskGraphHandler;
    Q_OBJECT

public:
    /**
     * Initialize the scheduler singleton. This should only be called once with
     * the allowed maximum number of threads.
     * @param maxNumThreads Number of threads.
     */
    static void initialize(int maxNumThreads);

    /**
     * Get the scheduler instance.
     */
    static MScheduler* getInstance();

    /**
     * Deletes the scheduler instance.
     */
    static void deleteInstance();

    /**
     * Schedule a task graph for multithreaded execution. It will be
     * submitted to the task graph, which will schedule all of its
     * parents.
     * @param task The task to schedule.
     */
    void scheduleTaskGraph(MTask *task);

    /**
     * Prints the current state of the scheduler.
     */
    void printCurrentState() const;

    /**
     * Queries whether a task with the specified data source and request has
     * already been scheduled for execution. If yes, the corresponding task is
     * returned. If no, a @p nullptr is returned.
     */
    MTask* isScheduled(MScheduledDataSource* dataSource,
                       MDataRequest request);

    /**
     * Whether or not the scheduler is currently processing.
     * @return True, if tasks are processed, otherwise false.
     */
    virtual bool isProcessing() const { return busyStatus; }

private:

    /**
     * Creates a new scheduler with the provided maximum number of
     * concurrent threads to allow.
     */
    explicit MScheduler(int maxNumThreads);

    ~MScheduler();

    /**
     * Checks if by rules imposed by the scheduler the provided @p task
     * should be allowed to run. The caller already has to check that the
     * task has no parents.
     * @param task The task to check.
     * @return True if allowed to run, false otherwise. If allowed, a
     * subsequent call to runTask() will start this task. That task will be
     * started before the next call to acceptTaskToRun() is invoked.
     */
    bool acceptTaskToRun(MTask* task);

    /**
     * Runs the task.
     * If the task is a GPU task, it is sent to the main thread to execute.
     * If the task is a CPU task, a new thread is spawned and the task is
     * executed therein. The thread will be deleted after execution as finished.
     * The slot @c onTaskCompleted() is called once execution finished.
     * @param task The task to execute.
     */
    void runTask(MTask* task);

    /**
     * Updates the scheduler's busy status by checking if the task graph
     * is empty. If it changed to busy, the @c schedulerIsProcessing(true) signal
     * is emitted. If it changed to not busy, the signal with opposing value
     * is emitted.
     */
    void updateBusyStatus();

    /**
     * Slot called once the @c task finished its execution.
     */
    void onTaskCompleted(MTask* task);

signals:
    /**
     * Needs to be emitted by derived classes with @p isProcessing = @p true
     * when processing starts, and with @p isProcessing = @p false after
     * processing has ended.
     */
    void schedulerIsProcessing(bool isProcessing);

private:
    // The thread having access to the task graph.
    MTaskGraphHandler* taskGraphHandler;

    int maxActiveTasks;
    int maxActiveDiskReaderTasks;
    int currentlyActiveDiskReaderTasks;
    bool gpuTaskActive;

    bool busyStatus;
    QMutex updateBusyStatusMutex;

    // Singleton instance of the scheduler.
    static MScheduler* instance;
};

} // namespace Met3D

#endif // SCHEDULER_H
