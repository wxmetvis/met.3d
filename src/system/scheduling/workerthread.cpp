/******************************************************************************
**
**  This file is part of Met.3D -- a research environment for the
**  three-dimensional visual exploration of numerical ensemble weather
**  prediction data.
**
**  Copyright 2025 Christoph Fischer
**
**  Visual Data Analysis Group
**  Universitaet Hamburg, Hamburg, Germany
**
**  Met.3D is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  Met.3D is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
**
*******************************************************************************/
#include "workerthread.h"

// standard library imports

// related third party imports

// local application imports

namespace Met3D
{
/******************************************************************************
***                     CONSTRUCTOR / DESTRUCTOR                            ***
*******************************************************************************/

MWorkerThread::MWorkerThread(std::function<void()> func)
: runFunc(std::move(func))
{
    // The finished signal will be triggered by the QThread once the run()
    // method finished execution. Then, the thread is safe to delete.
    connect(this, &QThread::finished, this, &QThread::quit);
    connect(this, &QThread::finished, this, &QThread::deleteLater);
}

/******************************************************************************
***                            PUBLIC METHODS                               ***
*******************************************************************************/

void MWorkerThread::run()
{
    // QThread triggers finished() signal once this method is done.
    runFunc();
}

} // Met3d