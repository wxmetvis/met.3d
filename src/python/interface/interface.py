"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This is the entry file for the Python interface of Met.3D.
It contains the functions that can be invoked from C++, see the
Met3D::MPyInterface class for reference.

The Met3D::MPyInterface class defines the met3d_bindings module, 
which contains definitions for sending objects and structs from 
C++ to Python.
"""

from util import extract_ctype_buffer

# Met3D imports, as defined in MPyInterface.cpp
import met3d_bindings
# Python bindings, as defined in pythonderivedprocessor.cpp
import derived_var_bindings
log = met3d_bindings.Logger


export_netcdf_endpoint_initialized = False
def export_netcdf(file_path: str, func_name: str, nc_memio: met3d_bindings.NC_memio):
    """
    This function takes the path to a Python file @p file_path, and a function name
    @p func_name in this file, and invokes this function with an xarray Dataset which
    is read from the @p nc_memio memory address.

    @param file_path The file path of the Python file to invoke.
    @param func_name The function to invoke.
    @param nc_memio The NetCDF C API memory struct where the NC file is located.
    """
    log.debug("Executing Python endpoint 'export_netcdf'...")

    # Inform users if packages needs to be imported, this might take some time.
    global export_netcdf_endpoint_initialized
    if not export_netcdf_endpoint_initialized:
        log.info("First time use of Export NetCDF interface, initializing endpoint...")
        export_netcdf_endpoint_initialized = True

    from io import BytesIO
    from util import import_file_from_path
    from xarray import open_dataset

    # Dynamically import the module.
    user_module = import_file_from_path(file_path)
    if user_module is None:
        log.error("Could not load file path '" + file_path
                  + "'. Make sure the path is correct and that it is a valid Python file.")
        return 1

    # Extract the buffer containing the NetCDF dataset from the memory address.
    buffer = extract_ctype_buffer(nc_memio)
    buffer_data = bytearray(buffer)

    with BytesIO(buffer_data) as bio:
        # Read the NetCDF data buffer using xarray.
        ds = open_dataset(bio, engine='h5netcdf')

        # Get the function from the module.
        try:
            func = getattr(user_module, func_name)
        except AttributeError:
            log.error("Function '" + func_name + "' not found in loaded module at '"
                      + file_path + "'. Cannot invoke method.")
            return 1

        # Call the function with the dataset.
        func(ds)

    log.debug("Python endpoint 'export_netcdf' finished.")
    return 0


derived_var_endpoint_initialized = False
def derived_var_endpoint(derivedFunc: derived_var_bindings.EndpointType, in_grid_names: list,
                   out_grid_name: str, nc_memio: met3d_bindings.NC_memio, return_address):
    """
    This function represents the endpoint for computing derived variables using Python.
    It is executed, whenever a data request requests a computation.
    The result of the computation overwrites the dataset provided via the nc_memio pointer.

    @param derivedFunc The Python function type to call.
    @param in_grid_names The variable names of the input grids required. They
    have to be in the correct order to match the Python signature.
    @param out_grid_name The variable name of the output grid.
    @param nc_memio The NetCDF C API memory struct where the NC file is located.
    @param return_address the address where the result file should be written to.
    """
    log.debug(f"Executing Python endpoint for derived computation: '{str(derivedFunc)}'...")

    # Inform users if packages needs to be imported, this might take some time.
    global derived_var_endpoint_initialized
    if not derived_var_endpoint_initialized:
        log.info("First time use of Python computation interface, initializing endpoint...")
        derived_var_endpoint_initialized = True

    from ctypes import c_char, cast, POINTER, memmove
    from xarray import open_dataset, backends
    from netCDF4 import Dataset
    from python_endpoints import invoke_python_endpoint
    from util import copy_xr_dataset_to_in_memory_netcdf

    # Extract the buffer containing the NetCDF dataset from the memory address.
    buffer = extract_ctype_buffer(nc_memio)
    buffer_data = bytearray(buffer)

    # Create a xarray Dataset by reading the NetCDF buffer into a NetCDF dataset.
    dataset_src = Dataset('dummy.nc', mode='r', memory=buffer_data)
    dataset = open_dataset(backends.NetCDF4DataStore(dataset_src))
    target_dtype = dataset[out_grid_name].dtype # Remember the target grid type

    # Invoke the function.
    dataset = invoke_python_endpoint(derivedFunc, dataset, in_grid_names, out_grid_name)

    # Change output type to the type set by the C++ side. Required so we have a file
    # of equal size to the input.
    dataset[out_grid_name] = dataset[out_grid_name].astype(target_dtype)
    # Copy the xarray.Dataset to an in-memory NetCDF file.
    dataset_out = copy_xr_dataset_to_in_memory_netcdf(dataset, len(buffer_data))

    # Close the datasets.
    dataset_src.close()
    res = dataset_out.close()

    # Check if input buffer size and output buffer size match, so we can copy over
    # the data to the pointer where the C++ endpoint expects the data to be.
    if (len(buffer_data) != len(res.tobytes())):
        log.error("The size of the NetCDF dataset returned from the Python endpoint "
                  "does not match the size of the provided input dataset. Cannot copy "
                  "memory for further processing.")
        return 1

    buffer_result = (c_char * len(res.tobytes())).from_buffer_copy(res)
    # Create a ctypes pointer from the memory address
    address_ptr = cast(return_address, POINTER(c_char))

    # Copy each byte from the buffer to the memory address
    memmove(address_ptr, buffer_result, len(buffer_result))

    log.debug(f"Executing Python endpoint to compute '{str(derivedFunc)}' finished.")
    return 0


