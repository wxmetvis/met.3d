"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This file contains the methods to call the individual functions used as
endpoints to computed derived variables in Python.
This file has a separate adapter method for each function, as for example
MetPy expects unit-aware grids. Currently, Met.3D does not keep track of 
units in the data grids, thus, we have to add them manually here.
"""

import metpy.calc as mpcalc
from metpy.units import units

# Met3D bindings, as defined in mpyinterface.cpp
import met3d_bindings
# Derived variable bindings, as defined in pythonderivedprocessor.cpp
import derived_var_bindings
log = met3d_bindings.Logger


def metpy_pv_baroclinic(dataset, in_grid_names, out_grid_name):
    """
    Calculates baroclinic potential vorticity.
    https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.potential_vorticity_baroclinic.html
    """
    # Potential temperature in K
    pt = dataset[in_grid_names[0]] * units('K')
    # Pressure in Pa
    p = dataset[in_grid_names[1]] * units('Pa')
    # u, v is in ms-1.
    u = dataset[in_grid_names[2]] * units('m/s')
    v = dataset[in_grid_names[3]] * units('m/s')

    dataset[out_grid_name] = mpcalc.potential_vorticity_baroclinic(pt, p, u, v)
    return dataset


def metpy_wet_bulb_temperature(dataset, in_grid_names, out_grid_name):
    """
    Calculates wet-bulb temperature.
    https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.wet_bulb_temperature.html
    """
    # Pressure in Pa
    p = dataset[in_grid_names[0]] * units('Pa')
    # Temperature in K
    t = dataset[in_grid_names[1]] * units('K')
    # Temperature in K
    dew_point = dataset[in_grid_names[2]] * units('K')

    log.info("NOTE: The wet_bulb_temperature routine used by MetPy uses Normand method which can be slow on "
             "bigger datasets. Computation may take a while.")
    dataset[out_grid_name] = mpcalc.wet_bulb_temperature(p, t, dew_point)
    return dataset


def metpy_wet_bulb_potential_temperature(dataset, in_grid_names, out_grid_name):
    """
    Calculates wet-bulb potential temperature.
    https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.wet_bulb_potential_temperature.html
    """
    # Pressure in Pa
    p = dataset[in_grid_names[0]] * units('Pa')
    # Temperature in K
    t = dataset[in_grid_names[1]] * units('K')
    # Temperature in K
    dew_point = dataset[in_grid_names[2]] * units('K')

    dataset[out_grid_name] = mpcalc.wet_bulb_potential_temperature(p, t, dew_point)
    return dataset


def metpy_static_stability(dataset, in_grid_names, out_grid_name):
    """
    Calculates the static stability.
    https://unidata.github.io/MetPy/latest/api/generated/metpy.calc.static_stability.html
    """
    # Pressure in Pa
    p = dataset[in_grid_names[0]] * units('Pa')
    # Temperature in K
    t = dataset[in_grid_names[1]] * units('K')

    dataset[out_grid_name] = mpcalc.static_stability(p, t)
    return dataset


def invoke_python_endpoint(func, dataset, in_grid_names, out_grid_name):
    """
    This method gets called to invoke a specific MetPy method,
    @param func The function endpoint to call.
    @param dataset The xarray Dataset to call the function with.
    @param in_grid_names The input variable names referencing DataArrays in the
    dataset. The order is important and has to match the MetPy signature.
    @param out_grid_name The variable name of the grid to populate. This
    grid is already allocated in the dataset.
    @return A dataset with the out_grid_name DataArray filled.
    """

    if func == derived_var_bindings.EndpointType.METPY_PV_BAROCLINIC:
        return metpy_pv_baroclinic(dataset, in_grid_names, out_grid_name)

    elif func == derived_var_bindings.EndpointType.METPY_WET_BULB_TEMPERATURE:
        return metpy_wet_bulb_temperature(dataset, in_grid_names, out_grid_name)

    elif func == derived_var_bindings.EndpointType.METPY_WET_BULB_POTENTIAL_TEMPERATURE:
        return metpy_wet_bulb_potential_temperature(dataset, in_grid_names, out_grid_name)

    elif func == derived_var_bindings.EndpointType.METPY_STATIC_STABILITY:
        return metpy_static_stability(dataset, in_grid_names, out_grid_name)

    return None
