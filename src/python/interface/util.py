"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This file contains utility functions of the Python-C++ interface.
"""

import sys
import ctypes
from importlib import util as importlib_util
from pathlib import Path
from xarray import Dataset as xrDataset
from numpy import datetime64, timedelta64
from netCDF4 import Dataset as netCDF4Dataset

import met3d_bindings


def import_file_from_path(file_path: str):
    """
    Import the file at the given @p file_path as @p module into the current python
    environment. The name of the module will be the file name in the provided path.
    @param file_path The file path.
    @return The imported Python module.
    """
    file_name_no_ext = Path(file_path).stem
    spec = importlib_util.spec_from_file_location(file_name_no_ext, file_path)
    if not spec:
        return None
    module = importlib_util.module_from_spec(spec)
    if not module:
        return None

    sys.modules["module.user"] = module
    spec.loader.exec_module(module)
    return module


def extract_ctype_buffer(nc_memio: met3d_bindings.NC_memio):
    """
    Extract the ctype buffer pointing to the memory passed via the NC_memio struct.
    This struct is defined in netCDF::NC_memio and the interface is defined in
    MPyInterface.h.
    @param nc_memio The NC_memio struct containing a pointer to the memory object.
    @return A ctype buffer pointing to the netCDF data.
    """
    address_capsule = nc_memio.memory
    size = nc_memio.size

    # Read the memory address from the memory capsule. Since Python does not support
    # pointers like some other languages, we need to perform additional steps to
    # interpret the memory value as a void pointer.
    ctypes.pythonapi.PyCapsule_GetPointer.restype = ctypes.c_void_p
    ctypes.pythonapi.PyCapsule_GetPointer.argtypes = [ctypes.py_object, ctypes.c_char_p]
    mem_nc_pointer = ctypes.pythonapi.PyCapsule_GetPointer(address_capsule, None)

    # Interpret the memory address and size as buffer.
    return (ctypes.c_char * size).from_address(mem_nc_pointer)


def copy_xr_dataset_to_in_memory_netcdf(dataset: xrDataset, size_in_bytes):
    """
    Copies the given xarray @c dataset to a new in-memory netCDF dataset.
    Sadly, there is no straightforward way for this conversion besides copying
    the fields manually.
    @param dataset The xarray.Dataset to copy.
    @return A netCDF.Dataset in-memory dataset.
    """

    dataset_out = netCDF4Dataset("dummy.nc", "w", memory=size_in_bytes)

    # Manually copy global attributes from xarray to netCDF4
    for attr_name, attr_value in dataset.attrs.items():
        dataset_out.setncattr(attr_name, attr_value)

    # Manually copy dimensions from xarray to netCDF4
    for dim_name, dim_size in dataset.sizes.items():
        dataset_out.createDimension(dim_name, dim_size)

    # Create and fill a dimension variable for the dimension values
    for coord_name in dataset.coords:
        dim_values = dataset[coord_name].values  # Extract values for this dimension

        # Handle time dimensions specifically
        if coord_name == 'time':
            # Extract the reference time from the attributes
            time_units = dataset[coord_name].attrs.get('units', 'hours since 1970-01-01 00:00:00')
            reference_time_str = time_units.split(' since ')[1]  # Assumes "hours since YYYY-MM-DD"
            reference_time = datetime64(reference_time_str)

            # Convert datetime64 to float (hours since the reference time)
            dim_values = (dim_values - reference_time) / timedelta64(1, 'h')  # Convert to hours since reference time

        # Create the dimension variable
        dim_var = dataset_out.createVariable(coord_name, 'f8', (coord_name,))  # Use float type for time
        dim_var[:] = dim_values  # Set values into the netCDF dimension variable

        # Copy dimension attributes if any
        for attr_name, attr_value in dataset[coord_name].attrs.items():
            dim_var.setncattr(attr_name, attr_value)

        if coord_name == 'time':
            dim_var.setncattr('units', 'hours since 1970-01-01 00:00:00')

    # Manually copy variables from xarray to netCDF4
    for var_name, xarray_var in dataset.data_vars.items():
        # Create the variable in the netCDF4.Dataset
        var_dimensions = xarray_var.dims
        netcdf_var = dataset_out.createVariable(var_name, xarray_var.dtype, var_dimensions)

        # Copy the variable data
        netcdf_var[:] = xarray_var.values  # Copy data into the netCDF variable

        # Copy variable attributes
        for attr_name, attr_value in xarray_var.attrs.items():
            netcdf_var.setncattr(attr_name, attr_value)

    return dataset_out
