#!/usr/bin/env python
"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Lagranto Trajectory to Met.3D Multidimensional Array Converter

This script converts trajectories computed using Lagranto (see 
http://www.lagranto.ethz.ch) into a trajectory format similar 
to the multidimensional array format outlined in the CF 
conventions, see [H.4.1: Multidimensional array representation 
of trajectories] [1].

[1] https://cfconventions.org/Data/cf-conventions/cf-conventions-1.12/cf-conventions.html#_multidimensional_array_representation_of_trajectories

Met.3D utilizes a NetCDF format that adheres closely to the CF 
conventions. This conversion facilitates the clear identification 
of trajectories based on forecast initialization time, trajectory 
start time, and integration times. Additionally, the structure of 
the output format allows for efficient metadata access without 
needing to read the actual trajectory data.

### Requires:
- xarray
- tqdm
- lagranto

### Usage Instructions:
1. Copy the `config.py.template` in this directory to `config.py`.
2. Adjust the input and output directories and set optional flags in the `config.py` file.
3. Execute the script by running:
   python convert_trajectories_lagranto-1-4_to_m3d-cf-multidimensional-array.py
"""

import os, glob, sys
from tqdm.auto import tqdm
import config
from config import LagrantoFormat
from lagranto import Tra

# Import parent directory to access util.py
script_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(script_dir))
from util import init_new_legacy_dataset

# Look for all files in the directory.
file_list = []
if config.input_file_directory is not None:
    file_pattern = os.path.join(config.input_file_directory, '*')
    # Sort files by name.
    file_list = list(sorted(glob.glob(file_pattern)))

# Iterate over files: Create an output file for each input file.
progress = tqdm(file_list, file=sys.stdout, colour='GREEN',
                bar_format='{l_bar}{bar:50}{r_bar}{bar:-50b}')
for file_index, file_path in enumerate(progress):
    file_name = os.path.basename(file_path)
    progress.set_description("Processing input file '%s'" % file_path)

    trajs = Tra()
    if config.input_format == LagrantoFormat.CSV:
        trajs.load_ascii(file_path)
    elif config.input_format == LagrantoFormat.NETCDF:
        trajs.load_netcdf(file_path)
    else:
        print("Unknown format provided, supported are '1' and '4'.")
        exit(1)

    # Check if all the times are consistent within a file.
    reference_times = trajs['time'][0]
    num_times = trajs['time'].shape[0]
    for i in range(1, num_times):
        if not (trajs['time'][i] == reference_times).all():
            print(f"Mismatch of times between trajectories in file '{file_path}'. "
                  f"A file must have consistent times for its trajectories.")
            print("Found the following time lists:")
            print(reference_times)
            print(trajs['time'][i])
            exit()

    # Check if required columns are available.
    if not ({"time", "lon", "lat", "p"} <= set(trajs.variables)):
        print(f"Missing required variables in file '{file_path}'. "
              f"Required are: time, lon, lat, p.")
        continue

    # All other variables are auxiliary variables.
    aux_variables = [var for var in trajs.variables if var not in ['time', 'lon', 'lat', 'p']]

    # Initialize a new xarray dataset in the Met.3D trajectory format.
    dataset = init_new_legacy_dataset(reference_times, trajs.ntra, aux_variables)

    # Iterate over the trajectories and copy the values into the dataset.
    for i in range(trajs.ntra):
        dataset['lon'][dict(trajectory=i)] = trajs['lon'][i]
        dataset['lat'][dict(trajectory=i)] = trajs['lat'][i]
        dataset['pressure'][dict(trajectory=i)] = trajs['p'][i]

        for aux_var in aux_variables:
            dataset[aux_var][dict(trajectory=i)] = trajs[aux_var][i]

    # Forecast init time is optional. Allow overwriting it, see above.
    if config.overwrite_lagranto_initial_for_forecast_init_time:
        it = config.forecast_init_time_per_file[file_index]
        dataset['time'].attrs['forecast_inittime'] = it.isoformat()
    else:
        dataset['time'].attrs['forecast_inittime'] = trajs.initial.isoformat()

    # Trajectory start time is mandatory. Allow overwriting it, see above.
    if config.overwrite_lagranto_startdate_for_trajectory_start_time:
        st = config.trajectory_start_time_per_file[file_index]
        dataset['time'].attrs['trajectory_starttime'] = st.isoformat()
    else:
        dataset['time'].attrs['trajectory_starttime'] = trajs.startdate.isoformat()

    # Change the encoding as Met.3D expects the start time as base time of the dimension.
    dataset['time'].encoding['units'] = "hours since " + trajs.startdate.isoformat()

    dataset = dataset.sortby('time')
    dataset.to_netcdf(os.path.join(config.output_file_directory, file_name))


