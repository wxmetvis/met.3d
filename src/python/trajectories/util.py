"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This file contains util functions used by the trajectory file converters.
"""

import datetime
import xarray as xr
import numpy as np

def init_new_cf_dataset(sample_dataset):

    ds = xr.Dataset(
        coords={
            'obs': ('obs', []),  # Empty observation dimension
            'trajectory': ('trajectory', []),  # Empty trajectory dimension
        },
        attrs={
            'featureType': 'trajectory'
        }
    )
    detected_aux_variables = []

    # Initialize the variables with appropriate dimensions and attributes
    ds['trajectory'] = (('trajectory'), np.array([], dtype=np.uint32))  # Trajectory IDs.
    ds['trajectory'].attrs['cf_role'] = 'trajectory_id'

    ds['rowSize'] = (('trajectory'), np.array([], dtype=np.uint32))  # Integer type for number of observations for each trajectory.
    ds['rowSize'].attrs['long_name'] = 'number of obs for this trajectory'
    ds['rowSize'].attrs['sample_dimension'] = 'obs'

    ds['time'] = (('obs'), np.array([]))  # Double type for time
    ds['time'].attrs['standard_name'] = 'time'
    ds['time'].attrs['long_name'] = 'time'

    ds['lon'] = (('obs'), np.array([], dtype='float32'))  # Float type for longitude
    ds['lon'].attrs['standard_name'] = 'longitude'
    ds['lon'].attrs['long_name'] = 'longitude'
    ds['lon'].attrs['units'] = 'degrees_east'

    ds['lat'] = (('obs'), np.array([], dtype='float32'))  # Float type for latitude
    ds['lat'].attrs['standard_name'] = 'latitude'
    ds['lat'].attrs['long_name'] = 'latitude'
    ds['lat'].attrs['units'] = 'degrees_north'

    ds['pressure'] = (('obs'), np.array([], dtype='float32'))  # Float type for pressure
    ds['pressure'].attrs['standard_name'] = 'air_pressure'
    ds['pressure'].attrs['long_name'] = 'pressure'
    ds['pressure'].attrs['units'] = 'hPa'
    ds['pressure'].attrs['positive'] = 'down'
    ds['pressure'].attrs['axis'] = 'Z'

    # Forecast initialization time is optional as trajectories could be computed
    # from reanalysis data. It should be added for forecast data.
    if "forecast_inittime" in sample_dataset.time.attrs:
        print(sample_dataset.time.attrs["forecast_inittime"])

        ds['forecastInitTime'] = (('trajectory'), np.array([]))
        ds['forecastInitTime'].attrs['standard_name'] = 'init_time'
        ds['forecastInitTime'].attrs['long_name'] = 'initialization time of the forecast'

    # Trajectory start time is mandatory.
    ds['trajectoryStartTime'] = (('trajectory'), np.array([]))
    ds['trajectoryStartTime'].attrs['standard_name'] = 'trajectory_start_time'
    ds['trajectoryStartTime'].attrs['long_name'] = 'start time for this trajectory'

    # Look for auxiliary variables in data.
    for data_var_str in sample_dataset.data_vars:
        data_var = sample_dataset[data_var_str]

        if "auxiliary_data" in data_var.attrs:
            # Auxiliary variable detected. add it to the new data set.
            ds[data_var_str] = (('obs'), np.array([], dtype='float32'))
            ds[data_var_str].attrs = data_var.attrs
            detected_aux_variables.append(data_var_str)

    for data_dim in sample_dataset.dims:
        if data_dim == "start_lon":
            ds['start_lon'] = sample_dataset['start_lon']
        if data_dim == "start_lat":
            ds['start_lat'] = sample_dataset['start_lat']
        if data_dim == "start_isobaric":
            ds['start_isobaric'] = sample_dataset['start_isobaric']

    if "ensemble" in  sample_dataset.dims:
        ds['member'] = (('trajectory'), np.array([], dtype=np.int8))  # Integer type for member of each trajectory
        ds['member'].attrs['standard_name'] = 'member'
        ds['member'].attrs['long_name'] = 'ensemble member for each trajectory'

    ds.attrs["Conventions"] = "CF-1.12"
    ds.attrs["creation_time"] = datetime.datetime.now().isoformat()
    ds.attrs["history"] = "written by lagranto_to_legacy_trajectories.py"
    return ds, detected_aux_variables



def init_new_legacy_dataset(times, num_trajectories, aux_variables, members=None):

    ds = xr.Dataset(
        coords={
            'time': ('time', times),  # Empty observation dimension
            'trajectory': ('trajectory', list(range(num_trajectories))),  # Empty trajectory dimension
        },
        attrs={
            'featureType': 'trajectory'
        }
    )

    # Dimensions per vertex.
    per_vertex_dimensions = ('trajectory', 'time')
    if members is not None:
        ds = ds.assign_coords({"ensemble": members})
        per_vertex_dimensions = ('ensemble', 'trajectory', 'time')

    ds['time'] = (('time'), times)  # Double type for time
    ds['time'].attrs['standard_name'] = 'time'
    ds['time'].attrs['long_name'] = 'time'

    ds['lon'] = (per_vertex_dimensions, np.ones(shape=(num_trajectories, len(times))) * np.nan)
    ds['lon'].attrs['standard_name'] = 'longitude'
    ds['lon'].attrs['long_name'] = 'longitude'
    ds['lon'].attrs['units'] = 'degrees_east'

    ds['lat'] = (per_vertex_dimensions, np.ones(shape=(num_trajectories, len(times))) * np.nan)
    ds['lat'].attrs['standard_name'] = 'latitude'
    ds['lat'].attrs['long_name'] = 'latitude'
    ds['lat'].attrs['units'] = 'degrees_north'

    ds['pressure'] = (per_vertex_dimensions, np.ones(shape=(num_trajectories, len(times))) * np.nan)
    ds['pressure'].attrs['standard_name'] = 'air_pressure'
    ds['pressure'].attrs['long_name'] = 'pressure'
    ds['pressure'].attrs['units'] = 'hPa'
    ds['pressure'].attrs['positive'] = 'down'
    ds['pressure'].attrs['axis'] = 'Z'

    # Forecast initialization time is optional as trajectories could be computed
    # from reanalysis data. It should be added for forecast data.
    # Starting time of a trajectory is mandatory.
    ds['time'].attrs['trajectory_starttime'] = None

    for aux_var in aux_variables:
        ds[aux_var] = (per_vertex_dimensions, np.ones(shape=(num_trajectories, len(times))) * np.nan)
        ds[aux_var].attrs['auxiliary_data'] = 'yes'

    ds = ds.drop_vars("trajectory")

    ds.attrs["Conventions"] = "Similar to CF-1.12"
    return ds