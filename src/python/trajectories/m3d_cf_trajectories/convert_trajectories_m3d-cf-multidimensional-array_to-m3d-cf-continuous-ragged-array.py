#!/usr/bin/env python
"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
Multidimensional to Contiguous Ragged Array Trajectory Format Converter

This script transforms precomputed trajectory NetCDF files from the 
multidimensional array format outlined in [H.4.1. Multidimensional 
array representation of trajectories] [1] to the contiguous ragged array 
format outlined in [H.4.3. Contiguous ragged array representation of 
trajectories] [2].
[1] https://cfconventions.org/Data/cf-conventions/cf-conventions-1.12/cf-conventions.html#_multidimensional_array_representation_of_trajectories
[2] https://cfconventions.org/Data/cf-conventions/cf-conventions-1.12/cf-conventions.html#_contiguous_ragged_array_representation_of_trajectories 

The transformation facilitates compatibility with systems utilizing the 
contiguous ragged array representation for trajectories, improving 
metadata handling and storage efficiency.

### Requires:
- xarray

### Usage Instructions:
1. Copy the `config.py.template` in this directory to `config.py`.
2. Adjust the input and output directories and set optional flags in the `config.py` file.
3. Execute the script by running:
   python convert_trajectories_m3d-cf-multidimensional-array_to-m3d-cf-continuous-ragged-array.py
"""

#############################################################################

#############################################################################

import os, sys, glob
import config
import xarray as xr
from tqdm import tqdm
from datetime import datetime

# Import parent directory to access util.py
script_dir = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(script_dir))
from util import init_new_cf_dataset

# Collect the input files.
file_pattern = os.path.join(config.input_file_directory, '*.nc')
file_list = glob.glob(file_pattern)

out_ds = None

# We cannot do a xarray.open_mfdataset() here merging all the files, as the time dimension
# in the files can be different. Therefore, we need to loop over all the files and add
# them to the new dataset.
num_trajectories = 0
num_obs = 0

# We cannot easily resize the lists at the start, as we do not know how many values we
# have before iterating over all elements. The old trajectory format pads shorter
# trajectories with -999 (invalid values), we cut these in the ragged array representation.
lon_per_obs = []
lat_per_obs = []
pressure_per_obs = []
time_per_obs = []
forecast_it_per_trajectory = []
traj_start_time_per_trajectory = []
member_per_trajectory = []
row_size_per_trajectory = []
aux_per_obs = dict()

detected_aux_variables = []

for file_id, input_file in enumerate(file_list):
    print(f"Start processing file 'input_file' ({file_id}/{len(file_list)-1})...")

    old_dataset = xr.open_dataset(input_file)

    # Initialize new data set based on fields present in first file.
    if out_ds is None:
        out_ds, detected_aux_variables = init_new_cf_dataset(old_dataset)
        for aux_var in detected_aux_variables:
            aux_per_obs[aux_var] = []

    # Check if we have an ensemble dimension.
    ensemble_members = []
    if "ensemble" in old_dataset.dims:
        ensemble_members = old_dataset.ensemble.values
    else:
        ensemble_members = [None]

    num_trajectories_per_member = len(old_dataset.trajectory)
    num_times = len(old_dataset.time)

    # Look for forecast initialization time and trajectory start time in the input file.
    init_time_dt = None
    if "forecast_inittime" in old_dataset.time.attrs:
        init_time_str = old_dataset.time.attrs["forecast_inittime"]
        init_time_dt = datetime.fromisoformat(init_time_str)

    traj_start_time_dt = None
    if "trajectory_starttime" in old_dataset.time.attrs:
        traj_start_time_str = old_dataset.time.attrs["trajectory_starttime"]
        traj_start_time_dt = datetime.fromisoformat(traj_start_time_str)

    num_sub_tasks = len(ensemble_members) * num_trajectories_per_member

    # Iterate over all the trajectories, copy the data over into the ragged array.
    for id in tqdm(range(num_sub_tasks), file=sys.stdout, colour='GREEN',
                bar_format='{l_bar}{bar:50}{r_bar}{bar:-50b}'):
        trajectory_id = id % num_trajectories_per_member
        member_id = int(id / num_trajectories_per_member)

        task_sub_dataset = old_dataset.isel(trajectory=trajectory_id)
        member = None
        if "ensemble" in old_dataset.dims:
            member = member_id
            task_sub_dataset = task_sub_dataset.isel(ensemble=member_id)

        # Add new "obs". Parse trajectory until we hit invalid values.
        num_new_obs = 0
        for i in range(num_times):
            if task_sub_dataset.lon[i] < -999:  # Invalid, stop here.
                break
            num_new_obs += 1

        new_lons = task_sub_dataset.lon.data[:num_new_obs]
        new_lats = task_sub_dataset.lat.data[:num_new_obs]
        new_p = task_sub_dataset.pressure.data[:num_new_obs]
        new_times = task_sub_dataset.time.data[:num_new_obs]

        new_aux_vals = dict()
        for aux_var in detected_aux_variables:
            new_aux_vals[aux_var] = task_sub_dataset[aux_var].data[:num_new_obs]

        # Update the lists.
        num_trajectories += 1
        num_obs += num_new_obs

        lon_per_obs.extend(new_lons)
        lat_per_obs.extend(new_lats)
        pressure_per_obs.extend(new_p)
        time_per_obs.extend(new_times)

        for aux_var in detected_aux_variables:
            aux_per_obs[aux_var].extend(new_aux_vals[aux_var])

        if init_time_dt is not None:
            forecast_it_per_trajectory.append(init_time_dt)

        if traj_start_time_dt is not None:
            traj_start_time_per_trajectory.append(traj_start_time_dt)

        if member is not None:
            member_per_trajectory.append(member)

        row_size_per_trajectory.append(num_new_obs)

    print("Finished file " + input_file)

# Write all generated lists into the data set.
out_ds = out_ds.reindex(trajectory=list(range(num_trajectories)))
out_ds = out_ds.reindex(obs=list(range(num_obs)))

out_ds['rowSize'].data = row_size_per_trajectory

out_ds['lon'].data = lon_per_obs
out_ds['lat'].data = lat_per_obs
out_ds['pressure'].data = pressure_per_obs
out_ds['time'].data = time_per_obs

for aux_var in detected_aux_variables:
    out_ds[aux_var].data = aux_per_obs[aux_var]

if 'forecastInitTime' in out_ds.data_vars:
    out_ds['forecastInitTime'].data = forecast_it_per_trajectory
if 'trajectoryStartTime' in out_ds.data_vars:
    out_ds['trajectoryStartTime'].data = traj_start_time_per_trajectory
if 'member' in out_ds.data_vars:
    out_ds['member'].data = member_per_trajectory

# Drop 'obs' as variable to be non-indexed.
out_ds = out_ds.drop_vars("obs")

out_ds.to_netcdf(config.output_file)