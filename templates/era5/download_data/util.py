import datetime
from dateutil.relativedelta import *
from enum import Enum

# Supported level types to download.
class LevelType(Enum):
    PRESSURE_LEVELS = 1,
    MODEL_LEVELS = 2,
    ISENTROPIC_LEVELS = 3,
    SURFACE = 4,


# Supported file types.
class OutputFormat(Enum):
    GRIB = 1,
    NETCDF = 2


# Time interval type for defining a range of dates.
class TimeInterval(Enum):
    DAY = 1,
    MONTH = 2,
    YEAR = 3


def level_type_to_string(t: LevelType):
    if t == LevelType.PRESSURE_LEVELS:
        return "pl"
    if t == LevelType.MODEL_LEVELS:
        return "ml"
    if t == LevelType.ISENTROPIC_LEVELS:
        return "pt"
    if t == LevelType.SURFACE:
        return "sfc"
    print("Unknown LevelType.")
    return None


def output_format_to_string(f: OutputFormat):
    if f == OutputFormat.GRIB:
        return "grib"
    if f == OutputFormat.NETCDF:
        return "netcdf"
    print("Unknown OutputFormat.")
    return None


# Increase time from date by value, with the given interval type.
def increase_time(date: datetime.date, interval_type: TimeInterval, value: int):
    if interval_type == TimeInterval.DAY:
        return date + datetime.timedelta(days=value)
    elif interval_type == TimeInterval.MONTH:
        return date + relativedelta(months=value)
    elif interval_type == TimeInterval.YEAR:
        return date + relativedelta(years=value)
    else:
        print("Unknown TimeInterval type.")
        exit(1)
