#!/usr/bin/env python
"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This is the download file to download data from the ERA5 archive.
It downloads surface and pressure level data from the Copernicus CDS,
other data (model levels, isentropic levels) are downloaded from the
ECMWF MARS archive.
Edit the config.py to reflect what data to download and execute this
script to download the data. It also does some sanity checks on the 
input. More details can be found in the README.

Requires: cdsapi
Usage: download.py
"""

import config, sys, pprint
import multiprocessing
from util import *

# Setting up the client.
import cdsapi

try:
    c = cdsapi.Client()
except Exception as e:
    # Check whether the API key is set by the user.
    print(e, file=sys.stderr)
    print("Make sure you have an up-to-date CDS API key, see "
          "https://cds.climate.copernicus.eu/api-how-to "
          "for details.", file=sys.stderr)
    exit(1)

printing_lock = multiprocessing.Lock()

# Do some sanity checks for user-set input:
# NetCDF files on model levels do not contain ak/bk coefficients: Therefore
# it is not possible to visualize them out-of-the-box. Ask user to download
# them as GRIB instead.
if (config.output_file_format == OutputFormat.NETCDF and
        config.parameters[LevelType.MODEL_LEVELS]):
    print("While it is possible to download NetCDF data on model levels, "
          "this data lacks the ak/bk coefficients required to reconstruct "
          "the pressure data. Therefore, we are not able to visualize them "
          "in Met.3D. Consider downloading the data as GRIB or on pressure "
          "levels instead. Download the model levels anyway? [y/n]")
    inp = input()
    if inp != 'y':
        # Remove the model levels from the download.
        config.parameters[LevelType.MODEL_LEVELS] = []

# Visualizing GRIB on model levels requires the corresponding surface pressure
# field. If the surface pressure is not requested, ask if we should download
# it too.
if (config.output_file_format == OutputFormat.GRIB and
        config.parameters[LevelType.MODEL_LEVELS] and
        "SP" not in config.parameters[LevelType.SURFACE]):
    print("To visualize data from GRIB Model Levels, a surface pressure field "
          "is required to reconstruct the pressure data in all three dimensions. "
          "Should the surface pressure field be downloaded as well? [y/n]")
    inp = input()
    if inp == 'y':
        # Append surface pressure to download variables.
        config.parameters[LevelType.SURFACE].append("SP")

# Warn the user that isentropic levels are not supported yet for visualization.
if config.parameters[LevelType.ISENTROPIC_LEVELS]:
    print("Isentropic levels are currently not supported in Met.3D. Continue "
          "the download anyway? [y/n]")
    inp = input()
    if inp != 'y':
        # Remove the isentropic levels from the download.
        config.parameters[LevelType.ISENTROPIC_LEVELS] = []

# Parse dates into date string: List of dates in YYYY-MM-DD joined by '/'.
current_date = config.start_date
request_dates = []
# Iterate until we reach the end date.
while current_date <= config.end_date:
    request_dates.append(current_date)
    current_date = increase_time(current_date, config.interval_type, config.interval)

# File format.
format_string = output_format_to_string(config.output_file_format)


# Formats and emits a request to the Copernicus CDS storage. Data is only available on
# pressure levels and surface, but access is faster, and the data also includes accumulations
# in the reanalysis fields. The MARS archive has these fields separated in the ERA5 forecasts.
def download_single_dataset_from_cds(level_type: LevelType, levels: list, param_list: list, dl_file: str):
    # To fit the CDS format, search all days, months, and years in request dates.
    # Transform them to set and back to remove duplicates.
    years = list(set([request_date.year for request_date in request_dates]))
    months = list(set([request_date.month for request_date in request_dates]))
    days = list(set([request_date.day for request_date in request_dates]))

    request_dict = {
        'product_type': 'reanalysis',
        'format': format_string,
        'variable': param_list,
        'year': years,
        'month': months,
        'day': days,
        'time': config.hours,
        'area': config.area_NWSE,
        'grid': config.grid_resolution
    }

    # Add levels if present (e.g., not for surface data).
    if level_type == LevelType.PRESSURE_LEVELS:
        request_dict['pressure_level'] = levels
        api_endpoint = 'reanalysis-era5-pressure-levels'
    else:  # SURFACE levels
        api_endpoint = 'reanalysis-era5-single-levels'

    with printing_lock:
        print("Starting request for data from the Copernicus CDS:")
        pprint.pprint(request_dict)
        print("to be downloaded to: " + dl_file)

    # Request the data.
    c.retrieve(api_endpoint, request_dict, dl_file)


# Formats and emits a request to the MARS archive. They use '/' separated lists
# for most of the parameters.
def download_single_dataset_from_mars(level_type: LevelType, levels: list, param_list: list, dl_file: str):

    # List of times.
    hours_string = '/'.join(map(str, config.hours))

    # Area.
    area_string = '/'.join(map(str, config.area_NWSE))

    # Resolution.
    grid_res_string = '/'.join(map(str, config.grid_resolution))

    # List of parameters.
    param_string = '/'.join(map(str, param_list))

    # Join levels to string. None if surface levels.
    levels_string = '/'.join(map(str, levels)) if levels is not None else None

    # Map the request dates to string
    request_dates_as_string = map(lambda t: t.strftime("%Y-%m-%d"), request_dates)

    # Join them together.
    date_string = '/'.join(request_dates_as_string)

    # Level type string.
    level_type_string = level_type_to_string(level_type)

    # The request dict to pass to the CDS API.
    request_dict = {
        "class": "ea",
        "type": "an",
        "stream": "oper",
        "expver": "1",
        "date": date_string,
        "levtype": level_type_string,
        "levelist": levels_string,
        "param": param_string,
        "time": hours_string,
        "area": area_string,
        "grid": grid_res_string,
        "format": format_string,
    }

    with printing_lock:
        print("Starting request with data from MARS archive:")
        pprint.pprint(request_dict)
        print("to be downloaded to: " + dl_file)

    # Request the data.
    c.retrieve("reanalysis-era5-complete", request_dict, dl_file)


# Helper function that downloads a single data set given the level
# type and the list of parameters. Calls the download function for
# the Copernicus CDS or MARS archive.
def download_single_dataset(level_type: LevelType, param_list: list):
    if not param_list:
        return  # No parameters for this level type.

    # List of levels.
    levels = None
    if level_type == LevelType.PRESSURE_LEVELS:
        levels = config.pressure_levels
    elif level_type == LevelType.MODEL_LEVELS:
        levels = config.model_levels
    elif level_type == LevelType.ISENTROPIC_LEVELS:
        levels = config.isentropic_levels

    # Download file: config + level type + file extension.
    dl_file = config.target_download_file
    dl_file += "_" + level_type_to_string(level_type)
    if config.output_file_format == OutputFormat.GRIB:
        dl_file += ".grb"
    elif config.output_file_format == OutputFormat.NETCDF:
        dl_file += ".nc"

    # Download the data from CDS if it is present there: The surface
    # and pressure fields are present, access is typically faster
    # there, and accumulations are part of the surface fields as well.
    if level_type in [LevelType.PRESSURE_LEVELS, LevelType.SURFACE]:
        download_single_dataset_from_cds(level_type, levels, param_list, dl_file)
    else:
        download_single_dataset_from_mars(level_type, levels, param_list, dl_file)


# Pool of threads to download the different level types in parallel.
pool = multiprocessing.Pool(processes=len(config.parameters))

# Execute the function to download a single level type in parallel.
pool.starmap(download_single_dataset, config.parameters.items())
pool.close()

pool.join()
