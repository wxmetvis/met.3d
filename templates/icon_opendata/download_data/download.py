#!/usr/bin/env python
"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""

"""
This is the download file to download data from the DWD ICON open data
server. It can download all icon, icon-eu, and icon-d2 configurations.
Edit the config.py to select what data to download.

Requires: enstools
Usage: download.py
"""

import config, sys
from util import *
from enstools.opendata import retrieve_nwp

grid_type = "regular-lat-lon"

# Parse and read provided info from config.
model_string = parse_model_to_string(config.model)

init_time = config.init_time
forecast_hours = config.forecast_hours
merge_files = config.merge_files

# Do some sanity checks:
# ICON global is only available on icosahedral.
if config.model == Model.ICON_GLOBAL:
    print("Data from the ICON global data set is only available on the native "
          "icosahedral grid. This grid is currently unsupported for visualization in "
          "Met.3D. Do you still want to download the data set? [y/n]")
    inp = input()
    if inp == 'n':
        exit(0)
    grid_type = "icosahedral"

if config.parameters[LevelType.MODEL_LEVELS]:
    # Check whether 3-D pressure is also requested. If not, ask the user whether the 3-D
    # pressure field should be downloaded as well. This is required for visualization
    # in Met.3D later on (as auxiliary pressure field).
    if "p" not in config.parameters[LevelType.MODEL_LEVELS]:
        print("To visualize data on model levels, it is required to have a pressure "
              "reference. Do you want to download the corresponding 3-D pressure "
              "field as well? [y/n]")
        inp = input()
        if inp == 'y':
            # Add pressure to the list of variables.
            config.parameters[LevelType.MODEL_LEVELS].append("p")


# Helper function that downloads data of a single level type. Calls
# the enstools routine.
def download_single_dataset(level_type: LevelType, param_list: list):
    if not param_list:
        return  # No parameters for this level type.

    download_forecast_hours = forecast_hours
    # Get the correct level list and forecast hours.
    if level_type == LevelType.MODEL_LEVELS:
        levels = config.model_levels
    elif level_type == LevelType.PRESSURE_LEVELS:
        levels = config.pressure_levels
    elif level_type == LevelType.SINGLE_LEVEL:
        # Surface data: set level to 0.
        levels = 0
    else:
        # Time invariant, set forecast hours to [0]
        download_forecast_hours = [0]
        levels = 0

    level_type_str = level_type_to_string(level_type)

    # Call enstools download routine.
    try:
        retrieve_nwp(variable=param_list,
                     model=model_string,
                     level_type=level_type_str,
                     init_time=init_time,
                     levels=levels,
                     forecast_hour=download_forecast_hours,
                     grid_type=grid_type,
                     dest=config.target_download_directory,
                     merge_files=merge_files,
                     validate_urls=False)
    except ValueError as e:
        # ValueError thrown by enstools: Some input is not present,
        # for example the provided time or one of the provided variables.
        # Catch the error and continue with the next level type.
        print(e, file=sys.stderr)
        print("Error encountered downloading the requested data. "
              "Check the log output for details.")


# Download the level types sequentially. Parallelization could lead
# to race conditions and undefined states on how enstools handles
# the cached log file with available data. Since there is no queue to
# retrieve data, this is not a big loss here.
for level_type, params in config.parameters.items():
    download_single_dataset(level_type, params)
