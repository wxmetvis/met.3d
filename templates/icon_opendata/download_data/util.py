from enum import Enum


# Supported level types from the DWD open data archive.
class LevelType(Enum):
    PRESSURE_LEVELS = 1,
    MODEL_LEVELS = 2,
    SINGLE_LEVEL = 3,
    TIME_INVARIANT = 4


class Model(Enum):
    ICON_D2 = 1,
    ICON_EU = 2,
    ICON_GLOBAL = 3


def level_type_to_string(t: LevelType):
    if t == LevelType.PRESSURE_LEVELS:
        return "pressure"
    if t == LevelType.MODEL_LEVELS:
        return "model"
    if t == LevelType.SINGLE_LEVEL:
        return "single"
    if t == LevelType.TIME_INVARIANT:
        return "time-inv"
    print("Unknown LevelType.")
    return None


def parse_model_to_string(m: Model):
    if m == Model.ICON_D2:
        return "icon-d2"
    if m == Model.ICON_EU:
        return "icon-eu"
    if m == Model.ICON_GLOBAL:
        return "icon"
    print("Unknown Model.")
    return None
