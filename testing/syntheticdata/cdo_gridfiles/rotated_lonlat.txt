# Rotated grid (global)

gridtype  = lonlat
gridsize  = 259920
xsize     = 720
ysize     = 361
xname     = rlon
xlongname = "grid_longitude"
xunits    = "degrees"
yname     = rlat
ylongname = "grid_latitude"
yunits    = "degrees"
xfirst    = -180
xinc      = 0.5
yfirst    = 90
yinc      = -0.5
grid_mapping = rotated_pole
grid_mapping_name = rotated_latitude_longitude
grid_north_pole_latitude = 60.
grid_north_pole_longitude = -37.5