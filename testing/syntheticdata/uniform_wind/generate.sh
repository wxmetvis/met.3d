# Generates an uniform wind field and transforms the data into rotated and
# stereographic coordinates. Note that the wind field itself is NOT rotated.
# Usage: bash generate.sh /path/to/out/directory/

# Generate the NetCDF wind field.
python uniform_wind_field.py $1
# Projects the files to rotated and stereographic.
cdo -f nc -remapbil,../cdo_gridfiles/rotated_lonlat.txt $1wind_data.nc $1wind_data_rotated.nc
cdo -f nc -remapbil,../cdo_gridfiles/polar_stereographic.txt $1wind_data.nc $1wind_data_stereo.nc