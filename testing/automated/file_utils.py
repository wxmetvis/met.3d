"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""


"""
This file contains file util methods used by the testing script.
"""

import os
import shutil
import glob


"""
Clean all the files in the given directory ending with '.png'.
"""
def clean_directory(directory):
    old_ref_filelist = [f for f in os.listdir(directory) if f.endswith(".png")]
    for f in old_ref_filelist:
        fpath = os.path.join(directory, f)
        os.remove(fpath)


"""
Copy the latest images from the src directory to the dst directory.
"""
def copy_latest_images(src_directory, dst_directory):
    for latest_fn in glob.glob(os.path.join(src_directory, '*.png')):
        shutil.copy(latest_fn, dst_directory)


"""
Get the list of actor config files in the provided template directory.
Returns both a list of file paths, and file names.
"""
def get_template_actors(template_directory):
    actor_conf_file_paths = []
    filenames = []

    for root, dirs, files in os.walk(template_directory):
        for file in files:
            if file.endswith(".actor.conf"):
                filenames.append(os.path.splitext(file)[0])
                actor_conf_file_paths.append(os.path.join(root, file))
    return actor_conf_file_paths, filenames
