#!/usr/bin/env python
"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""


"""
This is the entry script for automated tests. Based on predefined configurations
consisting of a data set, data set configuration, and session, an image is
generated in batch mode and compared to a baseline picture. If the pictures
differ, the test fails.

Further instructions on running and creating tests can be found in the 
documentation.

Requires: tqdm, pillow.
Usage: 'main.py --help' for optional arguments. If no arguments is provided,
the configuration saved in met3d_testing.cfg.template is used.
"""

import argparse
import os, sys, json
import shutil
from tqdm import tqdm
from time import time
from config_utils import parse_config, get_config_value, parse_tests
from test_runner import run_met3d_test, compare_screenshots, Met3DTestHelper
from file_utils import clean_directory, copy_latest_images, get_template_actors

fail_mark = u'\033[91m\u2717\033[0m'
success_mark = u'\033[92m\u2713\033[0m'


"""
The main method. It parses the command line arguments and the config file, and loops
over the parsed tests to call the met3d instance to execute the tests.
"""
def main():
    # Parse the command line arguments.
    parser = argparse.ArgumentParser(description='Run Met.3D tests and compare screenshots.')
    parser.add_argument('--config', type=str, default='met3d_testing.cfg.template', help='Path to met3d_testing.cfg file, '
                                                                         'defaults to current directory.')
    parser.add_argument('--base_data_directory_path', type=str,
                        help='Base directory path to the data. This is used to replace the base directory in the '
                             'pipeline config files.')
    parser.add_argument('--base_config_directory_path', type=str,
                        help='Base directory path to the configuration files. The path should contain the '
                             'sessions, datasets, and screenshots subfolders.')
    parser.add_argument('--executable', type=str, help='Absolute path to the Met.3D executable')

    args = parser.parse_args()
    config = parse_config(args.config)

    # Parse Met3D home env variable, also check for Met3D base.
    m3d_home_path = os.getenv("MET3D_HOME")
    if m3d_home_path is None:
        print("'MET3D_HOME' environment variable is not set. Aborting.")
        exit(1)
    if os.getenv("MET3D_BASE") is None:
        print("'MET3D_BASE' environment variable is not set. Aborting.")
        exit(1)

    # Read the .cfg file, and overwrite certain parameters which are overwritten by the command line parser.
    base_directory = get_config_value(config, 'paths', 'base_directory')
    base_config_directory = args.base_config_directory_path or get_config_value(config, 'paths',
                                                                              'base_config_directory_path')
    base_data_directory_path = args.base_data_directory_path or get_config_value(config, 'paths',
                                                                                 'base_data_directory_path')
    test_json_file = get_config_value(config, 'paths', 'test_json_file')
    met3d_executable = args.executable or get_config_value(config, 'met3d', 'executable')

    # Assemble paths: Directories where screenshots, datasets, sessions, and logs are located, as well as
    # full path to the json file containing the tests.
    actor_templates_base_directory_path = os.path.join(m3d_home_path, "templates")

    dataset_directory_path = os.path.join(base_config_directory, "datasets")
    session_directory_path = os.path.join(base_config_directory, "sessions")
    log_directory_path = os.path.join(base_config_directory, "logs")
    test_json_path = test_json_file
    reference_screenshot_directory_path = os.path.join(base_config_directory, "screenshots")

    # Parse the testing JSON file.
    tests = parse_tests(test_json_path)

    # Unroll additional tests to be generated. Look for the actor wildcard:
    # %ALL_TEMPLATES_ICON_EU%
    # This will duplicate the test for each available template actor, loading the
    # respective actor.
    i = 0
    while i < len(tests['tests']):
        test = tests['tests'][i]
        actors = test.get('actors', [])

        template_actor_file_paths, file_names = (
            get_template_actors(os.path.join(actor_templates_base_directory_path, "icon_opendata/vis_config")))
        if "%ALL_TEMPLATES_ICON_EU%" in actors:

            # For each template actor, create a copy of the test, replace the wildcard, and push it to the tests.
            for template_actor_path, template_actor_name in zip(template_actor_file_paths, file_names):
                test_cpy = test.copy()
                test_cpy['actors'] = [template_actor_path if a == "%ALL_TEMPLATES_ICON_EU%" else a for a in test_cpy['actors']]
                test_cpy['name'] = test_cpy['name'] + template_actor_name
                test_cpy['description'] = test_cpy['description'] + template_actor_name

                tests['tests'].insert(i + 1, test_cpy) # Push the copied test

            # Delete test with the wildcard.
            del tests['tests'][i]
        i += 1

    # Log the generated JSON file.
    print("Tests to process:")
    print(tests)
    print("-----------------")
    with open(os.path.join(log_directory_path, "generated_tests.json"), 'w') as json_file:
        json.dump(tests, json_file, indent=4)

    test_passed = [False] * len(tests['tests'])

    # Create a testing helper struct which contains test information required by the test runner.
    test_helper = Met3DTestHelper()
    test_helper.met3d_executable_path = met3d_executable
    test_helper.frontend_config_file = os.path.join(base_config_directory, "frontends/frontend.cfg")
    test_helper.session_directory_path = session_directory_path
    test_helper.latest_screenshot_directory_path = os.path.join(base_config_directory, "tmp" + str(int(time())))
    latest_screenshot_diff_directory_path = test_helper.latest_screenshot_directory_path + "diff"
    test_helper.met3d_log_directory = log_directory_path
    test_helper.base_data_directory_path = base_data_directory_path
    test_helper.base_config_directory_path = base_config_directory
    test_helper.actor_templates_base_directory_path = actor_templates_base_directory_path

    os.makedirs(test_helper.latest_screenshot_directory_path, exist_ok=True)

    # Iterate over all the tests, running them consecutively. Use tqdm for the progress dialog.
    with tqdm(total=len(tests['tests']), desc='Running Tests', file=sys.stdout) as pbar:
        for test_idx, test in enumerate(tests['tests']):
            test_helper.test_name = test.get('name')
            test_helper.session_name = test.get('session_name')
            test_helper.actors = test.get('actors', []) # Empty list if no actors set to load.

            # Get the dataset names. Can be not set to indicate that no data is required for test.
            if 'dataset_name' in test:
                test_helper.dataset_name = test.get('dataset_name')
            if 'dataset_config' in test:
                dataset_config = test.get('dataset_config')
                test_helper.dataset_config_file = dataset_config

            # Run the test.
            tqdm.write("Running test '" + test_helper.test_name + "'...")
            start_time = time()
            run_met3d_test(test_helper)
            end_time = time()

            # Elapsed time
            elapsed_time = end_time - start_time

            # Path of both screenshots (target vs reference)
            reference_screenshot_path = os.path.join(reference_screenshot_directory_path,
                                                     test_helper.test_name) + '.png'
            test_screenshot_path = os.path.join(test_helper.latest_screenshot_directory_path,
                                                test_helper.test_name) + '.png'

            # Check whether screenshots exist and are the same.
            success = True
            if not os.path.exists(test_screenshot_path):
                tqdm.write("Test case " + test_helper.test_name + " did not produce a screenshot. "
                           + "Expected here: " + test_screenshot_path)
                success = False
            if not os.path.exists(reference_screenshot_path):
                tqdm.write("Reference screenshot does not exist. Expected here: " + reference_screenshot_path)
                success = False

            # Compare the screenshots only if they exist.
            if success:
                diff_image, are_similar = compare_screenshots(reference_screenshot_path, test_screenshot_path)

                if not are_similar:
                    tqdm.write("Reference and test images differ. Review the changes manually.")
                    tqdm.write("    Reference screenshot at: " + reference_screenshot_path)
                    tqdm.write("    Generated screenshot at: " + test_screenshot_path)

                    # Make image black/white with white pixels indicating non-matching pixels.
                    diff_image_bw = diff_image.convert('L').point(lambda x : 255 if x > 0 else 0, mode='1')
                    os.makedirs(latest_screenshot_diff_directory_path, exist_ok=True)
                    diff_path = os.path.join(latest_screenshot_diff_directory_path,
                                   test_helper.test_name) + '.png'
                    diff_image_bw.save(diff_path)
                    tqdm.write("    Difference of screenshot at: " + diff_path)
                    success = False

            if success:
                tqdm.write(f"Test '{test_helper.test_name}' passed in {elapsed_time:.1f} seconds {success_mark}")
                test_passed[test_idx] = True
            else:
                tqdm.write(f"Test '{test_helper.test_name}' failed in {elapsed_time:.1f} seconds {fail_mark}")

            # Update progress bar.
            pbar.update(1)

    # Tests done.
    print("--- Summary ---")
    # Print green checkmarks or red crosses for each test.
    summary_string = ' '.join([success_mark if t else fail_mark for t in test_passed])
    print(summary_string)
    print(str(sum(test_passed)) + "/" + str(len(test_passed)) + " tests passed.")

    if sum(test_passed) == len(test_passed):
        print("All tests passed successfully!")
    else:
        print("Not all tests have passed. See the log output for details.")
        print("Do you want to set the newly generated images as new reference images? [y/n]")
        inp = input()

        if inp == 'y':
            print("Cleaning up current reference image folder...")
            clean_directory(reference_screenshot_directory_path)
            print("Copying latest test images to reference image folder...")
            copy_latest_images(test_helper.latest_screenshot_directory_path, reference_screenshot_directory_path)

    print("Finished. ")
    input("Press Enter to delete latest screenshots and differences...")
    shutil.rmtree(test_helper.latest_screenshot_directory_path)
    shutil.rmtree(latest_screenshot_diff_directory_path)
    print("Done!")


# Run main method.
if __name__ == '__main__':
    main()
