"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""


"""
Methods in this script are called from main.py. This script contains
functionality to edit the pipeline/session configuration files
based on the testing configuration, and executing the Met3D subprocess.
It also contains functionality to compare the images.
"""

import os, shutil
import subprocess
import configparser
from PIL import Image, ImageChops
from tqdm import tqdm


"""
Helper struct containing the information needed to run a Met3D instance:
Contains session, dataset, and frontend information, as well as locations
of the screenshots and the logging directory.
"""
class Met3DTestHelper:

    def __init__(self):
        self.met3d_executable_path = None
        self.frontend_config_file = None
        self.dataset_config_file = None
        self.session_directory_path = None
        self.latest_screenshot_directory_path = None
        self.base_data_directory_path = None
        self.base_config_directory_path = None
        self.met3d_log_directory = None
        self.test_name = None
        self.dataset_name = None
        self.session_name = None
        self.actor_templates_base_directory_path = None
        self.actors = []


"""
Runs a Met3D test instance. Creates a temporary dataset config file
which uses the in the configuration specified base data path.
Also adapts the session file to reflect the set screenshot directory
and the frontend configuration to load the correct session and data
set.
"""
def run_met3d_test(helper: Met3DTestHelper):
    session_config_file = os.path.join(helper.session_directory_path, helper.session_name) + ".session.config"
    test_log_file = os.path.join(helper.met3d_log_directory, helper.test_name + '.met3d.log')

    # Edit session configurations to reflect the correct data path. The %DATA_BASE_DIR% string in the dataset
    # configuration file is replaced with base_data_directory_path read from the config.ini. The %TESTING_BASE_DIR%
    # string is replaced with base_config_directory_path.
    session_config_tmp = None
    if session_config_file is not None:
        with open(session_config_file, 'r') as file:
            session_config_file_data = file.read()
        # Replace the target string.
        session_config_file_data = session_config_file_data.replace('%DATA_BASE_DIR%', helper.base_data_directory_path)
        session_config_file_data = session_config_file_data.replace('%TESTING_BASE_DIR%', helper.base_config_directory_path)

        # Write the file to a temporary file to use in this instance. Add tmp_ as prefix of session path.
        directory, filename = os.path.split(session_config_file)
        tmp_filename = f"tmp_{filename}"
        session_config_tmp = os.path.join(directory, tmp_filename)
        with open(session_config_tmp, 'w') as file:
            file.write(session_config_file_data)

    # Edit frontend file to load the correct session directory, the correct session name and the correct dataset name.
    config_parser = configparser.RawConfigParser()
    config_parser.optionxform = str

    config_parser.read(helper.frontend_config_file)
    config_parser.set('SessionManager', 'pathToSessionConfigurations', helper.session_directory_path)
    config_parser.set('SessionManager', 'loadSessionOnStart', "tmp_" + helper.session_name)
    config_parser.set('BatchMode', 'startAnimationAtMostRecentInitTimeOfDataSource', helper.dataset_name)
    # Write it to a temporary frontend configuration file.
    frontend_config_path_tmp = helper.frontend_config_file + ".tmp"
    with open(frontend_config_path_tmp, 'w') as configfile:
        config_parser.write(configfile)

    # Edit dataset configuration to reflect the correct data path. The %DATA_BASE_DIR% string in the dataset
    # configuration file is replaced with base_data_directory_path read from the config.ini.
    dataset_config_tmp = None
    if helper.dataset_config_file is not None:
        # If it is in the templates folder, replace the wildcard.
        helper.dataset_config_file = (
            helper.dataset_config_file.replace('%TEMPLATES%', helper.actor_templates_base_directory_path)
                                      .replace('%TESTING_BASE_DIR%', helper.base_config_directory_path))

        with open(helper.dataset_config_file, 'r') as file:
            dataset_config_file_data = file.read()
        # Replace the target string.
        dataset_config_file_data = dataset_config_file_data.replace('%DATA_BASE_DIR%', helper.base_data_directory_path)
        # Write the file to a temporary file to use in this instance.
        dataset_config_tmp = helper.dataset_config_file + ".tmp"
        with open(dataset_config_tmp, 'w') as file:
            file.write(dataset_config_file_data)

    # Edit screenshot directory and screenshot name in session file based on config.ini and tests.json.
    config_parser = configparser.RawConfigParser()
    config_parser.optionxform = str

    config_parser.read(session_config_tmp)
    config_parser.set('MSession', 'MSyncControls\\MSyncControl_Synchronization\\TimeSeries\\directory',
                      helper.latest_screenshot_directory_path)
    config_parser.set('MSession', 'MSyncControls\\MSyncControl_Synchronization\\TimeSeries\\fileName',
                      helper.test_name)

    with open(session_config_tmp, 'w') as configfile:
        config_parser.write(configfile)

    # Update actors to load alongside: Change %TEMPLATES% to template directory
    for i in range(len(helper.actors)):
        helper.actors[i] = (helper.actors[i].replace('%TEMPLATES%', helper.actor_templates_base_directory_path)
                                            .replace('%TESTING_BASE_DIR%', helper.base_config_directory_path))
    helper.actors = list(dict.fromkeys(helper.actors)) # Remove duplicates form actors.

    # The command to startup the Met3D instance.
    run_args = [helper.met3d_executable_path, "--frontend=" + frontend_config_path_tmp]
    if dataset_config_tmp is not None:
        run_args.append("--datasets=" + dataset_config_tmp)
    if len(helper.actors):
        run_args.append("--actors=" + ';'.join(helper.actors))

    try:
        with open(test_log_file, 'w') as f:
            # Run Met.3D
            subprocess.run(run_args, stdout=f, stderr=subprocess.STDOUT)
    except subprocess.CalledProcessError as e:
        tqdm.write("Error in Met.3D subprocess, check Met.3D logs for more information.")
    except KeyboardInterrupt as e:
        # Keyboard interrupt (e.g., forcefully stopping this script). Cleanup the temporary data.
        shutil.rmtree(helper.latest_screenshot_directory_path)
        os.remove(dataset_config_tmp)
        os.remove(frontend_config_path_tmp)
        os.remove(session_config_tmp)

    # Delete temporary files if succeeded.
    os.remove(dataset_config_tmp)
    os.remove(frontend_config_path_tmp)
    os.remove(session_config_tmp)

"""
Compare the given reference and test screenshots and return True if they match.
This is done using the ImageChops.difference() method to generate a difference image. 
Two images are currently considered the "same", if their RGB channels match at 
each pixel with a maximum difference of 1.
"""
def compare_screenshots(reference_screenshot, test_screenshot):
    ref_image = Image.open(reference_screenshot).convert('RGB')
    test_image = Image.open(test_screenshot).convert('RGB')

    diff = ImageChops.difference(ref_image, test_image)

    rgb_extrema = diff.getextrema()
    # Use a threshold as maximum difference per channel.
    for channel_extrema in rgb_extrema:
        maximum_difference_in_channel = channel_extrema[1]

        # Threshold: if channel difference <= 1 then OK.
        if maximum_difference_in_channel > 1:
            tqdm.write("Detected difference of " + str(maximum_difference_in_channel)
                       + " in one color channel.")
            return diff, False

    return diff, True  # Images are the same.


