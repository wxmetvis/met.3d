"""
This file is part of Met.3D -- a research environment for the
three-dimensional visual exploration of numerical ensemble weather
prediction data.

Copyright 2024 Christoph Fischer

Visual Data Analysis Group
Universitaet Hamburg, Hamburg, Germany

Met.3D is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Met.3D is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Met.3D.  If not, see <http://www.gnu.org/licenses/>.
"""


"""
This file contains methods for parsing and reading config files
associated with the tests.
"""

import configparser
import json


"""
Parse the config .ini file at the given file path.
"""
def parse_config(config_file):
    config = configparser.ConfigParser()
    config.read(config_file)
    return config


"""
Parse the test description JSON file.
"""
def parse_tests(tests_file):
    with open(tests_file, 'r') as file:
        tests = json.load(file)
    return tests


"""
Get the value in the config .ini file in the
given section and the given key.
"""
def get_config_value(config, section, option, fallback=None):
    if config.has_option(section, option):
        return config.get(section, option)
    return fallback
